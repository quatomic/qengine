﻿/* COPYRIGHT
 *
 * file="Constants.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/NumberTypes.h"

/// 
/// \file Constants.h
/// @brief Global constants 
/// 

namespace qengine
{
	constexpr double PI = 3.14159265358979323846;
}

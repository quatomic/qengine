﻿/* COPYRIGHT
 *
 * file="has_function.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

// This macro creates a template struct "has_functionName", that can be used to check if a class has a function named functionName with a given set of parameters

// use:
// MAKE_HAS_FUNCTION(propagate)
// 
// has_propagate<X, void(Param<2>, Param<2>, double)>::value
// this is true, if class X has or inherits a function called propagate, which takes two params and a double and returns void. 

// WATCH OUT FOR NAME-CLASHES! THIS THING INTRODUCEs A NEW NAME FOR EACH PARAMETER IT'S CALLED WITH

namespace qengine
{
	namespace internal
	{
		template<class C, class... Ts>
		struct hasValidReturn
		{
		private:
			template<typename T, typename = decltype(std::declval<C>()(std::declval<Ts>()...))>
			static constexpr bool check(T*) { return true; }

			template<typename>
			static constexpr bool check(...) { return false; }

		public:
			static constexpr bool value = check<C>(0);
		};

		template<class C>
		struct isValid_call
		{
			template<class... Ts>
			constexpr bool operator() (Ts&&...) { return hasValidReturn<C, Ts...>::value; }
		};

		template<class C>
		constexpr auto isValid(C) { return isValid_call<C>{}; }
	}
}


#define MAKE_HAS_FUNCTION_RETURN_QUALIFIED(structName, functionName)						\
template<typename C, template<class> class  returnQualifier, typename... Args>					\
struct structName \
{							\
private:																\
	template<typename T, typename = std::enable_if_t<returnQualifier<decltype(std::declval<T>(). functionName (std::declval<Args>()...))>::value>>												\
	static constexpr bool check(T*){return true;}\
																		\
	template<typename>													\
	static constexpr bool check(...){return false;}						\
																		\
public:																	\
	static constexpr bool value = check<C>(0);							\
}; \
template<typename C, template<class> class  returnQualifier, typename... Args> \
static constexpr bool structName##_v = structName<C, returnQualifier, Args...>::value		



#define MAKE_HAS_FUNCTION(structName, functionName)						\
template<typename, typename T>											\
struct structName {												\
	static_assert(														\
		std::integral_constant<T, false>::value,						\
		"Second template parameter needs to be of function type.");		\
};																		\
																		\
template<typename C, typename Ret, typename... Args>					\
struct structName <C, Ret(Args...)> {							\
private:																\
	template<typename T, typename = std::enable_if_t<std::is_same<decltype(std::declval<T>(). functionName (std::declval<Args>()...)), Ret>::value>>												\
	static constexpr bool check(T*){return true;}\
																		\
	template<typename>													\
	static constexpr bool check(...){return false;};						\
																		\
public:																	\
	static constexpr bool value = check<C>(0);							\
}; \
template<typename T1, typename T2>											\
static constexpr bool structName##_v = structName<T1,T2>::value 

template<class T>
struct any_return
{
	static constexpr bool value = true;
};

template<class T>
struct is_same_return_maker
{
	template<class C>
	struct type :std::is_same<T, C> {};
};



// This macro creates a template struct "structName", that can be used to check if a class has a member variable named functionName with a given type

// WATCH OUT FOR NAME-CLASHES! THIS THING INTRODUCES A NEW NAME FOR EACH PARAMETER IT'S CALLED WITH

#define MAKE_HAS_VAR(structName, varName)						\
template<class C, class Type> \
struct structName \
{\
private:\
	template<class T, class = std::enable_if_t<std::is_same<decltype(std::declval<T>().varName), Type>::value>>\
	static constexpr bool check(T*) { return true; }\
\
	template<class...>\
	static constexpr  bool check(...) { return false; }\
public:\
	static constexpr bool value = check<C>(nullptr);\
};\
\
template<class C, class Type> constexpr bool structName##_v = structName<C, Type>::value

// This macro creates a template struct "structName", that can be used to check if a class has a member variable named functionName with a given type

// WATCH OUT FOR NAME-CLASHES! THIS THING INTRODUCES A NEW NAME FOR EACH PARAMETER IT'S CALLED WITH

#define MAKE_HAS_STATIC_VAR(structName, varName)						\
template<class C, class Type> \
struct structName \
{\
private:\
	template<class T, class = std::enable_if_t<std::is_same<decltype(T::varName), Type>::value>>\
	static constexpr bool check(T*) { return true; }\
\
	template<class...>\
	static constexpr  bool check(...) { return false; }\
public:\
	static constexpr bool value = check<C>(nullptr);\
};\
\
template<class C, class Type> constexpr bool structName##_v = structName<C, Type>::value


#define MAKE_EXIST_FREE_FUNCTION(structName, functionName)						\
template<typename T>											\
struct structName {												\
	static_assert(														\
		std::integral_constant<T, false>::value,						\
		"Second template parameter needs to be of function type.");		\
};																		\
																		\
template<typename Ret, typename... Args>					\
struct structName <Ret(Args...)> {							\
private:																\
	template<typename T, typename = std::enable_if_t<std::is_same<decltype(functionName (std::declval<Args>()...)), T>::value>>												\
	static constexpr bool check(T*){return true;}\
																		\
	template<typename>													\
	static constexpr bool check(...){return false;};						\
																		\
public:																	\
	static constexpr bool value = check<Ret>(0);							\
}; \
template<typename T>											\
static constexpr bool structName##_v = structName<T>::value 

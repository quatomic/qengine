#pragma once
#include <type_traits>

#include "src/Utility/std17/is_same_v.h"

namespace qengine
{
	namespace internal
	{
		template<class T, class SFINAE = void>
		struct isEqualityComparable: std::false_type{};

		template<class T>
		struct isEqualityComparable<T, 
		std::enable_if_t<
			std17::is_same_v<bool, decltype(std::declval<T>() == std::declval<T>()) >
		,void>> : std::true_type{};

		template<class T> static constexpr bool isEqualityComparable_v = isEqualityComparable<T>::value;
	}
}

namespace qengine
{
	namespace internal
	{
		template<class T, class SFINAE = void>
		struct isInequalityComparable: std::false_type{};

		template<class T>
		struct isInequalityComparable<T,
		std::enable_if_t<
			std17::is_same_v<bool, decltype(std::declval<T>() != std::declval<T>()) >
		>> : std::true_type{};

		template<class T> static constexpr bool isInequalityComparable_v = isInequalityComparable<T>::value;
	}
}

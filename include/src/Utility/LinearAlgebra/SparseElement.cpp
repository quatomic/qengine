﻿/* COPYRIGHT
 *
 * file="SparseElement.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/LinearAlgebra/SparseElement.h"

#include <armadillo>

#include "src/NumberTypes.h"
#include "src/Utility/DeepCopy.h"

namespace qengine
{
	namespace internal
	{
		
		template <class T>
		SparseElement<T>::SparseElement(arma::SpMat_MapMat_val<T>&& data):
		data_(std::make_unique<arma::SpMat_MapMat_val<T>>(std::move(data)))
		{
		}

		template <class T>
		SparseElement<T>::SparseElement(SparseElement&& other) noexcept:
			data_(std::move(other.data_))
		{
		}

		template <class T>
		SparseElement<T>::SparseElement(const SparseElement& other):
			data_(internal::deep_copy(other.data_))
		{
		}

		template <class T>
		SparseElement<T>::~SparseElement() = default;

		template <class T>
		arma::SpMat_MapMat_val<T>& SparseElement<T>::data()
		{
			return *data_;
		}

		template <class T>
		const arma::SpMat_MapMat_val<T>& SparseElement<T>::data() const
		{
			return *data_;
		}

		template <class T>
		SparseElement<T>::operator T() const
		{
			return static_cast<T>(*data_);
		}

		template <class T>
		SparseElement<T>& SparseElement<T>::operator=(const SparseElement& other)
		{
			if(&other == this)
			{
				return *this;
			}
			*data_ = *other.data_;
			return *this;
		}

		template <class T>
		SparseElement<T>& SparseElement<T>::operator=(SparseElement&& other) noexcept
		{
			if (&other == this)
			{
				return *this;
			}
			data_ = std::move(other.data_);
			return *this;
		}

		template <class T>
		SparseElement<T>& SparseElement<T>::operator=(T val)
		{
			*data_ = val;
			return *this;
		}

		template <class T>
		SparseElement<T>& SparseElement<T>::operator+=(T val)
		{
			*data_ += val;
			return *this;
		}

		template <class T>
		SparseElement<T>& SparseElement<T>::operator-=(T val)
		{
			*data_ -= val;
			return *this;
		}

		template <class T>
		SparseElement<T>& SparseElement<T>::operator*=(T val)
		{
			*data_ *= val;
			return *this;
		}

		template <class T>
		SparseElement<T>& SparseElement<T>::operator/=(T val)
		{
			*data_ /= val;
			return *this;
		}
	}
}

namespace qengine
{
	namespace internal
	{
		template class SparseElement<real>;
		template class SparseElement<complex>;
	}
}

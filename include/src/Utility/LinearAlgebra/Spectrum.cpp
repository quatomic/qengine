﻿/* COPYRIGHT
 *
 * file="Spectrum.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/LinearAlgebra/Spectrum.h"

/*
 * Some helper-functionality in the top, functionality is very simple otherwise
 */

#include "src/Utility/qengine_assert.h"
#include "src/Utility/narrow.h"

#include "src/Utility/IntegerFunctions.h"


namespace
{
	template<typename number>
	void applySignConvention(qengine::Matrix<number>& eigVecs)
	{
		/// Applies the sign convention that even eigenstates should start positive and odd ones negative. 
		for (unsigned int m = 0; m < eigVecs.n_cols(); m++)
		{
			arma::Col<number> col = eigVecs._mat().col(m);
			auto i = arma::as_scalar(arma::find(arma::abs(col) > 1e-5, 1)); // find index of first element of the current eigvec that has bigger magnitude than 1e-5.
			if (qengine::internal::sgn(eigVecs.at(i, m)) != qengine::narrow<int>(std::pow(-1, m)))
			{
				eigVecs.col(m) = -1.0*qengine::Vector<number>(eigVecs.col(m));
			}
		}
	}

	template<typename number>
	void normalizeEigenstates(qengine::Matrix<number>& eigenvectors, const double normalization)
	{
		for (auto i = 0u; i < eigenvectors.n_cols(); ++i)
		{
			qengine::Vector<number> intermediate = eigenvectors.col(i);

			eigenvectors.col(i) = (normalization/intermediate.norm())*intermediate ;
		}
	}

}

namespace qengine
{
	namespace internal
	{
		template <typename valuesNumber>
		Spectrum<valuesNumber>::Spectrum(const Vector<valuesNumber>& eigenvalues, const Matrix<complex>& eigenstates, double normalization)
			:eigenvalues_(eigenvalues),
			eigenstates_(eigenstates)
		{
			qengine_assert(eigenvalues.size() == eigenvalues.size(), "eigvals and eigvecs are not the same size!");

			applySignConvention(this->eigenstates_);
			normalizeEigenstates(this->eigenstates_, normalization);
		}

		template <typename valuesNumber>
		Spectrum<valuesNumber>::Spectrum(Vector<valuesNumber>&& eigenvalues, Matrix<complex>&& eigenstates, double normalization)
			:eigenvalues_(std::move(eigenvalues)),
			eigenstates_(std::move(eigenstates))
		{
            qengine_assert(eigenvalues_.size() == eigenstates_.n_cols(), "eigvals and eigvecs are not the same size!");

			applySignConvention(this->eigenstates_);
			normalizeEigenstates(this->eigenstates_, normalization);
		}

		template <typename valuesNumber>
		Spectrum<valuesNumber>::Spectrum(Spectrum&& other, const double normalization):
		Spectrum(std::move(other.eigenvalues_),std::move(other.eigenstates_), normalization)
		{
		}

		template <typename valuesNumber>
		valuesNumber Spectrum<valuesNumber>::eigenvalue(const count_t index) const
		{
			return eigenvalues_.at(index);
		}

		template <typename valuesNumber>
		CVec Spectrum<valuesNumber>::eigenvector(const count_t index) const
		{
			return CVec(eigenstates_.col(index));
		}

		template <typename valuesNumber>
		CVec Spectrum<valuesNumber>::makeLinearCombination(const CVec& factors) const
		{
            qengine_assert(factors.size() <= eigenvalues_.size(), "Spectrum.makeLinearCombination: only as many factors as eigenstates are allowed!");

			auto rep = CVec(eigenstates_.n_rows(), 0.0);

			for (auto i = 0u; i < factors.size(); ++i)
			{
				if (factors.at(i) != 0.0)
				{
					rep += factors.at(i)*CVec(eigenstates_.col(i));
				}
			}

			return rep;
		}
		template<typename valuesNumber>
		count_t Spectrum<valuesNumber>::largestEigenstate()
		{
			return eigenvalues_.size() - 1;
		}
	}
}

namespace qengine
{
	namespace internal
	{

	}
}


namespace qengine
{
	namespace internal
	{
		template class Spectrum<real>;
		template class Spectrum<complex>;


	}
}

﻿/* COPYRIGHT
 *
 * file="SqHermitianBandMat.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/Spectrum.h"

namespace qengine
{
    namespace internal
    {
		template<class> class SqDiagMat;
		template<class> class SqDenseMat;
		template<class> class SqSparseMat;
		template<class> class SqHermitianBandMat;

        // The square hermitian band matrix is represented as a dim x bandwidth+1a matrix: eg. [d0 d1 d2] represents a hermitian fivediagonal matrix (as two cols can be inferred from hermicity)
        template<typename number>
        class SqHermitianBandMat{
            static_assert(std::is_same<number, real>::value, "HermitianBandMat MUST be real (currently)");
        
            /// CTOR
        public:
			enum STORAGE_LAYOUT{COL_LAYOUT, ROW_LAYOUT};

            explicit SqHermitianBandMat(count_t dim,count_t bandwidth);
            explicit SqHermitianBandMat(count_t dim,count_t bandwidth, real fill);
            explicit SqHermitianBandMat(const Matrix<number>& M, STORAGE_LAYOUT storage = STORAGE_LAYOUT::COL_LAYOUT);
            explicit SqHermitianBandMat(Matrix<number>&& M, STORAGE_LAYOUT storage = STORAGE_LAYOUT::COL_LAYOUT);

            /// DATA ACCESS
        public:
            Matrix<number>& bands();
            const Matrix<number>& bands() const;

			number get(count_t rowIndex, count_t colIndex) const;

            /// MATH OPERATIONS
        public: 
            void multiplyInPlace(RVec& v) const;
            void multiplyInPlace(CVec& v) const;

			SqHermitianBandMat& operator+=(const SqDiagMat<real>& diagMat);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqHermitianBandMat& operator/=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqHermitianBandMat& operator*=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqHermitianBandMat& operator+=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqHermitianBandMat& operator-=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqHermitianBandMat& operator+=(const SqHermitianBandMat<number1>& other);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqHermitianBandMat& operator-=(const SqHermitianBandMat<number1>& other);

			SqHermitianBandMat operator-() const;
			/// CONVERSIONS
        public:
			explicit operator SqDenseMat<number>() const;
			explicit operator SqSparseMat<number>() const;

			number at(count_t nRow, count_t nCol) const;
            /// MISC
        public:
            count_t dim() const;
            count_t bandwidth() const;

			bool hasNan() const;

        private:
        // todo: maybe make this to a unique pointer to an RMat
            RMat bands_; 
         };
    }
}


namespace qengine 
{
	namespace internal
	{		
		std::ostream& operator<<(std::ostream& stream, const SqHermitianBandMat<real>& matrix);

		SqDenseMat<real> exp(const SqHermitianBandMat<real>& exponent);

		SqDenseMat<real> pow(const SqHermitianBandMat<real>& d, count_t exponent);

		SqDenseMat<real> operator*(SqHermitianBandMat<real> left, const SqHermitianBandMat<real>& right);

		SqHermitianBandMat<real> kron(SqHermitianBandMat<real> left, const SqHermitianBandMat<real>& right);

		SqDenseMat<complex> operator*(complex c, const SqHermitianBandMat<real>& mat);


		SqHermitianBandMat<real> operator+(SqHermitianBandMat<real> left, const SqDiagMat<real>& right);

		SqHermitianBandMat<real> operator+(const SqDiagMat<real>& left, const SqHermitianBandMat<real>& right);

		template<>
		inline SqHermitianBandMat<real> identityMatrix<SqHermitianBandMat>(const count_t dim)
		{
			return SqHermitianBandMat<real>(dim, 0, 1.0);
		}

		Spectrum<complex> getSpectrum(const SqHermitianBandMat<real>& matrix, const count_t largestEigenstate, const real normalization = 1.0);
		Spectrum<real> getSpectrum_herm(const SqHermitianBandMat<real>& matrix, const count_t largestEigenstate, const real normalization = 1.0);
	}
}

﻿/* COPYRIGHT
 *
 * file="SqDenseMat.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/LinearAlgebra/SqDenseMat.h"

#include <armadillo>
#include <assert.h>

#include "src/Utility/qengine_assert.h"
#include "src/Utility/narrow.h"

#include "src/Utility/LinearAlgebra/LapackeWrapper.h" 

#include "src/Utility/LinearAlgebra/SqSparseMat.h"
#include "src/Utility/LinearAlgebra/SqDiagMat.h"
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
namespace qengine
{
	namespace internal
	{
		template <typename number>
		SqDenseMat<number>::SqDenseMat(const count_t dim) :mat_(dim, dim)
		{
		}

		template <typename number>
		SqDenseMat<number>::SqDenseMat(const Matrix<number>& mat) : mat_(mat)
		{
			qengine_assert(mat_._mat().is_square(), "matrix must be square");
		}

		template <typename number>
		SqDenseMat<number>::SqDenseMat(Matrix<number>&& mat) :mat_(std::move(mat))
		{
			qengine_assert(mat_._mat().is_square(), "matrix must be square");
		}

		namespace
		{
			void doTrans(arma::mat& m)
			{
				arma::inplace_trans(m);
			}
			void doTrans(arma::cx_mat& m)
			{
				arma::inplace_strans(m);
			}


			template<class number>
			Matrix<number> matFromVec(const Vector<number>& vec)
			{
				const auto dim = narrow<count_t>(std::sqrt(vec.dim()));
				assert(vec.dim() == dim * dim);

				auto mat = arma::Mat<number>(vec._vec());
				mat.reshape(dim, dim);
				doTrans(mat);
				return Matrix<number>(std::move(mat));
			}
			template<class number>
			Matrix<number> matFromVec(Vector<number>&& vec)
			{
				const auto dim = narrow<count_t>(std::sqrt(vec.dim()));
				assert(vec.dim() == dim * dim);

				auto mat = arma::Mat<number>(std::move(std::move(vec)._vec()));
				mat.reshape(dim, dim);
				doTrans(mat);
				return Matrix<number>(std::move(mat));
			}
		}

		template <typename number>
		SqDenseMat<number>::SqDenseMat(Vector<number>&& v) :SqDenseMat(matFromVec(std::move(v)))
		{
		}

		template <typename number>
		SqDenseMat<number>::SqDenseMat(const Vector<number>& v) : SqDenseMat(matFromVec(v))
		{
		}

		template <typename number>
		SqDenseMat<number>::SqDenseMat(const SqSparseMat<number>& m) : mat_(Matrix<number>(arma::Mat<number>(m.mat().mat())))
		{
		}

		template <typename number>
		template <typename T2, typename T>
		SqDenseMat<number>::SqDenseMat(const SqDenseMat<T>& other) :mat_(other.mat())
		{
		}

		template <typename number>
		SqDenseMat<number>& SqDenseMat<number>::operator=(const Matrix<number>& mat)
		{
			mat_ = mat;
			return *this;
		}

		template <typename number>
		SqDenseMat<number>& SqDenseMat<number>::operator=(Matrix<number>&& mat)
		{
			mat_ = std::move(mat);
			return *this;
		}

		/// DATA ACCESS
		template <typename number>
		Matrix<number>& SqDenseMat<number>::mat()
		{
			return mat_;
		}

		template <typename number>
		const Matrix<number>& SqDenseMat<number>::mat() const
		{
			return mat_;
		}

		/// MATH OPERATIONS
		template <typename number>
		void SqDenseMat<number>::multiplyInPlace(Vector<complex>& vector) const
		{
			arma::cx_vec intermediate = mat_._mat()*vector._vec(); // this step is sadly necessary to avoid copy-assignment below
			vector._vec() = intermediate;
		}

		template <typename number>
		template <typename number1, typename>
		SqDenseMat<number>& SqDenseMat<number>::operator/=(number1 t)
		{
			mat_ /= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqDenseMat<number>& SqDenseMat<number>::operator*=(number1 t)
		{
			mat_ *= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqDenseMat<number>& SqDenseMat<number>::operator+=(number1 t)
		{
			mat_._mat().diag(0) += t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqDenseMat<number>& SqDenseMat<number>::operator-=(number1 t)
		{
			mat_._mat().diag(0) -= t;
			return *this;
		}

		template<typename number>
		template <typename number1, typename>
		SqDenseMat<number>& SqDenseMat<number>::operator+=(const SqDenseMat<number1>& other) {
			mat() = mat() + other.mat();
			return *this;
		}

		template<typename number>
		template <typename number1, typename>
		SqDenseMat<number>& SqDenseMat<number>::operator-=(const SqDenseMat<number1>& other) {
			mat() = mat() - other.mat();
			return *this;
		}

		template<typename number>
		SqDenseMat<number> SqDenseMat<number>::operator-() const {
			return SqDenseMat<number>(Matrix<number>(-mat_));
		}

		template <typename number>
		SqDenseMat<number>& SqDenseMat<number>::operator+=(const SqDiagMat<number>& other)
		{
			mat_._mat() += arma::diagmat(other.diag()._vec());
			return *this;
		}

		template <typename number>
		number SqDenseMat<number>::at(count_t nRow, count_t nCol) const
		{
			return mat_.at(nRow, nCol);
		}

		/// MISC
		template <typename number>
		count_t SqDenseMat<number>::dim() const
		{
			return mat_.n_cols();
		}

		template <typename number>
		bool SqDenseMat<number>::isHermitian() const
		{
			return arma::as_scalar((arma::abs(mat_._mat() - mat_._mat().t())).max()) < 1e-14;
		}

		template <typename number>
		bool SqDenseMat<number>::hasNan() const
		{
			return mat_.hasNan();
		}
	}
}

namespace qengine
{
	namespace internal
	{
		/// Free functions
		std::ostream& operator<<(std::ostream& stream, const SqDenseMat<real>& matrix) {
			stream << matrix.mat();
			return stream;
		}
		std::ostream& operator<<(std::ostream& stream, const SqDenseMat<complex>& matrix) {
			stream << matrix.mat();
			return stream;
		}

		template <typename number1, typename number2>
		SqDenseMat<NumberMax_t<number1, number2>> operator+(const SqDenseMat<number1>& left, const SqDiagMat<number2>& right)
		{
			auto retval = convToNumber<NumberMax_t<number1, number2>>(left);
			retval += convToNumber<NumberMax_t<number1, number2>>(right);
			return retval;
		}

		template<typename number1, typename number2>
		SqDenseMat<NumberMax_t<number1, number2>> operator* (const SqDenseMat<number1>& left, const SqDenseMat<number2>& right) {
			return SqDenseMat<NumberMax_t<number1, number2>>(left.mat()*right.mat());
		}

		template <typename number1, typename number2>
		SqDenseMat<NumberMax_t<number1, number2>> kron(const SqDenseMat<number1>& left, const SqDenseMat<number2>& right)
		{
			return SqDenseMat<NumberMax_t<number1, number2>>(kron(left.mat(), right.mat()));
		}

		template <typename number>
		SqDenseMat<number> exp(const SqDenseMat<number>& d)
		{
			return SqDenseMat<number>{Matrix<number>(arma::expmat(d.mat()._mat()))};
		}

		template <typename number>
		SqDenseMat<number> pow(const SqDenseMat<number>& d, const count_t exponent)
		{
			auto A = d.mat()._mat();
			for (auto i = 1u; i < exponent; ++i)
			{
				A *= d.mat()._mat();
			}

			return SqDenseMat<number>(Matrix<number>(A));
		}

		void eig(const SqDenseMat<real>& mat, Vector<complex>& vals, Matrix<complex>& vecs, count_t)
		{
			arma::eig_gen(vals._vec(), vecs._mat(), mat.mat()._mat());
		}

		void eig(const SqDenseMat<complex>& mat, Vector<complex>& vals, Matrix<complex>& vecs, count_t)
		{
			arma::eig_gen(vals._vec(), vecs._mat(), mat.mat()._mat());
		}

		template <typename MatNumber>
		Spectrum<complex> getSpectrum(const SqDenseMat<MatNumber>& matrix, count_t largestEigenstate, const real normalization)
		{
			auto eigenvals = Vector<complex>(largestEigenstate + 1);
			auto eigenvecs = Matrix<complex>(matrix.dim(), largestEigenstate + 1);

			internal::eig(matrix, eigenvals, eigenvecs, largestEigenstate);

			return Spectrum<complex>(std::move(eigenvals), std::move(eigenvecs), normalization);
		}

		void eigHerm(const SqDenseMat<real>& mat, Vector<real>& vals, Matrix<real>& vecs, count_t largestEigenstate)
		{
			qengine_assert(mat.isHermitian(), "eigHerm(denseMat): input \"mat\" is not hermitian!");
			qengine_assert(static_cast<void*> (&vals) != static_cast<void*>(&vecs), "eigHerm(denseMat): \"vecs\" is an alias of \"vals\"!");

			const auto nEigenstates = narrow<int>( largestEigenstate + 1 );

			//arma::eig_sym(vals, vecs, mat.readRep(), "dc");
			auto matrix = mat.mat();
			const auto size = narrow<int>( mat.dim() );

			vals._vec() = arma::zeros(nEigenstates);
			vecs._mat() = arma::zeros(size, nEigenstates);


			auto m = 0;
			// Varying the size of the following 'work'-arrays may improve speed
			arma::vec work(8 * narrow<count_t>( size ));
			std::vector<int> iwork(5 * narrow<count_t>( size ));
			std::vector<int> ifail(narrow<count_t>( size ));
			LAPACKE_dsyevx_work(LAPACK_COL_MAJOR, 'V', 'I', 'U', size, matrix._mat().memptr(), size, 0, 0, 1, nEigenstates, 0.0, &m, vals._vec().memptr(), vecs._mat().memptr(), size, work.memptr(), 8 * size, iwork.data(), ifail.data());


		}

		void eigHerm(const SqDenseMat<complex>& mat, Vector<real>& vals, Matrix<complex>& vecs, count_t largestEigenstate)
		{
			qengine_assert(mat.isHermitian(), "eigHerm(denseMat): input \"mat\" is not hermitian!");
			qengine_assert(static_cast<void*> (&vals) != static_cast<void*>(&vecs), "eigHerm(denseMat): \"vecs\" is an alias of \"vals\"!");

			const auto nEigenstates = narrow<int>( largestEigenstate + 1 );

			//arma::eig_sym(vals, vecs, mat.readRep(), "dc");
			auto matrix = mat.mat();
			const auto size =  mat.dim();

			vals._vec() = arma::zeros(nEigenstates);
			vecs._mat() = arma::conv_to<arma::cx_mat>::from(arma::zeros(size, nEigenstates));
			auto m = 0;
			// Varying the size of the following 'work'-arrays may improve speed
			const auto worksize = 8 * size ;
			arma::cx_vec cwork(worksize);
			arma::vec rwork(worksize);
			std::vector<int> iwork(5 * size);
			std::vector<int> ifail(size);
			LAPACKE_zheevx_work(
				LAPACK_COL_MAJOR,
				'V',
				'I',
				'U',
				narrow<int>(size),
				reinterpret_cast<MKL_Complex16*>(matrix._mat().memptr()),
				narrow<int>(size),
				0,
				0,
				1,
				nEigenstates,
				0.0,
				&m,
				vals._vec().memptr(),
				reinterpret_cast<MKL_Complex16*>(vecs._mat().memptr()),
				narrow<int>(size),
				reinterpret_cast<MKL_Complex16*>(cwork.memptr()),
				narrow<int>(worksize),
				rwork.memptr(),
				iwork.data(),
				ifail.data());


			//arma::eig_sym(vals, vecs, mat.readRep(), "dc");
		}

		template <typename MatNumber>
		Spectrum<real> getSpectrum_herm(const SqDenseMat<MatNumber>& matrix, count_t largestEigenstate, const real normalization)
		{
			auto eigenvals = Vector<real>(largestEigenstate + 1);
			auto eigenvecs = Matrix<MatNumber>(matrix.dim(), largestEigenstate + 1);

			internal::eigHerm(matrix, eigenvals, eigenvecs, largestEigenstate);

			return Spectrum<real>(std::move(eigenvals), std::move(eigenvecs), normalization);
		}

	}
}

// explicit instantiations of SqDenseMat and related free operators
namespace qengine
{
	namespace internal
	{
		template class SqDenseMat<real>;
		template class SqDenseMat<complex>;

		template SqDenseMat<complex>::SqDenseMat(const SqDenseMat<real>&);

		template SqDenseMat<real>&    SqDenseMat<real>::operator/= (real);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator/= (real);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator/= (complex);

		template SqDenseMat<real>&    SqDenseMat<real>::operator*= (real);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator*= (real);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator*= (complex);

		template SqDenseMat<real>&    SqDenseMat<real>::operator+= (real);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator+= (real);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator+= (complex);

		template SqDenseMat<real>&    SqDenseMat<real>::operator-= (real);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator-= (real);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator-= (complex);

		template SqDenseMat<real>&    SqDenseMat<real>::operator+= (const SqDenseMat<real>&);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator+= (const SqDenseMat<real>&);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator+= (const SqDenseMat<complex>&);

		template SqDenseMat<real>&    SqDenseMat<real>::operator-= (const SqDenseMat<real>&);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator-= (const SqDenseMat<real>&);
		template SqDenseMat<complex>& SqDenseMat<complex>::operator-= (const SqDenseMat<complex>&);

		template SqDenseMat<real>    operator* (const SqDenseMat<real>&, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator* (const SqDenseMat<complex>&, const SqDenseMat<real>&);
		template SqDenseMat<complex> operator* (const SqDenseMat<real>&, const SqDenseMat<complex>&);
		template SqDenseMat<complex> operator* (const SqDenseMat<complex>&, const SqDenseMat<complex>&);

		template SqDenseMat<real>    kron(const SqDenseMat<real>&, const SqDenseMat<real>&);
		template SqDenseMat<complex> kron(const SqDenseMat<complex>&, const SqDenseMat<real>&);
		template SqDenseMat<complex> kron(const SqDenseMat<real>&, const SqDenseMat<complex>&);
		template SqDenseMat<complex> kron(const SqDenseMat<complex>&, const SqDenseMat<complex>&);

		template SqDenseMat<real>    operator+ (const SqDenseMat<real>&, const SqDiagMat<real>&);
		template SqDenseMat<complex> operator+ (const SqDenseMat<complex>&, const SqDiagMat<real>&);
		template SqDenseMat<complex> operator+ (const SqDenseMat<real>&, const SqDiagMat<complex>&);
		template SqDenseMat<complex> operator+ (const SqDenseMat<complex>&, const SqDiagMat<complex>&);

		template SqDenseMat<real> exp(const SqDenseMat<real>&);
		template SqDenseMat<complex> exp(const SqDenseMat<complex>&);

		template SqDenseMat<real> pow(const SqDenseMat<real>&, count_t);
		template SqDenseMat<complex> pow(const SqDenseMat<complex>&, count_t);


		template Spectrum<complex> getSpectrum(const SqDenseMat<real>& matrix, count_t largestEigenstate, const real normalization);
		template Spectrum<complex> getSpectrum(const SqDenseMat<complex>& matrix, count_t largestEigenstate, const real normalization);

		template Spectrum<real> getSpectrum_herm(const SqDenseMat<real>& matrix, count_t largestEigenstate, const real normalization);
		template Spectrum<real> getSpectrum_herm(const SqDenseMat<complex>& matrix, count_t largestEigenstate, const real normalization);

	}
}

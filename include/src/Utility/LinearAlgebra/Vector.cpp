﻿/* COPYRIGHT
 *
 * file="Vector.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/LinearAlgebra/Vector.h"

#include <armadillo>

#include "src/Utility/qengine_assert.h"

#include "src/DataContainer/SerializedObject.h"

namespace qengine
{
	/// MEMBERS

	template <typename number>
	Vector<number>::Vector() :vec_(std::make_unique<arma::Col<number>>())
	{
	}

	template <typename number>
	Vector<number>::Vector(count_t size) : vec_(std::make_unique<arma::Col<number>>(size))
	{
	}

	template <typename number>
	Vector<number>::Vector(count_t size, number fill) : vec_(std::make_unique<arma::Col<number>>(fill*arma::Col<number>(size, arma::fill::ones)))
	{
	}

	template <typename number>
	Vector<number>::Vector(count_t size, number* data) : vec_(std::make_unique<arma::Col<number>>(data, size))
	{
	}

	template <typename number>
	Vector<number>::Vector(const arma::Col<number>& data) : vec_(std::make_unique<arma::Col<number>>(data))
	{
	}

	template <typename number>
	Vector<number>::Vector(arma::Col<number>&& data) : vec_(std::make_unique<arma::Col<number>>(std::move(data)))
	{
	}

	template <typename number>
	Vector<number>::Vector(std::initializer_list<number> data) : vec_(std::make_unique<arma::Col<number>>(data))
	{
	}

	template <typename number>
	Vector<number>::Vector(std::vector<number> data) : vec_(std::make_unique<arma::Col<number>>(data))
	{
	}

	template <typename number>
	template <typename T2, typename T, typename>
	Vector<number>::Vector(const Vector<T>& other) :vec_(std::make_unique<arma::cx_vec>(arma::conv_to<arma::cx_vec>::from(other._vec())))
	{
	}

	template <typename number>
	template <typename T2, typename T>
	Vector<number>::Vector(const Vector<real>& re, const Vector<real>& im) :vec_(std::make_unique<arma::cx_vec>(re._vec(), im._vec()))
	{
	}


	template <typename number>
	count_t Vector<number>::size() const
	{
		return vec_->n_elem;
	}

	template <typename number>
	count_t Vector<number>::dim() const
	{
		return size();
	}

	template <typename number>
	real Vector<number>::norm() const
	{
		return arma::norm(*vec_);
	}

	template<>
	RVec Vector<real>::absSquare() const
	{
		return RVec((*vec_) % (*vec_));
	}

	template<>
	RVec Vector<complex>::absSquare() const
	{
		return RVec(arma::real(arma::conj(*vec_) % (*vec_)));
	}

	template <typename number>
	Vector<number>& Vector<number>::append(const Vector<number>& tail)
	{
		*vec_ = arma::join_cols(*vec_, *tail.vec_);
		return *this;
	}

	template <typename number>
	Vector<number>& Vector<number>::glue(const Vector<number>& tail)
	{
		qengine_assert(back() == tail.front(), "end of head must equal front of tail!");
		*vec_ = arma::join_cols(*vec_, tail.vec_->rows(1, tail.size() - 1));
		return *this;
	}

	template <typename number>
	bool Vector<number>::hasNan() const
	{
		return vec_->has_nan();
	}

	template <typename number>
	void Vector<number>::resize(count_t newSize)
	{
		vec_->resize(newSize);
	}

	template <typename number>
	number* Vector<number>::_data()
	{
		return vec_->memptr();
	}

	template <typename number>
	const number* Vector<number>::_data() const
	{
		return vec_->memptr();
	}

	template <typename number>
	number& Vector<number>::at(count_t index)
	{
		return vec_->at(index);
	}

	template <typename number>
	const number& Vector<number>::at(count_t index) const
	{
		return vec_->at(index);
	}

	template <typename number>
	arma::Col<number>& Vector<number>::_vec()&
	{
		return *vec_;
	}

	template <typename number>
	const arma::Col<number>& Vector<number>::_vec() const&
	{
		return *vec_;
	}

	template <typename number>
	arma::Col<number>&& Vector<number>::_vec() &&
	{
		return std::move(*vec_);
	}

	template <typename number>
	number& Vector<number>::front()
	{
		return vec_->at(0);
	}

	template <typename number>
	const number& Vector<number>::front() const
	{
		return vec_->at(0);
	}

	template <typename number>
	number& Vector<number>::back()
	{
		return vec_->at(size() - 1);
	}

	template <typename number>
	const number& Vector<number>::back() const
	{
		return vec_->at(size() - 1);
	}

	template <typename number>
	arma::Col<number>& Vector<number>::_rep()&
	{
		return _vec();
	}

	template <typename number>
	const arma::Col<number>& Vector<number>::_rep() const&
	{
		return _vec();
	}

	template <typename number>
	arma::Col<number>&& Vector<number>::_rep() &&
	{
		return std::move(_vec());
	}


	template<> // specialization for real vectors
	Vector<real> Vector<real>::re() const {
		return *this;
	}

	template<>
	Vector<real> Vector<real>::im() const {
		return Vector<real>(size(), 0.0);
	}

	template<> // specialization for complex vector
	Vector<real> Vector<complex>::re() const {
		return Vector<real>(arma::real(_vec()));
	}

	template<>
	Vector<real> Vector<complex>::im() const {
		return Vector<real>(arma::imag(_vec()));
	}

	template <typename number>
	Vector<number>::Vector(const Vector& other) : vec_{ std::make_unique<arma::Col<number>>(*other.vec_) }
	{
	}

	template <typename number>
	Vector<number>::Vector(Vector&& other) noexcept : vec_{ std::move(other.vec_) }
	{
	}

	template <typename number>
	Vector<number>::~Vector() = default;

	template <typename number>
	Vector<number>& Vector<number>::operator=(const Vector& other) &
	{
		if (this == &other)
			return *this;
		*vec_ = *other.vec_;
		return *this;
	}

	template <typename number>
	Vector<number>& Vector<number>::operator=(Vector&& other) & noexcept
	{
		if (this == &other)
			return *this;
		vec_ = std::move(other.vec_);
		return *this;
	}

	template <typename number>
	template <typename number1, typename>
	Vector<number>& Vector<number>::operator/=(number1 t)
	{
		*vec_ /= t;
		return *this;
	}

	template <typename number>
	template <typename number1, typename>
	Vector<number>& Vector<number>::operator*=(number1 t)
	{
		*vec_ *= t;
		return *this;
	}

	template <typename number>
	template <typename number1, typename>
	Vector<number>& Vector<number>::operator+=(number1 t)
	{
		*vec_ += t;
		return *this;
	}

	template <typename number>
	template <typename number1, typename>
	Vector<number>& Vector<number>::operator-=(number1 t)
	{
		*vec_ -= t;
		return *this;
	}

	template <typename number>
	Vector<number>& Vector<number>::operator+=(const Vector<number>& t)
	{
		*vec_ += t._vec();
		return *this;
	}

	template <typename number>
	Vector<number>& Vector<number>::operator-=(const Vector<number>& t)
	{
		*vec_ -= t._vec();
		return *this;
	}

	template <typename number>
	Vector<number> Vector<number>::operator-() const
	{
		return Vector(-*vec_);
	}

	Vector<real> makeVector(const arma::Col<real>& vec)
	{
		return Vector<real>(vec);
	}

	Vector<real> makeVector(arma::Col<real>&& vec)
	{
		return Vector<real>(std::move(vec));
	}

	Vector<complex> makeVector(const arma::Col<complex>& vec)
	{
		return Vector<complex>(vec);
	}

	Vector<complex> makeVector(arma::Col<complex>&& vec)
	{
		return Vector<complex>(std::move(vec));
	}

	Vector<complex> makeCVec(const Vector<real>& re, const Vector<real>& im)
	{
		return makeVector(arma::cx_vec{ re._vec(), im._vec() });
	}
}


namespace qengine
{
	template<>
	SerializedObject toSerializedObject(const Vector<real>& vec)
	{
		SerializedObject obj;
		obj._set(RMat(vec));
		return obj;
	}

	template<>
	Vector<real> fromSerializedObject(const SerializedObject& obj, internal::Type<Vector<real>>)
	{
		qengine_assert(obj._type() == SerializedObject::REAL && obj._format() == SerializedObject::VECTOR, "type of serializedObject must be rvec");
		return RVec(obj._rmat().col(0));
	}

	template<>
	SerializedObject toSerializedObject(const Vector<complex>& vec)
	{
		SerializedObject obj;
		obj._set(CMat(vec));
		return obj;
	}

	template<>
	Vector<complex> fromSerializedObject(const SerializedObject& obj, internal::Type<Vector<complex>>)
	{
		qengine_assert((obj._type() == SerializedObject::COMPLEX || obj._type() == SerializedObject::REAL) && obj._format() == SerializedObject::VECTOR, "type of serializedObject must be cvec or rvec");
		
		return obj._type() == SerializedObject::COMPLEX ? CVec(obj._cmat().col(0)) : CVec(RVec(obj._rmat().col(0)));
	}

}

/// operations on vectors
namespace qengine
{
	std::ostream& operator<<(std::ostream& stream, const Vector<real>& vector)
	{
		stream << vector._vec() << std::endl;
		return stream;
	}

	std::ostream& operator<<(std::ostream& stream, const Vector<complex>& vector)
	{
		stream << vector._vec() << std::endl;
		return stream;
	}

	namespace
	{
		template <typename number>
		bool equalsImplementation(const Vector<number>& left, const Vector<number>& right)
		{
			if (left.size() != right.size()) return false;

			for (auto i = 0u; i < left.size(); ++i)
			{
				if (left.at(i) != right.at(i)) return false;
			}
			return true;
		}

		bool equalsImplementation(const RVec& left, const CVec& right)
		{
			if (left.size() != right.size()) return false;

			for (auto i = 0u; i < left.size(); ++i)
			{
				if (left.at(i) != right.at(i).real()) return false;
				if (right.at(i).imag() != 0.0) return false;
			}
			return true;
		}

		bool equalsImplementation(const CVec& left, const RVec& right)
		{
			return equalsImplementation(right, left);
		}
	}
	template <typename number1, typename number2>
	bool operator==(const Vector<number1>& left, const Vector<number2>& right)
	{
		/// implementation is moved to other function to overload functionality on numberType
		return equalsImplementation(left, right);
	}

	template <typename number1, typename number2>
	bool operator!=(const Vector<number1>& left, const Vector<number2>& right)
	{
		return !(left == right);
	}

	template<typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator+ (const Vector<number1>& left, const Vector<number2>& right)
	{
		return makeVector(left._vec() + right._vec());
	}

	template <typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator-(const Vector<number1>& left, const Vector<number2>& right)
	{
		return Vector<internal::NumberMax_t<number1, number2>>{left._vec() - right._vec()};
	}


	template <typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator*(const Vector<number1>& left, const Vector<number2>& right)
	{
		return Vector<internal::NumberMax_t<number1, number2>>(left._vec() % right._vec());
	}

	template <typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator/(const Vector<number1>& left, const Vector<number2>& right)
	{
		return Vector<internal::NumberMax_t<number1, number2>>(left._vec() / right._vec());
	}
	namespace internal
	{
		template <typename number1, typename number2>
		Vector<internal::NumberMax_t<number1, number2>> add(const Vector<number1>& left,
			number2 right)
		{
			return Vector<internal::NumberMax_t<number1, number2>>{left._vec() + right};
		}

		template <typename number1, typename number2>
		Vector<internal::NumberMax_t<number1, number2>> multiply(number2 left, const Vector<number1>& right)
		{
			return Vector<internal::NumberMax_t<number1, number2>>{left*right._vec()};
		}

		template <typename number>
		Vector<number> invert(const Vector<number>& right)
		{
			return Vector<number>(arma::pow(right._vec(), -1));
		}
	}

	template <typename number1, typename number2, typename>
	Vector<internal::NumberMax_t<number1, number2>> operator/(const Vector<number2>& left, number1 right)
	{
		return Vector<internal::NumberMax_t<number1, number2>>{left._vec() / right};
	}


	template <typename number1, typename number2, typename>
	Vector<internal::NumberMax_t<number1, number2>> operator/(number1 left, const Vector<number2>& right)
	{
		return Vector<internal::NumberMax_t<number1, number2>>(left*arma::ones(right._vec().size()) / right._vec());
	}

	template <typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> kron(const Vector<number1>& left, const Vector<number2>& right)
	{
		return Vector<internal::NumberMax_t<number1, number2>>{arma::kron(left._vec(), right._vec())};
	}

	template <typename number1, typename number2>
	internal::NumberMax_t<number1, number2> dot(const Vector<number1>& left, const Vector<number2>& right)
	{
		return arma::dot(left._vec(), right._vec());
	}

	template <typename number2>
	complex cdot(const Vector<complex>& left, const Vector<number2>& right)
	{

		// todo: change this to work with real vectors and any combinations, when we have if constexpr available.
		return arma::cdot(left._vec(), arma::conv_to<arma::cx_vec>::from(right._vec()));
	}

	template <typename number>
	number normDot(const Vector<number>& left, const Vector<number>& right)
	{
		return arma::norm_dot(left._vec(), right._vec());
	}

	template <typename number>
	number sum(const Vector<number>& vector)
	{
		return arma::sum(vector._vec());
	}

	template <typename number>
	number trapz(const Vector<number>& vector)
	{
		return arma::as_scalar(arma::trapz(vector._vec()));
	}

	template <typename number>
	Vector<number> append(Vector<number> head, const Vector<number>& tail)
	{
		return head.append(tail);
	}

	template <typename number>
	Vector<number> glue(Vector<number> head, const Vector<number>& tail)
	{
		return head.glue(tail);
	}

	template <typename number>
	Vector<number> invert(const Vector<number>& vec)
	{
		return Vector<number>(arma::flipud(vec._vec()));
	}

	template <typename number>
	bool approxEqual(const Vector<number>& left, const Vector<number>& right, real absTol)
	{
		return arma::approx_equal(left._vec(), right._vec(), "absDiff", absTol);
	}

	template <typename number>
	number mean(const Vector<number>& v)
	{
		return arma::mean(v._vec());
	}

	template <typename number>
	number max(const Vector<number>& v)
	{
		return arma::max(v._vec());
	}

	template <typename number>
	number min(const Vector<number>& v)
	{
		return arma::min(v._vec());
	}

	template <>
	Vector<real> conj(const Vector<real>& vec)
	{
		return vec;
	}

	template <>
	Vector<complex> conj(const Vector<complex>& vec)
	{
		return Vector<complex>(arma::conj(vec._vec()));
	}
}

namespace qengine
{
	template <typename number>
	Vector<number> exp(const Vector<number>& vector)
	{
		return Vector<number>(arma::exp(vector._vec()));
	}

	template <typename number>
	Vector<number> pow(const Vector<number>& vector, count_t exponent)
	{
		return Vector<number>(arma::pow(vector._vec(), static_cast<double>(exponent)));
	}

	template <typename number>
	Vector<number> exp2(const Vector<number>& vector)
	{
		return Vector<number>(arma::exp2(vector._vec()));
	}

	template <typename number>
	Vector<number> exp10(const Vector<number>& vector)
	{
		return Vector<number>(arma::exp10(vector._vec()));
	}

	template <typename number>
	Vector<number> log(const Vector<number>& vector)
	{
		return Vector<number>(arma::log(vector._vec()));
	}

	template <typename number>
	Vector<number> log2(const Vector<number>& vector)
	{
		return Vector<number>(arma::log2(vector._vec()));
	}

	template <typename number>
	Vector<number> log10(const Vector<number>& vector)
	{
		return Vector<number>(arma::log10(vector._vec()));
	}

	template <typename number>
	Vector<number> sqrt(const Vector<number>& vector)
	{
		return Vector<number>(arma::sqrt(vector._vec()));
	}

	template <typename number>
	Vector<real> abs(const Vector<number>& vector)
	{
		return Vector<real>(arma::abs(vector._vec()));
	}

	Vector<real> re(const Vector<complex>& vector)
	{
		return vector.re();
	}

	Vector<real> im(const Vector<complex>& vector)
	{
		return vector.im();
	}

	template <typename number>
	Vector<number> sin(const Vector<number>& vector)
	{
		return Vector<number>(arma::sin(vector._vec()));
	}

	template <typename number>
	Vector<number> cos(const Vector<number>& vector)
	{
		return Vector<number>(arma::cos(vector._vec()));
	}

	template <typename number>
	Vector<number> tan(const Vector<number>& vector)
	{
		return Vector<number>(arma::tan(vector._vec()));
	}

	template <typename number>
	Vector<number> asin(const Vector<number>& vector)
	{
		return Vector<number>(arma::asin(vector._vec()));
	}

	template <typename number>
	Vector<number> acos(const Vector<number>& vector)
	{
		return Vector<number>(arma::acos(vector._vec()));
	}

	template <typename number>
	Vector<number> atan(const Vector<number>& vector)
	{
		return Vector<number>(arma::atan(vector._vec()));
	}

	template <typename number>
	Vector<number> sinh(const Vector<number>& vector)
	{
		return Vector<number>(arma::sinh(vector._vec()));
	}

	template <typename number>
	Vector<number> cosh(const Vector<number>& vector)
	{
		return Vector<number>(arma::cosh(vector._vec()));
	}

	template <typename number>
	Vector<number> tanh(const Vector<number>& vector)
	{
		return Vector<number>(arma::tanh(vector._vec()));
	}

	template <typename number>
	Vector<number> asinh(const Vector<number>& vector)
	{
		return Vector<number>(arma::asinh(vector._vec()));
	}

	template <typename number>
	Vector<number> acosh(const Vector<number>& vector)
	{
		return Vector<number>(arma::acosh(vector._vec()));
	}

	template <typename number>
	Vector<number> atanh(const Vector<number>& vector)
	{
		return Vector<number>(arma::atanh(vector._vec()));
	}
}

/// special construction functions
namespace qengine
{
	RVec linspace(real lower, real upper, count_t numberOfPoints)
	{
		return RVec(arma::linspace(lower, upper, numberOfPoints));
	}

	RVec step(const RVec& x, real stepPos)
	{
		arma::vec retval(x.size(), arma::fill::zeros);
		retval(arma::find(x._vec() > stepPos)).ones();
		return RVec(std::move(retval));
	}

	RVec box(const RVec& x, real left, real right)
	{
		return step(x, left) - step(x, right);
	}

	RVec well(const RVec& x, real left, real right)
	{
		return box(x, left, right) * (-1.0);
	}

	RVec sstep(const RVec& x, real center, real hardness)
	{
		return 1.0 / (1.0 + exp(-hardness * (x - center)));
	}

	RVec sbox(const RVec& x, real left, real right, real hardness)
	{
		return sstep(x, left, hardness) - sstep(x, right, hardness);
	}

	RVec swell(const RVec& x, real left, real right, real hardness)
	{
		return sbox(x, left, right, hardness) * (-1.0);
	}

}

namespace qengine
{
	template class Vector<real>;
	template class Vector<complex>;

	template Vector<complex>::Vector(const Vector<real>&);
	template Vector<complex>::Vector(const Vector<real>&, const Vector<real>&);

	template Vector<real>& Vector<real>::operator*= (real);
	template Vector<complex>& Vector<complex>::operator*= (real);
	template Vector<complex>& Vector<complex>::operator*= (complex);

	template Vector<real>& Vector<real>::operator/= (real);
	template Vector<complex>& Vector<complex>::operator/= (real);
	template Vector<complex>& Vector<complex>::operator/= (complex);

	template Vector<real>& Vector<real>::operator+= (real);
	template Vector<complex>& Vector<complex>::operator+= (real);
	template Vector<complex>& Vector<complex>::operator+= (complex);

	template Vector<real>& Vector<real>::operator-= (real);
	template Vector<complex>& Vector<complex>::operator-= (real);
	template Vector<complex>& Vector<complex>::operator-= (complex);

	template bool operator== (const Vector<real>&    left, const Vector<real>&    right);
	template bool operator== (const Vector<real>&    left, const Vector<complex>& right);
	template bool operator== (const Vector<complex>& left, const Vector<real>&    right);
	template bool operator== (const Vector<complex>& left, const Vector<complex>& right);

	template bool operator!= (const Vector<real>&    left, const Vector<real>&    right);
	template bool operator!= (const Vector<real>&    left, const Vector<complex>& right);
	template bool operator!= (const Vector<complex>& left, const Vector<real>&    right);
	template bool operator!= (const Vector<complex>& left, const Vector<complex>& right);

	template Vector<real>    operator+ (const Vector<real>&    left, const Vector<real>&    right);
	template Vector<complex> operator+ (const Vector<real>&    left, const Vector<complex>& right);
	template Vector<complex> operator+ (const Vector<complex>& left, const Vector<real>&    right);
	template Vector<complex> operator+ (const Vector<complex>& left, const Vector<complex>& right);

	template Vector<real>    operator- (const Vector<real>&    left, const Vector<real>&    right);
	template Vector<complex> operator- (const Vector<real>&    left, const Vector<complex>& right);
	template Vector<complex> operator- (const Vector<complex>& left, const Vector<real>&    right);
	template Vector<complex> operator- (const Vector<complex>& left, const Vector<complex>& right);

	namespace internal
	{
		template Vector<real>    add(const Vector<real>&    left, real    right);
		template Vector<complex> add(const Vector<real>&    left, complex right);
		template Vector<complex> add(const Vector<complex>& left, real    right);
		template Vector<complex> add(const Vector<complex>& left, complex right);

		template Vector<real>    multiply(real, const Vector<real>&);
		template Vector<complex> multiply(real, const Vector<complex>&);
		template Vector<complex> multiply(complex, const Vector<real>&);
		template Vector<complex> multiply(complex, const Vector<complex>&);

		template Vector<real> invert(const Vector<real>&);
		template Vector<complex> invert(const Vector<complex>&);
	}

	template Vector<real>    operator* (const Vector<real>&    left, const Vector<real>&    right);
	template Vector<complex> operator* (const Vector<real>&    left, const Vector<complex>& right);
	template Vector<complex> operator* (const Vector<complex>& left, const Vector<real>&    right);
	template Vector<complex> operator* (const Vector<complex>& left, const Vector<complex>& right);

	template Vector<real>    operator/ (const Vector<real>&    left, const Vector<real>&    right);
	template Vector<complex> operator/ (const Vector<real>&    left, const Vector<complex>& right);
	template Vector<complex> operator/ (const Vector<complex>& left, const Vector<real>&    right);
	template Vector<complex> operator/ (const Vector<complex>& left, const Vector<complex>& right);

	template Vector<real>    kron(const Vector<real>&    left, const Vector<real>&    right);
	template Vector<complex> kron(const Vector<real>&    left, const Vector<complex>& right);
	template Vector<complex> kron(const Vector<complex>& left, const Vector<real>&    right);
	template Vector<complex> kron(const Vector<complex>& left, const Vector<complex>& right);

	template real    dot(const Vector<real>&    left, const Vector<real>&    right);
	template complex dot(const Vector<real>&    left, const Vector<complex>& right);
	template complex dot(const Vector<complex>& left, const Vector<real>&    right);
	template complex dot(const Vector<complex>& left, const Vector<complex>& right);

	template complex cdot(const Vector<complex>& left, const Vector<real>&    right);
	template complex cdot(const Vector<complex>& left, const Vector<complex>& right);

	template real    normDot(const Vector<real>&    left, const Vector<real>&    right);
	template complex normDot(const Vector<complex>& left, const Vector<complex>& right);

	template real    sum(const Vector<real>&    vector);
	template complex sum(const Vector<complex>& vector);

	template real    trapz(const Vector<real>&    vector);
	template complex trapz(const Vector<complex>& vector);

	template Vector<real>    append(Vector<real>, const Vector<real>&);
	template Vector<complex> append(Vector<complex>, const Vector<complex>&);

	template Vector<real>    glue(Vector<real>, const Vector<real>&);
	template Vector<complex> glue(Vector<complex>, const Vector<complex>&);

	template Vector<real>    invert(const Vector<real>&);
	template Vector<complex> invert(const Vector<complex>&);

	template bool approxEqual(const Vector<real>&, const Vector<real>&, real);
	template bool approxEqual(const Vector<complex>&, const Vector<complex>&, real);

	template real mean(const Vector<real>&);
	template complex mean(const Vector<complex>&);

	template real max(const Vector<real>&);
	template complex max(const Vector<complex>&);

	template real min(const Vector<real>&);
	template complex min(const Vector<complex>&);

	template Vector<real>    exp(const Vector<real>&    vector);
	template Vector<complex> exp(const Vector<complex>& vector);

	template Vector<real>    pow(const Vector<real>&    vector, count_t exponent);
	template Vector<complex> pow(const Vector<complex>& vector, count_t exponent);

	template Vector<real>    exp2(const Vector<real>&    vector);
	template Vector<complex> exp2(const Vector<complex>& vector);

	template Vector<real>    exp10(const Vector<real>&    vector);
	template Vector<complex> exp10(const Vector<complex>& vector);

	template Vector<real>    log(const Vector<real>&    vector);
	template Vector<complex> log(const Vector<complex>& vector);

	template Vector<real>    log2(const Vector<real>&    vector);
	template Vector<complex> log2(const Vector<complex>& vector);

	template Vector<real>    log10(const Vector<real>&    vector);
	template Vector<complex> log10(const Vector<complex>& vector);

	template Vector<real>    sqrt(const Vector<real>&    vector);
	template Vector<complex> sqrt(const Vector<complex>& vector);

	template Vector<real>    abs(const Vector<real>&    vector);
	template Vector<real>	 abs(const Vector<complex>& vector);

	template Vector<real>    sin(const Vector<real>&    vector);
	template Vector<complex> sin(const Vector<complex>& vector);

	template Vector<real>    cos(const Vector<real>&    vector);
	template Vector<complex> cos(const Vector<complex>& vector);

	template Vector<real>    tan(const Vector<real>&    vector);
	template Vector<complex> tan(const Vector<complex>& vector);

	template Vector<real>    asin(const Vector<real>&    vector);
	template Vector<complex> asin(const Vector<complex>& vector);

	template Vector<real>	 acos(const Vector<real>&    vector);
	template Vector<complex> acos(const Vector<complex>& vector);

	template Vector<real>    atan(const Vector<real>&    vector);
	template Vector<complex> atan(const Vector<complex>& vector);

	template Vector<real>    sinh(const Vector<real>&    vector);
	template Vector<complex> sinh(const Vector<complex>& vector);

	template Vector<real>    cosh(const Vector<real>&    vector);
	template Vector<complex> cosh(const Vector<complex>& vector);

	template Vector<real>    tanh(const Vector<real>&    vector);
	template Vector<complex> tanh(const Vector<complex>& vector);

	template Vector<real>    asinh(const Vector<real>&    vector);
	template Vector<complex> asinh(const Vector<complex>& vector);

	template Vector<real>    acosh(const Vector<real>&    vector);
	template Vector<complex> acosh(const Vector<complex>& vector);

	template Vector<real>    atanh(const Vector<real>&    vector);
	template Vector<complex> atanh(const Vector<complex>& vector);
}



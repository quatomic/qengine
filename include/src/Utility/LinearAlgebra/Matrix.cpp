﻿/* COPYRIGHT
 *
 * file="Matrix.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
//
// Created by Jesper Hasseriis Mohr Jensen on 23/03/2017.
//

#include "src/Utility/LinearAlgebra/Matrix.h"

#include <armadillo>
#include "src/Utility/qengine_assert.h"

#include "src/DataContainer/SerializedObject.h"

namespace qengine
{
	/// MEMBERS


	template <typename number>
	Matrix<number>::Matrix() : mat_(std::make_unique<arma::Mat<number>>())
	{
	}

	template<typename number>
	Matrix<number>::Matrix(count_t n_rows, count_t n_cols) : mat_(std::make_unique<arma::Mat<number>>(n_rows, n_cols))
	{
	}

	template<typename number>
	Matrix<number>::Matrix(count_t n_rows, count_t n_cols, number fill) : mat_(std::make_unique<arma::Mat<number>>(fill*arma::Mat<number>(n_rows, n_cols, arma::fill::ones)))
	{
	}

	template<typename number>
	Matrix<number>::Matrix(count_t n_rows, count_t n_cols, number* data) : mat_(std::make_unique<arma::Mat<number>>(data, n_rows, n_cols))
	{
	}

	template<typename number>
	Matrix<number>::Matrix(const arma::Mat<number>& data) : mat_(std::make_unique<arma::Mat<number>>(data))
	{
	}


	template<typename number>
	Matrix<number>::Matrix(arma::Mat<number>&& data) : mat_(std::make_unique<arma::Mat<number>>(std::move(data)))
	{
	}

	template <typename number>
	Matrix<number>::Matrix(std::initializer_list<std::initializer_list<number>> data) : mat_(std::make_unique<arma::Mat<number>>(data))
	{
	}

	namespace
	{
		template<class number>
		auto makeArmaMatrix(const std::vector <Vector<number>>& list)
		{
			qengine_assert(list.size() > 0, "");
			const auto nRows = list.size();
			const auto nCols = list.begin()->size();

			arma::Mat<number> m(nRows, nCols);

			auto listIt = list.begin();
			for (auto i = 0u; i < nRows; ++i)
			{
				qengine_assert(listIt->size() == nCols, "");

				m.row(i) = listIt->_vec().t();
				++listIt;
			}

			return m;
		}
	}
	template <typename number>
	Matrix<number>::Matrix(const std::vector<Vector<number>>& data) : mat_(std::make_unique<arma::Mat<number>>(makeArmaMatrix(data)))
	{
	}



	template<typename number>
	Matrix<number>::Matrix(std::vector<number> data) : mat_(std::make_unique<arma::Mat<number>>(data))
	{
	}

	template<typename number>
	Matrix<number>::Matrix(const Vector<number>& Vec) : mat_(std::make_unique<arma::Mat<number>>(Vec._vec()))
	{
	}

	template<typename number>
	Matrix<number>::Matrix(const Matrix &other) : mat_(std::make_unique<arma::Mat<number>>(*other.mat_))
	{
	}

	template<typename number>
	Matrix<number>::Matrix(Matrix&& other) noexcept : mat_(std::move(other.mat_)) {}

	template <typename number>
	template <typename T2, typename T>
	Matrix<number>::Matrix(const Matrix<T>& other) : mat_(std::make_unique<arma::cx_mat>(arma::conv_to<arma::cx_mat>::from(other._mat())))
	{
	}

	template<typename number>
	Matrix<number>::~Matrix() = default;

	/// ASSIGNMENT

	template<typename number>
	Matrix<number>& Matrix<number>::operator=(const Matrix &other) {
		if (this == &other) {
			return *this;
		}
		*mat_ = *other.mat_;
		return *this;
	}

	template <typename number>
	Matrix<number>& Matrix<number>::operator=(Matrix&& other) noexcept
	{
		if (this == &other) {
			return *this;
		}
		mat_ = std::move(other.mat_);
		return *this;
	}


	/// DATA ACCESS

	template<typename number>
	number* Matrix<number>::_data()
	{
		return mat_->memptr();
	}

	template<typename number>
	const number * Matrix<number>::_data() const
	{
		return mat_->memptr();
	}

	template<typename number>
	number& Matrix<number>::at(count_t row_index, count_t col_index) {
		return mat_->at(row_index, col_index);
	}

	template<typename number>
	const number& Matrix<number>::at(count_t row_index, count_t col_index) const {
		return mat_->at(row_index, col_index);
	}

	template <typename number>
	number& Matrix<number>::operator()(count_t row_index, count_t col_index)
	{
		return (*mat_)(row_index, col_index);
	}

	template <typename number>
	const number& Matrix<number>::operator()(count_t row_index, count_t col_index) const
	{
		return (*mat_)(row_index, col_index);
	}

	template<typename number>
	const number& Matrix<number>::at(count_t n) const {
		return mat_->at(n);
	}


	template<typename number>
	number& Matrix<number>::at(count_t n) {
		return mat_->at(n);
	}

	template<typename number>
	arma::Mat<number>& Matrix<number>::_mat()
	{
		return *mat_;
	}

	template<typename number>
	const arma::Mat<number>& Matrix<number>::_mat() const
	{
		return *mat_;
	}

	template <typename number>
	void Matrix<number>::_append(const number& val)
	{
		qengine_assert(n_cols() == 1, "");

		(void) arma::join_cols(*mat_, arma::Col<number>{val});
	}

	template<typename number>
	internal::SubviewCol<number> Matrix<number>::col(count_t j) 
	{
		return internal::SubviewCol<number>(*this, j);
	}

	template <typename number>
	const internal::SubviewCol<number> Matrix<number>::col(count_t j) const
	{
		return internal::SubviewCol<number>(const_cast<Matrix<number>&>(*this), j);
	}


	/*
	template<typename number>
	Vector<number> Matrix<number>::col(count_t j){
		Vector<number> v(n_rows());
		for(auto i=0u; i < v.size();++i){
		v.at(i) = this->at(i,j);
		}
		return v;
	}
	*/


	template<>  // template specialization for getting re,im for real matrix
	Matrix<real> Matrix<real>::re() const 
	{
		return *this;
	}

	template<>
	Matrix<real> Matrix<real>::im() const 
	{
		return Matrix<real>(n_rows(), n_cols(), 0.0);
	}

	template <typename number>
	Vector<number> Matrix<number>::vectorize() const
	{
		return Vector<number>(arma::vectorise(*mat_));
	}

	template<>  // template specialization for getting re,im for complex matrix
	Matrix<real> Matrix<complex>::re() const {
		return Matrix<real>(arma::real(_mat()));
	}

	template<>
	Matrix<real> Matrix<complex>::im() const {
		return Matrix<real>(arma::imag(_mat()));
	}

	/// MATH OPERATORS

	template <typename number>
	template <typename number1, typename>
	Matrix<number>& Matrix<number>::operator/=(number1 t)
	{
		*mat_ /= t;
		return *this;
	}

	template <typename number>
	template <typename number1, typename>
	Matrix<number>& Matrix<number>::operator*=(number1 t)
	{
		*mat_ *= t;
		return *this;
	}


	template<typename number>
	Matrix<number>& Matrix<number>::operator+=(const Matrix<number>& other) {
		_mat() += other._mat();
		return *this;
	}

	template<typename number>
	Matrix<number>& Matrix<number>::operator-=(const Matrix<number>& other) {
		_mat() -= other._mat();
		return *this;
	}

	template<typename number>
	Matrix<number> Matrix<number>::operator-() const {
		return Matrix<number>(-_mat());
	}

	/// MISC
	template<typename number>
	count_t Matrix<number>::n_rows() const {
		return mat_->n_rows;
	}

	template<typename number>
	count_t Matrix<number>::n_cols() const {
		return mat_->n_cols;
	}


	template<typename number>
	count_t Matrix<number>::n_elem() const {
		return mat_->n_elem;
	}

	template <typename number>
	bool Matrix<number>::hasNan() const
	{
		return mat_->has_nan();
	}
}


namespace qengine
{
	template<>
	SerializedObject toSerializedObject(const Matrix<real>& mat)
	{
		SerializedObject obj;
		obj._set(mat);
		return obj;
	}

	template<>
	Matrix<real> fromSerializedObject(const SerializedObject& obj, internal::Type<Matrix<real>>)
	{
		qengine_assert(obj._type() == SerializedObject::REAL && obj._format() == SerializedObject::MATRIX, "type of serializedObject must be rmat");
		return obj._rmat();
	}

	template<>
	SerializedObject toSerializedObject(const Matrix<complex>& mat)
	{
		SerializedObject obj;
		obj._set(mat);
		return obj;
	}

	template<>
	Matrix<complex> fromSerializedObject(const SerializedObject& obj, internal::Type<Matrix<complex>>)
	{
		qengine_assert(obj._type() == SerializedObject::COMPLEX && obj._format() == SerializedObject::MATRIX, "type of serializedObject must be cmat");
		return obj._cmat();
	}

}

namespace qengine
{
	std::ostream& operator<<(std::ostream& stream, const Matrix<real>& matrix) {
		stream << matrix._mat() << std::endl;
		return stream;
	}

	std::ostream& operator<<(std::ostream& stream, const Matrix<complex>& matrix) {
		stream << matrix._mat() << std::endl;
		return stream;
	}

	namespace
	{
		template <typename number>
		bool equalsImplementation(const Matrix<number>& left, const Matrix<number>& right)
		{
			if (left.n_rows() != right.n_rows()) return false;
			if (left.n_cols() != right.n_cols()) return false;

			for (auto i = 0u; i < left.n_elem(); ++i)
			{
				if (left.at(i) != right.at(i)) return false;
			}
			return true;
		}

		bool equalsImplementation(const RMat& left, const CMat& right)
		{
			if (left.n_rows() != right.n_rows()) return false;
			if (left.n_cols() != right.n_cols()) return false;

			for (auto i = 0u; i < left.n_elem(); ++i)
			{
				if (left.at(i) != right.at(i).real()) return false;
				if (right.at(i).imag() != 0.0) return false;
			}
			return true;
		}

		bool equalsImplementation(const CMat& left, const RMat& right)
		{
			return equalsImplementation(right, left);
		}
	}
	template <typename number1, typename number2>
	bool operator==(const Matrix<number1>& left, const Matrix<number2>& right)
	{
		/// implementation is moved to other function to overload functionality on numberType
		return equalsImplementation(left, right);
	}

	template <typename number1, typename number2>
	bool operator!=(const Matrix<number1>& left, const Matrix<number2>& right)
	{
		return !(left == right);
	}


	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> operator+ (const Matrix<number1>& left, const Matrix<number2>& right) {
		return Matrix<internal::NumberMax_t<number1, number2>>(left._mat() + right._mat());
	}

	template <typename number1, typename number2, typename>
	Matrix<internal::NumberMax_t<number1, number2>> operator+(const Matrix<number1>& left, number2 right)
	{
		return Matrix<internal::NumberMax_t<number1, number2>>{left._mat() + right};
	}

	template <typename number1, typename number2, typename>
	Matrix<internal::NumberMax_t<number1, number2>> operator+(number1 left, const Matrix<number2>& right)
	{
		return Matrix<internal::NumberMax_t<number1, number2>>{right._mat() + left};
	}

	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> operator- (const Matrix<number1>& left, const Matrix<number2>& right) {
		return Matrix<internal::NumberMax_t<number1, number2>>(left._mat() - right._mat());
	}

	template <typename number1, typename number2, typename>
	Matrix<internal::NumberMax_t<number1, number2>> operator-(const Matrix<number1>& left, number2 right)
	{
		return Matrix<internal::NumberMax_t<number1, number2>>{left._mat() - right};
	}

	template<typename number1, typename number2, typename>
	Matrix<internal::NumberMax_t<number1, number2>> operator* (number1 left, const Matrix<number2>& right) {
		return Matrix<internal::NumberMax_t<number1, number2>>(left*right._mat());
	};

	template<typename number1, typename number2, typename>
	Matrix<internal::NumberMax_t<number1, number2>> operator* (const Matrix<number1>& left, number2 right) {
		return Matrix<internal::NumberMax_t<number1, number2>>(right*left._mat());
	};

	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> operator* (const Matrix<number1>& left, const Matrix<number2>& right) {
		return Matrix<internal::NumberMax_t<number1, number2>>(left._mat()*right._mat());
	}

	template<typename number1, typename number2, typename>
	Matrix<internal::NumberMax_t<number1, number2>> operator/ (const Matrix<number1>& left, number2 right) {
		return Matrix<internal::NumberMax_t<number1, number2>>(left._mat() / right);
	};

	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> operator% (const Matrix<number1>& left, const Matrix<number2>& right) {
		return Matrix<internal::NumberMax_t<number1, number2>>(left._mat() % right._mat());
	}

	template <typename number1, typename number2>
	internal::NumberMax_t<number1, number2> dot(const Matrix<number1>& left, const Matrix<number2>& right)
	{
		return arma::dot(left._mat(), right._mat());
	}

	template <typename number>
	internal::NumberMax_t<number, number> normDot(const Matrix<number>& left, const Matrix<number>& right)
	{
		return arma::norm_dot(left._mat(), right._mat());
	}

	template <typename number>
	Matrix<real> abs(const Matrix<number>& mat)
	{
		return Matrix<real>(arma::abs(mat._mat()));
	}

	template <typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator*(const Matrix<number1>& mat, const Vector<number2>& vec)
	{
		return makeVector(mat._mat()*vec._vec());
	}

	template <typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> kron(const Matrix<number1>& left, const Matrix<number2>& right)
	{
		return Matrix<internal::NumberMax_t<number1, number2>>(arma::kron(left._mat(), right._mat()));
	}

}


namespace qengine
{
	template class Matrix<real>;
	template class Matrix<complex>;

	template Matrix<complex>::Matrix(const Matrix<real>&);

	template Matrix<real>&	  Matrix<real>::operator*= (real);
	template Matrix<complex>& Matrix<complex>::operator*= (real);
	template Matrix<complex>& Matrix<complex>::operator*= (complex);

	template Matrix<real>&    Matrix<real>::operator/= (real);
	template Matrix<complex>& Matrix<complex>::operator/= (real);
	template Matrix<complex>& Matrix<complex>::operator/= (complex);

	template bool operator== (const Matrix<real>&    left, const Matrix<real>&    right);
	template bool operator== (const Matrix<real>&    left, const Matrix<complex>& right);
	template bool operator== (const Matrix<complex>& left, const Matrix<real>&    right);
	template bool operator== (const Matrix<complex>& left, const Matrix<complex>& right);

	template bool operator!= (const Matrix<real>&    left, const Matrix<real>&    right);
	template bool operator!= (const Matrix<real>&    left, const Matrix<complex>& right);
	template bool operator!= (const Matrix<complex>& left, const Matrix<real>&    right);
	template bool operator!= (const Matrix<complex>& left, const Matrix<complex>& right);

	template Matrix<real>    operator+ (const Matrix<real>& left, const Matrix<real>& right);
	template Matrix<complex> operator+ (const Matrix<real>& left, const Matrix<complex>& right);
	template Matrix<complex> operator+ (const Matrix<complex>& left, const Matrix<real>& right);
	template Matrix<complex> operator+ (const Matrix<complex>& left, const Matrix<complex>& right);

	template Matrix<real>    operator+ (real left, const Matrix<real>& right);
	template Matrix<complex> operator+ (real left, const Matrix<complex>& right);
	template Matrix<complex> operator+ (complex left, const Matrix<real>& right);
	template Matrix<complex> operator+ (complex left, const Matrix<complex>& right);

	template Matrix<real>    operator+ (const Matrix<real>& left, real right);
	template Matrix<complex> operator+ (const Matrix<complex>& left, real right);
	template Matrix<complex> operator+ (const Matrix<real>& left, complex right);
	template Matrix<complex> operator+ (const Matrix<complex>& left, complex right);


	template Matrix<real>    operator- (const Matrix<real>& left, const Matrix<real>& right);
	template Matrix<complex> operator- (const Matrix<real>& left, const Matrix<complex>& right);
	template Matrix<complex> operator- (const Matrix<complex>& left, const Matrix<real>& right);
	template Matrix<complex> operator- (const Matrix<complex>& left, const Matrix<complex>& right);

	template Matrix<real>    operator- (const Matrix<real>& left, real right);
	template Matrix<complex> operator- (const Matrix<complex>& left, real right);
	template Matrix<complex> operator- (const Matrix<real>& left, complex right);
	template Matrix<complex> operator- (const Matrix<complex>& left, complex right);

	template Matrix<real>    operator* (real    left, const Matrix<real>&       right);
	template Matrix<complex> operator* (real    left, const Matrix<complex>&    right);
	template Matrix<complex> operator* (complex left, const Matrix<real>&       right);
	template Matrix<complex> operator* (complex left, const Matrix<complex>&    right);

	template Matrix<real>    operator* (const Matrix<real>&    left, real    right);
	template Matrix<complex> operator* (const Matrix<complex>& left, real    right);
	template Matrix<complex> operator* (const Matrix<real>&    left, complex right);
	template Matrix<complex> operator* (const Matrix<complex>& left, complex right);

	template Matrix<real>    operator* (const Matrix<real>&     left, const Matrix<real>& right);
	template Matrix<complex> operator* (const Matrix<complex>&  left, const Matrix<real>& right);
	template Matrix<complex> operator* (const Matrix<real>&     left, const Matrix<complex>& right);
	template Matrix<complex> operator* (const Matrix<complex>&  left, const Matrix<complex>& right);

	template Matrix<real>    kron(const Matrix<real>&     left, const Matrix<real>& right);
	template Matrix<complex> kron(const Matrix<complex>&  left, const Matrix<real>& right);
	template Matrix<complex> kron(const Matrix<real>&     left, const Matrix<complex>& right);
	template Matrix<complex> kron(const Matrix<complex>&  left, const Matrix<complex>& right);

	template Matrix<real>    operator/ (const Matrix<real>&    left, real    right);
	template Matrix<complex> operator/ (const Matrix<complex>& left, real    right);
	template Matrix<complex> operator/ (const Matrix<real>&    left, complex right);
	template Matrix<complex> operator/ (const Matrix<complex>& left, complex right);

	template Matrix<real>    operator% (const Matrix<real>& left, const Matrix<real>& right);
	template Matrix<complex> operator% (const Matrix<complex>& left, const Matrix<real>& right);
	template Matrix<complex> operator% (const Matrix<real>& left, const Matrix<complex>& right);
	template Matrix<complex> operator% (const Matrix<complex>& left, const Matrix<complex>& right);

	template real    dot(const Matrix<real>& left, const Matrix<real>& right);
	template complex dot(const Matrix<complex>& left, const Matrix<real>& right);
	template complex dot(const Matrix<real>& left, const Matrix<complex>& right);
	template complex dot(const Matrix<complex>& left, const Matrix<complex>& right);


	template real    normDot(const Matrix<real>& left, const Matrix<real>& right);
	template complex normDot(const Matrix<complex>& left, const Matrix<complex>& right);

	template Matrix<real> abs(const Matrix<real>&);
	template Matrix<real> abs(const Matrix<complex>&);

	template Vector<real>    operator* (const Matrix<real>&     left, const Vector<real>& right);
	template Vector<complex> operator* (const Matrix<complex>&  left, const Vector<real>& right);
	template Vector<complex> operator* (const Matrix<real>&     left, const Vector<complex>& right);
	template Vector<complex> operator* (const Matrix<complex>&  left, const Vector<complex>& right);

}

﻿/* COPYRIGHT
 *
 * file="SubviewCol.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
//
// Created by Jesper Hasseriis Mohr Jensen on 23/03/2017.
//

#pragma once

#include <memory>
#include <vector>

#include "src/NumberTypes.h"

#include "src/Utility/LinearAlgebra/Vector.h"

namespace arma
{
	template<class> class Mat;
	template<class> class subview_col;
}

namespace qengine
{
	template<class> class Matrix;
}


// Matrix subview to get/set columns
namespace qengine {
	namespace internal {
		template<typename number>
		class SubviewCol
		{

			/// CTOR
		public:
			// todo: consider making constructors private and make matrix(?) friend
			SubviewCol(Matrix<number>& Mat, count_t j);

			SubviewCol(const SubviewCol&); 
			SubviewCol(SubviewCol&&) noexcept; 
			~SubviewCol() = default;

			/// ASSIGNMENT
		public:
			SubviewCol<number>& operator=(const Vector<number>& v);
			SubviewCol<number>& operator=(const SubviewCol<number>& svc);
			SubviewCol<number>& operator=(SubviewCol<number>&& svc) noexcept;

			// assignment for scalar
			// SubviewCol<number>& operator=(const number1 t);
			operator Vector<number>() const;

			/// DATA ACCESS
		public:
			number & at(count_t n);
			const number& at(count_t n) const;

			Vector<real> re() const;
			Vector<real> im() const;

			template<typename number1, typename = std::enable_if_t<std::is_assignable<number&, number1>::value>>
			SubviewCol& operator=(number1 a)
			{
				for (auto i = 0u; i < size(); ++i)
				{
					at(i) = a;
				}
				return *this;
			}

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SubviewCol& operator/=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SubviewCol& operator*=(number1 t);

			SubviewCol& operator+=(const SubviewCol& other);
			SubviewCol& operator-=(const SubviewCol& other);

			SubviewCol& operator+=(const Vector<number>& other);
			SubviewCol& operator-=(const Vector<number>& other);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SubviewCol& operator+=(const number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SubviewCol& operator-=(const number1 t);

			Vector<number> operator-(); // 			

			/// MISC
		public:
			count_t size() const;
			real norm() const;
			Vector<real> absSquare() const;

			bool hasNan() const;

		private:
			Matrix<number>& Mat_;
			count_t j_;
		};
	}
}


/// free operations on SubviewCol
namespace qengine {
	namespace internal {

		template<typename number1, typename number2>
		NumberMax_t<number1, number2> dot(const SubviewCol<number1>& left, const SubviewCol<number2>& right);

		template<typename number1, typename number2>
		NumberMax_t<number1, number2> cdot(const SubviewCol<number1>& left, const SubviewCol<number2>& right);

		template<typename number1, typename number2>
		Vector<NumberMax_t<number1, number2>> operator+ (const SubviewCol<number1>& left, const SubviewCol<number2>& right);

		template<typename number1, typename number2>
		Vector<NumberMax_t<number1, number2>> operator- (const SubviewCol<number1>& left, const Vector<number2>& right);

		template<typename number1, typename number2>
		Vector<NumberMax_t<number1, number2>> operator- (const Vector<number1>& left, const SubviewCol<number2>& right);

		template<typename number1, typename number2>
		Vector<NumberMax_t<number1, number2>> operator- (const SubviewCol<number1>& left, const SubviewCol<number2>& right);

	}
}

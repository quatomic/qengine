﻿/* COPYRIGHT
 *
 * file="GenericSquareMatrixArithmetic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

/// 
/// \file GenericSquareMatrixArithmetic 
/// This file defines many free operators for SqMats, that simply use the in-place operations defined by the individual SqMats.
///

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/isSqMat.h"

#include "src/NumberTypes.h"
#include "src/Utility/cpp17Replacements/constexpr_ternary.h"


namespace qengine
{
	namespace internal
	{
		/// SqMat-vector multiply
		template<typename number, template<typename> class SqMat, typename=std::enable_if_t<is_SqMat_v<SqMat>, int>>
		CVec operator* (const SqMat<number>& mat, CVec vec);

		/// SqMat - SqMat operations
		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>>
		SqMat<NumberMax_t<number1, number2>> operator+(const SqMat<number1>& left, const SqMat<number2>& right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>>
		SqMat<NumberMax_t<number1, number2>> operator-(const SqMat<number1>& left, const SqMat<number2>& right);
		
		/// scalar - SqMat operations
		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		SqMat<NumberMax_t<number1, number2>> operator*(number1 left, const SqMat<number2>& right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		SqMat<NumberMax_t<number1, number2>> operator*(const SqMat<number2>& left, number1 right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		SqMat<NumberMax_t<number1, number2>> operator/(const SqMat<number2>& left, number1 right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		SqMat<NumberMax_t<number1, number2>> operator+(number1 left, const SqMat<number2>& right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		SqMat<NumberMax_t<number1, number2>> operator+(const SqMat<number2>& left, number1 right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		SqMat<NumberMax_t<number1, number2>> operator-(number1 left, const SqMat<number2>& right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		SqMat<NumberMax_t<number1, number2>> operator-(const SqMat<number2>& left, number1 right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		bool operator==(const SqMat<number1>& left, const SqMat<number2>& right);

		template<typename number1, typename number2, template<typename> class SqMat, typename = std::enable_if_t<is_SqMat_v<SqMat>>, typename = std::enable_if_t<is_number_type_v<number1>>>
		bool operator!=(const SqMat<number1>& left, const SqMat<number2>& right);

		template<class number, template<class> class SqMat, class numberMat>
		SqMat<number> convToNumber(const SqMat<numberMat>& m)
		{
			return repl17::constexpr_ternary(std::is_same<number, numberMat>{},
				[&](auto _)
			{
				return _(m);
			}, 
				[&](auto _)
			{
				return SqMat<number>(_(m));
			});
		}
		
		template<template<class> class SqMat>
		SqMat<real> identityMatrix(count_t dim);
	}
}

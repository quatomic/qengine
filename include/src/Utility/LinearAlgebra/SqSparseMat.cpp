﻿/* COPYRIGHT
 *
 * file="SqSparseMat.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include <armadillo>
#include <cassert>

#include "src/Utility/qengine_assert.h"
#include "src/Utility/narrow.h"

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"

#include "src/Utility/LinearAlgebra/SqDenseMat.h"
#include "src/Utility/LinearAlgebra/SqDiagMat.h"
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"

namespace
{
	template<typename number>
	const auto& rep(const qengine::internal::SqDiagMat<number>& mat) { return mat.diag(); }
}

namespace qengine
{
	namespace internal
	{
		// CTOR/DTOR

		template <typename number>
		SqSparseMat<number>::SqSparseMat(const count_t dim) :mat_(dim, dim) {}

		template <typename number>
		SqSparseMat<number>::SqSparseMat(const SparseMatrix<number>& mat) : mat_(mat) {
			qengine_assert(mat_.mat().is_square(), "matrix must be square");
		}

		template <typename number>
		SqSparseMat<number>::SqSparseMat(SparseMatrix<number>&& mat) : mat_(std::move(mat)) {
			qengine_assert(mat_.mat().is_square(), "matrix must be square");
		}


		namespace
		{
			template<class number>
			SparseMatrix<number> SparseMatFromVec(const Vector<number>& vec)
			{
				const auto dim = narrow<count_t>( std::sqrt(vec.dim()) );
				assert(vec.dim() == dim * dim);

				auto mat = arma::Mat<number>(vec._vec());
				mat.reshape(dim, dim);
				return SparseMatrix<number>(std::move(mat));
			}
			template<class number>
			SparseMatrix<number> SparseMatFromVec(Vector<number>&& vec)
			{
				const auto dim = narrow<count_t>(std::sqrt(vec.dim()));
				assert(vec.dim() == dim * dim);

				auto mat = arma::Mat<number>(std::move(std::move(vec)._vec()));
				mat.reshape(dim, dim);
				return SparseMatrix<number>(std::move(mat));
			}
		}

		template <typename number>
		SqSparseMat<number>::SqSparseMat(Vector<number>&& v) : SqSparseMat(SparseMatFromVec(std::move(v)))
		{
		}

		template <typename number>
		SqSparseMat<number>::SqSparseMat(const Vector<number>& v) : SqSparseMat(SparseMatFromVec(v))
		{
		}

		template<typename number>
		template<typename T2, typename T>
		SqSparseMat<number>::SqSparseMat(const SqSparseMat<T>& other) : mat_(other.mat())
		{
			qengine_assert(mat_.mat().is_square(), "matrix must be square");
		}


		// DATA ACCESS
		template <typename number>
		SparseMatrix<number>& SqSparseMat<number>::mat()
		{
			return mat_;
		}

		template <typename number>
		const SparseMatrix<number>& SqSparseMat<number>::mat() const
		{
			return mat_;
		}

		// MATH OPERATIONS
		template <>
		void SqSparseMat<real>::multiplyInPlace(Vector<complex>& vector) const
		{
			const auto& in = vector._vec();
			auto out = arma::cx_vec(vector.size(), arma::fill::zeros);

			arma::SpMat<real>::const_iterator A_it = mat().mat().begin();
			const arma::SpMat<real>::const_iterator A_it_end = mat().mat().end();

			while (A_it != A_it_end)
			{
				const real    A_it_val = (*A_it);
				const arma::uword A_it_row = A_it.row();
				const arma::uword A_it_col = A_it.col();
				
				auto& target = out.at(A_it_row);
				const auto& source = in.at(A_it_col);

				target += A_it_val * source;

				++A_it;
			}

			vector._vec() = out;
		}


		template <>
		void SqSparseMat<complex>::multiplyInPlace(Vector<complex>& vector) const
		{
			vector = CVec(arma::cx_vec(mat_.mat()*vector._vec()));
		}

		template <typename number>
		template <typename T, typename>
		void SqSparseMat<number>::multiplyInPlace(Vector<real>& vector) const
		{
			auto sparseVec = arma::sp_vec(vector._vec());
			sparseVec = mat_.mat()*sparseVec;

			const auto intermediate = RVec(arma::vec(sparseVec));
			vector = intermediate;
		}

		template<typename number>
		SqSparseMat<number>& SqSparseMat<number>::operator+=(const SqDiagMat<number>& other)
		{
			/// surprisingly, the cast is way, way faster than accessing the diagonal
			//mat_.mat().diag() += other.diag().vec();
			//return *this;
			return operator+=(static_cast<SqSparseMat<number>>(other));
		}


		template <typename number>
		template <typename number1, typename>
		SqSparseMat<number>& SqSparseMat<number>::operator/=(number1 t)
		{
			mat_ /= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqSparseMat<number>& SqSparseMat<number>::operator*=(number1 t)
		{
			mat_ *= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqSparseMat<number>& SqSparseMat<number>::operator+=(number1 t)
		{
			mat_.mat().diag(0) += t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqSparseMat<number>& SqSparseMat<number>::operator-=(number1 t)
		{
			mat_.mat().diag(0) -= t;
			return *this;
		}

		template<typename number>
		template <typename number1, typename>
		SqSparseMat<number>& SqSparseMat<number>::operator+=(const SqSparseMat<number1>& other) {
			//mat() = mat() + other.mat();
			mat() += other.mat();
			return *this;
		}

		template<typename number>
		template <typename number1, typename>
		SqSparseMat<number>& SqSparseMat<number>::operator-=(const SqSparseMat<number1>& other) {
			mat() -= other.mat();
			//                    mat() = mat() - other.mat(); // this does not work always for some reason
			return *this;
		}

		template<typename number>
		SqSparseMat<number> SqSparseMat<number>::operator-() const {
			return SqSparseMat<number>(SparseMatrix<number>(-mat_));
		}

		template <typename number>
		number SqSparseMat<number>::at(count_t nRow, count_t nCol) const
		{
			return mat_.at(nRow, nCol);
		}

		// MISC
		template <typename number>
		count_t SqSparseMat<number>::dim() const
		{
			return mat_.mat().n_cols;
		}

		template <typename number>
		bool SqSparseMat<number>::isHermitian() const
		{
			return arma::approx_equal(mat_.mat(), mat_.mat().t(), "absdiff", 1e-6);
		}

		template <typename number>
		bool SqSparseMat<number>::hasNan() const
		{
			return mat_.hasNan();
		}
	}
}

namespace qengine {
	namespace internal {
		std::ostream&operator<<(std::ostream& stream, const SqSparseMat<complex>& matrix)
		{
			stream << matrix.mat();
			return stream;
		}

		std::ostream&operator<<(std::ostream& stream, const SqSparseMat<real>& matrix)
		{
			stream << matrix.mat();
			return stream;
		}

	}
}

namespace qengine
{
	namespace internal
	{

		//// Nonmembers


		template <typename number1, typename number2>
		SqSparseMat<NumberMax_t<number1, number2>> operator+(const SqSparseMat<number1>& left, const SqDiagMat<number2>& right)
		{
			auto retval = convToNumber<NumberMax_t<number1, number2>>(left);
			retval += convToNumber<NumberMax_t<number1, number2>>(right);
			return retval;
		}

		template<typename number1, typename number2>
		SqSparseMat<NumberMax_t<number1, number2>> operator* (const SqSparseMat<number1>& left, const SqSparseMat<number2>& right) {
			return SqSparseMat<NumberMax_t<number1, number2>>(left.mat()*right.mat());
		}

		template <typename number1, typename number2>
		SqSparseMat<NumberMax_t<number1, number2>> kron(const SqSparseMat<number1>& left, const SqSparseMat<number2>& right)
		{
			return SqSparseMat<NumberMax_t<number1, number2>>(kron(left.mat(), right.mat()));
		}

		template <typename number>
		SqSparseMat<number> exp(const SqSparseMat<number>& d)
		{
			// note: arma::expmat is not defined explicitly for sparse matrices.. we have to use these annoying workarounds
			auto temp = arma::Mat<number>(d.mat().mat());
			temp = arma::expmat(temp);
			return SqSparseMat<number>{SparseMatrix<number>(temp)};
		}

		template<>
		SqSparseMat<real> identityMatrix<SqSparseMat>(const count_t dim)
		{
			auto mat = arma::speye(dim, dim);
			return SqSparseMat<real>(SparseMatrix<real>(std::move(mat)));
		}


		template <>
		Spectrum<complex> getSpectrum(const SqSparseMat<real>& matrix, const count_t largestEigenstate, const real normalization, const count_t extraStates, const real tol)
		{
			auto eigenvals = Vector<complex>(largestEigenstate + 1);
			auto eigenvecs = Matrix<complex>(matrix.dim(), largestEigenstate + 1);

			arma::eigs_gen(
				eigenvals._vec(),
				eigenvecs._mat(),
				matrix.mat().mat(),
				largestEigenstate + 1 + extraStates,
				"sr",
				tol
			);

			return Spectrum<complex>(std::move(eigenvals), std::move(eigenvecs), normalization);
		}

		template <>
		Spectrum<complex> getSpectrum(const SqSparseMat<complex>& matrix, const count_t largestEigenstate, const real normalization, const count_t extraStates, const real tol)
		{
			if (arma::sp_mat(arma::imag(matrix.mat().mat())).n_nonzero == 0)
			{
				return getSpectrum(SqSparseMat<real>(matrix.mat().re()), largestEigenstate, normalization, extraStates, tol);
			}
			else
			{
				throw std::runtime_error("cannot diagonalize complex sparse matrix");
			}
		}

		template<>
		Spectrum<real> getSpectrum_herm(const SqSparseMat<real>& matrix, const count_t largestEigenstate, const real normalization, const count_t extraStates, const real tol)
		{
			auto eigenvals = Vector<real>(largestEigenstate + 1);
			auto eigenvecs = Matrix<real>(matrix.dim(), largestEigenstate + 1);

			arma::eigs_sym(
				eigenvals._vec(),
				eigenvecs._mat(),
				matrix.mat().mat(),
				largestEigenstate + 1 + extraStates,
				"sa",
				tol
			);

			return Spectrum<real>(std::move(eigenvals), std::move(eigenvecs), normalization);
		}

		template<>
		Spectrum<real> getSpectrum_herm(const SqSparseMat<complex>& matrix, const count_t largestEigenstate, const real normalization, const count_t extraStates, const real tol)
		{
			auto eigenvals = Vector<complex>(largestEigenstate + 1);
			auto eigenvecs = Matrix<complex>(matrix.dim(), largestEigenstate + 1);

			arma::eigs_gen(
				eigenvals._vec(),
				eigenvecs._mat(),
				matrix.mat().mat(),
				largestEigenstate + 1 + extraStates,
				"sr",
				tol
			);

			return Spectrum<real>(std::move(eigenvals.re()), std::move(eigenvecs), normalization);
		}
	}
}


// explicit instantiations of SqSparseMat and related free operators
namespace qengine {
	namespace internal
	{
		template class SqSparseMat<real>;
		template class SqSparseMat<complex>;

		template SqSparseMat<complex>::SqSparseMat(const SqSparseMat<real>&);

		template void SqSparseMat<real>::multiplyInPlace(Vector<real>&) const;

		template SqSparseMat<real>&    SqSparseMat<real>::operator/= (real);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator/= (real);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator/= (complex);

		template SqSparseMat<real>&    SqSparseMat<real>::operator*= (real);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator*= (real);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator*= (complex);

		template SqSparseMat<real>&    SqSparseMat<real>::operator+= (real);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator+= (real);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator+= (complex);

		template SqSparseMat<real>&    SqSparseMat<real>::operator-= (real);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator-= (real);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator-= (complex);

		template SqSparseMat<real>&    SqSparseMat<real>::operator+= (const SqSparseMat<real>&);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator+= (const SqSparseMat<real>&);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator+= (const SqSparseMat<complex>&);

		template SqSparseMat<real>&    SqSparseMat<real>::operator-= (const SqSparseMat<real>&);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator-= (const SqSparseMat<real>&);
		template SqSparseMat<complex>& SqSparseMat<complex>::operator-= (const SqSparseMat<complex>&);

		template SqSparseMat<real>    operator* (const SqSparseMat<real>&, const SqSparseMat<real>&);
		template SqSparseMat<complex> operator* (const SqSparseMat<complex>&, const SqSparseMat<real>&);
		template SqSparseMat<complex> operator* (const SqSparseMat<real>&, const SqSparseMat<complex>&);
		template SqSparseMat<complex> operator* (const SqSparseMat<complex>&, const SqSparseMat<complex>&);

		template SqSparseMat<real>    kron(const SqSparseMat<real>&, const SqSparseMat<real>&);
		template SqSparseMat<complex> kron(const SqSparseMat<complex>&, const SqSparseMat<real>&);
		template SqSparseMat<complex> kron(const SqSparseMat<real>&, const SqSparseMat<complex>&);
		template SqSparseMat<complex> kron(const SqSparseMat<complex>&, const SqSparseMat<complex>&);

		template SqSparseMat<real>    operator+ (const SqSparseMat<real>&, const SqDiagMat<real>&);
		template SqSparseMat<complex> operator+ (const SqSparseMat<complex>&, const SqDiagMat<real>&);
		template SqSparseMat<complex> operator+ (const SqSparseMat<real>&, const SqDiagMat<complex>&);
		template SqSparseMat<complex> operator+ (const SqSparseMat<complex>&, const SqDiagMat<complex>&);

		template SqSparseMat<real> exp(const SqSparseMat<real>&);
		template SqSparseMat<complex> exp(const SqSparseMat<complex>&);
	}
}

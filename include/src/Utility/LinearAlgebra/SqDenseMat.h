﻿/* COPYRIGHT
 *
 * file="SqDenseMat.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <memory>

#include "src/NumberTypes.h"

#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/Spectrum.h"

namespace qengine
{
	namespace internal
	{
		template<typename number> class SqSparseMat;
		template<typename number> class SqDiagMat;

		template<typename number>
		class SqDenseMat
		{
			// CTOR/DTOR
		public:
			explicit SqDenseMat(count_t dim);
			explicit SqDenseMat(const Matrix<number>& mat);
			explicit SqDenseMat(Matrix<number>&& mat);

			explicit SqDenseMat(Vector<number>&& v);
			explicit SqDenseMat(const Vector<number>& v);

			explicit SqDenseMat(const SqSparseMat<number>& m);
			
			template<typename T2 = number, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
			explicit SqDenseMat(const SqDenseMat<T>& other);

			// ASSIGNMENT
		public:

			SqDenseMat& operator=(const Matrix<number>& mat);
			SqDenseMat& operator=(Matrix<number>&& mat);

			// DATA ACCESS
		public:
			Matrix<number>& mat();
			const Matrix<number>& mat() const;

			// MATH OPERATIONS
		public:
			void multiplyInPlace(Vector<complex>& vector) const;

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDenseMat& operator/=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDenseMat& operator*=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDenseMat& operator+=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDenseMat& operator-=(number1 t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDenseMat& operator+=(const SqDenseMat<number1>& t);

			template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
			SqDenseMat& operator-=(const SqDenseMat<number1>& t);

			SqDenseMat operator-() const;


			SqDenseMat& operator+=(const SqDiagMat<number>& other);

			number at(count_t nRow, count_t nCol) const;

			//MISC
		public:
			count_t dim() const;
			bool isHermitian() const;

			bool hasNan() const;

		private:
			Matrix<number> mat_;
		};
	}
}

namespace qengine
{
	namespace internal
	{
		// Free functions
		std::ostream& operator<<(std::ostream& stream, const SqDenseMat<real>& matrix);
		std::ostream& operator<<(std::ostream& stream, const SqDenseMat<complex>& matrix);

		template<typename number1, typename number2>
		SqDenseMat<NumberMax_t<number1, number2>> operator+ (const SqDenseMat<number1>& left, const SqDiagMat<number2>& right);


		template<typename number1, typename number2>
		SqDenseMat<NumberMax_t<number1, number2>> operator* (const SqDenseMat<number1>& left, const SqDenseMat<number2>& right);

		template<typename number1, typename number2>
		SqDenseMat<NumberMax_t<number1, number2>> kron (const SqDenseMat<number1>& left, const SqDenseMat<number2>& right);

		template<typename number>
		SqDenseMat<number> exp(const SqDenseMat<number>& d);

		template<typename number>
		SqDenseMat<number> pow(const SqDenseMat<number>& d, count_t exponent);

		template <typename MatNumber>
		Spectrum<complex> getSpectrum(const SqDenseMat<MatNumber>& matrix, count_t largestEigenstate, const real normalization = 1.0);

		template <typename MatNumber>
		Spectrum<real> getSpectrum_herm(const SqDenseMat<MatNumber>& matrix, count_t largestEigenstate, const real normalization = 1.0);

	}
}

﻿/* COPYRIGHT
 *
 * file="Spectrum.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

/**
* file: Spectrum.h
* library: QEngine-C++
* module: QPhysics
*
* Helper-class used as return value of eigHerm-functions. Individual eigenstates/-values can be accessed and linear combinations constructed.
* Eigenstates in the QEngine follow the conventation that even eigenstates start positive, while odd eigenstates start negative. This does not always work for "weird" diagonalization routines.
**/

#include "src/NumberTypes.h"

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"

namespace qengine
{
	namespace internal
	{
		// class to carry a list of eigenvalues and -vectors of a matrix. Eigenvalues can be either real or complex, 
		// depending on hermiticity of the matrix. It is assumed (but not enforced), that eigenvalues are the smallest 
		// eigenvalues of the matrix in increasing order. It is also assumed that each eigenvalue corresponds to the eigenvector
		// with the same index. 
		template<typename valuesNumber>
		class Spectrum
		{
		public:
			Spectrum(const Vector<valuesNumber>& eigenvalues, const Matrix<complex>& eigenstates, double normalization);
			Spectrum(Vector<valuesNumber>&& eigenvalues, Matrix<complex>&& eigenstates, double normalization);
			Spectrum(Spectrum&& other, double normalization);

			valuesNumber eigenvalue(count_t index) const;

			CVec eigenvector(count_t index) const;

			/// For factors = {a,b,c} returns (a*|0>+b|1>+c*|2>)
			CVec makeLinearCombination(const CVec& factors) const;

			const Vector<valuesNumber>& eigenvalues() const { return eigenvalues_; }
			const Matrix<complex>& eigenvectors() const { return eigenstates_; }

			count_t largestEigenstate();
		private:
			Vector<valuesNumber> eigenvalues_;
			Matrix<complex> eigenstates_;
		};
	}
}

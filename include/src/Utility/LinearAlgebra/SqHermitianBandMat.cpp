﻿/* COPYRIGHT
 *
 * file="SqHermitianBandMat.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"
#include <armadillo>

#include "src/Utility/qengine_assert.h"
#include "src/Utility/narrow.h"

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/LapackeWrapper.h"
#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"

#include "src/Utility/LinearAlgebra/SqSparseMat.h"
#include "src/Utility/LinearAlgebra/SqDiagMat.h"
#include "src/Utility/LinearAlgebra/SqDenseMat.h"


namespace qengine
{
	namespace internal
	{
		namespace
		{
			void killZeroParts(Matrix<real>& bands)
			{
				for(auto i = 1u; i<bands.n_rows();++i)
				{
					for(auto j = bands.n_cols()-i; j<bands.n_cols();++j)
					{
						bands.at(i, j) = 0;
					}
				}
			}
		}

		/// CTOR
		template<typename number>
		SqHermitianBandMat<number>::SqHermitianBandMat(const count_t dim, const count_t bandwidth) : SqHermitianBandMat(dim, bandwidth, 0.0)
		{
		}

		template <typename number>
		SqHermitianBandMat<number>::SqHermitianBandMat(const count_t dim, const count_t bandwidth, const real fill) : bands_(bandwidth + 1, dim, fill)
		{
			killZeroParts(bands_);
		}

		template<typename number>
		SqHermitianBandMat<number>::SqHermitianBandMat(const Matrix<number>& M, const STORAGE_LAYOUT storage) : bands_(storage == STORAGE_LAYOUT::ROW_LAYOUT ? M : Matrix<number>(M._mat().st()))
		{
			killZeroParts(bands_);
		}

		template<typename number>
		SqHermitianBandMat<number>::SqHermitianBandMat(Matrix<number>&& M, const STORAGE_LAYOUT storage) : bands_(storage == STORAGE_LAYOUT::ROW_LAYOUT ? std::move(M) : Matrix<number>(M._mat().st()))
		{
			killZeroParts(bands_);
		}

		/// DATA ACCESS
		template<typename number>
		Matrix<number>& SqHermitianBandMat<number>::bands() {
			return bands_;
		}

		template<typename number>
		const Matrix<number>& SqHermitianBandMat<number>::bands() const {
			return bands_;
		}

		template <typename number>
		number SqHermitianBandMat<number>::get(const count_t rowIndex, const count_t colIndex) const
		{
			if (rowIndex >= dim() || colIndex >= dim()) throw std::runtime_error("index out of bounds");

			const auto diagIndex = rowIndex >= colIndex ? rowIndex - colIndex : colIndex - rowIndex;
			const auto posIndex = rowIndex >= colIndex ? colIndex : rowIndex;

			if (diagIndex > bandwidth()) return {0};

			if (rowIndex > colIndex) return bands_(diagIndex, posIndex);
			return bands_(diagIndex, posIndex);
		}

		/// MATH OPERATIONS

		template<>
		void SqHermitianBandMat<real>::multiplyInPlace(RVec& v) const {

			// todo: implement this; look in qengine2.0 for inspiration
			const auto matDimension = narrow<int>( dim() );
			const auto nOffdiags = narrow<int>( bandwidth() );

			auto realPart = v.re();
			auto imagPart = v.im();

			cblas_dsbmv( // linear algebra routine to do y = alpha*A*x+beta*y for real vectors x,y, real constants alpha and beta and symmetric real band matrix A
				CBLAS_LAYOUT::CblasColMajor, // each row of bands corresponds to a diagonal in matrix A
				CBLAS_UPLO::CblasLower, // We give the lower part of the symmetric matrix (relevant for the order of the diagonals)
				matDimension, // dimension of matrix A
				nOffdiags, // nr of offdiagonals
				1.0, // alpha
				bands_._mat().memptr(), //pointer to the representation of the matrix. 
				nOffdiags + 1,// "leading dimension". not sure what it means, but this number works!
				realPart._data(), // input x. Apparently, we can not just use the same array as for output
				1, // stepSize between doubles in x. more important below
				0.0, // beta
				v._data(),	// input AND output y. As y is actually a complex array, we need to reinterpret the pointer as an array to double
				1 // steps between the used double in vector y. This must be 2, as we want to apply the matrix to the real and imaginary parts separately
			);
		}


		template<>
		void SqHermitianBandMat<real>::multiplyInPlace(CVec& v) const {

			// todo: implement this; look in qengine2.0 for inspiration
			const auto matDimension = narrow<int>( dim() );
			const auto nOffdiags = narrow<int>( bandwidth() );

			auto realPart = v.re();
			auto imagPart = v.im();

			cblas_dsbmv( // linear algebra routine to do y = alpha*A*x+beta*y for real vectors x,y, real constants alpha and beta and symmetric real band matrix A
				CBLAS_LAYOUT::CblasColMajor, // each row of bands corresponds to a diagonal in matrix A
				CBLAS_UPLO::CblasLower, // We give the lower part of the symmetric matrix (relevant for the order of the diagonals)
				matDimension, // dimension of matrix A
				nOffdiags, // nr of offdiagonals
				1.0, // alpha
				bands_._mat().memptr(), //pointer to the representation of the matrix. 
				nOffdiags + 1,// "leading dimension". not sure what it means, but this number works!
				realPart._data(), // input x. Apparently, we can not just use the same array as for output
				1, // stepSize between doubles in x. more important below
				0.0, // beta
				reinterpret_cast<double*>(v._data()),	// input AND output y. As y is actually a complex array, we need to reinterpret the pointer as an array to double
				2 // steps between the used double in vector y. This must be 2, as we want to apply the matrix to the real and imaginary parts separately
			);

			cblas_dsbmv(
				CBLAS_LAYOUT::CblasColMajor,
				CBLAS_UPLO::CblasLower,
				matDimension,
				nOffdiags,
				1.0,
				bands_._mat().memptr(),
				nOffdiags + 1,
				imagPart._data(),
				1,
				0.0,
				reinterpret_cast<double*>(v._data()) + 1, // now +1, so we start with the imaginary part and take steps of 2 doubles
				2
			);
		}

		template <>
		SqHermitianBandMat<real>& SqHermitianBandMat<real>::operator+=(const SqDiagMat<real>& diagMat)
		{
			bands_._mat().row(0) += diagMat.diag()._vec().st();
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqHermitianBandMat<number>& SqHermitianBandMat<number>::operator/=(number1 t)
		{
			bands_ /= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqHermitianBandMat<number>& SqHermitianBandMat<number>::operator*=(number1 t)
		{
			bands_ *= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqHermitianBandMat<number>& SqHermitianBandMat<number>::operator+=(number1 t)
		{
			bands_._mat().row(0) += t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqHermitianBandMat<number>& SqHermitianBandMat<number>::operator-=(number1 t)
		{
			bands_._mat().row(0) -= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqHermitianBandMat<number>& SqHermitianBandMat<number>::operator+=(const SqHermitianBandMat<number1>& other)
		{
			if (other.bandwidth() > bandwidth())
			{
				// if the other has a larger bandwidth, things get complicated, as we can't just add each col, we would need to resize.
				// We get around this, by basically swapping the two matrices (needs intermediate) and then call the function again,
				// but this time the other one has a smaller bandwidth due to the swap.
				auto intermediate = other;
				bands_._mat().swap(intermediate.bands_._mat());
				this->operator+=(intermediate);
			}

			for (auto i = 0u; i < other.bandwidth() + 1; ++i)
			{
				bands_._mat().row(i) += other.bands_._mat().row(i);
			}

			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqHermitianBandMat<number>& SqHermitianBandMat<number>::operator-=(const SqHermitianBandMat<number1>& other)
		{
			if (other.bandwidth() > bandwidth())
			{
				// if the other has a larger bandwidth, things get complicated, as we can't just add each col, we would need to resize.
				// We get around this, by basically swapping the two matrices (needs intermediate) and then call the function again,
				// but this time the other one has a smaller bandwidth due to the swap.
				auto intermediate = other;
				bands_._mat().swap(intermediate.bands_._mat());
				this->operator-=(intermediate);
			}

			for (auto i = 0u; i < other.bandwidth() + 1; ++i)
			{
				bands_._mat().row(i) -= other.bands_._mat().row(i);
			}

			return *this;
		}

		template <typename number>
		SqHermitianBandMat<number> SqHermitianBandMat<number>::operator-() const
		{
			return SqHermitianBandMat(-bands_);
		}

		template <typename number>
		SqHermitianBandMat<number>::operator SqDenseMat<number>() const
		{
			auto denseRep = arma::mat(dim(), dim(), arma::fill::zeros);

			denseRep.diag(0) = bands_._mat().row(0);

			arma::Row<number> currentDiag;
			for (auto i = 1u; i <= bandwidth(); ++i)
			{
				currentDiag = bands_._mat().row(i);
				denseRep.diag(i) = currentDiag.head(dim() - i);
				denseRep.diag(-narrow<int>( i )) = currentDiag.head(dim() - i);
			}
			return SqDenseMat<number>(Matrix<number>(std::move(denseRep)));
		}

		template <typename number>
		SqHermitianBandMat<number>::operator SqSparseMat<number>() const
		{
			auto sparseRep = arma::SpMat<number>(dim(), dim());
			sparseRep.diag(0) = bands_._mat().row(0);

			arma::Row<number> currentDiag;
			for (auto i = 1u; i <= bandwidth(); ++i)
			{
				currentDiag = bands_._mat().row(i);
				sparseRep.diag(i) = currentDiag.head(dim() - i);
				sparseRep.diag(-narrow<int>( i )) = currentDiag.head(dim() - i);
			}

			return SqSparseMat<number>(SparseMatrix<number>(std::move(sparseRep)));
		}

		template <typename number>
		number SqHermitianBandMat<number>::at(const count_t nRow, const count_t nCol) const
		{
			const count_t diag = std::abs(static_cast<long long>( nRow ) - static_cast<long long>( nCol ));
			if (diag > bandwidth()) return 0;

			return bands_.at(diag, std::max(nRow, nCol));
		}

		/// MISC 
		template<typename number>
		count_t SqHermitianBandMat<number>::dim() const {
			return bands_.n_cols();
		}

		template<typename number>
		count_t SqHermitianBandMat<number>::bandwidth() const {
			return bands_.n_rows() - 1;
		}

		template <typename number>
		bool SqHermitianBandMat<number>::hasNan() const
		{
			return bands_.hasNan();
		}
	}
}

namespace qengine
{
	namespace internal
	{
		std::ostream& operator<<(std::ostream& stream, const SqHermitianBandMat<real>& matrix)
		{
			stream << static_cast<SqDenseMat<real>>(matrix);
			return stream;
		}

		SqDenseMat<real> exp(const SqHermitianBandMat<real>& exponent)
		{
			return qengine::internal::exp(static_cast<SqDenseMat<real>>(exponent));
		}

		SqDenseMat<real> pow(const SqHermitianBandMat<real>& d, const count_t exponent)
		{
			auto densemat = SqDenseMat<real>(d);
			return pow(densemat, exponent);
		}

		SqDenseMat<real> operator*(SqHermitianBandMat<real> left, const SqHermitianBandMat<real>& right)
		{
			return static_cast<SqDenseMat<real>>(left)*static_cast<SqDenseMat<real>>(right);
		}

		SqHermitianBandMat<real> kron(SqHermitianBandMat<real> a, const SqHermitianBandMat<real>& b)
		{
			const auto resultBandWidth = a.bandwidth()*b.dim() + b.bandwidth();
			auto ab = SqHermitianBandMat<real>(a.dim()*b.dim(), resultBandWidth, 0.0);

			auto a_dense = SqDenseMat<real>(a);
			auto b_dense = SqDenseMat<real>(b);

			const auto N_b = narrow<int>(b.dim());
			const auto M_b = narrow<int>(b.dim());

			for(auto i_ab = 0u; i_ab <= ab.bandwidth(); ++i_ab)
			{
				for(auto j_ab = 0u; j_ab<ab.dim()-i_ab;++j_ab)
				{
					const auto n_ab = i_ab + j_ab;
					const auto m_ab = j_ab;

					const auto n_a = n_ab / N_b;
					const auto m_a = m_ab / M_b;

					const auto n_b = n_ab % N_b;
					const auto m_b = m_ab % M_b;

					ab.bands()(i_ab, j_ab) = a.get(n_a,m_a)*b.get(n_b, m_b);
				}
			}
			return ab;
		}

		SqDenseMat<complex> operator*(complex c, const SqHermitianBandMat<real>& mat)
		{
			return c * static_cast<SqDenseMat<real>>(mat);
		}

		SqHermitianBandMat<real> operator+(SqHermitianBandMat<real> left, const SqDiagMat<real>& right)
		{
			left += right;

			return left;
		}

		SqHermitianBandMat<real> operator+(const SqDiagMat<real>& left, const SqHermitianBandMat<real>& right)
		{
			return right + left;
		}

		void eigHerm(const SqHermitianBandMat<real>& mat, Vector<real>& vals, Matrix<real>& vecs, const count_t largestEigenstate)
		{
			qengine_assert(largestEigenstate < mat.dim(), "Highest possible eigenstate that can be found is (dimension - 1)");
			const auto nEigenstates = narrow<int>(largestEigenstate + 1);

			//arma::eig_sym(vals, vecs, mat.readRep(), "dc");
			auto matrix = mat.bands();
			const auto size = mat.dim();
			const auto nOffDiags = narrow<int>(mat.bandwidth());

			vals = Vector<real>(arma::zeros(nEigenstates));
			vecs = Matrix<real>(arma::zeros(size, nEigenstates));

			arma::mat Q(size, size, arma::fill::zeros);
			auto m = 0;
			// Varying the size of the following 'work'-arrays may improve speed
			arma::vec work(7 * size );
			std::vector<int> iwork(5 * size );
			std::vector<int> ifail(size);
			LAPACKE_dsbevx_work(
				LAPACK_COL_MAJOR,
				'V',
				'I',
				'L',
				narrow<int>(size),
				nOffDiags,
				matrix._data(),
				nOffDiags + 1,
				Q.memptr(),
				static_cast<lapack_int>( size ),
				0,
				0,
				1,
				nEigenstates,
				0.0,
				&m,
				vals._data(),
				vecs._data(),
				narrow<int>(size),
				work.memptr(),
				iwork.data(),
				ifail.data()
			);
		}

		Spectrum<complex> getSpectrum(const SqHermitianBandMat<real>& matrix, const count_t largestEigenstate, const real normalization)
		{
			auto eigenvals = Vector<real>(largestEigenstate + 1);
			auto eigenvecs = Matrix<real>(matrix.dim(), largestEigenstate + 1);

			internal::eigHerm(matrix, eigenvals, eigenvecs, largestEigenstate);
			return Spectrum<complex>(std::move(eigenvals), std::move(eigenvecs), normalization);
		}

		Spectrum<real> getSpectrum_herm(const SqHermitianBandMat<real>& matrix, const count_t largestEigenstate, const real normalization)
		{
			auto eigenvals = Vector<real>(largestEigenstate + 1);
			auto eigenvecs = Matrix<real>(matrix.dim(), largestEigenstate + 1);

			internal::eigHerm(matrix, eigenvals, eigenvecs, largestEigenstate);
			return Spectrum<real>(std::move(eigenvals), std::move(eigenvecs), normalization);
		}
	}

}

namespace qengine
{
	namespace internal
	{
		template class SqHermitianBandMat<real>;

		template SqHermitianBandMat<real>& SqHermitianBandMat<real>::operator*= (real);

		template SqHermitianBandMat<real>& SqHermitianBandMat<real>::operator/= (real);

		template SqHermitianBandMat<real>& SqHermitianBandMat<real>::operator+= (real);

		template SqHermitianBandMat<real>& SqHermitianBandMat<real>::operator-= (real);

		template SqHermitianBandMat<real>& SqHermitianBandMat<real>::operator+= (const SqHermitianBandMat<real>&);

		template SqHermitianBandMat<real>& SqHermitianBandMat<real>::operator-= (const SqHermitianBandMat<real>&);
	}
}

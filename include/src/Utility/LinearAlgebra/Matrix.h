﻿/* COPYRIGHT
 *
 * file="Matrix.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
//
// Created by Jesper Hasseriis Mohr Jensen on 23/03/2017.
//

#pragma once

#include <memory>
#include <vector>

#include "src/NumberTypes.h"

#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Utility/LinearAlgebra/SubviewCol.h"

namespace arma
{
	template<class> class Mat;
}

namespace qengine
{
	namespace internal
	{
		template<class> class SubviewCol;
	}
}

namespace qengine
{
	template<typename number>
	class Matrix
	{

	public:
		/// CTOR/DTOR
		Matrix();
		explicit Matrix(count_t n_rows, count_t n_cols);
		explicit Matrix(count_t n_rows, count_t n_cols, number fill);
		explicit Matrix(count_t n_rows, count_t n_cols, number* data);
		Matrix(const arma::Mat<number>& data);
		Matrix(arma::Mat<number>&& data);
		explicit Matrix(std::initializer_list<std::initializer_list<number>> data);
		explicit Matrix(const std::vector<Vector<number>>& data);
		//explicit Matrix(std::initializer_list<Vector<number>> data);
		explicit Matrix(std::vector<number> data);
		explicit Matrix(const Vector<number>& Vec);

		Matrix(const Matrix& other);
		Matrix(Matrix&& other) noexcept;

		template<typename T2 = number, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
		Matrix(const Matrix<T>& other);

		~Matrix();

		/// ASSIGNMENT
		Matrix& operator=(const Matrix& other);
		Matrix& operator=(Matrix&& other) noexcept;

		/// DATA ACCESS
	public:
		number & at(count_t n); // access elements with flat layout (col. major)
		const number& at(count_t n) const;

		number& at(count_t row_index, count_t col_index);
		const number& at(count_t row_index, count_t col_index) const;

		number& operator()(count_t row_index, count_t col_index);
		const number& operator()(count_t row_index, count_t col_index) const;

		internal::SubviewCol<number> col(count_t j);
		const internal::SubviewCol<number> col(count_t j) const;

		Matrix<real> re() const;
		Matrix<real> im() const;

		Vector<number> vectorize() const;

		/// MATH OPERATORS
	public:
		template<typename number1, typename = std::enable_if_t<std::is_same<number, internal::NumberMax_t<number, number1>>::value, int>>
		Matrix& operator/=(number1 t);

		template<typename number1, typename = std::enable_if_t<std::is_same<number, internal::NumberMax_t<number, number1>>::value, int>>
		Matrix& operator*=(number1 t);

		Matrix& operator+=(const Matrix<number>& other);
		Matrix& operator-=(const Matrix<number>& other);

		Matrix operator-() const;

		/// MISC
	public:
		count_t n_rows() const;
		count_t n_cols() const;
		count_t n_elem() const;

		bool hasNan() const;

		/// INTERNAL USE
	public:
		number * _data();
		const number* _data() const;

		const auto& _rep() const { return _mat(); }

		arma::Mat<number>& _mat();
		const arma::Mat<number>& _mat() const;

		void _append(const number& val);

	private:
		std::unique_ptr<arma::Mat<number>> mat_;
	};
}

namespace qengine
{
	class SerializedObject;

	template<class number>
	SerializedObject toSerializedObject(const Matrix<number>& vec);

	template<class number>
	Matrix<number> fromSerializedObject(const SerializedObject& obj, internal::Type<Matrix<number>>);

}



namespace qengine
{
	// Free functions
	std::ostream& operator<<(std::ostream& stream, const Matrix<real>& matrix);
	std::ostream& operator<<(std::ostream& stream, const Matrix<complex>& matrix);

	// equality is checked for the full matrix: true only if every element is equal (remember limited double-precision)
	template<typename number1, typename number2>
	bool operator== (const Matrix<number1>& left, const Matrix<number2>& right);

	// equality is checked for the full matrix: true only if every element is equal (remember limited double-precision)
	template<typename number1, typename number2>
	bool operator!= (const Matrix<number1>& left, const Matrix<number2>& right);

	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> operator+ (const Matrix<number1>& left, const Matrix<number2>& right);

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_number_type_v<number2>>>
	Matrix<internal::NumberMax_t<number1, number2>> operator+ (const Matrix<number1>& left, number2 right);

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_number_type_v<number1>>>
	Matrix<internal::NumberMax_t<number1, number2>> operator+ (number1 left, const Matrix<number2>& right);

	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> operator- (const Matrix<number1>& left, const Matrix<number2>& right);

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_number_type_v<number2>>>
	Matrix<internal::NumberMax_t<number1, number2>> operator- (const Matrix<number1>& left, number2 right);

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_number_type_v<number1>, int>>
	Matrix<internal::NumberMax_t<number1, number2>> operator* (number1 left, const Matrix<number2>& right);

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_number_type_v<number1>, int>>
	Matrix<internal::NumberMax_t<number1, number2>> operator* (const Matrix<number1>& left, number2 right);

	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> operator* (const Matrix<number1>& left, const Matrix<number2>& right);

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_number_type_v<number1>, int>>
	Matrix<internal::NumberMax_t<number1, number2>> operator/ (const Matrix<number1>& left, number2 right);

	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> operator% (const Matrix<number1>& left, const Matrix<number2>& right);

	template<typename number1, typename number2>
	internal::NumberMax_t<number1, number2> dot(const Matrix<number1>& left, const Matrix<number2>& right);

	template<typename number>
	internal::NumberMax_t<number, number> normDot(const Matrix<number>& left, const Matrix<number>& right);

	template<typename number>
	Matrix<real> abs(const Matrix<number>& mat);

	template<typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator*(const Matrix<number1>& mat, const Vector<number2>& vec);


	template<typename number1, typename number2>
	Matrix<internal::NumberMax_t<number1, number2>> kron(const Matrix<number1>& left, const Matrix<number2>& right);
}

namespace qengine
{
	using RMat = Matrix<real>;
	using CMat = Matrix<complex>;
}

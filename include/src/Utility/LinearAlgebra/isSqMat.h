﻿/* COPYRIGHT
 *
 * file="isSqMat.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

namespace qengine
{
	namespace internal
	{
		template<class> class SqDiagMat;
		template<class> class SqDenseMat;
		template<class> class SqSparseMat;
		template<class> class SqHermitianBandMat;

		template<template<typename> class T> struct is_SqMat { constexpr static bool value = false; };
		template<template<typename> class T> constexpr static bool is_SqMat_v = is_SqMat<T>::value;

		template<> struct is_SqMat<SqDiagMat> { constexpr static bool value = true; };
		template<> struct is_SqMat<SqDenseMat> { constexpr static bool value = true; };
		template<> struct is_SqMat<SqSparseMat> { constexpr static bool value = true; };
		template<> struct is_SqMat<SqHermitianBandMat> { constexpr static bool value = true; };

		template<class T> struct is_SqMat_class { static constexpr bool value = false; };
		template<class T> constexpr static bool is_SqMat_class_v = is_SqMat_class<T>::value;

		template<template<class> class temp> struct is_SqMat_class<temp<real>> { static constexpr bool value = is_SqMat<temp>::value; };
		template<template<class> class temp> struct is_SqMat_class<temp<complex>> { static constexpr bool value = is_SqMat<temp>::value; };
	}
}

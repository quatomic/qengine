﻿/* COPYRIGHT
 *
 * file="SparseMatrix.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
//
// Created by Jesper Hasseriis Mohr Jensen on 23/03/2017.
//

#pragma once

#include <memory>

#include "src/NumberTypes.h"
#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/SparseElement.h"

namespace arma
{
    template<class> class SpMat;
    template<class> class Mat;
}

namespace qengine
{
    namespace internal
	{
        template<typename number>
        class SparseMatrix
        {

//            template<class number>
//            class NumberWrapper{
//            public:
//                number value

//            private:
//                SparseMatrix<number> Mat_;
//                count_t row_,col_;
//            };

        public:
            /// CTOR/DTOR
            explicit SparseMatrix(count_t n_rows,count_t n_cols);
            explicit SparseMatrix(const arma::Mat<number>& data);
            explicit SparseMatrix(arma::Mat<number>&& data);

            SparseMatrix(const arma::SpMat<number>& data);
            SparseMatrix(arma::SpMat<number>&& data);

            explicit SparseMatrix(const Matrix<number>& data);
            explicit SparseMatrix(Matrix<number>&& data);

            // todo: implement more constructor
			template<typename T2 = number, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
			explicit SparseMatrix(const SparseMatrix<T>& realMat, const SparseMatrix<T>& imagMat);

			template<typename T2 = number, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
			explicit SparseMatrix(const SparseMatrix<T>& realMat);


            SparseMatrix(const SparseMatrix& other);
            SparseMatrix(SparseMatrix&& other) noexcept;

            ~SparseMatrix();

            /// ASSIGNMENT
        public:
            SparseMatrix& operator=(const SparseMatrix& other);
            SparseMatrix& operator=(SparseMatrix&& other) noexcept;

            /// DATA ACCESS
        public:

           SparseElement<number> at(count_t row_index, count_t col_index);
            const SparseElement<number> at(count_t row_index, count_t col_index) const;

            SparseElement<number> at(count_t linearIndex);
            const SparseElement<number> at(count_t linearIndex) const;

            arma::SpMat<number>& mat();
            const arma::SpMat<number>& mat() const;


            SparseMatrix<real> re() const;
            SparseMatrix<real> im() const;

            /// MATH OPERATORS
        public:
            template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
            SparseMatrix& operator/=(number1 t);

            template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
            SparseMatrix& operator*=(number1 t);

            template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
            SparseMatrix& operator+=(const SparseMatrix<number1>& other);

            template<typename number1, typename = std::enable_if_t<std::is_same<number, NumberMax_t<number, number1>>::value, int>>
            SparseMatrix& operator-=(const SparseMatrix<number1>& other);

            SparseMatrix operator-() const;
            /// MISC
        public:
            count_t n_rows() const;
            count_t n_cols() const;
            count_t n_elem() const;

			bool hasNan() const;

        private:
            std::unique_ptr<arma::SpMat<number>> mat_;
        };

    }
}

namespace qengine{
    namespace internal{

        // FREE FUNCTIONS
        std::ostream& operator<<(std::ostream& stream,const SparseMatrix<real>& matrix);
        std::ostream& operator<<(std::ostream& stream,const SparseMatrix<complex>& matrix);

		// equality is checked for the full vector: true only if every element is equal (remember limited double-precision)
		template<typename number1, typename number2>
		bool operator== (const SparseMatrix<number1>& left, const SparseMatrix<number2>& right);

		// equality is checked for the full vector: true only if every element is equal (remember limited double-precision)
		template<typename number1, typename number2>
		bool operator!= (const SparseMatrix<number1>& left, const SparseMatrix<number2>& right);

        template<typename number1, typename number2>
        SparseMatrix<NumberMax_t<number1, number2>> operator+ (const SparseMatrix<number1>& left, const SparseMatrix<number2>& right);



        template<typename number1, typename number2>
        SparseMatrix<NumberMax_t<number1, number2>> operator- (const SparseMatrix<number1>& left, const SparseMatrix<number2>& right);



        template<typename number1, typename number2, typename T = std::enable_if_t<is_number_type_v<number1>, number1>>
        SparseMatrix<NumberMax_t<number1, number2>> operator* (number1 left, const SparseMatrix<number2>& right);

        template<typename number1, typename number2, typename T = std::enable_if_t<is_number_type_v<number2>, number2>>
        SparseMatrix<NumberMax_t<number1, number2>> operator* (const SparseMatrix<number1>& left, number2 right);

        template<typename number1, typename number2>
        SparseMatrix<NumberMax_t<number1, number2>> operator* (const SparseMatrix<number1>& left, const SparseMatrix<number2>& right);

        template<typename number1, typename number2, typename T = std::enable_if_t<is_number_type_v<number2>, number2>>
        SparseMatrix<NumberMax_t<number1, number2>> operator/ (const SparseMatrix<number1>& left, number2 right);

        template<typename number1,typename number2>
        SparseMatrix<NumberMax_t<number1,number2>> operator% (const SparseMatrix<number1>& left,const SparseMatrix<number2>& right);

		template<typename number1, typename number2>
	    SparseMatrix<NumberMax_t<number1, number2>> kron(const SparseMatrix<number1>& A, const SparseMatrix<number2>& B);
    }
}

namespace qengine
{
    namespace internal
    {
        using Sp_RMat = SparseMatrix<real>;
        using Sp_CMat = SparseMatrix<complex>;
    }
}

﻿/* COPYRIGHT
 *
 * file="SqDiagMat.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/LinearAlgebra/SqDiagMat.h"

#include <armadillo>

#include "src/Utility/cpp17Replacements/constexpr_ternary.h"

#include "src/Utility/LinearAlgebra/SqSparseMat.h"

namespace qengine {
	namespace internal
	{
		// CTOR/DTOR

		template <typename number>
		SqDiagMat<number>::SqDiagMat(count_t dim) :diag_(dim) {}

		template <typename number>
		SqDiagMat<number>::SqDiagMat(const Vector<number>& diag) : diag_(diag) {}

		template <typename number>
		SqDiagMat<number>::SqDiagMat(Vector<number>&& diag) : diag_(std::move(diag)) {}

		template <typename number>
		template <typename T2, typename T>
		SqDiagMat<number>::SqDiagMat(const SqDiagMat<T>& other):diag_(other.diag())
		{
		}

		template <typename number>
		SqDiagMat<number>& SqDiagMat<number>::operator=(const Vector<number>& diag)
		{
			diag_ = diag;
			return *this;
		}

		template <typename number>
		SqDiagMat<number>& SqDiagMat<number>::operator=(Vector<number>&& diag)
		{
			diag_ = std::move(diag);
			return *this;
		}

		// DATA ACCESS
		template <typename number>
		Vector<number>& SqDiagMat<number>::diag()
		{
			return diag_;
		}

		template <typename number>
		const Vector<number>& SqDiagMat<number>::diag() const
		{
			return diag_;
		}

		// MATH OPERATIONS
		template <typename number>
		void SqDiagMat<number>::multiplyInPlace(CVec& vector) const
		{
			// todo: test if this is as fast as %= from arma. %= does not work with mixed real/complex
			for (auto i = 0u; i < vector.size(); ++i)
			{
				vector.at(i) *= diag_.at(i);
			}
		}

		template <typename number>
		template <typename number1, typename>
		SqDiagMat<number>& SqDiagMat<number>::operator/=(number1 t)
		{
			diag_ /= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqDiagMat<number>& SqDiagMat<number>::operator*=(number1 t)
		{
			diag_ *= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqDiagMat<number>& SqDiagMat<number>::operator+=(number1 t)
		{
			diag_ += t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqDiagMat<number>& SqDiagMat<number>::operator-=(number1 t)
		{
			diag_ -= t;
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqDiagMat<number>& SqDiagMat<number>::operator+=(const SqDiagMat<number1>& other)
		{
			diag_._vec() = diag_._vec()+other.diag()._vec();
			return *this;
		}

		template <typename number>
		template <typename number1, typename>
		SqDiagMat<number>& SqDiagMat<number>::operator-=(const SqDiagMat<number1>& other)
		{
			diag_._vec() = diag_._vec() - other.diag()._vec();
			return *this;
		}

		template <typename number>
		SqDiagMat<number> SqDiagMat<number>::operator-() const
		{
			return SqDiagMat<number>(-diag_);
		}

		template <typename number>
		SqDiagMat<number>::operator SqSparseMat<number>() const
		{
			auto locations = arma::umat(2, dim(), arma::fill::zeros);
			for(auto i = 0ull; i<dim();++i)
			{
				locations.col(i) += i;
			}

			return SqSparseMat<number>(arma::SpMat<number>(locations, diag()._vec()));
		}

		// MISC
		template <typename number>
		count_t SqDiagMat<number>::dim() const
		{
			return diag_.size();
		}

		template<> bool SqDiagMat<real>::isHermitian() const
		{
			return true;
		}

		template<> bool SqDiagMat<complex>::isHermitian() const
		{
			if (arma::sum(arma::abs(arma::imag(diag_._vec()))) < 1e-12)
			{
				return true;
			}
			return false;
		}

		template <typename number>
		number SqDiagMat<number>::at(const count_t nRow, const count_t nCol) const
		{
			if (nRow == nCol) return diag_.at(nRow);
			return 0.0;
		}

		template <typename number>
		bool SqDiagMat<number>::hasNan() const
		{
			return diag_.hasNan();
		}
	}
}
namespace qengine {
	namespace internal
	{
		//// Nonmembers

		template<typename number>
		std::ostream& operator<<(std::ostream& stream, const SqDiagMat<number>& mat){
			return stream<<mat.diag();
		}


		template <typename number1, typename number2>
		SqDiagMat<NumberMax_t<number1, number2>> operator*(const SqDiagMat<number1>& left, const SqDiagMat<number2>& right)
		{
			return SqDiagMat<NumberMax_t<number1, number2>>(Vector<NumberMax_t<number1, number2>>(left.diag()._vec() % right.diag()._vec()));
		}

		template <typename number1, typename number2>
		SqDiagMat<NumberMax_t<number1, number2>> kron(const SqDiagMat<number1>& left, const SqDiagMat<number2>& right)
		{
			return SqDiagMat<NumberMax_t<number1, number2>>(kron(left.diag(), right.diag()));
		}

		template <typename number>
		SqDiagMat<number> exp(const SqDiagMat<number>& d)
		{
			return SqDiagMat<number>{exp(d.diag())};
		}

		template <typename number>
		SqDiagMat<number> pow(const SqDiagMat<number>& d, count_t exponent)
		{
			return SqDiagMat<number>{pow(d.diag(), exponent)};
		}


		template <typename MatNumber>
		Spectrum<complex> getSpectrum(const SqDiagMat<MatNumber>& matrix, const count_t largestEigenstate, const real normalization)
		{
			auto eigenvals = Vector<MatNumber>(largestEigenstate + 1);
			auto eigenvecs = Matrix<MatNumber>(matrix.dim(), largestEigenstate + 1);

			arma::uvec indices = arma::sort_index(matrix.diag()._vec());

			eigenvals._vec() = matrix.diag()._vec().elem(indices.subvec(0, largestEigenstate));
			arma::Mat<MatNumber> x = repl17::constexpr_ternary(std::is_same<MatNumber, real>{},
				[&](auto _)
			{
				// case for reals
				return arma::mat(arma::eye(_(matrix.dim()), matrix.dim()));
			}, 
				[&](auto _)
			{
				// case for complex
				return arma::cx_mat(arma::mat(arma::eye(_(matrix.dim()), matrix.dim())), arma::zeros(matrix.dim(), matrix.dim()));
			});
				
				
			eigenvecs._mat() = x.cols(indices.subvec(0, largestEigenstate));

			return Spectrum<complex>(std::move(eigenvals), std::move(eigenvecs), normalization);
		}

		void eigHerm(const SqDiagMat<real>& mat, Vector<real>& vals, Matrix<real>& vecs, const count_t largestEigenstate)
		{
			arma::uvec indices = arma::sort_index(mat.diag()._vec());

			vals._vec() = mat.diag()._vec().elem(indices.subvec(0, largestEigenstate));
			arma::mat x = arma::eye(mat.dim(), mat.dim());
			vecs._mat() = x.cols(indices.subvec(0, largestEigenstate));
		}

		void eigHerm(const SqDiagMat<complex>& mat, Vector<real>& vals, Matrix<complex>& vecs, const count_t largestEigenstate)
		{
			arma::mat intermediate = arma::real(mat.diag()._vec());

			arma::uvec indices = arma::sort_index(intermediate);

			vals._vec() = arma::real(mat.diag()._vec().elem(indices.subvec(0, largestEigenstate)));
			arma::mat x = arma::eye(mat.dim(), mat.dim());
			vecs._mat() = arma::conv_to<arma::cx_mat>::from(x.cols(indices.subvec(0, largestEigenstate)));
		}

		template <typename MatNumber>
		Spectrum<real> getSpectrum_herm(const SqDiagMat<MatNumber>& matrix, const count_t largestEigenstate, const real normalization)
		{
			auto eigenvals = Vector<real>(largestEigenstate + 1);
			auto eigenvecs = Matrix<MatNumber>(matrix.dim(), largestEigenstate + 1);

			internal::eigHerm(matrix, eigenvals, eigenvecs, largestEigenstate);

			return Spectrum<real>(std::move(eigenvals), std::move(eigenvecs), normalization);
		}
	}
}

// explicit instantiations of SqDiagMat
namespace qengine {
	namespace internal
	{
		template class SqDiagMat<real>;
		template class SqDiagMat<complex>;

		template SqDiagMat<complex>::SqDiagMat(const SqDiagMat<real>&);

		template std::ostream& operator<<(std::ostream& stream, const SqDiagMat<real>& mat);
		template std::ostream& operator<<(std::ostream& stream, const SqDiagMat<complex>& mat);

		template SqDiagMat<real>& SqDiagMat<real>::operator*= (real);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator*= (real);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator*= (complex);

		template SqDiagMat<real>& SqDiagMat<real>::operator/= (real);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator/= (real);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator/= (complex);

		template SqDiagMat<real>& SqDiagMat<real>::operator+= (real);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator+= (real);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator+= (complex);

		template SqDiagMat<real>& SqDiagMat<real>::operator-= (real);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator-= (real);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator-= (complex);

		template SqDiagMat<real>& SqDiagMat<real>::operator+= (const SqDiagMat<real>&);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator+= (const SqDiagMat<real>&);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator+= (const SqDiagMat<complex>&);

		template SqDiagMat<real>& SqDiagMat<real>::operator-= (const SqDiagMat<real>&);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator-= (const SqDiagMat<real>&);
		template SqDiagMat<complex>& SqDiagMat<complex>::operator-= (const SqDiagMat<complex>&);

		template SqDiagMat<real>    operator* (const SqDiagMat<real>&     left, const SqDiagMat<real>& right);
		template SqDiagMat<complex> operator* (const SqDiagMat<complex>&  left, const SqDiagMat<real>& right);
		template SqDiagMat<complex> operator* (const SqDiagMat<real>&     left, const SqDiagMat<complex>& right);
		template SqDiagMat<complex> operator* (const SqDiagMat<complex>&  left, const SqDiagMat<complex>& right);

		template SqDiagMat<real>    kron (const SqDiagMat<real>&     left, const SqDiagMat<real>& right);
		template SqDiagMat<complex> kron(const SqDiagMat<complex>&  left, const SqDiagMat<real>& right);
		template SqDiagMat<complex> kron(const SqDiagMat<real>&     left, const SqDiagMat<complex>& right);
		template SqDiagMat<complex> kron(const SqDiagMat<complex>&  left, const SqDiagMat<complex>& right);

		template SqDiagMat<real> exp(const SqDiagMat<real>& d);
		template SqDiagMat<complex> exp(const SqDiagMat<complex>& d);

		template Spectrum<complex> getSpectrum(const SqDiagMat<real>& matrix, count_t largestEigenstate, const real normalization);
		template Spectrum<complex> getSpectrum(const SqDiagMat<complex>& matrix, count_t largestEigenstate, const real normalization);

		template Spectrum<real> getSpectrum_herm(const SqDiagMat<real>& matrix, count_t largestEigenstate, const real normalization);
		template Spectrum<real> getSpectrum_herm(const SqDiagMat<complex>& matrix, count_t largestEigenstate, const real normalization);


		template SqDiagMat<real> pow(const SqDiagMat<real>&, count_t);
		template SqDiagMat<complex> pow(const SqDiagMat<complex>&, count_t);
	}
}

﻿/* COPYRIGHT
 *
 * file="Vector.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

/*
 * Our basic vector-class. Based on arma::Col through PImpl-idiom (if that sounds weird, google it or "opaque pointer").
 * Contains most of the same functionality as the arma::Col. In the bottom are free functions for operators, some
 * factories and basic math-functions
 */

#include <memory>
#include <vector>

#include "src/NumberTypes.h"
#include "src/Utility/NumberTypeOperations.h"
#include "src/Utility//TypeList.h"

namespace arma
{
	template<class> class Col;
}

namespace qengine
{
	template<typename number>
	class Vector
	{
		// CTOR/DTOR
	public:

		Vector();
		explicit Vector(count_t size);
		explicit Vector(count_t size, number fill);
		explicit Vector(count_t size, number* data);
		explicit Vector(const arma::Col<number>& data);
		explicit Vector(arma::Col<number>&& data);
		Vector(std::initializer_list<number> data);
		explicit Vector(std::vector<number> data);



		Vector(const Vector& other);
		Vector(Vector&& other) noexcept;

		template<typename T2 = number, typename T,typename SFINAE = std::enable_if_t<std::is_same<T2, complex>::value && std::is_same<T, real>::value>>
		Vector(const Vector<T>& other);

		template<typename T2 = number, typename T = std::enable_if_t<std::is_same<T2, complex>::value, complex>>
		Vector(const Vector<real>& re,const Vector<real>& im);

		~Vector();

		// ASSIGNMENT
	public:

		Vector& operator=(const Vector& other) &;
		Vector& operator=(Vector&& other) & noexcept;

		//template<typename T2 = number, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
		//Vector& operator=(const Vector<T>& other);

		// DATA ACCESS
	public:
		number& at(count_t index);
		const number& at(count_t index) const;

		number& front();
		const number& front() const;

		number& back();
		const number& back() const;


		Vector<real> re() const;
		Vector<real> im() const;



		// MATH OPERATORS
	public:
		template<typename number1, typename = std::enable_if_t<std::is_same<number, internal::NumberMax_t<number, number1>>::value, int>>
		Vector& operator/=(number1 t);

		template<typename number1, typename = std::enable_if_t<std::is_same<number, internal::NumberMax_t<number, number1>>::value, int>>
		Vector& operator*=(number1 t);

		template<typename number1, typename = std::enable_if_t<std::is_same<number, internal::NumberMax_t<number, number1>>::value, int>>
		Vector& operator+=(number1 t);

		template<typename number1, typename = std::enable_if_t<std::is_same<number, internal::NumberMax_t<number, number1>>::value, int>>
		Vector& operator-=(number1 t);

		Vector& operator+=(const Vector<number>& t);

		Vector& operator-=(const Vector<number>& t);

		Vector operator-() const;
		// MISC
	public:
		count_t size() const;
		count_t dim() const; // this simply forwards to size()
		real norm() const;

		Vector<real> absSquare() const;
		Vector<number>& append(const Vector<number>& tail);
		Vector<number>& glue(const Vector<number>& tail);

		bool hasNan() const;

		void resize(count_t newSize);

		/// INTERNAL USE
	public:
		number* _data();
		const number* _data() const;

		arma::Col<number>& _rep()&;
		const arma::Col<number>& _rep() const&;
		arma::Col<number>&& _rep() &&;

		arma::Col<number>& _vec() &;
		const arma::Col<number>& _vec() const&;
		arma::Col<number>&& _vec() &&;


	private:
		std::unique_ptr<arma::Col<number>> vec_;

	};

	// These functions are so we can internally construct vectors more easily, although I have not yet been successful in using them. Blah.
	Vector<real> makeVector(const arma::Col<real>& vec);
	Vector<real> makeVector(arma::Col<real>&& vec);

	Vector<complex> makeVector(const arma::Col<complex>& vec);
	Vector<complex> makeVector(arma::Col<complex>&& vec);

	Vector<complex> makeCVec(const Vector<real>& re, const Vector<real>& im);
}

namespace qengine 
{
	class SerializedObject;

		template<class number>
		SerializedObject toSerializedObject(const Vector<number>& vec);

		template<class number>
		Vector<number> fromSerializedObject(const SerializedObject& obj, internal::Type<Vector<number>>);
}

// operations on vectors
namespace qengine
{
	std::ostream& operator<<(std::ostream& stream, const Vector<real>& vector);
	std::ostream& operator<<(std::ostream& stream, const Vector<complex>& vector);

	// equality is checked for the full vector: true only if every element is equal (remember limited double-precision)
	template<typename number1, typename number2>
	bool operator== (const Vector<number1>& left, const Vector<number2>& right);

	// equality is checked for the full vector: true only if every element is equal (remember limited double-precision)
	template<typename number1, typename number2>
	bool operator!= (const Vector<number1>& left, const Vector<number2>& right);

	template<typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator+ (const Vector<number1>& left, const Vector<number2>& right);
	
	template<typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator- (const Vector<number1>& left, const Vector<number2>& right);

	// element-wise multiplication of vectors
	template<typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator* (const Vector<number1>& left, const Vector<number2>& right);

	// element-wise division of vectors
	template<typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> operator/ (const Vector<number1>& left, const Vector<number2>& right);


	template<typename number1, typename number2>
	Vector<internal::NumberMax_t<number1, number2>> kron(const Vector<number1>& left, const Vector<number2>& right);

	template<typename number1, typename number2>
	internal::NumberMax_t<number1, number2> dot(const Vector<number1>& left, const Vector<number2>& right);
	
	template<typename number2>
	complex cdot(const Vector<complex>& left, const Vector<number2>& right);

	template<typename number>
	number normDot(const Vector<number>& left, const Vector<number>& right);

	template<typename number>
	number sum(const Vector<number>& vector);

	template<typename number>
	number trapz(const Vector<number>& vector);
	
	template<typename number>
	Vector<number> append(Vector<number> head, const Vector<number>& tail);

	template<typename number>
	Vector<number> glue(Vector<number> head, const Vector<number>& tail);

	template<typename number>
	Vector<number> invert(const Vector<number>& vec);


	template<typename number>
	Vector<number> conj(const Vector<number>& vec);

	template<typename number>
	bool approxEqual(const Vector<number>& left, const Vector<number>& right, real absTol);

	template<typename number>
	number mean(const Vector<number>& v);

	template<typename number>
	number max(const Vector<number>& v);

	template<typename number>
	number min(const Vector<number>& v);


}

namespace qengine
{
	namespace internal
	{
		template<typename number1, typename number2>
		Vector<internal::NumberMax_t<number1, number2>> add(const Vector<number1>& vector, number2 scalar);

		template<typename number1, typename number2>
		Vector<internal::NumberMax_t<number1, number2>> multiply(number2 scalar, const Vector<number1>& vector);

		template<typename number>
		Vector<number> invert(const Vector<number>& vector);
	}

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator+ (const Vector<number1>& vector, number2 scalar)
	{
		return internal::add(vector, internal::toNumber(scalar));
	}

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator+ (number2 scalar, const Vector<number1>& vector)
	{
		return internal::add(vector, internal::toNumber(scalar));
	}

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator- (const Vector<number1>& vector, number2 scalar)
	{
		return internal::add(vector, -internal::toNumber(scalar));
	}

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator- (number1 scalar, const Vector<number2>& vector)
	{
		return internal::add(-vector, internal::toNumber(scalar));
	}

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator* (number1 scalar, const Vector<number2>& vector)
	{
		return internal::multiply(internal::toNumber(scalar), vector);
	}

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator* (const Vector<number1>& vector, number2 scalar)
	{
		return internal::multiply(internal::toNumber(scalar), vector);
	}

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator/ (const Vector<number2>& vector, number1 scalar)
	{
		return internal::multiply(1.0 / internal::toNumber(scalar), vector);
	}

	template<typename number1, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator/ (number2 scalar, const Vector<number1>& vector)
	{
		return internal::multiply(internal::toNumber(scalar), internal::invert(vector));
	}
}

namespace qengine
{
	template<typename number>
	Vector<number> exp(const Vector<number>& vector);

	template<typename number>
	Vector<number> pow(const Vector<number>& vector, count_t exponent);

	template<typename number>
	Vector<number> exp2(const Vector<number>& vector);

	template<typename number>
	Vector<number> exp10(const Vector<number>& vector);

	template<typename number>
	Vector<number> log(const Vector<number>& vector);

	template<typename number>
	Vector<number> log2(const Vector<number>& vector);

	template<typename number>
	Vector<number> log10(const Vector<number>& vector);

	template<typename number>
	Vector<number> sqrt(const Vector<number>& vector);

	template<typename number>
	Vector<real> abs(const Vector<number>& vector);

	Vector<real> re(const Vector<complex>& vector);
	Vector<real> im(const Vector<complex>& vector);


}
namespace qengine
{
	template<typename number>
	Vector<number> sin(const Vector<number>& vector);

	template<typename number>
	Vector<number> cos(const Vector<number>& vector);

	template<typename number>
	Vector<number> tan(const Vector<number>& vector);

	template<typename number>
	Vector<number> asin(const Vector<number>& vector);

	template<typename number>
	Vector<number> acos(const Vector<number>& vector);

	template<typename number>
	Vector<number> atan(const Vector<number>& vector);

	template<typename number>
	Vector<number> sinh(const Vector<number>& vector);

	template<typename number>
	Vector<number> cosh(const Vector<number>& vector);

	template<typename number>
	Vector<number> tanh(const Vector<number>& vector);

	template<typename number>
	Vector<number> asinh(const Vector<number>& vector);

	template<typename number>
	Vector<number> acosh(const Vector<number>& vector);

	template<typename number>
	Vector<number> atanh(const Vector<number>& vector);
}


// aliases
namespace qengine
{
	using RVec = Vector<real>;
	using CVec = Vector<complex>;
}

//special construction functions
namespace qengine
{
	RVec linspace(real lower, real upper, count_t numberOfPoints);

    RVec step(const RVec& x, real stepPos);
	RVec box(const RVec& x, real left, real right);
    RVec well(const RVec& x, real left, real right);

    // s for "sigmoid"
	RVec sstep(const RVec& x, real center, real hardness);
	RVec sbox(const RVec& x, real left, real right, real hardness);
    RVec swell(const RVec& x, real left, real right, real hardness);
}

#pragma once

namespace qengine
{
	namespace internal
	{
		template<class T, class... Ts>
		decltype(auto) getFirst(T&& t, Ts&&...)
		{
			return std::forward<T>(t);
		}
	}
}
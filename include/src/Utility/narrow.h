﻿/* COPYRIGHT
 *
 * file="narrow.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>
#include "src/Utility/qengine_assert.h"

#define STRINGIFY(x) #x

#define DISABLE_WARNING_SIGN_COMPARE_MSVC(line)\
	__pragma(warning(push))\
	__pragma(warning(disable:4389))\
	line \
	__pragma(warning(pop))

namespace qengine
{
	template<class T_to, class T_from, class = std::enable_if_t<std::is_arithmetic<T_to>::value && std::is_arithmetic<T_from>::value>>
	T_to narrow(T_from from)
	{
		auto retval = static_cast<T_to>(from);

		// moving this into a macro does not work for g++, due to g++ bug 53431 (https://gcc.gnu.org/bugzilla/show_bug.cgi?id=53431)
		// replace when that has been fixed (read: never -,-)
#ifdef __GNUC__
		_Pragma(STRINGIFY(GCC diagnostic push))
			_Pragma(STRINGIFY(GCC diagnostic ignored "-Wsign-compare"))
			qengine_assert(retval == from, "can only narrow if the result is the same as the original");
		_Pragma(STRINGIFY(GCC diagnostic pop))

#else
		DISABLE_WARNING_SIGN_COMPARE_MSVC
		(
			qengine_assert(retval == from, "can only narrow if the result is the same as the original");
		)
#endif



			return retval;
	}
}

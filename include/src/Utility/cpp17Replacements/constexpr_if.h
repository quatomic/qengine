﻿/* COPYRIGHT
 *
 * file="constexpr_if.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

namespace qengine
{
	namespace repl17
	{
		namespace detail
		{
			template<class TrueBranch, class FalseBranch>
			void constexpr constexpr_if_invoke(const std::true_type&, const TrueBranch& trueBranch, const FalseBranch&)
			{
				trueBranch([](const auto& t) {return t; });
			}

			template<class TrueBranch, class FalseBranch>
			void constexpr constexpr_if_invoke(const std::false_type&, const TrueBranch&, const FalseBranch& falseBranch)
			{
				falseBranch([](const auto& t) {return t; });
			}
		}

		template<class Condition, class TrueBranch>
		void constexpr constexpr_if(const Condition&, const TrueBranch& trueBranch)
		{
			detail::constexpr_if_invoke(std::integral_constant<bool, Condition::value>{}, trueBranch, [](const auto&){});
		}


	}
}

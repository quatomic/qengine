﻿/* COPYRIGHT
 *
 * file="check_all_true.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

namespace qengine
{
	namespace repl17
	{
		// check that all the values passed in are true. Assumes the passed-in types can be converted to boolean. Note, that c++17 allows fold-expressions that make this redundant
		template<typename ... V>
		constexpr bool check_all_true(const V &... v) {
			bool result = true;
			(void)std::initializer_list<int>{ (result = result && v, 0)... };
			return result;
		}

	}
}

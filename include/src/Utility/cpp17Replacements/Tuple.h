﻿/* COPYRIGHT
 *
 * file="Tuple.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>


#include "src/Utility/cpp17Replacements/check_all_true.h"
#include "src/Utility/stdTupleFromVec.h"

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/std17/remove_cvref.h"
#include "src/Utility/std17/apply.h"
#include "src/Utility/apply_vec.h"

namespace qengine
{
	// an adapter that can construct a tuple implicitly from variables
	// cpp17: tuples are constructed implicitly in c++17 so this can be removed
	template<class... Ts>
	struct Tuple
	{
		Tuple(){}
		Tuple(Ts... ts) : data(std::make_tuple(ts...)) {}
		Tuple(std::tuple<Ts...>&& ts) : data(std::move(ts)) {}
		Tuple(const std::tuple<Ts...>& ts) : data(ts) {}

		template<class Val, typename = std::enable_if_t<repl17::check_all_true(std::is_same<Val, Ts>::value...)>>
		Tuple(Vector<Val>&& vec) : data(internal::stdTupleFromVec<Ts...>(vec)) {}

		constexpr static size_t size()
		{
			return sizeof...(Ts);
		}

		using Tuple_t = std::tuple<Ts...>;
		std::tuple<Ts...> data;
	};

	template<class... Ts>
	Tuple<std17::remove_cvref_t<Ts>...> makeTuple(Ts&&... ts)
	{
		return { std::make_tuple(ts...) };
	}

	namespace internal
	{
		template<class F, class Tuple>
		constexpr decltype(auto) apply(F&& f, Tuple&& t)
		{
			return std17::apply(std::forward<F>(f), std::forward<Tuple>(t).data);
		}
		
	}

	namespace internal
	{
		template<std::size_t I_elem, class F, class... Tuples>
		constexpr auto applyToEach_bit(F&& f, Tuples&&... ts)
		{
			return std::forward<F>(f)(std::get<I_elem>(ts.data)...);
		}

		template<class F, std::size_t... I_elem, class... Tuples>
		constexpr auto applyToEach_impl(F&& f, std::index_sequence<I_elem...>, Tuples&&... ts)
		{
			return qengine::makeTuple(applyToEach_bit<I_elem>(std::forward<F>(f), std::forward<Tuples>(ts)...)...);
		}

		template<class T, class... Ts>
		constexpr decltype(auto) getFirstElementSize()
		{
			return std::tuple_size<typename T::Tuple_t>();
		}

		template<class F, class... Tuples>
		constexpr auto applyToEach(F&& f, Tuples&&... ts)
		{
			static_assert(sizeof...(Tuples) > 0, "");
			constexpr size_t size = internal::getFirstElementSize<std::decay_t<Tuples>...>();

			return applyToEach_impl(std::forward<F>(f), std::make_index_sequence<size>(), std::forward<Tuples>(ts)...);
		}
	}

	namespace internal
	{
		template<class... Params, class Vec >
		auto tupleFromVec(Vec&& v)
		{
			auto constructTuple = [](auto... ts) {return Tuple<Params...>(ts...); };
			return apply_vec<sizeof...(Params)>(constructTuple, v);
		}
	}

}

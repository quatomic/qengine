﻿/* COPYRIGHT
 *
 * file="constexpr_ternary.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


namespace qengine
{
	namespace repl17
	{
		namespace detail
		{
			struct ForwardSame
			{
				template<class T>
				constexpr decltype(auto) operator() (T&& t)
				{
					return std::forward<T>(t);
				}
			};


			template<class TrueBranch, class FalseBranch>
			decltype(auto) constexpr constexpr_ternary_invoke(const std::true_type&, const TrueBranch& trueBranch, const FalseBranch&)
			{
				return trueBranch(ForwardSame());
			}

			template<class TrueBranch, class FalseBranch>
			decltype(auto) constexpr constexpr_ternary_invoke(const std::false_type&, const TrueBranch&, const FalseBranch& falseBranch)
			{
				return falseBranch(ForwardSame());
			}
		}
		template<class Condition, class TrueBranch, class FalseBranch>
		decltype(auto) constexpr constexpr_ternary(const Condition&, const TrueBranch& trueBranch, const FalseBranch& falseBranch)
		{
			return detail::constexpr_ternary_invoke(std::integral_constant<bool, Condition::value>{}, trueBranch, falseBranch);
		}
	}
}

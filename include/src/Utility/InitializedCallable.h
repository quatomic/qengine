﻿/* COPYRIGHT
 *
 * file="InitializedCallable.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>

#include "src/Utility/is_callable.h"

#include "src/Utility/cpp17Replacements/Tuple.h"


#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/qengine_assert.h"

namespace qengine
{
	namespace internal
	{
		template<class Function, class... Params>
		class InitializedCallable
		{
		public:
			using RetType = decltype(std::declval<Function>()(std::declval<Params>()...));

			template<class... Ts>
			explicit InitializedCallable(Function functor, Ts&&... params)
				: functor_(functor),
				initialParams_(std::make_tuple<Ts...>(std::forward<Ts>(params)...))
			{
			}

			template<class... Ts, typename = std::enable_if_t<is_callable_v<Function, RetType(Ts...)>>>
			RetType operator() (Ts&&... ts) const { return functor_(std::forward<Ts>(ts)...); }

			template<class... Ts, typename = std::enable_if_t<is_callable_v<Function, RetType(Ts...)>>>
			RetType operator() (const Tuple<Ts...>& ts) const {
				return internal::apply(functor_, ts);
			}

			template<class Val, typename = std::enable_if_t<std::is_same<Val, real>::value && repl17::check_all_true(std::is_same<Val, Params>::value...)>>
			RetType operator() (const Vector<Val>& vec) const
			{
				qengine_assert(vec.size() == sizeof...(Params), "wrong number of arguments in vector");
				auto tuple = internal::tupleFromVec<Params...>(vec);
				return operator()(tuple);
			}

			template<class Val, typename = std::enable_if_t<std::is_same<Val, real>::value && repl17::check_all_true(std::is_same<Val, Params>::value...)>>
			RetType operator() (const Tuple<Vector<Val>>& vectup) const
			{
				qengine_assert(std::get<0>(vectup.data).size() == sizeof...(Params), "wrong number of arguments in vector");
				auto tuple = internal::tupleFromVec<Params...>(std::get<0>(vectup.data));
				return operator()(tuple);
			}

			const Function& functor() const { return functor_; }
			const Tuple<Params...>& initialParams() const { return initialParams_; }

		private:

			Function functor_;
			Tuple<Params...> initialParams_;
		};
	}
}

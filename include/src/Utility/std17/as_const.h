﻿/* COPYRIGHT
 *
 * file="as_const.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


namespace std17
{
	/// Forms lvalue reference to const type of t -- manually implemented here because apple clang does not yet support c++17 feaature std::as_const

	template <class T>
	constexpr std::add_const_t<T>& as_const(T& t) noexcept
	{
		return t;
	}

	template <class T>
	void as_const(const T&&) = delete;
}

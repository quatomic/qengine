﻿/* COPYRIGHT
 *
 * file="is_same_v.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


namespace std17
{
	template<class T1, class T2>
	constexpr bool is_same_v = std::is_same<T1, T2>::value;
}

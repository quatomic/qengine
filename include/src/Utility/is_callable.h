﻿/* COPYRIGHT
 *
 * file="is_callable.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <type_traits>

#include "has_function.h"

namespace qengine
{
	MAKE_HAS_FUNCTION_RETURN_QUALIFIED(is_callable_ret_qualified, operator());

	//MAKE_HAS_FUNCTION(is_callable, operator());


	template<typename, typename T>
	struct is_callable {
		static_assert(
			std::integral_constant<T, false>::value,
			"Second template parameter needs to be of function type.");
	};

	template<typename C, typename Ret, typename... Args>
	struct is_callable <C, Ret(Args...)> {
	private:
		template<typename T, typename = std::enable_if_t<std::is_same<decltype(std::declval<T>()(std::declval<Args>()...)), Ret>::value>>
		static constexpr bool check(T*) { return true; }

		template<typename>
		static constexpr bool check(...) { return false; };

	public:
		static constexpr bool value = check<C>(0);
	};
	template<typename T1, typename T2>
	static constexpr bool is_callable_v = is_callable<T1, T2>::value;


	template<typename T>
	struct is_callable_with_anything {
	private:
		typedef char(&yes)[1];
		typedef char(&no)[2];

		struct Fallback { void operator()(); };
		struct Derived : T, Fallback { };

		template<typename U, U> struct Check;

		template<typename>
		static yes test(...);

		template<typename C>
		static no test(Check<void (Fallback::*)(), &C::operator()>*);

	public:
		static const bool value = sizeof(test<Derived>(0)) == sizeof(yes);
	};
}

﻿/* COPYRIGHT
 *
 * file="TypeList.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <type_traits>

namespace qengine
{
	namespace internal
	{
		template<class... Ts> struct TypeList {};

		template<class>
		struct Type{};
	}
}

namespace qengine
{
	namespace internal
	{
		namespace detail 
		{
			template<size_t I, class ToFind>
			constexpr size_t findFirstIndex_impl()
			{
				return I;
			}

			template<size_t I, class ToFind, class T, class... Ts>
			constexpr size_t findFirstIndex_impl()
			{
				return std::is_same<ToFind, T>::value ? I : findFirstIndex_impl<I + 1, ToFind, Ts...>();
			}

			template<class ToFind, class... Ts>
			constexpr size_t findFirstIndex_impl()
			{
				return findFirstIndex_impl<0, ToFind, Ts...>();
			}
		}

		template<class ToFind, class... Ts>
		constexpr size_t findFirstIndex(TypeList<Ts...>)
		{
			return detail::findFirstIndex_impl<ToFind, Ts...>();
		}

		template<class ToFind, class... Ts>
		constexpr size_t findFirstIndex()
		{
			return detail::findFirstIndex_impl<ToFind, Ts...>();
		}
	}
}

namespace qengine
{
	namespace internal
	{
		template<class ToFind, class... Ts>
		constexpr bool isInList(TypeList<Ts...> l)
		{
			return findFirstIndex<ToFind>(l) < sizeof...(Ts);
		}
		template<class ToFind, class... Ts>
		constexpr bool isInList()
		{
			return findFirstIndex<ToFind>(TypeList<Ts...>{}) < sizeof...(Ts);
		}
	}
}

namespace qengine
{
	namespace internal 
	{
		namespace detail
		{
			template<class T>
			constexpr bool containsNoDuplicates_impl()
			{
				return true;
			}

			template<class T, class T2,class... Ts>
			constexpr bool containsNoDuplicates_impl()
			{
				return !isInList<T>(TypeList<T2, Ts...>{}) && containsNoDuplicates_impl<T2, Ts...>();
			}
		}

		template<class... Ts>
		constexpr bool containsNoDuplicates(TypeList<Ts...>)
		{
			return detail::containsNoDuplicates_impl<Ts...>();
		}
		template<class... Ts>
		constexpr bool containsNoDuplicates()
		{
			return detail::containsNoDuplicates_impl<Ts...>();
		}
	}
}

namespace qengine
{
	namespace internal 
	{
		template<class... Ts>
		constexpr auto size(TypeList<Ts...>)
		{
			return sizeof...(Ts);
		}

		template<class... Ts>
		constexpr bool isEmpty(TypeList<Ts...>)
		{
			return sizeof...(Ts) == 0u;
		}


	}
}

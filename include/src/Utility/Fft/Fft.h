﻿/* COPYRIGHT
 *
 * file="Fft.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <memory>
#include <vector>

#include "src/NumberTypes.h"

namespace qengine
{
	template<class> class Vector;
	namespace internal
	{
		class FFT
		{
			struct fftw_plan_wrapper;
		public:
			explicit FFT(Vector<complex>& data);
			explicit FFT(Vector<complex>& data, std::vector<count_t> dims);
			explicit FFT(Vector<complex>& data, std::vector<int> dims);

			FFT(FFT&& other) noexcept;
			FFT(const FFT& other);

			FFT& operator=(const FFT& other) = delete;
			FFT& operator=(FFT&& other) noexcept = delete;

			~FFT();

			void doFft() const;
			void doInverseFft() const;

			const Vector<complex>& data() const { return data_; };

		private:
			std::unique_ptr<fftw_plan_wrapper> impl_;

			Vector<complex>& data_;
			std::vector<int> dims_;
		};
	}
}

﻿/* COPYRIGHT
 *
 * file="Fft.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/Fft/Fft.h"

#include <mutex>
#include <fftw/fftw3.h>

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/NumberTypes.h"
#include "src/Utility/narrow.h"

namespace qengine
{
	namespace internal
	{
		struct FFT::fftw_plan_wrapper
		{
			fftw_plan p_fft;
			fftw_plan p_ifft;
		};


		FFT::FFT(Vector<complex>& data):
		FFT(data, std::vector<int>{narrow<int>(data.size())})
		{
		}

		std::vector<int> dimsToInt(const std::vector<count_t>& dims)
		{
			std::vector<int> retval;
			retval.reserve(dims.size());
			for(const auto& dim:dims)
			{
				retval.push_back(narrow<int>(dim));
			}
			return retval;
		}

		FFT::FFT(Vector<complex>& data, std::vector<count_t> dims) :
			FFT(data, dimsToInt(dims))
		{
		}

		FFT::FFT(Vector<complex>& data, std::vector<int> dims) :
			impl_(std::make_unique<fftw_plan_wrapper>()),
			data_(data),
			dims_(std::move(dims))
		{
			static std::mutex fftwCreationMutex;
			std::lock_guard<std::mutex> lock(fftwCreationMutex);

			auto* dataPointer = reinterpret_cast<fftw_complex*> (data_._data());

			impl_->p_fft  = fftw_plan_dft(narrow<int>(dims_.size()), dims_.data(), dataPointer, dataPointer, FFTW_FORWARD,  FFTW_ESTIMATE_PATIENT);
			impl_->p_ifft = fftw_plan_dft(narrow<int>(dims_.size()), dims_.data(), dataPointer, dataPointer, FFTW_BACKWARD, FFTW_ESTIMATE_PATIENT);
		}

		FFT::FFT(FFT&& other) noexcept:
			FFT(other.data_, other.dims_)
		{

		}

		FFT::FFT(const FFT& other) :
			FFT(other.data_, other.dims_)
		{

		}

		FFT::~FFT()
		{
			fftw_destroy_plan(impl_->p_fft);
			fftw_destroy_plan(impl_->p_ifft);
		}

		void FFT::doFft() const 
		{
			fftw_execute(impl_->p_fft);
		}

		void FFT::doInverseFft() const
		{
			fftw_execute(impl_->p_ifft);

			const real multiplier = 1.0 / data_.size();

			data_ *= multiplier;
		}
	}
}

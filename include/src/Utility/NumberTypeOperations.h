﻿/* COPYRIGHT
 *
 * file="NumberTypeOperations.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/NumberTypes.h"

namespace qengine
{
	namespace internal
	{
		/*
		 * check, if a type is one of our fundamental number-types. (real or complex)
		 */

		template<typename> struct is_number_type { constexpr static bool value = false; };
		template<typename T> constexpr static bool is_number_type_v = is_number_type<T>::value;

		template<> struct is_number_type<real> { constexpr static bool value = true; };
		template<> struct is_number_type<complex> { constexpr static bool value = true; };
	}
}

namespace qengine
{
	namespace internal
	{
		/*
		 * conversions to number-types. Allow us to make operations that take e.g. ints by converting them to reals first
		 */

		inline constexpr real toNumber(const real& t)
		{
			return t;
		}

		inline constexpr complex toNumber(const complex& t)
		{
			return t;
		}

		template<class T>
		constexpr std::enable_if_t<std::is_convertible<T, real>::value && !(std::is_same<T, real>::value), real> toNumber(const T& t)
		{
			return static_cast<real>(t);
		}

		template<class T>
		constexpr std::enable_if_t<std::is_convertible<T, complex>::value && !std::is_same<T, complex>::value && !std::is_convertible<T, real>::value, complex> toNumber(const T& t)
		{
			return static_cast<complex>(t);
		}

		template<typename T>
		struct is_convertible_to_number_type
		{
			constexpr static bool value = std::is_convertible<T, real>::value || std::is_convertible<T, complex>::value;
		};

		template<typename T>
		constexpr static bool is_convertible_to_number_type_v = is_convertible_to_number_type<T>::value;
	}
}

namespace qengine
{
	namespace internal
	{
		/*
		 * Check two types for "size", where complex > real. For operations where need to convert one of two objects so that they fit
		 */

		template<typename Left, typename Right>
		struct NumberMin;

		template<typename Left, typename Right>
		using NumberMin_t = typename NumberMin<Left, Right>::type;

		template<> struct NumberMin<real, real> { using type = real; };
		template<> struct NumberMin<real, complex> { using type = real; };
		template<> struct NumberMin<complex, real> { using type = real; };
		template<> struct NumberMin<complex, complex> { using type = complex; };

		template<template<class> class T, class Left, class Right>
		struct NumberMin<T<Left>, T<Right>> { using type = T<NumberMin_t<Left, Right>>; };
	}

}

namespace qengine
{

	namespace internal
	{
		/*
		* Check two types for "size", where complex > real. For operations where need to convert one of two objects so that they fit
		*/

		template<typename Left, typename Right, class SFINAE = void>
		struct NumberMax;

		template<typename Left, typename Right>
		using NumberMax_t = typename NumberMax<Left, Right>::type;

		template<> struct NumberMax<real, real> { using type = real; };
		template<> struct NumberMax<real, complex> { using type = complex; };
		template<> struct NumberMax<complex, real> { using type = complex; };
		template<> struct NumberMax<complex, complex> { using type = complex; };

		template<class Left, class Right>
		struct NumberMax < Left, Right, std::enable_if_t<is_convertible_to_number_type_v<Left>&& is_convertible_to_number_type_v<Right>>>
		{
			using type = NumberMax_t<decltype(toNumber(std::declval<Left>())), decltype(toNumber(std::declval<Right>()))>;
		};

		template<template<class> class T, class Left, class Right>
		struct NumberMax<T<Left>, T<Right>> { using type = T<NumberMax_t<Left, Right>>; };


	}
}

namespace qengine
{
	namespace internal
	{
		/*
		 * utility to get the numbertype of a template on that type. 
		 */

		template<class>
		struct getNumType;

		template<class T>
		using getNumType_t = typename getNumType<T>::type;

		template<template<typename> class X, typename number>
		struct getNumType<X<number>> { using type = number; };
	}
}

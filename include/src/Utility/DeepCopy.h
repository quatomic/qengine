﻿/* COPYRIGHT
 *
 * file="DeepCopy.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <memory>
#include <vector>
#include <tuple>

namespace qengine
{
	namespace internal
	{
		// deep copy of unique_ptr to abstract base class. requires clone() to be implemented in hierarchy.
		template<class T, std::enable_if_t<std::is_abstract<T>::value, int> = 0>
		std::unique_ptr<T> deep_copy(const std::unique_ptr<T>& t_ptr)
		{
			return t_ptr->clone();
		}

		// deep copy of non-abstract type. Calls copy-ctor, so must be copyable type. Watch out for slicing in deep hierarchies.
		template<class T, std::enable_if_t<!std::is_abstract<T>::value, int> = 0>
		std::unique_ptr<T> deep_copy(const std::unique_ptr<T>& t_ptr)
		{
			if(t_ptr) return std::make_unique<T>(*t_ptr);
			// deep copy of nullptr is nullptr
			return std::unique_ptr<T>(nullptr);
		}

		template<class T>
		std::vector<std::unique_ptr<T>> deep_copy(const std::vector<std::unique_ptr<T>>& t_vec)
		{
			std::vector<std::unique_ptr<T>> v;
			for(const auto& x:t_vec)
			{
				v.push_back(deep_copy(x));
			}
			return v;
		}

		template<class... Ts, std::size_t... I>
		std::tuple<std::unique_ptr<Ts>...> deep_copy(const std::tuple<std::unique_ptr<Ts>...>& tuple, std::index_sequence<I...>)
		{
			return std::make_tuple<std::unique_ptr<Ts>...>(deep_copy(std::get<I>(tuple))...);
		}
	}
}

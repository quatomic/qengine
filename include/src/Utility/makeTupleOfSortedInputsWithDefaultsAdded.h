﻿/* COPYRIGHT
 *
 * file="makeTupleOfSortedInputsWithDefaultsAdded.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>

#include "src/NumberTypes.h"

#include "src/Utility/cpp17Replacements/constexpr_ternary.h"
#include "src/Utility/std17/bool_constant.h"

#include "src/Utility/TypeMap.h"

namespace qengine
{
	namespace internal
	{
		namespace detail {
			template<class Key, class Map>
			constexpr auto contains(const Map&)
			{
				return std17::bool_constant<Map::template contains<Key>()>();
			}

			template<class CurrentKey, class MapOfDefaults, class MapOfInputs>
			auto getValuePreferInput(const MapOfDefaults& defaults, const MapOfInputs& inputs)
			{
				return repl17::constexpr_ternary(contains<CurrentKey>(inputs),
					[&](auto _)
				{
					return get<CurrentKey>(_(inputs));
				},
					[&](auto _)
				{
					return get<CurrentKey>(_(defaults));
				});
			}

			template<class... OrderedKeys, class MapOfDefaults, class MapOfInputs>
			auto sortAndAddDefaults_impl(const MapOfDefaults& defaults, const MapOfInputs& inputs)
			{
				return std::make_tuple(getValuePreferInput<OrderedKeys>(defaults, inputs)...);
			}
		}

		template<class MapOfDefaults, class... InputKeys, class... InputValues>
		constexpr bool areInputKeysValid( Type<MapOfDefaults>, Type<TypeMap<std::tuple<InputKeys...>, std::tuple<InputValues...>>>)
		{
			return repl17::check_all_true(MapOfDefaults::template contains<InputKeys>()...); //check, that each of the keys is contained in the map of defaults
		}

		template<class... DefaultKeys, class... DefaultValues, class... InputKeys, class... InputValues >
		auto makeTupleOfSortedInputsWithDefaultsAdded(const TypeMap<std::tuple<DefaultKeys...>, std::tuple<DefaultValues...>>& defaults, const TypeMap<std::tuple<InputKeys...>, std::tuple<InputValues...>>& inputs)
		{
			using DefaultMap = TypeMap<std::tuple<DefaultKeys...>, std::tuple<DefaultValues...>>;
			using InputMap = TypeMap<std::tuple<InputKeys...>, std::tuple<InputValues...>>;

			static_assert(internal::areInputKeysValid(Type<DefaultMap>{}, Type<InputMap>{}), "");
			return detail::sortAndAddDefaults_impl<DefaultKeys...>(defaults, inputs);
		}
	}
}

﻿/* COPYRIGHT
 *
 * file="NumericGradients.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "ParallelDriver.h"

namespace qengine 
{


	template<class Function >
	std::result_of_t<Function(double)> forwardDifference(const Function& f, const double alpha, const double epsilon = 1e-6)
	{

		return  (f(alpha + epsilon) - f(alpha)) / epsilon;
	}
	template<class Function >
	std::result_of_t<Function(double)> backwardDifference(const Function& f, const double alpha, const double epsilon = 1e-6)
	{

		return  (f(alpha) - f(alpha - epsilon)) / epsilon;
	}

	template<class Function >
	std::result_of_t<Function(double)> centralDifference(const Function& f, const double alpha, const double epsilon = 1e-6)
	{

		return  (f(alpha + epsilon) - f(alpha - epsilon)) / (2 * epsilon);
	}
	template<class Function >
	std::result_of_t<Function(double)> numericGradient(const Function& f, const double alpha, const double epsilon = 1e-6)
	{

		return  centralDifference(f, alpha, epsilon);
	}


	template<class Vector, class CostFunction>
	Vector numericGradient(const CostFunction& costFunction, const Vector& param, const double epsilon = 1e-6, const bool print = false)
	{
		Vector gradient = param;
		Vector searchDirection = param;
		for (auto i = 0u; i < param.size(); ++i)
		{
			searchDirection.at(i) = 0.0;
		}

		std::mutex m;
		auto driver = qengine::internal::ParallelDriver();

		for (auto i = 0u; i < param.size(); ++i)
		{
			driver.addTask([f{ costFunction }, i, &searchDirection, param, &gradient, epsilon, &m, print]()
			{
				auto s = searchDirection;
				s.at(i) = 1;
				auto testFunction = [&](double alpha) {return f(param + alpha * s); };

				auto gradval = numericGradient(testFunction, 0.0, epsilon);
				m.lock();
				gradient.at(i) = gradval;
				m.unlock();

				if (print)
				{
					std::cout << "done with " << i << " of " << param.size() << ". gradient: " << gradient.at(i) << std::endl;
				}
			});
		}
		driver.run();

		return gradient;
	}

	template<class Vector, class CostFunction>
	auto numericGradient2(const CostFunction& costFunction, const Vector& param, const double epsilon = 1e-6, const bool print = false)
	{
		Vector gradient = param;
		Vector searchDirection = param;
		for (auto i = 0u; i < param.size(); ++i)
		{
			searchDirection.at(i) = 0.0;
		}

		std::mutex m;
		auto driver = qengine::internal::ParallelDriver();

		auto uppers = param;
		auto lowers = param;

		for (auto i = 0u; i < param.size(); ++i)
		{
			driver.addTask([f{ costFunction }, i, &searchDirection, param, &gradient, &lowers, &uppers, epsilon, &m, print]()
			{
				auto s = searchDirection;
				s.at(i) = 1;
				auto testFunction = [&](double alpha) {return f(param + alpha * s); };

				auto upper = testFunction(epsilon);
				auto lower = testFunction(-epsilon);
				auto diff = upper - lower;
				auto gradval = diff / (2 * epsilon);

				//auto gradval = numericGradient(testFunction, 0.0, epsilon);
				m.lock();
				lowers.at(i) = lower;
				uppers.at(i) = upper;
				gradient.at(i) = gradval;
				m.unlock();

				if (print)
				{
					std::cout << "done with " << i << " of " << param.size() << ". gradient: " << gradient.at(i) << std::endl;
				}
			});
		}
		driver.run();

		return std::make_tuple(gradient, lowers, uppers);
	}
}

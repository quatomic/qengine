﻿/* COPYRIGHT
 *
 * file="apply_vec.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


namespace qengine
{
	namespace internal
	{
		namespace detail {
			template <class F, class Vector, std::size_t... I>
			constexpr decltype(auto) apply_vec_impl(F&& f, Vector&& t, std::index_sequence<I...>)
			{
				return f(std::forward<Vector>(t).at(I)...);
			}
		}  // namespace detail

		template <std::size_t nParams, class F, class Vector>
		constexpr decltype(auto) apply_vec(F&& f, Vector&& t)
		{
			qengine_assert(t.size() == nParams, "wrong number of arguments in vector");
			return detail::apply_vec_impl(
				std::forward<F>(f), std::forward<Vector>(t),
				std::make_index_sequence<nParams>{});
		}
	}
}

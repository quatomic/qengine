﻿/* COPYRIGHT
 *
 * file="IntegerFunctions.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/qengine_assert.h"

namespace qengine
{
	namespace internal
	{
		template <typename T>
		int sgn(T val)
		{
			return (static_cast<T>(0) < val) - (val < static_cast<T>(0));
		}

		inline int sgn(complex val)
		{
			return sgn(std::norm(val));
		}

		template<unsigned int exponent, class T>
		inline constexpr T power(const T base)
		{
			return (exponent == 0) ? 
						1 // if exponent ==0, return 1
					:
						(exponent % 2 == 0) ? 
									power<exponent/2>(base)*power<exponent / 2>(base) // if exponent is even, return product of half-exponent 
								:
									base * power<(exponent-1) / 2>(base) * power<(exponent - 1) / 2>(base);
		}

		inline constexpr bool odd(const std::size_t n) { return n % 2; }
		inline constexpr bool even(const std::size_t n) { return !odd(n); }

		// power for integers. DOES NOT HANDLE OVERFLOW!
		constexpr count_t int_power(const count_t base, const count_t exponent)
		{
			if (exponent == 1) return base;
			if (exponent == 0) return 1;

			if (odd(exponent)) return int_power(base, exponent - 1)*base;
			// default case: exponent is even
			const auto a = int_power(base, exponent / 2);
			return a * a;
		}

		template<class T>
		T int_power(const T& base, const std::size_t exponent)
		{
			qengine_assert(exponent > 0, "int_power cannot handle 0 in the exponent for general cases");

			if (exponent == 1) return base;
			
			if (odd(exponent)) return int_power(base, exponent - 1)*base;
			// default case: exponent is even
			auto a = int_power(base, exponent / 2);
			return a * a;
		}
	}
}

﻿/* COPYRIGHT
 *
 * file="TypeMap.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>

#include "src/Utility/TypeList.h"

namespace qengine
{
	namespace internal
	{
		template<class, class, class SFINAE = void> class TypeMap;

		// todo: put in check for uniqueness of keys!
		template<class... Keys, class... ValueTypes>
		class TypeMap<std::tuple<Keys...>, std::tuple<ValueTypes...>, std::enable_if_t<sizeof...(Keys) == sizeof...(ValueTypes)>>
		{
		public:
			TypeMap(std::tuple<ValueTypes...> values) : values_(values) {}

			template<class Key>
			const auto& get() const
			{
				static_assert(contains<Key>(), "requested type is not in list of keys");
				return std::get<internal::findFirstIndex<Key, Keys...>()>(values_);
			}

			template<class Key>
			auto& get()
			{
				static_assert(contains<Key>(), "requested type is not in list of keys");
				return std::get<internal::findFirstIndex<Key, Keys...>()>(values_);
			}

			// contains check, similar to std::map::contains
			template<class Key>
			static constexpr bool contains() { return internal::isInList<Key, Keys...>(); }

			static constexpr TypeList<Keys...> getKeys() { return {}; }

			std::tuple<ValueTypes...> values() const { return values_; }

		private:
			std::tuple<ValueTypes...> values_;
		};

		template<class T, class... Keys, class... ValueTypes>
		const auto& get(const TypeMap<std::tuple<Keys...>, std::tuple<ValueTypes...>>& map)
		{
			return map.template get<T>();
		}

		template<class... Keys, class... ValueTypes>
		TypeMap<std::tuple<Keys...>, std::tuple<ValueTypes...>> makeTypeMap(TypeList<Keys...>, std::tuple<ValueTypes...> values)
		{
			return TypeMap<std::tuple<Keys...>, std::tuple<ValueTypes...>>(values);
		}

		template<class... Keys, class... ValueTypes>
		constexpr TypeList<Keys...> getKeys(Type<TypeMap<std::tuple<Keys...>, std::tuple<ValueTypes...>>>)
		{
			return TypeList<Keys...>();
		}
	}
}

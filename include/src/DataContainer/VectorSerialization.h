﻿/* COPYRIGHT
 *
 * file="VectorSerialization.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/TypeList.h"

#include "src/DataContainer/SerializedObject.h"
#include "src/Utility/qengine_assert.h"

namespace qengine
{
	template<class T, class = std::enable_if_t<internal::isSerializable_v<T>>>
	SerializedObject toSerializedObject(const std::vector<T>& t)
	{
		std::vector<SerializedObject> sv;
		sv.reserve(t.size());
		for (const auto& x : t)
		{
			sv.push_back(SerializedObject(x));
		}
		SerializedObject retval;
		retval._set(sv);
		return retval;
	}

	template<class T, class = std::enable_if_t<internal::isSerializable_v<T>>>
	std::vector<T> fromSerializedObject(const SerializedObject& obj, internal::Type<std::vector<T>>)
	{
		qengine_assert(obj._type() == SerializedObject::LIST, "");
		std::vector<T> v;
		const auto& sv = obj._list();
		v.reserve(sv.size());
		for (const auto& x : sv)
		{
			v.push_back(fromSerializedObject(x, internal::Type<T>()));
		}
		return v;

	}
}

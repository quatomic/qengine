﻿/* COPYRIGHT
 *
 * file="SerializedObject.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <vector>

#include "src/NumberTypes.h"
#include "src/Utility/cpp17Replacements/constexpr_ternary.h"

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"

#include "src/DataContainer/isSerializable.h"
#include "src/DataContainer/Exceptions.h"
#include "src/DataContainer/DataStruct.h"

#include <variant.hpp>

namespace qengine
{
	class SerializedObject
	{
	public:
		enum TYPE
		{
			EMPTY, STRING, REAL, COMPLEX, STRUCT, LIST
		};
		enum FORMAT 
		{
			NA, SCALAR, VECTOR, MATRIX
		};

		SerializedObject();
		~SerializedObject();

		template<class T, typename = std::enable_if_t<internal::isSerializable_v<T>>>
		SerializedObject(const T& t) : SerializedObject(toSerializedObject(t)) {}


		SerializedObject(real t);
		SerializedObject(const complex& t);
		SerializedObject(const std::string& t);

		template<class T, class = std::enable_if_t<std::is_arithmetic<T>::value && !std17::is_same_v<T, real>, void>>
		SerializedObject(T t) : SerializedObject(static_cast<real>(t)) {}


		//SerializedObject(const SerializedObject& other);
		//SerializedObject(SerializedObject&& other) noexcept;

		//SerializedObject& operator=(const SerializedObject& other);
		//SerializedObject& operator=(SerializedObject&& other) noexcept;

		template<class T, std::enable_if_t<internal::isSerializable_v<T> || std17::is_same_v<T, real> || std17::is_same_v<T, complex> || std17::is_same_v<T, std::string>, int> = 0>
		T get() const
		{
			return repl17::constexpr_ternary(internal::isSerializable<T>(),
				[this](auto _) {return fromSerializedObject(_(*this), internal::Type<T>()); },
				[this](auto _) 
			{
				return _(this)->operator T();
			}
				);

			
		}

		template<class T, std::enable_if_t<std::is_arithmetic<T>::value && !std17::is_same_v<T, real>, int> = 0>
		T get() const
		{
			return static_cast<T>(real(*this));
		}
		
		template<class T, std::enable_if_t<std::is_arithmetic<T>::value && !std17::is_same_v<T, real>, int> = 0>
		operator T() const
		{
			return static_cast<T>(real(*this));
		}

		template<class T, std::enable_if_t<internal::isSerializable_v<T>, int> = 0>
		operator T() const
		{
			return fromSerializedObject(*this, internal::Type<T>());
		}

		operator real() const;
		operator complex() const;
		operator std::string() const;

		SerializedObject& append(const SerializedObject& other);

		/// INTERNAL USE
	public:

		TYPE _type() const { return type_; }
		FORMAT _format() const { return format_; }
		
		void _set(const std::string& string);
		void _set(const RMat& mat);
		void _set(const CMat& mat);
		void _set(const DataStruct& dataStruct);
		void _set(const std::vector<SerializedObject>& list);


		std::string& _string() { return mpark::get<std::string>(variant_); }
		RMat& _rmat() { return mpark::get<RMat>(variant_); }
		CMat& _cmat() { return mpark::get<CMat>(variant_); }
		DataStruct& _dataStruct() { return mpark::get<DataStruct>(variant_); }
		std::vector<SerializedObject>& _list() { return mpark::get<std::vector<SerializedObject>>(variant_); }

		const std::string& _string() const { return mpark::get<std::string>(variant_); }
		const RMat& _rmat() const { return mpark::get<RMat>(variant_); }
		const CMat& _cmat() const { return mpark::get<CMat>(variant_); }
		const DataStruct& _dataStruct() const { return mpark::get<DataStruct>(variant_); }
		const std::vector<SerializedObject>& _list() const { return mpark::get<std::vector<SerializedObject>>(variant_); }
		
		const auto& _variant() const { return variant_; }

		void _clear();

	private:


		TYPE type_ = EMPTY;
		FORMAT format_ = NA;
		
		mpark::variant<mpark::monostate, std::string, RMat, CMat, DataStruct, std::vector<SerializedObject>> variant_;

		//std::unique_ptr<std::string> string_;
		//std::unique_ptr<RMat> rmat_;
		//std::unique_ptr<CMat> cmat_;
		//std::unique_ptr<DataStruct> dataStruct_;
		//std::vector<SerializedObject> list_;

		template<class T>
		void appendFormatSwitch(Matrix<T>& thisMat, const Matrix<T>& otherMat, FORMAT thisFormat, FORMAT otherFormat, const SerializedObject& other);
	};

}


﻿/* COPYRIGHT
 *
 * file="SerializedObject.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/DataContainer/SerializedObject.h"

#include "src/DataContainer/DataStruct.h"

#include <armadillo>

#include "src/Utility/DeepCopy.h"
#include "src/Utility/qengine_assert.h"

namespace qengine
{
	SerializedObject::SerializedObject()
	{
	}
	SerializedObject::~SerializedObject()
	{
	}

	SerializedObject::SerializedObject(real r)
	{
		_set(RMat{ {r} });
	}

	SerializedObject::SerializedObject(const complex & z)  
	{
		_set(CMat{ { z } });
	}

	SerializedObject::SerializedObject(const std::string & str)
	{
		_set(str);
	}
/*
	SerializedObject::SerializedObject(const SerializedObject& other) :
		type_(other.type_),
		format_(other.format_),
		string_(internal::deep_copy(other.string_)),
		rmat_(internal::deep_copy(other.rmat_)),
		cmat_(internal::deep_copy(other.cmat_)),
		dataStruct_(internal::deep_copy(other.dataStruct_)),
		list_(other.list_)
	{
	}

	SerializedObject::SerializedObject(SerializedObject&& other) noexcept :
		type_(other.type_),
		format_(other.format_),
		string_(std::move(other.string_)),
		rmat_(std::move(other.rmat_)),
		cmat_(std::move(other.cmat_)),
		dataStruct_(std::move(other.dataStruct_)),
		list_(std::move(other.list_))
	{
	}

	SerializedObject& SerializedObject::operator=(const SerializedObject& other)
	{
		if (this == &other)
			return *this;
		type_ = other.type_;
		format_ = other.format_;
		string_ = internal::deep_copy(other.string_);
		rmat_ = internal::deep_copy(other.rmat_);
		cmat_ = internal::deep_copy(other.cmat_);
		dataStruct_ = internal::deep_copy(other.dataStruct_);
		list_ = other.list_;
		return *this;
	}

	SerializedObject& SerializedObject::operator=(SerializedObject&& other) noexcept
	{
		if (this == &other)
			return *this;
		type_ = other.type_;
		format_ = other.format_;
		string_ = std::move(other.string_);
		rmat_ = std::move(other.rmat_);
		cmat_ = std::move(other.cmat_);
		dataStruct_ = std::move(other.dataStruct_);
		list_ = std::move(other.list_);
		return *this;
	}*/


	template<class T>
	void SerializedObject::appendFormatSwitch(Matrix<T>& thisMat, const Matrix<T>& otherMat, FORMAT thisFormat, FORMAT otherFormat, const SerializedObject& other)
	{
		if (thisFormat == NA || otherFormat == NA) throw DC_TypeMismatch("");
		else if (thisFormat == SCALAR && otherFormat == SCALAR)
		{
			*this = Vector<T>{ thisMat.at(0,0), otherMat.at(0,0) };
		}
		else if (thisFormat == VECTOR && otherFormat == SCALAR)
		{
			thisMat._mat() = arma::join_cols(thisMat._mat(), otherMat._mat());
		}
		else if (thisFormat == VECTOR && otherFormat == VECTOR)
		{
			if (thisMat.n_rows() == otherMat.n_rows()) 
			{ 
				thisMat._mat() = arma::join_rows(thisMat._mat(), otherMat._mat()); 
				format_ = MATRIX;
			}
			else _set(std::vector<SerializedObject>{*this, other});
		}
		else if (thisFormat == MATRIX && otherFormat == VECTOR)
		{
			if (thisMat.n_rows() == otherMat.n_rows()) thisMat._mat() = arma::join_rows(thisMat._mat(), otherMat._mat());
			else _set(std::vector<SerializedObject>{*this, other});
		}
		else _set(std::vector<SerializedObject>{*this, other});
	}

	SerializedObject::operator real() const
	{
		if (_type() == SerializedObject::REAL)
		{
			//return _rmat()->at(0, 0);
			return _rmat().at(0, 0);
		}
		throw DC_TypeMismatch("failed to convert SerializedObject to real");
	}

	SerializedObject::operator complex() const
	{
		if (_type() == SerializedObject::COMPLEX)
		{
			return _cmat().at(0, 0);
		}
		else if (_type() == SerializedObject::REAL)
		{
			return static_cast<complex>(real(*this));
		}

		throw DC_TypeMismatch("failed to convert SerializedObject to complex");
	}

	SerializedObject::operator std::string() const
	{
		if (_type() == SerializedObject::STRING)
		{
			return _string();
		}
		throw DC_TypeMismatch("failed to convert SerializedObject to real");
	}

	SerializedObject & SerializedObject::append(const SerializedObject& other)
	{
		switch (type_)
		{
		case EMPTY:
			return (*this = other);
			break;
		case STRING:
			if (other.type_ == STRING) _set(std::vector<SerializedObject>{*this, other});
			else throw DC_TypeMismatch("can not append anything but a string to a string");
			break;
		case REAL:
			if (other.type_ == REAL)
			{
				appendFormatSwitch(_rmat(), other._rmat(), format_, other.format_, other);


			}
			else if (other.type_ == COMPLEX)
			{
				// turn this to complex and call again recursively, but this time we are complex
				*this = CMat(_rmat());
				return append(other);
			}
			else throw DC_TypeMismatch("can not append this type to real");

			break;
		case COMPLEX:
			if (other.type_ == REAL)
			{
				return append(SerializedObject(CMat(other._rmat()))); // convert the other to complex and append it.
			}
			else if (other.type_ == COMPLEX)
			{
				appendFormatSwitch(_cmat(), other._cmat(), format_, other.format_, other);
			}
			else throw DC_TypeMismatch("can not append this type to complex");

			break;
		case STRUCT:
			if (other.type_ == STRUCT) _set(std::vector<SerializedObject>{*this, other});
			else throw DC_TypeMismatch("can not append anything but a struct to a struct");

			break;
		case LIST:
			{
				auto baseType = _list().front().type_;
				if (other.type_ == LIST)
				{
					auto otherBase = other._list().front().type_;
					if (baseType != otherBase) throw DC_TypeMismatch("cannot append lists with different base types");
					else _list().insert(_list().end(), other._list().begin(), other._list().end());
				}
				else if (other.type_ == baseType)
				{
					_list().push_back(other);
				}
				else throw DC_TypeMismatch("cannot append this type to list");
				break;
			}

		default:
			break;
		}

		return *this;
	}

	//SerializedObject& SerializedObject::append(const std::string& s)
	//{
	//	if (type_ == STRING) strings_.push_back(s);
	//	else throw DC_TypeMismatch("");
	//	return *this;
	//}

	//namespace
	//{
	//	template<class T>
	//	void appendScalarToList(const T& val, std::vector<Matrix<T>>& list)
	//	{
	//		// if the current content is a vector, we append to it
	//		if (list.size() == 1 && list.front().n_cols() == 1)
	//		{
	//			list.front()._append(val);
	//		}
	//		else// else we append a new matrix
	//		{
	//			list.push_back(Matrix<T>{ {val}});
	//		}
	//	}

	//	template<class To, class From>
	//	std::vector<Matrix<To>> upconvert(const std::vector<Matrix<From>>& from)
	//	{
	//		auto retVal= std::vector<Matrix<To>>{};
	//		retVal.reserve(from.size());
	//		for(const auto& m:from)
	//		{
	//			retVal.push_back(Matrix<To>(m));
	//		}
	//		return retVal;
	//	}
	//}
	//
	//SerializedObject& SerializedObject::append(const real x)
	//{
	//	switch (type_)
	//	{
	//	case REAL:
	//		appendScalarToList(x, rmats_);
	//		return *this;
	//	case COMPLEX:
	//		return append(static_cast<complex>(x));
	//	default:
	//		throw DC_TypeMismatch("");
	//	}
	//}

	//SerializedObject& SerializedObject::append(const complex& z)
	//{
	//	switch (type_)
	//	{
	//	case REAL:
	//		_set(upconvert<complex>(rmats_)); // fallthrough is expected, as set changes type to COMPLEX
	//		return *this;
	//	case COMPLEX:
	//		appendScalarToList(z, cmats_);
	//	default:
	//		throw DC_TypeMismatch("");
	//	}
	//}

	//SerializedObject& SerializedObject::append(const RVec& v)
	//{
	//	return *this;
	//}

	//SerializedObject& SerializedObject::append(const CVec& v)
	//{
	//	return *this;
	//}

	//SerializedObject& SerializedObject::append(const RMat& m)
	//{
	//	return *this;
	//}

	//SerializedObject& SerializedObject::append(const CMat& m)
	//{
	//	return *this;
	//}

	void SerializedObject::_set(const std::string& string)
	{
		_clear();
		type_ = STRING;
		variant_ = string;
	}

	void SerializedObject::_set(const RMat& mat)
	{
		_clear();
		type_ = REAL;

		if (mat.n_cols() > 1) format_ = MATRIX;
		else if (mat.n_rows() > 1) format_ = VECTOR;
		else format_ = SCALAR;
		variant_ = mat;
	}

	void SerializedObject::_set(const CMat& mat)
	{
		_clear();
		type_ = COMPLEX;

		if (mat.n_cols() > 1) format_ = MATRIX;
		else if (mat.n_rows() > 1) format_ = VECTOR;
		else format_ = SCALAR;

		variant_ = mat;
	}

	void SerializedObject::_set(const DataStruct& dataStruct)
	{
		_clear();
		type_ = STRUCT;
		variant_ = dataStruct;
	}

	namespace
	{
		bool allTheSame(const std::vector<SerializedObject>& list)
		{
			auto type = list.front()._type();
			if (type == SerializedObject::LIST)
			{
				for (const auto& obj : list)
				{
					if (obj._type() != type) return false;
					if (!allTheSame(obj._list())) return false;
				}
			}
			if (type == SerializedObject::STRUCT)
			{
				auto typeId = list.front()._dataStruct().typeId;

				for (const auto& obj : list)
				{
					if (obj._type() != type) return false;
					if (obj._dataStruct().typeId != typeId) return false;
				}
			}
			else
			{
				for (const auto& obj : list)
				{
					if (obj._type() != type) return false;
				}
			}

			return true;
		}
	}

	void SerializedObject::_set(const std::vector<SerializedObject>& list)
	{
		if (list.empty())
		{
			_clear();
			return;
		}

		_clear();
		qengine_assert(allTheSame(list), "");
		type_ = LIST;
		variant_ = list;
	}

	void SerializedObject::_clear()
	{
		type_ = EMPTY;
		format_ = NA;

		variant_ = mpark::monostate{};
	}
}

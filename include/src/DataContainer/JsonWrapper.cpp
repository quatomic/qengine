﻿/* COPYRIGHT
 *
 * file="JsonWrapper.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "JsonWrapper.h"

#include <regex>

#include "src/Utility/qengine_assert.h"

#include "src/DataContainer/SerializedObject.h"

namespace
{
	void replaceAll(std::string& str, const std::string& from, const std::string& to) {
		size_t start_pos = 0;
		while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
			str.replace(start_pos, from.length(), to);
			start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
		}
	}
}

namespace arma
{
	void to_json(nlohmann::json& j, const arma::mat& m)
	{	
		if (m.n_cols > 1) // for matrix
		{
			std::vector<std::vector<double>> v;
			v.reserve(m.n_cols);
			for(auto i_row = 0u; i_row<m.n_rows; ++i_row)
			{
				v.push_back(arma::conv_to<std::vector<double>>::from(m.row(i_row)));
			}

			j = v;
		}
		else if(m.n_rows > 1)
		{
			j = arma::conv_to<std::vector<double>>::from(m.col(0));
		}
		else
		{
			j = m.at(0, 0);
		}
	}

	void to_json(nlohmann::json& j, const arma::cx_mat& m)
	{
		j["real"] = arma::mat(arma::real(m));
		j["imag"] = arma::mat(arma::imag(m));
	}

	void to_json(nlohmann::json& j, const arma::umat& m)
	{
		std::stringstream ss;
		m.save(ss, arma::csv_ascii);
		auto str = ss.str();

		str.pop_back(); // hacky way of removing the last linebreak in the string resulting from arma::save


		if (m.n_cols == 1) // for column vector
		{
			replaceAll(str, "\n", ",");
			str = "[" + str + "]";
		}
		else // for matrix
		{
			replaceAll(str, "\n", "],[");
			str = "[[" + str + "]]";
		}

		j = nlohmann::json::parse(str);
	}

	void to_json(nlohmann::json& j, const arma::sp_mat& mat)
	{
		arma::vec values(mat.n_nonzero);
		arma::uvec rowIndices(mat.n_nonzero);
		arma::uvec colIndices(mat.n_nonzero);

		auto i = 0u;
		for (auto it = mat.begin(); it != mat.end(); ++it, ++i)
		{
			rowIndices(i) = it.row();
			colIndices(i) = it.col();
			values(i) = *it;
		}

		j["n_rows"] = mat.n_rows;
		j["n_cols"] = mat.n_cols;

		j["values"] = values;
		j["rowIndices"] = rowIndices;
		j["colIndices"] = colIndices;
	}

	void to_json(nlohmann::json& j, const arma::sp_cx_mat& m)
	{
		j["real"] = arma::sp_mat(arma::real(m));
		j["imag"] = arma::sp_mat(arma::imag(m));
	}

	void from_json(const nlohmann::json& j, arma::mat& m)
	{
		if (j.is_number()) {
			m = arma::mat{ j.get<double>() };
			return;
		}

		qengine_assert(j.is_array(), "json to parse to create vector must be an array");

		const auto nRows = j.size();
		if (nRows == 0) return;

		const auto nCols = j.at(0).size();

		m = arma::mat(nRows, nCols, arma::fill::zeros);
		for (auto i_row = 0u; i_row < nRows; ++i_row)
		{
			if (j.at(i_row).type() == nlohmann::detail::value_t::array)
			{
				qengine_assert(j.at(i_row).size() == nCols, "number of elements is wrong in row " + std::to_string(i_row));
				for (auto i_col = 0u; i_col < nCols; ++i_col)
				{
					m.at(i_row, i_col) = j.at(i_row).at(i_col);
				}
			}
			else if (j.at(i_row).type() == nlohmann::detail::value_t::number_float || j.at(i_row).type() == nlohmann::detail::value_t::number_integer || j.at(i_row).type() == nlohmann::detail::value_t::number_unsigned)
			{
				m.at(i_row, 0u) = j.at(i_row);
			}
			else throw std::runtime_error("");
		}
	}

	void from_json(const nlohmann::json& j, arma::cx_mat& m)
	{
		m = arma::cx_mat(j["real"].get<arma::mat>(), j["imag"].get<arma::mat>());
	}


	void from_json(const nlohmann::json& j, arma::umat& m)
	{
		qengine_assert(j.type() == nlohmann::detail::value_t::array, "json to parse to create vector must be an array");

		const auto nRows = j.size();
		if (nRows == 0) return;

		const auto nCols = j.at(0).size();

		m = arma::umat(nRows, nCols, arma::fill::zeros);
		for (auto i_row = 0u; i_row < nRows; ++i_row)
		{
			if (j.at(i_row).type() == nlohmann::detail::value_t::array)
			{
				qengine_assert(j.at(i_row).size() == nCols, "number of elements is wrong in row " + std::to_string(i_row));
				for (auto i_col = 0u; i_col < nCols; ++i_col)
				{
					m.at(i_row, i_col) = j.at(i_row).at(i_col);
				}
			}
			else if (j.at(i_row).type() == nlohmann::detail::value_t::number_unsigned)
			{
				m.at(i_row, 0u) = j.at(i_row);
			}
			else throw std::runtime_error("");
		}
	}

	void from_json(const nlohmann::json& j, arma::sp_mat& m)
	{
		const arma::uvec rowIndices = j["rowIndices"].get<arma::umat>();
		const arma::uvec colIndices = j["colIndices"].get<arma::umat>();
		const arma::vec values = j["values"].get<arma::mat>();
		const arma::uword n_rows = j["n_rows"];
		const arma::uword n_cols = j["n_cols"];

		m = arma::sp_mat(arma::join_rows(rowIndices, colIndices).t(), values, n_rows, n_cols);
	}

	void from_json(const nlohmann::json& j, arma::sp_cx_mat& m)
	{
		m = arma::sp_cx_mat(j["real"].get<arma::sp_mat>(), j["imag"].get<arma::sp_mat>());
	}
}

namespace qengine
{

	void to_json(nlohmann::json& j, const SerializedObject& obj)
	{
		switch (obj._type())
		{
		case SerializedObject::EMPTY:
			break;
		case SerializedObject::STRING:
			j = obj._string();
			break;
		case SerializedObject::REAL:
			switch (obj._format())
			{
			case SerializedObject::NA:
				throw DC_TypeMismatch("");
				break;
			case SerializedObject::SCALAR:
				j = obj._rmat().at(0, 0);
				break;
			case SerializedObject::VECTOR:
				j = obj._rmat()._mat();
				break;
			case SerializedObject::MATRIX:
				j = obj._rmat()._mat();
				break;
			default:
				break;
			}
			break;
		case SerializedObject::COMPLEX:
			j["type"] = "complex";
			j["data"] = obj._cmat()._mat();
			break;
		case SerializedObject::STRUCT:
			break;
		case SerializedObject::LIST:
			j["type"] = "list";
			j["data"] = obj._list();
			break;

		default:
			break;
		}
	}

	void from_json(const nlohmann::json& j, SerializedObject& obj)
	{
		if (j.is_string()) obj = j.get<std::string>();
		if (j.is_number()) obj = j.get<double>();
		if (j.is_array()) obj = RMat(j.get<arma::mat>());
		if (j.is_object()) 
		{
			if (j["type"] == "complex") obj = CMat(j["data"].get<arma::cx_mat>());
			if (j["type"] == "list") 
			{
				std::vector<SerializedObject> v;
				v.reserve(j["data"].size());
				for(const auto& x: j["data"])
				{
					v.push_back(x.get<SerializedObject>());
				}
				obj._set(v);
			}
		}
	}
}

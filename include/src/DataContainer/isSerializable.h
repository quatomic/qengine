﻿/* COPYRIGHT
 *
 * file="isSerializable.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <type_traits>

#include "src/Utility/has_function.h"
#include "src/Utility/std17/is_same_v.h"
#include "src/Utility/TypeList.h"

namespace qengine
{
	class SerializedObject;

	namespace internal
	{
		template<class T, class SFINAE = void>
		struct isSerializable: std::false_type{};

		template<class T>
		struct isSerializable<T, std::enable_if_t < 
			std17::is_same_v<SerializedObject, decltype(toSerializedObject(std::declval<const T&>())) > &&
			std17::is_same_v<T, decltype(fromSerializedObject(std::declval<const SerializedObject&>(), Type<T>())) >
		, void>> 
		
		: std::true_type{};

		template<class T> constexpr bool isSerializable_v = isSerializable<T>::value;

	}
}

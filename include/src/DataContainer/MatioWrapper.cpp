﻿/* COPYRIGHT
 *
 * file="MatioWrapper.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "MatioWrapper.h"

#include <armadillo>
#include <matio.h>

#include "src/Utility/qengine_assert.h"

#include "src/DataContainer/SerializedObject.h"

std::mutex mat_mutex;

namespace matio_wrapper
{
	struct MatFile
	{
		MatFile(mat_t* file) :matioPtr(file) {};
		~MatFile() {
			Mat_Close(matioPtr);
		}

		mat_t* matioPtr;
	};
}

// save
namespace matio_wrapper
{

	namespace
	{
		matvar_t* toMatVar(const char* name, const std::string& str)
		{
			size_t dims[2] = { 1,str.size() };

			return Mat_VarCreate(name, MAT_C_CHAR, MAT_T_UTF8, 2, dims, const_cast<char*>(str.c_str()), 0);
		}

		matvar_t* toMatVar(const char* name, const qengine::RMat& mat)
		{
			//int rank = mat.n_cols() >1 ? 2: (mat.n_rows() >1? 1:0);
			size_t dims[2] = { mat.n_rows(), mat.n_cols() };

			return Mat_VarCreate(name, MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dims, const_cast<double*>(mat._mat().memptr()), 0);
		}

		matvar_t* toMatVar(const char* name, const qengine::CMat& mat)
		{
			size_t dimensions[2] = { mat.n_rows(), mat.n_cols() };

			arma::mat real = arma::real(mat._mat());
			arma::mat imag = arma::imag(mat._mat());

			struct mat_complex_split_t z = { real.memptr(),imag.memptr() };

			return Mat_VarCreate(name, MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dimensions, &z, MAT_F_COMPLEX);
		}

		matvar_t* toMatVar(const char* name, const qengine::SerializedObject& element);

		matvar_t* toMatVar(const char* name, const std::vector<qengine::SerializedObject>& list)
		{
			size_t dims[2] = { 1, list.size() };
			auto* var = Mat_VarCreate(name, MAT_C_CELL, MAT_T_CELL, 2, dims, nullptr, 0);

			matvar_t* cell_element;
			auto i = 0u;
			for (const auto& obj : list)
			{
				cell_element = toMatVar(NULL, obj);
				Mat_VarSetCell(var, i, cell_element);
				++i;
			}

			return var;
		}

		matvar_t* toMatVar(const char* name, const qengine::SerializedObject& element)
		{
			using namespace qengine;

			matvar_t* var = nullptr;
			const SerializedObject::TYPE type = element._type();
			switch (type)
			{
			case SerializedObject::EMPTY:
				break;
			case SerializedObject::STRING:
				var = toMatVar(name, element._string());
				//throw std::runtime_error("can't save strings to matfile at the moment");
				break;
			case SerializedObject::REAL:
				var = toMatVar(name, element._rmat());
				break;
			case SerializedObject::COMPLEX:
				var = toMatVar(name, element._cmat());
				//saveToMatFile(file, name.c_str(), *element._cmat());
				break;
			case SerializedObject::STRUCT:
				break;
			case SerializedObject::LIST:
				var = toMatVar(name, element._list());
				break;
			default:
				break;
			}

			return var;

		}
	}


	void writeToMatFile(const std::string & filename, const std::map<std::string, qengine::SerializedObject>& map)
	{
		std::lock_guard<std::mutex> lock(mat_mutex);
		auto file = MatFile(Mat_CreateVer(filename.c_str(), nullptr, MAT_FT_MAT73));

		qengine_assert(file.matioPtr != nullptr, "could not create file " + filename);

		for (const auto& elem : map)
		{
			matvar_t* var = toMatVar(elem.first.c_str(), elem.second);
			Mat_VarWrite(file.matioPtr, var, MAT_COMPRESSION_NONE);
			Mat_VarFree(var);
		}
	}
}

// load
namespace matio_wrapper
{
	namespace
	{
		arma::mat rmatFromMatvar(matvar_t* var)
		{
			size_t dims[2] = { var->dims[0], var->dims[1] };

			return arma::mat(static_cast<double*>(var->data), dims[0], dims[1]);
		}
		arma::cx_mat cmatFromMatvar(matvar_t* var)
		{
			size_t dims[2] = { var->dims[0], var->dims[1] };

			auto* splitArrayPtr = static_cast<mat_complex_split_t*> (var->data);
			arma::mat real = arma::mat(static_cast<double*>(splitArrayPtr->Re), dims[0], dims[1]);
			arma::mat imag = arma::mat(static_cast<double*>(splitArrayPtr->Im), dims[0], dims[1]);

			return arma::cx_mat(real, imag);
		}


		qengine::SerializedObject toSerializedObject(matvar_t* var);

		qengine::SerializedObject serializedObjectFromCell(matvar_t* var)
		{
			const auto size = var->dims[0] * var->dims[1];
			auto* cell_content = static_cast<matvar_t**>(var->data);
			std::vector<qengine::SerializedObject> vec;
			vec.reserve(size);

			for (auto i = 0u; i < size; ++i)
			{
				vec.push_back(toSerializedObject(cell_content[i]));
			}

			qengine::SerializedObject obj;
			obj._set(vec);
			return obj;
		}

		qengine::SerializedObject toSerializedObject(matvar_t* var)
		{
			const auto classType = var->class_type;
			switch (classType)
			{
			case MAT_C_DOUBLE:
				if (var->isComplex > 0) // what a weird way to describe a boolean...
				{
					return qengine::SerializedObject(qengine::CMat(cmatFromMatvar(var)));
				}
				else
				{
					return qengine::SerializedObject(qengine::RMat(rmatFromMatvar(var)));
				}
			case MAT_C_CHAR:
			{
#ifdef WIN32
				auto* dataPtr = static_cast<wchar_t*>(var->data);
#else
				auto* dataPtr = static_cast<char*>(var->data);
#endif

				auto str = std::string(dataPtr, dataPtr + var->dims[1]);

				return qengine::SerializedObject(std::move(str));
			}
			case MAT_C_CELL:
				return serializedObjectFromCell(var);
			default:
				throw std::runtime_error("cannot read this type");
			}

		}
	}

	void loadFromMatFile(const std::string & filename, std::map<std::string, qengine::SerializedObject>& map)
	{
		std::lock_guard<std::mutex> lock(mat_mutex);
		auto file = MatFile(Mat_Open(filename.c_str(), MAT_ACC_RDONLY));
		qengine_assert(file.matioPtr != nullptr, "could not load file " + filename);

		matvar_t* matvar = Mat_VarReadNext(file.matioPtr);
		while (matvar != nullptr)
		{
			map[matvar->name] = toSerializedObject(matvar);

			Mat_VarFree(matvar);
			matvar = Mat_VarReadNext(file.matioPtr);
		}
		Mat_VarFree(matvar);
	}
}

﻿/* COPYRIGHT
 *
 * file="MatioWrapper.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

//#include <matio.h>

#include <map>
#include <string>

namespace qengine
{
	class SerializedObject;
}

namespace matio_wrapper 
{
	void writeToMatFile(const std::string& filename, const std::map<std::string, qengine::SerializedObject>& map);
	void loadFromMatFile(const std::string& filename, std::map<std::string, qengine::SerializedObject>& map);

}

﻿/* COPYRIGHT
 *
 * file="DataContainer.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "DataContainer.h"

#include <iomanip>
#include <armadillo>

#include <nlohmann/json.hpp>

#include "src/Utility/StringStuff.h"

#include "src/DataContainer/JsonWrapper.h"

#ifdef BUILD_WITH_MATIO
#include "src/DataContainer/MatioWrapper.h"
#endif

namespace qengine
{
	void DataContainer::save(const std::string filename) const
	{
		const auto ext = internal::stringStuff::getExtension(filename);
		if (ext == "json")
		{
			auto j = nlohmann::json{};
			for (const auto& obj : map_)
			{
				auto j2 = nlohmann::json{};
				to_json(j2, obj.second);
				//j[obj.first] = obj.second;
				if (!j2.empty()) { j[obj.first] = j2; }

			}
			auto file = std::ofstream(filename);
			file << std::setw(4) << j;
		}

#ifdef BUILD_WITH_MATIO
		else if (ext == "mat") matio_wrapper::writeToMatFile(filename, map_);
#endif
		else if (ext == "") save(filename + ".json");
		else throw std::runtime_error("unrecognized extension: " + ext);
	}

	void qengine::DataContainer::save(std::string filename, std::string format) const
	{
		save(filename + "." + format);
	}

	void DataContainer::load(const std::string filename)
	{
		const auto ext = internal::stringStuff::getExtension(filename);
		clear();
		if (ext == "json") 
		{
			auto file = std::ifstream(filename);
			auto j = nlohmann::json{};
			file >> j;
			for (auto it = j.begin(); it!=j.end();++it) 
			{
				map_[it.key()] = it.value();
			}
		}
#ifdef BUILD_WITH_MATIO
		else if (ext == "mat") matio_wrapper::loadFromMatFile(filename, map_);
#endif
		else if (ext == "") load(filename + ".json");
		else throw std::runtime_error("unrecognized extension: " + ext);
	}

	void DataContainer::load(std::string filename, std::string format)
	{
		load(filename + "." + format);
	}

	std::vector<std::string> DataContainer::varNames() const
	{
		std::vector<std::string> allKeys;
		allKeys.reserve(map_.size());
		for (const auto& a : map_)
		{
			allKeys.push_back(a.first);
		}
		return allKeys;
	}

	bool DataContainer::hasVar(const std::string name) const
	{
		auto it = map_.find(name);
		return it != map_.end();
	}

	count_t DataContainer::nVars() const
	{
		return map_.size();
	}

	void DataContainer::clear()
	{
		map_.clear();
	}

	real DataContainer::getReal(std::string varname) const
	{
		real retval = map_.at(varname);
		return retval;
	}

	RVec DataContainer::getRVec(std::string varname) const
	{
		RVec retval = map_.at(varname);
		return retval;
	}

	RMat DataContainer::getRMat(std::string varname) const
	{
		RMat retval = map_.at(varname);
		return retval;
	}

	complex DataContainer::getComplex(std::string varname) const
	{
		complex retval = map_.at(varname);
		return retval;
	}

	CVec DataContainer::getCVec(std::string varname) const
	{
		CVec retval = map_.at(varname);
		return retval;
	}

	CMat DataContainer::getCMat(std::string varname) const
	{
		CMat retval = map_.at(varname);
		return retval;
	}
}

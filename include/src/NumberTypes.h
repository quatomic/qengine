﻿/* COPYRIGHT
 *
 * file="NumberTypes.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <complex>

namespace qengine
{
	/*
	 * Typedefs for basic number types, to abstract away different kinds of floating point numbers for physicists
	 */

	using real = double;
	using complex = std::complex<double>;


}

namespace qengine
{
	/*
	 * Typedef for counting-type used for indices etc.
	 */

	using count_t = unsigned long long;
}

namespace qengine
{
	/*
	* Some utilities for complex numbers
	*/

	inline real absSq(const complex& c)
	{
		return c.real()*c.real() + c.imag()*c.imag();
	}

	inline std::ostream& writeTo(std::ostream& stream, complex c)
	{
		auto a = c.real();
		auto b = c.imag();
		if (b == 0.0)
		{
			stream << a;
		}
		else if (a == 0)
		{
			stream << b << "i";
		}
		else
		{
			stream << a << "+" << b << "i";
		}
		return stream;
	}
}

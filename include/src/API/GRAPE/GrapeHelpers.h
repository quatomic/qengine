﻿/* COPYRIGHT
 *
 * file="GrapeHelpers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <vector>

//#include "src/Abstract/OptimalControl/Problems/StaticProblemInterfaces.h"
#include "src/NumberTypes.h"

#include "src/Abstract/OptimalControl/Parameters/Control.h"

namespace qengine
{
	namespace internal
	{
		inline auto makeL2InnerProductPolicy(real normalization)
		{
			return [normalization](const Control& left, const Control& right)
			{
				auto ip = dotL2(left, right);
				//			return ip * normalization;
				return ip;
			};
		}

		inline auto makeH1InnerProductPolicy(real normalization)
		{
			return [normalization](const Control& left, const Control& right)
			{
				auto ip = dotH1(left, right);
				//			return ip * normalization;
				return ip;
			};
		}

		inline auto makeL2GradientPolicy()
		{
			return [](const auto& problem)
			{
				return problem.gradient();
			};
		}
		
		inline void solvePoissonEquationInPlace(RVec& x, const double dt)
		{
			const auto dtSq = dt * dt;
			for (auto i = 0u; i < x.size(); ++i)
			{
				x.at(i) *= dtSq;
			}

			auto n = x.size() - 1;
			auto poissonDiagonal = std::vector<double>(x.size(), -2.0);
			for (auto k = 1u; k < x.size(); ++k)
			{
				auto m = 1 / poissonDiagonal.at(k - 1);
				poissonDiagonal.at(k) -= m;
				x.at(k) -= m * x.at(k - 1);
			}
			x.back() /= poissonDiagonal.back();

			for (auto k = static_cast<long long>(n - 1); k >= 0; --k)
			{
				x.at(k) -= x.at(k + 1);
				x.at(k) /= poissonDiagonal.at(k);
			}
		}

		inline auto makeH1GradientPolicy()
		{
			return [](const auto& problem)
			{
				auto grad = problem.gradient();

                solvePoissonEquationInPlace(grad._vecRef(), problem.dt());
                grad._vecRef() /= -problem.dt();

				return grad;
			};
		}

	}
}

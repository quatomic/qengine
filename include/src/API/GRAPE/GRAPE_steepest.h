﻿/* COPYRIGHT
 *
 * file="GRAPE_steepest.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/OptimalControl/Algorithms/SteepestDescent.h"

#include "src/API/GRAPE/GrapeHelpers.h"
#include "src/API/Common/OptimalControl/makeAlgorithm.h"

#include "src/API/Common/OptimalControl/Policies/Stoppers.h"
#include "src/API/Common/OptimalControl/Policies/Collectors.h"
#include "src/API/Common/OptimalControl/Policies/StepsizeFinders.h"
#include "src/API/Common/OptimalControl/Policies/BfgsRestarters.h"

namespace qengine
{
	namespace internal
	{
		template<class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy>
		auto makeGrape_steepest(const Problem& problem, const Stopper& stopper, const Collector& collector, const StepSizeFinder& stepSizeFinder, const InnerProductPolicy& innerProductPolicy, const GradientPolicy& gradientPolicy)
		{
			//static_assert(internal::is_QOCProblem_v<Problem>, "the problem does not fulfill the requirements for a quantum optimal control problem!");
			return SteepestDescent<Control, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy>(problem, stopper, collector, stepSizeFinder, innerProductPolicy, gradientPolicy);
		}

		inline auto makeGrapeSteepestDefaults()
		{
            auto stopper = makeFidelityStopper(0.99) + makeIterationStopper(1000);

			auto collector = makeEmptyCollector();
			auto stepSizeFinder = makeInterpolatingStepSizeFinder(10, 1.0);

			// the order and number of these defaults has to correspond to the order of inputs into makeGrape_steepest, except for inner product and gradient, which have to be set explicitly by a caller
			return toTypeMap(
				stopper,
				collector,
				stepSizeFinder);
		}
	}
}

namespace qengine
{
	template<class Problem, class... PolicyIDs, class... PolicyImpls>
	auto makeGrape_steepest_L2(const Problem& problem, const internal::PolicyWrapper<PolicyIDs, PolicyImpls>&... inputs)
	{
		auto makeAlg = [&problem](const auto& stopper, const auto& collector, const auto& stepSizeFinder)
		{
			return internal::makeGrape_steepest(problem, stopper, collector, stepSizeFinder, internal::makeL2InnerProductPolicy(problem.dt()), internal::makeL2GradientPolicy());
		};

		auto defaultsMap = internal::makeGrapeSteepestDefaults();

		return internal::makeAlgorithm(makeAlg, defaultsMap, inputs...);
	}

	template<class Problem, class... PolicyIDs, class... PolicyImpls>
	auto makeGrape_steepest_H1(const Problem& problem, const internal::PolicyWrapper<PolicyIDs, PolicyImpls>&... inputs)
	{
		auto makeAlg = [&problem](const auto& stopper, const auto& collector, const auto& stepSizeFinder)
		{
			return internal::makeGrape_steepest(problem, stopper, collector, stepSizeFinder, internal::makeH1InnerProductPolicy(problem.dt()), internal::makeH1GradientPolicy());
		};

		auto defaultsMap = internal::makeGrapeSteepestDefaults();

		return internal::makeAlgorithm(makeAlg, defaultsMap, inputs...);
	}
}

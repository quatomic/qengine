﻿/* COPYRIGHT
 *
 * file="GRAPE_bfgs.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/OptimalControl/Algorithms/Bfgs.h"

#include "src/API/GRAPE/GrapeHelpers.h"
#include "src/API/Common/OptimalControl/makeAlgorithm.h"

#include "src/API/Common/OptimalControl/Policies/Stoppers.h"
#include "src/API/Common/OptimalControl/Policies/Collectors.h"
#include "src/API/Common/OptimalControl/Policies/StepsizeFinders.h"
#include "src/API/Common/OptimalControl/Policies/BfgsRestarters.h"

namespace qengine
{
	namespace internal
	{
		template <class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy, class RestartPolicy>
		auto makeGrape_bfgs(const Problem& problem, const Stopper& stopper, const Collector& collector, const StepSizeFinder& stepSizeFinder, const InnerProductPolicy& innerProductPolicy, const GradientPolicy& gradientPolicy, const RestartPolicy& restartPolicy)
		{
			//static_assert(internal::is_QOCProblem_v<Problem>, "the problem does not fulfill the requirements for a quantum optimal control problem!");
			return Bfgs<Control, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy, RestartPolicy>(problem, stopper, collector, stepSizeFinder, innerProductPolicy, gradientPolicy, restartPolicy);
		}

		inline auto makeGrapeBfgsDefaults()
		{
            auto stopper = makeFidelityStopper(0.99) + makeIterationStopper(1000);

			auto collector = makeEmptyCollector();
			auto stepSizeFinder = makeInterpolatingStepSizeFinder(10, 1.0);
            auto bfgsRestarter = makeStepTimesYBfgsRestarter(1e-10);

			// the order and number of these defaults has to correspond to the order of inputs into makeGrape_steepest
			return toTypeMap(
				stopper,
				collector,
				stepSizeFinder,
				bfgsRestarter);
		}
	}
}


namespace qengine
{
	template<class Problem, class... PolicyIDs, class... PolicyImpls>
	auto makeGrape_bfgs_L2(const Problem& problem, const internal::PolicyWrapper<PolicyIDs, PolicyImpls>&... inputs)
	{
		auto makeAlg = [&problem](const auto& stopper, const auto& collector, const auto& stepSizeFinder, const auto& bfgsRestarter)
		{
			return internal::makeGrape_bfgs(problem, stopper, collector, stepSizeFinder, internal::makeL2InnerProductPolicy(problem.dt()), internal::makeL2GradientPolicy(), bfgsRestarter);
		};

		auto defaultsMap = internal::makeGrapeBfgsDefaults();

		return internal::makeAlgorithm(makeAlg, defaultsMap, inputs...);
	}

	template<class Problem, class... PolicyIDs, class... PolicyImpls>
	auto makeGrape_bfgs_H1(const Problem& problem, const internal::PolicyWrapper<PolicyIDs, PolicyImpls>&... inputs)
	{
		auto makeAlg = [&problem](const auto& stopper, const auto& collector, const auto& stepSizeFinder, const auto& bfgsRestarter)
		{
			return internal::makeGrape_bfgs(problem, stopper, collector, stepSizeFinder, internal::makeH1InnerProductPolicy(problem.dt()), internal::makeH1GradientPolicy(), bfgsRestarter);
		};

		auto defaultsMap = internal::makeGrapeBfgsDefaults();

		return internal::makeAlgorithm(makeAlg, defaultsMap, inputs...);
	}
}

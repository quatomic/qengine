﻿/* COPYRIGHT
 *
 * file="HamiltonianArithmetic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <functional>
#include "src/Utility/LinearAlgebra/SqDiagMat.h"

#include "src/API/TwoParticle/Hamiltonian2P.h"
#include "src/API/TwoParticle/PotentialWithInteraction.h"


namespace qengine
{
	namespace spatial
	{
		inline auto uplift(const FunctionOfX<real, two_particle::CommonDimensionSpace>& V, const spatial::SerializedHilbertSpaceND<2, 1, 2>& targetSpace)
		{
			const auto ones = RVec(V._serializedHilbertSpace().dim, 1.0);
			return makeFunctionOfX(targetSpace, kron(ones, V.vec()) + kron(V.vec(), ones));
		}
	}


	inline Hamiltonian2P operator+(const Hamiltonian2P& H, const FunctionOfX<real, two_particle::CommonDimensionSpace>& V)
	{
		auto fullV = spatial::uplift(V, H._serializedHilbertSpace());

		return internal::makeHamiltonian(H._serializedHilbertSpace(), H._hamiltonianMatrix() + internal::SqDiagMat<real>(std::move(fullV).vec()), H._potential() + V, H._interactionPotential());
	}

	inline Hamiltonian2P operator-(const Hamiltonian2P& H, const FunctionOfX<real, two_particle::CommonDimensionSpace>& V)
	{
		return H + (-V);
	}

	inline Hamiltonian2P operator+(const Hamiltonian2P& H, const two_particle::PointInteraction<real>& V)
	{
		auto fullV = spatial::uplift(V);

		return internal::makeHamiltonian(H._serializedHilbertSpace(), H._hamiltonianMatrix() + internal::SqDiagMat<real>(std::move(fullV).vec()), H._potential(), H._interactionPotential() + V);
	}

	inline Hamiltonian2P operator+(const Hamiltonian2P H, const PotentialWithInteraction& Vs)
	{
		return H + get<0>(Vs) + get<1>(Vs);
	}
}

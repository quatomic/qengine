﻿/* COPYRIGHT
 *
 * file="defaultSteppingAlg.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/TwoParticle/defaultSteppingAlg.h"

#include <armadillo>

#include "src/Utility/qengine_assert.h"

namespace qengine
{
	namespace two_particle
	{
		void applyExpVToPsiOptimizer(const CVec& expPot, const CVec& expInt, CVec& psi)
		{
			const auto dim = expPot.size();

			qengine_assert(expPot.size() == dim, "");
			qengine_assert(expInt.size() == dim, "");
			qengine_assert(psi.size() == dim*dim, "");

			//auto psiMat = arma::cx_mat(psi.vec().memptr(), dim, dim, false, true); // create an alias that points interprets the memory of psi as a matrix

			//psiMat.diag() %= expInt.vec();

			//const auto& V = expPot.vec();

			//psi.vec() %= arma::kron(V,V);

			for (auto i1 = 0u; i1<dim; ++i1)
			{
				psi.at(i1*dim + i1) *= expInt.at(i1);

				for (auto i2 = 0u; i2<dim; ++i2)
				{
					psi.at(i1*dim + i2) *= expPot.at(i1)*expPot.at(i2);
				}
			}
		}
	}
}

﻿/* COPYRIGHT
 *
 * file="Hamiltonian2P.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/NumberTypes.h"
#include "src/Utility/std17/remove_cvref.h"


#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.h"
#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.h"
#include "src/Abstract/Physics/Spatial/SerializedHilbertSpaceND.h"
#include "src/Abstract/Physics/Spatial/Spectrum.h"
#include "src/Abstract/Physics/Spatial/isHamiltonian.h"

#include "src/API/TwoParticle/CommonDimensionSpace.h"
#include "src/API/TwoParticle/PointInteraction.h"

#include "src/API/TwoParticle/PotentialWithInteraction.h"

namespace qengine
{
	class Hamiltonian2P : public internal::EigenstateSyntax_CRTP<Hamiltonian2P>
	{
	public:
		using SerializedHilbertSpace = spatial::SerializedHilbertSpaceND<2, 1, 2>;

		template<class Representation, class Potentials,
			typename = std::enable_if_t<
			std::is_same<std17::remove_cvref_t<Potentials>, PotentialWithInteraction>::value &&
			std::is_same<std17::remove_cvref_t<Representation>, internal::SqSparseMat<real>>::value>>
			Hamiltonian2P(const SerializedHilbertSpace& hilbertSpace, Representation&& representation, Potentials&& potentials);

		template<typename ScalarType>
		void applyInPlace(FunctionOfX<ScalarType, SerializedHilbertSpace>& f) const;

		spatial::Spectrum<real, SerializedHilbertSpace> makeSpectrum(count_t largestEigenvalue) const;
		spatial::Spectrum<real, SerializedHilbertSpace> makeSpectrum(count_t largestEigenvalue, count_t nExtraStates, real tolerance = 0.0) const;

		/// INTERNAL USE
	public:
		const auto& _potential() const { return get<0>(potentials_); }
		const internal::SqSparseMat<real>& _hamiltonianMatrix() const { return hamiltonianMatrix_; }
		const auto& _interactionPotential() const { return get<1>(potentials_); }
		const SerializedHilbertSpace& _serializedHilbertSpace() const { return serializedHilbertSpace_; }
		const auto& _potentials() const { return potentials_; }

	private:
		const SerializedHilbertSpace serializedHilbertSpace_;
		PotentialWithInteraction potentials_;
		internal::SqSparseMat<real> hamiltonianMatrix_;
	};

	template <class Representation, class Potentials, typename>
	Hamiltonian2P::Hamiltonian2P(const SerializedHilbertSpace& hilbertSpace, Representation&& representation, Potentials&& potentials) :
		serializedHilbertSpace_(hilbertSpace),
		potentials_(potentials),
		hamiltonianMatrix_(std::forward<Representation>(representation))
	{
		qengine_assert(!hamiltonianMatrix_.hasNan(), "Hamiltonian2P constructor: NaN found!");
	}

	namespace internal
	{
		template <class Representation, class Potentials, typename = std::enable_if_t<std::is_constructible<Hamiltonian2P, Hamiltonian2P::SerializedHilbertSpace, Representation, Potentials>::value>>
		Hamiltonian2P makeHamiltonian(const Hamiltonian2P::SerializedHilbertSpace& s, Representation&& rep, Potentials&& pot)
		{
			return Hamiltonian2P(s, std::forward<Representation>(rep), std::forward<Potentials>(pot));
		}

		template<class Representation, class Potential, class Interaction,
			typename = std::enable_if_t<
			std::is_same<std17::remove_cvref_t<Potential>, FunctionOfX<real, two_particle::CommonDimensionSpace>>::value &&
			std::is_same<std17::remove_cvref_t<Representation>, internal::SqSparseMat<real>>::value &&
			std::is_same<std17::remove_cvref_t<Interaction>, two_particle::PointInteraction<real>>::value>>
			Hamiltonian2P makeHamiltonian(const Hamiltonian2P::SerializedHilbertSpace& s, Representation&& rep, Potential&& pot, Interaction&& interaction)
		{
			return Hamiltonian2P(s, std::forward<Representation>(rep), std::forward<Potential>(pot) + std::forward<Interaction>(interaction));
		}
	}


	namespace spatial
	{
		template<>
		struct isLabelledAs_Hamiltonian<Hamiltonian2P> : std::true_type {};

	}

	namespace internal
	{
		template<>
		struct isLabelledAs_Operator<Hamiltonian2P> : std::true_type {};

		template<>
		struct isLabelledAs_Linear<Hamiltonian2P> : std::true_type {};

		template<>
		struct isLabelledAs_GuaranteedHermitian<Hamiltonian2P> : std::true_type {};

		template<>
		struct extract_serialized_hilbertspace<Hamiltonian2P>
		{
			using type = Hamiltonian2P::SerializedHilbertSpace;
		};
	}
}

namespace qengine
{
		template<class ScalarType>
		FunctionOfX<ScalarType, Hamiltonian2P::SerializedHilbertSpace> operator*(const Hamiltonian2P& H, FunctionOfX<ScalarType, Hamiltonian2P::SerializedHilbertSpace> psi)
		{
			H.applyInPlace(psi);
			return psi;
		}

}

﻿/* COPYRIGHT
 *
 * file="PointInteraction.h"
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


#include "src/Abstract/Physics/Spatial/SerializedHilbertSpaceND.h"

#include "src/API/TwoParticle/CommonDimensionSpace.h"

namespace qengine
{
	namespace two_particle
	{
		namespace detail
		{
			using PointInteractionSpace = spatial::SerializedHilbertSpace1D<struct PointInteractionSpaceIdentifier>;

			inline spatial::SerializedHilbertSpaceND<2, 1, 2> upliftSpace(const PointInteractionSpace& pointInteractionSpace)
			{
				return spatial::tensor(spatial::SubSpace<2, 1>(pointInteractionSpace), spatial::SubSpace<2, 2>(pointInteractionSpace));
			}
		}
	}

	namespace internal
	{
		template<class ScalarType>
		struct extract_serialized_hilbertspace<FunctionOfX<ScalarType, two_particle::detail::PointInteractionSpace>>
		{
			using type = spatial::SerializedHilbertSpaceND<2, 1, 2>;
		};
	}

	namespace two_particle
	{
		template<class N>
		using PointInteraction = FunctionOfX<N, detail::PointInteractionSpace>;


		/// pointInteractions is interpreted as interactionStrength(x)*delta(x1-x2), and the delta-interaction carries a factor of 1/dx
		template<class N>
		PointInteraction<N> makePointInteraction(const Potential<N>& interactionStrength)
		{
			return PointInteraction<N>(interactionStrength.vec() / interactionStrength._serializedHilbertSpace().dx(), detail::PointInteractionSpace(interactionStrength._serializedHilbertSpace()));
		}

		/// pointInteractions is interpreted as interactionStrength(x)*delta(x1-x2), and the delta-interaction carries a factor of 1/dx
		template<class N>
		PointInteraction<N> makePointInteraction(Potential<N>&& interactionStrength)
		{
			const auto dx = interactionStrength._serializedHilbertSpace().dx();
			return PointInteraction<N>(std::move(interactionStrength).vec() / dx, detail::PointInteractionSpace(interactionStrength._serializedHilbertSpace()));
		}

	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>> operator*(const two_particle::PointInteraction<N1>& left, FunctionOfX<N2, spatial::SerializedHilbertSpaceND<2, 1, 2>> f)
	{
		qengine_assert(f._serializedHilbertSpace().template get<1>().dim == f._serializedHilbertSpace().template get<2>().dim, "");
		auto dim = f._serializedHilbertSpace().template get<1>().dim;
		for (auto i = 0u; i < dim; ++i)
		{
			f.vec().at(i * dim + i) *= left.vec().at(i);
		}
		return f;
	}
}

namespace qengine
{
	namespace spatial
	{
		template<class N>
		FunctionOfX<N, spatial::SerializedHilbertSpaceND<2, 1, 2>> uplift(const two_particle::PointInteraction<N>& pointInteraction)
		{
			const auto xDim = pointInteraction._serializedHilbertSpace().dim;

			auto vec = Vector<N>(xDim * xDim, 0.0);

			for (auto i = 0u; i < xDim; ++i)
			{
				vec.at(i * xDim + i) = pointInteraction.vec().at(i);
			}

			return makeFunctionOfX(spatial::SerializedHilbertSpaceND<2, 1, 2>(
				std::make_tuple(
					spatial::SubSpace<2, 1>(pointInteraction._serializedHilbertSpace()),
					spatial::SubSpace<2, 2>(pointInteraction._serializedHilbertSpace())
				)), std::move(vec));

		}
	}
}

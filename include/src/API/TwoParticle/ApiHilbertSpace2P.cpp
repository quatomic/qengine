﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace2P.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/TwoParticle/ApiHilbertSpace2P.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.impl.h"
#include "src/Abstract/Physics/Spatial/LinearHamiltonian.impl.h"

#include <tuple>

namespace qengine
{
	ApiHilbertSpace2P::ApiHilbertSpace2P(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor) :
		serializedHilbertSpace_(std::make_tuple(spatial::SubSpace<2, 1>{xLower, xUpper, dimension, kinematicFactor},
			spatial::SubSpace<2, 2>{xLower, xUpper, dimension, kinematicFactor})),
		x1_(serializedHilbertSpace_.get<1>().makeSpatialDimension()),
		x2_(serializedHilbertSpace_.get<2>().makeSpatialDimension()),
		sCommon_(s1.xLower, s1.xUpper, s1.dim, s1.kinematicFactor),
		xCommon_(sCommon_.makeSpatialDimension())

	{
	}

	Hamiltonian2P ApiHilbertSpace2P::T() const
	{
		const auto left = T1();
		const auto right = T2();

		auto idLeft = internal::identityMatrix<internal::SqSparseMat>(left._serializedHilbertSpace().dim);
		auto idRight = internal::identityMatrix<internal::SqSparseMat>(right._serializedHilbertSpace().dim);

		return internal::makeHamiltonian(spatial::tensor(left._serializedHilbertSpace(), right._serializedHilbertSpace()),
			kron(static_cast<internal::SqSparseMat<real>>(left._hamiltonianMatrix()), idRight) + kron(idLeft, static_cast<internal::SqSparseMat<real>>(right._hamiltonianMatrix())),
			0.0 * xPotential(),
			makePointInteraction(0.0 * xPotential())
		);
	}

	bool operator==(const ApiHilbertSpace2P& left, const ApiHilbertSpace2P& right)
	{
		if (&left == &right) return true;

		return left.dim() == right.dim() && left.kinematicFactor() == right.kinematicFactor() && left.xLower() == right.xLower() && left.xUpper() == right.xUpper();
	}

	bool operator!=(const ApiHilbertSpace2P& left, const ApiHilbertSpace2P& right)
	{
		return !(left == right);
	}

	namespace two_particle 
	{
		ApiHilbertSpace2P makeHilbertSpace(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor)
		{
			return ApiHilbertSpace2P(xLower, xUpper, dimension, kinematicFactor);
		}
	}

}

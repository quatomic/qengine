﻿/* COPYRIGHT
 *
 * file="CommonDimensionSpace.h"
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/Spatial/SerializedHilbertSpaceND.h"

namespace qengine
{
	namespace two_particle
	{
		using CommonDimensionSpace = spatial::SerializedHilbertSpace1D<struct CommonDimensionSpaceIdentifier>;

		inline spatial::SerializedHilbertSpaceND<2, 1, 2> upliftSpace(const CommonDimensionSpace& commonDimensionSpace)
		{
			return spatial::tensor(spatial::SubSpace<2, 1>(commonDimensionSpace), spatial::SubSpace<2, 2>(commonDimensionSpace));
		}
		
		template<class N>
		using Potential = FunctionOfX<N, CommonDimensionSpace>;
	}

	namespace internal
	{
		template<class ScalarType>
		struct extract_serialized_hilbertspace<FunctionOfX<ScalarType, two_particle::CommonDimensionSpace>>
		{
			using type = spatial::SerializedHilbertSpaceND<2, 1, 2>;
		};
	}
}


namespace qengine
{
	namespace internal
	{
		template<class N>
		FunctionOfX<N, spatial::SerializedHilbertSpaceND<2, 1, 2>> uplift(const two_particle::Potential<N>& V)
		{
			const auto& s = V._serializedHilbertSpace();

			const auto foo = makeFunctionOfX(spatial::SubSpace<2, 1>(s), V._vec());
			return foo + makeFunctionOfX(spatial::SubSpace<2, 2>(s), V._vec());
		}
	}
}

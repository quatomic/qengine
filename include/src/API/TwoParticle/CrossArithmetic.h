﻿/* COPYRIGHT
 *
 * file="CrossArithmetic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.h"
#include "src/Abstract/Physics/Spatial/SerializedHilbertSpaceND.h"

#include "src/API/Common/Spatial/1D/LinearHamiltonian1D.h"
#include "src/API/Common/Spatial/2D/LinearHamiltonian2D.h"

#include "src/API/TwoParticle/HamiltonianArithmetic.h"

namespace qengine
{
	namespace spatial
	{
		template<class ScalarType>
		FunctionOfX<ScalarType, SerializedHilbertSpaceND<2, 1, 2>> uplift(const FunctionOfX<ScalarType, SubSpace<2, 1>>& f, const SerializedHilbertSpaceND<2, 1, 2>& targetSpace)
		{
			const auto onesRight = RVec(targetSpace.get<2>().dim, 1.0);
			return FunctionOfX<ScalarType, SerializedHilbertSpaceND<2, 1, 2>>(kron(f.vec(), onesRight), targetSpace);
		}
		template<class ScalarType>
		FunctionOfX<ScalarType, SerializedHilbertSpaceND<2, 1, 2>> uplift(const FunctionOfX<ScalarType, SubSpace<2, 2>>& f, const SerializedHilbertSpaceND<2, 1, 2>& targetSpace)
		{
			const auto onesLeft = RVec(targetSpace.get<1>().dim, 1.0);
			return FunctionOfX<ScalarType, SerializedHilbertSpaceND<2, 1, 2>>(kron(onesLeft, f.vec()), targetSpace);
		}
	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>> operator+(const FunctionOfX<N1, spatial::SubSpace<2, 1>>& left, const FunctionOfX<N2, spatial::SubSpace<2, 2>>& right)
	{
		auto space = spatial::tensor(left._serializedHilbertSpace(), right._serializedHilbertSpace());

		return spatial::uplift(left, space) + spatial::uplift(right, space);
	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator+(const FunctionOfX<N1, spatial::SubSpace<2, 2>>& left, const FunctionOfX<N2, spatial::SubSpace<2, 1>>& right)
	{
		return right + left;
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator-(const FunctionOfX<N1, spatial::SubSpace<2, 1>>& left, const FunctionOfX<N2, spatial::SubSpace<2, 2>>& right)
	{
		return left + (-right);
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator-(const FunctionOfX<N1, spatial::SubSpace<2, 2>>& left, const FunctionOfX<N2, spatial::SubSpace<2, 1>>& right)
	{
		return (-right) + left;
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator*(const FunctionOfX<N1, spatial::SubSpace<2, 1>>& left, const FunctionOfX<N2, spatial::SubSpace<2, 2>>& right)
	{
		return FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
			(
				kron(left.vec(), right.vec()),
				spatial::tensor(left._serializedHilbertSpace(), right._serializedHilbertSpace())
				);
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator*(const FunctionOfX<N1, spatial::SubSpace<2, 2>>& left, const FunctionOfX<N2, spatial::SubSpace<2, 1>>& right)
	{
		return right * left;
	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator/(const FunctionOfX<N1, spatial::SubSpace<2, 1>>& left, const FunctionOfX<N2, spatial::SubSpace<2, 2>>& right)
	{
		auto space = spatial::tensor(left._serializedHilbertSpace(), right._serializedHilbertSpace());

		return spatial::uplift(left, space) / spatial::uplift(right, space);
	}
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator/(const FunctionOfX<N1, spatial::SubSpace<2, 2>>& left, const FunctionOfX<N2, spatial::SubSpace<2, 1>>& right)
	{
		auto space = spatial::tensor(left._serializedHilbertSpace(), right._serializedHilbertSpace());

		return spatial::uplift(left, space) / spatial::uplift(right, space);
	}
}

namespace qengine
{
	inline LinearHamiltonian2D operator+(const LinearHamiltonian1D<spatial::SubSpace<2, 1>>& left, const LinearHamiltonian1D<spatial::SubSpace<2, 2>>& right)
	{
		auto idLeft = internal::identityMatrix<internal::SqSparseMat>(left._serializedHilbertSpace().dim);
		auto idRight = internal::identityMatrix<internal::SqSparseMat>(right._serializedHilbertSpace().dim);

		return LinearHamiltonian2D(spatial::tensor(left._serializedHilbertSpace(), right._serializedHilbertSpace()),
			kron(static_cast<internal::SqSparseMat<real>>(left._hamiltonianMatrix()), idRight) + kron(idLeft, static_cast<internal::SqSparseMat<real>>(right._hamiltonianMatrix())),
			left._potential() + right._potential()
		);
	}

	inline LinearHamiltonian2D operator+(const LinearHamiltonian1D<spatial::SubSpace<2, 2>>& left, const LinearHamiltonian1D<spatial::SubSpace<2, 1>>& right)
	{
		return right + left;
	}

	template<count_t dim>
	LinearHamiltonian2D operator+ (const LinearHamiltonian2D& left, const FunctionOfX<real, spatial::SubSpace<2, dim>>& pot)
	{
		return left + spatial::uplift(pot, left._serializedHilbertSpace());
	}
	template<count_t dim>
	LinearHamiltonian2D operator- (const LinearHamiltonian2D& left, const FunctionOfX<real, spatial::SubSpace<2, dim>>& pot)
	{
		return left - spatial::uplift(pot, left._serializedHilbertSpace());
	}
}


namespace qengine
{
	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator*(const FunctionOfX<N1, two_particle::CommonDimensionSpace>& left, const FunctionOfX<N2, spatial::SerializedHilbertSpaceND<2, 1, 2>>& right)
	{
		return spatial::uplift(left, right._serializedHilbertSpace())*right;
	}

	template<class N1, class N2>
	FunctionOfX<internal::NumberMax_t<N1, N2>, spatial::SerializedHilbertSpaceND<2, 1, 2>>
		operator*(const FunctionOfX<N2, spatial::SerializedHilbertSpaceND<2, 1, 2>>& left, const FunctionOfX<N1, two_particle::CommonDimensionSpace>& right)
	{
		return right*left;
	}
}

﻿/* COPYRIGHT
 *
 * file="defaultSteppingAlg.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/Constants.h"
#include "src/Utility/std17/is_same_v.h"

#include "src/Abstract/Physics/General/TimeStepper.h"

#include "src/API/Common/Spatial/TimeEvolutionHelpers.h"
#include "src/Abstract/Physics/Spatial/SplitStep.h"
#include "src/Abstract/Physics/Spatial/HamiltonianFunction.h"

#include "src/API/TwoParticle/Hamiltonian2P.h"
#include "src/API/TwoParticle/CrossArithmetic.h"

namespace qengine
{
	namespace two_particle
	{
		inline auto upliftExp(const Potential<complex>& expV)
		{
			const auto& s = expV._serializedHilbertSpace();

			const auto foo = makeFunctionOfX( spatial::SubSpace<2, 1>(s),expV.vec());
			return foo * makeFunctionOfX( spatial::SubSpace<2, 2>(s),expV.vec());
		}

		inline auto upliftExp(const PointInteraction<complex>& expV)
		{
			const auto& s = expV._serializedHilbertSpace();
			const auto xDim = s.dim;

			auto vec = Vector<complex>(xDim * xDim, 1.0);

			for (auto i = 0u; i < xDim; ++i)
			{
				vec.at(i * xDim + i) = expV.vec().at(i);
			}

			return makeFunctionOfX(spatial::SerializedHilbertSpaceND<2, 1, 2>(
				std::make_tuple(
					spatial::SubSpace<2, 1>(s),
					spatial::SubSpace<2, 2>(s)
				)), std::move(vec));

		}

		void applyExpVToPsiOptimizer(const CVec& expV_spat, const CVec& expV_int, CVec& psi);

		template<class State>
		auto applyExpVToPsi(const Potential<complex>& expV_spat, const PointInteraction<complex>& expV_int, const State& psi)
		{
			//return two_particle::upliftExp(expV_spat)*two_particle::upliftExp(expV_int)*psi;

			/// possible alternative approach using direct loop instead of uplifting

			const auto& expPot = expV_spat.vec();
			const auto& expInt = expV_int.vec();

			auto intermediate = psi;
			applyExpVToPsiOptimizer(expPot, expInt, intermediate.vec());

			return intermediate;
		}

		template<class State>
		auto applyExpTToPsi(const Potential<complex>& expT, const State& psi)
		{
			//return two_particle::upliftExp(expT)*psi;

			/// possible alternative approach using direct loop instead of uplifting

			const auto& expKin = expT.vec();

			const auto dim = expKin.size();

			auto intermediate = psi;
			for(auto i1 = 0u; i1<dim; ++i1)
			{
				for (auto i2 = 0u; i2<dim; ++i2)
				{
					intermediate.vec().at(i1*dim + i2) *= expKin.at(i1)*expKin.at(i2);
				}
			}

			return intermediate;
		}

		inline auto makeApplier(const Potential<complex>& expKin)
		{
			return[&](auto& psi)
			{
				const auto intermediate = applyExpTToPsi(expKin, psi);
				psi = intermediate;
			};
		}
		inline auto makeApplier(Potential<complex>&& expKin)
		{
			return[expKin = std::move(expKin)](auto& psi)
			{
				const auto intermediate = applyExpTToPsi(expKin, psi);
				psi = intermediate;
			};
		}

		inline auto makePSpaceKinetic(const Hamiltonian2P::SerializedHilbertSpace& s)
		{
			const auto& s1 = s.get<1>();
			auto pSpaceKineticVec = internal::makePSpaceKineticVec(s1.dx(), s1.dim, s1.kinematicFactor);

			return makeFunctionOfX(CommonDimensionSpace(s1), std::move(pSpaceKineticVec));
		}

	}

	inline FunctionOfX<real, two_particle::CommonDimensionSpace> makeDefaultImagPot(const two_particle::CommonDimensionSpace& hilbertSpace, const real barrierIntensity = 20 /*pretty arbitrary number. should probably depend on energy-units in system*/)
	{
		const auto N = hilbertSpace.dim;
		const auto dx = hilbertSpace.dx();

		return makeFunctionOfX(hilbertSpace, internal::makeDefaultImagPotVector(N, dx, barrierIntensity));
	}

	template<class AlgMaker>
	auto addImagBounds(AlgMaker&& algMaker, const FunctionOfX<real, two_particle::CommonDimensionSpace> imagPot)
	{
		return[algMaker{ std::forward<AlgMaker>(algMaker) }, imagPot](auto&&... ps)
		{
			auto alg = algMaker(std::forward<decltype(ps)>(ps)...);
			return [alg = std::move(alg), imagPot](auto& psi, real dt, auto&&... ts) mutable -> void
			{
				static_assert(std17::is_same_v<void, decltype(alg(psi, dt, std::forward<decltype(ts)>(ts)...))>, "");
				alg(psi, dt, std::forward<decltype(ts)>(ts)...);

				two_particle::makeApplier(exp(std::abs(dt) * imagPot))(psi);
			}; 
		};
	}

	template<class AlgMaker>
	auto addImagBounds(AlgMaker&& algMaker, const FunctionOfX<real, two_particle::CommonDimensionSpace> imagPot, const real  dt)
	{
		auto imagPotApplier = two_particle::makeApplier(exp(std::abs(dt) * imagPot));
		return[algMaker{ std::forward<AlgMaker>(algMaker) }, imagPotApplier](auto&&... ps)
		{
			auto alg = algMaker(std::forward<decltype(ps)>(ps)...);
			return [alg = std::move(alg), imagPotApplier](auto& psi, auto&&... ts) mutable -> void
			{
				static_assert(std17::is_same_v<void, decltype(alg(psi, std::forward<decltype(ts)>(ts)...))>, "");
				alg(psi, std::forward<decltype(ts)>(ts)...);
				imagPotApplier(psi);
			}; 
		};
	}

	inline auto makeSplitStepAlgorithm(const Hamiltonian2P& H)
	{
		using namespace std::complex_literals;
		return[splitStepAlg{ SplitStepAlgorithm() }, V{ H._potential() }, VInt{ H._interactionPotential() }, T{ two_particle::makePSpaceKinetic(H._serializedHilbertSpace()) }](auto& psi, real dt)
		{
			auto expV = exp(-1i * 0.5*dt*V);
			auto expVInt = exp(-1i * 0.5*dt*VInt);

			auto intermediate = two_particle::applyExpVToPsi(expV, expVInt, psi);
			psi = intermediate;

			auto expT = exp(-1i * dt*T);
			auto applyExpT = two_particle::makeApplier(expT);
			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);


			intermediate = two_particle::applyExpVToPsi(expV, expVInt, psi);
			psi = intermediate;
		}; 
	}

	inline auto makeSplitStepAlgorithm(const Hamiltonian2P& H, const real dt)
	{
		using namespace std::complex_literals;
		auto V = H._potential();
		auto expV = exp(-1i * 0.5*dt*V);

		auto VInt = H._interactionPotential();
		auto expVInt = exp(-1i * 0.5*dt*VInt);

		auto T = two_particle::makePSpaceKinetic(H._serializedHilbertSpace());
		auto expT = exp(-1i * dt*T);
		auto applyExpT = two_particle::makeApplier(std::move(expT));

		return[splitStepAlg{ SplitStepAlgorithm() }, expV, expVInt, applyExpT](auto& psi)
		{
			auto intermediate = two_particle::applyExpVToPsi(expV, expVInt, psi);
			psi = intermediate;

			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			intermediate = two_particle::applyExpVToPsi(expV, expVInt, psi);
			psi = intermediate;
		}; 
	}

	template<class PotentialFunctor, class... Params>
	auto makeSplitStepAlgorithm(const HamiltonianFunction<Hamiltonian2P, PotentialFunctor>& H, const Tuple<Params...>& initialParams)
	{
		using namespace std::complex_literals;
		auto V = [V_dyn{ H._potentialFunction() }, V_stat{ H._H0()._potentials() }](auto... ts){return V_stat + V_dyn(std::forward<decltype(ts)>(ts)...); };

		return[splitStepAlg{ SplitStepAlgorithm() }, V, T{ two_particle::makePSpaceKinetic(H._H0()._serializedHilbertSpace()) }, V_cache = V(initialParams)](auto& psi, real dt, const auto& p2) mutable
		{
			auto expV1 = exp(-1i * 0.5*dt*get<0>(V_cache));
			auto expV1Int = exp(-1i * 0.5*dt*get<1>(V_cache));
			auto intermediate = two_particle::applyExpVToPsi(expV1, expV1Int, psi);
			psi = intermediate;

			auto expT = exp(-1i * dt*T);
			auto applyExpT = two_particle::makeApplier(expT);
			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			V_cache = V(p2);
			auto expV2 = exp(-1i * 0.5*dt*get<0>(V_cache));
			auto expV2Int = exp(-1i * 0.5*dt*get<1>(V_cache));
			intermediate = two_particle::applyExpVToPsi(expV2, expV2Int, psi);
			psi = intermediate;
		}; 
	}

	template<class PotentialFunctor, class... Params>
	auto makeSplitStepAlgorithm(const HamiltonianFunction<Hamiltonian2P, PotentialFunctor>& H, const real dt, const Tuple<Params...>& initialParams)
	{
		using namespace std::complex_literals;
		auto T = two_particle::makePSpaceKinetic(H._H0()._serializedHilbertSpace());
		auto expT = exp(-1i * dt*T);
		auto applyExpT = two_particle::makeApplier(std::move(expT));

		auto ff = -1i * 0.5*dt;

		auto V = [V_dyn{ H._potentialFunction() }, V_stat{ H._H0()._potentials() }](auto... ts)->PotentialWithInteraction {return V_stat + V_dyn(std::forward<decltype(ts)>(ts)...); };

		auto expBoth = [ff](auto V) {return std::make_tuple(exp(ff*get<0>(V)), exp(ff*get<1>(V))); };
		return[splitStepAlg{ SplitStepAlgorithm() }, V, applyExpT, expBoth, expV_cache = expBoth(V(initialParams))](auto& psi, const auto& p2) mutable
		{
			auto intermediate = two_particle::applyExpVToPsi(std::get<0>(expV_cache), std::get<1>(expV_cache), psi);
			psi = intermediate;

			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			expV_cache = expBoth(V(p2));

			intermediate = two_particle::applyExpVToPsi(std::get<0>(expV_cache), std::get<1>(expV_cache), psi);
			psi = intermediate;
		}; 
	}
}

namespace qengine
{
	inline auto makeDefaultStepAlgorithm(const Hamiltonian2P& H)
	{
		return addImagBounds(splitStep(H), makeDefaultImagPot(H._potential()._serializedHilbertSpace()));
	}

	template<class PotentialFunction>
	auto makeDefaultStepAlgorithm(const HamiltonianFunction<Hamiltonian2P, PotentialFunction>& H)
	{
		return addImagBounds(splitStep(H), makeDefaultImagPot(H._H0()._potential()._serializedHilbertSpace()));
	}

	inline auto makeDefaultStepAlgorithm(const Hamiltonian2P& H, real dt)
	{
		return addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._potential()._serializedHilbertSpace()), dt);
	}

	template<class PotentialFunction>
	auto makeDefaultStepAlgorithm(const HamiltonianFunction<Hamiltonian2P, PotentialFunction>& H, real dt)
	{
		return addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._potential()._serializedHilbertSpace()), dt);
	}
}

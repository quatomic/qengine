﻿/* COPYRIGHT
 *
 * file="StateTransferProblemFactory.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/Spatial/HamiltonianFunction.h"
#include "src/Abstract/Physics/Spatial/Spectrum.impl.h"

#include "src/Abstract/OptimalControl/Problems/LinearStateTransferProblem.h"
#include "src/API/Common/Spatial/OptimalControlHelpers.h"

#include "src/API/TwoParticle/Hamiltonian2P.h"
#include "src/API/TwoParticle/defaultSteppingAlg.h"

namespace qengine
{
	template<class PotentialFunction, class dHdu>
	auto makeStateTransferProblem(
		const HamiltonianFunction<Hamiltonian2P, PotentialFunction>& H,
		const dHdu dV,
		const FunctionOfX<complex, Hamiltonian2P::SerializedHilbertSpace>& initialState,
		const FunctionOfX<complex, Hamiltonian2P::SerializedHilbertSpace>& targetState,
		const Control& x0
	)
	{
		using namespace spatial;
		const auto& dt = x0.dt();

		auto alg = addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._potential()._serializedHilbertSpace()), dt);
		auto algb = addImagBounds(splitStep(H, -dt), makeDefaultImagPot(H._H0()._potential()._serializedHilbertSpace()), dt);

		return makeLinearStateTransferProblem(alg, algb, dV, initialState, targetState, x0);
	}

	template<class SerializedHilbertSpace, class PotentialFunction>
	auto makeStateTransferProblem(
		const HamiltonianFunction<Hamiltonian2P, PotentialFunction>& H,
		const FunctionOfX<complex, SerializedHilbertSpace>& initialState,
		const FunctionOfX<complex, SerializedHilbertSpace>& targetState,
		const Control& x0
	)
	{
		auto dV = makeNumericDiffPotential(H._potentialFunction());
		return makeStateTransferProblem(H, dV, initialState, targetState, x0);
	}
}

﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace2P.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/Spatial/MakeKinetic.h"
#include "src/Abstract/Physics/Spatial/SerializedHilbertSpaceND.h"
#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"

#include "src/API/Common/Spatial/1D/LinearHamiltonian1D.h"
#include "src/API/TwoParticle/CommonDimensionSpace.h"
#include "src/API/TwoParticle/Hamiltonian2P.h"



namespace qengine
{
	class ApiHilbertSpace2P
	{
	public:
		using SerializedHilbertSpace = spatial::SerializedHilbertSpaceND<2, 1, 2>;

		ApiHilbertSpace2P(real xLower, real xUpper, count_t dimension, real kinematicFactor);

		const FunctionOfX<real, spatial::SubSpace<2, 1>>& x1() const { return x1_; }
		const FunctionOfX<real, spatial::SubSpace<2, 2>>& x2() const { return x2_; }

		const FunctionOfX<real, spatial::SubSpace<2, 1>>& spatialDimension1() const { return x1_; }
		const FunctionOfX<real, spatial::SubSpace<2, 2>>& spatialDimension2() const { return x2_; }

		const FunctionOfX<real, two_particle::CommonDimensionSpace>& xPotential() const { return xCommon_; }
		const FunctionOfX<real, two_particle::CommonDimensionSpace>& spatialDimensionPotential() const { return xCommon_; }

		LinearHamiltonian1D<spatial::SubSpace<2, 1>> T1() const { return internal::makeKinetic(serializedHilbertSpace_.get<1>()); }
		LinearHamiltonian1D<spatial::SubSpace<2, 2>> T2() const { return internal::makeKinetic(serializedHilbertSpace_.get<2>()); }
		Hamiltonian2P T() const;

		LinearHamiltonian1D<spatial::SubSpace<2, 1>> makeKinetic1() const { return internal::makeKinetic(serializedHilbertSpace_.get<1>()); }
		LinearHamiltonian1D<spatial::SubSpace<2, 2>> makeKinetic2() const { return internal::makeKinetic(serializedHilbertSpace_.get<2>()); }
		Hamiltonian2P makeKinetic() const { return T(); }



		real xLower() const { return s1.xLower; }
		real xUpper() const { return s1.xUpper; }
		count_t dim() const { return s1.dim; }
		real kinematicFactor() const { return s1.kinematicFactor; }
		real dx() const { return s1.dx(); }



		/// INTERNAL USE ONLY
	public:
		const spatial::SerializedHilbertSpaceND<2, 1, 2>& _serializedHilbertSpace() const { return serializedHilbertSpace_; }

	private:
		const SerializedHilbertSpace serializedHilbertSpace_;

		const spatial::SubSpace<2, 1>& s1 = serializedHilbertSpace_.get<1>();
		const spatial::SubSpace<2, 2>& s2 = serializedHilbertSpace_.get<2>();

		const FunctionOfX<real, spatial::SubSpace<2, 1>> x1_;
		const FunctionOfX<real, spatial::SubSpace<2, 2>> x2_;

		const two_particle::CommonDimensionSpace sCommon_;

		const FunctionOfX<real, two_particle::CommonDimensionSpace> xCommon_;
	};

	bool operator==(const ApiHilbertSpace2P& left, const ApiHilbertSpace2P& right);
	bool operator!=(const ApiHilbertSpace2P& left, const ApiHilbertSpace2P& right);

	namespace two_particle
	{
		ApiHilbertSpace2P makeHilbertSpace(real xLower, real xUpper, count_t dimension, real kinematicFactor = 0.5);


		template<class N>
		PointInteraction<N> makeConstantPointInteraction(const ApiHilbertSpace2P& s, const N interactionStrength)
		{
			return PointInteraction<N>(0.0 * s.xPotential() + interactionStrength);
		}
	}
}

namespace qengine
{
	namespace internal
	{
		template<>
		struct extract_serialized_hilbertspace<ApiHilbertSpace2P>
		{
			using type = spatial::SerializedHilbertSpaceND<2, 1, 2>;
		};
	}
}

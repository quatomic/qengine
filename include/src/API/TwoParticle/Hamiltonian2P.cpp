﻿/* COPYRIGHT
 *
 * file="Hamiltonian2P.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/TwoParticle/Hamiltonian2P.h"

#include "src/Utility/qengine_assert.h"

#include "src/Abstract/Physics/Spatial/Spectrum.impl.h"

namespace qengine
{
	template <typename ScalarType>
	void Hamiltonian2P::applyInPlace(FunctionOfX<ScalarType, SerializedHilbertSpace>& f) const
	{
		qengine_assert(f._serializedHilbertSpace() == serializedHilbertSpace_, "wrong hilbertspace!");
		hamiltonianMatrix_.multiplyInPlace(f.vec());
	}

	template void Hamiltonian2P::applyInPlace(FunctionOfX<real, Hamiltonian2P::SerializedHilbertSpace>&) const;
	template void Hamiltonian2P::applyInPlace(FunctionOfX<complex, Hamiltonian2P::SerializedHilbertSpace>&) const;

	spatial::Spectrum<real, Hamiltonian2P::SerializedHilbertSpace> Hamiltonian2P::makeSpectrum(const count_t largestEigenvalue) const
	{
		return spatial::Spectrum<real, SerializedHilbertSpace>(internal::getSpectrum_herm(hamiltonianMatrix_, largestEigenvalue, serializedHilbertSpace_.normalization()), serializedHilbertSpace_);
	}

	spatial::Spectrum<real, Hamiltonian2P::SerializedHilbertSpace> Hamiltonian2P::makeSpectrum(const count_t largestEigenvalue, const count_t nExtraStates, const real tolerance) const
	{
		return spatial::Spectrum<real, SerializedHilbertSpace>(internal::getSpectrum_herm(hamiltonianMatrix_, largestEigenvalue, serializedHilbertSpace_.normalization(), nExtraStates, tolerance), serializedHilbertSpace_);
	}
}

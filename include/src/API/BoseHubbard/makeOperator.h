﻿/* COPYRIGHT
 *
 * file="makeOperator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/SecondQuantization/FockOperators.h"

#include "src/API/BoseHubbard/ApiHilbertSpace.h"

namespace qengine
{
	Operator<internal::SqDenseMat, real, internal::SerializedBHHilbertSpace> makeDenseOperator(const bosehubbard::ApiHilbertSpace& s, std::initializer_list<std::initializer_list<real>> mat);
	Operator<internal::SqDenseMat, complex, internal::SerializedBHHilbertSpace> makeDenseOperator(const bosehubbard::ApiHilbertSpace& s, std::initializer_list<std::initializer_list<complex>> mat);

	Operator<internal::SqDenseMat, complex, internal::SerializedBHHilbertSpace> makeDenseOperator(const bosehubbard::ApiHilbertSpace& s, const FockOperator& op);

	Operator<internal::SqSparseMat, complex, internal::SerializedBHHilbertSpace> makeOperator(const bosehubbard::ApiHilbertSpace& s, const FockOperator& op);
	Operator<internal::SqSparseMat, complex, internal::SerializedBHHilbertSpace> makeOperator(const bosehubbard::ApiHilbertSpace& s, const FockOperator& op);
}

﻿/* COPYRIGHT
 *
 * file="TimeSteppers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/TimeStepper.h"

#include "src/Abstract/Physics/SecondQuantization/State.h"
#include "src/Abstract/Physics/SecondQuantization/KrylovStepping.h"
#include "src/Abstract/Physics/SecondQuantization/OperatorFunction.h"

#include "src/API/BoseHubbard/ApiHilbertSpace.h"

namespace qengine
{
	template<template<class> class MatType>
	auto makeTimeStepper(const Operator<MatType, real, internal::SerializedBHHilbertSpace>& H, const State<internal::SerializedBHHilbertSpace>& psi0, count_t krylovOrder)
	{
		return makeTimeStepper(krylov(H, krylovOrder), psi0);
	}

	template<template<class> class MatType>
	auto makeFixedTimeStepper(const Operator<MatType, real, internal::SerializedBHHilbertSpace>& H, const State<internal::SerializedBHHilbertSpace>& psi0, count_t krylovOrder, real dt)
	{
		return makeFixedTimeStepper(krylov(H, krylovOrder, dt), psi0);
	}

	template<class HamiltonianFunction, class... Params>
	auto makeTimeStepper(const OperatorFunction<HamiltonianFunction, Params...>& H, const State<internal::SerializedBHHilbertSpace>& psi0, count_t krylovOrder, const Tuple<Params...>& initialParams)
	{
		return makeTimeStepper(krylov(H, krylovOrder), psi0, initialParams);
	}

	template<class HamiltonianFunction, class... Params>
	auto makeFixedTimeStepper(const OperatorFunction<HamiltonianFunction, Params...>& H, const State<internal::SerializedBHHilbertSpace>& psi0, count_t krylovOrder, real dt, const Tuple<Params...>& initialParams)
	{
		return makeFixedTimeStepper(krylov(H, krylovOrder, dt), psi0, initialParams);
	}

	template<class OpFunction, class... Params, typename = std::enable_if_t<repl17::check_all_true(std17::is_same_v<Params, real>...)>>
	auto makeTimeStepper(const OperatorFunction<OpFunction, Params...>& H, const State<internal::SerializedBHHilbertSpace>& psi0, count_t krylovOrder, const RVec& initialParams)
	{
		return makeTimeStepper(H, psi0, krylovOrder, internal::tupleFromVec<Params...>(initialParams));
	}

	template<class OpFunction, class... Params, typename = std::enable_if_t<repl17::check_all_true(std17::is_same_v<Params, real>...)>>
	auto makeFixedTimeStepper(const OperatorFunction<OpFunction, Params...>& H, const State<internal::SerializedBHHilbertSpace>& psi0, count_t krylovOrder, const real dt, const RVec& initialParams)
	{
		return makeFixedTimeStepper(H, psi0, krylovOrder, dt, internal::tupleFromVec<Params...>(initialParams));
	}

	template<class HamiltonianFunction, class... Params>
	auto makeTimeStepper(const OperatorFunction<HamiltonianFunction, Params...>& H, const State<internal::SerializedBHHilbertSpace>& psi0, count_t krylovOrder)
	{
		return makeTimeStepper(H, psi0, krylovOrder, H.initialParams());
	}

	template<class HamiltonianFunction, class... Params>
	auto makeFixedTimeStepper(const OperatorFunction<HamiltonianFunction, Params...>& H, const State<internal::SerializedBHHilbertSpace>& psi0, count_t krylovOrder, real dt)
	{
		return makeFixedTimeStepper(H, psi0, krylovOrder, dt, H.initialParams());
	}

}

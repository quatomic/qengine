﻿/* COPYRIGHT
 *
 * file="SerializedHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/Physics/SecondQuantization/is2ndQSerializedHilbertSpace.h"

namespace qengine
{
	namespace internal
	{
		struct SerializedBHHilbertSpace
		{
			const count_t nParticles;
			const count_t nSites;
			const count_t dim;
		};

		inline bool operator==(const SerializedBHHilbertSpace& left, const SerializedBHHilbertSpace& right)
		{
			return left.nParticles == right.nParticles && left.nSites == right.nSites;
		}
		inline bool operator!=(const SerializedBHHilbertSpace& left, const SerializedBHHilbertSpace& right)
		{
			return !(left == right);
		}

		template<>
		struct isLabelledAs_SerializedHilbertSpace<internal::SerializedBHHilbertSpace> : std::true_type{};
	}
}

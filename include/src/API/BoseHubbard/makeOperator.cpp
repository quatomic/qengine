﻿/* COPYRIGHT
 *
 * file="makeOperator.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/BoseHubbard/makeOperator.h"

#include <armadillo>

#include "src/Utility/LinearAlgebra/SqDenseMat.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include "src/Abstract/Physics/SecondQuantization/Operator.impl.h"
#include "src/Abstract/Physics/SecondQuantization/Spectrum.impl.h"
#include "src/API/NLevel/ApiHilbertSpace.h"


namespace qengine
{
	Operator<internal::SqDenseMat, real, internal::SerializedBHHilbertSpace> makeDenseOperator(const bosehubbard::ApiHilbertSpace& s, std::initializer_list<std::initializer_list<real>> mat)
	{
		auto rep = RMat(mat);
		return internal::makeOperator(internal::SqDenseMat<real>(std::move(rep)), s._serializedHilbertSpace());
	}

	Operator<internal::SqDenseMat, complex, internal::SerializedBHHilbertSpace> makeDenseOperator(const bosehubbard::ApiHilbertSpace& s, std::initializer_list<std::initializer_list<complex>> mat)
	{
		auto rep = CMat(mat);
		return internal::makeOperator(internal::SqDenseMat<complex>(std::move(rep)), s._serializedHilbertSpace());
	}

	Operator<internal::SqDenseMat, complex, internal::SerializedBHHilbertSpace> makeDenseOperator(const bosehubbard::ApiHilbertSpace& s, const FockOperator& op)
	{
		auto rep = arma::cx_mat(s.nBasisElements(), s.nBasisElements(), arma::fill::zeros);

		for (auto i = 0u; i < s.nBasisElements(); ++i)
		{
			auto res = op * s._basis().at(i);
			for (const auto& a : res.factors())
			{
				if (s._isInBasis(a.first))
				{
					rep.at(s._get(a.first), i) = a.second;
				}
			}
		}

		return internal::makeOperator(internal::SqDenseMat<complex>(rep), s._serializedHilbertSpace());
	}
	
	Operator<internal::SqSparseMat, complex, internal::SerializedBHHilbertSpace> makeOperator(const bosehubbard::ApiHilbertSpace& s, const FockOperator& op)
	{
		auto rep = arma::sp_cx_mat(s.nBasisElements(), s.nBasisElements());

		for (auto i = 0u; i < s.nBasisElements(); ++i)
		{
			auto res = op * s._basis().at(i);
			for (const auto& a : res.factors())
			{
				if (s._isInBasis(a.first))
				{
					rep.at(s._get(a.first), i) = a.second;
				}
			}
		}

		return internal::makeOperator(internal::SqSparseMat<complex>(rep), s._serializedHilbertSpace());
	}
}

﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <unordered_map>

#include "src/Utility/LinearAlgebra/SqSparseMat.h"

#include "src/Abstract/Physics/SecondQuantization/isApiHilbertSpace.h"

#include "src/Abstract/Physics/SecondQuantization/Operator.h"
#include "src/Abstract/Physics/SecondQuantization/FockState.h"

#include "src/API/BoseHubbard/hashing.h"
#include "src/API/BoseHubbard/SerializedHilbertSpace.h"

namespace qengine
{
	namespace bosehubbard
	{
		class ApiHilbertSpace
		{
		public:
			using map_t = std::unordered_map<internal::FockState, count_t, detail::Hash>;

		public:
			using SerializedHilbertSpace = internal::SerializedBHHilbertSpace;

			ApiHilbertSpace(count_t nParticles, count_t nSites);

			ApiHilbertSpace(const ApiHilbertSpace& other) = default;

			ApiHilbertSpace& operator=(const ApiHilbertSpace& other) = default;

			/// GETTERS
			count_t nParticles() const;
			count_t nSites() const;
			count_t nBasisElements() const;
			count_t dim() const;

			/// SPECIAL OBJECT CREATION FUNCTIONS
		public:
			Operator<internal::SqSparseMat, real, SerializedHilbertSpace> makeHoppingOperator(bool usePeriodicBounds = true) const;
			Operator<internal::SqDenseMat, real, SerializedHilbertSpace> makeDenseHoppingOperator(bool usePeriodicBounds = true) const;


			Operator<internal::SqDiagMat, real, SerializedHilbertSpace> makeOnSiteOperator() const;
			Operator<internal::SqDiagMat, real, SerializedHilbertSpace> transformPotential(const RVec& vec) const;


			State<SerializedHilbertSpace> makeMott() const;
			State<SerializedHilbertSpace> makeSuperFluid() const;


			CMat singleParticleDensityMatrix(const State<SerializedHilbertSpace>& state) const;

			/// INTERNAL USE
		public:
			count_t _get(const internal::FockState& f) const;
			bool _isInBasis(const internal::FockState& f) const;
			std::vector<internal::FockState> _basis() const;
			map_t _stateMap() const;
			const RMat& _basisMat() const { return basisMat_; }

			const internal::SerializedBHHilbertSpace& _serializedHilbertSpace() const { return serializedHilbertSpace_; }

		private:
			/// DATAMEMBERS
			internal::SerializedBHHilbertSpace serializedHilbertSpace_;

			std::vector<internal::FockState> basis_;
			map_t stateMap_;
			RMat basisMat_;

		};


		inline ApiHilbertSpace makeHilbertSpace(const count_t nParticles, const count_t nSites) { return ApiHilbertSpace(nParticles, nSites); }
	}
}

namespace qengine
{
	namespace second_quantized
	{
		template<>
		struct has2ndQApiHilbertSpaceLabel<bosehubbard::ApiHilbertSpace> : std::true_type{};
	}
}

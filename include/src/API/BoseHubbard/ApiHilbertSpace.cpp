﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/BoseHubbard/ApiHilbertSpace.h"

#include <armadillo>

#include "src/Utility/cpp17Replacements/constexpr_ternary.h"

#include "src/Abstract/Physics/SecondQuantization/Operator.impl.h"
#include "src/Abstract/Physics/General/OperatorFunctions.h"

#include "src/API/BoseHubbard/makeOperator.h"
#include "src/API/Common/SecondQuantized/makeState.h"

namespace qengine
{
	namespace bosehubbard
	{
		namespace
		{
			count_t binomialCoeff(const count_t n, count_t k)
			{
				count_t res = 1;

				if (k > n - k)
					k = n - k;
				for (count_t i = 0; i < k; ++i)
				{
					res *= (n - i);
					res /= (i + 1);
				}
				return res;
			}

			count_t calculateDim(const count_t n_particles, const count_t n_sites, const count_t cutoff)
			{
				count_t dim = 0;
				const auto j_max = floor(n_particles / (cutoff + 1));

				for (count_t j = 0; j <= j_max; ++j) {
					const auto factor1 = binomialCoeff(n_sites + n_particles - 1 - j * (cutoff + 1), n_sites - 1);
					const auto factor2 = binomialCoeff(n_sites, j);
					dim += count_t(std::pow(-1, j))*factor1*factor2;
				}
				return dim;
			}

			bool acceptStateInBasis(const internal::FockState& state, const count_t cutoff)
			{
				for (auto i = 0u; i < state._quanta().size(); i++)
				{
					if (state._quanta().at(i) > cutoff) return false;
				}
				return true;
			}

			std::vector<internal::FockState> makeBasis(const count_t n_particles, const count_t n_sites, const count_t cutoff)
			{
				const auto dimWithoutCutoff = calculateDim(n_particles, n_sites, n_particles);

				auto basis = std::vector<internal::FockState>(calculateDim(n_particles, n_sites, cutoff), internal::FockState(std::vector<count_t>(n_sites, 0)));

				count_t id = 0;
				count_t basisIndex = 0;
				//           implement:  distributes the particles for the lowest ranking state with cut off.
				for (auto i = 0u; i < n_particles; i++)
				{
					if (basis.at(id)._quanta().at(0) == n_particles) id++; // maybe change nParticles->cutoff_ in this line later
					basis.at(id)._quanta().at(0) += 1;
				}

				id = 0;

				// Lowest ranking state
				auto state = basis.at(0);
				if (acceptStateInBasis(state, cutoff))
				{
					basis.at(basisIndex) = state;
					basisIndex++;
				}

				for (count_t j = 1; j < dimWithoutCutoff; j++)
				{
					if (state._quanta().at(id) == n_particles)
					{
						state = internal::FockState(std::vector<count_t>(n_sites, 0));
						state._quanta().at(id + 1) = 1;
						state._quanta().at(0) = n_particles - 1;
						id = 0;
					}
					else
					{
						state._quanta().at(id + 1) = state._quanta().at(id + 1) + 1;

						if (state._quanta().at(id) == 1)
						{
							state._quanta().at(id) = 0;
							id = id + 1;
						}
						else
						{
							auto tmp_val = state._quanta().at(id);
							state._quanta().at(id) = 0;
							state._quanta().at(0) = tmp_val - 1;
							id = 0;
						}
					}

					if (acceptStateInBasis(state, cutoff))
					{
						basis.at(basisIndex) = state;
						basisIndex++;
					}
				}

				return basis;
			}




			ApiHilbertSpace::map_t makeStateMap(const std::vector<internal::FockState>& basis, const count_t cutoff)
			{
				auto stateMap = ApiHilbertSpace::map_t(11, detail::Hash(cutoff));

				for (count_t i = 0; i < basis.size(); ++i)
				{
					auto vec = basis.at(i);
					stateMap.emplace(vec, i);
				}

				return stateMap;
			}
		}

		RMat detail::rMatFromBasis(const std::vector<internal::FockState>& basis)
		{
			auto rmat = RMat(basis.front()._quanta().size(), basis.size());
			for (auto i = 0u; i < basis.size(); ++i)
			{
				const auto& basisVec = basis.at(i)._quanta();
				for (auto j = 0u; j < basisVec.size(); ++j)
				{
					rmat.at(j, i) = static_cast<real>(basisVec.at(j));
				}
			}
			return rmat;
		}

		ApiHilbertSpace::ApiHilbertSpace(const count_t n_particles, const count_t n_sites) :
            serializedHilbertSpace_{n_particles, n_sites, calculateDim(n_particles,n_sites,n_particles)},
			basis_(makeBasis(n_particles, n_sites, n_particles)),
			stateMap_(makeStateMap(basis_, n_particles)),
			basisMat_(detail::rMatFromBasis(basis_))
		{}

		count_t ApiHilbertSpace::nParticles() const
		{
			return serializedHilbertSpace_.nParticles;
		}

		count_t ApiHilbertSpace::nSites() const
		{
			return serializedHilbertSpace_.nSites;
		}

		std::vector<internal::FockState> ApiHilbertSpace::_basis() const
		{
			return basis_;
		}

		count_t ApiHilbertSpace::nBasisElements() const
		{
			return basis_.size();
		}

		count_t ApiHilbertSpace::dim() const
		{
			return basis_.size();
		}

		namespace
		{
			internal::SqDiagMat<real> makeOnSiteMatrix(const RMat& basis)
			{
				arma::rowvec s = arma::sum(0.5 * basis._mat() % (basis._mat() - 1.0));
				return internal::SqDiagMat<real>(RVec(s.t()));
			}
		}
		namespace hoppingMatrix
		{

			struct SitePair
			{
				count_t from;
				count_t to;
				real factor;
			};

			namespace
			{
				template <template <class> class MatType>
				MatType<real> makeWithoutCutoff(const std::vector<SitePair>& sitePairs, const RMat& basisMat)
				{
					const auto& basis = basisMat._mat();
					const auto basisSize = basisMat.n_cols();
					const auto nSites = basisMat.n_rows();

					auto indices = std::vector<arma::uvec>();
					indices.reserve(nSites);

					for (auto i_site = 0u; i_site < nSites; ++i_site)
					{
						indices.push_back(arma::find(basis.row(i_site)));
					}

					// The matrix of basisElements, where all zeros have been removed. The remaining elements 
					// are then pushed together, so e.g. 
					//  2, 1, 0 
					//  0, 1, 2
					// becomes 
					// 2, 1
					// 1, 2
					// This only works because we know that each site will have the same number nonzeros elements
					const arma::mat nonzeros = arma::reshape(arma::nonzeros(basis.t()), indices.at(0).size(), nSites);
					const arma::mat sqrts = arma::sqrt(nonzeros);

					auto M = arma::sp_mat(basisSize, basisSize);

					for (auto p : sitePairs)
					{
						M += arma::sp_mat(arma::join_cols(indices.at(p.from).t(), indices.at(p.to).t()),
							p.factor*(sqrts.col(p.from) % sqrts.col(p.to)),
							basisSize, basisSize
						);
					}

					return MatType<real>(internal::SqSparseMat<real>(M));
				}

				namespace
				{
					real hop(internal::FockState& state, const count_t i_from, const count_t i_to)
					{
						const auto factor = std::sqrt(state._quanta().at(i_to) + 1)*std::sqrt(state._quanta().at(i_from));

						state._quanta().at(i_from) -= 1;
						state._quanta().at(i_to) += 1;

						return factor;
					}
				}

				template <template <class> class MatType, class Map>
				MatType<real> makeWithCutoff(const std::vector<SitePair>& siteTuples, const count_t nBasisElements, const std::vector<internal::FockState>& basis, const Map& stateMap)
				{
					auto mat = repl17::constexpr_ternary(std::is_same<MatType<real>, internal::SqDenseMat<real>>(),
						[&](auto _) {return Matrix<real>(_(nBasisElements), _(nBasisElements), 0.0); },
						[&](auto _) {return internal::SparseMatrix<real>(_(nBasisElements), _(nBasisElements)); }
					);

					for (count_t i = 0; i < nBasisElements; ++i)
					{
						for (const auto pair : siteTuples)
						{
							auto basisState = basis.at(i);
							const auto factor = pair.factor*hop(basisState, pair.from, pair.to);

							try
							{
								auto j = stateMap.at(basisState);
								mat.at(i, j) += factor;
							}
							catch (std::exception&)
							{
								continue;
							}
						}
					}
					return MatType<real>(std::move(mat));
				}
			}


			std::vector<SitePair> makeDefaultFactorList(const count_t nSites, const bool usePeriodicBoundaryConditions)
			{
				std::vector<SitePair> siteTuples;
				for (count_t site = 0; site < nSites - 1; site++)
				{
					count_t site_i = site;
					count_t site_j = site + 1;

					siteTuples.push_back(SitePair{ site_i, site_j, 1.0 });
					siteTuples.push_back(SitePair{ site_j, site_i, 1.0 });
				}

				if (usePeriodicBoundaryConditions && (nSites > 2))
				{
					count_t site_i = 0;
					count_t site_j = nSites - 1;
					siteTuples.push_back(SitePair{ site_i, site_j, 1.0 });
					siteTuples.push_back(SitePair{ site_j, site_i, 1.0 });
				}

				return siteTuples;
			}
		}
		namespace
		{
			template <template <class> class MatType>
			MatType<real> makeHoppingMatrix(const ApiHilbertSpace& s, const bool usePeriodicBounds)
			{
				const auto factors = hoppingMatrix::makeDefaultFactorList(s.nSites(), usePeriodicBounds);

				return hoppingMatrix::makeWithoutCutoff<MatType>(factors, s._basisMat());
			}
		}
		Operator<internal::SqSparseMat, real, internal::SerializedBHHilbertSpace> ApiHilbertSpace::makeHoppingOperator(const bool usePeriodicBounds) const
		{
			return  internal::makeOperator(makeHoppingMatrix<internal::SqSparseMat>(*this, usePeriodicBounds), serializedHilbertSpace_);
		}

		Operator<internal::SqDenseMat, real, internal::SerializedBHHilbertSpace> ApiHilbertSpace::makeDenseHoppingOperator(const bool usePeriodicBounds) const
		{
			return internal::makeOperator(makeHoppingMatrix<internal::SqDenseMat>(*this, usePeriodicBounds), serializedHilbertSpace_);

		}

		Operator<internal::SqDiagMat, real, internal::SerializedBHHilbertSpace> ApiHilbertSpace::makeOnSiteOperator() const
		{
			return  internal::makeOperator(makeOnSiteMatrix(_basisMat()), serializedHilbertSpace_);
		}

		Operator<internal::SqDiagMat, real, internal::SerializedBHHilbertSpace> ApiHilbertSpace::transformPotential(const RVec& vec) const
		{
			const arma::vec result = arma::trans(_basisMat()._mat())*vec._vec();
			return  internal::makeOperator(internal::SqDiagMat<real>(RVec(result)), serializedHilbertSpace_);
		}

		State<internal::SerializedBHHilbertSpace> ApiHilbertSpace::makeMott() const
		{
			return makeState(*this, internal::FockState(std::vector<count_t>(nSites(), 1)));
		}

		State<internal::SerializedBHHilbertSpace> ApiHilbertSpace::makeSuperFluid() const
		{
			auto op = fock::zero();
			for (auto i = 0u; i < nSites(); ++i)
			{
				op += fock::c(i);
			}
			op = pow(op, nParticles());

			const auto zero = fock::state(std::vector<count_t>(nSites(), 0));

			return makeState(*this, op*zero, NORMALIZE);
		}

		namespace
		{
			CMat singleParticleDensityMatrixNoCutoff(const State<internal::SerializedBHHilbertSpace>& state, const RMat& basisMat)
			{

				const auto& basis = basisMat._mat();
				//const auto basisSize = basisMat.n_cols();
				const auto nSites = basisMat.n_rows();

				auto indices = std::vector<arma::uvec>();
				indices.reserve(nSites);

				for (auto i_site = 0u; i_site < nSites; ++i_site)
				{
					indices.push_back(arma::find(basis.row(i_site)));
				}

				// The matrix of basisElements, where all zeros have been removed. The remaining elements 
				// are then pushed together, so e.g. 
				//  2, 1, 0 
				//  0, 1, 2
				// becomes 
				// 2, 1
				// 1, 2
				// This only works because we know that each site will have the same number nonzeros elements
				const arma::mat nonzeros = arma::reshape(arma::nonzeros(basis.t()), indices.at(0).size(), nSites);
				const arma::mat sqrts = arma::sqrt(nonzeros);

				const auto& stateVec = state.vec()._vec();
				auto OperatedStates = arma::cx_mat(size(nonzeros));
				for (auto i_site = 0u; i_site < nSites; ++i_site)
				{
					OperatedStates.col(i_site) = stateVec.elem(indices.at(i_site)) % sqrts.col(i_site);
				}

				return CMat(arma::cx_mat(OperatedStates.t()*OperatedStates));
			}
		}


		CMat ApiHilbertSpace::singleParticleDensityMatrix(const State<internal::SerializedBHHilbertSpace>& state) const
		{
			//qengine_assert(nParticles_ == cutoff_, "Can only calculate single particle density matrix if there is not cutoff!");
			//return singleParticleDensityMatrixNaive(*this, state);
			return singleParticleDensityMatrixNoCutoff(state, basisMat_);
		}

		count_t ApiHilbertSpace::_get(const internal::FockState& f) const
		{
			return _stateMap().at(f);
		}

		bool ApiHilbertSpace::_isInBasis(const internal::FockState& f) const
		{
			if (f.nModes() != nSites()) return false;

			count_t sum = 0;
			for (const auto& a : f._quanta())
			{
				if (a > nParticles()) return false;
				if ((sum += a) > nParticles()) return false;
			}
			if (sum != nParticles()) return false;

			return true;
		}

		ApiHilbertSpace::map_t ApiHilbertSpace::_stateMap() const
		{
			return stateMap_;
		}
	}
}

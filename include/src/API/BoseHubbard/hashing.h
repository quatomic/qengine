﻿/* COPYRIGHT
 *
 * file="hashing.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/IntegerFunctions.h"

namespace qengine
{
	namespace bosehubbard
	{
		namespace detail
		{
			class Hash
			{
			public:
				Hash(const count_t cutoff) :basis_(cutoff + 1) {}

				std::size_t operator() (const internal::FockState& s) const
				{
					std::size_t hash = 0;
					for (auto i = 0u; i < s._quanta().size(); ++i)
					{
						hash += s._quanta().at(i)*internal::int_power(basis_, i);
					}
					return hash;
				}

			private:
				count_t basis_;
			};


			RMat rMatFromBasis(const std::vector<internal::FockState>& basis);
		}

	}
}

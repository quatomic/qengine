﻿/* COPYRIGHT
 *
 * file="GROUP_steepest.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/OptimalControl/Algorithms/SteepestDescent.h"
#include "src/Abstract/OptimalControl/Parameters/BasisControl.h"
#include "src/Abstract/OptimalControl/Problems/ProblemTraits.h"
#include "src/Abstract/OptimalControl/Problems/BasisProblem.h"
#include "src/Abstract/OptimalControl/Algorithms/DressedAlgorithm.h"
#include "src/Abstract/OptimalControl/Parameters/Basis.h"

#include "src/API/Common/OptimalControl/Policies/PolicyWrapper.h"
#include "src/API/Common/OptimalControl/Policies/Stoppers.h"
#include "src/API/Common/OptimalControl/Policies/Collectors.h"
#include "src/API/Common/OptimalControl/Policies/StepsizeFinders.h"
#include "src/API/Common/OptimalControl/Policies/BasisMakers.h"
#include "src/API/Common/OptimalControl/Policies/DressedRestarters.h"

#include "src/API/Common/OptimalControl/BasisFactory.h"
#include "src/API/Common/OptimalControl/makeAlgorithm.h"

namespace qengine
{
	namespace internal
	{
		template<class BasisProblem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy>
		auto makeGroup_steepest(const BasisProblem& basisProblem, const Stopper& stopper, const Collector& collector, const StepSizeFinder& stepSizeFinder, const InnerProductPolicy& innerProductPolicy, const GradientPolicy& gradientPolicy)
		{
			static_assert(internal::is_ParametrizedQOCProblem_v<BasisProblem>, "Problem does not fulfill the requirements for a parametrized quantum optimal control problem!");
			return SteepestDescent<BasisControl, BasisProblem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy>(basisProblem, stopper, collector, stepSizeFinder, innerProductPolicy, gradientPolicy);
		}

		inline auto makeGroupSteepestDefaults()
		{
            auto stopper = makeFidelityStopper(0.99) + makeIterationStopper(1000);

			auto collector = makeEmptyCollector();
			auto stepSizeFinder = makeInterpolatingStepSizeFinder(10, 1.0);

			// the order and number of these defaults has to correspond to the order of inputs into makeGrape_steepest
			return toTypeMap(
				stopper,
				collector,
				stepSizeFinder);
		}
	}

	template<class Problem, class... PolicyIDs, class... PolicyImpls>
	auto makeGroup_steepest(const Problem& problem, const Basis& basis, const internal::PolicyWrapper<PolicyIDs, PolicyImpls>&... inputs)
	{
		auto basisProblem = optimal_control::makeBasisProblem(problem, basis, problem.control());
		auto innerProductPolicy = [](const BasisControl& left, const BasisControl& right) -> real {return dot(left, right); };
		auto gradientPolicy = [](const auto& p) {return p.gradient(); };

		auto makeAlg = [&basisProblem, &innerProductPolicy, &gradientPolicy](const auto& stopper, const auto& collector, const auto& stepSizeFinder)
		{
			return internal::makeGroup_steepest(basisProblem, stopper, collector, stepSizeFinder, innerProductPolicy, gradientPolicy);
		};

		auto defaultsMap = internal::makeGrapeSteepestDefaults();

		return internal::makeAlgorithm(makeAlg, defaultsMap, inputs...);
	}
}

namespace qengine
{
	namespace internal
	{
		template<class Problem, class Stopper, class Collector, class StepSizeFinder, class BasisPolicy, class DressedRestarter>
		auto makeDressedGroup_steepest(const Problem& problem, const Stopper& stopper, const Collector& collector, const StepSizeFinder& stepSizeFinder, const BasisPolicy basisPolicy, const DressedRestarter& dressedRestarter)
		{
			auto groupStopper = makeStopper([dressedRestarter{ dressedRestarter }, stopper{ stopper }](const auto& alg) mutable
			{
				return dressedRestarter(alg) || stopper(alg);
			});

			auto resetPolicy = [basisPolicy](auto& alg)
			{
				alg.problem()._resetBasis(basisPolicy());
			};

			using ResetPolicy = decltype(resetPolicy);
			auto actualCollector = makeCollector([collector](const auto& alg)
			{
				collector(static_cast<const DressedAlgorithm<std17::remove_cvref_t<decltype(alg)>, Stopper, ResetPolicy>&>(alg));
			});

			auto alg = makeGroup_steepest(problem, basisPolicy(), groupStopper, actualCollector, makeStepSizeFinder(stepSizeFinder));

			return DressedAlgorithm<decltype(alg), Stopper, ResetPolicy>(std::move(alg), stopper, resetPolicy);
		}

		inline auto makeDGroupSteepestDefaults()
		{
            auto stopper = makeFidelityStopper(0.99) + makeIterationStopper(1000);

			auto collector = makeEmptyCollector();
			auto stepSizeFinder = makeInterpolatingStepSizeFinder(10, 1.0);
			auto dressedRestarter = makeCostDecreaseDressedRestarter(1e-5);

			// the order and number of these defaults has to correspond to the order of inputs into makeGrape_steepest
			return toTypeMap(
				stopper,
				collector,
				stepSizeFinder,
				dressedRestarter);
		}
	}


	template<class Problem, class BasisMakerImpl, class... PolicyIDs, class... PolicyImpls>
	auto makeDGroup_steepest(const Problem& problem, const internal::PolicyWrapper<internal::policy::BasisMakerID, BasisMakerImpl>& basisMaker, const internal::PolicyWrapper<PolicyIDs, PolicyImpls>&... inputs)
	{
		auto makeAlg = [&problem, &basisMaker](const auto& stopper, const auto& collector, const auto& stepSizeFinder, const auto dressedRestarter)
		{
			return internal::makeDressedGroup_steepest(problem, stopper, collector, stepSizeFinder, basisMaker.f, dressedRestarter);
		};

		auto defaultsMap = internal::makeDGroupSteepestDefaults();

		return internal::makeAlgorithm(makeAlg, defaultsMap, inputs...);
	}
}

﻿/* COPYRIGHT
 *
 * file="GroupHelpers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <iostream>

#include "src/Abstract/OptimalControl/Parameters/Control.h"

namespace qengine
{

    Control makeSigmoidShapeFunction(const Control& t, const real a = 0.0025 , const real S1 = 0.001, const real S2 = 1 - 0.001);



	inline auto makeCostDecreaseDressedStopper(real costDecreaseTolerance = 1e-7, const bool verbose = false) 
	{
		return [previousCost{ std::numeric_limits<real>::max() }, costDecreaseTolerance{ costDecreaseTolerance }, verbose](const auto& alg) mutable
		{
			auto cost = alg.problem().cost();
			auto costDecrease = previousCost - cost;
			previousCost = cost;
			if (costDecrease < costDecreaseTolerance)
			{
				if(verbose) std::cout << "restarting GROUP algorithm: cost decrease < " << costDecreaseTolerance << std::endl;
				return true;
			}
			return false;
		};
	}
}

﻿/* COPYRIGHT
 *
 * file="GroupHelpers.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/GROUP/GroupHelpers.h"

namespace qengine
{

    Control makeSigmoidShapeFunction(const Control& t, const real a, const real S1, const real S2)
    {
        // Creates a sigmoid shape function that is initially satisfies:
        // S(0)   = S1        (is later set to exactly zero)
        // S(a*T) = S2

        // The default parameters makes a sigmoid function that is 0 at t=0 and attains the value 0.999 at t = 0.0025 T.

        const real duration = (t.getBack().at(0)-t.getFront().at(0));
        const real q = std::log(1.0/S1 - 1.0);
        const real k = -1.0/(a*duration)*(std::log(1.0/S2 -1.0)  - q);
        auto shapeFunction = 1.0 / (1.0 + qengine::exp(-(k*t - q)));
        shapeFunction.at(0) = 0.0;
        return shapeFunction * invert(shapeFunction);
    }
}

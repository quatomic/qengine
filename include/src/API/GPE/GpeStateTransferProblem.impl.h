﻿/* COPYRIGHT
 *
 * file="StateTransferProblem.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/API/GPE/GpeStateTransferProblem.h"

namespace qengine
{
	template <class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	GpeStateTransferProblem<DHdu, State, DynamicStateForward, BackwardStepper>::GpeStateTransferProblem(const DHdu& dHdu,
		const State&
		targetState,
		const
		DynamicStateForward
		& psi,
		const
		BackwardStepper&
		backwardStepper,
		const Control&
		initialControl) :
		psi_(psi),
		targetState_(targetState),
		backwardStepper_(backwardStepper),
		control_(initialControl),
		initialControl_(initialControl),
		fidelity_(0),
		isFidelityUpdated_(false),
		dHdu_(dHdu),
		isGradUpdated_(false),
		chiSteps_(0)
	{
		qengine_assert(initialControl.size() >= 3, "control must have at least size 3");
		psi_.retargetControl(control_);
	}

	template <class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	GpeStateTransferProblem<DHdu, State, DynamicStateForward, BackwardStepper>::GpeStateTransferProblem(
		const GpeStateTransferProblem& other) : psi_{ other.psi_ },
		targetState_{ other.targetState_ },
		backwardStepper_{ other.backwardStepper_ },
		control_{ other.control_ },
		initialControl_{ other.initialControl_ },
		fidelity_{ other.fidelity_ },
		isFidelityUpdated_{ other.isFidelityUpdated_ },
		dHdu_{ other.dHdu_ },
		isGradUpdated_{ other.isGradUpdated_ },
		grad_{ other.grad_ },
		chiSteps_{ other.chiSteps_ }
	{
		psi_.retargetControl(control_);
	}

	template <class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	GpeStateTransferProblem<DHdu, State, DynamicStateForward, BackwardStepper>::GpeStateTransferProblem(
		GpeStateTransferProblem&& other) noexcept : psi_{ std::move(other.psi_) },
		targetState_{ std::move(other.targetState_) },
		backwardStepper_{ std::move(other.backwardStepper_) },
		control_{ std::move(other.control_) },
		initialControl_{ std::move(other.initialControl_) },
		fidelity_{ other.fidelity_ },
		isFidelityUpdated_{ other.isFidelityUpdated_ },
		dHdu_{ std::move(other.dHdu_) },
		isGradUpdated_{ other.isGradUpdated_ },
		grad_{ std::move(other.grad_) },
		chiSteps_{ other.chiSteps_ }
	{
		psi_.retargetControl(control_);
	}

	template <class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	void GpeStateTransferProblem<DHdu, State, DynamicStateForward, BackwardStepper>::update(const Control& control)
	{
		qengine_assert(control.size() >= 3, "control must have at least size 3");
		qengine_assert(control.size() == control_.size(), "control cannot change length!");
		qengine_assert(control.paramCount() == control_.paramCount(), "control cannot change paramCount!");
		control_ = control;
		psi_.notifyOfChange();

		isFidelityUpdated_ = false;
		isGradUpdated_ = false;
	}

	template <class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	Control GpeStateTransferProblem<DHdu, State, DynamicStateForward, BackwardStepper>::gradient() const
	{
		using namespace std::complex_literals;
		grad_ = Control::zeros(control_.size(), control_.paramCount(), control_.dt());

		auto overlapFactor = overlap(targetState_, psi_(control_.size() - 1));

		backwardStepper_.reset(1i * overlapFactor * targetState_, control_.getBack(), psi_(control_.size() - 1));

		for (auto i = control_.size() - 2; i >= 1; --i)
		{
			backwardStepper_.step({ control_.at(i), psi_(i) });
			++chiSteps_;
			for (auto j = 0u; j < control_.paramCount(); ++j)
			{
				auto dV = dHdu_.at(j)(control_.get(i));
				grad_.at(i).at(j) = -control_.dt() * std::real(overlap(psi_(i), dV * backwardStepper_.state()));
			}
		}

		backwardStepper_.step({ control_.at(0), psi_(0) });

		return grad_;
	}

	template <class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	real GpeStateTransferProblem<DHdu, State, DynamicStateForward, BackwardStepper>::fidelity() const
	{
		if (!isFidelityUpdated_)
		{
			fidelity_ = qengine::fidelity(psi_(control_.size() - 1), targetState_);
			isFidelityUpdated_ = true;
		}
		return fidelity_;
	}

	template <class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	void GpeStateTransferProblem<DHdu, State, DynamicStateForward, BackwardStepper>::resetPropagationCount()
	{
		psi_.resetPropagationCount();
		chiSteps_ = 0;
	}

	template <class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	const std::vector<State>& GpeStateTransferProblem<DHdu, State, DynamicStateForward, BackwardStepper>::psis() const
	{
		psi_.forceFullUpdate();
		return psi_.cache();
	}
}

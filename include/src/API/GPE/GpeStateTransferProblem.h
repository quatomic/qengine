﻿/* COPYRIGHT
 *
 * file="StateTransferProblem.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/API/GPE/GpeStateTransferProblem.fwd.h"

#include "src/NumberTypes.h"

#include "src/Abstract/Physics/General/TimeStepper.h"

#include "src/Abstract/OptimalControl/Problems/DynamicState.h"
#include "src/Abstract/OptimalControl/Parameters/Control.h"

#include "src/API/GPE/AdjointSplitstep.h"

namespace qengine
{
	template<class DHdu, class State, class DynamicStateForward, class BackwardStepper>
	class GpeStateTransferProblem
	{
	public:
		GpeStateTransferProblem(const DHdu& dHdu, const State& targetState, const DynamicStateForward& psi,
			const BackwardStepper& backwardStepper, const Control& initialControl);

		GpeStateTransferProblem(const GpeStateTransferProblem& other);

		GpeStateTransferProblem(GpeStateTransferProblem&& other) noexcept;

		/// 
	public:

		void update(const Control& control);

		real cost() const { return 0.5*(1.0 - fidelity()); }
		Control gradient() const;
		Control control() const { return control_; }
		real fidelity() const;

		count_t nPropagationSteps() const { return psi_.nPropagations() + chiSteps_; }
		void resetPropagationCount();

		double dt() const { return control_.dt(); }
		const Control& initialControl() const { return initialControl_; }
		const Control& u0() const { return initialControl_; }
		DHdu dHdu() const { return dHdu_; }

		const std::vector<State>& psis() const;

		/// INTERNAL USE ONLY
	public:
		Control _params() const { return control(); }
        const DynamicStateForward& _dynamicStateForward() const {return psi_;}
	private:
		mutable DynamicStateForward psi_;

		State targetState_;
		mutable BackwardStepper backwardStepper_;

		Control control_;
		const Control initialControl_;

		mutable real fidelity_;
		mutable bool isFidelityUpdated_;

		const DHdu dHdu_;

		mutable bool isGradUpdated_;
		mutable Control grad_;

		mutable count_t chiSteps_;
	};

	template<class ForwardAlgorithm, class BackwardAlgorithm, class DHdu, class State>
	auto makeGpeStateTransferproblem(const ForwardAlgorithm& alg, const BackwardAlgorithm& algb, const DHdu& dV, const State& initialState, const State& targetState, const Control& x0)
	{
		//static_assert(is_callable_v<ForwardAlgorithm, void(State&, RVec)>, "alg must be callable as (State&, RVec) --> void");
		//static_assert(is_callable_v<BackwardAlgorithm, void(State&, std::tuple< RVec, State>)>, "alg must be callable as (State&, std::tuple<RVec, State>) --> void");
		auto forwardStepper = qengine::makeFixedTimeStepper(internal::forceCopy(alg), toComplex(initialState), makeTuple(x0.getFront()));
		auto psi = optimal_control::makeDynamicState<true>(forwardStepper, toComplex(initialState), x0);


		auto backwardStepper = qengine::makeFixedTimeStepper(internal::forceCopy(algb), toComplex(targetState), makeTuple(x0.getBack(), targetState));


		return GpeStateTransferProblem<DHdu, decltype(toComplex(initialState)), decltype(psi), decltype(backwardStepper)>(dV, targetState, psi, backwardStepper, x0);
	}

}

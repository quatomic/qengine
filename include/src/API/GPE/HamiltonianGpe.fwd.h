﻿/* COPYRIGHT
 *
 * file="HamiltonianGpe.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"
#include "src/Abstract/Physics/Spatial/isHamiltonian.h"

namespace qengine 
{
	template<class SerializedHilbertSpace>
	class HamiltonianGpe;
}

namespace qengine
{
	namespace spatial
	{
		template<class SerializedHilbertSpace>
		struct isLabelledAs_Hamiltonian<HamiltonianGpe<SerializedHilbertSpace>>:std::true_type{};
	}
}
namespace qengine
{
	namespace internal
	{
		template<class SerializedHilbertSpace>
		struct isLabelledAs_Operator<HamiltonianGpe<SerializedHilbertSpace>> : std::true_type {};

		template<class SerializedHilbertSpace>
		struct isLabelledAs_Linear<HamiltonianGpe<SerializedHilbertSpace>> : std::true_type {};

		template<class SerializedHilbertSpace>
		struct isLabelledAs_GuaranteedHermitian<HamiltonianGpe<SerializedHilbertSpace>> : std::true_type {};

		template<class SerializedHilbertSpace>
		struct extract_serialized_hilbertspace<HamiltonianGpe<SerializedHilbertSpace>>
		{
			using type = SerializedHilbertSpace;
		};
	}
}

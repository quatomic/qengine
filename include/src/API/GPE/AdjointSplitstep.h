﻿/* COPYRIGHT
 *
 * file="AdjointSplitstep.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/API/GPE/HamiltonianGpe.h"
#include "src/Abstract/Physics/Spatial/HamiltonianFunction.h"
#include "src/API/Common/Spatial/1D/TimeSteppersCommon.h"
#include "src/Abstract/Physics/Spatial/SplitStep.h"

namespace qengine
{
	template<class Potential, class Identifier>
	auto gpeAdjointSplitStep(const HamiltonianFunction<HamiltonianGpe<spatial::SerializedHilbertSpace1D<Identifier>>, Potential>& H, const real dt)
	{
		qengine_assert(dt < 0, "dt must be smaller than 0!");

		using namespace std::complex_literals;

		const auto T = oneD::makePSpaceKinetic(H._H0()._serializedHilbertSpace());
		auto expT = exp(-1i * dt*T);
		auto applyExpT = [expT](auto& psi)
		{
			auto intermediate = expT * psi;
			psi = intermediate;
		};

		return[splitStepAlg{ SplitStepAlgorithm{} }, beta{ H._H0()._beta() }, V{ H._potentialFunction() }, dt, applyExpT](auto psiAndParamInit)
		{
			return[=, psi_cache = std::get<1>(psiAndParamInit.data), V_cache = V(std::get<0>(psiAndParamInit.data))](FunctionOfX<complex, spatial::SerializedHilbertSpace1D<Identifier>>& chi, const auto& psiAndParam) mutable
			{
				auto applyExpV = [beta, dt](auto& chi, const auto& psi, const auto& V)
				{
					using namespace std::complex_literals;

					auto psiSq = psi * psi;

					auto ar = beta * re(psiSq);
					auto A = 2 * beta*psi.absSquare() + V;
					auto ai = beta * im(psiSq);

					auto uNorm = sqrt(ar*ar + A * A + ai * ai);
					//std::cout << uNorm << std::endl;

					//std::cout.precision(10);
					auto c = cos(-uNorm * std::abs(dt) / 2.0);
					//std::cout << 1-c << std::endl;
					auto s = sin(-uNorm * std::abs(dt) / 2.0) / uNorm;
					//std::cout << s << std::endl;


					auto chi_r = re(chi);
					//std::cout << chi_r << std::endl;
					auto chi_i = im(chi);
					//std::cout << chi_i << std::endl;

					auto chi_r_new = c * chi_r + s * (ai*chi_r + (A - ar)*chi_i);
					//std::cout << chi_r_new << std::endl;
					auto chi_i_new = c * chi_i - s * (ai*chi_i + (A + ar)*chi_r);
					//std::cout << chi_i_new << std::endl;

					auto intermediate = chi_r_new + 1i * chi_i_new;

					//std::cout << intermediate << std::endl;

					chi = intermediate;
				};

				applyExpV(chi, psi_cache, V_cache);

				splitStepAlg.iFFT_expT_FFT(chi, applyExpT);

				const auto& paramTo = std::get<0>(psiAndParam.data);
				const auto& psiTo = std::get<1>(psiAndParam.data);

				V_cache = V(paramTo);
				psi_cache = psiTo;
				applyExpV(chi, psi_cache, V_cache);
			};
		};
	}

}

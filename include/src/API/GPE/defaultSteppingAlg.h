﻿/* COPYRIGHT
 *
 * file="defaultSteppingAlg.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/Constants.h"

#include "src/Abstract/Physics/General/TimeStepper.h"

#include "src/Abstract/Physics/Spatial/SplitStep.h"
#include "src/Abstract/Physics/Spatial/HamiltonianFunction.h"

#include "src/API/Common/Spatial/1D/TimeSteppersCommon.h"
#include "src/API/GPE/HamiltonianGpe.h"

namespace qengine
{
	template<class SerializedHilbertSpace>
	auto makeSplitStepAlgorithm(const HamiltonianGpe<SerializedHilbertSpace>& H)
	{
		return[V{ H._potential() }, T{ oneD::makePSpaceKinetic(H._serializedHilbertSpace()) }, beta{ H._beta() }, splitStepAlg{ SplitStepAlgorithm{} }](auto& psi, real dt)
		{
			using namespace std::complex_literals;

			auto expT = exp(-1i * dt*T);
			auto applyExpT = oneD::makeApplier(expT);

			auto applyExpV = [beta, V, dt](auto& psi)
			{
				auto intermediate = exp(-1i * 0.5*dt*(V + beta * psi.absSquare()))*psi;
				psi = intermediate;
			};

			splitStepAlg(psi, applyExpV, applyExpT, applyExpV);
		};
	}


	template<class SerializedHilbertSpace>
	auto makeSplitStepAlgorithm(const HamiltonianGpe<SerializedHilbertSpace>& H, const real dt)
	{
		using namespace std::complex_literals;
		const auto T = oneD::makePSpaceKinetic(H._serializedHilbertSpace());
		auto expT = exp(-1i * dt*T);
		auto applyExpT = [expT](auto& psi)
		{
			auto intermediate = expT * psi;
			psi = intermediate;
		};

		auto minushalfIdt = -1i * 0.5*dt;
		return[V{ H._potential() }, minushalfIdt, applyExpT, beta{ H._beta() }, splitStepAlg{ SplitStepAlgorithm{} }](auto& psi)
		{
			auto applyExpV = [beta, V, minushalfIdt](auto& psi)
			{
				auto intermediate = exp(minushalfIdt*(V + beta * psi.absSquare()))*psi;
				psi = intermediate;
			};

			splitStepAlg(psi, applyExpV, applyExpT, applyExpV);
		};
	}

	template<class SerializedHilbertSpace, class PotentialFunction, class... Params>
	auto makeSplitStepAlgorithm(const HamiltonianFunction<HamiltonianGpe<SerializedHilbertSpace>, PotentialFunction>& H, const Tuple<Params...>& initialParams)
	{

		using namespace std::complex_literals;
		auto V = [V_dyn{ H._potentialFunction() }, V_stat{ H._H0()._potential() }](auto... ts){return V_stat + V_dyn(std::forward<decltype(ts)>(ts)...); };

		return[splitStepAlg{ SplitStepAlgorithm{} }, V, T{ oneD::makePSpaceKinetic(H._H0()._serializedHilbertSpace()) }, beta{ H._H0()._beta() }, V_cache = V(initialParams)](auto& psi, const real dt, const auto& p2) mutable
		{
			using namespace std::complex_literals;
			auto intermediate = exp(-1i * 0.5*(V_cache + beta * psi.absSquare())*dt)*psi;
			psi = intermediate;

			auto expT = exp(-1i * dt*T);
			auto applyExpT = oneD::makeApplier(expT);
			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			V_cache = V(p2);

			intermediate = exp(-1i * 0.5*(V_cache + beta * psi.absSquare())*dt)*psi;
			psi = intermediate;
		};
	}

	template<class SerializedHilbertSpace, class PotentialFunction, class... Params>
	auto makeSplitStepAlgorithm(const HamiltonianFunction<HamiltonianGpe<SerializedHilbertSpace>, PotentialFunction>& H, const real dt, const Tuple<Params...>& initialParams)
	{
		using namespace std::complex_literals;
		const auto T = oneD::makePSpaceKinetic(H._H0()._serializedHilbertSpace());
		auto expT = exp(-1i * dt*T);
		auto applyExpT = [expT](auto& psi)
		{
			auto intermediate = expT * psi;
			psi = intermediate;
		};

		auto minushalfIdt = -1i * 0.5*dt;

		auto V = [V_dyn{ H._potentialFunction() }, V_stat{ H._H0()._potential() }](auto... ts){return V_stat + V_dyn(std::forward<decltype(ts)>(ts)...); };

		return[splitStepAlg{ SplitStepAlgorithm{} }, V, applyExpT, beta{ H._H0()._beta() }, minushalfIdt, V_cache = V(initialParams)](auto& psi, const auto& p2) mutable
		{

			auto intermediate = exp(minushalfIdt*(V_cache + beta * psi.absSquare()))*psi;
			psi = intermediate;


			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			V_cache = V(p2);
			intermediate = exp(minushalfIdt*(V_cache + beta * psi.absSquare()))*psi;
			psi = intermediate;

		};
	}

}

namespace qengine
{
	template<class SerializedHilbertSpace>
	auto makeDefaultStepAlgorithm(const HamiltonianGpe<SerializedHilbertSpace>& H)
	{
		return addImagBounds(splitStep(H), makeDefaultImagPot(H._serializedHilbertSpace()));
	}

	template<class SerializedHilbertSpace, class PotentialFunction>
	auto makeDefaultStepAlgorithm(const HamiltonianFunction<HamiltonianGpe<SerializedHilbertSpace>, PotentialFunction>& H)
	{
		return addImagBounds(splitStep(H), makeDefaultImagPot(H._H0()._serializedHilbertSpace()));
	}

	template<class SerializedHilbertSpace>
	auto makeDefaultStepAlgorithm(const HamiltonianGpe<SerializedHilbertSpace>& H, real dt)
	{
		return addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._serializedHilbertSpace()), dt);
	}

	template<class SerializedHilbertSpace, class PotentialFunction>
	auto makeDefaultStepAlgorithm(const HamiltonianFunction<HamiltonianGpe<SerializedHilbertSpace>, PotentialFunction>& H, real dt)
	{
		return addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._serializedHilbertSpace()), dt);
	}
}

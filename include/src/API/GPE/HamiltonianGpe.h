﻿/* COPYRIGHT
 *
 * file="HamiltonianGpe.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/API/GPE/HamiltonianGpe.fwd.h"

#include "src/Utility/LinearAlgebra/Spectrum.h"

#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.h"

#include "src/API/Common/Spatial/1D/LinearHamiltonian1D.h"
#include "src/API/GPE/GpeNonlinearity.h"
#include "src/API/GPE/GpeDiagonalizer.h"

namespace qengine
{
	template<class SerializedHilbertSpace>
	class HamiltonianGpe : public internal::EigenstateSyntax_CRTP<HamiltonianGpe<SerializedHilbertSpace>>, private GpeDiagonalizer<SerializedHilbertSpace>
	{
	public:
		HamiltonianGpe(const LinearHamiltonian1D<SerializedHilbertSpace>& H0, real beta);
		HamiltonianGpe(LinearHamiltonian1D<SerializedHilbertSpace>&& H0, real beta);

		template<typename ScalarType>
		void applyInPlace(FunctionOfX<ScalarType, SerializedHilbertSpace>& f) const;

		spatial::Spectrum<real, SerializedHilbertSpace> makeSpectrum(count_t largestEigenvalue, GROUNDSTATE_ALGORITHM groundstateAlgorithm = GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING, real convergenceCriterion = 1e-8, count_t maxIterationsPerEigenstate = 12000, const std::vector<real>& mixingValues = std::vector<real>{ 1e-2, 1e-3, 1e-3, 0.8 * 1e-3, 0.5 * 1e-3, 0.2 * 1e-3, 1e-4, 0.5 * 1e-4, 1e-5, 0.5 * 1e-5,1e-6,0.5*1e-6,1e-7,0.5*1e-7 }) const;

		/// INTERNAL USE
	public:
		const auto& _potential() const { return H0_._potential(); }
		real _beta() const { return nonlinearity_.beta; }
		const auto& _H0() const { return H0_; }
		const auto& _serializedHilbertSpace() const { return H0_._serializedHilbertSpace(); }

	private:
		LinearHamiltonian1D<SerializedHilbertSpace> H0_;
		GpeTerm nonlinearity_;
	};

	template<class SerializedHilbertSpace>
	std::ostream& operator<<(std::ostream& s, const HamiltonianGpe<SerializedHilbertSpace>& H);
}

namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> operator*(const HamiltonianGpe<SerializedHilbertSpace>& H, FunctionOfX<ScalarType, SerializedHilbertSpace> psi)
	{
		H.applyInPlace(psi);
		return psi;
	}
}

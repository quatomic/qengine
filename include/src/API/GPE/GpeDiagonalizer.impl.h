﻿/* COPYRIGHT
 *
 * file="GpeDiagonalizer.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/API/GPE/GpeDiagonalizer.h"

namespace qengine
{

	template <class SerializedHilbertSpace>
	spatial::Spectrum<real, SerializedHilbertSpace> GpeDiagonalizer<SerializedHilbertSpace>::operator()(const HamiltonianGpe<SerializedHilbertSpace>& Ham, const LinearHamiltonian1D<SerializedHilbertSpace>& H0, const GpeTerm gpeTerm, count_t largestEigenvalue, const GROUNDSTATE_ALGORITHM alg, const real convergenceCriterion,
		const count_t maxIterationsPerEigenstate, const std::vector<real>& mixingValues)const
	{
		auto beta = gpeTerm.beta;

		qengine::Vector<real> eigenvalues(largestEigenvalue + 1, 0.0);
		Matrix<complex> eigenstates(H0._serializedHilbertSpace().dim, largestEigenvalue + 1, 0.0);

		auto groundState = FunctionOfX<complex, SerializedHilbertSpace>(CVec(H0._serializedHilbertSpace().dim, 0.0), H0._serializedHilbertSpace());
		if (alg == GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING)
		{
			groundState = groundStateOptimalDamping(H0, beta, convergenceCriterion, maxIterationsPerEigenstate);
		}
		else if (alg == GROUNDSTATE_ALGORITHM::MIXING)
		{
			groundState = groundStateMixing(H0, beta, convergenceCriterion, maxIterationsPerEigenstate, mixingValues.at(0));
		}
		else
		{
			throw std::runtime_error("could not find groundstate algorithm!");
		}

		if (largestEigenvalue == 0)
		{
			eigenstates.col(0) = groundState.vec();
			eigenvalues.at(0) = expectationValue(Ham, groundState);
			return spatial::Spectrum<real, SerializedHilbertSpace>(internal::Spectrum<real>(eigenvalues, eigenstates, H0._serializedHilbertSpace().normalization()), H0._serializedHilbertSpace());
		}

		auto gsSpectrum = (H0 + beta * groundState.absSquare()).makeSpectrum(largestEigenvalue);
		eigenvalues.at(0) = gsSpectrum.eigenvalue(0);
		eigenstates.col(0) = gsSpectrum.eigenvector(0);

		auto H = [&H0, &beta](const auto& rho) {return H0 + beta * rho; };

		auto psi = gsSpectrum.eigenFunction(0);

		auto rho = psi.absSquare();
		auto psiOld = psi;

		// find higher eigenstates
		for (auto j = 1u; j < largestEigenvalue + 1; j++) {
			psi = gsSpectrum.eigenFunction(j);
			rho = psi.absSquare();

			real mixing;
			if (j > mixingValues.size()) {
				mixing = mixingValues.back();
			}
			else {
				mixing = mixingValues.at(j);
			}

			for (auto i = 0u; i < maxIterationsPerEigenstate; ++i) {
				psiOld = psi;
				psi = normalize(H(rho)*psi);
								for (auto k = 0u; k < j; ++k)
				{
					psi = normalize(psi - overlap(gsSpectrum.eigenFunction(k), psi)*gsSpectrum.eigenFunction(k));
					
				}

				psi = normalize(psiOld - mixing * psi);
				
				rho = psi.absSquare();

				auto n3 = overlap(psi - psiOld, psi - psiOld).real();
				auto n4 = overlap(psi, psi).real();
				
				if (std::sqrt(n3) < convergenceCriterion * std::sqrt(n4)) {
					break;
				}
			}

			rho = psi.absSquare();
			const real E = expectationValue(H(rho), psi);

			eigenstates.col(j) = psi.vec();
			eigenvalues.at(j) = E;
		}

		auto psi1 = makeFunctionOfX( H0._serializedHilbertSpace(),CVec(eigenstates.col(0)));
		//std::cout << "E = " << expectationValue(H0, psi1) << " +/- " << standardDeviation(H0, psi1) << std::endl;


		return spatial::Spectrum<real, SerializedHilbertSpace>(internal::Spectrum<real>(eigenvalues, eigenstates, H0._serializedHilbertSpace().normalization()), H0._serializedHilbertSpace());

	}

	template <class SerializedHilbertSpace>
	FunctionOfX<complex, SerializedHilbertSpace> GpeDiagonalizer<SerializedHilbertSpace>::groundStateOptimalDamping(const LinearHamiltonian1D<SerializedHilbertSpace>& H0, const real beta, const real convergenceCriterion,
		const count_t maxIterations)
	{
		auto H = [&H0, &beta](const auto& rho) {return H0 + beta * rho; };

		// initialization
		auto psi = makeWavefunction(H0[0]); // this corresponds to psi0 in the article: needed for the three quantities below
											//std::cout << "E = " << expectationValue(H0, psi) << " +/- " << standardDeviation(H0, psi) << std::endl;

		auto rho = psi.absSquare();
		auto f = expectationValue(H0, psi);
		auto h = expectationValue(H(rho), psi);

		real fp, hp, hpp, s, c, alpha, Eopt;
		fp = hp = hpp = s = c = alpha = Eopt = 0;

		auto varE = variance(H(rho), psi);
		for (auto i = 0u; i < maxIterations; ++i)
		{
			// step 1
			auto rho_prev = rho;
			psi = makeWavefunction(H(rho)[0]);
			rho = psi.absSquare();

			varE = variance(H(rho), psi);
			if (varE < convergenceCriterion)
			{
				break;
			}

			// step 2
			fp = expectationValue(H0, psi);
			hp = expectationValue(H(rho_prev), psi);
			hpp = expectationValue(H(rho), psi);

			// step 3
			s = hp - h;
			c = h + hpp - 2 * hp + fp - f;

			// step 4
			if (c <= -s) {
				alpha = 1.0;
			}
			else {
				alpha = -s / c;
			}

			Eopt = 0.5*(f + h) + alpha * s + 0.5*alpha*alpha*c;
			rho = (1.0 - alpha)*rho_prev + alpha * rho;
			f = (1.0 - alpha)*f + alpha * fp;
			h = 2.0*Eopt - f;

		}
		return psi;

	}

	template <class SerializedHilbertSpace>
	FunctionOfX<complex, SerializedHilbertSpace> GpeDiagonalizer<SerializedHilbertSpace>::groundStateMixing(const LinearHamiltonian1D<SerializedHilbertSpace>& H0, const real beta, const real convergenceCriterion, const count_t maxIterations,
		const real mixingValue)
	{
		auto H = [&H0, beta](const auto& rho) {return H0 + beta * rho; };

		auto psi = makeWavefunction(H0[0]);

		auto psiOld = psi;

		for (auto i = 0u; i < maxIterations; i++)
		{
			psiOld = psi;

			psi = normalize(H(psi.absSquare())*psi);

			psi = normalize(psiOld - mixingValue * psi);

			if ((psi - psiOld).norm() < convergenceCriterion*psi.norm()) {
				break;
			}
		}
		return psi;
	}

}

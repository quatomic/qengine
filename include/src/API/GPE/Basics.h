﻿/* COPYRIGHT
 *
 * file="Basics.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.h"
#include "src/Abstract/Physics/Spatial/Basics.h"

#include "src/API/GPE/HamiltonianGpe.h"


namespace qengine
{
	template<class SerializedHilbertSpace, class ScalarType>
	complex cVariance(const HamiltonianGpe<SerializedHilbertSpace>& op, const FunctionOfX<ScalarType, SerializedHilbertSpace>& psi)
	{
		auto simpleOp = op._H0() + op._beta()*psi.absSquare();
		return cVariance(simpleOp, psi);
	}

	template<class ScalarType, class SerializedHilbertSpace>
	real variance(const HamiltonianGpe<SerializedHilbertSpace>& op, const FunctionOfX<ScalarType, SerializedHilbertSpace>& psi)
	{
		auto c = cVariance(op, psi);
		qengine_assert(std::abs(c.imag()) < 1e-10, "variance has nonzero imaginary part!");
		return c.real();
	}
}

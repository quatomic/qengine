﻿/* COPYRIGHT
 *
 * file="HamiltonianArithmetic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/SqDiagMat.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.h"

#include "src/API/Common/Spatial/1D/LinearHamiltonian1D.h"
#include "src/API/GPE/HamiltonianGpe.h"

namespace qengine
{
	template<class SerializedHilbertSpace>
	LinearHamiltonian1D<SerializedHilbertSpace> operator+(const LinearHamiltonian1D<SerializedHilbertSpace>& H, const FunctionOfX<real, SerializedHilbertSpace>& V)
	{
		return LinearHamiltonian1D<SerializedHilbertSpace>(H._serializedHilbertSpace(), H._hamiltonianMatrix() + internal::SqDiagMat<real>(V.vec()), H._potential() + V);
	}

	template<class SerializedHilbertSpace>
	LinearHamiltonian1D<SerializedHilbertSpace> operator-(LinearHamiltonian1D<SerializedHilbertSpace> H, const FunctionOfX<real, SerializedHilbertSpace>& V)
	{
		return H + (-V);
	}

	template<class SerializedHilbertSpace>
	HamiltonianGpe<SerializedHilbertSpace> operator+(const LinearHamiltonian1D<SerializedHilbertSpace>& H0, const GpeTerm gpeTerm)
	{
		return HamiltonianGpe<SerializedHilbertSpace>(H0, gpeTerm.beta);
	}

	template<class SerializedHilbertSpace>
	HamiltonianGpe<SerializedHilbertSpace> operator+(const HamiltonianGpe<SerializedHilbertSpace>& H, const FunctionOfX<real, SerializedHilbertSpace>& V)
	{
		return HamiltonianGpe<SerializedHilbertSpace>(H._H0() + V, H._beta());
	}

	template<class SerializedHilbertSpace>
	HamiltonianGpe<SerializedHilbertSpace> operator-(const HamiltonianGpe<SerializedHilbertSpace>& H, const FunctionOfX<real, SerializedHilbertSpace>& V)
	{
		return H + (-V);
	}

	template<class SerializedHilbertSpace>
	HamiltonianGpe<SerializedHilbertSpace> operator+(const HamiltonianGpe<SerializedHilbertSpace>& H, const GpeTerm gpeTerm)
	{
		return HamiltonianGpe<SerializedHilbertSpace>(H._H0(), H._beta() + gpeTerm.beta);
	}
}

﻿/* COPYRIGHT
 *
 * file="makeState.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/toString.h"
#include "src/Utility/Normalization.h"

#include "src/Abstract/Physics/SecondQuantization/FockOperators.h"
#include "src/Abstract/Physics/SecondQuantization/isApiHilbertSpace.h"

#include "src/API/NLevel/ApiHilbertSpace.h"
#include "src/API/BoseHubbard/ApiHilbertSpace.h"

namespace qengine
{

	State<internal::SerializedNLevelHilbertSpace> makeState(const n_level::ApiHilbertSpace& s, const LinearCombinationOfFocks& focks, NORMALIZATION shouldNormalize = NORMALIZE_NOT);
	State<internal::SerializedBHHilbertSpace> makeState(const bosehubbard::ApiHilbertSpace& s, const LinearCombinationOfFocks& def, NORMALIZATION shouldNormalize = NORMALIZE_NOT);

	template<class ApiHilbertSpace, class = std::enable_if_t<second_quantized::is2ndQApiHilbertSpace_v<ApiHilbertSpace>>>
	auto makeState(const ApiHilbertSpace& s, const CVec& vec, NORMALIZATION shouldNormalize = NORMALIZE_NOT)
	{
		if (shouldNormalize == NORMALIZE_NOT) return State<typename ApiHilbertSpace::SerializedHilbertSpace>(vec, s._serializedHilbertSpace());
		return normalize(State<typename ApiHilbertSpace::SerializedHilbertSpace>(vec, s._serializedHilbertSpace()));
	}

	template<class ApiHilbertSpace, class = std::enable_if_t<second_quantized::is2ndQApiHilbertSpace_v<ApiHilbertSpace>>>
	auto makeState(const ApiHilbertSpace& s, const RVec& vec, NORMALIZATION shouldNormalize = NORMALIZE_NOT)
	{
		return makeState(s, CVec(vec), shouldNormalize);
	}

	template<class ApiHilbertSpace, class = std::enable_if_t<second_quantized::is2ndQApiHilbertSpace_v<ApiHilbertSpace>>>
	auto makeState(const ApiHilbertSpace& s, std::initializer_list<real> vec, NORMALIZATION shouldNormalize = NORMALIZE_NOT)
	{
		return makeState(s, RVec(vec), shouldNormalize);
	}

	template<class ApiHilbertSpace, class = std::enable_if_t<second_quantized::is2ndQApiHilbertSpace_v<ApiHilbertSpace>>>
	auto makeState(const ApiHilbertSpace& s, std::initializer_list<complex> vec, NORMALIZATION shouldNormalize = NORMALIZE_NOT)
	{
		return makeState(s, CVec(vec), shouldNormalize);
	}

	template<template<class> class MatType, class NumberType, class... Ts>
	State<internal::SerializedBHHilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, NumberType, internal::SerializedBHHilbertSpace>>& l, Ts&&... ts)
	{
		//// todo: figure out why these do not work when making Hilbertspace a template
		return l.evaluate(std::forward<Ts>(ts)...);
	}

	template<template<class> class MatType, class NumberType, class... Ts>
	State<internal::SerializedBHHilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, NumberType, internal::SerializedBHHilbertSpace>>& l, const NORMALIZATION shouldNormalize, Ts&&... ts)
	{
		auto state = l.evaluate(std::forward<Ts>(ts)...);
		if (shouldNormalize == NORMALIZE) return normalize(state);
		return state;
	}

	template<template<class> class MatType, class NumberType, class... Ts>
	State<internal::SerializedNLevelHilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, NumberType, internal::SerializedNLevelHilbertSpace>>& l, Ts&&... ts)
	{
		return l.evaluate(std::forward<Ts>(ts)...);
	}
	template<template<class> class MatType, class NumberType, class... Ts>
	State<internal::SerializedNLevelHilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, NumberType, internal::SerializedNLevelHilbertSpace>>& l, const NORMALIZATION shouldNormalize, Ts&&... ts)
	{
		auto state = l.evaluate(std::forward<Ts>(ts)...);
		if (shouldNormalize == NORMALIZE) return normalize(state);
		return state;
	}
}

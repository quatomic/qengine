﻿/* COPYRIGHT
 *
 * file="makeState.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/Common/SecondQuantized/makeState.h"

#include "src/Abstract/Physics/SecondQuantization/State.impl.h"

namespace qengine
{
	State<internal::SerializedNLevelHilbertSpace> makeState(const n_level::ApiHilbertSpace& s, const LinearCombinationOfFocks& focks, const NORMALIZATION shouldNormalize)
	{
		qengine_assert(focks.nModes() == 1, "fockstate can only have a single mode for n-level");

		auto stateVec = CVec(s.nLevels());
		for (auto a : focks.factors())
		{
			qengine_assert(a.first.at(0) < s.nLevels(), "index of fockstate for N-level system out of bounds (N = " + std::to_string(s.nLevels()) + "). index:" + std::to_string(a.first.at(0)) + "; max index: " + std::to_string(s.nLevels() - 1));
			stateVec.at(a.first.at(0)) = a.second;
		}

		return makeState(s, stateVec, shouldNormalize);
	}

	State<internal::SerializedBHHilbertSpace> makeState(const bosehubbard::ApiHilbertSpace& s, const qengine::LinearCombinationOfFocks& def, const NORMALIZATION shouldNormalize)
	{
		qengine_assert(def.nModes() == s.nSites(), "number of modes in fockstate must correspond to number of sites in bosehubbard hilbert-space. Modes you tried to put in: " + std::to_string(def.nModes()) + ", sites in hilbertspace: " + std::to_string(s.nSites()));

		auto statevec = CVec(s.nBasisElements(), 0.0);

		for (const auto& p : def.factors())
		{
			qengine_assert(s._isInBasis(p.first), "The desired fockstate is not in the basis. Requested state: " + toString(p.first));
			statevec.at(s._get(p.first)) = p.second;
		}

		return makeState(s, statevec, shouldNormalize);
	}

}

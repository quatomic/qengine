﻿/* COPYRIGHT
 *
 * file="GeneralHelpers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Utility/NumericGradients.h"

#include "src/Abstract/OptimalControl/Parameters/Control.h"
#include "src/Abstract/OptimalControl/Problems/ProblemTraits.h"

namespace qengine
{
	namespace optimal_control
	{
		template<class Problem, class ControlFromRVec>
		auto fullNumericGradient_impl(const Problem& problem, const ControlFromRVec& makeControl, const bool verbose)
		{
			auto x0 = problem._params();

			const auto paramCount = x0.paramCount();
			auto f = [&problem, paramCount, dt{ x0.dt() }, makeControl](const RVec& controlvec)
			{
				auto p = problem;
				auto control = makeControl(controlvec);

				p.update(control);
				return p.cost();
			};
			return makeControl(numericGradient(f, x0._vec(), 1e-6, verbose));
		}
	}
	template<class Problem, std::enable_if_t<internal::is_QOCProblem_v<Problem>, int> = 0>
	auto fullNumericGradient(const Problem& problem, const bool verbose = false)
	{
		auto paramCount = problem._params().paramCount();
		auto dt = problem.control().dt();
		return optimal_control::fullNumericGradient_impl(problem, [paramCount, dt](const RVec& v) {return Control(v, paramCount, dt); }, verbose);
	}
	namespace internal2
	{
		template<class Problem, class ControlFromRVec>
		auto fullNumericGradient_impl2(const Problem& problem, const ControlFromRVec& makeControl, const bool verbose)
		{
			auto x0 = problem._params();

			const auto paramCount = x0.paramCount();
			auto f = [&problem, paramCount, dt{ x0.dt() }, makeControl](const RVec& controlvec)
			{
				auto p = problem;
				auto control = makeControl(controlvec);

				p.update(control);
				return p.cost();
			};

			auto tup = numericGradient2(f, x0._vec(), 1e-6, verbose);

			return std::make_tuple(makeControl(std::get<0>(tup)), makeControl(std::get<1>(tup)), makeControl(std::get<2>(tup)));
		}
	}
	template<class Problem, std::enable_if_t<internal::is_QOCProblem_v<Problem>, int> = 0>
	auto fullNumericGradient2(const Problem& problem, const bool verbose = false)
	{
		auto paramCount = problem._params().paramCount();
		auto dt = problem.control().dt();
		return internal2::fullNumericGradient_impl2(problem, [paramCount, dt](const RVec& v) {return Control(v, paramCount, dt); }, verbose);
	}

	namespace optimal_control
	{
		template<class Problem, class ControlFromRVec>
		real gradientDifference_impl(const Problem& problem, const ControlFromRVec& makeControl, const bool verbose, const bool changeLastPoint, const bool changeInitialPoint)
		{
			auto end = problem._params().size() - 1;
			const auto paramCount = problem._params().paramCount();

			auto analytic = problem.gradient();
			auto numeric = fullNumericGradient_impl(problem, makeControl, verbose);

			if (!changeLastPoint) numeric.at(end) = RVec(paramCount, 0.0);
			if (!changeInitialPoint) numeric.at(0) = RVec(paramCount, 0.0);

			auto mDiff = mean(abs(numeric - analytic));
			auto m = mean(abs(numeric));
			if (m == 0)
			{
				if (mDiff == 0)
				{
					return 0;
				}
				else
				{
					return std::numeric_limits<real>::max();
				}
			}
			return mDiff / m;
		}
	}

	template<class Problem>
	std::enable_if_t<internal::is_QOCProblem_v<Problem>, real>
		gradientDifference(const Problem& problem, const bool verbose = false, const bool changeLastPoint = false, const bool changeInitialPoint = false)
	{
		auto paramCount = problem._params().paramCount();
		auto dt = problem.control().dt();
		return optimal_control::gradientDifference_impl(problem, [paramCount, dt](const RVec& v) {return Control(v, paramCount, dt); }, verbose, changeLastPoint, changeInitialPoint);
	}
}

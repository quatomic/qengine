﻿/* COPYRIGHT
 *
 * file="TimeEvolutionHelpers.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/Common/Spatial/TimeEvolutionHelpers.h"

namespace qengine
{
	namespace internal
	{
		RVec makeKVec(const real dx, const count_t dim)
		{
			const auto frontFactor = 1.0 / (dx * dim);
			RVec kArray(dim);
			auto nval = 0.0;
			for (auto i = 0u; i < dim / 2; ++i)
			{
				kArray.at(i) = frontFactor * nval;
				nval = nval + 1.0;
			}
			nval = 0.0;
			for (auto i = dim / 2; i < dim; ++i)
			{
				kArray.at(i) = frontFactor * (-0.5 * dim + nval);
				nval = nval + 1.0;
			}

			return kArray;
		}

		RVec makePSpaceKineticVec(const real dx, const count_t dim, const real kinematicFactor)
		{
			return kinematicFactor * pow(2 * PI*makeKVec(dx, dim), 2);
		}

		RVec makeDefaultImagPotVector(const count_t N, const real dx, const real barrierIntensity)
		{
			auto x = qengine::linspace(0, (N - 1)*dx, N);

			const auto il = N / 8;
			const auto iu = N - 1 - il;

			const auto xl = x.at(il);
			const auto xu = x.at(iu);

			const auto Vl = (1 - step(x, xl))*pow(x - xl, 2);
			const auto Vu = step(x, xu)*pow(x - xu, 2);

			return -barrierIntensity * (Vl + Vu);
		}
	}

}

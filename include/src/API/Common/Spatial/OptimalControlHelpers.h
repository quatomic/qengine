﻿/* COPYRIGHT
 *
 * file="OptimalControlHelpers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <functional>

#include "src/Utility/cpp17Replacements/check_all_true.h"
#include "src/Utility/getFirst.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.h"

#include "src/Abstract/Physics/Spatial/Basics.h"

namespace qengine
{
	template<class... Ts>
	auto makeAnalyticDiffPotential(const Ts&... ts)
	{
		using SerializedHilbertSpace = internal::extract_serialized_hilbertspace_t<decltype(internal::getFirst(ts...)(std::declval<RVec>()))>;

		static_assert(repl17::check_all_true(is_callable_v<Ts, FunctionOfX<real, SerializedHilbertSpace>(const RVec&)>...), "the input functions must be callable the right way!");
		using dVdui = std::function<FunctionOfX<real, SerializedHilbertSpace>(const Vector<real>&)>;

		return std::vector<dVdui>{dVdui(ts)...};
	}

	// overload for RVec
	template<class PotentialFunctor>
	auto makeNumericDiffPotential(const PotentialFunction<PotentialFunctor, RVec>& V, const real epsilon = 1e-6)
	{
		using Potential = decltype(std::declval<PotentialFunctor>()(RVec{}));
		static_assert(std::is_same<Potential, decltype(std::declval<Potential>() - std::declval<Potential>())>::value, "");
		using dVdui = std::function<Potential(const RVec&)>;
		std::vector<dVdui> retval;


		const auto paramCount = std::get<0>(V.initialParams().data).size();
		auto dir = RVec(paramCount, 0.0);
		auto divisor = 1.0 / (2 * epsilon); // precalculate
		for (auto i = 0u; i < paramCount; ++i)
		{
			dir.at(i) = 1;

			auto edir = epsilon * dir;

			retval.push_back(dVdui(
				[edir, V, divisor](const RVec& x)
			{
				return (V(x + edir) - V(x - edir)) *divisor;
			}));

			dir.at(i) = 0;
		}

		return retval;
	}

	// overload for multiple reals
	template<class PotentialFunctor, class... Params, typename = std::enable_if_t<repl17::check_all_true(std::is_same<Params, real>::value...)>>
	auto makeNumericDiffPotential(const PotentialFunction<PotentialFunctor, Params...>& V, const real epsilon = 1e-6)
	{
		auto makeRVec = [](const auto... vals) {return RVec{ vals... }; };

		auto initialVec = internal::apply(makeRVec, V.initialParams());

		auto V_vec = makePotentialFunction([V = V.functor()](const RVec& controls)
		{
			return internal::apply_vec<sizeof...(Params)>(V, controls);
		}, initialVec);

		return makeNumericDiffPotential(V_vec, epsilon);
	}

	template<class PotentialFunction, class Identifier>
	RVec infidelityWithDiffPotential(const std::vector<std::function<FunctionOfX<real, spatial::SerializedHilbertSpace1D<Identifier>>(const Vector<real>&)>>& dVdu, PotentialFunction V, const RVec& param)
	{
		auto dVdu_num = makeNumericDiffPotential(V);

		auto retval = RVec(param.size(), 0.0);
		for (auto i = 0u; i < param.size(); ++i)
		{
			retval.at(i) = infidelity(normalize(dVdu_num.at(i)(param)), normalize(dVdu.at(i)(param)));
		}
		return retval;
	}
}

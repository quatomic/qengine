﻿/* COPYRIGHT
 *
 * file="TimeEvolutionHelpers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Constants.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.h"

namespace qengine
{
	template<class Alg>
	auto renormalizing(Alg&& alg)
	{
		return[alg{ std::forward<Alg>(alg) }](auto& psi, auto&&... ts)
		{
			auto norm = psi.norm();
			alg(psi, std::forward<decltype(ts)>(ts)...);
			auto intermediate = psi * norm / psi.norm();
			psi = intermediate;
		};
	}

	template<class Alg>
	auto normalizing(Alg&& alg)
	{
		return[alg{ std::forward<Alg>(alg) }](auto& psi, auto&&... ts)
		{
			alg(psi, std::forward<decltype(ts)>(ts)...);
			auto intermediate = normalize(psi);
			psi = intermediate;
		};
	}
}


namespace qengine
{
	namespace internal
	{
		RVec makeKVec(real dx, count_t dim);
		RVec makePSpaceKineticVec(real dx, count_t dim, real kinematicFactor);
		RVec makeDefaultImagPotVector(count_t N, real dx, real barrierIntensity);
	}
}

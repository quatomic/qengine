﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian1D.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"

#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.h"

#include "src/Abstract/Physics/Spatial/LinearHamiltonian.h"
#include "src/Abstract/Physics/Spatial/MakeKinetic.h"

namespace qengine
{
	template<class SerializedHilbertSpace>
	using LinearHamiltonian1D = LinearHamiltonian<SerializedHilbertSpace, internal::SqHermitianBandMat>;
}

namespace qengine
{
	namespace internal
	{
		template<class Identifier>
		LinearHamiltonian1D<spatial::SerializedHilbertSpace1D<Identifier>> makeKinetic(const spatial::SerializedHilbertSpace1D<Identifier>& s)
		{
			auto dx = s.dx();
			auto dim = s.dim;
			auto k = s.kinematicFactor;

			return { s, makeKineticMatrix(dim, dx, k), FunctionOfX<real, spatial::SerializedHilbertSpace1D<Identifier>>(RVec(s.dim, 0.0), s) };
		}
	}
}

﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/Common/Spatial/1D/ApiHilbertSpace.h"

#include "src/Utility/LinearAlgebra/Matrix.h"

#include "src/Abstract/Physics/Spatial/MakeKinetic.h"

#include "src/API/Common/Spatial/1D/LinearHamiltonian1D.h"

#include "src/Abstract/Physics/Spatial/LinearHamiltonian.impl.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.impl.h"

namespace qengine
{
	namespace oneD
	{

		ApiHilbertSpace::ApiHilbertSpace(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor) :
			serializedHilbertSpace_(xLower, xUpper, dimension, kinematicFactor),
			spatialDimension_(serializedHilbertSpace_.makeSpatialDimension())
		{
		}

		LinearHamiltonian1D<ApiHilbertSpace::SerializedHilbertSpace> ApiHilbertSpace::T() const
		{
			return internal::makeKinetic(serializedHilbertSpace_);
		}

		LinearHamiltonian1D<ApiHilbertSpace::SerializedHilbertSpace> ApiHilbertSpace::makeKinetic() const
		{
			return T();
		}
	}

	namespace one_particle
	{
		oneD::ApiHilbertSpace makeHilbertSpace(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor)
		{
			return oneD::ApiHilbertSpace(xLower, xUpper, dimension, kinematicFactor);
		}
	}

	namespace gpe
	{
		oneD::ApiHilbertSpace makeHilbertSpace(const real xLower, const real xUpper, const count_t dimension, const real kinematicFactor)
		{
			return oneD::ApiHilbertSpace(xLower, xUpper, dimension, kinematicFactor);
		}
	}
}

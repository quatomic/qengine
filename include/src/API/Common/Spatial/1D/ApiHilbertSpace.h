﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/Spatial/SerializedHilbertSpace1D.h"
#include "src/API/Common/Spatial/1D/LinearHamiltonian1D.h"


namespace qengine
{
	namespace oneD
	{
		class ApiHilbertSpace
		{
		public:
			using SerializedHilbertSpace = spatial::SerializedHilbertSpace1D<struct Simple1DIdentifier>;

			ApiHilbertSpace(real xLower, real xUpper, count_t dimension, real kinematicFactor);

			const FunctionOfX<real, SerializedHilbertSpace>& spatialDimension() const { return spatialDimension_; }
			const FunctionOfX<real, SerializedHilbertSpace>& x() const { return spatialDimension_; }

			LinearHamiltonian1D<SerializedHilbertSpace> T() const;
			LinearHamiltonian1D<SerializedHilbertSpace> makeKinetic() const;

			real xLower() const { return serializedHilbertSpace_.xLower; }
			real xUpper() const { return serializedHilbertSpace_.xUpper; }
			count_t dim() const { return serializedHilbertSpace_.dim; }
			real kinematicFactor() const { return serializedHilbertSpace_.kinematicFactor; }
			real dx() const { return serializedHilbertSpace_.dx(); }
			real integrationConstant() const { return serializedHilbertSpace_.integrationConstant(); }
			real normalization() const { return serializedHilbertSpace_.normalization(); }

			/// INTERNAL USE ONLY
		public:
			const SerializedHilbertSpace& _serializedHilbertSpace() const { return serializedHilbertSpace_; }

		private:
			const SerializedHilbertSpace serializedHilbertSpace_;
			FunctionOfX<real, SerializedHilbertSpace> spatialDimension_;

		};
	}

	namespace one_particle 
	{
		oneD::ApiHilbertSpace makeHilbertSpace(real xLower, real xUpper, count_t dimension, real kinematicFactor = 0.5);
	}
	namespace gpe 
	{
		oneD::ApiHilbertSpace makeHilbertSpace(real xLower, real xUpper, count_t dimension, real kinematicFactor = 0.5);
	}
}

namespace qengine
{
	namespace internal
	{
		template<>
		struct extract_serialized_hilbertspace<oneD::ApiHilbertSpace>
		{
			using type = oneD::ApiHilbertSpace::SerializedHilbertSpace;
		};
	}
}

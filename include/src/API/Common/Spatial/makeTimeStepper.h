﻿/* COPYRIGHT
 *
 * file="makeTimeStepper.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <type_traits>
#include "src/Abstract/Physics/Spatial/isHamiltonian.h"
#include "src/Abstract/Physics/Spatial/HamiltonianFunction.h"
#include "src/Abstract/Physics/Spatial/PotentialFunction.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.h"
#include "src/Utility/cpp17Replacements/Tuple.h"

namespace qengine
{
	template<class Hamiltonian, class SerializedHilbertSpace, typename = std::enable_if_t<spatial::isHamiltonian_v<Hamiltonian>> >
	auto makeTimeStepper(const Hamiltonian& H, const FunctionOfX<complex, SerializedHilbertSpace>& psi0)
	{
		return makeTimeStepper(makeDefaultStepAlgorithm(H), psi0);
	}

	template<class Hamiltonian, class SerializedHilbertSpace, class PotentialFunctor, class... Params, typename = std::enable_if_t<spatial::isHamiltonian_v<Hamiltonian>>>
	auto makeTimeStepper(const HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>>& H, const FunctionOfX<complex, SerializedHilbertSpace>& psi0, const Tuple<Params...>& initialParams)
	{
		return makeTimeStepper(makeDefaultStepAlgorithm(H), psi0, initialParams);
	}

	template<class Hamiltonian, class SerializedHilbertSpace, typename = std::enable_if_t<spatial::isHamiltonian_v<Hamiltonian>> >
	auto makeFixedTimeStepper(const Hamiltonian& H, const FunctionOfX<complex, SerializedHilbertSpace>& psi0, const real dt)
	{
		return makeFixedTimeStepper(makeDefaultStepAlgorithm(H, dt), psi0);
	}

	template<class Hamiltonian, class SerializedHilbertSpace, class PotentialFunctor, class... Params, typename = std::enable_if_t<spatial::isHamiltonian_v<Hamiltonian>>>
	auto makeFixedTimeStepper(const HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>>& H, const FunctionOfX<complex, SerializedHilbertSpace>& psi0, const real dt, const Tuple<Params...>& initialParams)
	{
		return makeFixedTimeStepper(makeDefaultStepAlgorithm(H, dt), psi0, initialParams);
	}

}


namespace qengine
{
	template< class Hamiltonian, class SerializedHilbertSpace, class PotentialFunctor, class... Params, typename = std::enable_if_t<repl17::check_all_true(std17::is_same_v<Params, real>...) && spatial::isHamiltonian_v<Hamiltonian>>>
	auto makeTimeStepper(const HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>>& H, const FunctionOfX<complex, SerializedHilbertSpace>& psi0, const RVec& initialParams)
	{
		return makeTimeStepper(H, psi0, Tuple<Params...>(initialParams));
	}

	template<class Hamiltonian, class SerializedHilbertSpace, class PotentialFunctor, class... Params, typename = std::enable_if_t<spatial::isHamiltonian_v<Hamiltonian>> >
	auto makeTimeStepper(const HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>>& H, const FunctionOfX<complex, SerializedHilbertSpace>& psi0)
	{
		return makeTimeStepper(H, psi0, Tuple<Params...>(H._potentialFunction().initialParams()));
	}

	template<class Hamiltonian, class SerializedHilbertSpace, class PotentialFunctor, class... Params, typename = std::enable_if_t<repl17::check_all_true(std17::is_same_v<Params, real>...) && spatial::isHamiltonian_v<Hamiltonian>>>
	auto makeFixedTimeStepper(const HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>>& H, const FunctionOfX<complex, SerializedHilbertSpace>& psi0, const real dt, const RVec& initialParams)
	{
		return makeFixedTimeStepper(H, psi0, dt, Tuple<Params...>(initialParams));
	}

	template<class Hamiltonian, class SerializedHilbertSpace, class PotentialFunctor, class... Params, typename = std::enable_if_t<spatial::isHamiltonian_v<Hamiltonian>> >
	auto makeFixedTimeStepper(const HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>>& H, const FunctionOfX<complex, SerializedHilbertSpace>& psi0, const real dt)
	{
		return makeFixedTimeStepper(H, psi0, dt, Tuple<Params...>(H._potentialFunction().initialParams()));
	}
}

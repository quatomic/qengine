﻿/* COPYRIGHT
 *
 * file="DressedRestarters.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <limits>
#include "src/NumberTypes.h"

#include "src/API/Common/OptimalControl/Policies/PolicyWrapper.h"

namespace qengine
{
	namespace internal
	{
		namespace policy
		{
			struct DressedRestarterID {};
		}
	}
}

namespace qengine
{

	template<class F>
	internal::PolicyWrapper<internal::policy::DressedRestarterID, F> makeDressedRestarter(F f)
	{
		return internal::PolicyWrapper<internal::policy::DressedRestarterID, F>{f};
	}

	namespace internal
	{
		template<class F1, class F2>
		auto operator+ (const PolicyWrapper<policy::DressedRestarterID, F1>& left, const PolicyWrapper<policy::DressedRestarterID, F2>& right)
		{
			return makeDressedRestarter([f1{ left.f }, f2{ right.f }](const auto& alg)
			{
				return f1(alg) || f2(alg);
			});
		}
	}

	inline auto makeCostDecreaseDressedRestarter(const real minimalCostDecrease, const bool verbose = false)
	{
		return makeDressedRestarter([verbose, previousCost = std::numeric_limits<real>::max(), minimalCostDecrease = minimalCostDecrease](const auto& alg) mutable
		{
			auto cost = alg.problem().cost();
			auto retval = (cost - previousCost) > minimalCostDecrease;

			if (verbose && retval) std::cout << "New superiteration in dGROUP algorithm : Too little cost decrease" << std::endl;

			previousCost = cost;
			return retval;
		});
	}

    inline auto makeStepSizeDressedRestarter(const real stepSizeTolerance, const bool verbose = false)
    {
        return makeDressedRestarter([verbose, tol{ stepSizeTolerance }](const auto& dGROUP) mutable
        {
            auto stepSize = dGROUP.stepSize();
            if (stepSize < tol)
            {
                if(verbose) std::cout << "New superiteration in dGROUP algorithm : Step size too small" << std::endl;
                return true;
            }
            return false;
        });
    }

}

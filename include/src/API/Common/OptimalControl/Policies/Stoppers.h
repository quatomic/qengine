﻿/* COPYRIGHT
 *
 * file="Stoppers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/NumberTypes.h"

#include "src/API/Common/OptimalControl/Policies/PolicyWrapper.h"

namespace qengine
{
	namespace internal
	{
		namespace policy
		{
			struct StopperID {};
		}
	}
}

namespace qengine
{
	template<class F>
	internal::PolicyWrapper<internal::policy::StopperID, F> makeStopper(F f)
	{
		return internal::PolicyWrapper<internal::policy::StopperID, F>{f};
	}	

	namespace internal
	{
		template<class F1, class F2>
		auto operator+ (const PolicyWrapper<policy::StopperID, F1>& left, const PolicyWrapper<policy::StopperID, F2>& right)
		{
			return makeStopper([f1{left.f}, f2{right.f}](const auto& alg)
			{
				return f1(alg) || f2(alg);
			});
		}
	}

    inline auto makeFidelityStopper(const real targetFidelity = 0.99)
	{
		return makeStopper([targetFidelity](const auto& alg) {return alg.problem().fidelity() > targetFidelity; });
	}	
	
	inline auto makeIterationStopper(const count_t maxIterations)
	{
		return makeStopper([maxIterations](const auto& alg) {return alg.iteration() >= maxIterations; });
	}

    inline auto makeStepsizeStopper(const real stepSizeMinimum = 1e-7)
    {
        return makeStopper([stepSizeMinimum](const auto& alg) {return alg.stepSize() < stepSizeMinimum; });
    }
}

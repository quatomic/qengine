﻿/* COPYRIGHT
 *
 * file="StepsizeFinders.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/NumberTypes.h"
#include "src/Utility/NumericGradients.h"

#include "src/Utility/std17/remove_cvref.h"

#include "src/API/Common/OptimalControl/Policies/PolicyWrapper.h"
#include "src/Abstract/OptimalControl/Algorithms/InterpolatingLinesearch.h"
#include "src/Abstract/OptimalControl/Algorithms/DerivativeFreeLinesearch.h"


namespace qengine
{
	namespace internal
	{
		namespace policy
		{
			struct StepSizeFinderID {};
		}
	}
}

namespace qengine
{
	template<class F>
	internal::PolicyWrapper<internal::policy::StepSizeFinderID, F> makeStepSizeFinder(F f)
	{
		return internal::PolicyWrapper<internal::policy::StepSizeFinderID, F>{f};
	}

    inline auto makeInterpolatingStepSizeFinder(const real maxStepSize, const real maxInitialStepSizeGuess, const real epsilon = 1e-8, const real alphaMin = 0, const real xtol = 1e-10, const count_t maxfev = 100, const real c1 = 1e-4, const real c2 = 0.9)
	{
        return makeStepSizeFinder([il{ InterpolatingLinesearch(alphaMin, xtol, static_cast<unsigned>(maxfev), c1, c2) }, maxStepSize, maxInitialStepSizeGuess,alphaMin,epsilon](const auto& algorithm)
		{
			const auto& constProblem = algorithm.problem();
			using ProblemType = std::decay_t<decltype(constProblem)>;

			auto& problem = const_cast<ProblemType&>(constProblem);

			auto& dir = const_cast<std17::remove_cvref_t<decltype(algorithm.stepDirection())>&>(algorithm.stepDirection());

			const auto& initialParam = problem._params();
			const auto initialCost = problem.cost();
			const auto epsilon = 1e-6;

			auto f_cost = [&problem, &dir, &initialParam](real pmEps)
			{
				problem.update(initialParam + pmEps * dir);
				return problem.cost();
			};

			// protect againt analytic gradient not being a descent direction
			auto initialGradient = (f_cost(epsilon)-initialCost)/epsilon;
			if (initialGradient > 0)
			{
				// TODO:: put this message in some sort of a logging mechanism
				//std::cout << "changed direction" << std::endl;
				dir = -dir;
				initialGradient *= -1;
			}

			auto func = [&f_cost, epsilon](real stepSize)
			{
				auto cost = f_cost(stepSize);
				auto gradInDirection = (f_cost(stepSize+epsilon)-cost)/epsilon;

				return std::make_tuple(cost, gradInDirection);
			};

			//auto initialStepSizeGuess =  maxInitialStepSizeGuess ;
			auto initialStepSizeGuess = algorithm.stepSize() == 0.0 ? maxInitialStepSizeGuess : algorithm.stepSize();
			real stepSize = il(func, maxStepSize, std::abs( initialStepSizeGuess ), initialGradient, initialCost);
            if(stepSize < alphaMin) stepSize = alphaMin;
			return stepSize;
		});
	}
	namespace experimental
	{
        inline auto makeDerivativeFreeStepSizeFinder(const real maxStepSize, const real lowerLimit, const real epsilon = 1e-8, const real gamma = 1e-6, const real delta = 0.66)
		{
            return makeStepSizeFinder([il{ DerivativeFreeLinesearch(gamma, delta, lowerLimit) }, maxStepSize, epsilon]
			(const auto& algorithm)
			{
				const auto& constProblem = algorithm.problem();
				using ProblemType = std::decay_t<decltype(constProblem)>;

				auto& problem = const_cast<ProblemType&>(constProblem);


				auto& dir = const_cast<std17::remove_cvref_t<decltype(algorithm.stepDirection())>&>(algorithm.stepDirection());

				const auto& initialParam = problem._params();
				const auto initialCost = problem.cost();

				auto f_cost = [&problem, &dir, &initialParam](real pmEps)
				{
					problem.update(initialParam + pmEps * dir);
					return problem.cost();
				};

				// protect againt analytic gradient not being a descent direction
				auto initialGradient = (f_cost(epsilon) - initialCost) / epsilon;
				if (initialGradient > 0)
				{
					dir = -dir;
					initialGradient *= -1;
				}

				auto func = [&f_cost](const real stepSize)
				{
					auto cost = f_cost(stepSize);
					return cost;
				};

				return il(func, maxStepSize, initialCost);
			});
		}
	}

}

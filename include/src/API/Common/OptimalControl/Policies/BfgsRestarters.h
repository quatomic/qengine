﻿/* COPYRIGHT
 *
 * file="BfgsRestarters.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/API/Common/OptimalControl/Policies/PolicyWrapper.h"

namespace qengine
{
	namespace internal
	{
		namespace policy
		{
			struct BfgsRestarterID {};
		}
	}
}
namespace qengine
{

	template<class F>
	internal::PolicyWrapper<internal::policy::BfgsRestarterID, F> makeBfgsRestarter(F f)
	{
		return internal::PolicyWrapper<internal::policy::BfgsRestarterID, F>{f};
	}

	namespace internal
	{
		template<class F1, class F2>
		auto operator+ (const PolicyWrapper<policy::BfgsRestarterID, F1>& left, const PolicyWrapper<policy::BfgsRestarterID, F2>& right)
		{
			return makeBfgsRestarter([f1{ left.f }, f2{ right.f }](const auto& alg)
			{
				return f1(alg) || f2(alg);
			});
		}
	}

	inline auto makeStepTimesYBfgsRestarter(const real tol, const bool verbose = false)
	{
		return makeBfgsRestarter([tol, verbose](const auto& alg)
		{
			if (alg.stepTimesY() < tol)
			{
				if (verbose) std::cout << "restarted bfgs" << std::endl;
				return true;
			}
			return false;
		});
	}

	inline auto makeIntervalBfgsRestarter(const count_t N, const bool verbose = false)
	{
		return makeBfgsRestarter([N, verbose](const auto& alg)
		{
			if (alg.iteration() % N == 0)
			{
				if (verbose) std::cout << "restarted bfgs" << std::endl;
				return true;
			}
			return false;
		});
	}

}

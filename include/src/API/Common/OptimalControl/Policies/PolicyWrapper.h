﻿/* COPYRIGHT
 *
 * file="PolicyWrapper.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

namespace qengine
{
	namespace internal
	{
		template<class PolicyID, class F>
		struct PolicyWrapper
		{
		public:
			PolicyWrapper(F f) : f(f) {}

			template<class... Ts>
			auto operator() (Ts&&... ts) -> decltype(std::declval<F>()(std::forward<Ts>(ts)...))
			{
				return f(std::forward<Ts>(ts)...);
			}

			template<class... Ts>
			auto operator() (Ts&&... ts) const -> decltype(std::declval<F>()(std::forward<Ts>(ts)...))
			{
				return f(std::forward<Ts>(ts)...);
			}

			F f;
		};

	}

	namespace experimental
	{
		namespace internal
		{

			template <class F>
			struct PointerPolicy
			{
				///
				/// Necessary for accessing the 'true' policy being wrapped up. 
				/// This is because makeAlgorithm implicitly copies the PolicyWrappers together with their internals.
				/// Used only for gaining access to member variables of QuickStep. 
				///

				PointerPolicy(F pPolicy) : _ptr(pPolicy) { }

				template<class... Ts>
				auto operator() (Ts&&... ts)
				{
					return (*_ptr)(std::forward<Ts>(ts)...);
				}

				template<class... Ts>
				auto operator() (Ts&&... ts) const
				{
					return (*_ptr)(std::forward<Ts>(ts)...);
				}

				F _ptr;
			};
		}
	}


	namespace internal
	{
		template<class PolicyID, class F>
		struct PolicyWrapper<PolicyID, experimental::internal::PointerPolicy<F>>
		{
			///
			/// Partial specialization of PolicyWrapper for PointerPolicy type.
			///
		public:
			PolicyWrapper(experimental::internal::PointerPolicy<F> f) : f(f) {}

			template<class... Ts>
			auto operator() (Ts&&... ts) -> decltype(std::declval<F>()(std::forward<Ts>(ts)...))
			{
				return (*f._ptr)(std::forward<Ts>(ts)...);
			}

			template<class... Ts>
			auto operator() (Ts&&... ts) const -> decltype(std::declval<F>()(std::forward<Ts>(ts)...))
			{
				return (*f._ptr)(std::forward<Ts>(ts)...);
			}

			decltype(*(std::declval<F>())) & wrapped()
			{
				return *(f._ptr);
			}

			experimental::internal::PointerPolicy<F> f;
		};
	}
}

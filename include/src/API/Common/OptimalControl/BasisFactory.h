﻿/* COPYRIGHT
 *
 * file="BasisFactory.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Utility/Constants.h"

#include "src/Abstract/OptimalControl/Parameters/Control.h"
#include "src/Abstract/OptimalControl/Parameters/Basis.h"

namespace qengine
{
	namespace internal 
	{
		template<class F>
		Basis constructBasis(F f, const count_t nBasisElements, const count_t paramCount)
		{
			auto retval = std::vector<Control>();
			retval.reserve(nBasisElements);

			for (auto i_basisElement = 0u; i_basisElement < nBasisElements; ++i_basisElement)
			{
				auto c = std::vector<Control>{};
				c.reserve(paramCount);
				for (auto i_param = 0u; i_param < paramCount; ++i_param)
				{
					c.push_back(f(i_basisElement,i_param));
				}
				retval.push_back(makeControl(c));
			}

			return retval;
		}
		
		RMat makeRandomizedFactors(count_t nBasisElements, count_t paramCount, real maxRandVal);
	}

	Basis makeSineBasis(count_t nBasisElements, const ControlMetaData& data, real maxRandVal = 0.0);
	Basis makeSineBasis(count_t nBasisElements, count_t paramCount, count_t controlSize, real dt, real maxRandVal = 0.0);

	Basis makeFourierBasis(count_t nBasisElements, const ControlMetaData& data, real maxRandVal = 0.0);
	Basis makeFourierBasis(count_t nBasisElements, count_t paramCount, count_t controlSize, real dt, real maxRandVal = 0.0);
}

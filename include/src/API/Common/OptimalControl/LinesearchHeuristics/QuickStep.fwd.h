#pragma once
namespace qengine
{
	namespace experimental
	{
		template <class Control, class LineSearch>
		class QuickStep;
	}
}
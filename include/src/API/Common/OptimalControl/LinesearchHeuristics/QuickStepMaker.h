/* COPYRIGHT
*
* file="QuickStepMaker.h"
* Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
*
* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a
* copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/
#pragma once
#include "src/API/Common/OptimalControl/LinesearchHeuristics/QuickStep.h"
#include "src/API/Common/OptimalControl/Policies/StepsizeFinders.h"

namespace qengine
{
	namespace internal
	{
		template <typename T, typename V, bool val>
		struct type_if
		{
			using type = T;
		};

		template <typename T, typename V>
		struct type_if<T, V, false>
		{
			using type = V;
		};
	}
	namespace experimental
	{
		template <typename LineSearch, typename ControlType>
		inline auto makeQuickStepHeuristic(	const LineSearch lsAlgorithm,
											const ControlType &,
											const count_t threshold,
											const count_t unrollBound)

		{
			// select the type of the Controls to be used in QuickStep template parameter
			// ControlType (same) for 'Control' input type, BasisControl for 'Basis' input type
			// the reason is that BasisControl type is inaccessible from the outside scope
			constexpr static bool isBasisType = std::is_same<ControlType, Basis>::value;
			using SelectedControlType = typename qengine::internal::type_if<BasisControl, ControlType, isBasisType>::type;
			using QuickStepType = QuickStep<SelectedControlType, LineSearch>;
			std::shared_ptr<QuickStepType> qs = std::make_shared<QuickStepType>(lsAlgorithm, threshold, unrollBound);
			return makeStepSizeFinder(experimental::internal::PointerPolicy<decltype(qs)> { qs });
		}
	}
}

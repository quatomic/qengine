/* COPYRIGHT
*
* file="QuickStep.h"
* Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
*
* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a
* copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/
#pragma once
#include <limits>
#include "src/Utility/qengine_assert.h"
#include "src/NumberTypes.h"

namespace qengine
{
namespace experimental
{
	template <typename Control, typename LineSearch>
	class QuickStep
	{
		enum class State {UNROLL, PROGRESSING, ROLLBACK};
		struct RollbackState
		{
			Control _direction;
			real _stepSize;
			real _cost;
		};

	public:
		explicit QuickStep(const LineSearch & linesearch, const count_t unrollThreshold, const count_t nUnrollsAfterThreshold);

		template <typename Algorithm>
		real operator()(Algorithm & algorithm);

		count_t sameStepSizes() const noexcept;

		count_t unrollsLeft() const noexcept;

		count_t nUnrollsAfterThreshold() const noexcept;

		count_t unrollThreshold() const noexcept;

		count_t nUnrollsPerformed() const noexcept;

		count_t nRollbacksPerformed() const noexcept;

		bool didRollback() const noexcept;

		double previousStepSize() const noexcept;

		void reset() noexcept;

		virtual ~QuickStep() = default;

	private:
		
		bool checkEnterUnroll() const noexcept;
		bool checkEnterProgressing() const noexcept;
		void setProgressing() noexcept;
		void setUnrolling() noexcept;
		void setRollback() noexcept;

		void updateCounter(const double) noexcept;
		double quickIteration();

		template <typename Algorithm>
		double makeRollback(Algorithm & alg);

		template <typename Algorithm>
		bool checkEnterRollback(const Algorithm & alg) const;

		template <typename Algorithm>
		void cacheStepInfo(const Algorithm & alg, const real step);

		template <typename Algorithm>
		bool costIncrease(const Algorithm & alg) const;

		template <typename Algorithm>
		void updateState(const Algorithm & alg) noexcept;

	private:
		const count_t _unrollThreshold;
		const count_t _nUnrollsAfterThreshold;
		count_t  _nSameStepSizes = 0;
		count_t _unrollsLeft = 0;
		real _previousStepSize = 0.0;
		State _state = State::PROGRESSING;
		count_t _nUnrollsPerformed = 0;
		count_t _nRollbacksPerformed = 0;
		bool _didRollback = false;
		RollbackState _rollback;
		LineSearch _linesearch;
	};

		template <typename Control, typename LineSearch>
		template <typename Algorithm>
		real QuickStep<Control, LineSearch>::operator()(Algorithm & algorithm)
		{
			real step = 0.0;

			updateState(algorithm);

			switch (_state)
			{
				case State::PROGRESSING:
				{
					step = _linesearch(algorithm);
					updateCounter(step);
					break;
				}
				case State::UNROLL:
				{
					step = quickIteration();
					cacheStepInfo(algorithm, step);
					break;
				}
				case State::ROLLBACK:
				{
					step = makeRollback(algorithm);
					break;
				}
			}

			updateState(algorithm);

			return step;
		}

		template <typename Control, typename LineSearch>
		void QuickStep<Control, LineSearch>::reset() noexcept
		{
			_nUnrollsPerformed = 0;
			_nRollbacksPerformed = 0;
			_nSameStepSizes = 0;
			_unrollsLeft = 0;
			_state = State::PROGRESSING;
			_rollback._cost = std::numeric_limits<double>::infinity();
		}

		template <typename Control, typename LineSearch>
		template <typename Algorithm>
		double QuickStep<Control, LineSearch>::makeRollback(Algorithm & alg)
		{
			// rollback the direction recently found by the algorithm
			alg._stepDirection = _rollback._direction;

			// locally rollback the problem to the previous state, so that line search can find a new step size
			const auto stepBack = -_rollback._stepSize * _rollback._direction;
			auto & problem = alg._problem;
			auto x = problem._params();
			x += stepBack;
			problem.update(x);
			++_nRollbacksPerformed;
			_didRollback = true;
			// compute and return a newly found step size, inlcuding the rollback (outside algorithm 'accumulates' the path independently from what we do here)
			return _linesearch(alg) - _rollback._stepSize;
		}

		template <typename Control, typename LineSearch>
		template <typename Algorithm>
		bool QuickStep<Control, LineSearch>::checkEnterRollback(const Algorithm & alg) const
		{
			return costIncrease(alg);
		}

		template <typename Control, typename LineSearch>
		template <typename Algorithm>
		void QuickStep<Control, LineSearch>::cacheStepInfo(const Algorithm & alg, const real step)
		{
			qengine_assert(step > 0, "QuickStep can't cache a nonpositive step. Is the linesearch algorithm correct?");
			_rollback._direction = alg.stepDirection();
			_rollback._cost = alg.problem().cost();
			_rollback._stepSize = step;
		}

		template <typename Control, typename LineSearch>
		template <typename Algorithm>
		bool QuickStep<Control, LineSearch>::costIncrease(const Algorithm & alg) const
		{
			return _rollback._cost < alg.problem().cost();
		}

		template <typename Control, typename LineSearch>
		template <typename Algorithm>
		void QuickStep<Control, LineSearch>::updateState(const Algorithm & alg) noexcept
		{
			switch (_state)
			{
				case State::PROGRESSING:
				{
					_didRollback = false;
					if (checkEnterUnroll())
					{
						setUnrolling();
					}
					break;
				}
				case State::UNROLL:
				{
					_didRollback = false;
					if (checkEnterRollback(alg))
					{
						setRollback();
					}
					else if (checkEnterProgressing())
					{
						setProgressing();
					}
					break;
				}
				case State::ROLLBACK:
				{
					setProgressing();
					break;
				}
			}
		}

		template <typename Control, typename LineSearch>
		QuickStep<Control, LineSearch>::QuickStep(	const LineSearch & ls,
										const count_t unrollThreshold,
										const count_t nUnrollsAfterThreshold) : _unrollThreshold(unrollThreshold), _nUnrollsAfterThreshold(nUnrollsAfterThreshold), _linesearch(ls)
		{
			qengine_assert(unrollThreshold > 0, "QuickStep unrollThreshold value has to be > 0");
			qengine_assert(nUnrollsAfterThreshold > 0, "QuickStep unroll number has to be > 0");
			_rollback._cost = std::numeric_limits<double>::infinity();
			_rollback._stepSize = 0.0;
			_rollback._direction = Control{};
			_previousStepSize = 1.0;
			_nSameStepSizes = 0;
			_unrollsLeft = 0;
		}

		template <typename C, typename LineSearch>
		void QuickStep<C, LineSearch>::updateCounter(const real step) noexcept
		{
			if (step == _previousStepSize)
			{
				++_nSameStepSizes;
			}
			else
			{
				_previousStepSize = step;
				_nSameStepSizes = 1;
			}
		}

		template <typename C, typename LineSearch>
		real QuickStep<C, LineSearch>::previousStepSize() const noexcept
		{
			return _previousStepSize;
		}

		template <typename C, typename LineSearch>
		bool QuickStep<C, LineSearch>::checkEnterUnroll() const noexcept
		{
			return (_unrollThreshold == _nSameStepSizes);
		}

		template <typename C, typename LineSearch>
		bool QuickStep<C, LineSearch>::checkEnterProgressing() const noexcept
		{
			return (0 == _unrollsLeft);
		}

		template <typename C, typename LineSearch>
		void QuickStep<C, LineSearch>::setRollback() noexcept
		{
			_state = State::ROLLBACK;
		}

		template <typename C, typename LineSearch>
		void QuickStep<C, LineSearch>::setProgressing() noexcept
		{
			_nSameStepSizes = _unrollThreshold - 1;
			_unrollsLeft = 0;
			_state = State::PROGRESSING;
			_rollback._cost = std::numeric_limits<double>::infinity();
		}

		template <typename C, typename LineSearch>
		void QuickStep<C, LineSearch>::setUnrolling() noexcept
		{
			_state = State::UNROLL;
			_unrollsLeft = _nUnrollsAfterThreshold;
		}

		template <typename C, typename LineSearch>
		real QuickStep<C, LineSearch>::quickIteration()
		{
			qengine_assert(_unrollsLeft > 0, "QuickStep error: number of unrolls is 0");
			--_unrollsLeft;
			++_nUnrollsPerformed;
			return previousStepSize();
		}

		template <typename C, typename LineSearch>
		count_t QuickStep<C, LineSearch>::sameStepSizes() const noexcept
		{
			return _nSameStepSizes;
		}

		template <typename C, typename LineSearch>
		count_t QuickStep<C, LineSearch>::unrollsLeft() const noexcept
		{
			return _unrollsLeft;
		}

		template <typename C, typename LineSearch>
		count_t QuickStep<C, LineSearch>::nUnrollsAfterThreshold() const noexcept
		{
			return _nUnrollsAfterThreshold;
		}

		template <typename C, typename LineSearch>
		count_t QuickStep<C, LineSearch>::unrollThreshold() const noexcept
		{
			return _unrollThreshold;
		}

		template <typename C, typename LineSearch>
		count_t QuickStep<C, LineSearch>::nUnrollsPerformed() const noexcept
		{
			return _nUnrollsPerformed;
		}

		template <typename C, typename LineSearch>
		count_t QuickStep<C, LineSearch>::nRollbacksPerformed() const noexcept
		{
			return _nRollbacksPerformed;
		}

		template <typename C, typename LineSearch>
		bool QuickStep<C, LineSearch>::didRollback() const noexcept
		{
			return _didRollback;
		}
}
}
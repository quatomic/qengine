﻿/* COPYRIGHT
 *
 * file="BasisFactory.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/Common/OptimalControl/BasisFactory.h"

#include <armadillo>

namespace qengine
{
	RMat internal::makeRandomizedFactors(const count_t nBasisElements, const count_t paramCount, const real maxRandVal)
	{
		arma::arma_rng::set_seed_random();

		auto armamat = arma::mat((arma::randu(paramCount, nBasisElements) - 0.5)*2*maxRandVal);
		for(auto i_basisElement = 0u; i_basisElement<nBasisElements;++i_basisElement)
		{
			for(auto i_param = 0u; i_param<paramCount;++i_param)
			{
				armamat.at(i_param, i_basisElement) += i_basisElement + 1;
			}
		}

		return RMat(std::move(armamat));
	}

	Basis makeSineBasis(const count_t nBasisElements, const ControlMetaData& data, const real maxRandVal)
	{
		return makeSineBasis(nBasisElements, data.paramCount, data.controlSize, data.dt, maxRandVal);
	}

	Basis makeSineBasis(const count_t nBasisElements, const count_t paramCount, const count_t controlSize,
		const real dt, const real maxRandVal)
	{
		auto factors = internal::makeRandomizedFactors(nBasisElements, paramCount, maxRandVal);

		const auto t = makeTimeControl(controlSize, dt);
		const auto omegat = PI * t / t.endTime();

		auto f = [&omegat, &factors](const count_t i_basisElement, const count_t i_param)
		{
			return sin(factors.at(i_param, i_basisElement)*omegat);
		};

		return internal::constructBasis(f, nBasisElements, paramCount);
	}

	Basis makeFourierBasis(const count_t nBasisElements, const ControlMetaData& data, const real maxRandVal)
	{
		return makeFourierBasis(nBasisElements, data.paramCount, data.controlSize, data.dt, maxRandVal);
	}

	Basis makeFourierBasis(const count_t nBasisElements, const count_t paramCount, const count_t controlSize, const real dt, const real maxRandVal)
	{
		auto factors = internal::makeRandomizedFactors(nBasisElements, paramCount, maxRandVal);

		const auto t = makeTimeControl(controlSize, dt);
		const auto omegat = 2*PI * t / t.endTime();

		auto f = [&omegat, &factors](const count_t i_basisElement, const count_t i_param)
		{
			// change between cosine (even) and sine (odd)-basis
			if(i_basisElement%2 == 0) return cos(factors.at(i_param, i_basisElement)*omegat);
			return sin(factors.at(i_param, i_basisElement)*omegat);
		};

		return internal::constructBasis(f, nBasisElements, paramCount);
	}
}

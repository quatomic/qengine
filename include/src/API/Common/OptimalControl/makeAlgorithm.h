﻿/* COPYRIGHT
 *
 * file="sortAndAddDefaults.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>

#include "src/NumberTypes.h"
#include "src/Utility/TypeList.h"
#include "src/Utility/cpp17Replacements/check_all_true.h"

#include "src/Utility/std17/apply.h"

#include "src/Utility/TypeMap.h"

#include "src/Utility/makeTupleOfSortedInputsWithDefaultsAdded.h"

#include "src/API/Common/OptimalControl/Policies/PolicyWrapper.h"


namespace qengine
{
	namespace internal
	{
		template<class... InputPolicyIDs, class... InputFunctions>
		TypeMap<std::tuple<InputPolicyIDs...>, std::tuple<InputFunctions...>> toTypeMap(PolicyWrapper<InputPolicyIDs, InputFunctions>... Ps)
		{
			return makeTypeMap(TypeList<InputPolicyIDs...>(), std::make_tuple(Ps.f...));
		}
		
		/// This is the generic way to create an optimal control algorithm with a bunch of policies. 
		/// The orderedDefaultMap contains the defaults for the policies in the order they should be passed to the creation-function f
		/// Ps contains the wrapped policies that the user has passed in, in any order. 
		template<class F, class MapOfDefaults, class... InputPolicyIDs, class... InputPolicies>
		auto makeAlgorithm(F f, MapOfDefaults orderedDefaultMap, PolicyWrapper<InputPolicyIDs, InputPolicies>... Ps)
		{
			auto inputMap = toTypeMap(Ps...);
			static_assert(internal::areInputKeysValid(Type<MapOfDefaults>{}, Type<decltype(inputMap)>{}), "One of the passed-in policies is not allowed for this algorithm");
			return std17::apply(f, internal::makeTupleOfSortedInputsWithDefaultsAdded(orderedDefaultMap, inputMap));
		}
	}
}

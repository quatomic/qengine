cmake_minimum_required (VERSION 3.3)
project (QEngine)

file(GLOB_RECURSE SOURCES 
	"*/*.h" "*/*.cpp" "*/*/*.h " "*/*/*.cpp"
)
file(GLOB ROOT_FILES "${CMAKE_CURRENT_SOURCE_DIR}/*.h" "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

file(GLOB EXTERNAL_INCLUDES 
	"${CMAKE_SOURCE_DIR}/include/qengine/*.h"
)

add_library(API OBJECT ${SOURCES} ${EXTERNAL_INCLUDES} ${ROOT_FILES})
target_include_directories(API PRIVATE "${ARMADILLO_INCLUDE_DIR}")


if(MSVC)
	source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SOURCES})
	source_group("external" FILES ${EXTERNAL_INCLUDES})
	source_group("" FILES ${ROOT_FILES})
endif()


set (pchFile "${CMAKE_CURRENT_SOURCE_DIR}/Common/pch.h")
add_pch(API ${pchFile})

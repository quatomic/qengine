﻿/* COPYRIGHT
 *
 * file="defaultSteppingAlg.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/Constants.h"

#include "src/Abstract/Physics/General/TimeStepper.h"

#include "src/Abstract/Physics/Spatial/SplitStep.h"
#include "src/Abstract/Physics/Spatial/HamiltonianFunction.h"

#include "src/API/Common/Spatial/1D/LinearHamiltonian1D.h"
#include "src/API/Common/Spatial/1D/TimeSteppersCommon.h"

namespace qengine
{
	template<class SerializedHilbertSpace>
	auto makeSplitStepAlgorithm(const LinearHamiltonian1D<SerializedHilbertSpace>& H)
	{
		using namespace std::complex_literals;
		using namespace std::complex_literals;
		return[V{ H._potential() }, T{ oneD::makePSpaceKinetic(H._serializedHilbertSpace()) }, splitStepAlg{ SplitStepAlgorithm() }](auto& psi, real dt)
		{
			auto expV = exp(-1i * dt / 2.0*V);
			auto applyExpV = oneD::makeApplier(expV);

			auto expT = exp(-1i * dt*T);

			auto applyExpT = oneD::makeApplier(expT);

			splitStepAlg(psi, applyExpV, applyExpT, applyExpV);
		};
	}

	template<class SerializedHilbertSpace>
	auto makeSplitStepAlgorithm(const LinearHamiltonian1D<SerializedHilbertSpace>& H, const real dt)
	{
		using namespace std::complex_literals;
		auto expV = exp(-1i * 0.5*dt*H._potential());
		auto expT = exp(-1i * dt*oneD::makePSpaceKinetic(H._serializedHilbertSpace()));

		auto applyExpV = [expV](auto& psi)
		{
			auto intermediate = expV * psi;
			psi = intermediate;//avoid move because of fft.
		};


		auto applyExpT = [expT](auto& psi)
		{
			auto intermediate = expT * psi;
			psi = intermediate;//avoid move because of fft.
		};

		return[applyExpT, applyExpV, splitStepAlg{ SplitStepAlgorithm() }](auto& psi)
		{
			splitStepAlg(psi, applyExpV, applyExpT, applyExpV);
		};
	}

	template<class  SerializedHilbertSpace, class PotentialFunction, class... Params>
	auto makeSplitStepAlgorithm(const HamiltonianFunction<LinearHamiltonian1D<SerializedHilbertSpace>, PotentialFunction>& H, const Tuple<Params...>& initialParams)
	{
		using namespace std::complex_literals;
		auto V = [V_dyn{ H._potentialFunction() }, V_stat{ H._H0()._potential() }](auto... ts){return V_stat + V_dyn(std::forward<decltype(ts)>(ts)...); };

		return[V, T{ oneD::makePSpaceKinetic(H._H0()._serializedHilbertSpace()) }, splitStepAlg{ SplitStepAlgorithm() }, V_cache = V(initialParams)](auto& psi, real dt,const auto& p2) mutable
		{
			auto expV = exp(-1i * 0.5*dt*V_cache);
			auto intermediate = expV * psi;
			psi = intermediate;

			auto expT = exp(-1i * dt*T);
			auto applyExpT = oneD::makeApplier(expT);
			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			V_cache = V(p2);
			auto expV2 = exp(-1i * 0.5*dt*V_cache);

			intermediate = expV2 * psi;
			psi = intermediate;

		};
	}

	template<class  SerializedHilbertSpace, class PotentialFunction, class... Params>
	auto makeSplitStepAlgorithm(const HamiltonianFunction<LinearHamiltonian1D<SerializedHilbertSpace>, PotentialFunction>& H, const real dt, const Tuple<Params...>& initialParams)
	{
		using namespace std::complex_literals;
		auto T = oneD::makePSpaceKinetic(H._H0()._serializedHilbertSpace());
		auto expT = exp(-1i * dt*T);

		auto applyExpT = [expT](auto& psi)
		{
			auto intermediate = expT * psi;
			psi = intermediate;//avoid move because of fft.
		};

		auto V = [V_dyn{ H._potentialFunction() }, V_stat{ H._H0()._potential() }](auto... ts){return V_stat + V_dyn(std::forward<decltype(ts)>(ts)...); };

		return[V, applyExpT, splitStepAlg{ SplitStepAlgorithm() }, dt, expV_cache = exp(-1i * 0.5*dt*V(initialParams))](auto& psi, const auto& p2) mutable
		{

			auto intermediate = expV_cache * psi;
			psi = intermediate;

			splitStepAlg.iFFT_expT_FFT(psi, applyExpT);

			expV_cache = exp(-1i * 0.5*dt*V(p2));

			intermediate = expV_cache * psi;
			psi = intermediate;

		};

	}
}

namespace qengine
{
	template<class SerializedHilbertSpace>
	auto makeDefaultStepAlgorithm(const LinearHamiltonian1D<SerializedHilbertSpace>& H)
	{
		return addImagBounds(splitStep(H), makeDefaultImagPot(H._serializedHilbertSpace()));
	}

	template<class SerializedHilbertSpace, class PotentialFunction>
	auto makeDefaultStepAlgorithm(const HamiltonianFunction<LinearHamiltonian1D<SerializedHilbertSpace>, PotentialFunction>& H)
	{
		return addImagBounds(splitStep(H), makeDefaultImagPot(H._H0()._serializedHilbertSpace()));
	}

	template<class SerializedHilbertSpace>
	auto makeDefaultStepAlgorithm(const LinearHamiltonian1D<SerializedHilbertSpace>& H, real dt)
	{
		return addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._serializedHilbertSpace()), dt);
	}

	template<class SerializedHilbertSpace, class PotentialFunction>
	auto makeDefaultStepAlgorithm(const HamiltonianFunction<LinearHamiltonian1D<SerializedHilbertSpace>, PotentialFunction>& H, real dt)
	{
		return addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._serializedHilbertSpace()), dt);
	}
}

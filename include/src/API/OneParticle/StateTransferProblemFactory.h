﻿/* COPYRIGHT
 *
 * file="StateTransferProblemFactory.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/Spatial/HamiltonianFunction.h"
#include "src/Abstract/Physics/Spatial/Spectrum.impl.h"

#include "src/Abstract/OptimalControl/Problems/LinearStateTransferProblem.h"
#include "src/API/Common/Spatial/OptimalControlHelpers.h"

#include "src/API/Common/Spatial/1D/LinearHamiltonian1D.h"
#include "src/API/OneParticle/defaultSteppingAlg.h"

namespace qengine
{
	template<class SerializedHilbertSpace, class PotentialFunction, class dHdu>
	auto makeStateTransferProblem(
		const HamiltonianFunction<LinearHamiltonian1D<SerializedHilbertSpace>, PotentialFunction>& H,
		const dHdu dV,
		const FunctionOfX<complex, SerializedHilbertSpace>& initialState,
		const FunctionOfX<complex, SerializedHilbertSpace>& targetState,
		const Control& x0
	)
	{

		const auto& dt = x0.dt();

		auto alg = addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._serializedHilbertSpace()), dt);
		auto algb = addImagBounds(splitStep(H, -dt), makeDefaultImagPot(H._H0()._serializedHilbertSpace()), dt);

		return makeLinearStateTransferProblem(alg, algb, dV, initialState, targetState, x0);
	}

	template<class SerializedHilbertSpace, class PotentialFunction>
	auto makeStateTransferProblem(
		const HamiltonianFunction<LinearHamiltonian1D<SerializedHilbertSpace>, PotentialFunction>& H,
		const FunctionOfX<complex, SerializedHilbertSpace>& initialState,
		const FunctionOfX<complex, SerializedHilbertSpace>& targetState,
		const Control& x0
	)
	{
		auto dV = makeNumericDiffPotential(H._potentialFunction());
		return makeStateTransferProblem(H, dV, initialState, targetState, x0);
	}
}

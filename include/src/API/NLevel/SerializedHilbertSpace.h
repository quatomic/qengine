﻿/* COPYRIGHT
 *
 * file="SerializedHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/Physics/SecondQuantization/is2ndQSerializedHilbertSpace.h"

namespace qengine
{
	namespace internal
	{
		struct SerializedNLevelHilbertSpace
		{
			const count_t nLevels;
			const count_t dim = nLevels;
		};

		inline bool operator==(const SerializedNLevelHilbertSpace& left, const SerializedNLevelHilbertSpace& right)
		{
			return left.nLevels == right.nLevels;
		}
		inline bool operator!=(const SerializedNLevelHilbertSpace& left, const SerializedNLevelHilbertSpace& right)
		{
			return !(left == right);
		}


		template<>
		struct isLabelledAs_SerializedHilbertSpace<internal::SerializedNLevelHilbertSpace> : std::true_type {};
	}
}

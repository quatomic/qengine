﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/NLevel/ApiHilbertSpace.h"

#include "src/Utility/qengine_assert.h"

namespace qengine
{
	namespace n_level
	{
		ApiHilbertSpace::ApiHilbertSpace(const count_t nLevels) : serializedHilbertSpace_{ nLevels }
		{
		}

		count_t ApiHilbertSpace::nLevels() const
		{
			return serializedHilbertSpace_.nLevels;
		}

		count_t ApiHilbertSpace::_get(const internal::FockState& basisState) const
		{
			qengine_assert(basisState._quanta().size() == 1, "only single-mode basisStates are possible for nlevel");
			qengine_assert(basisState.at(0) < nLevels(), "index out of bounds");

			return basisState.at(0);
		}

		ApiHilbertSpace makeHilbertSpace(const count_t nLevels)
		{
			return ApiHilbertSpace(nLevels);
		}
	}
}

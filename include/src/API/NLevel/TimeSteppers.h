﻿/* COPYRIGHT
 *
 * file="TimeSteppers.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/Physics/General/TimeStepper.h"

#include "src/Abstract/Physics/SecondQuantization/State.h"
#include "src/Abstract/Physics/SecondQuantization/Operator.h"
#include "src/Abstract/Physics/SecondQuantization/DirectExponentiationStepping.h"
#include "src/Abstract/Physics/SecondQuantization/OperatorFunction.h"

#include "src/API/NLevel/ApiHilbertSpace.h"

namespace qengine
{
	template<template<class>class MatType, class NumberType>
	auto makeTimeStepper(const Operator<MatType, NumberType, internal::SerializedNLevelHilbertSpace>& H, const State<internal::SerializedNLevelHilbertSpace>& psi0)
	{
		return makeTimeStepper(directExponentiation(H), psi0);
	}


	template<template<class>class MatType, class NumberType>
	auto makeFixedTimeStepper(const Operator<MatType, NumberType, internal::SerializedNLevelHilbertSpace>& H, const State<internal::SerializedNLevelHilbertSpace>& psi0, const real dt)
	{
		return makeFixedTimeStepper(directExponentiation(H, dt), psi0);
	}

	template<class OpFunction, class... Params>
	auto makeTimeStepper(const OperatorFunction<OpFunction, Params...>& H, const State<internal::SerializedNLevelHilbertSpace>& psi0, const Tuple<Params...>& initialParams)
	{
		return makeTimeStepper(directExponentiation(H), psi0, initialParams);
	}
	template<class OpFunction, class... Params>
	auto makeFixedTimeStepper(const OperatorFunction<OpFunction, Params...>& H, const State<internal::SerializedNLevelHilbertSpace>& psi0, const real dt, const Tuple<Params...>& initialParams)
	{
		return makeFixedTimeStepper(directExponentiation(H, dt), psi0, initialParams);
	}

	template<class OpFunction, class... Params, typename = std::enable_if_t<repl17::check_all_true(std17::is_same_v<Params, real>...)>>
	auto makeTimeStepper(const OperatorFunction<OpFunction, Params...>& H, const State<internal::SerializedNLevelHilbertSpace>& psi0, const RVec& initialParams)
	{
		return makeTimeStepper(H, psi0, internal::tupleFromVec<Params...>(initialParams));
	}

	template<class OpFunction, class... Params, typename = std::enable_if_t<repl17::check_all_true(std17::is_same_v<Params, real>...)>>
	auto makeFixedTimeStepper(const OperatorFunction<OpFunction, Params...>& H, const State<internal::SerializedNLevelHilbertSpace>& psi0, const real dt, const RVec& initialParams)
	{
		return makeFixedTimeStepper(H, psi0, dt, internal::tupleFromVec<Params...>(initialParams));
	}

	template<class OpFunction, class... Params>
	auto makeTimeStepper(const OperatorFunction<OpFunction, Params...>& H, const State<internal::SerializedNLevelHilbertSpace>& psi0)
	{
		return makeTimeStepper(H, psi0, H.initialParams());
	}

	template<class OpFunction, class... Params>
	auto makeFixedTimeStepper(const OperatorFunction<OpFunction, Params...>& H, const State<internal::SerializedNLevelHilbertSpace>& psi0, const real dt)
	{
		return makeFixedTimeStepper(H, psi0, dt, H.initialParams());
	}
}

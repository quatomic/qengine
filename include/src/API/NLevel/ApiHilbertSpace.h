﻿/* COPYRIGHT
 *
 * file="ApiHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/Physics/SecondQuantization/isApiHilbertSpace.h"

#include "src/Abstract/Physics/SecondQuantization/FockState.h"

#include "src/API/NLevel/SerializedHilbertSpace.h"

namespace qengine
{
	namespace n_level
	{
		class ApiHilbertSpace
		{
		public:
			using SerializedHilbertSpace = internal::SerializedNLevelHilbertSpace;

			explicit ApiHilbertSpace(count_t nLevels);

			count_t nLevels() const;

			/// INTERNAL USE ONLY
		public:
			count_t _get(const internal::FockState& basisState) const;

			const internal::SerializedNLevelHilbertSpace& _serializedHilbertSpace() const { return serializedHilbertSpace_; }

		private:
			internal::SerializedNLevelHilbertSpace serializedHilbertSpace_;
		};

		ApiHilbertSpace makeHilbertSpace(const count_t nLevels);
	}
}


namespace qengine
{
	namespace second_quantized
	{
		template<>
		struct has2ndQApiHilbertSpaceLabel<n_level::ApiHilbertSpace> : std::true_type {};
	}
}

static_assert(qengine::second_quantized::is2ndQApiHilbertSpace_v<qengine::n_level::ApiHilbertSpace>, "");
static_assert(qengine::second_quantized::is2ndQSerializedHilbertSpace_v<qengine::internal::SerializedNLevelHilbertSpace>, "");
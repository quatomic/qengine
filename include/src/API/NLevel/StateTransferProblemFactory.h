﻿/* COPYRIGHT
 *
 * file="StateTransferProblemFactory.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/SecondQuantization/DirectExponentiationStepping.h"

#include "src/Abstract/OptimalControl/Problems/LinearStateTransferProblem.h"

#include "src/API/NLevel/ApiHilbertSpace.h"
#include "src/API/Common/SecondQuantized/OptimalControlHelpers.h"

namespace qengine
{
	template<
		class dHdu,
		class HamiltonianFunction,
		typename = std::enable_if_t<
		is_callable_v<HamiltonianFunction, Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunction, Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace>(RVec)>
		>
	>
		auto makeStateTransferProblem(
			const HamiltonianFunction& H,
			const dHdu dH,
			const State<internal::SerializedNLevelHilbertSpace>& initialState,
			const State<internal::SerializedNLevelHilbertSpace>& targetState,
			const Control& x0
		)
	{
		const auto dt = x0.dt();

		auto alg = directExponentiation(H, dt);
		auto algb = directExponentiation(H, -dt);

		return makeLinearStateTransferProblem(alg, algb, dH, initialState, targetState, x0);
	}

	template<
		class HamiltonianFunction,
		typename = std::enable_if_t<
		is_callable_v<HamiltonianFunction, Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace>(RVec)> ||
		is_callable_v<HamiltonianFunction, Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace>(RVec)>
		>
	>
		auto makeStateTransferProblem(
			const HamiltonianFunction& H,
			const State<internal::SerializedNLevelHilbertSpace>& initialState,
			const State<internal::SerializedNLevelHilbertSpace>& targetState,
			const Control& x0
		)
	{
		auto dHdu = makeNumericDiffHamiltonian(H, x0.paramCount());
		return makeStateTransferProblem(H, dHdu, initialState, targetState, x0);
	}
}

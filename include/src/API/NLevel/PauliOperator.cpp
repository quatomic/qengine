﻿/* COPYRIGHT
 *
 * file="PauliOperator.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/NLevel/PauliOperator.h"

#include "src/Utility/Constants.h"

#include "src/API/NLevel/makeOperator.h"

#include "src/Utility/LinearAlgebra/SqDenseMat.h"

namespace qengine
{
	namespace n_level
	{
		Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace> makePauliI()
		{
			const auto s = makeHilbertSpace(2);
			return makeOperator(s, { {1, 0},
				{0, 1 } });
		}

		Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace> makePauliX()
		{
			const auto s = makeHilbertSpace(2);
			return makeOperator(s, { {0, 1},
				{1, 0 } });
		}

		Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace> makePauliY()
		{
			using namespace std::complex_literals;
			const auto s = makeHilbertSpace(2);
			return makeOperator(s, { { 0,        -1i },
										{1i, 0 } });
		}

		Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace> makePauliZ()
		{
			const auto s = makeHilbertSpace(2);
			return makeOperator(s, { {1,  0},
				{ 0, -1 } });
		}
	}
}

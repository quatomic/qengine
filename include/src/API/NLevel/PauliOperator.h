﻿/* COPYRIGHT
 *
 * file="PauliOperator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Abstract/Physics/SecondQuantization/Operator.h"

#include "src/API/NLevel/SerializedHilbertSpace.h"

namespace qengine
{
	namespace internal
	{
		template<class> class SqDenseMat;
	}

	namespace n_level
	{
		Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace> makePauliI();
		Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace> makePauliX();
		Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace> makePauliY();
		Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace> makePauliZ();
	}
}

﻿/* COPYRIGHT
 *
 * file="makeOperator.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/NLevel/makeOperator.h"


#include "src/Abstract/Physics/SecondQuantization/Operator.impl.h"
#include "src/Abstract/Physics/SecondQuantization/Spectrum.impl.h"

namespace qengine
{
	Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace> makeOperator(const n_level::ApiHilbertSpace& s, const FockOperator& op)
	{
		auto mat = CMat(s.nLevels() - 1, s.nLevels() - 1, 0.0);
		for (auto i = 0u; i < s.nLevels(); ++i)
		{
			auto res = op * fock::state{ i };
			for (const auto& a : res.factors())
			{
				if (a.first.at(0) < s.nLevels())
				{
					mat.at(s._get(a.first), i) = a.second;
				}
			}
		}

		return internal::makeOperator(internal::SqDenseMat<complex>(mat), s._serializedHilbertSpace());
	}

	Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace> makeOperator(const n_level::ApiHilbertSpace& s, RMat rep)
	{
		return { internal::SqDenseMat<real>(std::move(rep)), s._serializedHilbertSpace() };
	}

	Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace> makeOperator(const n_level::ApiHilbertSpace& s, CMat rep)
	{
		return { internal::SqDenseMat<complex>(std::move(rep)), s._serializedHilbertSpace() };
	}

	Operator<internal::SqDenseMat, real, internal::SerializedNLevelHilbertSpace> makeOperator(const n_level::ApiHilbertSpace& s, std::initializer_list<std::initializer_list<real>> rep)
	{
		return internal::makeOperator(internal::SqDenseMat<real>(RMat(rep)), s._serializedHilbertSpace());
	}

	Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace> makeOperator(const n_level::ApiHilbertSpace& s, std::initializer_list<std::initializer_list<complex>> rep)
	{\
		return internal::makeOperator(internal::SqDenseMat<complex>(CMat(rep)), s._serializedHilbertSpace());
	}
}

﻿/* COPYRIGHT
 *
 * file="PolicyRequirementsBFGSandSteepest.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>
#include "src/Utility/is_callable.h"
#include "src/NumberTypes.h"


namespace qengine
{
	namespace internal
	{
		template<class Policy, class Algorithm, class SFINAE = void> struct isStopper: std::false_type{};
		template<class Policy, class Algorithm>
		struct isStopper<Policy, Algorithm, std::enable_if_t<is_callable_v<Policy, bool(const Algorithm&)>>>: std::true_type{};

		template<class Policy, class Algorithm, class SFINAE = void> struct isCollector: std::false_type{};
		template<class Policy, class Algorithm>
		struct isCollector<Policy, Algorithm, std::enable_if_t<is_callable_v<Policy, void(const Algorithm&)>>> : std::true_type {};

		template<class Policy, class Algorithm, class SFINAE = void> struct isStepSizeFinder: std::false_type{};
		template<class Policy, class Algorithm>
		struct isStepSizeFinder<Policy, Algorithm, std::enable_if_t<is_callable_v<Policy, real(const Algorithm&)>>> : std::true_type {};

		template<class Policy, class Algorithm, class SFINAE = void> struct isInnerProductPolicy: std::false_type{};
		template<class Policy, class Algorithm>
		struct isInnerProductPolicy<Policy, Algorithm, std::enable_if_t<is_callable_v<Policy, real(const decltype(std::declval<Algorithm>().stepDirection())&, const decltype(std::declval<Algorithm>().stepDirection())&)>>> : std::true_type {};

		template<class Policy, class Algorithm, class SFINAE = void> struct isGradientPolicy: std::false_type{};
		template<class Policy, class Algorithm>
		struct isGradientPolicy<Policy, Algorithm, std::enable_if_t<is_callable_v<Policy, std17::remove_cvref_t<decltype(std::declval<Algorithm>().stepDirection())>(const std17::remove_cvref_t<decltype(std::declval<Algorithm>().problem())>&)>>> : std::true_type {};

		template<class Policy, class Algorithm, class SFINAE = void> struct isBfgsRestartPolicy: std::false_type{};
		template<class Policy, class Algorithm>
		struct isBfgsRestartPolicy<Policy, Algorithm, std::enable_if_t<is_callable_v<Policy, bool(const Algorithm&)>>> : std::true_type {};

		template<class Policy, class Algorithm, class SFINAE = void> struct isDressedResetPolicy: std::false_type{};
		template<class Policy, class Algorithm>
		struct isDressedResetPolicy<Policy, Algorithm, std::enable_if_t<is_callable_v<Policy, void(Algorithm&)>>> : std::true_type {};
	}
}

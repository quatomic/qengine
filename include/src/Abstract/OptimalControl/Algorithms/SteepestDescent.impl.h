﻿/* COPYRIGHT
 *
 * file="SteepestDescent.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/OptimalControl/Algorithms/SteepestDescent.h"
#include "src/Abstract/OptimalControl/Algorithms/PolicyRequirementsBFGSandSteepest.h"

#include "src/Utility/is_callable.h"
#include "src/NumberTypes.h"

namespace qengine
{
	template <class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy>
	std::tuple<real, Params, Collector>
		SteepestDescent<Params, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy>::
		optimize()
	{
		auto& problem = _problem;
		// declare aliases for hooks so they look like function-calls in the code below
		auto& shouldStop = _stopper;
		auto& collect = _collector;
		auto& calculateStepSize = _stepSizeFinder;
		auto& calculateGradient = _gradientPolicy;

		auto x = problem._params();
		while (true)
		{
			auto grad = calculateGradient(problem);

			_stepDirection = -grad;

			// stepSize-algorithm is allowed to modify the problem!
			_stepSize = calculateStepSize(*this);

			x += _stepSize*_stepDirection;
			problem.update(x);

			++_iteration;

			// We only check and collect AFTER each iteration, so not on the zero'th iteration.
			collect(*this);
			if (shouldStop(*this)) break;
		}

		return std::make_tuple(problem.cost(), problem._params(), _collector);
	}
	
	template <class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy>
	SteepestDescent<Params, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy>::SteepestDescent(const Problem& problem, const Stopper& stopper, const Collector& collector, const StepSizeFinder& stepSizeFinder, const InnerProductPolicy& innerProductPolicy, const GradientPolicy& gradientPolicy) :
		_problem(problem),
		_stopper(stopper),
		_collector(collector),
		_stepSizeFinder(stepSizeFinder),
		_innerProductPolicy(innerProductPolicy),
		_gradientPolicy(gradientPolicy)
	{
		using Alg = SteepestDescent<Params, Problem, Stopper, Collector, StepSizeFinder, InnerProductPolicy, GradientPolicy>;

		static_assert(internal::is_GradientOptimizationProblem<Problem, Params>::value, "");
		static_assert(internal::isStopper<Stopper, Alg>::value, "");
		static_assert(internal::isCollector<Collector, Alg>::value, "");
		static_assert(internal::isStepSizeFinder<StepSizeFinder, Alg>::value, "");
		static_assert(internal::isInnerProductPolicy<InnerProductPolicy, Alg>::value, "");
		static_assert(internal::isGradientPolicy <GradientPolicy, Alg>::value, "");
	}
}

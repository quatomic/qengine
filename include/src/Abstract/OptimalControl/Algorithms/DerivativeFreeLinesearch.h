/* COPYRIGHT
*
* file="DerivativeFreeLinesearch.h"
* Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
*
* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a
* copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/
#pragma once
#include <cmath>
#include <stdexcept>
#include <algorithm>
#include <iostream>

namespace qengine
{
namespace experimental
{

	class DerivativeFreeLinesearch
	{
		///
		/// Implementation of a simple backtracking approach to linesearch
		/// Based on 'A Linesearch-Based Derivative-Free Approach for Nonsmooth Constrained Optimization' G. Fasano, G. Liuzzi, S. Lucidi, and F. Rinaldi (2014)
		///

		struct DFParameters
		{
			const double _conditionStrength; // gamma
			const double _shrinkFactor; // delta
			const double _minimumStepSize;

			DFParameters(const double conditionStrength, const double shrinkFactor, const double lim = 0.0) : _conditionStrength(conditionStrength), _shrinkFactor(shrinkFactor), _minimumStepSize(lim)
			{
				if (!(_shrinkFactor > 0 && _conditionStrength && _shrinkFactor < 1 && lim >= 0))
				{
					throw std::runtime_error("illegal parameters in DerivativeFreeLinesearch");
				}
			}

			virtual ~DFParameters() = default;
		};

	public:

		explicit DerivativeFreeLinesearch(const double conditionStrength, const double shrinkFactor, const double minimumStepSize = 0.0) : mAlgParams(conditionStrength, shrinkFactor, minimumStepSize) { }

		template <typename Function>
		double operator() (const Function & func, const double alphaMax, const double f0) const
		{
			auto alpha = alphaMax;
			if (DFCondition(func, alpha, f0))
			{
				return alpha;
			}

			while (alpha > mAlgParams._minimumStepSize)
			{
				// alpha <- alpha * 1/delta
				alpha *= mAlgParams._shrinkFactor;
				if (DFCondition(func, alpha, f0))
				{
					break;
				}
			}
			return alpha;
		}

		virtual ~DerivativeFreeLinesearch() = default;

	private:

		template <typename Function>
		bool DFCondition(Function & func, const double stepSize, const double f0) const
		{
			// f(alpha) <= f(0) - gamma * alpha^2
			auto costStepVal = func(stepSize);
			return costStepVal <= f0 - mAlgParams._conditionStrength * stepSize * stepSize;
		}

		const DFParameters mAlgParams;
	};
}
}
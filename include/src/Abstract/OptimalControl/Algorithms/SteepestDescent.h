﻿/* COPYRIGHT
 *
 * file="SteepestDescent.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>
#include "src/API/Common/OptimalControl/LinesearchHeuristics/QuickStep.fwd.h"

namespace qengine
{
	template<class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy>
	class SteepestDescent
	{
		/// HOOKABLE PROPERTIES
	public:
		Problem & problem() { return _problem; }
		Collector& collector() { return _collector; }
		Stopper& stopper() { return _stopper; }
		StepSizeFinder& stepSizeFinder() { return _stepSizeFinder; }
		InnerProductPolicy& innerProductPolicy() { return _innerProductPolicy; }
		GradientPolicy& gradientPolicy() { return _gradientPolicy; }

		const Problem& problem() const { return _problem; }
		const Collector& collector() const { return _collector; }
		const Stopper& stopper()  const { return _stopper; }
		const StepSizeFinder& stepSizeFinder()  const { return _stepSizeFinder; }
		const InnerProductPolicy& innerProductPolicy()  const { return _innerProductPolicy; }
		const GradientPolicy& gradientPolicy()  const { return _gradientPolicy; }

		const auto& stepSize() const { return _stepSize; }
		const auto& stepDirection() const { return _stepDirection; }
		count_t iteration() const { return _iteration; }

		/// CORE FUNCTIONALITY
	public:
		std::tuple<real, Params, Collector> optimize();

		/// CTOR
	public:
		SteepestDescent(const Problem& problem, const Stopper& stopper, const Collector& collector, const StepSizeFinder& stepSizeFinder, const InnerProductPolicy& innerProductPolicy, const GradientPolicy& gradientPolicy);

	private:
		mutable Problem _problem;
		Stopper _stopper;
		Collector _collector;
		StepSizeFinder _stepSizeFinder;
		InnerProductPolicy _innerProductPolicy;
		GradientPolicy _gradientPolicy;

		// some variables that are required so hooks can access them.
		mutable Params _stepDirection;
		real _stepSize = 0.0;
		count_t _iteration = 0;

		template <typename T, typename L>
		friend class experimental::QuickStep;
	};
}
﻿/* COPYRIGHT
 *
 * file="Bfgs.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>

namespace qengine
{

	// BFGS implementation follows
	// "Computational techniques for a quantum control problem with H1-cost", G von Winckel and A Borzi, 2008
	// doi:10.1088/0266-5611/24/3/034007

	template<class Params, class Problem, class Stopper, class Collector, class StepSizeFinder, class InnerProductPolicy, class GradientPolicy, class RestartPolicy>
	class Bfgs
	{
		/// HOOKABLE PROPERTIES
	public:
		Problem & problem() { return _problem; }
		Collector& collector() { return _collector; }
		Stopper& stopper() { return _stopper; }
		StepSizeFinder& stepSizeFinder() { return _stepSizeFinder; }
		InnerProductPolicy& innerProductPolicy() { return _innerProductPolicy; }
		GradientPolicy& gradientPolicy() { return _gradientPolicy; }
		RestartPolicy& restartPolicy() { return _restartPolicy; }

		const Problem& problem() const { return _problem; }
		const Collector& collector()  const { return _collector; }
		const Stopper& stopper()  const { return _stopper; }
		const StepSizeFinder& stepSizeFinder()  const { return _stepSizeFinder; }
		const InnerProductPolicy& innerProductPolicy()  const { return _innerProductPolicy; }
		const GradientPolicy& gradientPolicy()  const { return _gradientPolicy; }
		const RestartPolicy& restartPolicy()  const { return _restartPolicy; }

		auto stepSize() const { return _stepSize; }
		auto & stepDirection() { return _stepDirection;  }
		const auto& stepDirection() const { return _stepDirection; }
		auto iteration() const { return _iteration; }
		auto stepTimesY() const { return _q; }
		auto didRestart() const { return _restart; }

		count_t stepsSinceLastRestart() const;

		/// CORE FUNCTIONALITY
	public:
		std::tuple<real, Params, Collector> optimize();
		void resetBfgs();

		/// CTOR
	public:
		Bfgs(const Problem& problem, const Stopper& stopper, const Collector& collector, const StepSizeFinder& stepSizeFinder, const InnerProductPolicy& innerProductPolicy, const GradientPolicy& gradientPolicy, const RestartPolicy& restartPolicy);

	private:

		mutable Problem _problem;
		Stopper _stopper;
		Collector _collector;
		StepSizeFinder _stepSizeFinder;
		InnerProductPolicy _innerProductPolicy;
		GradientPolicy _gradientPolicy;
		RestartPolicy _restartPolicy;

		// some variables that are required so hooks can access them.
		mutable Params _stepDirection;
		real _stepSize = 0.0;
		count_t _iteration = 0;
		real _q = 0.0; // this is needed for stoppers
		bool _restart = false;

		// Stuff to make the algorithm work
	private:

        struct PreviousIteration {
			double c;
			Params step;
            Params r; // d*sj-zj
			Params z;
		};

		std::vector<PreviousIteration> _previousIterations;
        void saveIteration(const Params& step, const Params& y, double q);

		template <typename T, typename L>
		friend class experimental::QuickStep;
	};
}

﻿/* COPYRIGHT
 *
 * file="Boundaries.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/OptimalControl/Problems/Boundaries.h"

#include <algorithm>
#include <iostream>

namespace
{
	qengine::RVec maxVec(qengine::RVec v)
	{
		for (auto i = 0u; i < v.size(); ++i)
		{
			v.at(i) = std::max(v.at(i), 0.0);
		}
		return v;
	}
}

namespace qengine
{
	Boundaries::Boundaries(Control control, const RVec lowerleft, const RVec upperright, const real factor) :
		control_(std::move(control)),
		lowerleft_(lowerleft),
		upperright_(upperright),
		factor_(factor)
	{
	}

	void Boundaries::update(const Control& control)
	{
		control_ = control;
	}

	void Boundaries::_update(const count_t index, const RVec& x)
	{
		control_.set(index, x);
	}

	real Boundaries::cost() const
	{
		auto retval = 0.0;
		for (auto i = 0u; i < control_.size(); ++i)
		{
			retval += _cost(i);
		}
		return retval;
	}

	real Boundaries::_cost(const count_t i) const
	{
		const auto no = (maxVec(control_.at(i) - upperright_) + maxVec(lowerleft_ - control_.at(i))).norm();

		return 0.5*factor_ * no*no;
	}

	Control Boundaries::gradient() const
	{
		auto retval = Control::zeros(control_.size(), control_.paramCount(), control_.dt());

		for (auto i = 1u; i < control_.size() - 1; ++i)
		{
			retval.set(i, _gradient(i));
		}
		return retval;
	}

	RVec Boundaries::_gradient(const count_t i) const
	{
		return factor_* (-maxVec(lowerleft_ - control_.at(i)) + maxVec(control_.at(i) - upperright_));
	}

	real& Boundaries::factor()
	{
		return factor_;
	}

	const real& Boundaries::factor() const
	{
		return factor_;
	}

	Boundaries& Boundaries::operator*=(const real factor)
	{
		factor_ *= factor;
		return *this;
	}

	Boundaries operator*(const real factor, Boundaries reg)
	{
		return reg *= factor;
	}
}

﻿/* COPYRIGHT
 *
 * file="BasisProblem.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/OptimalControl/Parameters/BasisControl.h"
#include "src/Abstract/OptimalControl/Parameters/Control.h"

#include "src/Abstract/OptimalControl/Problems/ProblemTraits.h"
#include "src/Abstract/OptimalControl/Parameters/Basis.h"

namespace qengine
{
	namespace optimal_control
	{
		template<class Problem>
		class BasisProblem : public Problem
		{
			static_assert(internal::is_QOCProblem_v<Problem>, "");
		public:

			BasisProblem(const Problem& problem, const Basis& basis, const Control& baseControl)
				: Problem(problem),
				cs_(BasisControl::zeros(basis.size(), baseControl.paramCount())),
				us_(basis),
				u0_(baseControl)
			{
			}

			void update(const BasisControl& basisControl)
			{
				Problem::update(constructControl(basisControl));
				cs_ = basisControl;
			}

			BasisControl gradient() const
			{
				auto grapeGradient = Problem::gradient();
				auto grad = BasisControl::zeros(cs_.size(), cs_.paramCount());
				for (auto i = 0u; i < cs_.size(); ++i)
				{
					grad.at(i) = dotsL2(us_.at(i), grapeGradient);
				}

				return grad;
			}

			BasisControl coefficients() const { return cs_; }

			const Control& baseControl() const { return u0_; }
			const Basis& basis() const { return us_; }

			/// INTERNAL USE ONLY
		public:
			BasisControl _params() const { return cs_; }
			void _resetBasis(const Basis& us)
			{
				u0_ = Problem::control();
				us_ = us;

				cs_ = BasisControl::zeros(us.size(), u0_.paramCount());
			}


		private:
			BasisControl cs_;

			Basis us_;
			Control u0_;

			Control constructControl(const BasisControl& coefficients) const
			{
				auto control = u0_;
				for (auto i = 0u; i < coefficients.size(); ++i)
				{
					control += coefficients.at(i)*us_.at(i);
				}
				return control;
			}
		};

		template<class Problem>
		auto makeBasisProblem(const Problem& problem, const Basis& basis, const Control& baseControl)
		{
			return BasisProblem<Problem>(problem, basis, baseControl);
		}

	}
}

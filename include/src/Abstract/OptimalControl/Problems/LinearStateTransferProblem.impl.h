﻿/* COPYRIGHT
 *
 * file="LinearStateTransferProblem.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/qengine_assert.h"

#include "src/Abstract/OptimalControl/Problems/LinearStateTransferProblem.h"

#include "src/Abstract/Physics/Spatial/Basics.h"

namespace qengine
{
	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::LinearStateTransferProblem(const DHdu& dV, const State&, const DynamicStateForward& psi, const DynamicStateBackward& chi, const Control& initialControl) :
		psi_(psi),
		chi_(chi),
		fidelity_(0),
		isFidelityUpdated_(false),
		control_(initialControl),
		initialControl_(initialControl),
		dHdu_(dV),
		lastUpdatedIndex_(initialControl.size() - 1),
		isGradUpdated_(false)
	{
		qengine_assert(initialControl.size() >= 3, "control must have at least size 3");
		psi_.retargetControl(control_);
		chi_.retargetControl(control_);
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::LinearStateTransferProblem(const LinearStateTransferProblem& other) :
		psi_{ other.psi_ },
		chi_{ other.chi_ },
		fidelity_{ other.fidelity_ },
		isFidelityUpdated_{ other.isFidelityUpdated_ },
		control_{ other.control_ },
		initialControl_{ other.initialControl_ },
		dHdu_{ other.dHdu_ },
		lastUpdatedIndex_{ other.lastUpdatedIndex_ },
		isGradUpdated_{ other.isGradUpdated_ },
		grad_{ other.grad_ }
	{
		psi_.retargetControl(control_);
		chi_.retargetControl(control_);
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::LinearStateTransferProblem(LinearStateTransferProblem&& other) noexcept :
		psi_{ std::move(other.psi_) },
		chi_{ std::move(other.chi_) },
		fidelity_{ other.fidelity_ },
		isFidelityUpdated_{ other.isFidelityUpdated_ },
		control_{ std::move(other.control_) },
		initialControl_{ std::move(other.initialControl_) },
		dHdu_{ std::move(other.dHdu_) },
		lastUpdatedIndex_{ other.lastUpdatedIndex_ },
		isGradUpdated_{ other.isGradUpdated_ },
		grad_{ std::move(other.grad_) }
	{
		psi_.retargetControl(control_);
		chi_.retargetControl(control_);
	}


	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	void LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::update(const Control& control)
	{
		qengine_assert(control.size() >= 3, "control must have at least size 3");
		control_ = control;
		psi_.notifyOfChange();
		chi_.notifyOfChange();

		isFidelityUpdated_ = false;
		isGradUpdated_ = false;
		lastUpdatedIndex_ = control.size() - 1;
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	void LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::_update(count_t index, const RVec& x)
	{
		qengine_assert(x.size() == control_.paramCount(), "updated control-point must have length paramCount");

		control_.set(index, x);
		psi_.notifyOfChange(index);
		chi_.notifyOfChange(index);

		isFidelityUpdated_ = false;
		isGradUpdated_ = false;

		lastUpdatedIndex_ = index;
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	real LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::cost() const
	{
		return 0.5 * (1.0 - fidelity());
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	Control LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::gradient() const
	{
		if (!isGradUpdated_)
		{
			grad_ = Control::zeros(control_.size(), control_.paramCount(), control_.dt());

			auto overlapFactor = overlap(chi_(control_.size() - 1), psi_(control_.size() - 1));

			for (auto i = control_.size() - 2; i >= 1; --i)
			{
				for (auto j = 0u; j < control_.paramCount(); ++j)
				{
					auto dV = dHdu_.at(j)(control_.get(i));
					grad_.at(i).at(j) = control_.dt() * std::imag(overlapFactor*overlap(psi_(i), dV * chi_(i)));
				}
			}
			isGradUpdated_ = true;
		}
		return grad_;
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	RVec LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::_gradient(count_t i) const
	{
		auto grad = RVec(control_.paramCount());

		for (auto j = 1u; j < control_.paramCount(); ++j)
		{
			auto dH = dHdu_.at(j)(control_.get(i));
			grad.at(j) = control_.dt() * std::imag(overlap(chi_(i), psi_(i)) * overlap(psi_(i), dH * chi_(i)));
		}

		return grad;
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	const Control& LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::initialControl() const
	{
		return initialControl_;
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	Control LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::control() const
	{
		return control_;
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	const std::vector<State>& LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::psis() const
	{
		psi_.forceFullUpdate();
		return psi_.cache();
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	Control LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::_params() const
	{
		return control_;
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	double LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::dt() const
	{
		return control().dt();
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	RVec LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::t() const
	{
		const auto n = control().size();
		return qengine::linspace(0, (n - 1)*dt(), n);
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	real LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::fidelity() const
	{
		if (!isFidelityUpdated_)
		{
			fidelity_ = qengine::fidelity(psi_(lastUpdatedIndex_), chi_(lastUpdatedIndex_));
			isFidelityUpdated_ = true;
		}
		return fidelity_;
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	count_t LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::nPropagationSteps() const
	{
		return (psi_.nPropagations() + chi_.nPropagations());
	}

	template <class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	void LinearStateTransferProblem<DHdu, State, DynamicStateForward, DynamicStateBackward>::resetPropagationCount()
	{
		psi_.resetPropagationCount();
		chi_.resetPropagationCount();
	}
}

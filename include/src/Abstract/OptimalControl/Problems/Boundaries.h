﻿/* COPYRIGHT
 *
 * file="Boundaries.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/OptimalControl/Parameters/Control.h"

namespace qengine
{
	class Boundaries
	{
	public:
		explicit Boundaries(Control control, RVec lowerleft, RVec upperright, real factor = 1.0);

		void update(const Control& control);

		real cost() const;

		Control gradient() const;

		real& factor();
		const real& factor() const;

		Control control() const { return control_; }

		Boundaries& operator*=(real factor);

		/// INTERNAL USE ONLY
	public:
		Control _params() const { return control(); }
		void _update(count_t index, const RVec& x);
		real _cost(count_t i) const;
		RVec _gradient(count_t i) const;

	private:
		Control control_;
		RVec lowerleft_;
		RVec upperright_;
		real factor_;
	};

	Boundaries operator*(real factor, Boundaries reg);


	inline Boundaries makeRegularization(Control control, const RVec lowerLeft,const RVec upperRight, const real factor = 1.0)
	{
		return Boundaries(std::move(control), lowerLeft, upperRight, factor);
	}
}

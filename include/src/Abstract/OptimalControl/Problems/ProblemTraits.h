﻿/* COPYRIGHT
 *
 * file="ProblemTraits.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Utility/has_function.h"
#include "src/Abstract/OptimalControl/Parameters/Control.h"
#include "src/Abstract/OptimalControl/Parameters/BasisControl.h"

namespace qengine
{
	class Regularization;
	class Boundaries;

	namespace internal
	{
		template<class T> struct is_helper_problem { constexpr static bool value = false; };
		template<class T> constexpr bool is_helper_problem_v = is_helper_problem<T>::value;

		template<> struct is_helper_problem<Regularization> { constexpr static bool value = true; };
		template<> struct is_helper_problem<Boundaries> { constexpr static bool value = true; };
	}
}


namespace qengine
{
	namespace internal
	{
		namespace OC
		{
			MAKE_HAS_FUNCTION(has_update, update);
			MAKE_HAS_FUNCTION(has_cost, cost);
			MAKE_HAS_FUNCTION(has_gradient, gradient);

			MAKE_HAS_FUNCTION(has_control, control);
			MAKE_HAS_FUNCTION(has_params, _params);

			MAKE_HAS_FUNCTION(has_coefficients, coefficients);
		}
	}

    namespace internal
    {
        template<class Problem, class Params>
        struct is_OptimizationProblem
        {
            static constexpr bool value =
                OC::has_update_v<Problem, void(const Params&)> &&
                OC::has_cost_v<Problem,real()> &&
                OC::has_params_v<Problem,Params()>;
        };

        template<class Problem, class Params> static constexpr bool is_OptimizationProblem_v = is_OptimizationProblem<Problem,Params>::value;
    }

	namespace internal
	{
		template<class Problem, class Params>
        struct is_GradientOptimizationProblem
		{
			static constexpr bool value =
                is_OptimizationProblem_v<Problem,Params> &&
				OC::has_params_v<Problem, Params()>;
		};

        template<class Problem, class Params> static constexpr bool is_GradientOptimizationProblem_v = is_GradientOptimizationProblem<Problem, Params>::value;
	}

	namespace internal
	{
		template<class Problem>
		struct is_QOCProblem // QOC stands for Quantum Optimal Control
		{
			static constexpr bool value =
                is_GradientOptimizationProblem_v<Problem, Control> &&
				OC::has_control_v<Problem, Control()>;
		};

		template<class Problem> static constexpr bool is_QOCProblem_v = is_QOCProblem<Problem>::value;
	}

	namespace internal
	{
		template<class Problem>
		struct is_ParametrizedQOCProblem // QOC stands for Quantum Optimal Control
		{
			static constexpr bool value =
                is_GradientOptimizationProblem_v<Problem, BasisControl> &&
				OC::has_coefficients_v<Problem, BasisControl()> &&
				OC::has_control_v<Problem, Control()>;
		};

		template<class Problem> static constexpr bool is_ParametrizedQOCProblem_v = is_ParametrizedQOCProblem<Problem>::value;
	}
}

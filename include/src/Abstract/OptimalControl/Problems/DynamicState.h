﻿/* COPYRIGHT
 *
 * file="DynamicState.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <type_traits>

#include "src/NumberTypes.h"

#include "src/Abstract/OptimalControl/Parameters/Control.h"

namespace qengine
{
	namespace optimal_control
	{
		template<class State, class Stepper, bool forward>
		class DynamicState
		{
		public:
			DynamicState(const Stepper& stepper, const State& initialState, std::reference_wrapper<const Control> control);

			void notifyOfChange();
			void notifyOfChange(count_t index);

			void retargetControl(const Control& control);

			void setInitialState(const State& state);

			const State& operator()(count_t i) const;

			count_t nPropagations() const;
			void resetPropagationCount();

			double dt() const;

            const std::vector<State>& cache() const;

			void forceFullUpdate();

		private:
			mutable std::vector<State> cache_;
			mutable Stepper stepper_;
			State initialState_;
			std::reference_wrapper<const Control> control_;
			mutable count_t validIndex_ = 0;
			mutable count_t nPropagations_ = 0;

			void update(count_t index) const;

			count_t index(count_t index) const;
		};
	}
}

namespace qengine
{
	namespace optimal_control
	{
		template<bool forward, class Stepper, class State>
		auto makeDynamicState(const Stepper& stepper, const State& initialState, std::reference_wrapper<const Control> control)
		{
			return DynamicState<State, Stepper, forward>(stepper, initialState, control);
		}
	}
}

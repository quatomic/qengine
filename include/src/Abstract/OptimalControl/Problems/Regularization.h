﻿/* COPYRIGHT
 *
 * file="Regularization.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/OptimalControl/Parameters/Control.h"

namespace qengine
{
	class Regularization
	{
	public:
		explicit Regularization(Control control, real factor = 1.0);

		void update(const Control& control);

		real cost() const;

		Control gradient() const;

		real& factor();
		const real& factor() const;

		Control control() const { return control_; }

		Regularization& operator*=(real factor);

		/// INTERNAL USE ONLY
	public:
		auto _params() const { return control_; }
		void _update(count_t index, const RVec& x);
		real _cost(count_t i) const;
		RVec _gradient(count_t i) const;

	private:
		Control control_;
		real factor_;
	};

	Regularization operator*(real factor, Regularization reg);

	inline Regularization makeRegularization(Control control, const real factor = 1.0)
	{
		return Regularization(std::move(control), factor);
	}
}

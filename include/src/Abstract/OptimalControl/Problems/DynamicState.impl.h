﻿/* COPYRIGHT
 *
 * file="DynamicState.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/OptimalControl/Problems/DynamicState.h"

#include "src/Utility/cpp17Replacements/constexpr_ternary.h"

namespace qengine
{
	namespace optimal_control 
	{
		template<class State, class Stepper, bool forward>
		DynamicState<State, Stepper, forward>::DynamicState(const Stepper& stepper, const State& initialState, std::reference_wrapper<const Control> control) :
			cache_(control.get().size(), initialState),
			stepper_(stepper),
			initialState_(initialState),
			control_(control)
		{
			cache_.front() = initialState_;
		}

		template<class State, class Stepper, bool forward>
		void DynamicState<State, Stepper, forward>::notifyOfChange()
		{
			validIndex_ = 0;
			stepper_.reset(cache_.front(), control_.get().get(index(0)));
		}

		template<class State, class Stepper, bool forward>
		void DynamicState<State, Stepper, forward>::notifyOfChange(count_t j)
		{
			auto i = index(j);
			if (i == 0)
			{
				validIndex_ = 0;
			}
			else
			{
				validIndex_ = i - 1;
			}
			stepper_.reset(cache_.at(validIndex_), control_.get().get(index(i)));
		}

		template<class State, class Stepper, bool forward>
		void DynamicState<State, Stepper, forward>::retargetControl(const Control& control)
		{
			control_ = control;
			notifyOfChange();
		}

		template <class State, class Stepper, bool forward>
		void DynamicState<State, Stepper, forward>::setInitialState(const State& state)
		{
			initialState_ = state;
			cache_.front() = initialState_;
			validIndex_ = 0;
			stepper_.reset(cache_.at(validIndex_), control_.get().get(index(0)));
		}

		template<class State, class Stepper, bool forward>
		const State& DynamicState<State, Stepper, forward>::operator()(count_t i) const
		{
			update(index(i));
			return cache_.at(index(i));
		}

		template<class State, class Stepper, bool forward>
		count_t DynamicState<State, Stepper, forward>::nPropagations() const
		{
			return nPropagations_;
		}

		template<class State, class Stepper, bool forward>
		void DynamicState<State, Stepper, forward>::resetPropagationCount()
		{
			nPropagations_ = 0;
		}

		template<class State, class Stepper, bool forward>
		double DynamicState<State, Stepper, forward>::dt() const
		{
			return stepper_.dt();
		}

        template<class State, class Stepper, bool forward>
        const std::vector<State>& DynamicState<State, Stepper, forward>::cache() const
        {
            return cache_;
        }

		template <class State, class Stepper, bool forward>
		void DynamicState<State, Stepper, forward>::forceFullUpdate()
		{
			update(cache_.size() - 1);
		}

		template<class State, class Stepper, bool forward>
		void DynamicState<State, Stepper, forward>::update(count_t j) const
		{
			for (auto i = validIndex_; i < j; ++i)
			{
				//assert(stepper_.previous() == std::make_tuple(control_.get().get(index(i))));

				stepper_.step(control_.get().get(index(i + 1)));
				cache_.at(i + 1) = stepper_.state();
				++nPropagations_;
			}
			if (j > validIndex_) validIndex_ = j;
		}

		template <class State, class Stepper, bool forward>
		count_t DynamicState<State, Stepper, forward>::index(count_t index) const
		{
			return repl17::constexpr_ternary(std::integral_constant<bool, forward>(),
				[&](auto _) {return _(index); },
				[&](auto _) {return _(cache_.size() - 1 - index); }
			);
		}

		///// index takes care of the forward/backward logic: any input index is "turned around" if
		///// we are going backward. This simplifies implementation. 
		//template<class State, class Stepper, bool forward>
		//template <bool forward2, std::enable_if_t<forward2, int>>
		//count_t DynamicState<State, Stepper, forward>::index(count_t i) const
		//{
		//	return i;
		//}

		//template<class State, class Stepper, bool forward>
		//template <bool forward2, std::enable_if_t<!forward2, int>>
		//count_t DynamicState<State, Stepper, forward>::index(count_t i) const
		//{
		//	return cache_.size() - 1 - i;
		//}
	}
}

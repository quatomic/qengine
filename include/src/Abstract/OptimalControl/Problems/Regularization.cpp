﻿/* COPYRIGHT
 *
 * file="Regularization.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/OptimalControl/Problems/Regularization.h"

namespace qengine
{
	Regularization::Regularization(Control control, const real factor) :
		control_(std::move(control)),
		factor_(factor)
	{
	}

	void Regularization::update(const Control& control)
	{
		 control_ = control;
	}

	void Regularization::_update(const count_t index, const RVec& x)
	{
		control_.set(index, x);
	}

	real Regularization::cost() const
	{
		return 0.5*factor_*dotH1(control_, control_);
	}

	real Regularization::_cost(const count_t i) const
	{
		auto theNorm = 0.0;
		if (i == 0u)
		{
			/// forward/backward differences for first and last point in control (best possible approximation)
			theNorm = (control_.get(1) - control_.get(0)).norm();
		}
		else if (i == control_.size() - 1)
		{
			theNorm = (control_.get(control_.size() - 1) - control_.get(control_.size() - 2)).norm();
		}
		else
		{
			/// central difference for all other points (more accurate)
			theNorm = (0.5*(control_.get(i + 1) - control_.get(i - 1))).norm();
		}

		return factor_ * theNorm*theNorm;
	}

	Control Regularization::gradient() const
	{
		auto retval = Control::zeros(control_.size(), control_.paramCount(), control_.dt());
		/// currently excludes endpoints. commented out below is what should be done to include endpoints
		const auto a = factor_ / control_.dt();
		for (auto i = 1ull; i < control_.size() - 1; ++i)
		{
			retval.set(i, a*(2 * control_.get(i) - control_.get(i - 1) - control_.get(i + 1)));
		}
		//for (auto i = 0u; i < control_.size(); ++i)
		//{
		//	if (i < control_.size()-1) 
		//	{
		//		retval.at(i) -= 2 * factor_*(control_.at(i + 1) - control_.at(i));
		//	}
		//	if (i > 0u) 
		//	{
		//		retval.at(i) -= 2 * factor_*(control_.at(i - 1) - control_.at(i));
		//	}
		//}

		return retval;
	}

	RVec Regularization::_gradient(count_t i) const
	{
		auto retval = RVec(control_.paramCount(), 0.0);
		if (i == 0u)
		{
			/// return 0-gradient for i = start, for consistency.
		}
		else if (i == control_.size() - 1)
		{
			/// return 0-gradient for i = start, for consistency.
		}
		else
		{
			/// central difference for all other points 
			retval -= (factor_)*(control_.get(i + 1) - control_.get(i));
			retval -= (factor_)*(control_.get(i - 1) - control_.get(i));
		}

		return retval;
	}

	real& Regularization::factor()
	{
		return factor_;
	}

	const real& Regularization::factor() const
	{
		return factor_;
	}

	Regularization& Regularization::operator*=(const real factor)
	{
		factor_ *= factor;
		return *this;
	}

	Regularization operator*(const real factor, Regularization reg)
	{
		return reg *= factor;
	}
}

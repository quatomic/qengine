﻿/* COPYRIGHT
 *
 * file="LinearStateTransferProblem.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>

#include "src/NumberTypes.h"

#include "src/Abstract/Physics/General/TimeStepper.h"

#include "src/Utility/forceCopy.h"

#include "src/Abstract/OptimalControl/Problems/DynamicState.impl.h"
#include "src/Abstract/OptimalControl/Parameters/Control.h"

namespace qengine
{
	template<class DHdu, class State, class DynamicStateForward, class DynamicStateBackward>
	class LinearStateTransferProblem
	{
	public:
		LinearStateTransferProblem(const DHdu& dV, const State& targetState, const DynamicStateForward& psi, const DynamicStateBackward& chi, const Control& initialControl);

		LinearStateTransferProblem(const LinearStateTransferProblem& other);

		LinearStateTransferProblem(LinearStateTransferProblem&& other) noexcept;

		/// 
	public:
		void update(const Control& control);

		real cost() const;
		Control gradient() const;
		Control control() const;
		real fidelity() const;

		count_t nPropagationSteps() const;
		void resetPropagationCount();

		const Control& initialControl() const;
		const Control& u0() const { return initialControl(); }
		double dt() const;
		RVec t() const;
		DHdu dHdu() const { return dHdu_; }

		const std::vector<State>& psis() const;


		/// INTERNAL ONLY
	public:
		Control _params() const;
		void _update(count_t index, const RVec& x);
		RVec _gradient(count_t index) const;
        const DynamicStateForward& _dynamicStateForward() const {return psi_;}

	private:
		mutable DynamicStateForward psi_;

		mutable DynamicStateBackward chi_;

		mutable real fidelity_;
		mutable bool isFidelityUpdated_;

		Control control_;
		const Control initialControl_;
		const DHdu dHdu_;
		mutable count_t lastUpdatedIndex_; // traces which part of the control has last been changed by update. Update(Control) sets it to last point

		mutable bool isGradUpdated_;
		mutable Control grad_;
	};

	template<class TimeStepAlgorithm, class BackwardsTimeStepAlgorithm, class DHdu, class State>
	auto makeLinearStateTransferProblem(const TimeStepAlgorithm& alg, const BackwardsTimeStepAlgorithm& algb, const DHdu& dV, const State& initialState, const State& targetState, const Control& x0)
	{
		static_assert(std::is_same < State, decltype(dV.at(0)(RVec{})* initialState) > ::value, "");

		auto forwardStepper = qengine::makeFixedTimeStepper(internal::forceCopy(alg), toComplex(initialState), makeTuple(x0.getFront()));
		auto backwardStepper = qengine::makeFixedTimeStepper(internal::forceCopy(algb), toComplex(targetState), makeTuple(x0.getBack()));

		auto psi = optimal_control::makeDynamicState<true>(forwardStepper, toComplex(initialState), x0);
		auto chi = optimal_control::makeDynamicState<false>(backwardStepper, toComplex(targetState), x0);

		return LinearStateTransferProblem<DHdu, decltype(toComplex(initialState)), decltype(psi), decltype(chi)>(dV, targetState, psi, chi, x0);
	}
}


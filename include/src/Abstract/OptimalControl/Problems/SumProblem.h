﻿/* COPYRIGHT
 *
 * file="SumProblem.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/OptimalControl/Parameters/Control.h"
#include "src/Abstract/OptimalControl/Problems/ProblemTraits.h"

namespace qengine
{
	template<class PhysicsProblem, class OtherProblem>
	class SumProblem : public PhysicsProblem, private OtherProblem
	{
	public:
		SumProblem(PhysicsProblem&& physicsProblem, OtherProblem&& otherProblem);

		void update(const Control& control);

		real cost() const;

		Control gradient() const;

		Control control() const { return PhysicsProblem::control(); }


		/// INTERNAL USE ONLU
	public:
		Control _params() const { return PhysicsProblem::_params(); }

		PhysicsProblem& _left() { return *this; }
		const PhysicsProblem& _left()const { return *this; }

		OtherProblem& _right() { return *this; }
		const OtherProblem& _right()const { return *this; }

		void _update(count_t index, const RVec& x);
		real _cost(count_t i) const;
		RVec _gradient(count_t index) const;
	};

	template<class PhysicsProblem, class OtherProblem, typename = std::enable_if_t<internal::is_helper_problem_v<OtherProblem> && internal::is_QOCProblem_v<PhysicsProblem>>>
	SumProblem<PhysicsProblem, OtherProblem> operator+(PhysicsProblem&& physicsProblem, OtherProblem&& otherProblem);
}

﻿/* COPYRIGHT
 *
 * file="SumProblem.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/OptimalControl/Problems/SumProblem.h"

namespace qengine
{
		template <class PhysicsProblem, class OtherProblem>
		SumProblem<PhysicsProblem, OtherProblem>::SumProblem(PhysicsProblem&& physicsProblem, OtherProblem&& otherProblem) :
			PhysicsProblem(std::move( physicsProblem)),
			OtherProblem(std::move(otherProblem))
		{
		}

		template <class PhysicsProblem, class OtherProblem>
		void SumProblem<PhysicsProblem, OtherProblem>::update(const Control& control)
		{
			PhysicsProblem::update(control);
			OtherProblem::update(control);
		}

		template <class PhysicsProblem, class OtherProblem>
		void SumProblem<PhysicsProblem, OtherProblem>::_update(count_t index, const RVec& x)
		{
			PhysicsProblem::update(index, x);
			OtherProblem::update(index, x);
		}

		template <class PhysicsProblem, class OtherProblem>
		real SumProblem<PhysicsProblem, OtherProblem>::cost() const
		{
			return PhysicsProblem::cost() + OtherProblem::cost();
		}

		template <class PhysicsProblem, class OtherProblem>
		real SumProblem<PhysicsProblem, OtherProblem>::_cost(count_t i) const
		{
			return PhysicsProblem::_cost(i) + OtherProblem::_cost(i);
		}

		template <class PhysicsProblem, class OtherProblem>
		Control SumProblem<PhysicsProblem, OtherProblem>::gradient() const
		{
			return PhysicsProblem::gradient() + OtherProblem::gradient();
		}

		template <class PhysicsProblem, class OtherProblem>
		RVec SumProblem<PhysicsProblem, OtherProblem>::_gradient(count_t index) const
		{
			return PhysicsProblem::_gradient(index) + OtherProblem::_gradient(index);
		}

		template <class PhysicsProblem, class OtherProblem, typename>
		SumProblem<PhysicsProblem, OtherProblem> operator+(PhysicsProblem&& physicsProblem, OtherProblem&& otherProblem)
		{
			return SumProblem<PhysicsProblem, OtherProblem>(std::move(physicsProblem), std::move(otherProblem));
		}
}

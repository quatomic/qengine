﻿/* COPYRIGHT
 *
 * file="Basis.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <vector>
#include "src/Abstract/OptimalControl/Parameters/Control.h"
#include "src/Abstract/OptimalControl/Parameters/BasisControl.h"

namespace qengine
{
	class Basis
	{
	public:
		Basis(const std::vector<Control>& controls);
		Basis(std::initializer_list<Control> controls);

		const Control& at(count_t i) const;

		count_t size() const;
		count_t controlSize() const;
		count_t paramCount() const;
		real dt() const;

		Basis operator*= (const Control& shapeFunction);

		// INTERNAL USE ONLY
	public:
		const std::vector<Control>& _controls() const noexcept { return controls_; }

	private:
		std::vector<Control> controls_;
	};

	Basis operator* (const Control& shapeFunction, Basis basis);

	Control operator*(const BasisControl& basisControl, const Basis& basis);

}

﻿/* COPYRIGHT
 *
 * file="BasisControl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Utility/LinearAlgebra/Matrix.h"

namespace qengine
{
	class BasisControl
	{
	public:
		/// CTORS
	public:
		BasisControl();

		/// construct BasisControl from matrix, assuming mat is {x0 x1 x2 ... ; A1 A2 A3 ...}
		explicit BasisControl(const RMat& mat);

		explicit BasisControl(const RVec& vec, count_t paramCount);

		static BasisControl zeros(count_t length, count_t paramCount);

		BasisControl(const BasisControl& other);
		BasisControl(BasisControl&& other) noexcept;

		BasisControl& operator=(const BasisControl& other);
		BasisControl& operator=(BasisControl&& other) noexcept;

		~BasisControl() = default;

		///MISC
	public:
		void zeros();

		count_t size() const { return mat_.n_cols(); }
		count_t paramCount() const { return mat_.n_rows(); }

		friend std::ostream& operator<<(std::ostream& stream, const BasisControl& control);

		RVec norms() const;
        real norm() const;

		BasisControl& append(const BasisControl& tail);
		BasisControl& glue(const BasisControl& tail);

		internal::SubviewCol<real> at(count_t index);
		const internal::SubviewCol<real> at(count_t index) const;

		/// MATH ops
	public:
		BasisControl & operator+=(const BasisControl& other);
		BasisControl& operator-=(const BasisControl& other);

		BasisControl operator-() const;

		RMat& mat();
		const RMat& mat() const;


		/// INTERNAL USE
	public:
		RVec & _vecRef();


		RVec _vec() const;



	private:
		RMat mat_;
		RVec vec_;
	};

	BasisControl operator*(real x, const BasisControl& p);

	BasisControl operator+(BasisControl a, const BasisControl& b);

	BasisControl operator-(BasisControl a, const BasisControl& b);

	real normDot(const BasisControl& left, const BasisControl& right);

	real dot(const BasisControl& left, const BasisControl& right);

	BasisControl diff(const BasisControl& BasisControl);

	real dot_H1(const BasisControl& left, const BasisControl& right);

	RVec norms(const BasisControl& BasisControl);

	/// add BasisControl tail to back of BasisControl head
	BasisControl append(BasisControl head, const BasisControl& tail);

	/// add tail to head, but without the first point in tail
	BasisControl glue(BasisControl head, const BasisControl& tail);
}

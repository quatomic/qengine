﻿/* COPYRIGHT
 *
 * file="BasisControl.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/OptimalControl/Parameters/BasisControl.h"

#include <armadillo>
#include "src/Utility/qengine_assert.h"

/// BasisControl
namespace qengine
{
	BasisControl::BasisControl() : mat_()
	{
	}

	BasisControl::BasisControl(const RMat& mat) : mat_(mat)
	{
		qengine_assert(!mat_.hasNan(), "basiscontrol has NaN");
	}

	BasisControl::BasisControl(const RVec& vec, const count_t paramCount) : mat_(RMat(arma::reshape(vec._vec(), paramCount, vec.size() / paramCount)))
	{
		qengine_assert(!mat_.hasNan(), "basiscontrol has NaN");
	}

	BasisControl BasisControl::zeros(const count_t length, const count_t paramCount)
	{
		return BasisControl(RMat(paramCount, length, 0.0));
	}

	BasisControl::BasisControl(const BasisControl& other) : mat_{ other.mat_ }
	{
	}

	BasisControl::BasisControl(BasisControl&& other) noexcept : mat_{ std::move(other.mat_) }
	{
	}

	BasisControl& BasisControl::operator=(const BasisControl& other)
	{
		if (this == &other)
			return *this;
		mat_ = other.mat_;
		return *this;
	}

	BasisControl& BasisControl::operator=(BasisControl&& other) noexcept
	{
		if (this == &other)
			return *this;
		mat_ = std::move(other.mat_);
		return *this;
	}

	void BasisControl::zeros()
	{
		for (auto i = 0u; i < mat_.n_cols(); ++i)
		{
			for (auto j = 0u; j < mat_.n_rows(); ++j)
			{
				mat_.at(j, i) = 0.0;
			}
		}
	}

	internal::SubviewCol<real> BasisControl::at(const count_t index)
	{
		return mat_.col(index);
	}

	const internal::SubviewCol<real> BasisControl::at(const count_t index) const
	{
		return mat_.col(index);
	}

	RVec& BasisControl::_vecRef()
	{
		vec_ = RVec(arma::vec(mat_._mat().memptr(), mat_.n_elem(), false, true));
		return vec_;
	}

	RMat& BasisControl::mat()
	{
		return mat_;
	}

	const RMat& BasisControl::mat() const
	{
		return mat_;
	}

	RVec BasisControl::norms() const
	{
		auto normvec = RVec(paramCount());

		for (auto i = 0u; i < paramCount(); ++i)
		{
			auto no = arma::norm(mat_._mat().row(i));
            normvec.at(i) = std::sqrt(no * no);
		}

		return normvec;
	}

    real BasisControl::norm() const
    {
        return sum(norms());
    }


	BasisControl& BasisControl::append(const BasisControl& tail)
	{
		mat_._mat() = arma::join_rows(mat_._mat(), tail.mat_._mat());
		return *this;
	}

	BasisControl& BasisControl::glue(const BasisControl& tail)
	{
		mat_._mat() = arma::join_rows(mat_._mat(), tail.mat_._mat().cols(1, tail.size() - 1));
		return *this;
	}

	RVec BasisControl::_vec() const
	{
		return mat_.vectorize();
	}

	BasisControl& BasisControl::operator+=(const BasisControl& other)
	{
		this->mat_ += other.mat_;
		return *this;
	}

	BasisControl& BasisControl::operator-=(const BasisControl& other)
	{
		this->mat_ -= other.mat_;
		return *this;
	}

	BasisControl BasisControl::operator-() const
	{
		return BasisControl(-this->mat_);
	}


	std::ostream& operator<<(std::ostream& stream, const BasisControl& BasisControl)
	{
		stream << BasisControl.mat_._mat().t();
		return stream;
	}

}

/// BasisControl free functions
namespace qengine
{
	BasisControl operator*(real x, const BasisControl& p)
	{
		return BasisControl(x * p.mat());
	}

	BasisControl operator+(BasisControl a, const BasisControl& b)
	{
		return a += b;
	}

	BasisControl operator-(BasisControl a, const BasisControl& b)
	{
		return a -= b;
	}

	real normDot(const BasisControl& left, const BasisControl& right)
	{
		return normDot(left.mat(), right.mat());
	}

	real dot(const BasisControl& left, const BasisControl& right)
	{
		auto val1 = arma::mat(left.mat()._mat() % right.mat()._mat());

		if (val1.n_cols > 1)
		{
			auto val2 = arma::mat(arma::trapz(val1, 1));
			return arma::as_scalar(arma::sum(val2));
		}
		else
		{
			return arma::as_scalar(arma::sum(val1));
		}
	}

	BasisControl diff(const BasisControl& control)
	{
		return BasisControl(RMat(arma::diff(control.mat()._mat().t()).t()));
	}

	real dot_H1(const BasisControl& left, const BasisControl& right)
	{
		return dot(diff(left), diff(right));
	}

	RVec norms(const BasisControl& control)
	{
		return control.norms();
	}

	BasisControl append(BasisControl head, const BasisControl& tail)
	{
		return head.append(tail);
	}

	BasisControl glue(BasisControl head, const BasisControl& tail)
	{
		return head.glue(tail);
	}
}

﻿/* COPYRIGHT
 *
 * file="Control.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/OptimalControl/Parameters/Control.h"

#include <armadillo>

#include "src/Utility/qengine_assert.h"

/// Control
namespace qengine
{
	Control::Control() : Control(0.0)
	{
	}

	Control::Control(const real dt) : mat_(), dt_(dt)
	{
		qengine_assert(!std::isnan(dt), "dt cannot be NaN!");
	}

	Control::Control(const RMat& mat, const real dt) : mat_(mat), dt_(dt)
	{
		qengine_assert(!std::isnan(dt), "dt cannot be NaN!");
		qengine_assert(!mat_.hasNan(), "control has NaN");
	}

	Control::Control(const RVec& vec, const count_t paramCount, const real dt) : mat_(RMat(arma::reshape(vec._vec(), paramCount, vec.size() / paramCount))), dt_(dt)
	{
		qengine_assert(!std::isnan(dt), "dt cannot be NaN!");
		qengine_assert(!mat_.hasNan(), "control has NaN");
	}

	Control Control::zeros(const count_t length, const count_t paramCount, const real dt)
	{
		return Control(RMat(paramCount, length, 0.0), dt);
	}

	Control::Control(const Control& other) :
		mat_{ other.mat_ },
		dt_(other.dt_)
	{
	}

	Control::Control(Control&& other) noexcept :
		mat_{ std::move(other.mat_) },
		dt_(other.dt_)
	{
	}

	Control& Control::operator=(const Control& other)&
	{
		if (this == &other)
			return *this;
		mat_ = other.mat_;
		dt_ = other.dt_;
		return *this;
	}

	Control& Control::operator=(Control&& other)& noexcept
	{
		if (this == &other)
			return *this;
		mat_ = std::move(other.mat_);
		dt_ = other.dt_;
		return *this;
	}

	void Control::zeros()
	{
		for (auto i = 0u; i < mat_.n_cols(); ++i)
		{
			for (auto j = 0u; j < mat_.n_rows(); ++j)
			{
				mat_.at(j, i) = 0.0;
			}
		}
	}

	RVec Control::get(const count_t index) const
	{
		return mat_.col(index);
	}

	void Control::set(const count_t index, const RVec& v)
	{
		mat_.col(index) = v;
	}

	RVec Control::getFront() const
	{
		return get(0);
	}

	void Control::setFront(const RVec& v)
	{
		set(0, v);
	}

	RVec Control::getBack() const
	{
		return get(size() - 1);
	}

	void Control::setBack(const RVec& v)
	{
		set(size() - 1, v);
	}

	RVec& Control::_vecRef()&
	{
		vec_ = RVec(arma::vec(mat_._mat().memptr(), mat_.n_elem(), false, true));
		return vec_;
	}

	RMat& Control::mat()&
	{
		return mat_;
	}

	RMat&& Control::mat() &&
	{
		return std::move(mat_);
	}

	const RMat& Control::mat() const&
	{
		return mat_;
	}

	internal::SubviewCol<real> Control::at(const count_t index)
	{
		return mat_.col(index);
	}

	const internal::SubviewCol<real> Control::at(const count_t index) const
	{
		return mat_.col(index);
	}

	Control& Control::operator+=(const Control& other)
	{
		mat_ += other.mat();
		return *this;
	}

	Control& Control::operator-=(const Control& other)
	{
		mat_ -= other.mat();
		return *this;
	}

    RVec Control::norms() const
    {
        return normsL2();
    }

    real Control::norm() const
    {
        return normL2();
    }

	RVec Control::normsL2() const
	{
		return sqrt(dotsL2(*this, *this));
	}

	real Control::normL2() const
	{
		return sum(normsL2());
	}

	RVec Control::normsH1() const
	{
		return sqrt(dotsH1(*this, *this));
	}

	real Control::normH1() const
	{
		return sum(normsH1());
	}

	Control& Control::append(const Control& tail)
	{
		qengine_assert(tail.dt() == dt(), "trying to append together controls with different dts!");
		mat_._mat() = arma::join_rows(mat_._mat(), tail.mat_._mat());
		return *this;
	}

	Control& Control::glue(const Control& tail)
	{
		qengine_assert(getBack() == tail.getFront(), "end of head must equal front of tail!");
		qengine_assert(tail.dt() == dt(), "trying to glue together controls with different dts!");
		mat_._mat() = arma::join_rows(mat_._mat(), tail.mat_._mat().cols(1, tail.size() - 1));
		return *this;
	}

	RVec Control::_vec() const
	{
		return mat_.vectorize();
	}

	real Control::dt() const
	{
		return dt_;
	}

	void Control::setDt(const real newDt)
	{
		dt_ = newDt;
	}

	real Control::endTime() const
	{
		return (size() - 1)*dt_;
	}

	Control Control::operator-() const
	{
		return Control(-this->mat_, this->dt_);
	}


	std::ostream& operator<<(std::ostream& stream, const Control& Control)
	{
		stream << Control.mat_._mat().t();
		return stream;
	}


	Control makeControl(const std::vector<RVec>& params, real dt)
	{
		auto armaMat = arma::mat(params.size(), params.at(0).size());
		for (auto i = 0u; i < params.size(); ++i)
		{
			armaMat.row(i) = params.at(i)._vec().t();
		}

		return Control(RMat(armaMat), dt);
	}

	Control makeControl(const std::vector<Control>& params)
	{
		qengine_assert(params.size() > 0, "");
		const auto dt = params.front().dt();

		auto fullArmaMatrix = params.front().mat()._mat();
		
		for (auto it = params.begin()+1; it != params.end(); ++it)
		{
			const auto& c = *it;
			qengine_assert(c.dt() == dt, "dt must be the same for all params!");
			fullArmaMatrix = arma::join_cols(fullArmaMatrix, c.mat()._mat());
		}
		return Control(RMat(fullArmaMatrix), dt);
	}

}

/// Control free functions
namespace qengine
{
	bool operator==(const Control& left, const Control& right)
	{
		if (left.dt() != right.dt()) return false;
		return left.mat() == right.mat();
	}

	bool operator!=(const Control& left, const Control& right)
	{
		return !(left == right);
	}

	Control operator+(const Control& left, const Control& right)
	{
		qengine_assert(left.dt() == right.dt(), "controls do not have the same dt!");
		return Control(left.mat() + right.mat(), left.dt());
	}
	Control operator-(const Control& left, const Control& right)
	{
		qengine_assert(left.dt() == right.dt(), "controls do not have the same dt!");
		return Control(left.mat() - right.mat(), left.dt());
	}

	Control operator*(const Control& left, const Control& right)
	{
		qengine_assert(left.dt() == right.dt(), "controls do not have the same dt!");
		return Control(left.mat() % right.mat(), left.dt());
	}
	Control operator/(const Control& left, const Control& right)
	{
		qengine_assert(left.dt() == right.dt(), "controls do not have the same dt!");
		return Control(RMat(left.mat()._mat() / right.mat()._mat()), left.dt());
	}

	Control operator*(const real x, Control p)
	{
		p.mat() *= x;
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator*(Control p, const real x)
	{
		p.mat() *= x;
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator+(const real x, Control p)
	{
		p.mat()._mat() += x;
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator+(Control p, const real x)
	{
		p.mat()._mat() += x;
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator-(const real x, Control p)
	{

		p.mat()._mat() *= -1;
		p.mat()._mat() += x;
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator-(Control p, const real x)
	{
		p.mat()._mat() -= x;
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator/(Control p, const real x)
	{
		p.mat()._mat() /= x;

		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator/(const real x, Control p)
	{
		p.mat()._mat() = x / p.mat()._mat();
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator*(const RVec x, Control p)
	{
		qengine_assert(x.dim() == p.paramCount(), "x must have size equal to p's paramCount!");
		p.mat()._mat().each_col() %= x._vec();
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator*(Control p, const RVec x)
	{
		qengine_assert(x.dim() == p.paramCount(), "x must have size equal to p's paramCount!");
		p.mat()._mat().each_col() %= x._vec();
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator+(const RVec x, Control p)
	{
		qengine_assert(x.dim() == p.paramCount(), "x must have size equal to p's paramCount!");
		p.mat()._mat().each_col() += x._vec();
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator+(Control p, const RVec x)
	{
		qengine_assert(x.dim() == p.paramCount(), "x must have size equal to p's paramCount!");
		p.mat()._mat().each_col() += x._vec();
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator-(const RVec x, const Control& p)
	{
		qengine_assert(!x.hasNan(), "control has NaN");
		return -p + x;
	}

	Control operator-(Control p, const RVec x)
	{
		qengine_assert(x.dim() == p.paramCount(), "x must have size equal to p's paramCount!");
		p.mat()._mat().each_col() -= x._vec();
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator/(Control p, const RVec x)
	{
		qengine_assert(x.dim() == p.paramCount(), "x must have size equal to p's paramCount!");
		p.mat()._mat().each_col() /= x._vec();
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}

	Control operator/(const RVec x, Control p)
	{
		qengine_assert(x.dim() == p.paramCount(), "x must have size equal to p's paramCount!");
		p.mat()._mat() = x._vec() / p.mat()._mat().each_col();
		qengine_assert(!p.mat().hasNan(), "control has NaN");
		return p;
	}


}

namespace qengine
{
	RVec dotsL2(const Control& left, const Control& right)
	{
		qengine_assert(left.dt() == right.dt(), "controls do not have the same dt!");

		const auto val1 = arma::mat(left.mat()._mat() % right.mat()._mat());
		const auto val2 = arma::mat(arma::sum(val1, 1)*left.dt());
		return RVec(val2);
	}

	real dotL2(const Control& left, const Control& right)
	{
		return sum(dotsL2(left, right));
	}
	real normL2(const Control& p)
	{
		return p.normL2();
	}
	RVec normsL2(const Control& p)
	{
		return p.normsL2();
	}
	real normDotL2(const Control& left, const Control& right)
	{
		return dotL2(left, right) / (normL2(left)*normL2(right));
	}

	namespace
	{
		Control ddt(const Control& u)
		{
			return Control(RMat(arma::diff(u.mat()._mat(), 1, 1)) / u.dt(), u.dt());
		}
	}

	RVec dotsH1(const Control& left, const Control& right)
	{
		qengine_assert(left.dt() == right.dt(), "controls do not have the same dt!");

		if (&left == &right)
		{
			const auto ddt_ = ddt(left);
			return dotsL2(ddt_, ddt_);
		}

		return dotsL2(ddt(left), ddt(right));
	}
	real dotH1(const Control& left, const Control& right)
	{
		return sum(dotsH1(left, right));
	}
	RVec normsH1(const Control& p)
	{
		return p.normsH1();
	}
	real normH1(const Control& p)
	{
		return p.normH1();
	}
	real normDotH1(const Control& left, const Control& right)
	{
		return dotH1(left, right) / (normH1(left)*normH1(right));
	}

	real mean(const Control& control)
	{
		return mean(means(control));
	}

	RVec means(const Control& control)
	{
		return RVec(arma::mean(control.mat()._mat(), 1));
	}

	Control abs(const Control& control)
	{
		return Control(abs(control.mat()), control.dt());
	}

	real max(const Control& control)
	{
		return max(maxs(control));
	}

	RVec maxs(const Control& control)
	{
		return RVec(arma::max(control.mat()._mat(), 1));
	}

	real min(const Control& control)
	{
		return min(mins(control));
	}

	RVec mins(const Control& control)
	{
		return RVec(arma::min(control.mat()._mat(), 1));
	}


	Control append(Control head, const Control& tail)
	{
		return head.append(tail);
	}

	Control glue(Control head, const Control& tail)
	{
		return head.glue(tail);
	}

	Control glue(const std::vector<Control>& controls)
	{
		qengine_assert(controls.size() > 0, "there must be at least one control in call to glue");

		auto u = controls.at(0);

		for (auto i = 1u; i < controls.size(); ++i)
		{
			u.glue(controls.at(i));
		}
		return u;
	}

	Control invert(const Control& control)
	{
		return Control(RMat(arma::fliplr(control.mat()._mat())), control.dt());
	}


}

namespace qengine
{
	Control exp(const Control& u) { return Control(RMat(arma::exp(u.mat()._mat())), u.dt()); }

	Control pow(const Control& u, const count_t exponent) { return Control(RMat(arma::pow(u.mat()._mat(), static_cast<real>(exponent))), u.dt()); }

	Control exp2(const Control& u) { return Control(RMat(arma::exp2(u.mat()._mat())), u.dt()); }

	Control exp10(const Control& u) { return Control(RMat(arma::exp10(u.mat()._mat())), u.dt()); }

	Control log(const Control& u) { return Control(RMat(arma::log(u.mat()._mat())), u.dt()); }

	Control log2(const Control& u) { return Control(RMat(arma::log2(u.mat()._mat())), u.dt()); }

	Control log10(const Control& u) { return Control(RMat(arma::log10(u.mat()._mat())), u.dt()); }

	Control sqrt(const Control& u) { return Control(RMat(arma::sqrt(u.mat()._mat())), u.dt()); }

}

namespace qengine
{
	Control sin(const Control& u) { return Control(RMat(arma::sin(u.mat()._mat())), u.dt()); }

	Control cos(const Control& u) { return Control(RMat(arma::cos(u.mat()._mat())), u.dt()); }

	Control tan(const Control& u) { return Control(RMat(arma::tan(u.mat()._mat())), u.dt()); }

	Control asin(const Control& u) { return Control(RMat(arma::asin(u.mat()._mat())), u.dt()); }

	Control acos(const Control& u) { return Control(RMat(arma::acos(u.mat()._mat())), u.dt()); }

	Control atan(const Control& u) { return Control(RMat(arma::atan(u.mat()._mat())), u.dt()); }

	Control sinh(const Control& u) { return Control(RMat(arma::sinh(u.mat()._mat())), u.dt()); }

	Control cosh(const Control& u) { return Control(RMat(arma::cosh(u.mat()._mat())), u.dt()); }

	Control tanh(const Control& u) { return Control(RMat(arma::tanh(u.mat()._mat())), u.dt()); }

	Control asinh(const Control& u) { return Control(RMat(arma::asinh(u.mat()._mat())), u.dt()); }

	Control acosh(const Control& u) { return Control(RMat(arma::acosh(u.mat()._mat())), u.dt()); }

	Control atanh(const Control& u) { return Control(RMat(arma::atanh(u.mat()._mat())), u.dt()); }
}

namespace qengine
{
	Control makeTimeControl(const count_t numberOfPoints, const real dt, const real t0)
	{
		return makeLinearControl(t0, t0 + (numberOfPoints - 1)*dt, numberOfPoints, dt);
	}

	Control makeLinearControl(const real from, const real to, const count_t numberOfPoints, const real dt)
	{
		return makeControl({ linspace(from, to, numberOfPoints) }, dt);
	}

	Control makePieceWiseLinearControl(std::vector<std::pair<count_t, real>> points, real dt)
	{
		qengine_assert(points.size() >= 2, "need at least 2 points in call to makePiecewiseLinearControl!");

		auto u = makeLinearControl(points.at(0).second, points.at(1).second, points.at(1).first - points.at(0).first + 1, dt);
		for (auto i = 1ull; i < points.size() - 1; ++i)
		{
			u.glue(makeLinearControl(points.at(i).second, points.at(i + 1).second, points.at(i + 1).first - points.at(i).first + 1, dt));
		}

		return u;
	}

	Control step(const Control& x, const real stepPos)
	{
		qengine_assert(x.size() == 1, "");

		return Control(RMat(step(x._vec(), stepPos)), x.dt());
	}
	Control box(const Control& x, const real left, const real right)
	{
		qengine_assert(x.size() == 1, "");

		return Control(RMat(box(x._vec(), left, right)), x.dt());
	}
	Control well(const Control& x, const real left, const real right)
	{
		qengine_assert(x.size() == 1, "");

		return Control(RMat(well(x._vec(), left, right)), x.dt());
	}

	// s for "sigmoid"
	Control sstep(const Control& x, const real center, const real hardness)
	{
		qengine_assert(x.size() == 1, "");

		return Control(RMat(sstep(x._vec(), center, hardness)), x.dt());
	}
	Control sbox(const Control& x, const real left, const real right, const real hardness)
	{
		qengine_assert(x.size() == 1, "");

		return Control(RMat(sbox(x._vec(), left, right, hardness)), x.dt());
	}
	Control swell(const Control& x, const real left, const real right, const real hardness)
	{
		qengine_assert(x.size() == 1, "");

		return Control(RMat(swell(x._vec(), left, right, hardness)), x.dt());
	}
}

﻿/* COPYRIGHT
 *
 * file="Control.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Utility/LinearAlgebra/Matrix.h"

namespace qengine
{
	struct ControlMetaData
	{
		count_t paramCount;
		count_t controlSize;
		real dt;
	};

	class Control
	{
	public:
		/// CTORS
	public:
		Control();

		explicit Control(real dt);

		/// construct Control from matrix, assuming _mat is time along horizontal and parameter along vertical
		explicit Control(const RMat& mat, real dt);

		// constructs control from vector, assuming vector is a matrix of vertical size paramCount in column-major order. 
		// So the first 'paramCount' elements are the control at t=0
		explicit Control(const RVec& vec, count_t paramCount, real dt);
		//explicit Control(std::initializer_list<RVec> list, real dt);

		static Control zeros(count_t length, count_t paramCount, real dt);

		Control(const Control& other);
		Control(Control&& other) noexcept;

		Control& operator=(const Control& other) &;
		Control& operator=(Control&& other) & noexcept;

		~Control() = default;
		/// DATA ACCESS
	public:
		RVec get(count_t i) const;
		void set(count_t i, const RVec& v);

		RVec getFront() const;
		void setFront(const RVec& v);

		RVec getBack() const;
		void setBack(const RVec& v);

		///MISC
	public:
		void zeros();

		count_t size() const { return mat_.n_cols(); }
		count_t paramCount() const { return mat_.n_rows(); }

		friend std::ostream& operator<<(std::ostream& stream, const Control& control);

        RVec norms() const;
        real norm() const;

		RVec normsL2() const;
		real normL2() const;

		RVec normsH1() const;
		real normH1() const;

		Control& append(const Control& tail);
		Control& glue(const Control& tail);

		real dt() const;
		void setDt(real newDt);
		real endTime() const;

		ControlMetaData metaData() const { return ControlMetaData{ paramCount(), size(), dt() }; }

		internal::SubviewCol<real> at(count_t index);
		const internal::SubviewCol<real> at(count_t index) const;

		/// MATH ops
	public:
		Control operator-() const;

		Control& operator+=(const Control& other);
		Control& operator-=(const Control& other);

		RMat& mat() &;
		RMat&& mat() && ;
		const RMat& mat() const&;

		/// INTERNAL USE
	public:

		// this allows us to interpret the Control as an RVec. 
		// However, the pointer to memory held by the ref may change from other sources, so it should not be saved by itself. 
		// In other words: whenever you need to interpret a Contorl like an RVec, call _vecRef. Do not save the result from ._vecRef()!
		RVec& _vecRef() &;
		RVec _vec() const;

		const auto& _rep() const noexcept { return mat_; }

	private:
		RMat mat_;
		RVec vec_;
		real dt_;
	};


	// assumes each RVec in the std::vector is one control-parameter over time. Must be same length
	Control makeControl(const std::vector<RVec>& params, real dt);
	Control makeControl(const std::vector<Control>& params);

}

namespace qengine
{
	bool operator==(const Control& left, const Control& right);
	bool operator!=(const Control& left, const Control& right);


	Control operator+(const Control& left, const Control& right);
	Control operator-(const Control& left, const Control& right);
	Control operator*(const Control& left, const Control& right);
	Control operator/(const Control& left, const Control& right);

	Control operator*(real x, Control p);
	Control operator*(Control p, real x);
	Control operator+(real x, Control p);
	Control operator+(Control p, real x);
	Control operator-(real x, const Control& p);
	Control operator-(Control p, real x);
	Control operator/(Control p, real x);
	Control operator/(real x, Control p);

	Control operator*(RVec x, Control p);
	Control operator*(Control p, RVec x);
	Control operator+(RVec x, Control p);
	Control operator+(Control p, RVec x);
	Control operator-(RVec x, Control p);
	Control operator-(Control p, RVec x);
	Control operator/(Control p, RVec x);
	Control operator/(RVec x, Control p);
}



namespace qengine
{
	RVec dotsL2(const Control& left, const Control& right);
	real dotL2(const Control& left, const Control& right);
	RVec normsL2(const Control& p);
	real normL2(const Control& p);
	real normDotL2(const Control& left, const Control& right);

	RVec dotsH1(const Control& left, const Control& right);
	real dotH1(const Control& left, const Control& right);
	RVec normsH1(const Control& p);
	real normH1(const Control& p);
	real normDotH1(const Control& left, const Control& right);

	real mean(const Control& control);
	RVec means(const Control& control);

	Control abs(const Control& control);

	real max(const Control& control);
	RVec maxs(const Control& control);

	real min(const Control& control);
	RVec mins(const Control& control);

	/// add Control tail to back of Control head
	Control append(Control head, const Control& tail);

	/// add tail to head, but without the first point in tail
	Control glue(Control head, const Control& tail);

	Control glue(const std::vector<Control>& controls);

	Control invert(const Control& control);

}

namespace qengine
{
	Control exp(const Control& u);

	Control pow(const Control& u, count_t exponent);

	Control exp2(const Control& u);

	Control exp10(const Control& u);

	Control log(const Control& u);

	Control log2(const Control& u);

	Control log10(const Control& u);

	Control sqrt(const Control& u);
}

namespace qengine
{
	Control sin(const Control& u);

	Control cos(const Control& u);

	Control tan(const Control& u);

	Control asin(const Control& u);

	Control acos(const Control& u);

	Control atan(const Control& u);

	Control sinh(const Control& u);

	Control cosh(const Control& u);

	Control tanh(const Control& u);

	Control asinh(const Control& u);

	Control acosh(const Control& u);

	Control atanh(const Control& u);
}

//special construction functions
namespace qengine
{
	Control makeTimeControl(count_t numberOfPoints, real dt, real t0 = 0.0);
	Control makeLinearControl(real from, real to, count_t numberOfPoints, real dt);
	Control makePieceWiseLinearControl(std::vector<std::pair<count_t, real>> points, real dt);

	Control step(const Control& x, real stepPos);
	Control box(const Control& x, real left, real right);
	Control well(const Control& x, real left, real right);

	// s for "sigmoid"
	Control sstep(const Control& x, real center, real hardness);
	Control sbox(const Control& x, real left, real right, real hardness);
	Control swell(const Control& x, real left, real right, real hardness);
}

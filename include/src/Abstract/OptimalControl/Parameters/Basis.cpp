﻿/* COPYRIGHT
 *
 * file="Basis.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/OptimalControl/Parameters/Basis.h"
#include "src/Utility/qengine_assert.h"

namespace qengine
{
	namespace
	{
		bool areControlsValid(const std::vector<Control>& controls)
		{
			if (controls.size() == 0) return false;
			const auto controlSize = controls.front().size();
			const auto paramCount = controls.front().paramCount();
			const auto dt = controls.front().dt();

			for(const auto& c:controls)
			{
				if (c.size() != controlSize) return false;
				if (c.paramCount() != paramCount) return false;
				if (c.dt() != dt) return false;
			}

			return true;
		}
	}

	Basis::Basis(const std::vector<Control>& controls):
	controls_(controls)
	{
		qengine_assert(areControlsValid(controls), "");
	}

	Basis::Basis(std::initializer_list<Control> controls):Basis(std::vector<Control>(controls))
	{
	}

	const Control& Basis::at(count_t i) const
	{
		return controls_.at(i);
	}

	count_t Basis::size() const
	{
		return controls_.size();
	}

	count_t Basis::controlSize() const
	{
		return controls_.front().size();
	}

	count_t Basis::paramCount() const
	{
		return controls_.front().paramCount();
	}

	real Basis::dt() const
	{
		return controls_.front().dt();
	}

	Basis Basis::operator*=(const Control& shapeFunction)
	{
		qengine_assert(shapeFunction.size() == controlSize(), "shapefunction does not have same size as basis-elements");
		qengine_assert(shapeFunction.paramCount() == paramCount() || shapeFunction.paramCount() == 1u, "shapefunction does not have proper paramCount");
		
		if(shapeFunction.paramCount() == paramCount())
		{
			for(auto& c: controls_)
			{
				c = shapeFunction * c;
			}
		}
		if(shapeFunction.paramCount() == 1)
		{
			const auto actualShapeFunction = makeControl(std::vector<Control>(paramCount(), shapeFunction));

			for(auto& c: controls_)
			{
				c = actualShapeFunction * c;
			}
		}

		return *this;
	}

	Basis operator*(const Control& shapeFunction, Basis basis)
	{
		return basis *= shapeFunction;
	}

	Control operator*(const BasisControl& basisControl, const Basis& basis)
	{
		qengine_assert(basisControl.size() == basis.size(), "basisControl and basis must have same size. sizes are (control/basis): " + std::to_string(basisControl.size()) + "/" + std::to_string(basis.size()));
		qengine_assert(basisControl.paramCount() == basis.paramCount(), "basisControl and basis must have same number of control parameters");
		auto control = Control::zeros(basis.controlSize(), basis.paramCount(), basis.dt());

		for(auto i = 0u; i<basis.size(); ++i)
		{
			control += basisControl.at(i)*basis.at(i);
		}
		return control;
	}
}

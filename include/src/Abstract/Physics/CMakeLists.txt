cmake_minimum_required (VERSION 3.3)
project (QEngine)

file(GLOB_RECURSE SOURCES 
	"*.h" "*.cpp"
)

SET(HELPERS pch.h pch.cpp 
)

add_library(AbstractPhysics OBJECT ${SOURCES} ${HELPERS})
target_include_directories(AbstractPhysics PRIVATE "${ARMADILLO_INCLUDE_DIR}")


if(MSVC)
	
	source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SOURCES})
	source_group("" FILES ${HELPERS})

endif()

set (pchFile "${CMAKE_CURRENT_SOURCE_DIR}/pch.h")
add_pch(AbstractPhysics ${pchFile})

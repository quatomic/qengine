﻿/* COPYRIGHT
 *
 * file="HamiltonianFunction.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once



#include "src/Abstract/Physics/Spatial/PotentialFunction.h"
#include "src/Abstract/Physics/Spatial/isHamiltonian.h"

namespace qengine
{
	template<class Hamiltonian, class PotentialType>
	class HamiltonianFunction
	{
		static_assert(spatial::isHamiltonian<Hamiltonian>::value, "");
	public:
		HamiltonianFunction(const Hamiltonian& H0, PotentialType potential) :
			H0_(H0),
			V_(potential)
		{}

		template<class... Ts>
		Hamiltonian operator()(const Ts&... ts) const
		{
			//using StaticPotential = decltype(V_(V_.initialParams()));
			//static_assert(is_callable<PotentialType, StaticPotential(const Ts&...)>::value, "Potential must be callable with params");
			return H0_ + V_(ts...);
		}

		/// INTERNAL USE
	public:
		PotentialType _potentialFunction() const { return V_; }
		const auto& _H0() const { return H0_; }

	private:
		const Hamiltonian H0_;
		PotentialType V_;
	};

	namespace internal
	{
		template<class Hamiltonian, class PotentialFunctor, class... Params>
		HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>> makeDynamicHamiltonian(const Hamiltonian& H0, PotentialFunction<PotentialFunctor, Params...> potential)
		{
			return HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>>(H0, potential);
		}
	}
}

namespace qengine
{
	// standard way of creating a dynamicHamiltonian: sum a static hamiltonian with a dynamic potential.
	template<class Hamiltonian, class PotentialFunctor, class...Params>
	auto operator+ (const Hamiltonian& H0, const PotentialFunction<PotentialFunctor, Params...>& V)
		-> std::enable_if_t<
		spatial::isHamiltonian_v<Hamiltonian> && // limits the scope of the operator to types that have been explicitly declared as spatial hamiltonians
		spatial::isHamiltonian_v<decltype(H0 + internal::apply(V, V.initialParams()))>, // this line ensures both that the hamiltonian and the potential can be added together, and that the result is a spatialhamiltonian
		HamiltonianFunction<Hamiltonian, PotentialFunction<PotentialFunctor, Params...>> >
	{
		return internal::makeDynamicHamiltonian(H0, V);
	}

	// add additional static potentials to an existing dynamic hamiltonian
	template<class Hamiltonian, class DynPotential, class Potential>
	auto operator+ (const HamiltonianFunction<Hamiltonian, DynPotential>& H, const Potential& V)
		-> std::enable_if_t<spatial::isPotential_v<Potential>, // ensure Potential is a potential
	HamiltonianFunction<decltype(H._H0() + V), DynPotential >>
	{
		return internal::makeDynamicHamiltonian(H._H0() + V, H._potentialFunction());
	}

	// subtract additional static potentials fram an existing dynamic hamiltonian
	template<class Hamiltonian, class DynPotential, class Potential>
	auto operator- (const HamiltonianFunction<Hamiltonian, DynPotential>& H, const Potential& V)
		-> HamiltonianFunction<decltype(H._H0() - V), DynPotential >
	{
		return internal::makeDynamicHamiltonian(H._H0() - V, H._potentialFunction());
	}
}

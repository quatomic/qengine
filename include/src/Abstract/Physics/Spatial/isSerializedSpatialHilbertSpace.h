﻿/* COPYRIGHT
 *
 * file="isSerializedSpatialHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/has_function.h"
#include "src/Abstract/Physics/General/isSerializedHilbertSpace.h"
#include "src/Utility/std17/remove_cvref.h"

namespace qengine
{
	namespace internal
	{
		MAKE_HAS_FUNCTION(hasMethod_integrationConstant, integrationConstant);
		MAKE_HAS_FUNCTION(hasMethod_normalization, normalization);
		MAKE_HAS_FUNCTION(hasMethod_dimensions, dimensions);

		template<class Test>
		struct isSerializedSpatialHilbertSpace
		{
			using T = std17::remove_cvref_t<Test>;

			constexpr static bool value =
				isSerializedHilbertSpace_v<T>
				&& hasMethod_integrationConstant_v<const T, real()>
				&& hasMethod_normalization_v<const T, real()>
				&& hasMethod_dimensions_v<const T, std::vector<count_t>()>
				;
		};

		template<class T> constexpr bool isSerializedSpatialHilbertSpace_v = isSerializedSpatialHilbertSpace<T>::value;
	}
}

﻿/* COPYRIGHT
 *
 * file="SplitStep.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Fft/Fft.h"

namespace qengine
{
	class SplitStepAlgorithm
	{
	public:
		template<class Wavefunction, class expV1, class expT, class expV2>
		void operator() (Wavefunction& psi, expV1 applyExpPot1, expT applyExpKin, expV2 applyExpPot2) const
		{
			// todo: put in check that expotapplier and expkinapplier are callable with the correct types here!


			applyExpPot1(psi);

			iFFT_expT_FFT(psi, applyExpKin);

			applyExpPot2(psi);
		}

		template<class Wavefunction, class expT>
		void iFFT_expT_FFT(Wavefunction& psi, expT applyExpKin) const
		{

			if (psi.vec()._data() != memPtr_ || (&fft_->data() != &psi.vec()))
			{
				memPtr_ = psi.vec()._data();
				fft_.reset(new internal::FFT(psi.vec(), psi._serializedHilbertSpace().dimensions()));
			}

			fft_->doFft();
			applyExpKin(psi);
			fft_->doInverseFft();


		}

		SplitStepAlgorithm() {}
		SplitStepAlgorithm(const SplitStepAlgorithm&) {}
		SplitStepAlgorithm(SplitStepAlgorithm&&) noexcept {}

		SplitStepAlgorithm& operator=(const SplitStepAlgorithm&) = delete;
		SplitStepAlgorithm& operator=(SplitStepAlgorithm&&) = delete;

		~SplitStepAlgorithm() = default;

	private:
		mutable std::unique_ptr<internal::FFT> fft_{ nullptr };

		mutable complex* memPtr_ = nullptr;
	};


	template<class... Ts>
	auto splitStep(const Ts&... ts)
	{
		return [ts...](auto&&... ps)
		{
			return makeSplitStepAlgorithm(ts..., std::forward<decltype(ps)>(ps)...);
		};
	}
}

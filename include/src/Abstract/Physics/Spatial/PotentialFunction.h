﻿/* COPYRIGHT
 *
 * file="PotentialFunction.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>

#include "src/Utility/InitializedCallable.h"

#include "src/Abstract/Physics/Spatial/isPotential.h"

namespace qengine
{
	template<class PotentialFunctor, class... Params>
	class PotentialFunction : public internal::InitializedCallable<PotentialFunctor, Params...>
	{
	public:
		template<class V, class... Ts>
		friend auto makePotential(V f, Ts&&... initialParams)->PotentialFunction<V, std17::remove_cvref_t<Ts>...>;

	private:
		using internal::InitializedCallable<PotentialFunctor, Params...>::InitializedCallable;
	};

	template<class V, class... Ts>
	auto makePotentialFunction(V f, Ts&&... initialParams)->PotentialFunction<V, std17::remove_cvref_t<Ts>...>
	{
		static_assert(spatial::isPotential_v<decltype(f(initialParams...))>, "return type of the passed in function must be a spatial potential!");
		return PotentialFunction<V, std17::remove_cvref_t<Ts>...>(f, std::forward<Ts>(initialParams)...);
	}
}

﻿/* COPYRIGHT
 *
 * file="MakeKinetic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"

namespace qengine
{
	namespace internal
	{
		internal::SqHermitianBandMat<real> makeKineticMatrix(count_t dimension, real dx, real kinematicFactor);
	}
}

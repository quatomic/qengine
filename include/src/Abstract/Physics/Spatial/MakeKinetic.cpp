﻿/* COPYRIGHT
 *
 * file="MakeKinetic.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/Physics/Spatial/MakeKinetic.h"

#include "src/Abstract/Physics/Spatial/LinearHamiltonian.impl.h"
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"
#include <src/NumberTypes.h>
#include "src/Utility/LinearAlgebra/Matrix.h"

namespace qengine
{
	namespace internal
	{
		SqHermitianBandMat<real> makeKineticMatrix(const count_t dimension, const real dx, const real kinematicFactor)
		{
			auto dim = dimension;
			auto k = kinematicFactor;

			auto a = 30.0 / 12.0;
			auto b = -16.0 / 12.0;
			auto c = 1.0 / 12.0;

			auto scale = k / (dx*dx);

			auto kinetic = Matrix<real>(dim, 3, 1.0);
			kinetic.col(0) *= a * scale;
			kinetic.col(1) *= b * scale;
			kinetic.col(2) *= c * scale;

			return internal::SqHermitianBandMat<real>(kinetic, internal::SqHermitianBandMat<real>::COL_LAYOUT);
		}

	}
}

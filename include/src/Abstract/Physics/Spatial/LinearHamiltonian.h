﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/Spatial/LinearHamiltonian.fwd.h"

#include <ostream>

#include "src/NumberTypes.h"
#include "src/Utility/std17/remove_cvref.h"

#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.h"
#include "src/Abstract/Physics/Spatial/Spectrum.fwd.h"

namespace qengine
{
	template<class SerializedHilbertSpace, template<class> class Rep>
	class LinearHamiltonian : public internal::EigenstateSyntax_CRTP<LinearHamiltonian<SerializedHilbertSpace, Rep>>
	{
		static_assert(internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>, "");
	public:
		template<class Potential, class Representation, typename = std::enable_if_t<std::is_same<std17::remove_cvref_t<Potential>, FunctionOfX<real, SerializedHilbertSpace>>::value && std::is_same<std17::remove_cvref_t<Representation>, Rep<real>>::value, void>>
		LinearHamiltonian(const SerializedHilbertSpace& hilbertSpace, Representation&& kineticTerm, Potential&& potential);

		template<typename ScalarType>
		void applyInPlace(FunctionOfX<ScalarType, SerializedHilbertSpace>& f) const;

		spatial::Spectrum<real, SerializedHilbertSpace> makeSpectrum(count_t largestEigenvalue) const;

		/// INTERNAL USE
	public:
		const auto& _potential() const noexcept { return potential_; }
		const Rep<real>& _hamiltonianMatrix() const noexcept { return hamiltonianMatrix_; }
		const auto& _serializedHilbertSpace() const noexcept { return serializedHilbertSpace_; }


	private:
		FunctionOfX<real, SerializedHilbertSpace> potential_;
		Rep<real> hamiltonianMatrix_;
		const SerializedHilbertSpace serializedHilbertSpace_;
	};
	template<class SerializedHilbertSpace, template<class> class Rep>
	std::ostream& operator<<(std::ostream& s, const LinearHamiltonian<SerializedHilbertSpace, Rep>& H);
}

namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace, template<class> class Rep>
	FunctionOfX<ScalarType, SerializedHilbertSpace> operator*(const LinearHamiltonian<SerializedHilbertSpace, Rep>& H, FunctionOfX<ScalarType, SerializedHilbertSpace> psi)
	{
		H.applyInPlace(psi);
		return psi;
	}
}

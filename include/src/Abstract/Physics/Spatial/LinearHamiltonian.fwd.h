﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"
#include "src/Abstract/Physics/Spatial/isHamiltonian.h"

namespace qengine
{
	template<class SerializedHilbertSpace, template<class> class Rep>
	class LinearHamiltonian;
}

namespace qengine
{
	namespace spatial
	{
		template<class SerializedHilbertSpace, template<class> class Rep>
		struct isLabelledAs_Hamiltonian<LinearHamiltonian<SerializedHilbertSpace, Rep>> : std::true_type {};
	}
}

namespace qengine
{
	namespace internal
	{
		template<class SerializedHilbertSpace, template<class> class Rep>
		struct isLabelledAs_Linear<LinearHamiltonian<SerializedHilbertSpace, Rep>> : std::true_type {};

		template<class SerializedHilbertSpace, template<class> class Rep>
		struct isLabelledAs_Operator<LinearHamiltonian<SerializedHilbertSpace, Rep>> : std::true_type{};
		
		template<class SerializedHilbertSpace, template<class> class Rep>
		struct isLabelledAs_GuaranteedHermitian<LinearHamiltonian<SerializedHilbertSpace, Rep>> : std::true_type{};

		template<class SerializedHilbertSpace, template<class> class Rep>
		struct extract_serialized_hilbertspace<LinearHamiltonian<SerializedHilbertSpace, Rep>>
		{
			using type = SerializedHilbertSpace;
		};
	}
}

#include "src/Abstract/Physics/Spatial/SerializedHilbertSpace1D.h"

#include "src/Utility/qengine_assert.h"

namespace qengine
{
	namespace spatial
	{

		SerializedHilbertSpace1D_Base::SerializedHilbertSpace1D_Base(const real xLower, const real xUpper, const count_t dim, const real kinematicFactor)
			:
			xLower(xLower),
			xUpper(xUpper),
			dim(dim),
			kinematicFactor(kinematicFactor)
		{
			qengine_assert(xUpper > xLower, "xUpper must be larger than xLower when constructing spatial Hilbert space!");
			qengine_assert(dim >= 2, "dimension must be larger than or equal 2!");
			qengine_assert(kinematicFactor > 0.0, "kinematicFactor must be positive!");
		}

		bool operator==(const SerializedHilbertSpace1D_Base& left, const SerializedHilbertSpace1D_Base& right)
		{
			return
				left.xLower == right.xLower &&
				left.xUpper == right.xUpper &&
				left.dim == right.dim &&
				left.kinematicFactor == right.kinematicFactor;
		}

		bool operator!=(const SerializedHilbertSpace1D_Base& left, const SerializedHilbertSpace1D_Base& right)
		{
			return !(left == right);
		}

		real SerializedHilbertSpace1D_Base::dx() const
		{
			return (xUpper - xLower) / (dim - 1);
		}

		real SerializedHilbertSpace1D_Base::integrationConstant() const
		{
			return dx();
		}

		real SerializedHilbertSpace1D_Base::normalization() const 
		{
			return 1.0 / std::sqrt(dx());
		}

		std::vector<count_t> SerializedHilbertSpace1D_Base::dimensions() const
		{
			return { dim };
		}
	}
}

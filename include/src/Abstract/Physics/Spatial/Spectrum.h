﻿/* COPYRIGHT
 *
 * file="Spectrum.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/Spatial/Spectrum.fwd.h"

#include "src/Utility/LinearAlgebra/Spectrum.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.fwd.h"

namespace qengine
{
	namespace spatial
	{
		template<class ScalarType, class SerializedHilbertSpace>
		class Spectrum : public internal::Spectrum<ScalarType>
		{
			static_assert(internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>, "");
		public:
			template<class T1>
			Spectrum(T1&& t1, const SerializedHilbertSpace hilbertSpace);

			Spectrum(internal::Spectrum<ScalarType>&& t1, const SerializedHilbertSpace hilbertSpace);

			Spectrum(const internal::Spectrum<ScalarType>& t1, const SerializedHilbertSpace hilbertSpace);


			FunctionOfX<complex, SerializedHilbertSpace> eigenFunction(count_t i) const;

			FunctionOfX<complex, SerializedHilbertSpace> makeLinearCombination(const CVec& factors) const;

		private:
			const SerializedHilbertSpace serializedHilbertSpace_;
		};
	}
}

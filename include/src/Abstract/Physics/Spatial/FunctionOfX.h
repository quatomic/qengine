﻿/* COPYRIGHT
 *
 * file="FunctionOfX.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Abstract/Physics/Spatial/isSerializedSpatialHilbertSpace.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.fwd.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.traits.h"

namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	class FunctionOfX
	{
		static_assert(internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>, "");

		/// CTOR DTOR
	public:
		FunctionOfX(const Vector<ScalarType>& vec, const SerializedHilbertSpace& hilbertSpace);
		FunctionOfX(Vector<ScalarType>&& vec, const SerializedHilbertSpace& hilbertSpace);

		FunctionOfX(const FunctionOfX& other) = default;
        FunctionOfX(FunctionOfX&& other) = default;

		template<typename T2 = ScalarType, typename T, typename SFINAE = std::enable_if_t<std::is_same<T2, complex>::value && std::is_same<T, real>::value>>
		FunctionOfX(const FunctionOfX<T, SerializedHilbertSpace>& other);

		~FunctionOfX() = default;
		/// ASSIGNMENT
	public:
		FunctionOfX & operator=(const FunctionOfX& other);
		FunctionOfX& operator=(FunctionOfX&& other) noexcept;

		template<class T2 = ScalarType, class T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
		FunctionOfX& operator=(const FunctionOfX<T, SerializedHilbertSpace>& other);

		/// MISC
	public:
		FunctionOfX operator- () const;

		void normalize();
		real norm() const;

		FunctionOfX<real, SerializedHilbertSpace> absSquare() const; /// return |this|^2 
		FunctionOfX<real, SerializedHilbertSpace> re() const;
		FunctionOfX<real, SerializedHilbertSpace> im() const;

		Vector<ScalarType>& vec() & noexcept { return vec_; }
		const Vector<ScalarType>& vec() const& noexcept { return vec_; }
		Vector<ScalarType>&& vec() && noexcept {return std::move(vec_); }

		/// INTERNAL USE
	public:

		const auto& _rep() const { return vec_; }

		const SerializedHilbertSpace& _serializedHilbertSpace() const noexcept { return serializedHilbertSpace_; }
	private:
		Vector<ScalarType> vec_;
		SerializedHilbertSpace serializedHilbertSpace_;
	};

	template<class ScalarType, class SerializedHilbertSpace, typename = std::enable_if_t<internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>>>
	auto makeFunctionOfX(const SerializedHilbertSpace& hs, Vector<ScalarType>&& vec)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(std::move(vec), hs);
	}

	template<class ScalarType, class SerializedHilbertSpace, typename = std::enable_if_t<internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>>>
	auto makeFunctionOfX(const SerializedHilbertSpace& hs, const Vector<ScalarType>& vec)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(vec, hs);
	}

	template<class SerializedHilbertSpace>
	FunctionOfX<complex, SerializedHilbertSpace> makeFunctionOfX(const FunctionOfX<real, SerializedHilbertSpace>& re, const FunctionOfX<real, SerializedHilbertSpace>& im);
}

namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> normalize(FunctionOfX<ScalarType, SerializedHilbertSpace> f);

	template<class ScalarType, class SerializedHilbertSpace>
	real norm(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> absSquare(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> re(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> im(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class SerializedHilbertSpace>
	FunctionOfX<complex, SerializedHilbertSpace> toComplex(const FunctionOfX<real, SerializedHilbertSpace>& f);
	template<class SerializedHilbertSpace>
	FunctionOfX<complex, SerializedHilbertSpace> toComplex(const FunctionOfX<complex, SerializedHilbertSpace>& f);


	template <typename number1, typename number2, class SerializedHilbertSpace>
	complex overlap(const FunctionOfX<number1, SerializedHilbertSpace>& f1, const FunctionOfX<number2, SerializedHilbertSpace>& f2);

	template <typename number1, typename number2, class SerializedHilbertSpace>
	real fidelity(const FunctionOfX<number1, SerializedHilbertSpace>& f1, const FunctionOfX<number2, SerializedHilbertSpace>& f2);

	template <typename number1, typename number2, class SerializedHilbertSpace>
	real infidelity(const FunctionOfX<number1, SerializedHilbertSpace>& f1, const FunctionOfX<number2, SerializedHilbertSpace>& f2);

}


namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	std::ostream& operator<< (std::ostream& s, const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator+(const FunctionOfX<ScalarType1, SerializedHilbertSpace>& left, const FunctionOfX<ScalarType2, SerializedHilbertSpace>& right);

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator- (const FunctionOfX<ScalarType1, SerializedHilbertSpace>& left, const FunctionOfX<ScalarType2, SerializedHilbertSpace>& right);

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator* (const FunctionOfX<ScalarType1, SerializedHilbertSpace>& left, const FunctionOfX<ScalarType2, SerializedHilbertSpace>& right);

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator/ (const FunctionOfX<ScalarType1, SerializedHilbertSpace>& left, const FunctionOfX<ScalarType2, SerializedHilbertSpace>& right);

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType integrate(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType integrate(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f, count_t i_from, count_t i_to);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> diff(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f, count_t order = 1);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> conj(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType min(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType max(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType mean(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);
}


namespace qengine
{
	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator+ (const FunctionOfX<ScalarType1, SerializedHilbertSpace>& f, ScalarType2 a)
	{
		return makeFunctionOfX( f._serializedHilbertSpace(),f.vec() + a);
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator+ (ScalarType2 a, const FunctionOfX<ScalarType1, SerializedHilbertSpace>& f)
	{
		return makeFunctionOfX( f._serializedHilbertSpace(),a + f.vec());
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator- (const FunctionOfX<ScalarType1, SerializedHilbertSpace>& f, ScalarType2 a)
	{
		return makeFunctionOfX( f._serializedHilbertSpace(),f.vec() - a);
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator- (ScalarType2 a, const FunctionOfX<ScalarType1, SerializedHilbertSpace>& f)
	{
		return makeFunctionOfX( f._serializedHilbertSpace(),a - f.vec());
	}
	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator* (const FunctionOfX<ScalarType1, SerializedHilbertSpace>& f, ScalarType2 a)
	{
		return makeFunctionOfX( f._serializedHilbertSpace(),f.vec() * a);
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator* (ScalarType2 a, const FunctionOfX<ScalarType1, SerializedHilbertSpace>& f)
	{
		return makeFunctionOfX( f._serializedHilbertSpace(),a * f.vec());
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator/ (const FunctionOfX<ScalarType1, SerializedHilbertSpace>& f, ScalarType2 a)
	{
		return makeFunctionOfX( f._serializedHilbertSpace(),f.vec() / a);
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace, class = std::enable_if_t<internal::is_convertible_to_number_type_v<ScalarType2>>>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator/ (ScalarType2 a, const FunctionOfX<ScalarType1, SerializedHilbertSpace>& f)
	{
		return makeFunctionOfX( f._serializedHilbertSpace(),a / f.vec());
	}
}



namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> exp(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> pow(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f, count_t exponent);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> exp2(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> exp10(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> log(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> log2(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> log10(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> sqrt(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> abs(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> re(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> im(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

}
namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> sin(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> cos(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> tan(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> asin(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> acos(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> atan(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> sinh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> cosh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> tanh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> asinh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> acosh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> atanh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f);
}

//special construction functions
namespace qengine
{
	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> linspace(real lower, real upper, count_t numberOfPoints, const SerializedHilbertSpace& hilbertSpace);

	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> step(const FunctionOfX<real, SerializedHilbertSpace>& x, real stepPos);

	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> box(const FunctionOfX<real, SerializedHilbertSpace>& x, real left, real right);
	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> well(const FunctionOfX<real, SerializedHilbertSpace>& x, real left, real right);

	// s for "sigmoid" or "soft", whatever you prefer
	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> sstep(const FunctionOfX<real, SerializedHilbertSpace>& x, real center, real hardness);
	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> sbox(const FunctionOfX<real, SerializedHilbertSpace>& x, real left, real right, real hardness);
	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> swell(const FunctionOfX<real, SerializedHilbertSpace>& x, real left, real right, real hardness);
}

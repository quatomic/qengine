#pragma once

#include "src/Abstract/Physics/Spatial/isSerializedSpatialHilbertSpace.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.h"

namespace qengine
{
	namespace spatial
	{
		/*
		 * all spatial hilbertspaces have similar structure. Especially the ones with only one dimension.
		 * we add an identifier to make it easy to use the same class while still being able to
		 * distinguish different 1D hilbertspaces
		 */

		class SerializedHilbertSpace1D_Base
		{
		public:
			const real xLower;
			const real xUpper;
			const count_t dim;
			const real kinematicFactor;

			real dx() const;
			real integrationConstant() const;
			real normalization() const;
			std::vector<count_t> dimensions() const;


			SerializedHilbertSpace1D_Base& operator=(const SerializedHilbertSpace1D_Base& other) = delete;
			SerializedHilbertSpace1D_Base& operator=(SerializedHilbertSpace1D_Base&& other) noexcept = delete;
		protected:
			SerializedHilbertSpace1D_Base(real xLower, real xUpper, count_t dim, real kinematicFactor);
			SerializedHilbertSpace1D_Base(const SerializedHilbertSpace1D_Base& other) = default;
			SerializedHilbertSpace1D_Base(SerializedHilbertSpace1D_Base&& other) noexcept = default;
			~SerializedHilbertSpace1D_Base() = default;

		};

		bool operator==(const SerializedHilbertSpace1D_Base& left, const SerializedHilbertSpace1D_Base& right);
		bool operator!=(const SerializedHilbertSpace1D_Base& left, const SerializedHilbertSpace1D_Base& right);

		template<class Identifier>
		class SerializedHilbertSpace1D : public SerializedHilbertSpace1D_Base
		{
		public:
			static constexpr count_t nDimensions = 1;


			SerializedHilbertSpace1D(real xLower, real xUpper, count_t dim, real kinematicFactor)
				:SerializedHilbertSpace1D_Base{ xLower,xUpper, dim, kinematicFactor }
			{}

			// this allows converting between SerializedHilbertSpace1D with different Identifiers, by interpreting it as a Base
			explicit SerializedHilbertSpace1D(const SerializedHilbertSpace1D_Base& base):
				SerializedHilbertSpace1D_Base(base)
			{}

			FunctionOfX<real, SerializedHilbertSpace1D> makeSpatialDimension() const
			{
				return FunctionOfX<real, SerializedHilbertSpace1D>(qengine::linspace(xLower, xUpper, dim), *this);
			}

		private:
		};
	}
}

namespace qengine
{
	namespace internal
	{
		template<class Identifier>
		struct isLabelledAs_SerializedHilbertSpace<spatial::SerializedHilbertSpace1D<Identifier>> : std::true_type {};
	}
}

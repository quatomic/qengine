﻿/* COPYRIGHT
 *
 * file="FunctionOfX.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/Spatial/FunctionOfX.h"

#include "src/Utility/cpp17Replacements/constexpr_ternary.h"

#include <cassert>

#include "src/Utility/qengine_assert.h"

namespace qengine
{
	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace>::FunctionOfX(const Vector<ScalarType>& vec,
		const SerializedHilbertSpace&
		hilbertSpace) : vec_(vec), serializedHilbertSpace_(hilbertSpace)
	{
		qengine_assert(vec_.size() == serializedHilbertSpace_.dim, "vector must have dimension of SerializedHilbertSpace");
		qengine_assert(!vec_.hasNan(), "functionOfX has NaN");
	}

	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace>::FunctionOfX(Vector<ScalarType>&& vec,
		const SerializedHilbertSpace&
		hilbertSpace) : vec_(std::move(vec)), serializedHilbertSpace_(hilbertSpace)
	{
		qengine_assert(vec_.size() == serializedHilbertSpace_.dim, "vector must have dimension of SerializedHilbertSpace");
		qengine_assert(!vec_.hasNan(), "functionOfX has NaN");
	}

	template<class ScalarType, class SerializedHilbertSpace>
	template <typename T2, typename T, typename>
	FunctionOfX<ScalarType, SerializedHilbertSpace>::FunctionOfX(const FunctionOfX<T, SerializedHilbertSpace>& other) : FunctionOfX(Vector<complex>(other.vec()), other._serializedHilbertSpace())
	{
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace>& FunctionOfX<ScalarType, SerializedHilbertSpace>::operator=(const FunctionOfX& other)
	{
		if (&other != this)
		{
			qengine_assert(other._serializedHilbertSpace() == this->_serializedHilbertSpace(), "trying to assign FunctionOfX with different hilbertSpaces!");
			vec_ = other.vec_;
		}
		return *this;
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace>& FunctionOfX<ScalarType, SerializedHilbertSpace>::operator=(FunctionOfX&& other) noexcept
	{
		if (&other != this)
		{
            assert(other._serializedHilbertSpace() == this->_serializedHilbertSpace());
			vec_ = std::move(other.vec_);
		}
		return *this;
	}

	template<class ScalarType, class SerializedHilbertSpace>
	template <typename T2, typename T>
	FunctionOfX<ScalarType, SerializedHilbertSpace>& FunctionOfX<ScalarType, SerializedHilbertSpace>::operator=(const FunctionOfX<T, SerializedHilbertSpace>& other)
	{
		qengine_assert(other._serializedHilbertSpace() == this->_serializedHilbertSpace(), "trying to assign FunctionOfX with different dx!");

		vec_ = other.vec();
		return *this;
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> FunctionOfX<ScalarType, SerializedHilbertSpace>::operator-() const
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(-vec_, _serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	void FunctionOfX<ScalarType, SerializedHilbertSpace>::normalize()
	{
		vec_ *= serializedHilbertSpace_.normalization() / vec_.norm();
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> FunctionOfX<ScalarType, SerializedHilbertSpace>::absSquare() const
	{
		return FunctionOfX<real, SerializedHilbertSpace>(vec_.absSquare(), serializedHilbertSpace_);
	}

	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> FunctionOfX<ScalarType, SerializedHilbertSpace>::re() const
	{
		const auto& it = *this;
		return repl17::constexpr_ternary(std::is_same<ScalarType, real>{},
			[&it](auto _) {return _(it); },
			[&it](auto _) {return FunctionOfX<real, SerializedHilbertSpace>(_(it.vec().re()), it._serializedHilbertSpace()); }
		);
	}

	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> FunctionOfX<ScalarType, SerializedHilbertSpace>::im() const
	{
		const auto& it = *this;

		return repl17::constexpr_ternary(std::is_same<ScalarType, real>{},
			[&it](auto _) {return _(0.0*it); },
			[&it](auto _) {return FunctionOfX<real, SerializedHilbertSpace>(it.vec().im(), _(it._serializedHilbertSpace())); }
		);
	}

	template <class SerializedHilbertSpace>
	FunctionOfX<complex, SerializedHilbertSpace> makeFunctionOfX(const FunctionOfX<real, SerializedHilbertSpace>& re,
		const FunctionOfX<real, SerializedHilbertSpace>& im)
	{
		qengine_assert(re._serializedHilbertSpace() == im._serializedHilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<complex, SerializedHilbertSpace>{Vector<complex>(re.vec(), im.vec()), re._serializedHilbertSpace()};
	}


	template<class ScalarType, class SerializedHilbertSpace>
	real FunctionOfX<ScalarType, SerializedHilbertSpace>::norm() const
	{
		return vec_.norm() / serializedHilbertSpace_.normalization();
	}


}

namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> normalize(FunctionOfX<ScalarType, SerializedHilbertSpace> f)
	{
		f.normalize();
		return f;
	}

	template<class ScalarType, class SerializedHilbertSpace>
	real norm(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return f.norm();
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> absSquare(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return f.absSquare();
	}

	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> re(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return f.re();
	}

	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> im(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return f.im();
	}

	template<class SerializedHilbertSpace>
	FunctionOfX<complex, SerializedHilbertSpace> toComplex(const FunctionOfX<real, SerializedHilbertSpace>& f)
	{
		return f;
	}
	template<class SerializedHilbertSpace>
	FunctionOfX<complex, SerializedHilbertSpace> toComplex(const FunctionOfX<complex, SerializedHilbertSpace>& f)
	{
		return f;
	}


	template <typename number1, typename number2, class SerializedHilbertSpace>
	complex overlap(const FunctionOfX<number1, SerializedHilbertSpace>& f1, const FunctionOfX<number2, SerializedHilbertSpace>& f2)
	{
		return integrate(conj(f1)*f2);
	}

	template <typename number1, typename number2, class SerializedHilbertSpace>
	real fidelity(const FunctionOfX<number1, SerializedHilbertSpace>& f1, const FunctionOfX<number2, SerializedHilbertSpace>& f2)
	{
		return std::norm(overlap(f1, f2));
	}

	template <typename number1, typename number2, class SerializedHilbertSpace>
	real infidelity(const FunctionOfX<number1, SerializedHilbertSpace>& f1, const FunctionOfX<number2, SerializedHilbertSpace>& f2)
	{
		return 1.0 - fidelity(f1, f2);
	}

}
namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	std::ostream& operator<<(std::ostream& s, const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		s << f.vec();
		return s;
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator+(const FunctionOfX<ScalarType1, SerializedHilbertSpace>& left, const FunctionOfX<ScalarType2, SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace>(left.vec() + right.vec(), left._serializedHilbertSpace());
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator-(const FunctionOfX<ScalarType1, SerializedHilbertSpace>& left, const FunctionOfX<ScalarType2, SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace>(left.vec() - right.vec(), left._serializedHilbertSpace());
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator*(const FunctionOfX<ScalarType1, SerializedHilbertSpace>& left, const FunctionOfX<ScalarType2, SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace>(left.vec() * right.vec(), left._serializedHilbertSpace());
	}

	template<class ScalarType1, class ScalarType2, class SerializedHilbertSpace>
	FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator/(const FunctionOfX<ScalarType1, SerializedHilbertSpace>& left, const FunctionOfX<ScalarType2, SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "trying to operate with different dxs!");
		return FunctionOfX<internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace>(left.vec() / right.vec(), left._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType integrate(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return sum(f.vec())*f._serializedHilbertSpace().integrationConstant();
	}

	template <class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> conj(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return repl17::constexpr_ternary(std::is_same<ScalarType, real>{},
			[&f](auto _) {return _(f); },
			[&f](auto _) {return _(FunctionOfX<complex, SerializedHilbertSpace>(conj(f.vec()), f._serializedHilbertSpace())); }
		);
	}

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType min(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return min(f.vec());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType max(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return max(f.vec());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	ScalarType mean(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return mean(f.vec());
	}
}


namespace qengine
{

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> exp(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(exp(f.vec()), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> pow(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f, count_t exponent)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(pow(f.vec(), exponent), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> exp2(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(exp2(f.vec()), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> exp10(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(exp10(f.vec()), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> log(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(log(f.vec()), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> log2(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(log2(f.vec()), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> log10(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(log10(f.vec()), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> sqrt(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(sqrt(f.vec()), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> abs(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<real, SerializedHilbertSpace>(abs(f.vec()), f._serializedHilbertSpace());
	}
}


namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> sin(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(sin(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> cos(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(cos(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> tan(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(tan(f.vec()), f._serializedHilbertSpace());
	}

	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> asin(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(asin(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> acos(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(acos(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> atan(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(atan(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> sinh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(sinh(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> cosh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(cosh(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> tanh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(tanh(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> asinh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(asinh(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> acosh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(acosh(f.vec()), f._serializedHilbertSpace());
	}
	template<class ScalarType, class SerializedHilbertSpace>
	FunctionOfX<ScalarType, SerializedHilbertSpace> atanh(const FunctionOfX<ScalarType, SerializedHilbertSpace>& f)
	{
		return FunctionOfX<ScalarType, SerializedHilbertSpace>(atanh(f.vec()), f._serializedHilbertSpace());
	}

}

namespace qengine
{
	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> linspace(const real lower, const real upper, const count_t numberOfPoints, const SerializedHilbertSpace& hilbertSpace)
	{
		return FunctionOfX<real, SerializedHilbertSpace>(qengine::linspace(lower, upper, numberOfPoints), hilbertSpace);
	}

	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> step(const FunctionOfX<real, SerializedHilbertSpace>& x, const real stepPos)
	{
		return  FunctionOfX<real, SerializedHilbertSpace>(step(x.vec(), stepPos), x._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> box(const FunctionOfX<real, SerializedHilbertSpace>& x, const real left, const real right)
	{
		return  FunctionOfX<real, SerializedHilbertSpace>(box(x.vec(), left, right), x._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> well(const FunctionOfX<real, SerializedHilbertSpace>& x, const real left, const real right)
	{
		return  FunctionOfX<real, SerializedHilbertSpace>(well(x.vec(), left, right), x._serializedHilbertSpace());
	}

	// s for "sigmoid" or "soft", whatever you prefer
	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> sstep(const FunctionOfX<real, SerializedHilbertSpace>& x, const real center, const real hardness)
	{
		return  FunctionOfX<real, SerializedHilbertSpace>(sstep(x.vec(), center, hardness), x._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> sbox(const FunctionOfX<real, SerializedHilbertSpace>& x, const real left, const real right, const real hardness)
	{
		return  FunctionOfX<real, SerializedHilbertSpace>(sbox(x.vec(), left, right, hardness), x._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	FunctionOfX<real, SerializedHilbertSpace> swell(const FunctionOfX<real, SerializedHilbertSpace>& x, const real left, const real right, const real hardness)
	{
		return  FunctionOfX<real, SerializedHilbertSpace>(swell(x.vec(), left, right, hardness), x._serializedHilbertSpace());
	}
}

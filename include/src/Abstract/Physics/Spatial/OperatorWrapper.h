﻿/* COPYRIGHT
 *
 * file="OperatorWrapper.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/isSqMat.h"

#include "src/Abstract/Physics/Spatial/isSerializedSpatialHilbertSpace.h"
#include "src/Abstract/Physics/Spatial/Spectrum.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.h"

namespace qengine
{
	namespace spatial
	{
		template<class SerializedHilbertSpace>
		class IOperatorWrapper
		{
			static_assert(internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>, "");
		public:
			virtual ~IOperatorWrapper() = default;

			virtual void applyInPlace(FunctionOfX<complex, SerializedHilbertSpace>& wf) const = 0;
		};

		template<class SerializedHilbertSpace>
		FunctionOfX<complex, SerializedHilbertSpace> operator*(const IOperatorWrapper<SerializedHilbertSpace>& op, FunctionOfX<complex, SerializedHilbertSpace> psi)
		{
			op.applyInPlace(psi);
			return psi;
		}
	}

	namespace internal
	{
		template<class SerializedHilbertSpace>
		struct isLabelledAs_Operator<spatial::IOperatorWrapper<SerializedHilbertSpace>> : std::true_type{};

		template<class SerializedHilbertSpace>
		struct isLabelledAs_Linear<spatial::IOperatorWrapper<SerializedHilbertSpace>> : std::true_type{};
	}
}


namespace qengine
{
	namespace spatial
	{
		template<class SerializedHilbertSpace, template<class> class MatType, class ScalarType>
		class OperatorWrapper : public IOperatorWrapper<SerializedHilbertSpace>
		{
			static_assert(internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>, "");
			static_assert(internal::is_SqMat_v<MatType>, "");

		public:
			OperatorWrapper(SerializedHilbertSpace hilbertSpace, MatType<ScalarType> mat) :
				IOperatorWrapper<SerializedHilbertSpace>(),
				serializedHilbertSpace_(std::move(hilbertSpace)),
				mat_(std::move(mat))
			{}

			void applyInPlace(FunctionOfX<complex, SerializedHilbertSpace>& wf) const override
			{
				mat_.multiplyInPlace(wf.vec());
			}

			template<class... Ts>
			Spectrum<complex, SerializedHilbertSpace> makeSpectrum(count_t largestEigenstate, Ts... ts) const
			{
				return { internal::getSpectrum(mat_, largestEigenstate, serializedHilbertSpace_.normalization(), std::move(ts)...), serializedHilbertSpace_ };
			}

		private:
			SerializedHilbertSpace serializedHilbertSpace_;
			MatType<ScalarType> mat_;
		};
		template<class SerializedHilbertSpace, template<class> class MatType, class ScalarType>
		FunctionOfX<complex, SerializedHilbertSpace> operator*(const OperatorWrapper<SerializedHilbertSpace, MatType, ScalarType>& op, FunctionOfX<complex, SerializedHilbertSpace> psi)
		{
			op.applyInPlace(psi);
			return psi;
		}
	}


	namespace internal
	{
		template<class SerializedHilbertSpace, template<class> class MatType, class ScalarType>
		struct isLabelledAs_Operator<spatial::OperatorWrapper<SerializedHilbertSpace, MatType, ScalarType>> : std::true_type {};

		template<class SerializedHilbertSpace, template<class> class MatType, class ScalarType>
		struct isLabelledAs_Linear<spatial::OperatorWrapper<SerializedHilbertSpace, MatType, ScalarType>> : std::true_type {};
	}
}

namespace qengine
{
	namespace spatial
	{
		template<class SerializedHilbertSpace, class ScalarType>
		auto makeOperatorWrapper(FunctionOfX<ScalarType, SerializedHilbertSpace> diag)
		{
			return OperatorWrapper<SerializedHilbertSpace, internal::SqDiagMat, ScalarType>(diag._serializedHilbertSpace(), internal::SqDiagMat<ScalarType>(diag.vec()));
		}

		template<class SerializedHilbertSpace, template<class> class MatType, class ScalarType, class = std::enable_if_t<internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>>>
		auto makeOperatorWrapper(SerializedHilbertSpace hs, MatType<ScalarType> mat)
		{
			return OperatorWrapper<SerializedHilbertSpace, MatType, ScalarType>(std::move(hs), std::move(mat));
		}

		template<class SerializedHilbertSpace, template<class> class MatType, class ScalarType, class = std::enable_if_t<!internal::isSerializedSpatialHilbertSpace_v<SerializedHilbertSpace>>>
		auto makeOperatorWrapper(const SerializedHilbertSpace& hs, MatType<ScalarType> mat)
		{
			return makeOperatorWrapper(hs._serializedHilbertSpace(), std::move(mat));
		}
	}
}

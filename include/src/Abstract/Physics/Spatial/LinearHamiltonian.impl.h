﻿/* COPYRIGHT
 *
 * file="LinearHamiltonian.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/Spatial/LinearHamiltonian.h"

#include "src/Abstract/Physics/Spatial/Spectrum.h"
#include "src/Utility/qengine_assert.h"

namespace qengine
{
	template<class SerializedHilbertSpace, template<class> class Rep>
	template <class Potential, class Representation, typename >
	LinearHamiltonian<SerializedHilbertSpace, Rep>::LinearHamiltonian(const SerializedHilbertSpace& hilbertSpace, Representation&& representation, Potential&& potential) :
		potential_(std::forward<Potential>(potential)),
		hamiltonianMatrix_(representation),
		serializedHilbertSpace_(hilbertSpace)
	{
		qengine_assert(!hamiltonianMatrix_.hasNan(), "Hamiltonian has NaN");
	}

	template<class SerializedHilbertSpace, template<class> class Rep>
	template <typename ScalarType>
	void LinearHamiltonian<SerializedHilbertSpace, Rep>::applyInPlace(FunctionOfX<ScalarType, SerializedHilbertSpace>& f) const
	{
		qengine_assert(f._serializedHilbertSpace() == serializedHilbertSpace_, "wrong hilbertspace!");
		hamiltonianMatrix_.multiplyInPlace(f.vec());
	}

	template<class SerializedHilbertSpace, template<class> class Rep>
	spatial::Spectrum<real, SerializedHilbertSpace> LinearHamiltonian<SerializedHilbertSpace, Rep>::makeSpectrum(const count_t largestEigenvalue) const
	{
		return spatial::Spectrum<real, SerializedHilbertSpace>(internal::getSpectrum_herm(hamiltonianMatrix_, largestEigenvalue, serializedHilbertSpace_.normalization()), serializedHilbertSpace_);
	}

	template<class SerializedHilbertSpace, template<class> class Rep>
	std::ostream& operator<<(std::ostream& s, const LinearHamiltonian<SerializedHilbertSpace, Rep>& H)
	{
		s << H._hamiltonianMatrix();
		return s;
	}

}

﻿/* COPYRIGHT
 *
 * file="PotentialSum.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

#include "src/Abstract/Physics/General/isOperator.h"

#include "src/Abstract/Physics/Spatial/isPotential.h"

namespace qengine
{
	template<class... Vs>
	class PotentialSum;
}

namespace qengine
{
	namespace spatial
	{
		template<class... Vs>
		struct isLabelledAs_Potential<PotentialSum<Vs...>> : std::true_type{};
	}
}

namespace qengine
{
	namespace internal
	{
		template<class... Vs>
		struct isLabelledAs_Operator<PotentialSum<Vs...>> : std::true_type{};
	}
}

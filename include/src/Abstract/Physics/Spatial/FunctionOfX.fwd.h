﻿/* COPYRIGHT
 *
 * file="FunctionOfX.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"

namespace qengine
{
	template<class ScalarType, class SerializedHilbertSpace>
	class FunctionOfX;
}

namespace qengine
{
	namespace internal
	{
		// this is the general case for Functions of x, but it may be further specialized by e.g. functions of x that live on helper-hilbertspaces
		template<class ScalarType, class SerializedHilbertSpace>
		struct extract_serialized_hilbertspace<FunctionOfX<ScalarType, SerializedHilbertSpace>>
		{
			using type = SerializedHilbertSpace;
		};
	}
}

﻿/* COPYRIGHT
 *
 * file="FunctionOfX.traits.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"
#include "src/Abstract/Physics/General/isState.h"

#include "src/Abstract/Physics/Spatial/isPotential.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.fwd.h"


namespace qengine
{
	namespace spatial
	{
		template<class SerializedHilbertSpace>
		struct isLabelledAs_Potential<FunctionOfX<real, SerializedHilbertSpace>> : std::true_type {};
	}
}

namespace qengine
{
	namespace internal
	{
		template<class ScalarType, class SerializedHilbertSpace>
		struct isLabelledAs_State<FunctionOfX<ScalarType, SerializedHilbertSpace>, SerializedHilbertSpace> : std::true_type{};

		template<class ScalarType, class SerializedHilbertSpace>
		struct isLabelledAs_Operator<FunctionOfX<ScalarType, SerializedHilbertSpace>> : std::true_type{};

		template<class ScalarType, class SerializedHilbertSpace>
		struct isLabelledAs_Linear<FunctionOfX<ScalarType, SerializedHilbertSpace>> : std::true_type{};

		template<class SerializedHilbertSpace>
		struct isLabelledAs_GuaranteedHermitian<FunctionOfX<real, SerializedHilbertSpace>> : std::true_type{};
	}
}

﻿/* COPYRIGHT
 *
 * file="PotentialSum.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>

#include "src/Abstract/Physics/Spatial/PotentialSum.fwd.h"

#include "src/NumberTypes.h"
#include "src/Utility/TypeList.h"

namespace qengine
{
	template<class... Vs>
	class PotentialSum
	{
		static_assert(repl17::check_all_true(spatial::isPotential_v<std17::remove_cvref_t<Vs>>...), "all inputs must be potentials!");
		static_assert(internal::containsNoDuplicates(internal::TypeList<Vs...>{}), "types must be unique inside potentialSum");
		static_assert(repl17::check_all_true(std::is_same<Vs, std17::remove_cvref_t<Vs>>::value...), "potential types must not be reference types!");
	public:

		PotentialSum(std::tuple<Vs...>&& t) :potentials_(std::move(t)) {}
		PotentialSum(const std::tuple<Vs...>& t) :potentials_(std::move(t)) {}


		PotentialSum(const PotentialSum& other) = default;
		PotentialSum(PotentialSum&& other) noexcept : potentials_(std::move(other.potentials_)){};
		PotentialSum& operator=(const PotentialSum& other) = default;
		PotentialSum& operator=(PotentialSum&& other) noexcept = default;

		~PotentialSum() = default;

		std::tuple<Vs...>& _potentials() { return potentials_; }
		const std::tuple<Vs...>& _potentials() const { return potentials_; }
	private:
		std::tuple<Vs...> potentials_;
	};


	template<size_t I, class... Ts>
	auto& get(PotentialSum<Ts...>& V) { return std::get<I>(V._potentials()); }
	template<size_t I, class... Ts>
	const auto& get(const PotentialSum<Ts...>& V) { return std::get<I>(V._potentials()); }

	template<class T, class... Ts>
	T& get(PotentialSum<Ts...>& V)
	{
		static_assert(internal::isInList<T, Ts...>(), "requested type is not in list");
		return std::get<internal::findFirstIndex<T, Ts...>()>(V._potentials());
	}
	template<class T, class... Ts>
	const T& get(const PotentialSum<Ts...>& V)
	{
		static_assert(internal::isInList<T, Ts...>(), "requested type is not in list");
		return std::get<internal::findFirstIndex<T, Ts...>()>(V._potentials());
	}

	template<class... Ts>
	PotentialSum<std17::remove_cvref_t<Ts>...> makePotentialSum(Ts&&... vs)
	{
		static_assert(repl17::check_all_true(spatial::isPotential_v<std17::remove_cvref_t<Ts>>...), "all inputs must be potentials!");
		static_assert(internal::containsNoDuplicates(internal::TypeList<Ts...>{}), "types must be unique!");
		return PotentialSum<std17::remove_cvref_t<Ts>...>(std::make_tuple(std::forward<Ts>(vs)...));
	}

	template<class... Vs, class V, class SFINAE = std::enable_if_t<internal::isInList<V, Vs...>()>>
	PotentialSum<Vs...> operator+ (PotentialSum<Vs...> left, const V& right)
	{
		//std::get<internal::findFirstIndex<V, Vs...>()>(left._potentials()) = std::get<internal::findFirstIndex<V, Vs...>()>(left._potentials()) + right;
		get<V>(left) = get<V>(left) + right;
		return left;
	}

	template<class... Vs, class V, class SFINAE = std::enable_if_t<internal::isInList<V, Vs...>()>>
	PotentialSum<Vs...> operator+ (const V& left, PotentialSum<Vs...> right)
	{
		return right + left;
	}

	namespace internal
	{

		template<size_t... Is, class... Vs, class OpFunc>
		auto operator_impl(const PotentialSum<Vs...>& left, const PotentialSum<Vs...>& right, std::index_sequence<Is...>, OpFunc opFunc)
		{
			return makePotentialSum(opFunc(get<Is>(left), get<Is>(right))...);
		}
	}

	template<class... Vs>
	PotentialSum<Vs...> operator+ (const PotentialSum<Vs...>& left, const PotentialSum<Vs...>& right)
	{
		auto opFunc = [](const auto& left, const auto& right) {return left + right; };
		return internal::operator_impl(left, right, std::make_index_sequence<sizeof...(Vs)>{}, opFunc);
	}
}

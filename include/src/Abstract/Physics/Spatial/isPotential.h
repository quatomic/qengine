﻿/* COPYRIGHT
 *
 * file="isPotential.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Abstract/Physics/General/isOperator.h"

namespace qengine
{
	namespace spatial
	{
		template<class V, class SFINAE = void> struct isLabelledAs_Potential: public std::false_type {};
		template<class T> constexpr bool isLabelledAs_Potential_v = isLabelledAs_Potential<T>::value;
	}
}

namespace qengine
{
	namespace spatial
	{
		template<class V, class SFINAE = void> struct isPotential : public std::false_type {};
		template<class V> constexpr bool isPotential_v = isPotential<V>::value;
	}
}

namespace qengine
{
	namespace spatial
	{
		template<class V>
		struct isPotential<V, std::enable_if_t<
			isLabelledAs_Potential_v<V> 
		>>: std::true_type{};
	}
}

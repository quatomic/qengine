﻿/* COPYRIGHT
 *
 * file="SerializedHilbertSpaceND.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>
#include <algorithm>

#include "src/NumberTypes.h"
#include "src/Utility/TypeList.h"
#include "src/Utility/qengine_assert.h"

#include "src/Utility/cpp17Replacements/constexpr_ternary.h"
#include "src/Utility/cpp17Replacements/check_all_true.h"
#include "src/Utility/std17/bool_constant.h"

#include "src/Abstract/Physics/Spatial/SerializedHilbertSpace1D.h"

namespace qengine
{
	namespace spatial
	{
		template<count_t FullDim, count_t Index>
		struct SubSpaceIdentifier {};

		template<count_t FullDim, count_t Index>
		using SubSpace = spatial::SerializedHilbertSpace1D<SubSpaceIdentifier<FullDim, Index>>;

		template<count_t Dim, count_t... Indices>
		class SerializedHilbertSpaceND
		{
		public:
			SerializedHilbertSpaceND(std::tuple<SubSpace<Dim, Indices>...>&& subspaces) :
				dim(dimProduct(subspaces)),
				subspaces_(std::move(subspaces)),
				integrationConstant_(integrationConstantsProduct(subspaces)),
				normalization_(normalizationProduct(subspaces)),
				dimensions_({ std::get<findTupleIndex<Indices>()>(subspaces).dim... })
			{
			}

			const count_t dim;

			auto integrationConstant() const { return integrationConstant_; }
			auto normalization() const { return normalization_; }
			auto dimensions() const { return dimensions_; }

			static constexpr count_t nDimensions = sizeof...(Indices);

			template<count_t Index>
			const SubSpace<Dim, Index>& get() const
			{
				return std::get<findTupleIndex<Index>()>(subspaces_);
			}

			count_t linearIndex(const std::vector<count_t>& indices) const
			{
				qengine_assert(indices.size() == sizeof...(Indices), "wrong number of indices!");

				auto it = indices.rbegin();
				auto dimIt = dimensions_.rbegin();

				count_t index = *it;
				++it;
				count_t dimProd = 1;

				for (; it != indices.rend(); ++it, ++dimIt)
				{
					dimProd *= *dimIt;
					index += dimProd * (*it);
				}
				return index;
			}

		private:
			std::tuple<SubSpace<Dim, Indices>...> subspaces_;

			const real integrationConstant_;
			const real normalization_;
			const std::vector<count_t> dimensions_;

			template<count_t Index> constexpr static std::size_t findTupleIndex()
			{
				return qengine::internal::findFirstIndex<SubSpace<Dim, Index>>(qengine::internal::TypeList<SubSpace<Dim, Indices>...>());
			}

			template<class Access>
			constexpr static auto productOverSubSpaces(const std::tuple<SubSpace<Dim, Indices>...>& subspaces, Access accessFunction)
			{
				decltype(accessFunction(std::get<0>(subspaces))) product = 1; // we can assume that there are at least 2 entries in subspaces

				// The following line is a neat trick to multiply whatever accessfunction returns onto the product for each subspace. 
				// basically, it can be expressed as
				// for(auto subspace: subspaces_)
				// {
				//     product *= accessFunction(subspace);
				// }
				// But we can't do that due to how parameter packs work
				(void)std::initializer_list<int>{ (product *= accessFunction(std::get<findTupleIndex<Indices>()>(subspaces)), 0)... };
				return product;
			}

			constexpr static count_t dimProduct(const std::tuple<SubSpace<Dim, Indices>...>& subspaces)
			{
				return productOverSubSpaces(subspaces, [](const auto& space) {return space.dim; });
			}

			constexpr static real normalizationProduct(const std::tuple<SubSpace<Dim, Indices>...>& subspaces)
			{
				return productOverSubSpaces(subspaces, [](const auto& space) {return space.normalization(); });
			}
			constexpr static real integrationConstantsProduct(const std::tuple<SubSpace<Dim, Indices>...>& subspaces)
			{
				return productOverSubSpaces(subspaces, [](const auto& space) {return space.integrationConstant(); });
			}
		};

		template<count_t Dim, count_t... Indices>
		bool operator==(const SerializedHilbertSpaceND<Dim, Indices...>& left, const SerializedHilbertSpaceND<Dim, Indices...>& right)
		{
			return repl17::check_all_true((left.template get<Indices>() == right.template get<Indices>())...);
		}

		template<count_t Dim, count_t... Indices>
		bool operator!=(const SerializedHilbertSpaceND<Dim, Indices...>& left, const SerializedHilbertSpaceND<Dim, Indices...>& right)
		{
			return !(left == right);
		}

		template<count_t Dim, count_t Index1, count_t Index2>
		auto tensor(const spatial::SubSpace<Dim, Index1>& left, const spatial::SubSpace<Dim, Index2>& right)
		{
			return repl17::constexpr_ternary(std17::bool_constant<(Index1 < Index2)>{},
				[&](auto _)
			{
				return SerializedHilbertSpaceND<Dim, Index1, Index2>(std::make_tuple(_(left), _(right)));
			},
				[&](auto _)
			{
				return SerializedHilbertSpaceND<Dim, Index2, Index1>(std::make_tuple(_(right), _(left)));
			});
		}


	}
}


namespace qengine
{
	namespace internal
	{
		template<count_t Dim, count_t... Indices>
		struct isLabelledAs_SerializedHilbertSpace<spatial::SerializedHilbertSpaceND<Dim, Indices...>> : public std::true_type{};
	}
}


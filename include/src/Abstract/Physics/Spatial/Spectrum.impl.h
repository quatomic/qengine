﻿/* COPYRIGHT
 *
 * file="Spectrum.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/Spatial/Spectrum.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.h"

namespace qengine
{
	namespace spatial {
		template <class ScalarType, class SerializedHilbertSpace>
		template <class T1>
		Spectrum<ScalarType, SerializedHilbertSpace>::Spectrum(T1&& t1, const SerializedHilbertSpace hilbertSpace) :
			internal::Spectrum<ScalarType>(std::forward<T1>(t1), hilbertSpace.normalization),
			serializedHilbertSpace_(hilbertSpace)
		{
		}

		template <class ScalarType, class SerializedHilbertSpace>
		Spectrum<ScalarType, SerializedHilbertSpace>::Spectrum(internal::Spectrum<ScalarType>&& t1, const SerializedHilbertSpace hilbertSpace) :
			internal::Spectrum<ScalarType>(std::move(t1)),
			serializedHilbertSpace_(hilbertSpace)
		{
		}

		template <class ScalarType, class SerializedHilbertSpace>
		Spectrum<ScalarType, SerializedHilbertSpace>::Spectrum(const internal::Spectrum<ScalarType>& t1, const SerializedHilbertSpace hilbertSpace) :
			internal::Spectrum<ScalarType>(t1),
			serializedHilbertSpace_(hilbertSpace)
		{
		}

		template <class ScalarType, class SerializedHilbertSpace>
		FunctionOfX<complex, SerializedHilbertSpace> Spectrum<ScalarType, SerializedHilbertSpace>::eigenFunction(count_t i) const
		{
			return FunctionOfX<complex, SerializedHilbertSpace>(internal::Spectrum<ScalarType>::eigenvector(i), serializedHilbertSpace_);
		}

		template <class ScalarType, class SerializedHilbertSpace>
		FunctionOfX<complex, SerializedHilbertSpace> Spectrum<ScalarType, SerializedHilbertSpace>::makeLinearCombination(const CVec& factors) const
		{
			return FunctionOfX<complex, SerializedHilbertSpace>(internal::Spectrum<ScalarType>::makeLinearCombination(factors), serializedHilbertSpace_);
		}
	}
}

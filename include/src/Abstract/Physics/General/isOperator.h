﻿/* COPYRIGHT
 *
 * file="isOperator.h"
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

#include "src/Abstract/Physics/General/isState.h"
#include "extract_serialized_hilbertspace.h"

namespace qengine
{
	namespace internal
	{
		template<class O, class SFINAE = void> struct isLabelledAs_Operator : std::false_type {};
		template<class O> constexpr bool isLabelledAs_Operator_v = isLabelledAs_Operator<O>::value;
	}
}

namespace qengine
{
	namespace internal
	{

		template<class O, class SFINAE = void> struct isLabelledAs_Linear : std::false_type {};
		template<class O> constexpr bool isLabelledAs_Linear_v = isLabelledAs_Linear<O>::value;

		template<class O>
		struct isLinear : isLabelledAs_Linear<O> {};

		template<class O> constexpr bool isLinear_v = isLinear<O>::value;
	}
}

namespace qengine
{
	namespace internal
	{
		template<class O, class SFINAE = void> struct isLabelledAs_GuaranteedHermitian : std::false_type {};
		template<class O> constexpr bool isLabelledAs_GuaranteedHermitian_v = isLabelledAs_GuaranteedHermitian<O>::value;
	}
}

namespace qengine
{
	namespace internal
	{
		template<class O, class Psi, class SFINAE = void>
		struct isCompatibleOperator : std::false_type {};

		template<class O, class Psi> constexpr bool isCompatibleOperator_v = isCompatibleOperator<O, Psi>::value;

		template<class O, class Psi>
		struct isCompatibleOperator<O, Psi, std::enable_if_t<
			isLabelledAs_Operator_v<O> &&
			isState_v<Psi, extract_serialized_hilbertspace_t<Psi>> &&
			isState_v<decltype(std::declval<O>() * std::declval<Psi>()), extract_serialized_hilbertspace_t<Psi>>  // the operator must fulfill that O*psi -> psi, so it must take a state of the hilbertspace and translate it into another state in the same hilbertspace
			>> : std::true_type{};
	}
}

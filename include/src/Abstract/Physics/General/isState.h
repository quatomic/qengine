﻿/* COPYRIGHT
 *
 * file="isState.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

#include "src/NumberTypes.h"

namespace qengine
{
	namespace internal
	{
		template<class Psi, class SerializedHilbertSpace, class SFINAE = void> struct isLabelledAs_State : std::false_type {};
		template<class Psi, class SerializedHilbertSpace> constexpr bool isLabelledAs_State_v = isLabelledAs_State<Psi, SerializedHilbertSpace>::value;

		template<class Psi, class SerializedHilbertSpace, class SFINAE = void>
		struct isState : std::false_type {};

		template<class Psi, class SerializedHilbertSpace> constexpr bool isState_v = isState<Psi, SerializedHilbertSpace>::value;
	}
}

namespace qengine
{
	namespace internal
	{
		// A state has to be labelled as a state AND has to have an inner product defined, in the form of the 'overlap' function
		template<class Psi, class SerializedHilbertSpace>
		struct isState<Psi, SerializedHilbertSpace, std::enable_if_t<
			isLabelledAs_State_v<Psi, SerializedHilbertSpace> &&
			std::is_same<complex, decltype(overlap(std::declval<Psi>(), std::declval<Psi>()))>::value
		,void>> : std::true_type{};
	}
}

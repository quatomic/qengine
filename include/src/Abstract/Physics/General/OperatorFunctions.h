﻿/* COPYRIGHT
 *
 * file="OperatorFunctions.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/qengine_assert.h"

#include "src/Abstract/Physics/General/isOperator.h"

namespace qengine
{
	template<class O,class Psi, class = std::enable_if_t<internal::isCompatibleOperator_v<O, Psi>>>
	qengine::complex cexpectationValue(const O& op, const Psi& psi)
	{
//        qengine_assert(std::abs(psi.norm() - 1) < 1e-10, "state must be normalized to calculate expectationValue");
		return overlap(psi, op*psi);
	}

	template<class O, class Psi, class = std::enable_if_t<internal::isCompatibleOperator_v<O, Psi> && internal::isLabelledAs_Linear_v<O>>>
	complex cVariance(const O& op, const Psi& psi)
	{
//        qengine_assert(std::abs(psi.norm() - 1) < 1e-10, "state must be normalized to calculate variance");
		auto expect = cexpectationValue(op, psi);
		return overlap(psi, op*(op*psi)) - expect * expect; // <op>^2-<op^2>. There is no guarantee that the op can be multiplied with itself!
	}

	template<class O, class Psi, class = std::enable_if_t<internal::isCompatibleOperator_v<O, Psi>>>
	real expectationValue(const O& op, const Psi& psi)
	{
		auto c = cexpectationValue(op, psi);
        qengine_assert(std::abs(c.imag()) < 1e-10, "expectationValue has nonzero imaginary part!");
		return c.real();
	}

	template<class O, class Psi, class = std::enable_if_t<internal::isCompatibleOperator_v<O, Psi>>>
	real variance(const O& op, const Psi& psi)
	{
		auto c = cVariance(op, psi);
        qengine_assert(std::abs(c.imag()) < 1e-10, "variance has nonzero imaginary part!");
		return c.real();
	}

	template<class O, class Psi, class = std::enable_if_t<internal::isCompatibleOperator_v<O, Psi>>>
	real standardDeviation(const O& op, const Psi& psi)
	{
		auto var = variance(op, psi);
		if (var < 0.0 && var > -1e-10) return 0.0; // for when calculations give very small negative numbers that are basically zero

		qengine_assert(var >= 0.0, "variance has is negative, cannot calculate standard deviation!");
		return std::sqrt(var);
	}
}

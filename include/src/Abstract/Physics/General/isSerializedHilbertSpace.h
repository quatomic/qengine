#pragma once
#include <type_traits>

#include "src/Utility/has_function.h"
#include "src/Utility/isEqualityComparable.h"

namespace qengine
{
	namespace internal
	{
		MAKE_HAS_VAR(hasVar_dim, dim);

		template<class T, class SFINAE = void> struct isLabelledAs_SerializedHilbertSpace : std::false_type {};

		template<class T> constexpr bool isLabelledAs_SerializedHilbertSpace_v = isLabelledAs_SerializedHilbertSpace<T>::value;

		// serialized hilbertspaces are used to ensure physics only acts within the same hilbertspace
		template<class T>
		struct isSerializedHilbertSpace
		{
			constexpr static bool value =
				isLabelledAs_SerializedHilbertSpace_v<T> &&
				isEqualityComparable_v<T> &&
				isInequalityComparable_v<T> &&
				hasVar_dim_v<T, const count_t>
				;
		};

		template<class T> constexpr bool isSerializedHilbertSpace_v = isSerializedHilbertSpace<T>::value;

	}
}

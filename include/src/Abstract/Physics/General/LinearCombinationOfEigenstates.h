﻿/* COPYRIGHT
 *
 * file="LinearCombinationOfEigenstates.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Utility/has_function.h"

#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Abstract/Physics/General/has_spectrum.h"

namespace qengine
{
	namespace internal
	{
		// this type is meant only create temporary objects. It is created by calling operator[] in physics-Operators to get eigenstates.
		template<class Operator>
		class LinearCombinationOfEigenstates
		{
		public:
			LinearCombinationOfEigenstates(const Operator& op) :op_(op) {}

			template<class... Ts, typename = std::enable_if_t<has_spectrum_v<Operator, any_return, count_t, Ts...>>>
			auto evaluate(Ts&&... ts) const
			{
				auto spectrum = op_.makeSpectrum(factors_.size() - 1, std::forward<Ts>(ts)...);

				return spectrum.makeLinearCombination(factors_);
			}

			complex factor(const count_t index) const;

			void setFactor(const count_t index, const complex& factor);


			LinearCombinationOfEigenstates& operator+=(const LinearCombinationOfEigenstates& other);
			LinearCombinationOfEigenstates& operator-=(const LinearCombinationOfEigenstates& other);

			LinearCombinationOfEigenstates& operator*=(const real d);

			LinearCombinationOfEigenstates& operator*=(const complex& z);

			LinearCombinationOfEigenstates& operator/=(const real d);

			LinearCombinationOfEigenstates& operator/=(const complex& z);

			/// INTERNAL USE ONLY
		public:
			const Operator& _op() const { return op_; }
		private:
			const Operator& op_;
			CVec factors_;
		};

		template<class Operator>
		LinearCombinationOfEigenstates<Operator> operator+(LinearCombinationOfEigenstates<Operator> left, const LinearCombinationOfEigenstates<Operator>& right);

		template<class Operator>
		LinearCombinationOfEigenstates<Operator> operator-(LinearCombinationOfEigenstates<Operator> left, const LinearCombinationOfEigenstates<Operator>& right);

		template<class Operator>
		LinearCombinationOfEigenstates<Operator> operator*(LinearCombinationOfEigenstates<Operator> left, const real right);

		template<class Operator>
		LinearCombinationOfEigenstates<Operator> operator*(const real left, const LinearCombinationOfEigenstates<Operator>& right);

		template<class Operator>
		LinearCombinationOfEigenstates<Operator> operator*(LinearCombinationOfEigenstates<Operator> left, const complex right);

		template<class Operator>
		LinearCombinationOfEigenstates<Operator> operator*(const complex left, const LinearCombinationOfEigenstates<Operator>& right);

		template<class Operator>
		LinearCombinationOfEigenstates<Operator> operator/(LinearCombinationOfEigenstates<Operator> left, const real right);

		template<class Operator>
		LinearCombinationOfEigenstates<Operator> operator/(LinearCombinationOfEigenstates<Operator> left, const complex right);
	}
}

﻿/* COPYRIGHT
 *
 * file="has_spectrum.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <type_traits>

namespace qengine
{
	namespace internal
	{
		template<typename C, template<class> class  returnQualifier, typename... Args>
		struct has_spectrum
		{
		private:
			template<typename T, typename = std::enable_if_t<returnQualifier<decltype(std::declval<T>().makeSpectrum(std::declval<Args>()...))>::value>>
			static constexpr bool check(T*) { return true; }

			template<typename>
			static constexpr bool check(...) { return false; }


		public:
			static constexpr bool value = check<C>(0);
		};

		template<typename C, template<class> class  returnQualifier, typename... Args>
		static constexpr bool has_spectrum_v = has_spectrum<C, returnQualifier, Args...>::value;
	}
}

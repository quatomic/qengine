﻿/* COPYRIGHT
 *
 * file="EigenstateSyntax_CRTP.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.h"
#include "src/Abstract/Physics/General/LinearCombinationOfEigenstates.h"

namespace qengine
{
	namespace internal
	{
		template <class Impl>
		LinearCombinationOfEigenstates<Impl> EigenstateSyntax_CRTP<Impl>::operator[](count_t index) const
		{
			auto l = LinearCombinationOfEigenstates<Impl>(static_cast<const Impl&>(*this));
			l.setFactor(index, 1);
			return l;
		}
	}
}

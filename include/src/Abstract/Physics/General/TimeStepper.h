﻿/* COPYRIGHT
 *
 * file="TimeStepper.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"

#include "src/Utility/is_callable.h"
#include "src/Utility/cpp17Replacements/Tuple.h"

#include "src/Utility/DeepCopy.h"

#include "src/Utility/std17/is_same_v.h"

namespace qengine
{
	template<class State, class AlgMaker, class Alg, class... Params>
	class GenericTimeStepper
	{
		static_assert(is_callable_v<Alg, void(State&, real, Tuple<Params...>)>, "");
	public:
		template<class State2, class AlgMaker2, class Alg2, class ParamTuple>
		GenericTimeStepper(State2&& s, AlgMaker2&& maker, Alg2&& alg, ParamTuple&& ps) :
			previous_(std::forward<ParamTuple>(ps)),
			alg_(std::make_unique<Alg>(std::forward<Alg2>(alg))),
			algMaker_(std::forward<AlgMaker2>(maker)),
			state_(std::forward<State2>(s))
		{}


		GenericTimeStepper(const GenericTimeStepper& other)
			: previous_(other.previous_),
			alg_(internal::deep_copy(other.alg_)),
			state_(other.state_)
		{
		}

		GenericTimeStepper(GenericTimeStepper&& other) = default;

		GenericTimeStepper& operator=(const GenericTimeStepper& other) = delete;

		GenericTimeStepper& operator=(GenericTimeStepper&& other) = delete;

		void step(real dt, const Tuple<Params...>& to)
		{
			(*alg_)(state_, dt, to);
			previous_ = to;
		}

		void cstep(real dt)
		{
			(*alg_)(state_, dt, previous_);
		}


		const State& state() const { return state_; }
		const Tuple<Params...>& previous() const { return previous_; }
		Alg alg() const { return *alg_; }
		AlgMaker _algMaker() const { return algMaker_; }

		void reset(const State& newState, const Params&... newParams)
		{
			state_ = newState;
			previous_ = std::make_tuple(newParams...);
			alg_ = std::make_unique<Alg>(algMaker_(previous_));
		}
		void reset(const State& newState, const Tuple<Params...>& newParams)
		{
			state_ = newState;
			previous_ = newParams;
			alg_ = std::make_unique<Alg>(algMaker_(previous_));
		}

	private:
		Tuple<Params...> previous_;
		std::unique_ptr<Alg> alg_;
		AlgMaker algMaker_;
		State state_;
	};

	// Specialization of TimeStepper for Algorithms without Params
	template<class State, class Alg>
	class GenericTimeStepper<State, Alg, void>
	{
		static_assert(is_callable_v<Alg, void(State&, real)>, "");
	public:
		template<class Alg2, class State2>
		GenericTimeStepper(State2&& s, Alg2&& a) :
			alg_(std::make_unique<Alg>(std::forward<Alg2>(a))),
			state_(std::forward<State2>(s))
		{}

		GenericTimeStepper(const GenericTimeStepper& other)
			: alg_(internal::deep_copy(other.alg_)),
			state_(other.state_)
		{
		}

		GenericTimeStepper(GenericTimeStepper&& other) = default;

		GenericTimeStepper& operator=(const GenericTimeStepper& other) = delete;
		GenericTimeStepper& operator=(GenericTimeStepper&& other) = delete;

		void step(real dt)
		{
			(*alg_)(state_, dt);
		}

		void cstep(real dt)
		{
			(*alg_)(state_, dt);
		}


		const State& state() const { return state_; }

		void reset(const State& state) { state_ = state; }

	private:
		std::unique_ptr<Alg> alg_;
		State state_;
	};
}

namespace qengine
{
	template<class State, class AlgMaker, class Alg, class... Params>
	class GenericFixedTimeStepper
	{
		static_assert(is_callable_v<Alg, void(State&, Tuple<Params...>)>, "");
	public:
		template<class State2, class AlgMaker2, class Alg2, class ParamTuple>
		GenericFixedTimeStepper(State2&& s, AlgMaker2&& maker, Alg2&& alg, ParamTuple&& ps) :
			previous_(std::forward<ParamTuple>(ps)),
			algMaker_(std::forward<AlgMaker2>(maker)),
			alg_(std::make_unique<Alg>(std::forward<Alg2>(alg))),
			state_(std::forward<State2>(s))
		{
			
		}

		GenericFixedTimeStepper(const GenericFixedTimeStepper& other)
			: previous_(other.previous_),
			algMaker_(other.algMaker_),
			alg_(internal::deep_copy(other.alg_)),
			state_(other.state_)
		{
		}

		GenericFixedTimeStepper(GenericFixedTimeStepper&& other) = default;

		GenericFixedTimeStepper& operator=(const GenericFixedTimeStepper& other) = delete;

		GenericFixedTimeStepper& operator=(GenericFixedTimeStepper&& other) = delete;

		void step(const Tuple<Params...>& to)
		{
			(*alg_)(state_, to);
			previous_ = to;
		}

		void cstep()
		{
			(*alg_)(state_, previous_);
		}

		const State& state() const { return state_; }
		const Tuple<Params...>& previous() const { return previous_; }
		Alg alg() const { return *alg_; }
		AlgMaker _algMaker() const { return algMaker_; }

		void reset(const State& newState, const Params&... newParams)
		{
			state_ = newState;
			previous_ = std::make_tuple(newParams...);
			alg_ = std::make_unique<Alg>(algMaker_(previous_));
		}

		void reset(const State& newState, const Tuple<Params...>& newParams)
		{
			state_ = newState;
			previous_ = newParams;
			alg_ = std::make_unique<Alg>(algMaker_(previous_));
		}

	private:
		Tuple<Params...> previous_;
		AlgMaker algMaker_;
		std::unique_ptr<Alg> alg_;
		State state_;
	};

	// Specialization of FixedTimeStepper for Algorithms without Params
	template<class State, class Alg>
	class GenericFixedTimeStepper<State, Alg, void>
	{
		static_assert(is_callable_v<Alg, void(State&)>, "");
	public:
		template<class Alg2, class State2>
		GenericFixedTimeStepper(State2&& s, Alg2&& a) :
			alg_(std::make_unique<Alg>(std::forward<Alg2>(a))),
			state_(std::forward<State2>(s))
		{}
		GenericFixedTimeStepper(const GenericFixedTimeStepper& other)
			: alg_(internal::deep_copy(other.alg_)),
			state_(other.state_)
		{
		}

		GenericFixedTimeStepper(GenericFixedTimeStepper&& other) = default;
		GenericFixedTimeStepper& operator=(const GenericFixedTimeStepper& other) = delete;

		GenericFixedTimeStepper& operator=(GenericFixedTimeStepper&& other) = delete;
		void step()
		{
			(*alg_)(state_);
		}

		void cstep()
		{
			(*alg_)(state_);
		}

		const State& state() const { return state_; }

		void reset(const State& state) { state_ = state; }
	private:
		std::unique_ptr<Alg> alg_;
		State state_;
	};
}

namespace qengine
{
	template<class State, class AlgMaker, class... Params>
	auto makeTimeStepper(AlgMaker algMaker, const State& psi0, Tuple<Params...> initialParams)
	{
		auto alg = algMaker(initialParams);
		static_assert(is_callable_v<decltype(alg), void(State&, real, Tuple<Params...>)>, "");
		return GenericTimeStepper<State, AlgMaker, decltype(alg), Params...>(psi0, algMaker, alg, initialParams);
	}

	template<class State, class AlgMaker>
	auto makeTimeStepper(AlgMaker algMaker, const State& psi0)
	{
		auto alg = algMaker();

		using Alg = decltype(alg);
		static_assert(is_callable_v<Alg, void(State&, real)>, "");
		return GenericTimeStepper<State, Alg, void>(psi0, alg);
	}
	template<class State, class AlgMaker, class... Params>
	auto makeFixedTimeStepper(AlgMaker algMaker, const State& psi0, Tuple<Params...> initialParams)
	{
		auto alg = algMaker(initialParams);
		static_assert(is_callable_v<decltype(alg), void(State&, Tuple<Params...>)>, "");
		return GenericFixedTimeStepper<State, AlgMaker, decltype(alg), Params...>(psi0, algMaker, alg, initialParams);
	}
	template<class State, class AlgMaker>
	auto makeFixedTimeStepper(AlgMaker algMaker, const State& psi0)
	{
		auto alg = algMaker();

		using Alg = decltype(alg);
		static_assert(is_callable_v<Alg, void(State&)>, "");
		return GenericFixedTimeStepper<State, Alg, void>(psi0, alg);
	}
}

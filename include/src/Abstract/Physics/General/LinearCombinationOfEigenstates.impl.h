﻿/* COPYRIGHT
 *
 * file="LinearCombinationOfEigenstates.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/LinearCombinationOfEigenstates.h"
#include "src/Utility/qengine_assert.h"

namespace qengine
{
	namespace internal
	{
		template <class Operator>
		complex LinearCombinationOfEigenstates<Operator>::factor(const count_t index) const
		{
			if (index > factors_.size() - 1) return 0.0;
			return factors_.at(index);
		}

		template <class Operator>
		void LinearCombinationOfEigenstates<Operator>::setFactor(const count_t index, const complex& factor)
		{
			if (index >= factors_.size()) factors_.resize(index + 1);
			factors_.at(index) = factor;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator>& LinearCombinationOfEigenstates<Operator>::operator+=(
			const LinearCombinationOfEigenstates& other)
		{
			qengine_assert(&op_ == &other.op_, "different operators are not allowed!");
			if (other.factors_.size() > factors_.size()) factors_.resize(other.factors_.size());

			for (auto i = 0u; i < other.factors_.size(); ++i)
			{
				factors_.at(i) += other.factors_.at(i);
			}

			return *this;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator>& LinearCombinationOfEigenstates<Operator>::operator-=(
			const LinearCombinationOfEigenstates& other)
		{
			qengine_assert(&op_ == &other.op_, "different operators are not allowed!");
			if (other.factors_.size() > factors_.size()) factors_.resize(other.factors_.size());

			for (auto i = 0u; i < other.factors_.size(); ++i)
			{
				factors_.at(i) -= other.factors_.at(i);
			}

			return *this;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator>& LinearCombinationOfEigenstates<Operator>::operator*=(const real d)
		{
			factors_ *= d;
			return *this;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator>& LinearCombinationOfEigenstates<Operator>::operator*=(const complex& z)
		{
			factors_ *= z;
			return *this;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator>& LinearCombinationOfEigenstates<Operator>::operator/=(const real d)
		{
			factors_ /= d;
			return *this;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator>& LinearCombinationOfEigenstates<Operator>::operator/=(const complex& z)
		{
			factors_ /= z;
			return *this;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator> operator+(LinearCombinationOfEigenstates<Operator> left,
		                                                   const LinearCombinationOfEigenstates<Operator>& right)
		{
			return left += right;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator> operator-(LinearCombinationOfEigenstates<Operator> left,
			const LinearCombinationOfEigenstates<Operator>& right)
		{
			return left -= right;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator> operator*(LinearCombinationOfEigenstates<Operator> left, const real right)
		{
			return left *= right;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator> operator*(const real left,
		                                                   const LinearCombinationOfEigenstates<Operator>& right)
		{
			return right * left;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator> operator*(LinearCombinationOfEigenstates<Operator> left, const complex right)
		{
			return left *= right;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator> operator*(const complex left,
		                                                   const LinearCombinationOfEigenstates<Operator>& right)
		{
			return right * left;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator> operator/(LinearCombinationOfEigenstates<Operator> left, const real right)
		{
			return left /= right;
		}

		template <class Operator>
		LinearCombinationOfEigenstates<Operator> operator/(LinearCombinationOfEigenstates<Operator> left, const complex right)
		{
			return left /= right;
		}
	}
}

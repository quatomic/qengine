﻿/* COPYRIGHT
 *
 * file="State.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/SecondQuantization/State.fwd.h"


#include "src/NumberTypes.h"

#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Abstract/Physics/SecondQuantization/is2ndQSerializedHilbertSpace.h"

namespace qengine
{
	namespace internal
	{
		// calculates a one-dimensional index from a multidimensional one.
		count_t collapseIndex(const std::vector<count_t>& indexVec, std::vector<count_t> dimensions);
	}
}

namespace qengine
{
	template<class SerializedHilbertSpace>
	class State
	{
		static_assert(second_quantized::is2ndQSerializedHilbertSpace_v<SerializedHilbertSpace>, "Hilbertspace must be a second quantized hilbertspace");
		/// CTOR DTOR
	public:
		State(const Vector<complex>& vec, const SerializedHilbertSpace& hilbertSpace);
		State(const Vector<real>& vec, const SerializedHilbertSpace& hilbertSpace);
		State(Vector<complex>&& vec, const SerializedHilbertSpace& hilbertSpace);

		State(const State& other) = default;
		State(State&& other) = default;

		~State() = default;

		/// ASSIGNMENT
	public:
		State & operator=(const State& other);
		State& operator=(State&& other) noexcept;

		/// MISC
	public:
		State operator- () const;

		void normalize();
		real norm() const;

		RVec absSquare() const; /// return |this|^2 
		RVec re() const;
		RVec im() const;

		complex sum() const;
		State conj() const;

		complex& at(count_t index);
		const complex& at(count_t index) const;

		count_t size() const;

		CVec & vec() & { return vec_; }
		const CVec& vec() const& { return vec_; }
		CVec&& vec() && {return std::move(vec_); }

		/// INTERNAL USE
	public:
		SerializedHilbertSpace  _serializedHilbertSpace() const;


	private:
		CVec vec_;
		SerializedHilbertSpace serializedHilbertSpace_;
	};
}

namespace qengine
{

	template<class SerializedHilbertSpace>
	complex overlap(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		return cdot(left.vec(), right.vec());
	}

	template<class SerializedHilbertSpace>
	real fidelity(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		return std::pow(std::abs(overlap(left, right)), 2);
	}

	template<class SerializedHilbertSpace>
	real infidelity(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		return 1 - fidelity(left, right);
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> normalize(State<SerializedHilbertSpace> f);

	template<class SerializedHilbertSpace>
	real norm(const State<SerializedHilbertSpace>& f);

	template<class SerializedHilbertSpace>
	RVec absSquare(const State<SerializedHilbertSpace>& f);

	template<class SerializedHilbertSpace>
	RVec re(const State<SerializedHilbertSpace>& f);

	template<class SerializedHilbertSpace>
	RVec im(const State<SerializedHilbertSpace>& f);

	template<class SerializedHilbertSpace>
	complex sum(const State<SerializedHilbertSpace>& f);

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> conj(const State<SerializedHilbertSpace>& f);

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> toComplex(const State<SerializedHilbertSpace>& f) { return f; }
}

namespace qengine
{
	template<class SerializedHilbertSpace>
	std::ostream& operator<<(std::ostream& s, const State<SerializedHilbertSpace>& f);

	template<class SerializedHilbertSpace>
	bool operator== (const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right);
	template<class SerializedHilbertSpace>
	bool operator!= (const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right);

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator+(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right);
	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator- (const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right);
	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator* (const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right);
	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator/ (const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right);

}

namespace qengine
{
	template<class SerializedHilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator+ (const State<SerializedHilbertSpace>& f, number2 a)
	{
		return State<SerializedHilbertSpace>(f.vec() + a, f._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator+ (number2 a, const State<SerializedHilbertSpace>& f)
	{
		return State<SerializedHilbertSpace>(a + f.vec(), f._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator- (const State<SerializedHilbertSpace>& f, number2 a)
	{
		return State<SerializedHilbertSpace>(f.vec() - a, f._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator- (number2 a, const State<SerializedHilbertSpace>& f)
	{
		return State<SerializedHilbertSpace>(a - f.vec(), f._serializedHilbertSpace());
	}
	template<class SerializedHilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator* (const State<SerializedHilbertSpace>& f, number2 a)
	{
		return State<SerializedHilbertSpace>(f.vec() * a, f._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator* (number2 a, const State<SerializedHilbertSpace>& f)
	{
		return State<SerializedHilbertSpace>(a * f.vec(), f._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator/ (const State<SerializedHilbertSpace>& f, number2 a)
	{
		return State<SerializedHilbertSpace>(f.vec() / a, f._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace, typename number2, typename = std::enable_if_t<internal::is_convertible_to_number_type_v<number2>>>
	auto operator/ (number2 a, const State<SerializedHilbertSpace>& f)
	{
		return State<SerializedHilbertSpace>(a / f.vec(), f._serializedHilbertSpace());
	}
}

﻿/* COPYRIGHT
 *
 * file="Operator.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <assert.h>

#include "src/Abstract/Physics/SecondQuantization/Operator.h"

#include "src/Abstract/Physics/SecondQuantization/Spectrum.h"

namespace qengine
{
	template <template <class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	Operator<MatType, ScalarType, SerializedHilbertSpace>::Operator(const MatType<ScalarType>& mat, const SerializedHilbertSpace& hilbertSpace) : mat_(mat),
		serializedHilbertSpace_(hilbertSpace)
	{
		qengine_assert(!mat_.hasNan(), "operator has NaN");
	}

	template <template <class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	Operator<MatType, ScalarType, SerializedHilbertSpace>::Operator(MatType<ScalarType>&& mat, const SerializedHilbertSpace& hilbertSpace) : mat_(mat),
		serializedHilbertSpace_(hilbertSpace)
	{
		qengine_assert(!mat_.hasNan(), "operator has NaN");
	}

	template <template <class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	template <typename N, typename T>
	Operator<MatType, ScalarType, SerializedHilbertSpace>::Operator(const Operator<MatType, T, SerializedHilbertSpace>& other) :
		mat_(other._mat()),
		serializedHilbertSpace_(other._serializedHilbertSpace())
	{
	}

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	Operator<MatType, ScalarType, SerializedHilbertSpace>& Operator<MatType, ScalarType, SerializedHilbertSpace>::operator=(const Operator& other)
	{
		if (&other != this)
		{
			qengine_assert(other._serializedHilbertSpace() == this->_serializedHilbertSpace(), "trying to assign Operator with different dimensions!");
			mat_ = other.mat_;
		}
		return *this;
	}

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	Operator<MatType, ScalarType, SerializedHilbertSpace>& Operator<MatType, ScalarType, SerializedHilbertSpace>::operator=(Operator&& other) noexcept
	{
		if (&other != this)
		{
            assert(other._serializedHilbertSpace() == this->_serializedHilbertSpace());
			mat_ = other.mat_;
		}
		return *this;
	}

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	void Operator<MatType, ScalarType, SerializedHilbertSpace>::applyInPlace(State<SerializedHilbertSpace>& state) const
	{
		//qengine_assert(state._serializedHilbertSpace() == this->_serializedHilbertSpace(), "dimensions don't fit!");
		mat_.multiplyInPlace(state.vec());
	}

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	second_quantized::Spectrum<SerializedHilbertSpace> Operator<MatType, ScalarType, SerializedHilbertSpace>::makeSpectrum(const count_t largestEigenstate) const
	{
		return second_quantized::Spectrum<SerializedHilbertSpace>(internal::getSpectrum(mat_, largestEigenstate, 1), _serializedHilbertSpace());
	}

	template <template <class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	template <class T, class>
	second_quantized::Spectrum<SerializedHilbertSpace> Operator<MatType, ScalarType, SerializedHilbertSpace>::makeSpectrum(count_t largestEigenstate, count_t nExtraStates, real tolerance) const
	{
		return second_quantized::Spectrum<SerializedHilbertSpace>(internal::getSpectrum(mat_, largestEigenstate, 1, nExtraStates, tolerance), _serializedHilbertSpace());
	}

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	ScalarType Operator<MatType, ScalarType, SerializedHilbertSpace>::at(const count_t col, const count_t row) const
	{
		return mat_.at(col, row);
	}

}

namespace qengine
{
	template <template <class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	std::ostream& operator<<(std::ostream& stream, const Operator<MatType, ScalarType, SerializedHilbertSpace>& op)
	{
		stream << op._mat();
		return stream;
	}

	template <template <class> class M1, template <class> class M2, typename N1, typename N2, class SerializedHilbertSpace>
	bool operator==(const Operator<M1, N1, SerializedHilbertSpace>& left, const Operator<M2, N2, SerializedHilbertSpace>& right)
	{
		return left._mat().mat() == right._mat().mat();
	}

	template <template <class> class M1, template <class> class M2, typename N1, typename N2, class SerializedHilbertSpace>
	bool operator!=(const Operator<M1, N1, SerializedHilbertSpace>& left, const Operator<M2, N2, SerializedHilbertSpace>& right)
	{
		return left._mat().mat() == right._mat().mat();
	}

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator*(const Operator<MatType, ScalarType, SerializedHilbertSpace>& op, State<SerializedHilbertSpace> s)
	{
		op.applyInPlace(s);
		return s;
	}

	template <template <class> class MatType, typename ScalarType1, typename ScalarType2, class SerializedHilbertSpace>
	Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator+(const Operator<MatType, ScalarType1, SerializedHilbertSpace>& op, const Vector<ScalarType2>& s)
	{
		return Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace>(op._mat() + internal::SqDiagMat<ScalarType2>(s), op._serializedHilbertSpace());
	}

	template <template <class> class MatType, typename ScalarType1, typename ScalarType2, class SerializedHilbertSpace>
	Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator+(const Vector<ScalarType2>& s, const Operator<MatType, ScalarType1, SerializedHilbertSpace>& op)
	{
		return op + s;
	}
}

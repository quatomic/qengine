﻿/* COPYRIGHT
 *
 * file="is2ndQSerializedHilbertSpace.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/isSerializedHilbertSpace.h"

namespace qengine
{
	namespace second_quantized
	{
		// check if type can be considered Hilbertspace by internal functionality.
		template<class T>
		struct is2ndQSerializedHilbertSpace
		{
			constexpr static bool value =
				internal::isSerializedHilbertSpace_v<T>
				;
		};

		template<class T> constexpr bool is2ndQSerializedHilbertSpace_v = is2ndQSerializedHilbertSpace<T>::value;
	}
}

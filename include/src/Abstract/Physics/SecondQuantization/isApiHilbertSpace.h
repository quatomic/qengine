#pragma once

#include <type_traits>

namespace qengine
{
	namespace second_quantized
	{
		template<class T, class SFINAE = void>
		struct has2ndQApiHilbertSpaceLabel: std::false_type{};

		template<class T>
		struct is2ndQApiHilbertSpace
		{
			static constexpr bool value = 
				has2ndQApiHilbertSpaceLabel<T>::value
			;
		};

		template<class T>
		constexpr bool is2ndQApiHilbertSpace_v = is2ndQApiHilbertSpace<T>::value;
	}
}
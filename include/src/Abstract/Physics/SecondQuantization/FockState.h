﻿/* COPYRIGHT
 *
 * file="FockState.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <vector>
#include <map>

#include "src/NumberTypes.h"

namespace qengine
{
	namespace internal
	{

		class FockState
		{
		public:
			/*implicit*/ FockState(std::initializer_list<count_t> quanta) :quanta_(quanta) {}
			/*implicit*/ FockState(const std::vector<count_t>& quanta) :quanta_(quanta) {}
			/*implicit*/ FockState(std::vector<count_t>&& quanta) :quanta_(std::move(quanta)) {}
		public:

			count_t & at(std::size_t index);
			count_t at(std::size_t index) const;
			count_t nModes() const;

			/// INTERNAL USE
		public:
			const std::vector<count_t>& _quanta() const noexcept { return quanta_; }
			std::vector<count_t>& _quanta() noexcept { return quanta_; }

		private:
			std::vector<count_t> quanta_;
		};

		bool operator==(const FockState& left, const FockState& right);
		bool operator!=(const FockState& left, const FockState& right);
		bool operator<(const FockState& left, const FockState&right);
		bool operator>(const FockState& left, const FockState&right);

		bool operator<=(const FockState& left, const FockState&right);
		bool operator>=(const FockState& left, const FockState&right);


		std::ostream& operator<< (std::ostream& stream, const FockState& state);

		struct DefaultFockHash
		{
			std::size_t operator()(const FockState& state) const;
		};
	}

	class LinearCombinationOfFocks
	{
	public:
		LinearCombinationOfFocks(std::initializer_list<count_t> quanta);
		LinearCombinationOfFocks(const internal::FockState& state, const complex& factor);
		LinearCombinationOfFocks(const internal::FockState& state);
		explicit LinearCombinationOfFocks(count_t nModes);


		LinearCombinationOfFocks(const LinearCombinationOfFocks& other);
		LinearCombinationOfFocks(LinearCombinationOfFocks&& other) noexcept;

		LinearCombinationOfFocks& operator=(const LinearCombinationOfFocks& other);
		LinearCombinationOfFocks& operator=(LinearCombinationOfFocks&& other) noexcept;

		void add(const internal::FockState& state, const complex& factor);
		LinearCombinationOfFocks& operator+=(const LinearCombinationOfFocks& other);
		LinearCombinationOfFocks& operator*=(const complex& c);

		count_t at(count_t i) const;

		bool empty() const;

		std::map<internal::FockState, complex>& factors() noexcept { return factors_; }
		const std::map<internal::FockState, complex>& factors() const noexcept { return factors_; }
		count_t nModes() const noexcept { return nModes_; }
	private:
		std::map<internal::FockState, complex> factors_;
		const count_t nModes_;
	};

	LinearCombinationOfFocks operator+(LinearCombinationOfFocks left, const LinearCombinationOfFocks& right);
	LinearCombinationOfFocks operator-(LinearCombinationOfFocks left, const LinearCombinationOfFocks& right);
	LinearCombinationOfFocks operator* (const complex& factor, LinearCombinationOfFocks state);

	bool operator==(const LinearCombinationOfFocks& left, const LinearCombinationOfFocks& right);
	bool operator!=(const LinearCombinationOfFocks& left, const LinearCombinationOfFocks& right);

	std::ostream& operator<<(std::ostream& stream, const LinearCombinationOfFocks& states);

	real norm(const LinearCombinationOfFocks& state);
	LinearCombinationOfFocks normalize(LinearCombinationOfFocks state);

	namespace internal
	{
		real create(FockState& col, count_t i);
		real annihilate(FockState& col, count_t i);
	}

	complex overlap(const LinearCombinationOfFocks& left, const LinearCombinationOfFocks& right);

	namespace fock 
	{
		using state = LinearCombinationOfFocks;
	}
}

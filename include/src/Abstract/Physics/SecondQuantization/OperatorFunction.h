﻿/* COPYRIGHT
 *
 * file="OperatorFunction.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <tuple>

#include "src/Utility/InitializedCallable.h"

#include "src/Abstract/Physics/General/isOperator.h"
#include "src/Abstract/Physics/SecondQuantization/State.fwd.h"
#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"


namespace qengine
{
	template<class F, class... Ts>
	class OperatorFunction: public internal::InitializedCallable<F,Ts...>
	{
	public:
		using internal::InitializedCallable<F, Ts...>::InitializedCallable;
	};

	template<class F, class... Ts>
	auto makeOperatorFunction(F f, Ts&&... initialParams)->OperatorFunction<F, std17::remove_cvref_t<Ts>...>
	{
		using O = decltype(f(initialParams...));
		static_assert(internal::isCompatibleOperator_v<O, State<internal::extract_serialized_hilbertspace_t<O>>>, "return type of the passed in function must be a second quantization operator!");
		return OperatorFunction<F, std17::remove_cvref_t<Ts>...>(f, std::forward<Ts>(initialParams)...);
	}
}

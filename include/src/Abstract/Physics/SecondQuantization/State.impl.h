﻿/* COPYRIGHT
 *
 * file="State.impl.h"
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/SecondQuantization/State.h"

 /*
  * Basically all functions forward to Vector<complex> in LinearAlgebra-module
  */

#include <assert.h>

#include "src/Utility/qengine_assert.h"


namespace qengine
{
	namespace internal
	{
		inline count_t collapseIndex(const std::vector<count_t>& indexVec, std::vector<count_t> dimensions)
		{
			assert(indexVec.size() == dimensions.size());

			for (auto dIt = dimensions.rbegin() + 1; dIt != dimensions.rend(); ++dIt)
			{
				(*dIt) = *dIt * *(dIt - 1);
			}

			auto index = indexVec.back();


			for (auto i = dimensions.size() - 1; i > 0u; --i)
			{
				index += indexVec.at(i - 1) * dimensions.at(i);
			}

			return index;
		}
	}
}

namespace qengine
{
	template <class SerializedHilbertSpace>
	State<SerializedHilbertSpace>::State(const Vector<complex>& vec, const SerializedHilbertSpace& hilbertSpace) :
		vec_(vec),
		serializedHilbertSpace_(hilbertSpace)
	{
		qengine_assert(!vec_.hasNan(), "state has NaN");
	}

	template <class SerializedHilbertSpace>
	State<SerializedHilbertSpace>::State(const Vector<real>& vec, const SerializedHilbertSpace& hilbertSpace) :
		vec_(vec),
		serializedHilbertSpace_(hilbertSpace)
	{
		qengine_assert(!vec_.hasNan(), "state has NaN");
	}

	template <class SerializedHilbertSpace>
	State<SerializedHilbertSpace>::State(Vector<complex>&& vec, const SerializedHilbertSpace& hilbertSpace) :
		vec_(std::move(vec)),
		serializedHilbertSpace_(hilbertSpace)
	{
		qengine_assert(!vec_.hasNan(), "state has NaN");
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace>& State<SerializedHilbertSpace>::operator=(const State<SerializedHilbertSpace>& other)
	{
		if (&other != this)
		{
			qengine_assert(other._serializedHilbertSpace() == this->_serializedHilbertSpace(), "trying to assign State<SerializedHilbertSpace> with different dimensions!");
			vec_ = other.vec_;
		}
		return *this;
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace>& State<SerializedHilbertSpace>::operator=(State<SerializedHilbertSpace>&& other) noexcept
	{
		if (&other != this)
		{
			assert(other._serializedHilbertSpace() == this->_serializedHilbertSpace());
			vec_ = std::move(other.vec_);
		}
		return *this;
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> State<SerializedHilbertSpace>::operator-() const
	{
		return State<SerializedHilbertSpace>(-vec_, _serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	void State<SerializedHilbertSpace>::normalize()
	{
		vec_ /= vec_.norm();
	}

	template<class SerializedHilbertSpace>
	real State<SerializedHilbertSpace>::norm() const
	{
		return vec_.norm();
	}

	template<class SerializedHilbertSpace>
	RVec State<SerializedHilbertSpace>::absSquare() const
	{
		return vec_.absSquare();
	}

	template<class SerializedHilbertSpace>
	RVec State<SerializedHilbertSpace>::re() const
	{
		return vec_.re();
	}

	template<class SerializedHilbertSpace>
	RVec State<SerializedHilbertSpace>::im() const
	{
		return vec_.re();
	}

	template<class SerializedHilbertSpace>
	complex State<SerializedHilbertSpace>::sum() const
	{
		return qengine::sum(vec_);
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> State<SerializedHilbertSpace>::conj() const
	{
		return State<SerializedHilbertSpace>(qengine::conj(vec_), _serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	complex& State<SerializedHilbertSpace>::at(const count_t index)
	{
		return vec_.at(index);
	}

	template<class SerializedHilbertSpace>
	const complex& State<SerializedHilbertSpace>::at(const count_t index) const
	{
		return vec_.at(index);
	}

	template<class SerializedHilbertSpace>
	SerializedHilbertSpace State<SerializedHilbertSpace>::_serializedHilbertSpace() const
	{
		return serializedHilbertSpace_;
	}

	template<class SerializedHilbertSpace>
	count_t State<SerializedHilbertSpace>::size() const
	{
		return vec_.size();
	}

}

namespace qengine
{
	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> normalize(State<SerializedHilbertSpace> f)
	{
		f.normalize();
		return f;
	}

	template<class SerializedHilbertSpace>
	real norm(const State<SerializedHilbertSpace>& f)
	{
		return f.norm();
	}

	template<class SerializedHilbertSpace>
	RVec absSquare(const State<SerializedHilbertSpace>& f)
	{
		return f.absSquare();
	}

	template<class SerializedHilbertSpace>
	RVec re(const State<SerializedHilbertSpace>& f)
	{
		return f.re();
	}

	template<class SerializedHilbertSpace>
	RVec im(const State<SerializedHilbertSpace>& f)
	{
		return f.im();
	}

	template<class SerializedHilbertSpace>
	complex sum(const State<SerializedHilbertSpace>& f)
	{
		return f.sum();
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> conj(const State<SerializedHilbertSpace>& f)
	{
		return f.conj();
	}
}

namespace qengine
{
	template<class SerializedHilbertSpace>
	std::ostream& operator<<(std::ostream& s, const State<SerializedHilbertSpace>& f)
	{
		s << f.vec();
		return s;
	}

	template <class SerializedHilbertSpace>
	bool operator==(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		return left._serializedHilbertSpace() == right._serializedHilbertSpace() && left.vec() == right.vec();
	}

	template <class SerializedHilbertSpace>
	bool operator!=(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		return !(left == right);
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator+(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "trying to operate with different dimensions!");
		return State<SerializedHilbertSpace>(left.vec() + right.vec(), left._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator-(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "trying to operate with different dimensions!");
		return State<SerializedHilbertSpace>(left.vec() - right.vec(), left._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator*(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "trying to operate with different dimensions!");
		return State<SerializedHilbertSpace>(left.vec() * right.vec(), left._serializedHilbertSpace());
	}

	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator/(const State<SerializedHilbertSpace>& left, const State<SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "trying to operate with different dimensions!");
		return State<SerializedHilbertSpace>(left.vec() / right.vec(), left._serializedHilbertSpace());
	}
}

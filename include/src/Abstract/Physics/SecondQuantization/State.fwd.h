﻿/* COPYRIGHT
 *
 * file="State.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"
#include "src/Abstract/Physics/General/isState.h"

namespace qengine
{
	template<class SerializedHilbertSpace>
	class State;
}

namespace qengine
{
	namespace internal
	{
		template<class SerializedHilbertSpace>
		struct isLabelledAs_State<State<SerializedHilbertSpace>, SerializedHilbertSpace>: std::true_type{};
	}
}

namespace qengine
{
	namespace internal
	{
		// this is the general case for Functions of x, but it may be further specialized by e.g. functions of x that live on helper-hilbertspaces
		template<class SerializedHilbertSpace>
		struct extract_serialized_hilbertspace<State<SerializedHilbertSpace>>
		{
			using type = SerializedHilbertSpace;
		};
	}
}

﻿/* COPYRIGHT
 *
 * file="KrylovStepping.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/Physics/SecondQuantization/KrylovStepping.impl.h"
#include <armadillo>

namespace qengine
{
	namespace internal
	{
		CVec KrylovAlgorithm_Lanczos::buildLanczosState(const Spectrum<real>& spectrum, const real dt) const
		{
			using namespace std::complex_literals;

			const auto& D = spectrum.eigenvalues()._vec(); // eigenvalues
			const auto& A = spectrum.eigenvectors()._mat(); // eigenvectors

			// todo: find out what exactly happens here. It is not exactly a spectral decomposition, but close
            return CVec(lanczosVectors_._mat() * (A* (arma::exp(-1i * dt*D) % A.row(0).t())));
		}
	}
}

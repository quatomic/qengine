﻿/* COPYRIGHT
 *
 * file="Operator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"

#include "src/Utility/LinearAlgebra/SqDiagMat.h"
#include "src/Utility/LinearAlgebra/SqDenseMat.h"
#include "src/Utility/LinearAlgebra/SqSparseMat.h"
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"

#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.h"
#include "src/Abstract/Physics/General/extract_serialized_hilbertspace.h"

#include "src/Abstract/Physics/SecondQuantization/State.h"
#include "src/Abstract/Physics/SecondQuantization/is2ndQSerializedHilbertSpace.h"
#include "src/Abstract/Physics/SecondQuantization/Spectrum.fwd.h"
#include "src/Abstract/Physics/SecondQuantization/Operator.fwd.h"

#include "src/Utility/qengine_assert.h"

namespace qengine
{
	// basic static operator with different representations and arithmetic
	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	class Operator : public internal::EigenstateSyntax_CRTP<Operator<MatType, ScalarType, SerializedHilbertSpace>>
	{
		static_assert(second_quantized::is2ndQSerializedHilbertSpace_v<SerializedHilbertSpace>, "Hilbertspace must be a serialized second quantized hilbertspace");
		static_assert(internal::is_number_type_v<ScalarType>, "ScalarType for BasicOperator must be a numbertype (real or complex)!");

		/// CTOR DTOR
	public:
		Operator(const MatType<ScalarType>& mat, const SerializedHilbertSpace& hilbertSpace);
		Operator(MatType<ScalarType>&& mat, const SerializedHilbertSpace& hilbertSpace);

		Operator(const Operator& other) = default;
		Operator(Operator&& other) = default;

		template<typename N = ScalarType, typename T = std::enable_if_t<std::is_same<N, complex>::value, real>>
		Operator(const Operator<MatType, T, SerializedHilbertSpace>& other);

		~Operator() = default;

		/// ASSIGNMENT
	public:
		Operator & operator=(const Operator& other);
		Operator& operator=(Operator&& other) noexcept;
		/*
		template<typename T2 = ScalarType, typename T = std::enable_if_t<std::is_same<T2, complex>::value, real>>
		Operator& operator=(const Operator<T>& other);*/

		/// MISC
	public:
		void applyInPlace(State<SerializedHilbertSpace>& state) const;
		second_quantized::Spectrum<SerializedHilbertSpace> makeSpectrum(count_t largestEigenstate) const;

		template<class T = MatType<ScalarType>, class = std::enable_if_t<std::is_same<T, internal::SqSparseMat<ScalarType>>::value>>
		second_quantized::Spectrum<SerializedHilbertSpace> makeSpectrum(count_t largestEigenstate, count_t nExtraStates, real tolerance = 0.0) const;

		ScalarType at(count_t col, count_t row) const;

		/// INTERNAL USE
	public:
		const MatType<ScalarType>& _mat() const { return mat_; }
		MatType<ScalarType>& _mat() { return mat_; }

		const SerializedHilbertSpace& _serializedHilbertSpace() const { return serializedHilbertSpace_; }

	protected:
		MatType<ScalarType> mat_;
		SerializedHilbertSpace serializedHilbertSpace_;
	};

	namespace internal
	{

		template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace >
		auto makeOperator(MatType<ScalarType>&& mat, const SerializedHilbertSpace& hilbertSpace)
		{
			return Operator<MatType, ScalarType, SerializedHilbertSpace>(std::move(mat), hilbertSpace);
		}


		template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
		auto makeOperator(const MatType<ScalarType>& mat, const SerializedHilbertSpace& hilbertSpace)
		{
			return Operator<MatType, ScalarType, SerializedHilbertSpace>(mat, hilbertSpace);
		}

	}
}


namespace qengine
{
	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	std::ostream& operator<<(std::ostream& stream, const Operator<MatType, ScalarType, SerializedHilbertSpace>& op);

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class SerializedHilbertSpace>
	bool operator==(const Operator<M1, N1, SerializedHilbertSpace>& left, const Operator<M2, N2, SerializedHilbertSpace>& right);

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class SerializedHilbertSpace>
	bool operator!=(const Operator<M1, N1, SerializedHilbertSpace>& left, const Operator<M2, N2, SerializedHilbertSpace>& right);

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator*(const  Operator<MatType, ScalarType, SerializedHilbertSpace>& op, State<SerializedHilbertSpace> s);

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator+(const  Operator<M1, N1, SerializedHilbertSpace>& left, const  Operator<M2, N2, SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "");
		return internal::makeOperator(left._mat() + right._mat(), left._serializedHilbertSpace());
	}

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator-(const  Operator<M1, N1, SerializedHilbertSpace>& left, const  Operator<M2, N2, SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "");
		return internal::makeOperator(left._mat() - right._mat(), left._serializedHilbertSpace());
	}

	template<template<class> class M1, template<class> class M2, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator*(const  Operator<M1, N1, SerializedHilbertSpace>& left, const  Operator<M2, N2, SerializedHilbertSpace>& right)
	{
		qengine_assert(left._serializedHilbertSpace() == right._serializedHilbertSpace(), "");
		return internal::makeOperator(left._mat()*right._mat(), left._serializedHilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator*(const N1 left, const  Operator<M, N2, SerializedHilbertSpace>& right)
	{
		return internal::makeOperator(left*right._mat(), right._serializedHilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator/(const  Operator<M, N1, SerializedHilbertSpace>& left, const N2 right)
	{
		return internal::makeOperator(left._mat() / right, left._serializedHilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator+(const  Operator<M, N1, SerializedHilbertSpace>& left, const N2 right)
	{
		return internal::makeOperator(left._mat() + right, left._serializedHilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator+(const N1 left, const  Operator<M, N2, SerializedHilbertSpace>& right)
	{
		return internal::makeOperator(left + right._mat(), right._serializedHilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator-(const  Operator<M, N1, SerializedHilbertSpace>& left, const N2 right)
	{
		return internal::makeOperator(left._mat() - right, left._serializedHilbertSpace());
	}

	template<template<class> class M, typename N1, typename N2, class SerializedHilbertSpace>
	auto operator-(const N1 left, const  Operator<M, N2, SerializedHilbertSpace>& right)
	{
		return internal::makeOperator(left - right._mat(), right._serializedHilbertSpace());
	}

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	auto exp(const Operator<MatType, ScalarType, SerializedHilbertSpace>& exponent)
	{
		return internal::makeOperator(exp(exponent._mat()), exponent._serializedHilbertSpace());
	}

	template<template<class> class MatType, typename ScalarType1, typename ScalarType2, class SerializedHilbertSpace>
	Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator+(const  Operator<MatType, ScalarType1, SerializedHilbertSpace>& op, const Vector<ScalarType2>& s);

	template<template<class> class MatType, typename ScalarType1, typename ScalarType2, class SerializedHilbertSpace>
	Operator<MatType, internal::NumberMax_t<ScalarType1, ScalarType2>, SerializedHilbertSpace> operator+(const Vector<ScalarType2>& s, const  Operator<MatType, ScalarType1, SerializedHilbertSpace>& op);
}

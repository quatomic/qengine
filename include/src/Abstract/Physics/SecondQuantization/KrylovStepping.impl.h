﻿/* COPYRIGHT
 *
 * file="KrylovStepping.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "src/Abstract/Physics/SecondQuantization/KrylovStepping.h"

namespace qengine
{
	namespace internal
	{

		template <class Operator, class State>
		void KrylovAlgorithm_Lanczos::doStep(const Operator& H, State& psi, const real dt) const
		{
			static_assert(internal::isCompatibleOperator_v<Operator, State>, "The passed-in operator is not an operator for this State-type");
			static_assert(internal::isLinear_v<Operator>, "The passed-in operator is not linear");
			/// Lanczos: 
			const auto T = buildLanczosMatrices(H, psi);

			// Calculate psi(t+dt) = beta*Q*exp(factor*T)*e1

			// case 1: by eigendecomposition
			auto spectrum = getSpectrum_herm(T, T.dim() - 1, 1);

			//            auto expT_e1 = SqDenseMat<complex>(makeSpectrum.eigenvectors())
			//                          *(exp(SqDiagMat<complex>(factor*makeSpectrum.eigenvalues()))
			//                          *CVec(CMat(makeSpectrum.eigenvectors().mat().t()).col(0)));
			//            psi = State<SerializedHilbertSpace>(Q_*expT_e1,psi._serializedHilbertSpace());
			// |dpsi| = | psi(t+dt) - exp(factor*H) | = beta*|psi(t)|*|e_k^T exp(factor*T)*e_1| (assuming beta=|psi(t)|=1)
			//            auto residual_error = norm(expT_e1.at(expT_e1.dim()-1)); // use this as a measure for the necessary krylovorder or dt
			//            std::cout << residual_error << std::endl;

			// In one go, faster, but cannot get residual error easily
			psi = State(buildLanczosState(spectrum, dt), psi._serializedHilbertSpace());

			// case 3: direct exp.
			//            auto U = exp(factor * T);
			//            psi = State<SerializedHilbertSpace>(beta * Q_ * CVec(U.mat().col(0)), psi._serializedHilbertSpace());
			//            std::cout << norm(CVec(U.mat().col(0)).at(U.dim()-1)) << std::endl;

		}

		/// Lanczos: https://arxiv.org/pdf/1407.7370.pdf
		template<class Operator, class State>
		SqHermitianBandMat<real> KrylovAlgorithm_Lanczos::buildLanczosMatrices(const Operator& H_in, State& state) const
		{
			complex zero(0.0);
			count_t N = H_in._serializedHilbertSpace().dim;

            if (lanczosVectors_.n_rows() != N || lanczosVectors_.n_cols() != krylovOrder_)
			{
                lanczosVectors_ = CMat(N, krylovOrder_, zero);
			}
			auto bands = RMat(krylovOrder_, 2, 0.0); // tridiagonal symmetric

			auto qi = state;

			auto z = H_in*qi;

            lanczosVectors_.col(0) = qi.vec();

			for (auto i = 0u; i < krylovOrder_ - 1; ++i)
			{
                qi = State(lanczosVectors_.col(i), state._serializedHilbertSpace());
				auto ai = (cdot(qi.vec(), z.vec())).real();
				z = z - ai * qi;
				auto bi = z.norm();
                lanczosVectors_.col(i + 1) = z.vec() / bi;
                z = H_in*State(lanczosVectors_.col(i + 1), state._serializedHilbertSpace()) - bi * qi;

				bands.at(i, 0) = ai;
				bands.at(i, 1) = bi;
			}

            qi = State(lanczosVectors_.col(krylovOrder_ - 1), state._serializedHilbertSpace());
			auto ai = (cdot(qi.vec(), z.vec())).real();

			bands.at(krylovOrder_ - 1, 0) = ai;

			return SqHermitianBandMat<real>(std::move(bands), internal::SqHermitianBandMat<real>::COL_LAYOUT);
		}


		template <class Operator, class SerializedHilbertSpace>
		void KrylovAlgorithm_arnoldi::doStep(const Operator& H, State<SerializedHilbertSpace>& psi, const real dt) const
		{
			using namespace std::complex_literals;

			auto arnoldiResult = arnoldi(H, psi, krylovOrder_);
			auto krylovHamiltonian = std::get<0>(arnoldiResult);
			auto Vm = std::get<1>(arnoldiResult);
			auto beta = std::get<2>(arnoldiResult);

			auto U = exp(-1i * dt * krylovHamiltonian);
			psi = State<SerializedHilbertSpace>(beta * Vm * CVec(U.mat().col(0)), psi._serializedHilbertSpace());
		}

		/// Arnoldi: https://www.maths.uq.edu.au/expokit/guide.html
		template <class Operator, class SerializedHilbertSpace>
		std::tuple<internal::SqDenseMat<complex>, Matrix<complex>, double> KrylovAlgorithm_arnoldi::arnoldi(const Operator& H_in, State<SerializedHilbertSpace>& state, count_t krylovOrder) const
		{
			const count_t size = H_in._serializedHilbertSpace().dim;
			complex zero(0.0);

			Matrix<complex> V(size, krylovOrder_ + 1, zero);
			Matrix<complex> H(krylovOrder_ + 1, krylovOrder_, zero);

			real beta = state.norm();

			State<SerializedHilbertSpace> v0 = state / beta; // v0
												   //            State w(CVec(v0.dim()));

			V.col(0) = v0.vec();

			//            std::cout << cdot(V.col(0)._rep(),V.col(0)._rep()) << std::endl;


			for (auto j = 0u; j <= krylovOrder - 1; j++)
			{
				auto state_j = State<SerializedHilbertSpace>(V.col(j), state._serializedHilbertSpace());
				auto w = state_j;

				H_in.applyInPlace(w);

				for (auto i = 0u; i <= j; i++)
				{
					auto state_i = State<SerializedHilbertSpace>(V.col(i), state._serializedHilbertSpace());
					H.at(i, j) = cdot(state_i.vec(), w.vec());
					w = w - state_i * H.at(i, j);
				}
				H.at(j + 1, j) = norm(w);
				const auto tol = 1e-10;
				if (norm(H.at(j + 1, j)) < tol)
				{
					krylovOrder = j + 1;
					//                    std::cout << "Happy breakdown for Krylov order " << krylovOrder << " instead of the supplied " << krylovOrder_ << std::endl; break; // happy breakdown
				}
				V.col(j + 1) = (w / H.at(j + 1, j)).vec();
			}


			Matrix<complex> Vm(size, krylovOrder, zero);
			Matrix<complex> krylovHamiltonian(krylovOrder, krylovOrder, zero);

			for (auto i = 0u; i < krylovOrder; ++i)
			{
				for (auto j = 0u; j < krylovOrder; ++j)
				{
					krylovHamiltonian.at(i, j) = H.at(i, j);
				}
			}

			for (auto i = 0u; i < size; ++i)
			{
				for (auto j = 0u; j < krylovOrder; ++j)
				{
					Vm.at(i, j) = V.at(i, j);
				}
			}


			return std::make_tuple(internal::SqDenseMat<complex>(krylovHamiltonian), Vm, beta);
		}


	}
}

﻿/* COPYRIGHT
 *
 * file="Functions.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Normalization.h"

#include "src/Abstract/Physics/General/LinearCombinationOfEigenstates.h"

#include "src/Abstract/Physics/SecondQuantization/State.h"
#include "src/Abstract/Physics/SecondQuantization/Operator.h"

namespace qengine
{
	template<template<class> class MatType, typename Number, class SerializedHilbertSpace, class... Ts, typename = std::enable_if_t<internal::has_spectrum_v<Operator<MatType, Number, SerializedHilbertSpace>, any_return, count_t, Ts...>>>
	State<SerializedHilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, Number, SerializedHilbertSpace>>& l, const qengine::NORMALIZATION shouldNormalize, Ts&&... additionalParams)
	{
		if (shouldNormalize == NORMALIZE)
		{
			return normalize(l.evaluate(std::forward<Ts>(additionalParams)...));
		}
		return l.evaluate(std::forward<Ts>(additionalParams)...);
	}

	template<template<class> class MatType, typename Number, class SerializedHilbertSpace, class... Ts, typename = std::enable_if_t<internal::has_spectrum_v<Operator<MatType, Number, SerializedHilbertSpace>, any_return, count_t, Ts...>>>
	State<SerializedHilbertSpace> makeState(const internal::LinearCombinationOfEigenstates<Operator<MatType, Number, SerializedHilbertSpace>>& l, Ts&&... additionalParams)
	{
		return l.evaluate(std::forward<Ts>(additionalParams)...);
	}
}

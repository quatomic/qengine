﻿/* COPYRIGHT
 *
 * file="Spectrum.impl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/SecondQuantization/Spectrum.h"

#include "src/Abstract/Physics/SecondQuantization/State.h"

namespace qengine
{
	namespace second_quantized
	{
		template <class SerializedHilbertSpace>
		Spectrum<SerializedHilbertSpace>::Spectrum(internal::Spectrum<complex>&& t1, const SerializedHilbertSpace& hilbertSpace) :
			internal::Spectrum<complex>(t1),
			serializedHilbertSpace_(hilbertSpace)
		{
		}

		template <class SerializedHilbertSpace>
		Spectrum<SerializedHilbertSpace>::Spectrum(const internal::Spectrum<complex>& t1,
			const SerializedHilbertSpace& hilbertSpace) :
			internal::Spectrum<complex>(t1),
			serializedHilbertSpace_(hilbertSpace)
		{
		}

		template<class SerializedHilbertSpace>
		State<SerializedHilbertSpace> Spectrum<SerializedHilbertSpace>::eigenState(const count_t i) const
		{
			return State<SerializedHilbertSpace>(internal::Spectrum<complex>::eigenvector(i), serializedHilbertSpace_);
		}

		template<class SerializedHilbertSpace>
		State<SerializedHilbertSpace> Spectrum<SerializedHilbertSpace>::makeLinearCombination(const CVec& factors, const qengine::NORMALIZATION shouldNormalize) const
		{
			auto state = State<SerializedHilbertSpace>(internal::Spectrum<complex>::makeLinearCombination(factors), serializedHilbertSpace_);

			if (shouldNormalize == NORMALIZE)
			{
				return normalize(state);
			}
			return state;
		}

		template <class SerializedHilbertSpace>
		SerializedHilbertSpace Spectrum<SerializedHilbertSpace>::_serializedHilbertSpace() const
		{
			return serializedHilbertSpace_;
		}
	}
}

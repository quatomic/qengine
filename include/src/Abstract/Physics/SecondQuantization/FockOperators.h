﻿/* COPYRIGHT
 *
 * file="FockOperators.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <memory>

#include "src/NumberTypes.h"
#include "src/Utility/std17/as_const.h"

#include "src/Abstract/Physics/SecondQuantization/FockState.h"

namespace qengine
{
	namespace internal
	{
		enum class OPERATOR_TYPE
		{
			SUM,
			PRODUCT,
			CREATE,
			ANNIHILATE,
			COUNT,
			SCALARMUL,
			IDENTITY,
			ZERO
		};

		class FockOperator_impl
		{
		public:
			virtual ~FockOperator_impl() = default;
			virtual void applyInPlace_impl(LinearCombinationOfFocks& states) const = 0;
			virtual std::unique_ptr<FockOperator_impl> clone() const& = 0;
			virtual std::unique_ptr<FockOperator_impl> clone() && {return std17::as_const(*this).clone(); }
			virtual OPERATOR_TYPE type() const = 0;
		};

		class CreationOperator : public FockOperator_impl
		{
		public:
			CreationOperator(const count_t index);

			void applyInPlace_impl(LinearCombinationOfFocks& states) const override;
			std::unique_ptr<FockOperator_impl> clone() const & override;
			OPERATOR_TYPE type() const override;

			count_t index() const noexcept { return index_; }
		private:
			count_t index_;
		};

		class AnnihilationOperator : public FockOperator_impl
		{
		public:
			AnnihilationOperator(const count_t index);
			void applyInPlace_impl(LinearCombinationOfFocks& states) const override;
			std::unique_ptr<FockOperator_impl> clone() const & override;
			OPERATOR_TYPE type() const override;

			count_t index() const noexcept { return index_; }
		private:
			count_t index_;
		};

		class NumberOperator : public FockOperator_impl
		{
		public:
			NumberOperator(const count_t index);
			void applyInPlace_impl(LinearCombinationOfFocks& states) const override;
			std::unique_ptr<FockOperator_impl> clone() const & override;
			OPERATOR_TYPE type() const override;

			count_t index() const noexcept { return index_; }
		private:
			count_t index_;
		};

		class FockSumNode : public FockOperator_impl
		{
		public:
			FockSumNode(std::unique_ptr<FockOperator_impl>&& left, std::unique_ptr<FockOperator_impl>&& right);
			void applyInPlace_impl(LinearCombinationOfFocks& states) const override;
			std::unique_ptr<FockOperator_impl> clone() const & override;
			std::unique_ptr<FockOperator_impl> clone() && override;
			OPERATOR_TYPE type() const override;
		private:
			std::unique_ptr<FockOperator_impl> left_;
			std::unique_ptr<FockOperator_impl> right_;
		};

		class FockProductNode : public FockOperator_impl
		{
		public:
			FockProductNode(std::unique_ptr<FockOperator_impl>&& left, std::unique_ptr<FockOperator_impl>&& right);
			void applyInPlace_impl(LinearCombinationOfFocks& states) const override;
			std::unique_ptr<FockOperator_impl> clone() const & override;
			std::unique_ptr<FockOperator_impl> clone() && override;
			OPERATOR_TYPE type() const override;

		private:
			std::unique_ptr<FockOperator_impl> left_;
			std::unique_ptr<FockOperator_impl> right_;
		};

		class FockMultiplyByConstantOperator : public FockOperator_impl
		{
		public:
			explicit FockMultiplyByConstantOperator(const complex& number);
			void applyInPlace_impl(LinearCombinationOfFocks& states) const override;
			std::unique_ptr<FockOperator_impl> clone() const & override;
			OPERATOR_TYPE type() const override;
		private:
			complex number_;
		};

		class FockIdentityOperator : public FockOperator_impl
		{
		public:
			void applyInPlace_impl(LinearCombinationOfFocks& states) const override;
			std::unique_ptr<FockOperator_impl> clone() const & override;
			OPERATOR_TYPE type() const override;
		};

		class FockZeroOperator : public FockOperator_impl
		{
		public:
			void applyInPlace_impl(LinearCombinationOfFocks& states) const override;
			std::unique_ptr<FockOperator_impl> clone() const & override;
			OPERATOR_TYPE type() const override;
		};
	}

	class FockOperator
	{
	public:
		explicit FockOperator(std::unique_ptr<internal::FockOperator_impl>&& impl);

		FockOperator(const FockOperator& other);
		FockOperator(FockOperator&& other) noexcept;

		FockOperator& operator=(const FockOperator& other);
		FockOperator& operator=(FockOperator&& other) noexcept;

	public:
		void applyInPlace(LinearCombinationOfFocks& states) const;

	public:
		FockOperator & operator*=(const FockOperator& other);
		FockOperator& operator*=(FockOperator&& other);

		FockOperator& operator+=(const FockOperator& other);
		FockOperator& operator+=(FockOperator&& other);

		FockOperator& operator*=(const complex& c);

	private:
		std::unique_ptr<internal::FockOperator_impl> impl_;
	};

	LinearCombinationOfFocks operator*(const FockOperator& op, LinearCombinationOfFocks states);

	FockOperator operator*(FockOperator left, const FockOperator& right);
	FockOperator operator*(FockOperator left, FockOperator&& right);

	FockOperator operator+(FockOperator left, const FockOperator& right);
	FockOperator operator+(FockOperator left, FockOperator&& right);

	FockOperator operator-(const FockOperator& left, const FockOperator& right);
	FockOperator operator-(const FockOperator& left, FockOperator&& right);

	FockOperator operator+(FockOperator left, const complex& right);
	FockOperator operator+(const complex& left, const FockOperator& right);

	FockOperator operator-(const FockOperator& left, const complex& right);
	FockOperator operator-(const complex& left, const FockOperator& right);


	FockOperator operator*(const complex& c, FockOperator right);

	FockOperator pow(const FockOperator& op, count_t power);

	namespace fock
	{
		// creates an annihilation-operator
		FockOperator a(count_t i);

		// creates a creation-operator
		FockOperator c(count_t i);

		// creates a number-operator
		FockOperator n(count_t i);

		// creates a zero-operator (kills any state!)
		FockOperator zero();

		// creates identity operator
		FockOperator I();
	}
}

﻿/* COPYRIGHT
 *
 * file="FockState.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/Physics/SecondQuantization/FockState.h"

#include <cassert>

#include "src/Utility/qengine_assert.h"

#include "src/Utility/IntegerFunctions.h"

namespace qengine
{
	namespace internal
	{
		count_t& FockState::at(const std::size_t index)
		{
			return quanta_.at(index);
		}

		count_t FockState::at(const std::size_t index) const
		{
			return quanta_.at(index);
		}

		count_t FockState::nModes() const
		{
			return quanta_.size();
		}

		bool operator==(const FockState& left, const FockState& right)
		{
			if (left._quanta().size() != right._quanta().size()) return false;

			auto lIt = left._quanta().begin();
			auto rIt = right._quanta().begin();
			for (; lIt != left._quanta().end(); ++lIt, ++rIt)
			{
				if (*lIt != *rIt) return false;
			}
			return true;
		}

		bool operator!=(const FockState& left, const FockState& right)
		{
			return !(left == right);
		}

		bool operator<(const FockState& left, const FockState& right)
		{
            assert(left._quanta().size() == right._quanta().size());
			for (auto i = 0u; i < left._quanta().size(); ++i)
			{
				if (left._quanta().at(i) == right._quanta().at(i)) continue;
				return left._quanta().at(i) < right._quanta().at(i);
			}
			return false;
		}

		bool operator>(const FockState& left, const FockState& right)
		{
			return right < left;
		}

		bool operator<=(const FockState& left, const FockState& right)
		{
			return !(left > right);
		}

		bool operator>=(const FockState& left, const FockState& right)
		{
			return !(left < right);
		}

		std::ostream& operator<<(std::ostream& stream, const FockState& state)
		{
			if (state._quanta().empty())
			{
				stream << "|>";
				return stream;
			}

			stream << "|";

			// here, we can be certain that quanta is not empty, so the comparison works out
			for(auto it = state._quanta().begin(); it!=state._quanta().end()-1; ++it)
			{
				stream << *it << ", ";
			}

			stream << state._quanta().back();

			stream << ">";
			return stream;
		}

	}
	LinearCombinationOfFocks::LinearCombinationOfFocks(std::initializer_list<count_t> quanta) :
		LinearCombinationOfFocks(internal::FockState(quanta))
	{
	}

	namespace internal
	{
		std::size_t DefaultFockHash::operator()(const FockState& state) const
		{
			auto hash = std::size_t(0);

			for (auto i = 0u; i < state._quanta().size(); ++i)
			{
				hash += internal::int_power(10, i);
			}

			return hash;
		}
	}
	LinearCombinationOfFocks::LinearCombinationOfFocks(const internal::FockState& state, const complex& factor) :
		nModes_(state._quanta().size())
	{
		factors_.emplace(state, factor);
	}

	LinearCombinationOfFocks::LinearCombinationOfFocks(const internal::FockState& state) :
		LinearCombinationOfFocks(state, 1.0)
	{
	}

	LinearCombinationOfFocks::LinearCombinationOfFocks(const count_t nModes) :
		nModes_(nModes)
	{
	}

	LinearCombinationOfFocks::LinearCombinationOfFocks(const LinearCombinationOfFocks& other) : factors_{ other.factors_ },
		nModes_{ other.nModes_ }
	{
	}

	LinearCombinationOfFocks::LinearCombinationOfFocks(LinearCombinationOfFocks&& other) noexcept :
		factors_{ std::move(other.factors_) },
		nModes_{ other.nModes_ }
	{
	}

	LinearCombinationOfFocks& LinearCombinationOfFocks::operator=(const LinearCombinationOfFocks& other)
	{
		if (this == &other)
			return *this;
		qengine_assert(other.nModes_ == nModes_, "can only assign with same number of modes");
		factors_ = other.factors_;
		return *this;
	}

	LinearCombinationOfFocks& LinearCombinationOfFocks::operator=(LinearCombinationOfFocks&& other) noexcept
	{
		if (this == &other)
			return *this;
		assert(other.nModes_ == nModes_);
		factors_ = std::move(other.factors_);
		return *this;
	}

	void LinearCombinationOfFocks::add(const internal::FockState& state, const complex& factor)
	{
		qengine_assert(state._quanta().size() == nModes_, "all FockState states must have same number of modes! existing: " + std::to_string(nModes_) + "; trying to add: " + std::to_string(state._quanta().size()));

		auto it = factors_.find(state);
		if (it == factors_.end())
		{
			factors_.emplace(state, factor);
		}
		else
		{
			it->second += factor;
		}
	}

	LinearCombinationOfFocks& LinearCombinationOfFocks::operator+=(const LinearCombinationOfFocks& other)
	{
		for (const auto& a : other.factors())
		{
			add(a.first, a.second);
		}
		return *this;
	}

	LinearCombinationOfFocks& LinearCombinationOfFocks::operator*=(const complex& c)
	{
		for (auto& f : factors_)
		{
			f.second *= c;
		}
		return *this;
	}

	count_t LinearCombinationOfFocks::at(const count_t i) const
	{
		if (factors_.size() != 1) throw std::runtime_error("at(i) is only possible when there is only a single state!");
		return factors_.begin()->first._quanta().at(i);
	}

	bool LinearCombinationOfFocks::empty() const
	{
		return factors_.empty();
	}

	LinearCombinationOfFocks operator+(LinearCombinationOfFocks left, const LinearCombinationOfFocks& right)
	{
		left += right;
		return left;
	}

	LinearCombinationOfFocks operator-(LinearCombinationOfFocks left, const LinearCombinationOfFocks& right)
	{
		for (const auto& a : right.factors())
		{
			left.add(a.first, -a.second);
		}
		return left;
	}

	LinearCombinationOfFocks operator*(const complex& factor, LinearCombinationOfFocks state)
	{
		state *= factor;
		return state;
	}

	bool operator==(const LinearCombinationOfFocks& left, const LinearCombinationOfFocks& right)
	{
		if (left.factors().size() != right.factors().size()) return false;

		auto leftIt = left.factors().begin();
		auto rightIt = right.factors().begin();
		for (; leftIt != left.factors().end(); ++leftIt, ++rightIt)
		{
			if (leftIt->first != rightIt->first) return false;
			if (leftIt->second != rightIt->second) return false;
		}
		return true;
	}

	bool operator!=(const LinearCombinationOfFocks& left, const LinearCombinationOfFocks& right)
	{
		return !(left == right);
	}

	std::ostream& operator<<(std::ostream& stream, const LinearCombinationOfFocks& states)
	{
		for (auto it = states.factors().begin(); it != states.factors().end(); ++it)
		{
			if (it->second != 1.0)
			{
				writeTo(stream, it->second);
				stream << " ";
			}
			stream << it->first;
			if (std::next(it) != states.factors().end())
			{
				stream << " + ";
			}
		}
		return stream;
	}

	real norm(const LinearCombinationOfFocks& state)
	{
		auto normSq = 0.0;
		for (const auto& a : state.factors())
		{
			normSq += absSq(a.second);
		}
		return std::sqrt(normSq);
	}


	LinearCombinationOfFocks normalize(LinearCombinationOfFocks state)
	{
		const auto n = norm(state);
		for (auto& a : state.factors())
		{
			a.second /= n;
		}
		return state;
	}

	complex overlap(const LinearCombinationOfFocks& left, const LinearCombinationOfFocks& right)
	{
		auto retval = complex(0);
		qengine_assert(left.nModes() == right.nModes(), "must have same number of modes!");
		for (const auto& a : left.factors())
		{
			const auto it = right.factors().find(a.first);
			if (it != right.factors().end())
			{
				retval += std::conj(a.second)*it->second;
			}
		}
		return retval;
	}

	namespace internal
	{
		real create(FockState& state, const count_t i)
		{
			const auto factor = std::sqrt(state._quanta().at(i) + 1);
			state._quanta().at(i) += 1;
			return factor;
		}

		real annihilate(FockState& state, const count_t i)
		{
			const auto factor = std::sqrt(state._quanta().at(i));
			state._quanta().at(i) -= 1;
			return factor;
		}
	}
}

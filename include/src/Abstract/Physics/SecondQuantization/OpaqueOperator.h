﻿/* COPYRIGHT
 *
 * file="OpaqueOperator.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/SecondQuantization/Operator.h"

namespace qengine
{
	namespace internal 
	{
		template<class SerializedHilbertSpace>
		class OperatorHider_Base
		{
		public:
			virtual ~OperatorHider_Base() {}

			virtual void applyInPlace(State<SerializedHilbertSpace>& state) const = 0;
		};

		template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
		class OperatorHider :public OperatorHider_Base<SerializedHilbertSpace>
		{
		public:
			OperatorHider(Operator<MatType, ScalarType, SerializedHilbertSpace>&& op):operator_(op){}

			virtual void applyInPlace(State<SerializedHilbertSpace>& state) const override
			{
				operator_.applyInPlace(state);
			}

		private:
			const Operator<MatType, ScalarType, SerializedHilbertSpace> operator_;
		};

		template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
		auto makeOperatorHiderPointer(Operator<MatType, ScalarType, SerializedHilbertSpace>&& op)
		{
			return std::make_unique<OperatorHider<MatType, ScalarType, SerializedHilbertSpace>>(std::move(op));
		}
	}

	template<class SerializedHilbertSpace>
	class OpaqueOperator
	{
	public:
		OpaqueOperator(std::unique_ptr<internal::OperatorHider_Base<SerializedHilbertSpace>>&& op):
		operator_(std::move(op))
		{}

		void applyInPlace(State<SerializedHilbertSpace>& state) const
		{
			operator_->applyInPlace(state);
		}

	private:
		std::unique_ptr<internal::OperatorHider_Base<SerializedHilbertSpace>> operator_;
	};

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	OpaqueOperator<SerializedHilbertSpace> makeOpaqueOperator(Operator<MatType, ScalarType, SerializedHilbertSpace>&& op)
	{
		return OpaqueOperator<SerializedHilbertSpace>(internal::makeOperatorHiderPointer(std::move(op)));
	}

	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	OpaqueOperator<SerializedHilbertSpace> makeOpaqueOperator(const Operator<MatType, ScalarType, SerializedHilbertSpace>& op)
	{
		auto op2 = op;
		return OpaqueOperator<SerializedHilbertSpace>(internal::makeOperatorHiderPointer(std::move(op2)));
	}


	template<class SerializedHilbertSpace>
	State<SerializedHilbertSpace> operator*(const OpaqueOperator<SerializedHilbertSpace>& op, State<SerializedHilbertSpace> s)
	{
		op.applyInPlace(s);
		return s;
	}

}

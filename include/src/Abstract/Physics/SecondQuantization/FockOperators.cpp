﻿/* COPYRIGHT
 *
 * file="FockOperators.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Abstract/Physics/SecondQuantization/FockOperators.h"

#include <limits>
#include "src/Utility/IntegerFunctions.h"

namespace qengine
{
	namespace internal
	{

		FockSumNode::FockSumNode(std::unique_ptr<FockOperator_impl>&& left, std::unique_ptr<FockOperator_impl>&& right) :
			left_(std::move(left)),
			right_(std::move(right))
		{
		}

		void FockSumNode::applyInPlace_impl(LinearCombinationOfFocks& states) const
		{
			auto s1 = states;
			left_->applyInPlace_impl(states);
			right_->applyInPlace_impl(s1);
			states += s1;
		}

		std::unique_ptr<FockOperator_impl> FockSumNode::clone() const &
		{
			return std::make_unique<FockSumNode>(left_->clone(), right_->clone());
		}

		std::unique_ptr<FockOperator_impl> FockSumNode::clone() &&
		{
			return std::make_unique<FockSumNode>(std::move(left_), std::move(right_));
		}

		OPERATOR_TYPE FockSumNode::type() const
		{
			return OPERATOR_TYPE::SUM;
		}

		FockProductNode::FockProductNode(std::unique_ptr<FockOperator_impl>&& left, std::unique_ptr<FockOperator_impl>&& right) :
			left_(std::move(left)),
			right_(std::move(right))
		{
		}

		void FockProductNode::applyInPlace_impl(LinearCombinationOfFocks& states) const
		{
			right_->applyInPlace_impl(states);
			left_->applyInPlace_impl(states);
		}

		std::unique_ptr<FockOperator_impl> FockProductNode::clone() const &
		{
			return std::make_unique<FockProductNode>(left_->clone(), right_->clone());
		}

		std::unique_ptr<FockOperator_impl> FockProductNode::clone() &&
		{
			return std::make_unique<FockSumNode>(std::move(left_), std::move(right_));
		}

		OPERATOR_TYPE FockProductNode::type() const
		{
			return OPERATOR_TYPE::PRODUCT;
		}

		CreationOperator::CreationOperator(const count_t index) : index_(index)
		{
		}

		void CreationOperator::applyInPlace_impl(LinearCombinationOfFocks& states) const
		{
			auto newStates = LinearCombinationOfFocks(states.nModes());
			for (const auto& a : states.factors())
			{
				auto f = a.first;
				if (f.at(index_) != std::numeric_limits<count_t>::max())
				{
					const auto factor = a.second * internal::create(f, index_);
					newStates.add(f, factor);
				}
			}
			states = newStates;
		}

		std::unique_ptr<FockOperator_impl> CreationOperator::clone() const &
		{
			return std::make_unique<CreationOperator>(index_);
		}

		OPERATOR_TYPE CreationOperator::type() const
		{
			return OPERATOR_TYPE::CREATE;
		}

		AnnihilationOperator::AnnihilationOperator(const count_t index) : index_(index)
		{
		}

		void AnnihilationOperator::applyInPlace_impl(LinearCombinationOfFocks& states) const
		{
			auto newStates = LinearCombinationOfFocks(states.nModes());
			for (const auto& a : states.factors())
			{
				auto f = a.first;
				if (f.at(index_) != 0)
				{
					const auto factor = a.second * annihilate(f, index_);
					newStates.add(f, factor);
				}
			}
			states = newStates;
		}

		std::unique_ptr<FockOperator_impl> AnnihilationOperator::clone() const &
		{
			return std::make_unique<AnnihilationOperator>(index_);
		}

		OPERATOR_TYPE AnnihilationOperator::type() const
		{
			return OPERATOR_TYPE::ANNIHILATE;
		}

		NumberOperator::NumberOperator(const count_t index) :index_(index)
		{
		}

		void NumberOperator::applyInPlace_impl(LinearCombinationOfFocks& states) const
		{
			// todo: there is an inconcistency in this behaviour: when applied to {0}, it return 0*{0}. 
			// in contrast, c*a applied to {0} return empty!
			for (auto& a : states.factors())
			{
				a.second *= static_cast<real>(a.first.at(index_));
			}
		}

		std::unique_ptr<FockOperator_impl> NumberOperator::clone() const &
		{
			return std::make_unique<NumberOperator>(index_);
		}

		OPERATOR_TYPE NumberOperator::type() const
		{
			return OPERATOR_TYPE::COUNT;
		}

		FockMultiplyByConstantOperator::FockMultiplyByConstantOperator(const complex& number) : number_(number)
		{
		}

		void FockMultiplyByConstantOperator::applyInPlace_impl(LinearCombinationOfFocks& states) const
		{
			for (auto& a : states.factors())
			{
				a.second *= number_;
			}
		}

		std::unique_ptr<FockOperator_impl> FockMultiplyByConstantOperator::clone() const &
		{
			return std::make_unique<FockMultiplyByConstantOperator>(number_);
		}

		OPERATOR_TYPE FockMultiplyByConstantOperator::type() const
		{
			return OPERATOR_TYPE::SCALARMUL;
		}

		void FockIdentityOperator::applyInPlace_impl(LinearCombinationOfFocks&) const
		{
			// The empty function is not an oversight. Identity operator does nothing.
		}

		std::unique_ptr<FockOperator_impl> FockIdentityOperator::clone() const &
		{
			return std::make_unique<FockIdentityOperator>();
		}

		OPERATOR_TYPE FockIdentityOperator::type() const
		{
			return OPERATOR_TYPE::IDENTITY;
		}

		void FockZeroOperator::applyInPlace_impl(LinearCombinationOfFocks& states) const
		{
			states = LinearCombinationOfFocks(states.nModes());
		}

		std::unique_ptr<FockOperator_impl> FockZeroOperator::clone() const &
		{
			return std::make_unique<FockZeroOperator>();
		}

		OPERATOR_TYPE FockZeroOperator::type() const
		{
			return OPERATOR_TYPE::ZERO;
		}
	}



	FockOperator::FockOperator(std::unique_ptr<internal::FockOperator_impl>&& impl) :
		impl_(std::move(impl))
	{
	}

	FockOperator::FockOperator(const FockOperator& other) : impl_{ other.impl_->clone() }
	{
	}

	FockOperator::FockOperator(FockOperator&& other) noexcept : impl_{ std::move(other.impl_) }
	{
	}

	FockOperator& FockOperator::operator=(const FockOperator& other)
	{
		if (this == &other)
			return *this;
		impl_ = other.impl_->clone();
		return *this;
	}

	FockOperator& FockOperator::operator=(FockOperator&& other) noexcept
	{
		if (this == &other)
			return *this;
		impl_ = std::move(other.impl_);
		return *this;
	}

	void FockOperator::applyInPlace(LinearCombinationOfFocks& states) const
	{
		impl_->applyInPlace_impl(states);
	}

	FockOperator& FockOperator::operator*=(const FockOperator& other)
	{
		impl_ = std::make_unique<internal::FockProductNode>(std::move(impl_), other.impl_->clone());
		return *this;
	}

	FockOperator& FockOperator::operator*=(FockOperator&& other)
	{
		impl_ = std::make_unique<internal::FockProductNode>(std::move(impl_), std::move(other.impl_)->clone());
		return *this;
	}

	FockOperator& FockOperator::operator+=(const FockOperator& other)
	{
		impl_ = std::make_unique<internal::FockSumNode>(std::move(impl_), other.impl_->clone());
		return *this;
	}

	FockOperator& FockOperator::operator+=(FockOperator&& other)
	{
		impl_ = std::make_unique<internal::FockSumNode>(std::move(impl_), std::move(other.impl_)->clone());
		return *this;
	}

	FockOperator& FockOperator::operator*=(const complex& c)
	{
		impl_ = std::make_unique<internal::FockProductNode>(std::make_unique<internal::FockMultiplyByConstantOperator>(c), std::move(impl_));
		return *this;
	}

	LinearCombinationOfFocks operator*(const FockOperator& op, LinearCombinationOfFocks states)
	{
		op.applyInPlace(states);
		return states;
	}

	FockOperator operator*(FockOperator left, const FockOperator& right)
	{
		left *= right;
		return left;
	}

	FockOperator operator*(FockOperator left, FockOperator&& right)
	{
		left *= std::move(right);
		return left;
	}

	FockOperator operator+(FockOperator left, const FockOperator& right)
	{
		left += right;
		return left;
	}

	FockOperator operator+(FockOperator left, FockOperator&& right)
	{
		left += std::move(right);
		return left;
	}

	FockOperator operator-(const FockOperator& left, const FockOperator& right)
	{
		return left + (-1 * right);
	}

	FockOperator operator-(const FockOperator& left, FockOperator&& right)
	{
		return left + (-1 * std::move(right));
	}

	FockOperator operator+(FockOperator left, const complex& right)
	{
		return left += right * fock::I();
	}

	FockOperator operator+(const complex& left, const FockOperator& right)
	{
		return right + left;
	}

	FockOperator operator-(const FockOperator& left, const complex& right)
	{
		return left + (-right);
	}

	FockOperator operator-(const complex& left, const FockOperator& right)
	{
		return left * fock::I() - right;
	}

	FockOperator operator*(const complex& c, FockOperator right)
	{
		right *= c;
		return right;
	}

	FockOperator pow(const FockOperator& op, const count_t power)
	{
		if (power == 0) return FockOperator(std::make_unique<internal::FockIdentityOperator>());
		return internal::int_power(op, power);
	}

	namespace fock
	{

		FockOperator a(count_t i)
		{
			return FockOperator(std::make_unique<internal::AnnihilationOperator>(i));
		}

		FockOperator c(count_t i)
		{
			return FockOperator(std::make_unique<internal::CreationOperator>(i));
		}

		FockOperator n(count_t i)
		{
			return FockOperator(std::make_unique<internal::NumberOperator>(i));
		}

		FockOperator zero()
		{
			return FockOperator(std::make_unique<internal::FockZeroOperator>());
		}

		FockOperator I()
		{
			return FockOperator(std::make_unique<internal::FockIdentityOperator>());
		}
	}
}

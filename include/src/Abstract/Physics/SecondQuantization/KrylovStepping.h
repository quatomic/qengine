﻿/* COPYRIGHT
 *
 * file="KrylovStepping.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <tuple>

#include "src/NumberTypes.h"
#include "src/Utility/cpp17Replacements/Tuple.h"



#include "src/Utility/LinearAlgebra/Matrix.h"

#include "src/Abstract/Physics/SecondQuantization/Operator.h"
#include "src/Abstract/Physics/SecondQuantization/KrylovStepping.fwd.h"
#include "src/Abstract/Physics/Spatial/isHamiltonian.h"

namespace qengine
{
	namespace internal
	{
		class KrylovAlgorithm_Lanczos
		{
		public:
			explicit KrylovAlgorithm_Lanczos(const count_t krylovOrder) :krylovOrder_(krylovOrder) {}

			template<class Operator, class State>
			void doStep(const Operator& H, State& psi, const real dt) const;

		private:
			template< class Operator, class State>
			SqHermitianBandMat<real> buildLanczosMatrices(const Operator& H_in, State& state) const;

			CVec buildLanczosState(const Spectrum<real>& spectrum, real dt) const;

			const count_t krylovOrder_;

			mutable CMat lanczosVectors_;
		};

		class KrylovAlgorithm_arnoldi
		{
		public:
			explicit KrylovAlgorithm_arnoldi(const count_t krylovOrder) :krylovOrder_(krylovOrder) {}

			template<class Operator, class SerializedHilbertSpace>
			void doStep(const Operator& H, State<SerializedHilbertSpace>& psi, const real dt) const;

		private:
			template<class Operator, class SerializedHilbertSpace>
			std::tuple<internal::SqDenseMat<complex>, Matrix<complex>, double> arnoldi(const Operator& H_in, State<SerializedHilbertSpace>& state, count_t krylovOrder) const;

			const count_t krylovOrder_;
		};
	}

	template<class Operator>
	auto makeKrylovAlgorithm(const Operator& H, const count_t krylovOrder)
	{
		static_assert(!qengine::spatial::isHamiltonian_v<Operator>, "cannot use krylov with spatial hamiltonians (yet)");
		return[alg{ internal::KrylovAlgorithm_Lanczos(krylovOrder) }, H](auto& psi, const real dt)
		{
			alg.doStep(H, psi, dt);
		};
	}

	template<class Operator>
	auto makeKrylovAlgorithm(const Operator& H, const count_t krylovOrder, const real dt)
	{
		static_assert(!qengine::spatial::isHamiltonian_v<Operator>, "cannot use krylov with spatial hamiltonians (yet)");
		return[alg{ internal::KrylovAlgorithm_Lanczos(krylovOrder) }, H, dt](auto& psi)
		{
			alg.doStep(H, psi, dt);
		};
	}

	template<class HamiltonianFunction, class... Params>
	auto makeKrylovAlgorithm(const  HamiltonianFunction& H, const count_t krylovOrder, const Tuple<Params...>& initialParams)
	{
		return[alg{ internal::KrylovAlgorithm_Lanczos(krylovOrder) }, H, H_cache = H(initialParams), paramCache = initialParams](auto& psi, const real dt, auto&& to) mutable
		{
			auto H_step = internal::apply(H, internal::applyToEach([](const auto& left, const auto& right) {return 0.5*(left + right); }, paramCache, to));
			static_assert(!qengine::spatial::isHamiltonian_v<decltype(H_step)>, "cannot use krylov with spatial hamiltonians (yet)");
			paramCache = to;

			alg.doStep(H_step, psi, dt);
		};
	}

	template<class HamiltonianFunction, class... Params>
	auto makeKrylovAlgorithm(const  HamiltonianFunction& H, const count_t krylovOrder, const real dt, const Tuple<Params...>& initialParams)
	{
		return[alg{ internal::KrylovAlgorithm_Lanczos(krylovOrder) }, H, dt, H_cache = H(initialParams), paramCache = initialParams](auto& psi, auto&& to) mutable
		{
			auto H_step = internal::apply(H, internal::applyToEach([](const auto& left, const auto& right) {return 0.5*(left + right); }, paramCache, to));
			static_assert(!qengine::spatial::isHamiltonian_v<decltype(H_step)>, "cannot use krylov with spatial hamiltonians (yet)");

			paramCache = to;

			alg.doStep(H_step, psi, dt);

		};
	}

	template<class... Ts>
	auto krylov(Ts... ts)
	{
		return[ts...](auto&&... ps)
		{
			return makeKrylovAlgorithm(ts..., std::forward<decltype(ps)>(ps)...);
		};
	}
}

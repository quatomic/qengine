﻿/* COPYRIGHT
 *
 * file="DirectExponentiationStepping.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Constants.h"

#include "src/Utility/cpp17Replacements/Tuple.h"

#include "src/Abstract/Physics/SecondQuantization/Operator.h"

namespace qengine
{
	template<template<class>class MatType, class NumberType, class SerializedHilbertSpace>
	auto makeDirectExponentiationAlgorithm(const Operator<MatType, NumberType, SerializedHilbertSpace>& op)
	{
		using namespace std::complex_literals;
		return [op](auto& psi, real dt)
		{
			psi = exp(-1i * dt*op)*psi;
		};
	}

	template<template<class>class MatType, class NumberType, class SerializedHilbertSpace>
	auto makeDirectExponentiationAlgorithm(const Operator<MatType, NumberType, SerializedHilbertSpace>& op, const real dt)
	{
		using namespace std::complex_literals;
		auto eOp = exp(-1i * dt*op);
		return [eOp](auto& psi)
		{
			psi = eOp * psi;
		};
	}

	template<class Op, class... Params>
	auto makeDirectExponentiationAlgorithm(const Op& op, const Tuple<Params...>& initialParams)
	{
		using namespace std::complex_literals;
		return [op, op_cache = op(initialParams)](auto& psi, real dt, auto&& to) mutable
		{
			auto opNext = internal::apply(op, to);
			psi = exp(-1i * dt*0.5*(op_cache + opNext))*psi;
			op_cache = opNext;
		};
	}

	template<class Op, class... Params>
	auto makeDirectExponentiationAlgorithm(const Op& op, const real dt, const Tuple<Params...>& initialParams)
	{
		using namespace std::complex_literals;
		auto ff = -1i * dt*0.5;
		return [op, ff, op_cache = op(initialParams)](auto& psi, auto&& to) mutable
		{
			auto opNext = internal::apply(op, to);
			psi = exp(ff*(op_cache + opNext))*psi;
			op_cache = opNext;
		};
	}

	template<class... Ts>
	auto directExponentiation(const Ts&... ts)
	{
		return [ts...](auto&&... ps)
		{
			return makeDirectExponentiationAlgorithm(ts..., std::forward<decltype(ps)>(ps)...);
		};
	}
}

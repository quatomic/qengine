﻿/* COPYRIGHT
 *
 * file="Operator.fwd.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Abstract/Physics/General/isOperator.h"

namespace qengine
{
	template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
	class Operator;
}


namespace qengine
{
	namespace internal
	{
		template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
		struct isLabelledAs_Operator<Operator<MatType, ScalarType, SerializedHilbertSpace >> : std::true_type{};

		template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
		struct isLabelledAs_Linear<Operator<MatType, ScalarType, SerializedHilbertSpace >> : std::true_type{};

		template<template<class> class MatType, typename ScalarType, class SerializedHilbertSpace>
		struct extract_serialized_hilbertspace<Operator<MatType, ScalarType, SerializedHilbertSpace>>
		{
			using type = SerializedHilbertSpace;
		};
	}
}

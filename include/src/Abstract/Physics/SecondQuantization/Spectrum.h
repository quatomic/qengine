﻿/* COPYRIGHT
 *
 * file="Spectrum.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once


#include "src/Utility/Normalization.h"
#include "src/Utility/LinearAlgebra/Spectrum.h"

#include "src/Abstract/Physics/SecondQuantization/Spectrum.fwd.h"
#include "src/Abstract/Physics/SecondQuantization/State.fwd.h"

namespace qengine
{
	namespace second_quantized
	{
		template<class SerializedHilbertSpace>
		class Spectrum : public internal::Spectrum<complex>
		{
			static_assert(second_quantized::is2ndQSerializedHilbertSpace_v<SerializedHilbertSpace>, "");
		public:
			template<class T1>
			Spectrum(T1&& t1, const SerializedHilbertSpace& hilbertSpace) :
				internal::Spectrum<complex>(std::forward<T1>(t1), 1),
				serializedHilbertSpace_(hilbertSpace)
			{
			}

			Spectrum(internal::Spectrum<complex>&& t1, const SerializedHilbertSpace& hilbertSpace);

			Spectrum(const internal::Spectrum<complex>& t1, const SerializedHilbertSpace& hilbertSpace);

			State<SerializedHilbertSpace> eigenState(count_t i) const;

			State<SerializedHilbertSpace> makeLinearCombination(const CVec& factors, const qengine::NORMALIZATION shouldNormalize = NORMALIZE_NOT) const;

			/// INTERNAL USE ONLY
		public:
			SerializedHilbertSpace _serializedHilbertSpace() const;

		private:
			SerializedHilbertSpace serializedHilbertSpace_;
		};
	}
}

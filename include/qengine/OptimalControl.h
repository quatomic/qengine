﻿/* COPYRIGHT
 *
 * file="OptimalControl.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"
#include "src/API/UserUtilities/AutoMember.h"

/// Problems:
#include "src/Abstract/OptimalControl/Problems/LinearStateTransferProblem.h"
#include "src/Abstract/OptimalControl/Problems/LinearStateTransferProblem.impl.h"

#include "src/Abstract/OptimalControl/Problems/DynamicState.impl.h"

#include "src/Abstract/OptimalControl/Problems/Boundaries.h"
#include "src/Abstract/OptimalControl/Problems/Regularization.h"
#include "src/Abstract/OptimalControl/Problems/SumProblem.h"
#include "src/Abstract/OptimalControl/Problems/SumProblem.impl.h"


#include "src/API/GRAPE/GRAPE_steepest.h"
#include "src/API/GRAPE/GRAPE_bfgs.h"

#include "src/API/GROUP/GROUP_steepest.h"
#include "src/API/GROUP/GROUP_bfgs.h"

#include "src/API/Common/OptimalControl/BasisFactory.h"
#include "src/API/GROUP/GroupHelpers.h"

#include "src/API/Common/OptimalControl/Policies/PolicyWrapper.h"
#include "src/API/Common/OptimalControl/makeAlgorithm.h"

#include "src/Abstract/OptimalControl/Algorithms/SteepestDescent.impl.h"
#include "src/Abstract/OptimalControl/Algorithms/Bfgs.impl.h"

/// Controls:
#include "src/Abstract/OptimalControl/Parameters/Control.h"


#include "src/API/Common/GeneralHelpers.h"

#define QENGINE_USES_OPTIMAL_CONTROL

#ifdef QENGINE_USES_GPE
	#include "qengine/GPE_OC.h"
#endif
#ifdef QENGINE_USES_1P
	#include "qengine/OneParticle_OC.h"
#endif
#ifdef QENGINE_USES_2P
	#include "qengine/TwoParticle_OC.h"
#endif
#ifdef QENGINE_USES_NLEVEL
	#include "qengine/NLevel_OC.h"
#endif
#ifdef QENGINE_USES_BOSEHUBBARD
	#include "qengine/BoseHubbard_OC.h"
#endif

// Heuristics:
#include "src/API/Common/OptimalControl/LinesearchHeuristics/QuickStepMaker.h"

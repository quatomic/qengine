﻿/* COPYRIGHT
 *
 * file="GPE.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"
#include "src/API/UserUtilities/AutoMember.h"

////////// INCLUDES //////////
#include "src/Utility/Constants.h"
#include "src/NumberTypes.h"

#include "src/Abstract/Physics/General/OperatorFunctions.h"
#include "src/Abstract/Physics/Spatial/Basics.h"
#include "src/Abstract/Physics/Spatial/PotentialFunction.h"

#include "src/API/Common/Spatial/makeTimeStepper.h"

#include "src/API/Common/Spatial/1D/ApiHilbertSpace.h"

#include "src/API/GPE/HamiltonianArithmetic.h"
#include "src/API/GPE/defaultSteppingAlg.h"
#include "src/API/GPE/Basics.h"
#include "src/API/GPE/HamiltonianGpe.h"

/////////// IMPLEMENTATION-FILES //////////
#include "src/Abstract/Physics/General/LinearCombinationOfEigenstates.impl.h"
#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.impl.h"

#include "src/Abstract/Physics/Spatial/FunctionOfX.impl.h"
#include "src/Abstract/Physics/Spatial/Spectrum.impl.h"

#include "src/API/GPE/HamiltonianGpe.impl.h"

#define QENGINE_USES_GPE

#ifdef QENGINE_USES_OPTIMAL_CONTROL
#include "qengine/GPE_OC.h"
#endif

﻿/* COPYRIGHT
 *
 * file="NLevel.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"
#include "src/API/UserUtilities/AutoMember.h"

#include "src/Utility/Constants.h"
#include "src/NumberTypes.h"

#include "src/Abstract/Physics/General/OperatorFunctions.h"
#include "src/Abstract/Physics/SecondQuantization/Functions.h"

#include "src/Abstract/Physics/SecondQuantization/KrylovStepping.impl.h"

#include "src/API/NLevel/ApiHilbertSpace.h"
#include "src/API/NLevel/PauliOperator.h"
#include "src/API/Common/SecondQuantized/makeState.h"
#include "src/API/NLevel/makeOperator.h"
#include "src/API/NLevel/TimeSteppers.h"

/////////// IMPLEMENTATION-FILES //////////
#include "src/Abstract/Physics/General/LinearCombinationOfEigenstates.impl.h"
#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.impl.h"

#include "src/Abstract/Physics/SecondQuantization/Operator.impl.h"
#include "src/Abstract/Physics/SecondQuantization/State.impl.h"
#include "src/Abstract/Physics/SecondQuantization/Spectrum.impl.h"

#define QENGINE_USES_NLEVEL

#ifdef QENGINE_USES_OPTIMAL_CONTROL
#include "qengine/NLevel_OC.h"
#endif

﻿/* COPYRIGHT
 *
 * file="OneParticle.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/version.h"
#include "src/API/UserUtilities/AutoMember.h"

////////// INCLUDES //////////
#include "src/Utility/Constants.h"
#include "src/NumberTypes.h"

#include "src/Abstract/Physics/General/OperatorFunctions.h"
#include "src/Abstract/Physics/Spatial/Basics.h"
#include "src/Abstract/Physics/Spatial/PotentialFunction.h"

#include "src/API/Common/Spatial/makeTimeStepper.h"

#include "src/API/Common/Spatial/1D/ApiHilbertSpace.h"
#include "src/API/GPE/HamiltonianArithmetic.h"
#include "src/API/OneParticle/defaultSteppingAlg.h"

#include "src/Abstract/Physics/SecondQuantization/KrylovStepping.impl.h"

/////////// IMPLEMENTATION-FILES //////////
#include "src/Abstract/Physics/General/LinearCombinationOfEigenstates.impl.h"
#include "src/Abstract/Physics/General/EigenstateSyntax_CRTP.impl.h"

#include "src/Abstract/Physics/Spatial/LinearHamiltonian.impl.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.impl.h"
#include "src/Abstract/Physics/Spatial/Spectrum.impl.h"

#define QENGINE_USES_1P

#ifdef QENGINE_USES_OPTIMAL_CONTROL
#include "qengine/OneParticle_OC.h"
#endif

﻿/* COPYRIGHT
 *
 * file="gpe-example.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * The contencts of this file are subject to the conditions expressed in the LICENSE file 
 * distributed with the source files. If no such file has been distributed with the source code,
 * you are not allowed to use the code.
 * 
 * THE PROGRAM IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, EXCEPT TO THE EXTENT THAT THESE DISCLAIMERS ARE 
 * HELD TO BE LEGALLY INVALID.
 * The Licensor shall not be liable for any direct or indirect consequences of any use or 
 * improper use of the Program and/or damage caused to the Licensee (including, but not 
 * limited to planet destruction) and/or third parties as a result of any use or disuse of 
 * the Program including the possible faults or failures of the Program functioning to the 
 * maximum extent allowed by the applicable legislation.
 */
#include <qengine/qengine.h>
#include <iostream>

using namespace qengine;

int main()
{
    std::cout << "GPE Example Program" << std::endl << "---------------" << std::endl;
    const auto start = std::chrono::high_resolution_clock::now();

    // Control
    const auto dt = 0.002;
    const auto duration = 1.25; // corresponds to 1.25ms
    const auto n_steps = floor(duration/dt) + 1;

    const auto ts = makeTimeControl(n_steps,dt);
    const auto initialAmplitude = 0.55;
    const auto u = initialAmplitude*sin(PI/duration*ts);

    // Hilbert space
    const auto kinFactor = 0.36537;  // T = -kinFactor*d^2/dx^2
    const auto s = gpe::makeHilbertSpace(-2,+2,256,kinFactor);
    const auto x = s.x(); // FunctionOfX of x-grid values

    // Potential
    const auto p2 =  65.8392;
    const auto p4 =  97.6349;
    const auto p6 = -15.3850;

    const auto V_func = [&x,p2,p4,p6](const real u)
    {
        // By saving intermediate calculations we reduce overall computation time
		const auto x_u = x - u;
        const auto x_uPow2 = x_u * x_u;
        const auto x_uPow4 = x_uPow2 * x_uPow2;
        const auto x_uPow6 = x_uPow2 * x_uPow4;

        return  p2*x_uPow2 + p4*x_uPow4 + p6*x_uPow6;
    };

    const auto u_initial = u.getFront().front(); // first entry in first time index
    const auto V = makePotentialFunction(V_func, u_initial);

    // Kinetic energy
    const auto T = s.T();

    // Meanfield interaction
    const auto g1D = 1.8299;

    const auto meanfield = makeGpeTerm(g1D);

    // Hamiltonian
    const auto H = T + V + meanfield;

    // States
    const auto psi_0 = makeWavefunction(H(u.getFront())[0]);
    const auto psi_t = makeWavefunction(H(u.getBack() )[1]);

    // Container
    DataContainer dc;
    dc["dt"] = dt;
    dc["duration"] = duration;
    dc["x"] =  x.vec();
    dc["psi_t"] = psi_t.vec();

    // Time Evolution over control
    auto stepper = makeFixedTimeStepper(H,psi_0,dt);


    // Here we introduce a lambda to propagate over a given control.
    // This reduces boilerplate code, since we need to do the same thing for both the un-optimized uncontrol, and the controls optimized by 3 different optimizers.
    const auto propagateOverControl = [&](auto& u,std::string fieldname)
    {
        stepper.reset(psi_0, u.getFront());
        for(auto i = 0; i < n_steps; i++)
        {
            const auto& psi = normalize( stepper.state() );
            dc["V_" + fieldname].append(V(u.get(i)).vec());
            dc["psis_" + fieldname].append(psi.vec());
            dc["overlap_" + fieldname].append( overlap(psi, psi_t));
            dc["fidelity_"+ fieldname].append(fidelity(psi,psi_t));
            dc["x_expect_"+ fieldname].append(expectationValue(x,psi));
            if(i < n_steps-1) stepper.step(u.get(i+1));
        }

        // Constant control propagation
        const auto n_stepsConst = 0.30*duration/dt;
        const auto ts_const = makeTimeControl(n_stepsConst,dt,duration);
        const auto u_const  = makeLinearControl(u.getBack().at(0),u.getBack().at(0),n_stepsConst,dt); // constant control field from t > duration

        for(auto i = 0; i < n_stepsConst ; i++)
        {
            stepper.cstep();

            const auto psi = normalize( stepper.state() );
            dc["V_"+ fieldname].append(V(u.getBack()).vec());
            dc["psis_"+ fieldname].append(psi.vec());
            dc["overlap_"+ fieldname].append(overlap(psi,psi_t));
            dc["fidelity_"+ fieldname].append(fidelity(psi,psi_t));
            dc["x_expect_"+ fieldname].append(expectationValue(x,psi));
        }

       dc["t"] =  append(ts,ts_const).mat();
       dc["u_" + fieldname] = append(u,u_const).mat();

    };
    propagateOverControl(u,"unopt");

    /// OPTIMAL CONTROL
    bool optimize = true;
    if(optimize)
    {
        const auto dHdu_func = [&x,p2,p4,p6](const real u)
        {
            auto x_u = x-u;
            auto x_u_Pow2 = x_u*x_u;
            auto x_u_Pow3 = x_u*x_u_Pow2;
            auto x_u_Pow5 = x_u_Pow2*x_u_Pow3;

            return  -(2*p2*x_u + 4*p4*x_u_Pow3 + 6*p6*x_u_Pow5);
        };

        const auto dHdu = makeAnalyticDiffPotential(makePotentialFunction(dHdu_func,u_initial));

        auto problem = makeStateTransferProblem(H,dHdu,psi_0,psi_t,u) 
					 + 1e-5*Regularization(u) 
					 + 2e3*Boundaries(u,RVec{-1},RVec{+1});

        const auto stopper = makeStopper([](auto& optimizer) -> bool
        {
                bool stop = false;
                if (optimizer.problem().fidelity() > 0.99) { std::cout << "Fidelity criterion satisfied" << std::endl; stop = true; }
                if (std::abs( optimizer.stepSize() ) < 1e-7)    { std::cout << "Step size too small" << std::endl; stop = true; };
                if (optimizer.iteration() == 200) { std::cout << "Max iterations exceeded" << std::endl; stop = true; }
                // IDEAS FOR OTHER POSSIBLE STOPPING CRITERIA:
                // if (optimizer.problem().gradient().normL2() < 1e-4) { std::cout << "Gradient norm below threshold" << std::endl; stop = true; }
                // if (optimizer.problem().nPropagationSteps() > 3e5) { std::cout << "Max full path propagations exceeded" << std::endl; stop = true; }
                if (stop) std::cout << "STOPPING" << std::endl;
                return stop;
        });

        const auto collector = makeCollector([&dc,n_steps](auto& optimizer) {
            dc["fidelityHistory"].append(optimizer.problem().fidelity());
            std::cout <<
                 "ITER "       << optimizer.iteration() << " | " <<
                 "fidelity : " << optimizer.problem().fidelity() << "\t " <<
                 "stepSize : " << optimizer.stepSize()   << "\t " <<
                 "fpp : "      << round(optimizer.problem().nPropagationSteps()/n_steps)   << "\t " <<
            std::endl;
        });

        const auto maxStepSize = 5.0;
        const auto maxInitGuess = 1.0;
        const auto stepSizeFinder = makeInterpolatingStepSizeFinder(maxStepSize,maxInitGuess);
//        const auto constStepSize=makeStepSizeFinder([](auto& dir,auto& problem,auto& optimizer){return 0.01;});

//        auto bfgsRestarter  = makeStepTimesYBfgsRestarter(1e-9); // restart when l-bfgs curvature condition is violated
//        auto bfgsRestarter  = makeIntervalBfgsRestarter(30); // restart every 30 iterations

        /// GRAPE
        auto GRAPE = makeGrape_bfgs_L2(problem,stopper,collector, stepSizeFinder);
//        auto GRAPE = makeGrape_bfgs_L2(problem, stopper, collector, stepSizeFinder,bfgsRestarter);
        collector(GRAPE);  // collect iteration 0
        GRAPE.optimize(); // begin optimization
        const auto u_grape = GRAPE.problem().control(); // extract GRAPE optimized control

        propagateOverControl(u_grape,"grape");

        /// GROUP
        const auto basisSize = 60; // basis size
        auto maxRand = 0.5; // -maxRand < theta_m < maxRand
        const auto shapeFunction = makeSigmoidShapeFunction(ts);
        const auto basis =  shapeFunction*makeSineBasis(basisSize,u.metaData(),maxRand);


        problem.update(0*u); // update problem such that reference control will be u_0(t)=0

        auto GROUP = makeGroup_bfgs(problem,basis,stopper,collector, stepSizeFinder);
//        auto GROUP = makeGroup_bfgs(problem, basis, stopper, collector, stepSizeFinder, bfgsRestarter);

        // the initial guess is the first element in the basis, so we may set explicity:
        auto cs = GROUP.problem().coefficients();
        cs.at(0).at(0) = initialAmplitude;
        GROUP.problem().update(cs); // set initial coefficient vector \vec c = [initialAmplitude,0,0,...,0]

        collector(GROUP);
        GROUP.optimize(); // begin optimization
        const auto u_group = GROUP.problem().control(); // extract GROUP optimized control
        propagateOverControl(u_group,"group");

        /// dGROUP
		const auto basisMaker = makeRandSineBasisMaker(basisSize, shapeFunction, maxRand);

        auto restart_func = [tol{ 1e-6 }](const auto& dGROUP) mutable
        {
            auto stepSize = dGROUP.stepSize();
            if (stepSize < tol)
            {
                std::cout << "New superiteration in dGROUP algorithm." << std::endl;
                return true;
            }
            return false;
        };

        const auto dRestarter = makeDressedRestarter(restart_func);
        problem.update(0*u);
        auto dGROUP = makeDGroup_bfgs(problem, basisMaker, stopper, collector, stepSizeFinder, dRestarter);
//        auto dGROUP = makeDGroup_bfgs(problem, basisMaker, stopper, collector, stepSizeFinder, dRestarter,bfgsRestarter);

        dGROUP.problem().update(cs); // set initial coefficient vector \vec c = [initialAmplitude,0,0,...,0]

        collector(dGROUP);  // collect iteration 0
        dGROUP.optimize();  // begin optimization

        const auto u_dgroup = dGROUP.problem().control(); // extract dGROUP optimized control
        propagateOverControl(u_dgroup, "dgroup");
    }

    /// Saving
    //dc.save("gpe-example.mat");
    dc.save("gpe-example.json");

    const auto end = std::chrono::high_resolution_clock::now();
    std::cout << "---------------" << std::endl;
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Program execution time: " << elapsed.count() << "s";

}

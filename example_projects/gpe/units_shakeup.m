%clc

%% SI units and quantities
Hz = 1;    % [Hz]
KHz = 1e3; % [Hz]
micrometer = 1e-6; % [m]
nm = 1e-9; % [m]
ms = 1e-3; % [s]

h = 6.626070040e-34; % [J*s]
hbar = h/(2*pi); % [J*s]
mRb = 86.9091835*1.660539040e-27; % [kg]
bohr_radius = 0.529177e-10; % [m]
%aRb = 110*bohr_radius; % [m] scattering length
aRb = 100.44*bohr_radius;% [m] Three-dimensional s-scattering length |1,-1>.                        
g3D = 4*pi*hbar^2*aRb/mRb;

% Conversion factors between SI units
HzFromJoule = 1/h;  JouleFromHz = 1/HzFromJoule;
KHzFromHz = 1e-3;   HzFromKHz = 1/KHzFromHz;
KHzFromJoule = KHzFromHz*HzFromJoule; JouleFromKHz = 1/KHzFromJoule;

%% Quantities

wx = 2*pi*16.3*Hz;       % Axial frequency of the trap (Hz).
wy = 2*pi*1.83*KHz;      % Frequency along the (transverse) y direction (Hz).
wz = 2*pi*2.58*KHz;      % Frequency along the (transverse) z direction (Hz).
wr = sqrt(wy*wz);        %           

ax = sqrt(hbar/mRb/wx);           % Axial oscillator length (m).
az = sqrt(hbar/mRb/wz);           % Oscillator length along the (transverse) z direction (m).
ar = sqrt(hbar/mRb/wr);           % Transverse mean oscillator length (m).

r0 = 172*nm; % 
p2 = h*310   /r0^2*Hz;   % [J/m^2]
p4 = h*13.6  /r0^4*Hz;   % [J/m^4]
p6 = h*0.0634/r0^6*Hz;   % [J/m^6]

Natoms = 700;
falpha=@(alpha)alpha^3*(alpha+5)^2-(15*Natoms*ar*aRb)^2/ax^4 ;
alpha = fzero(falpha,0.5);
L = ax^2*sqrt(alpha)/ar;
Ix = alpha^2*L*(alpha^2 + 9*alpha + 21)/(315*(aRb*Natoms)^2);
g1D = g3D*Ix/(sqrt(2*pi)*az);
%g1D = g1D*Natoms;


x = linspace(-3*micrometer,3*micrometer,10000); 
%% Conversions to simulation units

convert = 1
if convert
    SI = [Natoms,r0, p2, p4, p6, g1D,g1D/Natoms,nan];
    
    % natural units: denoted by e.g. x0
    % simulation quantities: denoted by primes, e.g. x'
    % si units: obtained by multiplying e.g. x=x0*x'

    % fundamental units
    x0 = micrometer;     % x = x0*x' (length) 
    t0 = ms;             % t = t0*t' (time)
    m0 = mRb;            % m = m0*m' (mass)
    
    
    % derived units
    c  = hbar*t0/(2*m0*x0^2);   % kinetic factor 
    E0 = hbar/t0;               % E = E0*E' (energy)
    f0 = 1/t0;                  % f = f0*f' (frequency)
    w0 = 2*pi*f0;               % w = w0*w' (ang. freq.)    
    k0 = 1/x0;                  % k = k0*k' (spat. freq.)   
    
    % Conversion to nondimensionalized simulation quantities (divide all eq.
    % above by x0, e.g. x' = x/x0) 
    % primes are dropped.

    x = x/x0;
    bohr_radius = bohr_radius/x0;
    aRb = aRb/x0;
    
    r0 = r0/x0; 
    p2 = p2/(E0/x0^2);
    p4 = p4/(E0/x0^4);
    p6 = p6/(E0/x0^6);

    g1D = g1D*(Natoms/(E0*x0)); % psi = psi_0* psi' = 1/sqrt(x0) psi' gives the additional x0 scaling
    
    % Tables
    names = {'Length','Time','Energy','Freq.','Ang. freq.','Mass'};
    conversion_factors = [x0,t0,E0,f0,w0,m0];
    Table1 = table(conversion_factors','rowNames',names,'VariableNames',{'SIfromSim_Factors'})

    sim = [Natoms,r0, p2, p4, p6, g1D,g1D/Natoms,c];
    names = {'Natoms','r0', 'p2', 'p4', 'p6', 'g1D','g1D/Natoms','kin factor'};
    Table2 = table(SI',sim','rownames',names,'VariableNames',{'SI','Simulation'})

    KHzFromE0 = KHzFromJoule*E0; E0FromKHz = 1/KHzFromE0;
    
end
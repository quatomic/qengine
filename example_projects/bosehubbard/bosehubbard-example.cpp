﻿/* COPYRIGHT
 *
 * file="bosehubbard-example.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * The contencts of this file are subject to the conditions expressed in the LICENSE file 
 * distributed with the source files. If no such file has been distributed with the source code,
 * you are not allowed to use the code.
 * 
 * THE PROGRAM IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, EXCEPT TO THE EXTENT THAT THESE DISCLAIMERS ARE 
 * HELD TO BE LEGALLY INVALID.
 * The Licensor shall not be liable for any direct or indirect consequences of any use or 
 * improper use of the Program and/or damage caused to the Licensee (including, but not 
 * limited to planet destruction) and/or third parties as a result of any use or disuse of 
 * the Program including the possible faults or failures of the Program functioning to the 
 * maximum extent allowed by the applicable legislation.
 */
#include <qengine/qengine.h>
#include <iostream>

using namespace qengine;
using namespace fock;

int main()
{
    std::cout << "Bose-Hubbard Example Program" << std::endl << "---------------" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    // Control field. The physical interation strength is parameterized by the control u(t) as U(u(t)).
    // Using the transformation squeezes the values of U into the range [Umin,Umax]
    const auto Umin = 2.0;
    const auto Umax = 40.0;

    const auto B = (1+Umin/Umax)/(1-Umin/Umax); // transformation params
    const auto A = Umax/(1+B);

    const auto UFromu = [A,B](auto u){return A*(tanh(u) + B);}; // control to physical U
    const auto uFromU = [A,B](auto U){return atanh(U/A-B);};    // physical U to control

    const auto dt = 0.002;
    const auto duration = 2.0;
    const auto n_steps = floor(duration / dt) + 1;

    const auto ts = makeTimeControl(n_steps, dt);
    const auto u = uFromU(Umin + 0.5*exp(log((30-Umin)/0.5)*ts/duration));

    // Hilbert space
    const auto space = bosehubbard::makeHilbertSpace(5,5);

    // The initialization of operators and states can either be done manually (commented out below)
    // by explicitly constructing the Hamiltonian from creation and annihilation operators,
    // or automatically by using the appropriate API.

    /// MANUAL SETUP OF OPERATORS AND STATES (SLOW FOR #particles,#sites > 6)
    //    const auto abstractHoppingOperator =
    //              c(1)*a(0) + c(0)*a(1)
    //            + c(2)*a(1) + c(1)*a(2)
    //            + c(0)*a(2) + c(2)*a(0);

    //    const auto abstractOnSiteOperator =
    //              n(0)*(n(0)-1)
    //            + n(1)*(n(1)-1)
    //            + n(2)*(n(2)-1);

    //    auto hoppingOperator(makeOperator(space,abstractHoppingOperator));
    //    auto onSiteOperator(makeOperator(space,abstractOnSiteOperator));

    //    // Want to create SF and Mott gs:
    //    auto SF_operator = fock::zero();
    //    for(auto i =0; i < space.nSites(); ++i)
    //    {
    //        SF_operator += c(i);
    //    }
    //    SF_operator = pow(SF_operator,space.nParticles());
    //    auto SF   = makeState(space,SF_operator*fock::state{0,0,0},NORMALIZE);
    //    auto MOTT = makeState(space,fock::state{1,1,1},NORMALIZE);


    /// AUTOMATED SETUP OF OPERATORS AND STATES
    const auto periodicBoundaries = false;
    const auto hoppingOperator = space.makeHoppingOperator(periodicBoundaries);
    const auto onSiteOperator  = space.makeOnSiteOperator();

    // Weak harmonic confinement
    const auto sitePositions = linspace(-1.0, +1.0, space.nSites());
    const auto potential = 0.1*pow(sitePositions,2);
    const auto V = space.transformPotential(potential); // transform to site indices

    const auto H_J = -1.0* hoppingOperator; // J = 1.0

    const auto H_const = H_J + V; // Constant parts of Hamiltonian is added

    // Hamiltonian as function of u
	const auto H_func = [&H_const, &onSiteOperator, &UFromu](const real u)
	{
		return H_const + 0.5*UFromu(u)* onSiteOperator;
	};
	
	const auto H = makeOperatorFunction(H_func,u.getFront().front());

    const auto psi_0 = makeState(H(u.getFront())[0]);
    const auto psi_t = makeState(H(u.getBack() )[0]);
	//const auto psi_t = space.makeMott();

    // Solver
    const auto krylovOrder = 4;
    auto stepper = makeFixedTimeStepper(H,psi_0,krylovOrder,dt);

    // Propagate over control
    DataContainer dc;
    dc["dt"]=dt;
    dc["duration"] = duration;

    const auto propagateOverControl = [&](auto& u,std::string fieldname)
    {
        stepper.reset(psi_0, u.getFront());
        for(auto i = 0; i < n_steps; i++)
        {
            const auto& state = stepper.state();
            dc["rho1_" + fieldname ].append(space.singleParticleDensityMatrix(state));
            if(i < n_steps-1) stepper.step(u.get(i+1));
        }

        dc["fidelity_" + fieldname] =fidelity(stepper.state(),psi_t);


        const auto n_stepsConst = 0.25*duration/dt;


        for(auto i = 0; i < n_stepsConst ; i++)
        {
            stepper.cstep();
            const auto& state = stepper.state();
            dc["rho1_" + fieldname].append(space.singleParticleDensityMatrix(state));
        }

       const auto ts_const = makeTimeControl(n_stepsConst,dt,duration);
       const auto u_const  = makeLinearControl(u.getBack().at(0),u.getBack().at(0),n_stepsConst,dt);

       dc["U_" + fieldname] = UFromu(append(u,u_const)).mat();
       dc["t"] = append(ts,ts_const).mat();
    };

    propagateOverControl(u,"unopt");

    /// OPTIMAL CONTROL
    bool optimize = true;
    if(optimize)
    {
//        auto dUdu   = [A,B](real u){return A*(1-tanh(u)*tanh(u));}; // single number derivative
//		auto dHdu_lambda = [&hoppingOperator,&onSiteOperator,dUdu](const RVec& p)
//        {
//            auto u = p.at(0);
//            return 0.5*dUdu(u)*onSiteOperator;
//		};
//        auto dHdu = makeAnalyticDiffHamiltonian(dHdu_lambda);
//        auto problem = makeStateTransferProblem(H, dHdu, psi_0, psi_t, u, krylovOrder);

        auto problem = makeStateTransferProblem(H,psi_0,psi_t,u,krylovOrder);

//        auto stopper = makeStopper(makeIterationStopper(100) + makeFidelityStopper(0.999));
//        auto collector = makeCollector([&](auto& opt){std::cout << opt.iteration() << " F = " << opt.problem().fidelity() << std::endl;});

        /// GRAPE
        auto GRAPE = makeGrape_bfgs_L2(problem);
        GRAPE.optimize(); // begin optimization
        const auto u_grape = GRAPE.problem().control(); // extract GRAPE optimized control
        propagateOverControl(u_grape,"grape");

        /// GROUP
        const auto M = 60; // basis size
        const auto shapeFunction = makeSigmoidShapeFunction(ts);
        const auto maxRand = 0.5; // -maxRand < theta_m < maxRand
        const auto basis =  shapeFunction*makeSineBasis(M,u.metaData(),maxRand);

        auto GROUP = makeGroup_bfgs(problem,basis);

        GROUP.optimize(); // begin optimization
        const auto u_group = GROUP.problem().control(); // extract GROUP optimized control
        propagateOverControl(u_group,"group");

        /// dGROUP
        auto basisMaker = makeBasisMaker([M,maxRand,&u, &shapeFunction]()
        {
            return shapeFunction*makeSineBasis(M, u.metaData(), maxRand);
        });

        auto dGROUP = makeDGroup_bfgs(problem,basisMaker);
		dGROUP.optimize(); // begin optimization
        const auto u_dgroup = dGROUP.problem().control(); // extract dGROUP optimized control
        propagateOverControl(u_dgroup,"dgroup");

    }

    //dc.save("bosehubbard-example.mat");
    dc.save("bosehubbard-example.json");

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "---------------" << std::endl;
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Program execution time: " << elapsed.count() << "s" << std::endl;
}

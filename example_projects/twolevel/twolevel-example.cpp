﻿/* COPYRIGHT
 *
 * file="twolevel-example.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * The contencts of this file are subject to the conditions expressed in the LICENSE file 
 * distributed with the source files. If no such file has been distributed with the source code,
 * you are not allowed to use the code.
 * 
 * THE PROGRAM IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, EXCEPT TO THE EXTENT THAT THESE DISCLAIMERS ARE 
 * HELD TO BE LEGALLY INVALID.
 * The Licensor shall not be liable for any direct or indirect consequences of any use or 
 * improper use of the Program and/or damage caused to the Licensee (including, but not 
 * limited to planet destruction) and/or third parties as a result of any use or disuse of 
 * the Program including the possible faults or failures of the Program functioning to the 
 * maximum extent allowed by the applicable legislation.
 */
#include <qengine/qengine.h>
#include <iostream>

using namespace qengine;
using namespace std::complex_literals;

int main()
{
	std::cout << "2 Level Example Program" << std::endl << "---------------" << std::endl;
	auto start = std::chrono::high_resolution_clock::now();

	// Control function
	const auto dt = 0.002;
	const auto T = PI;
	const auto n_steps = floor(T / dt) + 1;

	const auto ts = makeTimeControl(n_steps, dt);
	const auto u = sin(ts) + 0.3*cos(3 * ts) + 0.1*sin(50 * 2 * PI / T * ts);

	// Hilbert space
	const auto space = n_level::ApiHilbertSpace(2);

	// LZ-Hamiltonian
	const auto rabiFreq = 1.0;
	const auto PauliX = n_level::makePauliX();
	const auto PauliY = n_level::makePauliY();
	const auto PauliZ = n_level::makePauliZ();
	const auto H = makeOperatorFunction([&rabiFreq, &PauliX, &PauliY, &PauliZ](const real detuning)
	{
		return 0.5*(rabiFreq*PauliX + detuning * PauliZ);
	}, u.getFront().front());

	// States
    const auto spectrum = PauliZ.makeSpectrum(2);
    const auto psi_0   = spectrum.eigenState(0);
    const auto psi_t = spectrum.eigenState(1);

//    const auto psi_0 = makeState(space, {1, 0});  // create states from list instead
//    const auto psi_t = makeState(space, {0, 1});


	// Datacontainer and saving of initial data
	DataContainer dc;
    dc["u"] = u.mat();
    dc["psi_t"] = psi_t.vec();
    dc["R_t"] = RVec{
                  expectationValue(PauliX,psi_t),
                  expectationValue(PauliY,psi_t),
                  expectationValue(PauliZ,psi_t)};

	// Propagation along control and saving
    auto solver = makeFixedTimeStepper(H, psi_0, dt);
	auto t = 0.0;
	for (auto i = 0; i < u.size(); i++)
	{
		const auto state = solver.state();
		dc["psi"].append(state.vec());
		dc["R"].append(RVec{
					  expectationValue(PauliX,state),
					  expectationValue(PauliY,state),
					  expectationValue(PauliZ,state)
			});
		dc["t"].append(t);

		if (i < u.size() - 1) solver.step(u.get(i + 1));
		t += dt;
	}

	/// OPTIMAL CONTROL
//    The commented lines below allows the user to provide an analytic expression for the derivative dHdu to the program

//    const auto dHdu = makeAnalyticDiffHamiltonian([&PauliZ](const auto& p){return 0.5*PauliZ;});
//    auto problem = makeStateTransferProblem(H,dHdu, psi_init,psi_target,u);

    auto problem = makeStateTransferProblem(H, psi_0, psi_t, u);
	const auto stopper = makeStopper([&dc](const auto& optimizer)
	{
		bool stop = false;
		if (optimizer.problem().fidelity() > 0.9999) { std::cout << "Fidelity criterion satisfied" << std::endl; stop = true; };
		if (optimizer.iteration() == 300) { std::cout << "Max iterations exceeded" << std::endl; stop = true; };
		//        if(optimizer.problem().gradient().normL2() < 1e-4) {std::cout << "Gradient norm below threshold"       << std::endl; stop=true;}
		//        if(optimizer.problem().nPropagationSteps() > 2e5)  {std::cout << "Max full path propagations exceeded" << std::endl; stop=true;}
		return stop;
	});
    const auto collector = makeCollector([&dc,n_steps](const auto& optimizer)
	{
		dc["fidelity"].append(optimizer.problem().fidelity());
        std::cout <<
             "ITER "       << optimizer.iteration() << " | " <<
             "fidelity : " << optimizer.problem().fidelity() << "\t " <<
             "stepSize : " << optimizer.stepSize()   << "\t " <<
             "fpp : "      << round(optimizer.problem().nPropagationSteps()/n_steps)   << "\t " <<
        std::endl;
	});

	const auto stepSizeFinder = makeInterpolatingStepSizeFinder(1000, 0.1);
	auto optimizer = makeGrape_steepest_L2(problem, stopper, collector, stepSizeFinder);

	collector(optimizer);
	optimizer.optimize();
	auto u_opt = optimizer.problem().control();

	/// Propagate over optimized control
    solver.reset(psi_0, u_opt.getFront());
	dc["u_opt"] = u_opt.mat();

	for (auto i = 0; i < u_opt.size(); i++)
	{
		auto state = solver.state();
		dc["psi_opt"].append(state.vec());
		dc["R_opt"].append(RVec{
					  expectationValue(PauliX,state),
					  expectationValue(PauliY,state),
					  expectationValue(PauliZ,state)
			});

		if (i < u_opt.size() - 1) solver.step(u_opt.get(i + 1));
	}

	auto end = std::chrono::high_resolution_clock::now();
	std::cout << "---------------" << std::endl;
	std::chrono::duration<double> elapsed = end - start;
    std::cout << "Program execution time: " << elapsed.count() << "s";

    //dc.save("twolevel-example.mat");
	dc.save("twolevel-example.json");

	return 0;
}

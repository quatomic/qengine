﻿/* COPYRIGHT
 *
 * file="twoparticle-example.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * The contencts of this file are subject to the conditions expressed in the LICENSE file 
 * distributed with the source files. If no such file has been distributed with the source code,
 * you are not allowed to use the code.
 * 
 * THE PROGRAM IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, EXCEPT TO THE EXTENT THAT THESE DISCLAIMERS ARE 
 * HELD TO BE LEGALLY INVALID.
 * The Licensor shall not be liable for any direct or indirect consequences of any use or 
 * improper use of the Program and/or damage caused to the Licensee (including, but not 
 * limited to planet destruction) and/or third parties as a result of any use or disuse of 
 * the Program including the possible faults or failures of the Program functioning to the 
 * maximum extent allowed by the applicable legislation.
 */
#include <qengine/qengine.h>

#include <iostream>

using namespace qengine;

int main()
{
    std::cout << "Twoparticle Example Program" << std::endl << "---------------" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    // Control
    const auto dt = 0.001;
    const auto duration = 1.0;
    const auto n_steps = floor(duration/dt) + 1;

    const auto ts = makeTimeControl(n_steps,dt); // only used for saving

    const auto V0Scale = 174.6219;
    const auto initParams  = RVec{1.0, 0.0,    -0.474*PI }; // V0, beta, theta
    const auto finalParams = RVec{1.0, 0.5*PI, -0.474*PI };

    const auto u_beta  = makeLinearControl(initParams.at(0),finalParams.at(0),n_steps,dt);
    const auto u_theta = makeLinearControl(initParams.at(1),finalParams.at(1),n_steps,dt);
    const auto u_V0    = makeLinearControl(initParams.at(2),finalParams.at(2),n_steps,dt);
    const auto u = makeControl({u_beta,u_theta,u_V0});

    // Space
    const auto xMin = -1;
    const auto xMax = +1;
    const auto padding = +0.2;
    const auto kinFactor = 0.5;
    const auto s = two_particle::makeHilbertSpace(xMin-padding,xMax+padding,64,kinFactor);
    const auto x = s.xPotential(); // make detailed comment about this (xV?)

    // Potentials
    const auto k = PI;
    const auto phaseShift = -0.5*PI;
    const auto y = 0.0;

    const auto Vext = [&x,k,y,padding,xMin,xMax,phaseShift, V0Scale](const RVec& p) // deChiara potential
        {
            const auto V0    = p.at(0)*V0Scale;
            const auto beta  = p.at(1);
            const auto theta = p.at(2);

            const auto deChiara = -V0*(
                        pow(cos(beta/2.0),2)*(pow(cos(k*y),2) + pow(cos(k*x + phaseShift),2))
                     +  pow(sin(beta/2.0),2)*pow(cos(k*y) + cos(k*x - theta + phaseShift),2)
                        );

            const auto boundaryExtension = box(x,xMin,xMax);
            const auto boundaryOffsetL = max(deChiara)*(step(x,xMin-2*padding) - step(x,xMin));
            const auto boundaryOffsetR = max(deChiara)*(step(x,xMax) - step(x,xMax+2*padding));

            return boundaryExtension*deChiara + boundaryOffsetL + boundaryOffsetR ;
        };



    // Point interaction (from old matlab)
    const auto omegaY = [&x,k,phaseShift,V0Scale](const RVec& p)
    {
        const auto V0    = p.at(0)*V0Scale;
        const auto beta  = p.at(1);
        const auto theta = p.at(2);

        //return sqrt(2*V0*k*k)*sqrt(1+pow(sin(0.5*beta),2)*cos(k*xPotential - theta + phaseShift));
        return sqrt(2*V0*k*k*(
                        pow(cos(0.5*beta),2)+
                        pow(sin(0.5*beta),2)*
                        (1+cos(k*x - theta + phaseShift))));
    };
    const auto omegaZ = 72.5259523429781; // Corresponds to Vz = 266.475410234039
    const auto aRb = 0.0116730220588235;     // corresponds to 90 bohr radii

    const auto g1D = [&omegaY,g1D_constantPart{sqrt(2.0*kinFactor)*2*aRb*sqrt(omegaZ)}](const RVec& p)
    {
        return g1D_constantPart*sqrt(omegaY(p));
    };

    const auto V = makePotentialFunction([&Vext,&g1D](const RVec& p)->PotentialWithInteraction
    {
        return Vext(p) + two_particle::makePointInteraction(g1D(p));
    }, initParams); // 2p potential with interaction

    // Hamiltonian
    const auto H  = s.T()  + V; // 2p Hamiltonian, includes interaction

    // 2p states
    const auto eigenspectrum_0 = H(initParams).makeSpectrum(9,0,1e-5);
    const auto psi_0 = eigenspectrum_0.eigenFunction(0);

    const auto eigenspectrum_t = H(finalParams).makeSpectrum(9,0,1e-5);
    const auto psi_t = eigenspectrum_t.eigenFunction(2);

    // Alternative syntax
    //    const auto H_init = H(initParams);
    //    const auto psi_init = makeWavefunction(H_init[0], NORMALIZE, 10);
    //    const auto H_target = H(finalParams);
    //    const auto psi_target = makeWavefunction(H_target[2], NORMALIZE, 10);

    DataContainer dc;
    dc["x"] = x.vec();
    dc["psi_0"] = psi_0.vec();
    dc["psi_t"] = psi_t.vec();

    auto stepper = makeFixedTimeStepper(H,psi_0,dt);
    const auto propagateOverControl = [&](auto& u,std::string fieldname)
    {
        stepper.reset(psi_0, u.getFront());
        for(auto i = 0; i < n_steps; i++)
        {
            dc["Vext_" + fieldname].append(Vext(u.get(i)).vec());
            dc["g1D_" + fieldname].append(g1D(u.get(i)).vec());
            dc["fidelity_" + fieldname].append(fidelity(stepper.state(), psi_t)); // saving wave function at each t is expensive, save fidelity instead
            if(i < n_steps-1) stepper.step(u.get(i+1));
        }

       dc["u_" + fieldname]= u.mat();
    };

    propagateOverControl(u,"unopt");

    /// Optimal Control
    bool optimize = true;
	if (optimize)
	{
        const auto problem = makeStateTransferProblem(H, psi_0, psi_t, u);
        const auto stepSizeFinder = makeInterpolatingStepSizeFinder(1.0,1.0);
		const auto stopper = makeStopper([](const auto& optimizer)
		{
			bool stop = false;
            if (optimizer.problem().fidelity() > 0.99) { std::cout << "Fidelity criterion satisfied" << std::endl; stop = true; }
            if (optimizer.iteration() == 100) { std::cout << "Max iterations exceeded" << std::endl; stop = true; }
//			if (optimizer.problem().gradient().normL2() < 1e-4) { std::cout << "Gradient norm below threshold" << std::endl; stop = true; }
//			if (optimizer.problem().nPropagationSteps() > 3e5) { std::cout << "Max full path propagations exceeded" << std::endl; stop = true; }
            if (optimizer.stepSize() < 1e-7) {std::cout << "Step size too small" << std::endl; stop = true;}
            if (stop) std::cout << "STOPPING" << std::endl;
			return stop;
		});

        const auto collector = makeCollector([&dc,n_steps](const auto& optimizer)
		{
            dc["iterationFidelity"].append( optimizer.problem().fidelity());
            std::cout <<
                 "ITER "       << optimizer.iteration() << " | " <<
                 "fidelity : " << optimizer.problem().fidelity() << "\t " <<
                 "stepSize : " << optimizer.stepSize()   << "\t " <<
                 "fpp : "      << round(optimizer.problem().nPropagationSteps()/n_steps)   << "\t " <<
            std::endl;
        });

        auto GRAPE = makeGrape_bfgs_L2(problem, stopper, collector, stepSizeFinder);
        collector(GRAPE);
        GRAPE.optimize();
        const auto u_opt = GRAPE.problem().control();
        propagateOverControl(u_opt,"grape");
    }

//    dc.save("twoparticle-example.mat");
    dc.save("twoparticle-example.json");

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "---------------" << std::endl;
    std::cout << "Program execution time: " << elapsed.count() << "s" <<std::endl;
}

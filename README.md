QEngine - A C++ Library for Quantum Optimal Control
===================================================
QEngine is a C++ library for simulating and controlling ultracold quantum systems using optimal control theory.
This includes Bose-Einstein condensates, many-body systems described by Bose-Hubbard type models, and two interacting particles. 
The QEngine provides a number of optimal control algorithms including the recently introduced GROUP method. 
The QEngine library has a strong focus on accessibility and performance. We provide several examples of how to prepare simulations of the physical systems and apply optimal control.

Download
--------
On all platforms you will need git to get the source code. We use submodules so downloading the code manually is not recommended.
Navigate to the folder where you want to download the QEngine and run the git-commands,
```bash
git clone "https://gitlab.com/quatomic/qengine.git"
git submodule init
git submodule update
```
On Linux and Mac this can be run in the appropriate shell. On Windows your git-install likely comes with a separate command line. Alternatively, use a GUI such as Sourcetree or Tortoise.

Reference
--------
A reference for the QEngine library is avaliable at,
- https://www.quatomic.com/qengine/reference/.

How to Build
------------
The build process for the QEngine uses cmake. It has been tested on Windows with latest msvc, on Ubuntu with gcc 8.2.0 and clang 7.0, and on Mac with clang 5.9.1.
1. You will need to install the Intel MKL from https://software.intel.com/en-us/mkl. This comes with an installer for Windows, Linux, and Mac.

2. The library and example projects are build using cmake. If you use an IDE that can open cmake projects directly, such as QtCreator, Visual Studio, VsCode, or CLion, you can just open the project and build it as you are used to. If you prefer to build from the command line use,
```bash
mkdir build
cd build
cmake ..
make
```
If you want to create a project for a certain IDE, you can run cmake from the command line (e.g. for use with visual studio) and pass a generator (see e.g. https://cmake.org/cmake/help/v3.10/manual/cmake-generators.7.html).
To create a visual studio solution you would create a build-folder (e.g. build_win) navigate to it from your preferred shell, and run
```bash
cmake .. -A x64
```

3. However you chose to build the project, it should have created two folders in you QEngine directory: 'bin' and 'lib'. The 'lib' folder contains the QEngine library, while 'bin' contains the example-projects (for visual studio, this is split into debug and release folders) and tests.

4. To use the QEngine in you own projects, you will have to include the include-folder and link to the QEngine library in the lib-directory. You will also have to link to the MKL, for an example of how to do this see the cmakelists in the example projects.

Advanced Build
--------------
The QEngine build can be personalized in several ways,
- you can allow saving to MATLAB-files (requires matio).
- you can ignore the example projects and/or the test-suite (to speedup your build).
- you can disable debug checks to improve runtime speed slightly (just make sure your code is stable).
- you can tell the build where you installed MKL, if you did not install it to the default directory.
- you can also tell setup where to find armadillo and json-libraries, if you did not get them as submodules.
	
These settings can be set in a settings.txt file, which will then be included by cmake. For you convenience we have created some templates for different platforms, so you can just copy the template_settings_{PLATFORM}.txt for your platform, rename it to settings.txt and edit the contents to your liking.	

Depending on you settings, you may require additional dependencies, which you can set in the settings file.

- If you want to save QEngine-data to MATLAB datafiles (.mat), you will have to get and build the matio library (https://github.com/tbeu/matio). Note, building matio is a bit of a hassle, as it requires hdf5 and zlib as well. Saving to .json is built into the QEngine without matio.


Troubleshooting
---------------
- If any of the folders armadillo, json, or AddPCH are empty, something went wrong in getting the git submodules. Try running,
```bash
git submodule init
git submodule update
```

- If you encounter other problems, want to report a bug, or propose a feature, create an issue on gitlab using one of our templates at https://gitlab.com/quatomic/qengine/issues/.


License
-------

The QEngine is licensed under the Mozilla public license. See LICENSE for details, or go to https://www.mozilla.org/en-US/MPL/2.0/


File and directory structure
----------------------------
The following gives an overview of the files and directories that make up the QEngine. Only the the headers in the 'include/qengine' folder are part of the public interface. 

A: Overall folder structure
-------------------
- include  - contains all the code that makes up the QEngine proper.
 - qengine - interface headers for including by users.
 - src - implementation files.
- example_projects - contains example projects showcasing advanced use cases for the QEngine.
- cmakeModules - small cmake-helpers.
- dependencies - 3rd party libraries as submodules or directly included.
- test - testing-project for the QEngine. Contains tests for most aspects of physics-simulation and as much optimal control as can be tested.
   
B: public include headers
-------------------------
All inside folder include/qengine

- BoseHubbard.h - interface header for Bose-Hubbard physics 
- BoseHubbard_OC.h - interface header for optimal control of Bose-Hubbard physics 
- GPE.h - interface header for GPE physics 
- GPE_OC.h - interface header for optimal control of GPE physics 
- NLevel.h - interface header for n-level physics 
- NLevel_OC.h - interface header for optimal control of n-level physics 
- OneParticle.h - interface header for one-particle physics 
- OneParticle_OC.h - interface header for optimal control of one-particle physics 
- TwoParticle.h - interface header for two-particle physics 
- TwoParticle_OC.h - interface header for optimal control of two-particle physics 
- OptimalControl.h - interface header for optimal control 
- DataContainer.h - interface header for file IO with qengine qengine.h - interface for the full qengine-library

C: internal implementation files
--------------------------------
All files inside folder include/src

- *DataContainer* - implementation of the DataContainer-class and serialization.
- *Generics* - generic aspects of the simulation, that are not specific to one of the physics aspects or OC - algorithms.
    - *OptimalControl* - generic OC aspects.
        - *Algorithms* - generic optimization algorithms.
        - *Problems* - General problems and helpers for them.
    - *Physics* - generic aspects of physics simulations.
        - *General* - stuff used for both wave mechanics and second quantized physics.
        - *SecondQuantization* - generic second quantized aspects.
        - *Spatial* - generic aspects of spatial (wave mechanics) simulations.
- *Specifics* - specific simulations building on the generics.
    - *Algorithms* - specific quantum optimal control algorithms.
        - *Policies* - hooks for the algorithms.
    - *SecondQuantized* - specifics for second quantized simulations.
        - *BoseHubbard* - specifics for bosehubbard simulations.
        - *NLevel* - specifics for n-level simulations.
    - *Spatial* - specifics for wave mechanics. 
        - *1D* - one dimensional wave mechanics.
            - *Gpe* - pertaining to the Gross-Pitaevskii equation.
            - *OneParticle* - specifics for the single particle schroedinger equation.
        - *2D* - two dimensional wave mechanics.
            - *TwoParticle* - schroedinger equation with two particles on the same grid with point interactions.
- *Utility* - helpers for the rest of the code.
    - *nostd* - helpers from the c++17 stdlib that works in c++14.
    - *Fft* - fast fourier-transform wrapper.
    - *LinearAlgebra* - linear algebra wrappers.
    - *cpp17Replacements* - functionality that simulates c++17 functionality.

D: Example programs
-------------------
Inside folder example_programs there are folders for example programs with accompanying matlab programs for data-visualization

- bosehubbard
- gpe
- oneparticle
- twolevel
- twoparticle

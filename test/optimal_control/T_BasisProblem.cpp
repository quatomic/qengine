﻿/* COPYRIGHT
 *
 * file="T_BasisProblem.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "testFunctions.h"
#include "src/Abstract/OptimalControl/Problems/BasisProblem.h"

using namespace qengine;


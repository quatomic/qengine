﻿/* COPYRIGHT
 *
 * file="T_Boundaries.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/Utility/Constants.h"
#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Abstract/OptimalControl/Parameters/Control.h"
#include "src/Abstract/OptimalControl/Problems/Boundaries.h"
#include "src/API/Common/GeneralHelpers.h"

using namespace qengine;


class Boundaries_Fixture : public ::testing::Test
{
public:
	count_t controllength = 201;

	RVec lowerleft{ -1.0 };
	RVec upperright{ 1.0 };

	real dt = 0.01;
};

static_assert(internal::is_QOCProblem_v<Boundaries>, "");

TEST_F(Boundaries_Fixture, cost)
{
	/// control is within bounds
	auto control = Control::zeros(controllength, 1, dt);

	auto boundaries = Boundaries(control, lowerleft, upperright, 1.0);

	EXPECT_NEAR(0.0, boundaries.cost(), 1e-16);

	/// control with a single point outside bounds
	control.at(controllength / 2) = RVec{ 2.0 };

	boundaries.update(control);
	EXPECT_NEAR(0.5, boundaries.cost(), 1e-16);

	/// control with a two points outside bounds
	control.at(0) = RVec{ -2.0 };

	boundaries.update(control);
	EXPECT_NEAR(1.0, boundaries.cost(), 1e-16);

	/// higher frontfactor
	boundaries.factor() = 2.0;
	EXPECT_NEAR(2.0, boundaries.cost(), 1e-16);
}

TEST_F(Boundaries_Fixture, gradient)
{	
	/// control is within bounds
	auto control = Control::zeros(controllength, 1, dt);

	auto boundaries = Boundaries(control, lowerleft, upperright, 1.0);
	EXPECT_NEAR(0, gradientDifference(boundaries), 1e-10);

	auto gradient = boundaries.gradient();

	for (auto i = 0u; i < control.size(); ++i)
	{
		EXPECT_NEAR(0.0, gradient.at(i).at(0), 1e-16);
	}

	/// control with a single point outside bounds
	control.at(control.size() / 2) = RVec{ 2.0 };
	boundaries.update(control);
	EXPECT_NEAR(0, gradientDifference(boundaries), 1e-10);

	gradient = boundaries.gradient();
	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == control.size() / 2)
		{
			EXPECT_NEAR(1.0, gradient.at(i).at(0), 1e-16) << i;
		}
		else 
		{
			EXPECT_NEAR(0.0, gradient.at(i).at(0), 1e-16);
		}
	}

	/// control with a two points outside bounds
	control.at(1) = RVec{ -2.0 };

	boundaries.update(control);
	EXPECT_NEAR(0, gradientDifference(boundaries), 1e-10);

	gradient = boundaries.gradient();

	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == control.size() / 2)
		{
			EXPECT_NEAR(1.0, gradient.at(i).at(0), 1e-16) << i;
		}		
		else if (i == 1)
		{
			EXPECT_NEAR(-1.0, gradient.at(i).at(0), 1e-16) << i;
		}
		else
		{
			EXPECT_NEAR(0.0, gradient.at(i).at(0), 1e-16) << i;
		}
	}

	/// higher frontfactor
	boundaries.factor() = 2.0;
	EXPECT_NEAR(0, gradientDifference(boundaries), 1e-10);

	gradient = boundaries.gradient();
	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == control.size() / 2)
		{
			EXPECT_NEAR(2.0, gradient.at(i).at(0), 1e-16) << i;
		}
		else if (i == 1)
		{
			EXPECT_NEAR(-2.0, gradient.at(i).at(0), 1e-16) << i;
		}
		else
		{
			EXPECT_NEAR(0.0, gradient.at(i).at(0), 1e-16) << i;
		}
	}
}
TEST_F(Boundaries_Fixture, gradientVsNumeric)
{
	const auto control = makeControl({2*sin(qengine::linspace(0,10, controllength)) }, dt);
	auto boundaries = Boundaries(control, lowerleft, upperright, 1.0);
	auto gradDiff = gradientDifference(boundaries);
	
	ASSERT_NEAR(gradDiff, 0.0, 4e-9);
}

TEST_F(Boundaries_Fixture, indexedCost)
{
	/// control is within bounds
	auto control = Control::zeros(controllength, 1, dt);

	auto boundaries = Boundaries(control, lowerleft, upperright, 1.0);
	for (auto i = 0u; i < control.size(); ++i)
	{
		EXPECT_NEAR(0.0, boundaries._cost(i), 1e-16);
	}

	/// control with a single point outside bounds
	control.at(controllength / 2) = RVec{ 2.0 };

	boundaries.update(control);
	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == controllength / 2)
		{
			ASSERT_NEAR(0.5, boundaries._cost(i), 1e-16);
		}
		else
		{
			ASSERT_NEAR(0.0, boundaries._cost(i), 1e-16);
		}
	}
	/// control with a two points outside bounds
	control.at(0) = RVec{ -2.0 };

	boundaries.update(control);
	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == controllength / 2)
		{
			ASSERT_NEAR(0.5, boundaries._cost(i), 1e-16);
		}
		else if (i == 0)
		{
			ASSERT_NEAR(0.5, boundaries._cost(i), 1e-16);
		}
		else
		{
			ASSERT_NEAR(0.0, boundaries._cost(i), 1e-16) << i;
		}
	}


	/// higher frontfactor
	boundaries.factor() = 2.0;	
	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == controllength / 2)
		{
			ASSERT_NEAR(1.0, boundaries._cost(i), 1e-16);
		}
		else if (i == 0)
		{
			ASSERT_NEAR(1.0, boundaries._cost(i), 1e-16);
		}
		else
		{
			ASSERT_NEAR(0.0, boundaries._cost(i), 1e-16) << i;
		}
	}
}

TEST_F(Boundaries_Fixture, indexedGradient)
{
	/// control is within bounds
	auto control = Control::zeros(controllength, 1, dt);

	auto boundaries = Boundaries(control, lowerleft, upperright, 1.0);

	for (auto i = 0u; i < control.size(); ++i)
	{
		ASSERT_NEAR(0.0, boundaries._gradient(i).at(0), 1e-16);
	}


	/// control with a single point outside bounds
	control.at(controllength / 2) = RVec{ 2.0 };
	boundaries.update(control);

	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == controllength / 2)
		{
			ASSERT_NEAR(1.0, boundaries._gradient(i).at(0), 1e-16);
		}
		else
		{
			ASSERT_NEAR(0.0, boundaries._gradient(i).at(0), 1e-16);
		}
	}

	/// control with a two points outside bounds
	control.at(1) = RVec{ -2.0 };

	boundaries.update(control);
	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == controllength / 2)
		{
			ASSERT_NEAR(1.0, boundaries._gradient(i).at(0), 1e-16) << i;
		}
		else if (i == 1)
		{
			ASSERT_NEAR(-1.0, boundaries._gradient(i).at(0), 1e-16) << i;
		}
		else
		{
			ASSERT_NEAR(0.0, boundaries._gradient(i).at(0), 1e-16) << i;
		}
	}

	/// higher frontfactor
	boundaries.factor() = 2.0;

	for (auto i = 0u; i < control.size(); ++i)
	{
		if (i == controllength / 2)
		{
			ASSERT_NEAR(2.0, boundaries._gradient(i).at(0), 1e-16) << i;
		}
		else if (i == 1)
		{
			ASSERT_NEAR(-2.0, boundaries._gradient(i).at(0), 1e-16) << i;
		}
		else
		{
			ASSERT_NEAR(0.0, boundaries._gradient(i).at(0), 1e-16) << i;
		}
	}
}


TEST_F(Boundaries_Fixture, multiply)
{
	/// control with a two points outside bounds
	auto control = Control::zeros(controllength, 1, dt);
	control.at(0) = RVec{ -2.0 };
	control.at(controllength / 2) = RVec{ 2.0 };

	auto boundaries = Boundaries(control, lowerleft, upperright, 1.0);

	EXPECT_NEAR(1.0, boundaries.cost(), 1e-16);

	boundaries *= 2.0;
	EXPECT_NEAR(2.0, boundaries.cost(), 1e-16);

	boundaries = 2.0*boundaries;
	EXPECT_NEAR(4.0, boundaries.cost(), 1e-16);
}

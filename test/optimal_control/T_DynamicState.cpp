﻿/* COPYRIGHT
 *
 * file="T_DynamicState.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <qengine/OneParticle.h>

#include "src/NumberTypes.h"

#include "src/API/UserUtilities/AutoMember.h"


#include "src/Abstract/OptimalControl/Parameters/Control.h"
#include "src/Abstract/OptimalControl/Problems/DynamicState.impl.h"

using namespace qengine;


template<class T>
auto makeHamiltonian(const count_t nSpatialPts, const T& initialControlValue)
{
	auto s = one_particle::makeHilbertSpace(-4, 4, nSpatialPts);
	auto x = s.spatialDimension();
	auto V = makePotentialFunction([x](const RVec& p)
	{
		const auto& x0 = p.at(0);
		const auto& omega = p.at(1);
		return 0.5*omega*omega*pow(x - x0, 2);
	}, initialControlValue);

	return s.T()+V;
}

class DynamicState_Fixture : public ::testing::Test
{
public:
	count_t nSpatialPts = 128;
	count_t nTemporalPts = 20;
	count_t end = nTemporalPts - 1;
	real dt = 0.001;

	Control control = makeControl({linspace(0,1, nTemporalPts), linspace(5,7,nTemporalPts)}, dt);

	AUTO_MEMBER(H, makeHamiltonian(nSpatialPts, control.getFront()));

	AUTO_MEMBER( initialState, makeWavefunction(H(control.getFront())[0]));
	AUTO_MEMBER( targetState, makeWavefunction(H(control.getBack())[0]));
};

TEST_F(DynamicState_Fixture, checkSetup)
{
	ASSERT_NEAR(control.get(0).at(1) / 2, expectationValue(H(control.getFront()), initialState), 1e-4);
	ASSERT_NEAR(control.get(end).at(1) / 2, expectationValue(H(control.getBack()), targetState), 1e-4);
}


TEST_F(DynamicState_Fixture, retargetControl)
{
	/// test that accessing dynamicstate repeatedly without notifyOfChange does not change anything

	auto stepper = makeFixedTimeStepper(H, initialState, dt);
	auto psi = optimal_control::makeDynamicState<true>(stepper, initialState, std::cref(control));

	for(auto i= 1u; i<nTemporalPts;++i)
	{
		stepper.step({ control.get(i) });
		ASSERT_NEAR(1.0, fidelity(stepper.state(), psi(i)), 1e-6);
	}

	auto control2 = makeControl({ linspace(0,1, nTemporalPts), linspace(5,6,nTemporalPts) }, dt);
	psi.retargetControl(control2);

	stepper.reset(initialState, control.getFront());
	for (auto i = 1u; i<nTemporalPts; ++i)
	{
		stepper.step(control2.get(i));
		ASSERT_NEAR(1.0, fidelity(stepper.state(), psi(i)), 1e-6);
	}
}


//TEST_F(DynamicState_Fixture, changeableInitialState)
//{
//	/// test that dynamicState works if passed a function instead of the actual initialState
//
//	auto stepper = makeFixedTimeStepper(H, initialState, dt, RVec(control.getFront()));
//
//	auto initialStateMaker = [H{ H }](const RVec& v){return makeWavefunction(H(v)[0]); }; // return groundstate of Hamiltonian with given parameter
//	auto psi = makeDynamicState<true>(stepper, initialStateMaker, std::cref(control));
//
//	for (auto i = 1u; i<nTemporalPts; ++i)
//	{
//		stepper.step(control.get(i - 1), control.get(i));
//		ASSERT_NEAR(1.0, fidelity(stepper.state(), psi(i)), 1e-6);
//	}
//
//	control = controlfactory::makeLinearControl(RVec{ 0.0,6.0 }, RVec{ 1.0,7.0 }, nTemporalPts, dt);
//	psi.notifyOfChange();
//
//	stepper.reset(initialStateMaker(control.getFront()), control.getFront());
//	for (auto i = 1u; i<nTemporalPts; ++i)
//	{
//		stepper.step(control.get(i - 1), control.get(i));
//		ASSERT_NEAR(1.0, fidelity(stepper.state(), psi(i)), 1e-6);
//	}
//}

TEST_F(DynamicState_Fixture, forward_consistentAccess)
{
	/// test that accessing dynamicstate repeatedly without notifyOfChange does not change anything

	auto stepper = makeFixedTimeStepper(H, initialState, dt);
	auto psi = optimal_control::makeDynamicState<true>(stepper, initialState, std::cref(control));

	ASSERT_EQ(0u, psi.nPropagations());

	auto finalstate = psi(end);
	ASSERT_EQ(nTemporalPts-1, psi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(finalstate, psi(end)), 1e-10);
	ASSERT_EQ(nTemporalPts - 1, psi.nPropagations());

	// set new control, check psi is UNAFFECTED until notifyOfChange is called.
	control = makeControl({ linspace(0,1, nTemporalPts), linspace(5,6,nTemporalPts) }, dt);

	finalstate = psi(end);
	ASSERT_EQ(nTemporalPts - 1, psi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(finalstate, psi(end)), 1e-10);
	ASSERT_EQ(nTemporalPts - 1, psi.nPropagations());

	// Now actually notify it
	psi.notifyOfChange();
	finalstate = psi(end);
	ASSERT_EQ(2*(nTemporalPts - 1), psi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(finalstate, psi(end)), 1e-10);
	ASSERT_EQ(2 * (nTemporalPts - 1), psi.nPropagations());


	// same as before, but only notify on a single point
	control.set(end, RVec{0.0,7.0});

	finalstate = psi(end);
	ASSERT_EQ(2 * (nTemporalPts - 1), psi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(finalstate, psi(end)), 1e-10);
	ASSERT_EQ(2 * (nTemporalPts - 1), psi.nPropagations());

	// Now actually notify it
	psi.notifyOfChange(end);
	finalstate = psi(end);
	ASSERT_EQ(2 * (nTemporalPts - 1)+1, psi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(finalstate, psi(end)), 1e-10);
	ASSERT_EQ(2 * (nTemporalPts - 1)+1, psi.nPropagations());


}


TEST_F(DynamicState_Fixture, backward_consistentAccess)
{
	/// test that accessing dynamicstate repeatedly without notifyOfChange does not change anything

	auto stepper = makeFixedTimeStepper(H, initialState, -dt);
	auto chi = optimal_control::makeDynamicState<false>(stepper, initialState, std::cref(control));

	ASSERT_EQ(0u, chi.nPropagations());

	auto chiAt0 = chi(0);
	ASSERT_EQ(nTemporalPts - 1, chi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(chiAt0, chi(0)), 1e-10);
	ASSERT_EQ(nTemporalPts - 1, chi.nPropagations());

	// set new control, check chi is UNAFFECTED until notifyOfChange is called.
	control = makeControl({ linspace(0,0, nTemporalPts), linspace(5,6,nTemporalPts) }, dt);

	chiAt0 = chi(0);
	ASSERT_EQ(nTemporalPts - 1, chi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(chiAt0, chi(0)), 1e-10);
	ASSERT_EQ(nTemporalPts - 1, chi.nPropagations());

	// Now actually notify it
	chi.notifyOfChange();
	chiAt0 = chi(0);
	ASSERT_EQ(2 * (nTemporalPts - 1), chi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(chiAt0, chi(0)), 1e-10);
	ASSERT_EQ(2 * (nTemporalPts - 1), chi.nPropagations());


	// same as before, but only notify on a single point
	control.setFront(RVec{ 0.0,7.0 });

	chiAt0 = chi(0);
	ASSERT_EQ(2 * (nTemporalPts - 1), chi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(chiAt0, chi(0)), 1e-10);
	ASSERT_EQ(2 * (nTemporalPts - 1), chi.nPropagations());

	// Now actually notify it
	chi.notifyOfChange(0);
	chiAt0 = chi(0);
	ASSERT_EQ(2 * (nTemporalPts - 1) + 1, chi.nPropagations());

	ASSERT_NEAR(1.0, fidelity(chiAt0, chi(0)), 1e-10);
	ASSERT_EQ(2 * (nTemporalPts - 1) + 1, chi.nPropagations());
}

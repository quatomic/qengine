﻿/* COPYRIGHT
 *
 * file="F_BringHomeWater.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once 

#include <gtest/gtest.h>

#include <qengine/OneParticle.h>
#include <qengine/OptimalControl.h>

#include "src/API/UserUtilities/AutoMember.h"

using namespace qengine;


namespace bhw_testing
{
	template<class T1, class T2>
	auto makeDynamicPotential(const T1& x, const real waist, const T2& initialControlValue)
	{
		const auto ff = 2.0 / (waist*waist);

		return makePotentialFunction([x{ x }, ff{ ff }](const RVec& p)
		{
			const auto& x0 = p.at(0);
			const auto& A = p.at(1);
			return 100 * A*exp(-ff * pow(x - x0, 2));
		}, initialControlValue);
	}
}

class BringHomeWater_Fixture : public ::testing::Test
{
public:
	const real dt = 0.001;

	const AUTO_MEMBER(s, one_particle::makeHilbertSpace(-2, 2, 256, 0.5));
	const AUTO_MEMBER(x, s.spatialDimension());

	const real waist = 0.25;

	const AUTO_MEMBER(V_static, -130 * exp(-2.0 / (waist*waist) * pow(x - 0.6, 2)));

	const Control initialControl = makeControl({
		glue(linspace(-0.8, 0.6, 5), linspace(0.6, -0.75, 197)),
		glue(linspace(-1,  -1.4, 5), linspace(-1.4, -1.4, 197))
		}, dt);

	const RVec lowerLeft{ -1, -1.5 };
	const RVec upperRight{ 1, -0.1 };

	const AUTO_MEMBER(T, s.T());
	const AUTO_MEMBER(V_dyn, bhw_testing::makeDynamicPotential(x, waist, initialControl.getFront()));
	const AUTO_MEMBER(H, T + V_static + V_dyn);

	const AUTO_MEMBER(initialState, makeWavefunction((T + V_static)[0]));
	const AUTO_MEMBER(targetState, makeWavefunction((T + V_dyn(initialControl.getBack()))[0]));

	auto makeProblem()
	{


		const auto ff = 2.0 / (waist*waist);
		auto dVdu = makeAnalyticDiffPotential
		(
			[x{ x }, ff{ ff }](const RVec& p)
		{
			const auto& x0 = p.at(0);
			const auto& A = p.at(1);
			return 2 * 100 * ff*A*(x - x0)*exp(-ff * pow(x - x0, 2));

		},
			[x{ x }, ff{ ff }](const RVec& p)
		{
			const auto& x0 = p.at(0);
			//const auto& A = p.at(1);
			return 100 * exp(-ff * pow(x - x0, 2));
		}
		);

		return makeStateTransferProblem(H, dVdu, initialState, targetState, initialControl);
	}
};

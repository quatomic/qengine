#include "gtest/gtest.h"

#include <qengine/OptimalControl.h>
#include <qengine/../src/API/Common/OptimalControl/LinesearchHeuristics/QuickStep.cpp>

#include <queue>
#include "F_BringHomeWater.h"




using uint = unsigned int;
using ControlMock = double;

// the value is irrelevant
constexpr double ref = 3.14159265358;


constexpr size_t unrollThreshold = 4000;
constexpr size_t unrolls = 5000;
constexpr size_t attempts = unrollThreshold * 2;

namespace qengine
{
	namespace experimental
	{
		class ProblemMock
		{
		public:

			ProblemMock() = default;

			void ResetContainers()
			{
				mStepSizes = std::queue<double>{};
				mCosts = std::queue<double>{};
			}

			void SetDiffSizes(const size_t count)
			{
				for (size_t i = 0; i < count; ++i)
				{
					mStepSizes.emplace((double)i + 1);
					mCosts.emplace(mMaxCost - (i + 1.0) / (double)(count + 1));
				}
			}

			void SetUnrollSizes(const double value, const size_t count)
			{
				for (size_t i = 0; i < count; ++i)
				{
					mStepSizes.emplace(value);
					mCosts.emplace(mMaxCost - (i + 1.0) / (double)(count + 1));
				}
			}

			void SetUnrollError(const double value, const size_t errorPosition)
			{
				for (size_t i = 0; i < errorPosition; ++i)
				{
					mStepSizes.emplace(value);
					// cost has suddenly increased in iteration 'errorPosition'
					mCosts.emplace(mMaxCost - (i + 1.0) / (double)(errorPosition + 1));
				}
				// cost is steadily decreasing
				mCosts.emplace(mMaxCost);
				mStepSizes.emplace(value + 1);
			}

			double step() const
			{
				SanityCheck();
				return mStepSizes.front();
			}

			double cost() const
			{
				SanityCheck();
				return mCosts.front();
			}

			void pop()
			{
				SanityCheck();
				mStepSizes.pop();
				mCosts.pop();
			}

			ControlMock _params()
			{
				return mParams;
			}

			void update(ControlMock)
			{

			}

		private:
			void SanityCheck() const
			{
				if (mCosts.size() && mStepSizes.size())
				{
					return;
				}
				throw std::runtime_error("Invalid ProblemMock usage");
			}
			ControlMock mParams;
			std::queue<double> mStepSizes;
			std::queue<double> mCosts;
			const double mMaxCost = 1;
		};


		class AlgorithmMock
		{
		public:
			AlgorithmMock() : _stepDirection(0) {}

			ProblemMock & problem() const
			{
				return _problem;
			}

			ControlMock stepDirection() const
			{
				return _stepDirection;
			}

			void SetDirection(const ControlMock & ctrl)
			{
				_stepDirection = ctrl;
			}

			mutable ProblemMock _problem;
			mutable ControlMock _stepDirection;

			template <typename Control, typename LineSearch>
			friend class QuickStep;
		};

		class LineSearchMock
		{
		public:
			template <typename Algorithm>
			double operator()(const Algorithm & alg) const
			{
				return alg.problem().step();
			}
		private:

		};

		QuickStep<ControlMock, LineSearchMock> MakeQuickStep(const uint thresh, const uint unroll)
		{
			return QuickStep<ControlMock, LineSearchMock>({}, thresh, unroll);
		}

		class QuickStepOptimizeFixture : public BringHomeWater_Fixture
		{
		public:
			template <typename OptimizerFunctor, typename Control>
			auto makeOptimalSet(const OptimizerFunctor & func, const Control &, const bool runOpt = true)
			{
				auto problem = makeProblem();

				auto costs = std::vector<real>();
				auto its = std::vector<count_t>();


				auto stepSizeFinder = makeInterpolatingStepSizeFinder(10, 0.1);
				const auto thresh = 5;

				auto quickStepHeuristic = makeQuickStepHeuristic(stepSizeFinder, Control(), thresh, 200);
				auto & rawQS = quickStepHeuristic.wrapped();

				auto stopper = makeStopper([](const auto& alg) -> bool
				{
					return (alg.problem().fidelity() > 0.9);
				});

				auto collector = makeCollector([&costs, &its, &rawQS, thresh](const auto& optimizer)
				{
					// std::cout << rawQS.previousStepSize() << std::endl;
					// std::cout <<
					// 	"ITER " << optimizer.iteration() << " | " <<
					// 	"fidelity : " << optimizer.problem().fidelity() << "\t " <<
					// 	"stepSize : " << optimizer.stepSize() << "\t " <<
					// 	std::endl;
					ASSERT_GT(rawQS.previousStepSize(), 0);
					if (optimizer.stepSize() < 0)
					{
						ASSERT_TRUE(rawQS.didRollback());
					}
					if (rawQS.didRollback())
					{
						ASSERT_GT(rawQS.nRollbacksPerformed(), 0u);
						ASSERT_EQ(rawQS.unrollsLeft(), 0);
						ASSERT_EQ(rawQS.sameStepSizes(), thresh - 1);
					}
				});

				auto optimizer = func(problem, stopper, collector, quickStepHeuristic);
				if (runOpt)
				{
					optimizer.optimize();
				}
			}
		};

		TEST(QuickStepTest, TestMockSanity)
		{
			constexpr size_t count = 1000;
			ProblemMock prob;
			ASSERT_THROW(prob.step(), std::runtime_error);
			ASSERT_THROW(prob.cost(), std::runtime_error);


			prob.SetDiffSizes(count);
			for (size_t i = 0; i < count; ++i)
			{
				prob.pop();
			}
			ASSERT_THROW(prob.step(), std::runtime_error);
			ASSERT_THROW(prob.cost(), std::runtime_error);

			prob.SetDiffSizes(count);
			prob.ResetContainers();
			ASSERT_THROW(prob.step(), std::runtime_error);
			ASSERT_THROW(prob.cost(), std::runtime_error);
		}

		TEST(QuickStepTest, TestMockUnroll)
		{
			ProblemMock prob;
			constexpr double step = 3.1415926;

			prob.SetUnrollSizes(step, unrolls);
			double prevCost = 1;
			for (size_t i = 0; i < unrolls; ++i)
			{
				ASSERT_EQ(prob.step(), step);
				ASSERT_LT(prob.cost(), prevCost);
				prevCost = prob.cost();
				prob.pop();
			}
			prob.ResetContainers();
		}

		TEST(QuickStepTest, TestMockUnrollError)
		{
			constexpr size_t errorPos = unrolls - 1;
			ProblemMock prob;
			constexpr double step = 3.1415926;
			prob.SetUnrollError(step, errorPos);
			double prevCost = std::numeric_limits<double>::max();
			for (size_t i = 0; i < errorPos; ++i)
			{
				ASSERT_EQ(prob.step(), step);
				ASSERT_LT(prob.cost(), prevCost);
				prevCost = prob.cost();
				prob.pop();
			}
			ASSERT_GT(prob.cost(), prevCost);
		}

		TEST(QuickStepTest, TestMockDiff)
		{
			constexpr size_t counts = 10000;
			ProblemMock prob;

			prob.SetDiffSizes(counts);
			double prevStep = -1.0;
			double prevCost = 1;
			for (size_t i = 0; i < counts; ++i)
			{
				ASSERT_NE(prob.step(), prevStep);
				prevStep = prob.step();
				ASSERT_NE(prob.cost(), prevCost);
				prevCost = prob.cost();
				prob.pop();
			}
			prob.ResetContainers();
		}

		TEST(QuickStepTest, InitialParametersTest)
		{
			QuickStep<ControlMock, LineSearchMock> quickStep = MakeQuickStep(unrollThreshold, unrolls);

			EXPECT_EQ(0, quickStep.sameStepSizes());
			EXPECT_EQ(0, quickStep.unrollsLeft());
			EXPECT_EQ(quickStep.nUnrollsPerformed(), 0);
			EXPECT_EQ(quickStep.nRollbacksPerformed(), 0);;
			EXPECT_EQ(unrolls, quickStep.nUnrollsAfterThreshold());
			EXPECT_EQ(unrollThreshold, quickStep.unrollThreshold());
		}

		TEST(QuickStepTest, DiffValuesTest)
		{
			QuickStep<ControlMock, LineSearchMock> quickStep = MakeQuickStep(unrollThreshold, unrolls);

			AlgorithmMock alg;

			alg.problem().SetDiffSizes(attempts);
			double prevStep = -1.0;
			double step;
			for (size_t i = 0; i < attempts; ++i)
			{
				step = quickStep(alg);
				ASSERT_NE(prevStep, step);
				ASSERT_EQ(quickStep.sameStepSizes(), 1);
				ASSERT_EQ(quickStep.unrollsLeft(), 0);
				ASSERT_EQ(quickStep.nUnrollsPerformed(), 0);
				ASSERT_EQ(quickStep.nRollbacksPerformed(), 0);
				alg.problem().pop();
			}
		}

		template < typename Algorithm>
		void PassThreshold(QuickStep<ControlMock, LineSearchMock> & quickStep, Algorithm & alg)
		{
			auto recentStep = quickStep.previousStepSize();
			auto choicesLeft = quickStep.unrollThreshold() - quickStep.sameStepSizes();
			if (choicesLeft == 0)
			{
				return;
			}
			alg.problem().ResetContainers();
			alg.problem().SetUnrollSizes(recentStep, choicesLeft);

			double step;
			// progress until unrolling is turned on
			for (size_t i = 0; i < choicesLeft; ++i)
			{
				step = quickStep(alg);
				ASSERT_EQ(step, recentStep);
				ASSERT_LE(quickStep.sameStepSizes(), quickStep.unrollThreshold());
				alg.problem().pop();
			}
			ASSERT_EQ(quickStep.unrollsLeft(), quickStep.nUnrollsAfterThreshold());
		}

		template < typename Algorithm>
		void RollbackCycle(QuickStep<ControlMock, LineSearchMock> & quickStep, Algorithm & alg)
		{
			constexpr size_t errorPosition = unrolls - 1;
			static_assert(errorPosition < unrolls, "");
			PassThreshold(quickStep, alg);

			alg.problem().ResetContainers();
			alg.problem().SetUnrollError(ref, errorPosition);
			alg.problem().SetDiffSizes(1);

			// keep unrolling and perform invalid step at the end
			for (size_t i = 0; i < errorPosition; ++i)
			{
				quickStep(alg);
				ASSERT_LE(quickStep.unrollsLeft(), quickStep.nUnrollsAfterThreshold());
				alg.problem().pop();
			}

			// alter the direction (before the rollback)
			const ControlMock newControl = alg.stepDirection() + 1;
			alg.SetDirection(newControl);

			// force rollback
			quickStep(alg);

			// check if we rolled back to the direction before the erroneous unroll
			ASSERT_NE(alg.stepDirection(), newControl);
		}

		TEST(QuickStepTest, PassThresholdTest)
		{
			QuickStep<ControlMock, LineSearchMock> quickStep = MakeQuickStep(unrollThreshold, unrolls);

			AlgorithmMock alg;
			PassThreshold(quickStep, alg);

			// have we reached the unrollThreshold value?
			ASSERT_EQ(quickStep.sameStepSizes(), quickStep.unrollThreshold());
			ASSERT_EQ(quickStep.nRollbacksPerformed(), 0);
			ASSERT_EQ(quickStep.nUnrollsPerformed(), 0);
		}

		TEST(QuickStepTest, UnrollTest)
		{
			QuickStep<ControlMock, LineSearchMock> quickStep = MakeQuickStep(unrollThreshold, unrolls);
			AlgorithmMock alg;
			PassThreshold(quickStep, alg);
			ASSERT_EQ(quickStep.nRollbacksPerformed(), 0);
			ASSERT_EQ(quickStep.nUnrollsPerformed(), 0);

			alg.problem().ResetContainers();
			alg.problem().SetUnrollSizes(ref, unrolls);

			for (size_t i = 0; i < unrolls; ++i)
			{
				quickStep(alg);
				ASSERT_LE(quickStep.unrollsLeft(), quickStep.nUnrollsAfterThreshold());
				ASSERT_EQ(quickStep.nUnrollsPerformed(), i + 1);
				alg.problem().pop();
			}
			ASSERT_EQ(quickStep.unrollsLeft(), 0);
		}

		TEST(QuickStepTest, RollbackTest)
		{
			QuickStep<ControlMock, LineSearchMock> quickStep = MakeQuickStep(unrollThreshold, unrolls);
			AlgorithmMock alg;
			for (size_t cycle = 0; cycle < 2000; ++cycle)
			{
				RollbackCycle(quickStep, alg);
				ASSERT_EQ(quickStep.nRollbacksPerformed(), cycle + 1);
			}
		}

		TEST_F(QuickStepOptimizeFixture, GRAPE_SteepestDescent_L2)
		{
			makeOptimalSet(
				[]
			(auto & prob, auto & stop, auto & coll, auto & quickStep)
			{
				return makeGrape_steepest_L2(prob, stop, coll, quickStep);
			}, Control{}
			);
		}

		TEST_F(QuickStepOptimizeFixture, GRAPE_SteepestDescent_H1)
		{
			makeOptimalSet(
				[]
			(auto & prob, auto & stop, auto & coll, auto & quickStep)
			{
				return makeGrape_steepest_H1(prob, stop, coll, quickStep);
			}, Control{}
			);
		}

		TEST_F(QuickStepOptimizeFixture, GRAPE_BFGS_L2)
		{
			makeOptimalSet(
				[]
			(auto & prob, auto & stop, auto & coll, auto & quickStep)
			{
				return makeGrape_bfgs_L2(prob, stop, coll, quickStep);
			}, Control{}
			);
		}

		TEST_F(QuickStepOptimizeFixture, GRAPE_BFGS_H1)
		{
			makeOptimalSet(
				[]
			(auto & prob, auto & stop, auto & coll, auto & quickStep)
			{
				return makeGrape_bfgs_H1(prob, stop, coll, quickStep);
			}, Control{}
			);
		}

		TEST_F(QuickStepOptimizeFixture, GROUP_BFGS)
		{
			makeOptimalSet(
				[]
			(auto & prob, auto & stop, auto & coll, auto & quickStep)
			{
				auto u = prob.control();
				const auto basisSize = 120; // basis size
				auto maxRand = 0.0; // -maxRand < theta_m < maxRand
                const auto shapeFunction = makeSigmoidShapeFunction(u);
				const auto basis = shapeFunction * makeSineBasis(basisSize, u.metaData(), maxRand);
				prob.update(0 * u);
				return makeGroup_bfgs(prob, basis, stop, coll, quickStep);
			}, BasisControl{}, false // only instantiate the optimizer, don't run it (tough to choose the right parameters)
				);
		}
	}
}

﻿/* COPYRIGHT
 *
 * file="T_SpatialHelpers.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <gtest/gtest.h>

#include "src/Abstract/Physics/Spatial/PotentialFunction.h"
#include "src/Abstract/Physics/Spatial/FunctionOfX.impl.h"

#include "src/API/Common/Spatial/1D/ApiHilbertSpace.h"
#include "src/API/Common/Spatial/OptimalControlHelpers.h"

using namespace qengine;


class SpatialHelpers_Fixture : public ::testing::Test
{
public:

};

TEST_F(SpatialHelpers_Fixture, NumericDiffPotential)
{
	const auto s = one_particle::makeHilbertSpace(-2, 2, 256);
	const auto x = s.spatialDimension();

	const auto waist = 0.25;
	const auto ff = 2.0 / (waist*waist);
	auto V = makePotentialFunction([x, ff](const RVec& p)
	{
		const auto& x0 = p.at(0);
		const auto& A = p.at(1);
		return  100 * A*exp(-ff*pow(x - x0, 2)); // dynamic Bring Home Water potential
	}, RVec{-0.8,-1});

	auto dVdu_ana = makeAnalyticDiffPotential
	(
		[x, ff](const RVec& p)
	{
		const auto& x0 = p.at(0);
		const auto& A = p.at(1);
		return 2 * 100 * ff*A*(x - x0)*exp(-ff*pow(x - x0, 2));

	},
		[x, ff](const RVec& p)
	{
		const auto& x0 = p.at(0);
		//const auto& A = p.at(1);
		return 100 * exp(-ff*pow(x - x0, 2));
	}
	);

	auto tolerance = 1e-15;

	auto p = RVec{ -0.8, -1 };
	auto infidelities = infidelityWithDiffPotential(dVdu_ana, V, p);
	EXPECT_NEAR(0.0,infidelities.at(0), tolerance) << "p1";
	EXPECT_NEAR(0.0, infidelities.at(1), tolerance) << "p1";

	p = RVec{ 1.0, -1.0 };
	infidelities = infidelityWithDiffPotential(dVdu_ana, V, p);
	EXPECT_NEAR(0.0, infidelities.at(0), tolerance) << "p2";
	EXPECT_NEAR(0.0, infidelities.at(1), 1.4*tolerance) << "p2";
}



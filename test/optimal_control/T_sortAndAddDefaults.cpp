﻿/* COPYRIGHT
 *
 * file="T_sortAndAddDefaults.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "src/API/Common/OptimalControl/Policies/PolicyWrapper.h"
#include "src/API/Common/OptimalControl/Policies/Stoppers.h"
#include "src/API/Common/OptimalControl/Policies/Collectors.h"
#include "src/API/Common/OptimalControl/Policies/StepsizeFinders.h"
#include "src/API/Common/OptimalControl/Policies/BfgsRestarters.h"


#include "src/API/Common/OptimalControl/makeAlgorithm.h"

using namespace qengine;

class SortAndAddDefaults_Fixture: public ::testing::Test{};

TEST_F(SortAndAddDefaults_Fixture, orderMapAccess)
{
	auto orderMap = internal::makeTypeMap(internal::TypeList<internal::policy::StopperID, internal::policy::CollectorID>{}, std::make_tuple(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); })));

	ASSERT_EQ(std::string("defaultStopper"), orderMap.template get<internal::policy::StopperID>().f());
	ASSERT_EQ(std::string("defaultCollector"), orderMap.template get<internal::policy::CollectorID>().f());
}

TEST_F(SortAndAddDefaults_Fixture, wrappersToTypeMap)
{
	auto map = internal::toTypeMap(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); }));

	ASSERT_EQ(std::string("defaultStopper"), map.template get<internal::policy::StopperID>()());
	ASSERT_EQ(std::string("defaultCollector"), map.template get<internal::policy::CollectorID>()());
}

TEST_F(SortAndAddDefaults_Fixture, areInputIDsValid)
{
	auto orderMap = internal::makeTypeMap(internal::TypeList<internal::policy::StopperID, internal::policy::CollectorID>{}, std::make_tuple(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); })));

	auto map = internal::toTypeMap(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); }));

	static_assert(internal::areInputKeysValid(internal::Type<decltype(orderMap)>{}, internal::Type<decltype(map)>{}), "");
}

TEST_F(SortAndAddDefaults_Fixture, sortAndAddDefaults_impl)
{
	auto orderMap = internal::toTypeMap(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); }));

	auto inputs = internal::toTypeMap(makeCollector([]() {return std::string("NotDefaultCollector"); }));

	auto sorted = internal::detail::sortAndAddDefaults_impl<internal::policy::StopperID, internal::policy::CollectorID>(orderMap, inputs);
	
	ASSERT_EQ(std::string("defaultStopper"), std::get<0>(sorted)());
	ASSERT_EQ(std::string("NotDefaultCollector"), std::get<1>(sorted)());
}

TEST_F(SortAndAddDefaults_Fixture, sortAndAddDefaults)
{
	auto orderMap = internal::toTypeMap(makeStopper([]() {return std::string("defaultStopper"); }), makeCollector([]() {return std::string("defaultCollector"); }));

	auto inputs = internal::toTypeMap(makeCollector([]() {return std::string("NotDefaultCollector"); }));

	auto sorted = internal::makeTupleOfSortedInputsWithDefaultsAdded(orderMap, inputs);
	
	ASSERT_EQ(std::string("defaultStopper"), std::get<0>(sorted)());
	ASSERT_EQ(std::string("NotDefaultCollector"), std::get<1>(sorted)());
}

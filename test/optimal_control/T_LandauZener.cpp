﻿/* COPYRIGHT
 *
 * file="T_LandauZener.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>
#include <numeric>
#include <qengine/NLevel.h>
#include <qengine/OptimalControl.h>

#include "src/API/UserUtilities/AutoMember.h"


using namespace qengine;

namespace landauzener_testing
{



    auto makeDynamicHamiltonian(const n_level::ApiHilbertSpace& s, const real E_diff)
    {
        return makeOperatorFunction([s{ s }, E_diff {E_diff}](const real p)
        {
            return E_diff* n_level::makePauliZ()+p* n_level::makePauliX();
        }, 0.0);
    }
}

class LandauZener_Fixture : public ::testing::Test
{
public:

    const real dt = 0.001;
    const real E_diff = 1.1;

    const AUTO_MEMBER(s, n_level::makeHilbertSpace(2));


    const Control initialControl = glue(makeLinearControl(0, 0.6, 100, dt),makeLinearControl(0.6 , 0, 100, dt));

    const RVec lowerLeft{ -2.0 };
    const RVec upperRight{ 2.0 };

    const AUTO_MEMBER(H, landauzener_testing::makeDynamicHamiltonian(s, E_diff));

	const AUTO_MEMBER(initialState, makeState(s, fock::state{ 0 }));
	const AUTO_MEMBER(targetState, makeState(s, fock::state{ 1 }));

    auto makeProblem()
	{
        auto dHdu = makeAnalyticDiffHamiltonian(
    	
                [s{ s }](const RVec&) 
             {
                 return n_level::makePauliX();
             }
        );

		auto problem = makeStateTransferProblem(H, dHdu, initialState, targetState, initialControl);
        return problem;
    }


};

TEST_F(LandauZener_Fixture, full_control_consistency)
{

    auto problem = makeProblem();
	// initial step test
    ASSERT_EQ(problem.control(), initialControl);
    ASSERT_EQ(problem.initialControl(), initialControl);
    ASSERT_EQ(problem.dt(), dt);

	auto expectedStepCount = 0ull;
	ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());

	auto fidelity = problem.fidelity();
	ASSERT_EQ(expectedStepCount += initialControl.size() - 1, problem.nPropagationSteps());

	auto cost = problem.cost(); // should not increase stepcount because uses fidelity
	ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());

	auto grad = problem.gradient();
	ASSERT_EQ(expectedStepCount += initialControl.size() - 2, problem.nPropagationSteps());

	// update and test again
    const auto newControl = glue(makeLinearControl( 2 ,  0.55 , 100, 0.99*dt), makeLinearControl( 0.55 ,  2 , 100, 0.99*dt));
	ASSERT_NE(problem.control(), newControl);

	problem.update(newControl);
    ASSERT_EQ(problem.control(), newControl);
    ASSERT_EQ(problem.initialControl(), initialControl);
    ASSERT_EQ(problem.dt(), newControl.dt());
	ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());

	ASSERT_NE(fidelity, problem.fidelity());
	ASSERT_EQ(expectedStepCount += initialControl.size() - 1, problem.nPropagationSteps());

	ASSERT_NE(cost, problem.cost());
	ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());

	ASSERT_NE(grad, problem.gradient());
	ASSERT_EQ( expectedStepCount += initialControl.size() - 2, problem.nPropagationSteps());

	problem.resetPropagationCount();
    ASSERT_EQ(expectedStepCount =0, problem.nPropagationSteps());
}


TEST_F(LandauZener_Fixture, stepwise_consistency)
{
	auto problem = makeProblem();

	// initial step test
	ASSERT_EQ(problem.control(), initialControl);
	ASSERT_EQ(problem.initialControl(), initialControl);
	ASSERT_EQ(problem.dt(), dt);

	auto expectedStepCount = 0ull;
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	auto fidelity = problem.fidelity();
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 1);

	auto cost = problem.cost(); // should not increase stepcount because uses fidelity
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

    /*for(auto i = initialControl.size()-2; i>=1;--i)
    {
        auto grad = problem.gradient(i);
        ASSERT_EQ(++expectedStepCount, problem.nPropagationSteps());
        grad = problem.gradient(i);
        ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());
    }*/

	ASSERT_EQ(cost, problem.cost()); // should not increase stepcount
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);


    problem._update(1, RVec{0.0}); //

	ASSERT_NE(fidelity, problem.fidelity());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += 2);

	ASSERT_NE(cost, problem.cost());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);


	auto g = problem._gradient(1);
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

    /*
	for (auto i = 2u; i<initialControl.size(); ++i)
	{
		auto grad = problem.gradient(i);
		ASSERT_EQ(++expectedStepCount, problem.nPropagationSteps());
		grad = problem.gradient(i);
		ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());
	}
    */
}

TEST_F(LandauZener_Fixture, initialAndTargetStates)
{
	auto tol = 1e-9;
	ASSERT_NEAR(0.0, variance(H(initialControl.getFront()), initialState), tol);
	ASSERT_NEAR(0.0, variance(H(initialControl.getBack()), targetState), tol);
}

TEST_F(LandauZener_Fixture, physicsGradient)
{
    auto problem = makeProblem();

    auto gradDiff = gradientDifference(problem);
    ASSERT_NEAR(0.0, gradDiff, 1e-6);
}


TEST_F(LandauZener_Fixture, gradientWithRegAndBounds)
{
    auto problem = makeProblem()+1e-9*Regularization(initialControl)+10*Boundaries(initialControl, lowerLeft, upperRight);

	auto gradDiff = gradientDifference(problem);

	ASSERT_NEAR(0.0, gradDiff, 1e-6);
}

TEST_F(LandauZener_Fixture, alg_grape_steepest_L2)
{
	auto maxIteration = 50u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
    auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});

	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	auto grape = makeGrape_steepest_L2(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);

	//ASSERT_TRUE(false);
}

TEST_F(LandauZener_Fixture, alg_grape_steepest_H1)
{
	auto maxIteration = 50u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
    auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(1000, 2);

	auto grape = makeGrape_steepest_H1(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);

}

TEST_F(LandauZener_Fixture, alg_grape_bfgs_L2)
{
	auto maxIteration = 50u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
    auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	auto grape = makeGrape_bfgs_L2(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);
}

TEST_F(LandauZener_Fixture, alg_grape_bfgs_H1)
{
	auto maxIteration = 50u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
    auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());

	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(1000, 1);

	auto grape = makeGrape_bfgs_H1(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);

}

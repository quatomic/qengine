﻿/* COPYRIGHT
 *
 * file="T_CondensateSplitting.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <tuple>
#include <gtest/gtest.h>

#include <qengine/GPE.h>
#include <qengine/OptimalControl.h>
#include <qengine/DataContainer.h>

#include "src/API/UserUtilities/AutoMember.h"



using namespace qengine;


//todo: put in reference to lesanovsky paper 


namespace lesanovsky_testing
{
	template<class T1, class T2>
	auto makeDynamicPotential(const T1& x, const T2& initialControlValue)
	{
		auto hbar = 1.054571800*1e-34;
		auto bohr = 9.2740092e-24;
		//  static magnetic field in z - direction of Ioffe - Pritchard trap
			// (in units of Gauss)
		auto bi = 1;
		//  gradient of quadrupole field of Ioffe - Pritchard trap
		auto g = 0.2;
		//  coupling constant[see eq. (7)]
		auto kappa = 1;
		//  detuning of radio frequency field(MHz->Gauss)
		auto det = 1.26e6 * (2 * PI * hbar / bohr) * 1e4;

		return makePotentialFunction([x, hbar, bohr, bi, g, kappa, det](const RVec& p)
		{
			auto u = p.at(0);
			//  radio frequency field
			auto brf = 0.5 + 0.3 * u;

			//  static magnetic field of Ioffe - Pritchard trap
			auto bs = sqrt(pow(g * x, 2) + bi *bi);
			//  potential according to eq. (10)
			auto pot = kappa * sqrt(pow(bs - det, 2) + pow(0.5 * brf * bi / bs, 2));

			//  subtract minimum
			pot = pot - min(pot);
			//  convert from Gauss to kHz
			return pot * 1e-7 * bohr / hbar;
		}, initialControlValue);

	}
}

class CondensateSplitting_Fixture : public ::testing::Test
{
public:
	const real dt = 0.001;

	const AUTO_MEMBER( s, gpe::makeHilbertSpace(-2.25, 2.25, 256, 0.36498735032571));
	const AUTO_MEMBER( x, s.spatialDimension());

	const real beta = PI;

	const count_t N = 401;
	const Control initialControl = makeControl({sqrt(linspace(0,1,N))}, dt);

	const RVec lowerLeft{ 0 };
	const RVec upperRight{ 1 };

	const AUTO_MEMBER(T, s.T());
	const AUTO_MEMBER(V_dyn, lesanovsky_testing::makeDynamicPotential(x, initialControl.getFront()));
	const AUTO_MEMBER(H, T + V_dyn + makeGpeTerm(beta));

	const AUTO_MEMBER(initialState, makeWavefunction((H(initialControl.getFront()))[0], NORMALIZE, GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING, 1e-12, 12000));
	//FunctionOfX<complex> targetState = makeWavefunction((H(initialControl.getBack()))[0], NORMALIZE);
	const AUTO_MEMBER( targetState, makeWavefunction((H(initialControl.getBack()))[0], NORMALIZE, GROUNDSTATE_ALGORITHM::MIXING, 1e-8, 5000, std::vector<real>{1e-3}));

	auto makeProblem()
	{
		auto dVdu = makeNumericDiffPotential(V_dyn, 1);

		return makeStateTransferProblem(H, dVdu, initialState, targetState, initialControl);
	}
};

/*
TEST_F(CondensateSplitting_Fixture, DISABLED_full_control_consistency)
{
	auto problem = makeProblem();

	// initial step test
	ASSERT_EQ(problem.control(), initialControl);
	ASSERT_EQ(problem.initialControl(), initialControl);
	ASSERT_EQ(problem.dt(), dt);

	auto expectedStepCount = 0ull;
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	auto fidelity = problem.fidelity();
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 1);

	auto cost = problem.cost(); // should not increase stepcount because uses fidelity
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	auto grad = problem.gradient();
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 2);

	// update and test again
	const auto newControl =	makeControl({ linspace(0,1,N) }, dt);
	ASSERT_NE(problem.control(), newControl);

	problem.update(newControl);
	ASSERT_EQ(problem.control(), newControl);
	ASSERT_EQ(problem.initialControl(), initialControl);
	ASSERT_EQ(problem.dt(), newControl.dt());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	ASSERT_NE(fidelity, problem.fidelity());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 1);

	ASSERT_NE(cost, problem.cost());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	ASSERT_NE(grad, problem.gradient());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 2);

	problem.resetPropagationCount();
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount = 0);
}

TEST_F(CondensateSplitting_Fixture, DISABLED_initialAndTargetStates)
{
	auto tol = 3e-8;
	EXPECT_NEAR(0.0, variance(H(initialControl.getFront()), initialState), tol);
	EXPECT_NEAR(0.0, variance(H(initialControl.getBack()), targetState), tol);
}

TEST_F(CondensateSplitting_Fixture, DISABLED_dVdu)
{
	auto problem = makeProblem();
	auto dVdu = problem.dHdu();

	auto tol = 1e-14;

	for (auto j = 0u; j < initialControl.size(); ++j)
	{
		auto infidelities = infidelityWithDiffPotential(dVdu, V_dyn, initialControl.at(j));

		for (auto i = 0u; i < initialControl.paramCount(); ++i)
		{
			ASSERT_NEAR(0.0, infidelities.at(i), tol);
		}
	}
}
<<<<<<< HEAD

TEST_F(CondensateSplitting_Fixture, DISABLED_physicsGradient)
{
	auto problem = makeProblem();

	std::cout << "problem created!" << std::endl;

	DataContainer ws{};
	ws.set("startguessCPP", initialControl.mat());

	ws.set("startStateCPP", initialState.vec());
	ws.set("targetStateCPP", targetState.vec());

	ws.set("V_initCPP", V_dyn(initialControl.getFront()).vec());
	ws.set("V_targetCPP", V_dyn(initialControl.getBack()).vec());

	ws.set("gradientCPP", problem.gradient().mat());

	for(auto i = 0u; i<initialControl.size(); ++i)
	{
		ws.append("potentialCPP", V_dyn(initialControl.at(i)).vec());
	}

	Control numeric, lowers, uppers;

	std::tie(numeric, lowers, uppers) = fullNumericGradient2(problem);
	numeric.at(0) = RVec(1, 0.0);
	numeric.at(initialControl.size()-1) = RVec(1, 0.0);

	ws.set("numGradientCPP", numeric.mat());
	ws.set("numGradLowersCPP", lowers.mat());
	ws.set("numGradUppersCPP", uppers.mat());

	ws.set("costCPP", problem.cost());
	ws.set("fidelityCPP", problem.fidelity());

	ws.set("dxCPP", s.dx());
	ws.set("xCPP", s.spatialDimension().vec());
	ws.save("lesanovsky.mat");
	ASSERT_TRUE(false);

	//auto gradDiff = gradientDifference(problem);
	//ASSERT_NEAR(0.0, gradDiff, 1e-8);
}
*/

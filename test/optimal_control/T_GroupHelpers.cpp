﻿/* COPYRIGHT
 *
 * file="T_GroupHelpers.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "F_BringHomeWater.h"


TEST_F(BringHomeWater_Fixture, costDecreaseDressedStopper)
{
	auto maxIteration = 15u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration](const auto& alg) {return alg.iteration() >= maxIteration; });

    auto collector = makeCollector([](const auto&){});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	const auto t = makeTimeControl(initialControl.size(), initialControl.dt());
    const auto shapeFunction = makeSigmoidShapeFunction(t);
    auto basisMaker = makeBasisMaker([metadata{ initialControl.metaData() }, shapeFunction]()
	{
		return shapeFunction * makeSineBasis(10, metadata, 0.5);
    });

	// this is not a smart stopper, but it does something
    auto dressedStopper = makeDressedRestarter(makeCostDecreaseDressedStopper(1));

    auto group = makeDGroup_bfgs(problem, basisMaker,stopper, collector, stepSizeFinder, dressedStopper);

	group.optimize();

	ASSERT_EQ(maxIteration-2, group.numberOfResets()); // check that it resets after each iteration, not counting the last one
}

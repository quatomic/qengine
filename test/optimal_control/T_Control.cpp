﻿/* COPYRIGHT
 *
 * file="T_Control.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "gtest/gtest.h"

#include "src/NumberTypes.h"
#include "src/Abstract/OptimalControl/Parameters/Control.h"
#include "src/Abstract/OptimalControl/Parameters/BasisControl.h"
#include "src/Abstract/OptimalControl/Parameters/Basis.h"

#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/SparseMatrix.h"
#include "src/API/Common/OptimalControl/BasisFactory.h"
#include "additionalAsserts.h"


using namespace qengine;


class Control_Fixture : public ::testing::Test
{
public:
	count_t controllength = 11;
	real dt = 0.001;

	Control makeCurrentControl(const count_t paramCount = 1) const
	{
		auto controls = std::vector<RVec>();

		for (auto i = 0u; i < paramCount; ++i)
		{
			controls.push_back(linspace(i, 2 * (i + 1), controllength));
		}

		return makeControl(controls, dt);
	}
};

TEST_F(Control_Fixture, constructFromTime)
{
	const auto nPoints = 201ull;
	const auto T = 0.2;
	const auto t = makeTimeControl(nPoints, dt);
	EXPECT_EQ(T, t.getBack().back());
	EXPECT_EQ(T, t.endTime());


	const auto u = makeControl({ sin(t), cos(t) });
	EXPECT_EQ(nPoints, u.size());
	EXPECT_EQ(2ull, u.paramCount());
	EXPECT_EQ(sin(T), u.getBack().front());
	EXPECT_EQ(cos(T), u.getBack().back());

	const auto u2 = glue(makeLinearControl(0, 0.6, 100, dt), makeLinearControl(0.6, 0, 100, dt));

	EXPECT_EQ(199u, u2.size());
	EXPECT_EQ(1u, u2.paramCount());
	EXPECT_EQ(0, u2.getBack().front());
	EXPECT_EQ(0.6, u2.get(99).front());
}

TEST_F(Control_Fixture, append)
{
	auto control0 = makeCurrentControl(2);

	auto control = control0;
	control.append(-control0);

	EXPECT_EQ(2 * control0.size(), control.size());

	EXPECT_NEAR(control0.get(0).at(0), control.get(0).at(0), 1e-16);
	EXPECT_NEAR(-control0.get(control0.size() - 1).at(0), control.get(control.size() - 1).at(0), 1e-16);

	control = Control::zeros(0, 1, dt);

	EXPECT_EQ(0u, control.size());

	control = append(control0, -control0);

	EXPECT_EQ(2 * control0.size(), control.size());

	EXPECT_NEAR(control0.get(0).at(0), control.get(0).at(0), 1e-16);
	EXPECT_NEAR(-control0.get(control0.size() - 1).at(0), control.get(control.size() - 1).at(0), 1e-16);
}

TEST_F(Control_Fixture, invert)
{
	auto control0 = makeCurrentControl(2);

	auto inverted = invert(control0);

	for (auto i = 0u; i < control0.size(); ++i)
	{
		EXPECT_EQ(control0.get(i), inverted.get(control0.size() - 1 - i)) << i;
	}
}

TEST_F(Control_Fixture, glue)
{
	auto control0 = makeCurrentControl(2);

	auto control = control0;
	control.glue(invert(control0));

	EXPECT_EQ(2 * control0.size() - 1, control.size());

	EXPECT_EQ(control0.get(0), control.get(0));
	EXPECT_EQ(control0.get(0), control.get(control.size() - 1));
	EXPECT_EQ(control0.get(control0.size() - 1), control.get(control0.size() - 1));

	control = Control::zeros(0, 1, dt);

	EXPECT_EQ(0u, control.size());

	control = glue(control0, invert(control0));

	EXPECT_EQ(2 * control0.size() - 1, control.size());

	EXPECT_EQ(control0.get(0), control.get(0));
	EXPECT_EQ(control0.get(0), control.get(control.size() - 1));
	EXPECT_EQ(control0.get(control0.size() - 1), control.get(control0.size() - 1));

}

TEST_F(Control_Fixture, normL2)
{
    real dt = 0.1;
    Control control(RMat{{std::sqrt(2),std::sqrt(2),std::sqrt(5)},
                         {std::sqrt(4),std::sqrt(2),std::sqrt(10)}},
                    dt);

    RVec theNormsL2 = control.normsL2();
    ASSERT_NEAR(theNormsL2.at(0),3.0*sqrt(dt),1e-10);
    ASSERT_NEAR(theNormsL2.at(1),4.0*sqrt(dt),1e-10);

    real sumNormL2 = control.normL2();
    ASSERT_NEAR(sumNormL2,(3.0 + 4.0)*sqrt(dt),1e-10);

    RVec theNorms = control.norms();
    ASSERT_EQ(theNorms.at(0),theNormsL2.at(0));
    ASSERT_EQ(theNorms.at(1),theNormsL2.at(1));

    real sumNorm = control.norm();
    ASSERT_EQ(sumNorm,sumNormL2);
}


TEST_F(Control_Fixture, vecRef)
{
	Control control = Control::zeros(2, 2, dt);

	EXPECT_EQ(control.get(0).at(0), 0);

	control._vecRef().at(0) = 1;
	EXPECT_EQ(control.get(0).at(0), 1);
}

TEST_F(Control_Fixture, mean)
{
	auto control = makeControl({ {0.0, 1.0}, {1.0,2.0} }, dt);

	auto means = qengine::means(control);
	EXPECT_NEAR(0.5, means.at(0), 1e-16);
	EXPECT_NEAR(1.5, means.at(1), 1e-16);

	EXPECT_NEAR(1.0, mean(control), 1e-16);
}

TEST_F(Control_Fixture, max)
{
	auto control = makeControl({ {0.0, 1.0}, {1.0,2.0} }, dt);

	auto maxs = qengine::maxs(control);
	EXPECT_NEAR(1.0, maxs.at(0), 1e-16);
	EXPECT_NEAR(2.0, maxs.at(1), 1e-16);

	EXPECT_NEAR(2.0, max(control), 1e-16);
}

TEST_F(Control_Fixture, min)
{
	auto control = makeControl({ {0.0, 1.0}, {1.0,2.0} }, dt);

	auto mins = qengine::mins(control);
	EXPECT_NEAR(0.0, mins.at(0), 1e-16);
	EXPECT_NEAR(1.0, mins.at(1), 1e-16);

	EXPECT_NEAR(0.0, min(control), 1e-16);
}


TEST_F(Control_Fixture, makePieceWiseLinearControl)
{
	auto x0 = 0.0;
	auto x1 = 0.1;

	const auto n1 = 10;
	const auto n2 = 20;
	const auto n_steps = 30u;

	const auto u = makePieceWiseLinearControl({
		{ 0, x0 },
		{ n1, x1 } ,
		{ n2, x1 },
		{ n_steps, x0 }
		}, dt);

	EXPECT_EQ(n_steps + 1, u.size());
	EXPECT_EQ(x0, u.get(0).at(0));
	EXPECT_EQ(x1, u.get(n1).at(0));
	EXPECT_EQ(x1, u.get(n2).at(0));
	EXPECT_EQ(x0, u.get(n_steps).at(0));
}

class Basis_Fixture : public Control_Fixture
{
public:
};

TEST_F(Basis_Fixture, multiplyShapeFunction)
{

	const auto basisSize = 5u;
	const auto paramCount = 2u;
	const auto maxRandval = 0.5;

	const auto basis = makeSineBasis(basisSize, paramCount, controllength, dt, maxRandval);

	ASSERT_EQ(basis.size(), basisSize);

	const auto t = makeTimeControl(controllength, dt);
	const auto shapeFunction1 = sin(PI*t / t.endTime());

	const auto shapedBasis1 = shapeFunction1 * basis;

	for (auto i_basis = 0u; i_basis < basisSize; ++i_basis)
	{
		const auto& c = shapedBasis1.at(i_basis);
		const auto& b = basis.at(i_basis);
		for (auto i_param = 0u; i_param < paramCount; ++i_param)
		{
			for (auto i_time = 0u; i_time < controllength; ++i_time)
			{
				ASSERT_EQ(c.at(i_time).at(i_param), shapeFunction1.at(i_time).at(0)*b.at(i_time).at(i_param));
			}
		}
	}

	const auto shapeFunction2 = makeControl({ shapeFunction1, 2 * shapeFunction1 });
	const auto shapedBasis2 = shapeFunction2 * basis;

	for (auto i_basis = 0u; i_basis < basisSize; ++i_basis)
	{
		const auto& c = shapedBasis2.at(i_basis);
		const auto& b = basis.at(i_basis);

		ASSERT_NEAR_M(shapeFunction2.mat() % b.mat(), c.mat(), 1e-16);
	}


}

TEST_F(Basis_Fixture, multiplyBasisParamsWithBasis)
{
	// simplest case
	const auto u1 = makeControl({ {1,2} }, 0.1);
	const auto u2 = makeControl({ {3,5} }, 0.1);

	const auto basisS = Basis{ u1,u2 };

	const auto basisControlS = BasisControl{ RMat{{7, 11} }};

	const auto controlS = basisControlS * basisS;
	const auto referenceControlS = makeControl({ {40, 69} }, 0.1);

	ASSERT_NEAR_M(controlS.mat(), referenceControlS.mat(), 1e-14);

	// more complex case
	const auto p1 = makeControl({ {0,0.5,1},{ 1,1,0 } }, 0.1);
	const auto p2 = makeControl({ {2,3,4},{ 0.5,1,6 } }, 0.1);

	const auto basis = Basis{ p1,p2 };

	const auto basisControl = BasisControl{ RMat{{1,2},{3,4}} };

	const auto control = basisControl * basis;

	const auto referenceControl = makeControl({ {4, 6.5, 9},{5,7,24} }, 0.1);

	ASSERT_EQ(referenceControl.dt(), control.dt());
	ASSERT_NEAR_M(control.mat(), referenceControl.mat(), 1e-14);
}

TEST_F(Basis_Fixture, norms)
{
    BasisControl basis(RMat{{std::sqrt(2),std::sqrt(2),std::sqrt(5)},
                      {std::sqrt(4),std::sqrt(2),std::sqrt(10)}});

    RVec theNorms = basis.norms();
    real sumNorm  = basis.norm();

    ASSERT_NEAR(theNorms.at(0),3,  1e-10);
    ASSERT_NEAR(theNorms.at(1),4,  1e-10);
    ASSERT_NEAR(sumNorm,       3+4,1e-10);
}

﻿/* COPYRIGHT
 *
 * file="T_BasisFactory.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "src/API/Common/OptimalControl/BasisFactory.h"
#include "additionalAsserts.h"

using namespace qengine;

class BasisFactory_fixture : public ::testing::Test
{
public:

};

TEST_F(BasisFactory_fixture, sineBasis)
{
	const auto paramCount = 2u;
	const auto nBasisElements = 5u;
	const auto factors = internal::makeRandomizedFactors(nBasisElements, paramCount, 0.0);

	ASSERT_EQ(paramCount, factors.n_rows());
	ASSERT_EQ(nBasisElements, factors.n_cols());

	auto controlSize = 5u;
	auto dt = 0.1;
	const auto basis = makeSineBasis(nBasisElements, {paramCount, controlSize, dt});

	for (auto i = 1u; i <= controlSize; ++i) 
	{
		ASSERT_NEAR_V(kron(sin(linspace(0, i*PI, controlSize)), RVec{ 1,1 }), basis.at(i-1)._vec(), 1e-15);
	}
}

TEST_F(BasisFactory_fixture, fourierBasis)
{
	const auto paramCount = 2u;
	const auto nBasisElements = 6u;

	auto controlSize = 5u;
	auto dt = 0.1;
	const auto basis = makeFourierBasis(nBasisElements, {paramCount, controlSize, dt});

	for (auto i = 1u; i <= controlSize; ++i) 
	{
		if(i%2==1) ASSERT_NEAR_V(kron(cos(linspace(0, i*2*PI, controlSize)), RVec{ 1,1 }), basis.at(i-1)._vec(), 1e-15);
		else ASSERT_NEAR_V(kron(sin(linspace(0, i*2*PI, controlSize)), RVec{ 1,1 }), basis.at(i-1)._vec(), 1e-15);
	}
}

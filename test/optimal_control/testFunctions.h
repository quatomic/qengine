﻿/* COPYRIGHT
 *
 * file="testFunctions.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/NumberTypes.h"
#include "src/Utility/Constants.h"
#include "src/Utility/LinearAlgebra/Vector.h"

#include "src/Abstract/OptimalControl/Parameters/Control.h"


using namespace qengine;
using namespace internal;

// rosenbrock-function. minimum at f(1.0, ..., 1.0) = 0.0
inline real rosenbrock(const RVec& x)
{
	auto retVal = 0.0;

	for(auto i = 0u;i<x.size()-1;++i)
	{
		retVal += 100 * pow(x.at(i + 1) - pow(x.at(i), 2), 2) + pow(x.at(i) - 1, 2);
	}

	return retVal;
}

inline RVec grad_rosenbrock(const RVec& x)
{
	auto retVal = RVec(x.size(), 0.0);
	auto N = x.size() - 1;

	{
		auto x1 = x.at(0);
		auto x2 = x.at(1);
		retVal.at(0) = 2 * (x1 - 1) - 400 * x1*(x2 - pow(x1, 2));
	}
	{
		retVal.at(N) = 200 * (x.at(N) - pow(x.at(N - 1), 2));
	}

	for(auto i = 1u; i<N;++i)
	{
		retVal.at(i) = 2 * (x.at(i) - 1) + 200 * (x.at(i) - pow(x.at(i - 1), 2)) - 400 * x.at(i)*(x.at(i + 1) - pow(x.at(i), 2));
	}

	return retVal;
}

inline real parabola(const RVec& x)
{
	return dot(x, x);
}

inline RVec grad_parabola(const RVec&x)
{
	return 2.0 * x;
}

// himmelblau-function. minimum at f(p) = 0.0 for p=		
// {3,2},
// { -2.805118, 3.131312 },
// { -3.779310, -3.283186 },
// { 3.584428,  -1.848126 }
inline real himmelblau(const RVec& xs)
{
	const auto& x = xs.at(0);
	const auto& y = xs.at(1);

	return pow(x*x + y - 11, 2) + pow(x + y*y - 7, 2);
}


inline RVec grad_himmelblau(const RVec& xs)
{
	const auto& x = xs.at(0);
	const auto& y = xs.at(1);

	auto retVal = RVec{0.0,0.0};

	retVal.at(0) = 4 * x*(x*x + y - 11) + 2 *   (x + y*y - 7);
	retVal.at(1) = 2 *   (x*x + y - 11) + 4*y * (x + y*y - 7);

	return retVal;
}

inline real gauss(real x) { return exp(-x*x); }

// easom-function. Minimum at f(PI,PI) = -1
inline real easom(const RVec& xs)
{
	const auto& x = xs.at(0);
	const auto& y = xs.at(1);

	return -cos(x)*cos(y)*gauss(x-PI)*gauss(y-PI);
}


inline RVec grad_easom(const RVec& xs)
{
	const auto& x = xs.at(0);
	const auto& y = xs.at(1);

	auto retVal = RVec{ 0.0,0.0 };

	retVal.at(0) = cos(y)*gauss(y - PI)*gauss(x - PI)*(sin(x) + 2 * (x - PI)*cos(x));
	retVal.at(1) = cos(x)*gauss(x - PI)*gauss(y - PI)*(sin(y) + 2 * (y - PI)*cos(y));

	return retVal;
}



class RosenBrockProblem
{
public:
	explicit RosenBrockProblem(const Control& control) :_control(control) {}

	void update(const Control& control) { _control = control; }
	void update(count_t i, const RVec& param) { _control.at(i) = param; }

	real cost() const { return rosenbrock(_control._vec()); }
	auto gradient() const { return Control(grad_rosenbrock(_control._vec()), _control.paramCount(), dt()); }

	RVec gradient(count_t index) const { return Control(grad_rosenbrock(_control._vec()), _control.paramCount(), dt()).at(index); }

	auto control() const { return _control; }
	auto _params() const { return control(); }

	real dt() const { return 0.001; }

private:
	Control _control;
}; 

class ParabolaProblem
{
public:
	explicit ParabolaProblem(const Control& control) :_control(control) {}

	void update(const Control& control) { _control = control; }
	void update(count_t i, const RVec& param) { _control.at(i) = param; }

	real cost() const { return parabola(_control._vec()); }
	auto gradient() const { return Control(grad_parabola(_control._vec()), _control.paramCount(), dt()); }

	RVec gradient(count_t index) const { return Control(grad_parabola(_control._vec()), _control.paramCount(), dt()).at(index); }

	auto control() const { return _control; }
	auto _params() const { return control(); }

	real dt() const { return 0.001; }

private:
	Control _control;
};

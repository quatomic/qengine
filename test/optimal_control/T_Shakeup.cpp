﻿/* COPYRIGHT
 *
 * file="T_Shakeup.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include <qengine/GPE.h>
#include <qengine/OptimalControl.h>

#include "src/API/UserUtilities/AutoMember.h"
#include "additionalAsserts.h"


using namespace qengine;


namespace shakeup_testing
{
	template<class T>
	auto makeDynamicPotential(const T& x, const real p6ho, const real p4ho, const real p2ho)
	{
		return [p6ho, p4ho, p2ho, x](const RVec& v)
		{
			const auto& u = v.at(0);
			return p6ho * pow(x - u, 6) +
				p4ho * pow(x - u, 4) +
				p2ho * pow(x - u, 2);
		};
	}
}

class Shakeup_Fixture : public ::testing::Test
{
public:
	Shakeup_Fixture(const real T):duration(T) {}

	//calculate beta and kinematicFactor
	const real hbar = 1.054571800*1e-34;               // Planck's reduced constant (J*s)
	const real 	h = 2 * PI*hbar;                          // Planck's constant (J*s)
	const real 	atomicUnit = 1.660539040*1e-27;         // Atomic mass unit 1 / 12 of C(kg)
	const real 	a0 = 5.291771e-11;                        // Bohr radius(m)

	const count_t nAtoms = 700;
														  // Units for Rubidium 87
	const real 	mRb87 = 86.909180527*atomicUnit;        // Mass of the atom(kg)
	const real 	a11 = 100.44*a0;                          // Three - dimensional s - scattering length | 1, -1>.
	const real 	r0 = 172e-9;                            // Mean radial ground state radius(m).

	const real wx = 2 * PI*16.3;                     // Axial frequency of the trap(Hz).
	const real wy = 2 * PI*1.83e3;                   // Frequency along the(transverse) y direction(Hz).
	const real 	wz = 2 * PI*2.58e3;                   // Frequency along the(transverse) z direction(Hz).
	const real 	wr = sqrt(wy*wz);

	const real ax = sqrt(hbar / mRb87 / wx);           // Axial oscillator length(m).
	const real 	az = sqrt(hbar / mRb87 / wz);           // Oscillator length along the(transverse) z direction(m).
	const real 	ar = sqrt(hbar / mRb87 / wr);           // Transverse mean oscillator length(m).

														// Setting up the potential
	const real 	p2 = hbar * 2 * PI * 310 / pow(r0, 2);            // Strength of the quadratic term of the potential(J / m ^ 2).
	const real 	p4 = hbar * 2 * PI*13.6 / pow(r0, 4);         // Strength of the quartic term of the potential(J / m ^ 4).
	const real 	p6 = -hbar * 2 * PI*0.0634 / pow(r0, 6);        // Strength of the sixth term of the potential(J / m ^ 6).

																// Define Antonio's units
	const real 	w0 = sqrt(2 * p2 / mRb87);              // Harmonic oscillator frequency m*wy ^ 2 / 2 = p2(Hz).
	const real 	E0 = hbar*w0;                       // Corresponding energy scale along y(J).
	const real 	lho = sqrt(hbar / mRb87 / w0);          // Harmonic oscillator length(m).

														// Define potential in the new units
	const real 	p2ho = p2 / E0*pow(lho, 2);                 // Strength of the quadratic term of the potential.
	const real 	p4ho = p4 / E0*pow(lho, 4);                  // Strength of the quartic term of the potential.
	const real 	p6ho = p6 / E0*pow(lho, 6);                  // Strength of the sixth term of the potential.

															 // Start calculation of beta
	const real 	a3D = a11;

	//const auto falpha = [=](auto a) {return pow(a, 3) * pow(a + 5, 2) - pow(15 * nAtoms*ar*a3D / pow(ax, 2), 2); };
	const real alpha = 0.477869867335240; // stupid magic number. calculated as fzero(falpha, 0.5) in matlab.

	const real L = pow(ax, 2) * sqrt(alpha) / ar;            // Condensate length(m).

															 // Coupling constant g1D(J*m) for the GPE along the y direction.
	const real 	g1DJm = (2 * sqrt(2 * PI)*pow(hbar, 2)) / (mRb87*az*a3D)*(L*pow(alpha, 2)) / 315 * (pow(alpha, 2) + 9 * alpha + 21) / pow(nAtoms, 2);

	// Parameters in Schrödinger equation.
	const real 	kin = hbar / (2 * mRb87*pow(lho, 2) * w0);
	const real beta = g1DJm / (E0*lho)*nAtoms;

	const real dt = 0.0025;

	const real duration;
	const Control initialControl = makeControl({ 0.5*3.6*sin((PI / duration* linspace(0.0,duration,static_cast<count_t>(std::round(duration / dt + 1))))) }, dt);

	const RVec lowerLeft{ -1e-6/lho };
	const RVec upperRight{ 1e-6 / lho };

	const AUTO_MEMBER( s, gpe::makeHilbertSpace(-8, 8, 256, kin));

	AUTO_MEMBER(V, makePotentialFunction(shakeup_testing::makeDynamicPotential(s.x(), p6ho, p4ho, p2ho), initialControl.getFront()));
	AUTO_MEMBER(H, s.T() + V + makeGpeTerm(beta));

	AUTO_MEMBER(H0, H(RVec{0.0}));

	AUTO_MEMBER( initialState, makeWavefunction(H0[0])); // ground state
	AUTO_MEMBER(targetState, makeWavefunction(H0[1])); // first excited state


	auto makeProblem()
	{
		return makeStateTransferProblem(H, initialState, targetState, initialControl);
	}

};

class Shakeup_Basics_Fixture: public Shakeup_Fixture
{
public:
	Shakeup_Basics_Fixture() : Shakeup_Fixture(1.0){}

};

TEST_F(Shakeup_Basics_Fixture, psis)
{
	auto problem = makeProblem();

	const auto psis = problem.psis();
	const auto control = problem.control();

	//auto alg = addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._serializedHilbertSpace()), dt);

	auto stepper = makeFixedTimeStepper(H, psis.front(), dt);

	for (auto i = 1u; i< psis.size(); ++i)
	{
		stepper.step(control.get(i));

		ASSERT_NEAR_V(stepper.state().vec(), psis.at(i).vec(), 1e-15);
	}

	auto stopper = makeStopper([](const auto&) {return true; }); //stops after one iteration

	auto grape = makeGrape_steepest_L2(problem, stopper);
	grape.optimize();

	const auto psis_optimize = grape.problem().psis();
	stepper.reset(psis_optimize.front(), makeTuple(grape.problem().control().get(0)));

	for (auto i = 1u; i< psis_optimize.size(); ++i)
	{
		stepper.step(grape.problem().control().get(i));

		ASSERT_NEAR_V(stepper.state().vec(), psis_optimize.at(i).vec(), 1e-15);
	}
}

TEST_F(Shakeup_Basics_Fixture, full_control_consistency)
{
	auto problem = makeProblem();

	// initial step test
	ASSERT_EQ(problem.control(), initialControl);
	ASSERT_EQ(problem.initialControl(), initialControl);
	ASSERT_EQ(problem.dt(), dt);

	auto expectedStepCount = 0ull;
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	auto fidelity = problem.fidelity();
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 1);

	auto cost = problem.cost(); // should not increase stepcount because uses fidelity
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	auto grad = problem.gradient();
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 2);

	// update and test again
	const auto newControl = makeControl({ linspace(0,0,initialControl.size()) }, dt);
	ASSERT_NE(problem.control(), newControl);

	problem.update(newControl);
	ASSERT_EQ(problem.control(), newControl);
	ASSERT_EQ(problem.initialControl(), initialControl);
	ASSERT_EQ(problem.dt(), newControl.dt());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	ASSERT_NE(fidelity, problem.fidelity());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 1);

	ASSERT_NE(cost, problem.cost());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	ASSERT_NE(grad, problem.gradient());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += initialControl.size() - 2);

	problem.resetPropagationCount();
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount = 0);
}

TEST_F(Shakeup_Basics_Fixture, initialAndTargetStates)
{
	auto tol = 4e-9;
	ASSERT_NEAR(0.0, variance(H(initialControl.getFront()), initialState), tol);
	ASSERT_NEAR(0.0, variance(H(initialControl.getBack()), targetState), tol);
}

TEST_F(Shakeup_Basics_Fixture, physicsGradient)
{
	auto problem = makeProblem();

	auto gradDiff = gradientDifference(problem);

	ASSERT_NEAR(0.0, gradDiff, 6e-5);
}


TEST_F(Shakeup_Basics_Fixture, gradientWithRegAndBounds)
{
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) +10 * Boundaries(initialControl, lowerLeft, upperRight);

	auto gradDiff = gradientDifference(problem);

	ASSERT_NEAR(0.0, gradDiff, 6e-5);
}


class Shakeup_algs_Fixture : public Shakeup_Fixture
{
public:
	Shakeup_algs_Fixture() : Shakeup_Fixture(1.0) {}

	count_t maxIteration = 10u;

};

TEST_F(Shakeup_algs_Fixture, alg_grape_steepest_L2)
{
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration{maxIteration}](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });

	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);
	std::cout << "alpha=";
	auto grape = makeGrape_steepest_L2(problem, stopper, collector, stepSizeFinder);
	grape.optimize();
	std::cout << std::endl;

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);

	//ASSERT_TRUE(false);
}

TEST_F(Shakeup_algs_Fixture, alg_grape_steepest_H1) //disabled until we fix H1
{
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration{maxIteration}](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
		std::cout << alg.stepSize() << ",";
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(200, 2);

	//std::cout << "alpha=";
	auto grape = makeGrape_steepest_H1(problem, stopper, collector, stepSizeFinder);
	grape.optimize();
	std::cout << std::endl;

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);

}

TEST_F(Shakeup_algs_Fixture, alg_grape_bfgs_L2) // disabled until we fix BFGS
{
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration{maxIteration}](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		//std::cout << alg.iteration() << " cost: " << alg.problem().cost() << "; fpp: " << alg.problem().nPropagationSteps() / (alg.problem().control().size() - 1) << "; fidelity: " << alg.problem().fidelity() << "; stepSize: " << alg.stepSize() << std::endl;

		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);
	
	auto grape = makeGrape_bfgs_L2(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);
}

TEST_F(Shakeup_algs_Fixture, alg_grape_bfgs_H1) // disabled until we fix H1 and BFGS
{
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
    auto stopper = makeStopper([maxIteration{maxIteration}](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		//std::cout << alg.iteration() << " cost: " << alg.problem().cost() << "; fpp: " << alg.problem().nPropagationSteps() / (alg.problem().control().size() - 1) << "; fidelity: " << alg.problem().fidelity() << "; stepSize: " << alg.stepSize() << std::endl;

		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(1000, 1);
	
	auto grape = makeGrape_bfgs_H1(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);

}

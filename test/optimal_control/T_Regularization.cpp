﻿/* COPYRIGHT
 *
 * file="T_Regularization.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <qengine/OptimalControl.h>

using namespace qengine;


class Regularization_Fixture : public ::testing::Test
{
public:
	count_t controllength = 2001;
	const real dt = 0.001;

	Control makeLinearControl(const count_t paramCount = 1, const real multiplier= 1.0) const
	{

		auto controls = std::vector<RVec>();
		controls.reserve(paramCount);

		for (auto i = 0u; i<paramCount; ++i)
		{
			controls.push_back(linspace(i, multiplier * (i + 1), controllength));
		}

		return makeControl(controls, dt);
	}

	Control makeParabolicControl(const count_t paramCount = 1) const
	{
		auto x0_mat = arma::mat(paramCount, controllength);
		for (auto i = 0u; i < paramCount; ++i)
		{
			x0_mat.row(i) = arma::pow(arma::linspace(i,  i + 1, controllength).t(),2);
		}
		return Control(RMat(x0_mat), dt);

	}
};

static_assert(internal::is_QOCProblem_v<Regularization>, "");

TEST_F(Regularization_Fixture, cost)
{
	const auto control = makeLinearControl(1, 2.0); // correspond to f(t) = t on interval 0<t<2.0

	auto regularization= Regularization(control, 2.0);

	EXPECT_NEAR( (controllength-1)*dt, regularization.cost(), 4e-15); // expected to be integral(t) from 0 to 2 = 2

	regularization = Regularization(control, 4.0);

	EXPECT_NEAR(2* (controllength - 1)*dt, regularization.cost(), 7e-15);
}

TEST_F(Regularization_Fixture, gradient)
{
	const auto t = linspace(0, 1, 100);
	const auto control = makeControl({ exp(t) }, dt);
	
	auto problem = Regularization(control, 1.0);

	auto gradDiff = gradientDifference(problem);
	ASSERT_NEAR(gradDiff, 0.0, 1e-8);
}

TEST_F(Regularization_Fixture, indexedCost)
{
	auto control = makeLinearControl(1, 2.0);

	auto regularization = Regularization(control, 1.0);

	for (auto i = 0u; i < control.size(); ++i) 
	{
		ASSERT_NEAR(pow(2.0, 2) / pow(controllength - 1, 2), regularization._cost(0), 1e-16);
	}
}

TEST_F(Regularization_Fixture, indexedGradient)
{
	auto control = makeParabolicControl(1);

	auto regularization = Regularization(control, 2.0);

	EXPECT_NEAR(0.0, regularization._gradient(0).at(0), 1e-16);
	EXPECT_NEAR(0.0, regularization._gradient(controllength-1).at(0), 1e-16);

	for (auto i = 1u; i < control.size()-1; ++i)
	{
		ASSERT_NEAR(-4.0 / pow(controllength - 1, 2), regularization._gradient(i).at(0), 1e-15);
	}
}


TEST_F(Regularization_Fixture, multiply)
{
	const auto control = makeLinearControl(1, 2.0);

	auto regularization = Regularization(control, 2.0);

	/// expected regularization-cost for linear control = multiplier^2/(N-1)
	EXPECT_NEAR((controllength - 1)*dt, regularization.cost(), 4e-15); // expected to be integral(t) from 0 to 2 = 2

	regularization*=2.0;

	EXPECT_NEAR(2*(controllength - 1)*dt, regularization.cost(), 7e-15); // expected to be integral(2*t) from 0 to 2 = 2

	regularization = 2.0*regularization;

	EXPECT_NEAR(4*(controllength - 1)*dt, regularization.cost(), 14e-15); // expected to be integral(2*2*t) from 0 to 2 = 2

}

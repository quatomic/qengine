﻿/* COPYRIGHT
 *
 * file="T_BringHomeWater.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "F_BringHomeWater.h"
#include "additionalAsserts.h"

TEST_F(BringHomeWater_Fixture, psis)
{
	auto problem = makeProblem();

	const auto psis = problem.psis();
	const auto control = problem.control();

	//auto alg = addImagBounds(splitStep(H, dt), makeDefaultImagPot(H._H0()._serializedHilbertSpace()), dt);

	auto stepper = makeFixedTimeStepper(H, psis.front(), dt);

	for(auto i = 1u; i< psis.size(); ++i)
	{
		stepper.step(control.get(i));

		ASSERT_NEAR_V(stepper.state().vec(), psis.at(i).vec(), 1e-15);
	}

	auto stopper = makeStopper([](const auto&) {return true; }); //stops after one iteration

	auto grape = makeGrape_steepest_L2(problem, stopper);
	grape.optimize();

	const auto psis_optimize = grape.problem().psis();
	stepper.reset(psis_optimize.front(), makeTuple(grape.problem().control().get(0)));

	for (auto i = 1u; i< psis_optimize.size(); ++i)
	{
		stepper.step(grape.problem().control().get(i));

		ASSERT_NEAR_V(stepper.state().vec(), psis_optimize.at(i).vec(), 1e-15);
	}
}

TEST_F(BringHomeWater_Fixture, full_control_consistency)
{
	auto problem = makeProblem();

	// initial step test
	ASSERT_EQ(problem.control(), initialControl);
	ASSERT_EQ(problem.initialControl(), initialControl);
	ASSERT_EQ(problem.dt(), dt);

	auto expectedStepCount = 0ull;
	ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());

	auto fidelity = problem.fidelity();
	ASSERT_EQ(expectedStepCount += initialControl.size() - 1, problem.nPropagationSteps());

	auto cost = problem.cost(); // should not increase stepcount because uses fidelity
	ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());

	auto grad = problem.gradient();
	ASSERT_EQ(expectedStepCount += initialControl.size() - 2, problem.nPropagationSteps());

	// update and test again
	const auto newControl = makeControl({
		glue(linspace(-0.8, 0.55, 5), linspace(0.55, -0.75, 197)),
		glue(linspace(-1,  -1.4, 5), linspace(-1.4, -1.4, 197))
		}, 0.99*dt);

	
	ASSERT_NE(problem.control(), newControl);

	problem.update(newControl);
	ASSERT_EQ(problem.control(), newControl);
	ASSERT_EQ(problem.initialControl(), initialControl);
	ASSERT_EQ(problem.dt(), newControl.dt());
	ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());

	ASSERT_NE(fidelity, problem.fidelity());
	ASSERT_EQ(expectedStepCount += initialControl.size() - 1, problem.nPropagationSteps());

	ASSERT_NE(cost, problem.cost());
	ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());

	ASSERT_NE(grad, problem.gradient());
	ASSERT_EQ(expectedStepCount += initialControl.size() - 2, problem.nPropagationSteps());

	problem.resetPropagationCount();
	ASSERT_EQ(expectedStepCount = 0, problem.nPropagationSteps());
}

TEST_F(BringHomeWater_Fixture, stepwise_consistency)
{
	auto problem = makeProblem();

	// initial step test
	ASSERT_EQ(problem.control(), initialControl);
	ASSERT_EQ(problem.initialControl(), initialControl);
	ASSERT_EQ(problem.dt(), dt);

	auto expectedStepCount = 0u;
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	auto fidelity = problem.fidelity();
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += static_cast<unsigned int>(initialControl.size() - 1));

	auto cost = problem.cost(); // should not increase stepcount because uses fidelity
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	for (auto i = initialControl.size() - 2; i >= 1; --i)
	{
		auto grad = problem._gradient(i);
		ASSERT_EQ(++expectedStepCount, problem.nPropagationSteps());
		grad = problem._gradient(i);
		ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());
	}

	ASSERT_EQ(cost, problem.cost()); // should not increase stepcount
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);


	problem._update(1, RVec{ 0.0, 0.0 }); // 

	ASSERT_NE(fidelity, problem.fidelity());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount += 2);

	ASSERT_NE(cost, problem.cost());
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);


	auto g = problem._gradient(1);
	ASSERT_EQ(problem.nPropagationSteps(), expectedStepCount);

	for (auto i = 2u; i < initialControl.size(); ++i)
	{
		auto grad = problem._gradient(i);
		ASSERT_EQ(++expectedStepCount, problem.nPropagationSteps());
		grad = problem._gradient(i);
		ASSERT_EQ(expectedStepCount, problem.nPropagationSteps());
	}
}


TEST_F(BringHomeWater_Fixture, initialAndTargetStates)
{
	auto tol = 1e-9;
	ASSERT_NEAR(0.0, variance(H(initialControl.getFront()), initialState), tol);
	ASSERT_NEAR(0.0, variance(H(initialControl.getBack()), targetState), tol);
}

TEST_F(BringHomeWater_Fixture, dVdu)
{
	auto problem = makeProblem();
	auto dVdu = problem.dHdu();

	auto tol = 1e-14;

	for (auto j = 0u; j < initialControl.size(); ++j)
	{
		auto infidelities = infidelityWithDiffPotential(dVdu, V_dyn, initialControl.at(j));

		for (auto i = 0u; i < initialControl.paramCount(); ++i)
		{
			ASSERT_NEAR(0.0, infidelities.at(i), tol);
		}
	}
}

TEST_F(BringHomeWater_Fixture, physicsGradient)
{
	auto problem = makeProblem();

	auto gradDiff = gradientDifference(problem);


	ASSERT_NEAR(0.0, gradDiff, 2e-8);
}


TEST_F(BringHomeWater_Fixture, gradientWithRegAndBounds)
{
	auto problem = makeProblem() + 1e-9*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);

	auto gradDiff = gradientDifference(problem);

	ASSERT_NEAR(0.0, gradDiff, 2e-7);
}

TEST_F(BringHomeWater_Fixture, alg_grape_steepest_L2)
{
	auto maxIteration = 50u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
	auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });

	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());

	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	auto grape = makeGrape_steepest_L2(problem, stopper, collector, stepSizeFinder);

	grape.optimize();

	const auto psis = problem.psis();
	const auto control = problem.control();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);
}

TEST_F(BringHomeWater_Fixture, alg_grape_steepest_H1)
{
	auto maxIteration = 50u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
	auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(1000, 2);

	auto grape = makeGrape_steepest_H1(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);

}

TEST_F(BringHomeWater_Fixture, alg_grape_bfgs_L2)
{
	auto maxIteration = 50u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
	auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	auto grape = makeGrape_bfgs_L2(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);
}

TEST_F(BringHomeWater_Fixture, alg_grape_bfgs_H1)
{
	auto maxIteration = 50u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
	auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(1000, 1);
	
	auto grape = makeGrape_bfgs_H1(problem, stopper, collector, stepSizeFinder);
	grape.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);

}

class BringHomeWater_BasisProblem_Fixture: public BringHomeWater_Fixture
{
public:

};

TEST_F(BringHomeWater_BasisProblem_Fixture, update)
{
	const auto problem = makeProblem() ;

	const auto nBasisElements = 3;
	const auto basis = makeSineBasis(nBasisElements, initialControl.metaData());

	auto basisProblem = optimal_control::makeBasisProblem(problem, basis, initialControl);

	auto params = BasisControl(RMat{ {0,0,0},{0,0,0} });
	
	basisProblem.update(params);

	ASSERT_NEAR_M(basisProblem._params().mat(), params.mat(), 1e-16);
	ASSERT_NEAR_M(basisProblem.control().mat(), initialControl.mat(), 1e-16);

	params = BasisControl(RMat{ { 1,0,0 },{ 1,0,0 } });
	basisProblem.update(params);

	const auto refControl = basis.at(0) + initialControl;

	ASSERT_NEAR_M(params.mat(), basisProblem._params().mat(), 1e-16);
	ASSERT_NEAR_M(refControl.mat(), basisProblem.control().mat(), 1e-16);
}

TEST_F(BringHomeWater_Fixture, alg_group_steepest)
{
	auto maxIteration = 10u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
	auto stopper = makeStopper([maxIteration](const auto& alg){	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
		std::cout << alg.iteration() << ": cost " << alg.problem().cost() << "; fidelity: " << alg.problem().fidelity() << std::endl;
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	const auto t = makeTimeControl(initialControl.size(), initialControl.dt());
    const auto shapeFunction = makeSigmoidShapeFunction(t);
	auto basis = shapeFunction*makeSineBasis(10, initialControl.metaData(), 0.5);
	auto group = makeGroup_steepest(problem, basis, stopper, collector, stepSizeFinder);
	
	group.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);
}

TEST_F(BringHomeWater_Fixture, alg_group_bfgs)
{
	auto maxIteration = 20u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
	auto stopper = makeStopper([maxIteration](const auto& alg){	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });
	
	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
		//std::cout << alg.iteration() << ": cost " << alg.problem().cost() << "; fidelity: " << alg.problem().fidelity() << std::endl;
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	const auto t = makeTimeControl(initialControl.size(), initialControl.dt());
    const auto shapeFunction = makeSigmoidShapeFunction(t);
	auto basis = shapeFunction * makeSineBasis(10, initialControl.metaData(), 0.5);
	auto group = makeGroup_bfgs(problem, basis, stopper, collector, stepSizeFinder);
	
	group.optimize();

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 1);
	ASSERT_EQ(its, referenceIndex);
}

TEST_F(BringHomeWater_Fixture, alg_dressedGroup_steepest)
{
	auto maxIteration = 20u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
	auto stopper = makeStopper([maxIteration](const auto& alg) {	return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration); });

	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
		std::cout << alg.iteration() << ": cost " << alg.problem().cost() << "; fidelity: " << alg.problem().fidelity() << "; numberOfResets: " << alg.numberOfResets() <<std::endl;
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	const auto t = makeTimeControl(initialControl.size(), initialControl.dt());
	const auto shapeFunction = sin(PI*t / t.endTime());
	auto basisMaker = makeBasisMaker([metadata{initialControl.metaData()}, shapeFunction]()
	{
		return shapeFunction*makeSineBasis(10, metadata, 0.5);
	});

	// this is not a smart stopper, but it does something
	auto dressedRestarter = makeDressedRestarter([i{1u}](const auto&) mutable
	{
		++i;
		if(i>6u)
		{
			//std::cout << "restarting algorithm" << std::endl;
			i = 1u;
			return true;
		}
		return false;
	});


	auto group = makeDGroup_steepest(problem, basisMaker, stopper, collector, stepSizeFinder, dressedRestarter);

	collector(group);
	group.optimize();

	ASSERT_EQ(maxIteration+1, costs.size()); // one for each iteration plus the iteration 0 call.

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration+1);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 0);
	ASSERT_EQ(its, referenceIndex);
}

TEST_F(BringHomeWater_Fixture, alg_dressedGroup_bfgs)
{
	auto maxIteration = 20u;
	auto problem = makeProblem() + 1e-7*Regularization(initialControl) + 10 * Boundaries(initialControl, lowerLeft, upperRight);
	auto stopper = makeStopper([maxIteration](const auto& alg)
	{
		return (alg.problem().fidelity() > 0.999 || alg.iteration() >= maxIteration);
	});

	auto costs = std::vector<real>();
	auto its = std::vector<count_t>();
	auto collector = makeCollector([&costs, &its](const auto& alg)
	{
		its.push_back(alg.iteration());
		costs.push_back(alg.problem().cost());
		std::cout << alg.iteration() << ": cost " << alg.problem().cost() << "; fidelity: " << alg.problem().fidelity() << "; numberOfResets: " << alg.numberOfResets() <<std::endl;
	});
	auto stepSizeFinder = makeInterpolatingStepSizeFinder(2, 0.1);

	const auto t = makeTimeControl(initialControl.size(), initialControl.dt());
	const auto shapeFunction = sin(PI*t / t.endTime());
	auto basisMaker = makeBasisMaker([metadata{initialControl.metaData()}, shapeFunction]()
	{
		return shapeFunction*makeSineBasis(10, metadata, 0.5);
	});

	// this is not a smart stopper, but it does something
	auto dressedRestarter = makeDressedRestarter([i{1u}](const auto&) mutable
	{
		++i;
		if(i>6u)
		{
			//std::cout << "restarting algorithm" << std::endl;
			i = 1u;
			return true;
		}
		return false;
	});


	auto group = makeDGroup_bfgs(problem, basisMaker, stopper, collector, stepSizeFinder, dressedRestarter);

	collector(group);
	group.optimize();

	ASSERT_EQ(maxIteration + 1, costs.size()); // one for each iteration plus the iteration 0 call.

	auto sortedCosts = costs;
	std::sort(sortedCosts.rbegin(), sortedCosts.rend());
	ASSERT_EQ(costs, sortedCosts);

	auto referenceIndex = std::vector<count_t>(maxIteration+1);
	std::iota(referenceIndex.begin(), referenceIndex.end(), 0);
	ASSERT_EQ(its, referenceIndex);
}

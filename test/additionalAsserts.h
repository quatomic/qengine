﻿/* COPYRIGHT
 *
 * file="additionalAsserts.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include "gtest/gtest.h"
#include "src/Utility/LinearAlgebra/Vector.h"
#include "src/Utility/LinearAlgebra/Matrix.h"
#include "src/Utility/LinearAlgebra/SparseMatrix.h"


inline bool largerThanTol(const double d, const double tol)
{
    return std::abs(d) > tol;
}

inline bool largerThanTol(const std::complex<double>& c, const double tol)
{
    return std::abs(c.real()) > tol || std::abs(c.imag()) > tol;
}
namespace qengine_testing
{
    inline testing::AssertionResult assertNearC(const char*, const char*, const char*, const std::complex<double>& c_ref, const std::complex<double>& c, double tol)
    {
        auto diff = c - c_ref;

        if (largerThanTol(diff, tol))
        {
            return testing::AssertionFailure()
                << "diff between complex numbers exceeds tolerance(" << tol << ")! v-v_ref: \n"
                << diff
                << "\n v: \n" << c
                << "\n v_ref: \n" << c_ref
                ;
        }

        return testing::AssertionSuccess();
    }

    template<class T>
    testing::AssertionResult assertNearV(const char*, const char*, const char*, const qengine::Vector<T>& v_ref, const qengine::Vector<T>& v, double tol)
    {
        if (v.size() != v_ref.size())
        {
            return testing::AssertionFailure()
                << "size of test-vector is not same as reference! \n size of v: "
                << v.size() << "; size of ref: " << v_ref.size();
        }

		auto diff = v - v_ref;
		for (auto i = 0u; i < diff.size(); ++i)
		{
			if (largerThanTol(diff.at(i), tol))
			{
				return testing::AssertionFailure()
					<< "at least one diff between vectors is larger than tolerance(" << tol << ")! diff: \n"
					<< diff.at(i)
					<< "\n index: \n" << i
					;
			}
		}

        return testing::AssertionSuccess();
    }

	template<class T, template<class> class Matrix>
	testing::AssertionResult assertNearM(const char*, const char*, const char*, const Matrix<T>& m_ref, const Matrix<T>& m, double tol)
	{
		if (m.n_rows() != m_ref.n_rows() || m.n_cols() != m_ref.n_cols())
		{
			return testing::AssertionFailure()
				<< "dimensions of matrix are not same as reference! \n dims of m: ["
				<< m.n_rows() << ", " << m.n_cols() <<
				"; dims of ref: " << m_ref.n_rows() << ", " << m_ref.n_cols();
		}

		auto diff = m - m_ref;
		for (auto i = 0u; i < diff.n_elem(); ++i)
		{
			if (largerThanTol(diff.at(i), tol))
			{
				return testing::AssertionFailure()
					<< "at least one diff between matrices is larger than tolerance(" << tol << ")! diff: \n"
					<< diff.at(i) << "\n at index: " << i;
					;
			}
		}

		return testing::AssertionSuccess();
	}
	template<class T>
	testing::AssertionResult assertNearSPM(const char*, const char*, const char*, const qengine::internal::SparseMatrix<T>& m_ref, const qengine::internal::SparseMatrix<T>& m, double tol)
	{
		if (m.n_rows() != m_ref.n_rows() || m.n_cols() != m_ref.n_cols())
		{
			return testing::AssertionFailure()
				<< "dimensions of matrix are not same as reference! \n dims of m: ["
				<< m.n_rows() << ", " << m.n_cols() <<
				"; dims of ref: " << m_ref.n_rows() << ", " << m_ref.n_cols();
		}

        auto diff = m - m_ref;
        for (auto i = 0u; i < diff.n_elem(); ++i)
        {
            if (largerThanTol(diff.at(i), tol))
            {
                return testing::AssertionFailure()
                    << "largest diff between matrices is larger than tolerance(" << tol << ")! m-m_ref: \n"
                    << diff
                    << "\n m: \n" << m
                    << "\n m_ref: \n" << m_ref
                    ;
            }
        }

        return testing::AssertionSuccess();
    }
}

#define EXPECT_NEAR_V(expect, test, tol)\
    EXPECT_PRED_FORMAT3(qengine_testing::assertNearV, expect, test, tol)
#define ASSERT_NEAR_V(expect, test, tol)\
    ASSERT_PRED_FORMAT3(qengine_testing::assertNearV, expect, test, tol)


#define EXPECT_NEAR_M(expect, test, tol)\
    EXPECT_PRED_FORMAT3(qengine_testing::assertNearM, expect, test, tol)
#define ASSERT_NEAR_M(expect, test, tol)\
    ASSERT_PRED_FORMAT3(qengine_testing::assertNearM, expect, test, tol)

#define EXPECT_NEAR_SPM(expect, test, tol)\
    EXPECT_PRED_FORMAT3(qengine_testing::assertNearSPM, expect, test, tol)
#define ASSERT_NEAR_SPM(expect, test, tol)\
    ASSERT_PRED_FORMAT3(qengine_testing::assertNearSPM, expect, test, tol)

#define EXPECT_NEAR_C(expect, test, tol)\
    EXPECT_PRED_FORMAT3(qengine_testing::assertNearC, expect, test, tol)
#define ASSERT_NEAR_C(expect, test, tol)\
    ASSERT_PRED_FORMAT3(qengine_testing::assertNearC, expect, test, tol)

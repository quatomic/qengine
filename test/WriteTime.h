﻿/* COPYRIGHT
 *
 * file="WriteTime.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#include <iostream>
const unsigned int secondsPerMinute = 60;
const unsigned int minutesPerHour = 60;
const unsigned int HoursPerDay = 24;
inline void writeTime(unsigned int seconds)
{
	auto days = seconds / (secondsPerMinute*minutesPerHour*HoursPerDay);
	seconds = seconds % (secondsPerMinute*minutesPerHour*HoursPerDay);

	auto hours = seconds / (secondsPerMinute*minutesPerHour);
	seconds = seconds % (secondsPerMinute*minutesPerHour);

	auto minutes = seconds / (secondsPerMinute);
	seconds = seconds % (secondsPerMinute);

	if (days != 0)
	{
		std::cout << days << " days, ";
	}
	if (hours != 0)
	{
		std::cout << hours << " hours, ";
	}
	if (minutes != 0)
	{
		std::cout << minutes << " minutes and ";
	}
	std::cout << seconds << " seconds" << std::endl;
}

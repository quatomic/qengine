﻿/* COPYRIGHT
 *
 * file="T_DataContainer.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */


#include <gtest/gtest.h>
#include <fstream>
#include <regex>

#include "qengine/DataContainer.h"
#include "qengine/NLevel.h"
#include "qengine/OneParticle.h"
#include "qengine/OptimalControl.h"


#include "additionalAsserts.h"

#include "src/Utility/StringStuff.h"

using namespace qengine;
using namespace internal;
using namespace std::complex_literals;

class DataContainer_Fixture : public ::testing::Test
{
public:

};

TEST_F(DataContainer_Fixture, nvars)
{
	DataContainer dc{};

	ASSERT_EQ(dc.nVars(), 0u);

	dc["a"] = 1.0;// insert something, check nvars goes up
	ASSERT_EQ(dc.nVars(), 1u);

	dc["b"]= 1.0; // insert something more, check nvars goes up
	ASSERT_EQ(dc.nVars(), 2u);

	dc["a"]= 0.0; // replace existing, check nvars stays same
	ASSERT_EQ(dc.nVars(), 2u);
}

TEST_F(DataContainer_Fixture, getAllVars)
{
	DataContainer dc{};

	auto names = dc.varNames();
	ASSERT_EQ(names.size(), 0u);

	dc["a"] = 1.0;
	names = dc.varNames();
	ASSERT_EQ(names.size(), 1u);
	ASSERT_EQ(names.at(0), "a");

	dc["b"] = 1.0;
	names = dc.varNames();
	ASSERT_EQ(names.size(), 2u);
	ASSERT_EQ(names.at(0), "a");
	ASSERT_EQ(names.at(1), "b");

	dc["a"] = 0.0;
	names = dc.varNames();
	ASSERT_EQ(names.size(), 2u);
	ASSERT_EQ(names.at(0), "a");
	ASSERT_EQ(names.at(1), "b");
}

TEST_F(DataContainer_Fixture, clear)
{
	DataContainer dc{};

	ASSERT_EQ(dc.nVars(), 0u);

	dc["a"] = 1.0; 
	ASSERT_EQ(dc.nVars(), 1u);

	dc.clear();
	ASSERT_EQ(dc.nVars(), 0u);

}

TEST_F(DataContainer_Fixture, setGet)
{
	DataContainer dc{};

	auto real = 0.0;
	auto rvec = RVec{ 0.0,1.0 };
	auto rmat = RMat(3,2, 0.0);
	auto sp_rmat = Sp_RMat(rmat);

	auto complex = 1i;
	auto cvec = rvec + 1i* rvec;
	auto cmat = rmat + 1i*rmat;
	auto sp_cmat = Sp_CMat(cmat);

	dc["real"] =  real;
	dc["rvec"] =  rvec;
	dc["rmat"] =  rmat;

	dc["complex"] =  complex;
	dc["cvec"] =  cvec;
	dc["cmat"] =  cmat;

	qengine::real getReal = dc["real"];
	RVec getRVec = dc["rvec"];
	RMat getRMat = dc["rmat"];

	ASSERT_EQ(real, getReal);
	ASSERT_EQ(rvec, getRVec);
	ASSERT_EQ(rmat, getRMat);

	qengine::complex getComplex = dc["complex"];
	CVec getCVec = dc["cvec"];
	CMat getCMat = dc["cmat"];

	ASSERT_EQ(complex, getComplex);
	ASSERT_EQ(cvec, getCVec);
	ASSERT_EQ(cmat, getCMat);
}

TEST_F(DataContainer_Fixture, overwrite)
{
	auto rmat = RMat(3, 2, 0.0);

	auto cmat = rmat + 1i*rmat;
	auto sp_cmat = Sp_CMat(cmat);

	DataContainer dc{};
	dc["var"] =  rmat;
	RMat getRMat = dc["var"];

	ASSERT_EQ(rmat, getRMat);

	dc["var"]= 2.0 * rmat;
	getRMat = dc["var"];
	ASSERT_EQ(2.0*rmat, getRMat);

	dc["var"] =  cmat;
	CMat getCMat = dc["var"];
	ASSERT_EQ(cmat, getCMat);
}

TEST_F(DataContainer_Fixture, appendScalars)
{
	DataContainer dc{};

	dc["vec"] = 0;
	dc["vec"].append(1);
	dc["vec"].append(2);

	ASSERT_EQ(dc.nVars(), 1u);

	auto rvec = RVec{ 0,1,2 };
	RVec rvec_out = dc["vec"];
	ASSERT_EQ(rvec, rvec_out);
	
	dc["cvec"] =  1i;
	dc["cvec"].append( 2.0i);
	dc["cvec"].append( 3.0i);

	ASSERT_EQ(dc.nVars(), 2u);

	auto cvec = 1i*CVec{ 1,2,3 };
	auto cvec_out = dc.getCVec("cvec");
	ASSERT_EQ(cvec, cvec_out);
}

TEST_F(DataContainer_Fixture, appendVectors)
{
	DataContainer dc{};

	auto rvec = RVec{ 0,1,2 };
	dc["rmat"].append(rvec);
	dc["rmat"].append(rvec);

	ASSERT_EQ(dc.nVars(), 1u);

	auto rmat = RMat(3, 2, 0.0);
	rmat.col(0) = rvec;
	rmat.col(1) = rvec;

	auto rmat_out = dc.getRMat("rmat");
	ASSERT_EQ(rmat, rmat_out);

	auto cvec = 1i*CVec{ 0,1,2 };
	dc["cmat"].append(cvec);
	dc["cmat"].append(cvec);

	ASSERT_EQ(2u, dc.nVars());

	auto cmat = CMat(3, 2, 0.0);
	cmat.col(0) = cvec;
	cmat.col(1) = cvec;

	auto cmat_out = dc.getCMat("cmat");
	ASSERT_EQ(cmat, cmat_out);

}

TEST_F(DataContainer_Fixture, forwardedTypes)
{
	DataContainer dc{};

	const auto s = one_particle::makeHilbertSpace(-1, 1, 128);

	dc["x"]=s.x().vec();
	dc["x2"].append(s.x().vec());

	ASSERT_EQ(dc.getRVec("x"), s.x().vec());
	ASSERT_EQ(dc.getRVec("x2"), s.x().vec());

	const auto s_second = n_level::makeHilbertSpace(2);
	const auto state = makeState(s_second, CVec{ 1.0, 0.0 });

	dc["state"] =  state.vec();
	dc["state2"].append(state.vec());

	ASSERT_EQ(dc.getCVec("state"), state.vec());
	ASSERT_EQ(dc.getCVec("state2"), state.vec());

	const auto control = makeTimeControl(101, 0.01);

	dc["control"] =  control.mat();

	ASSERT_EQ(dc.getRMat("control"), control.mat());
}

TEST_F(DataContainer_Fixture, getExtension)
{
	using namespace internal::stringStuff;

	EXPECT_EQ("mat", getExtension("a.mat"));
	EXPECT_EQ("json", getExtension("cheese.json"));
	EXPECT_EQ("", getExtension("a"));

	EXPECT_EQ("mat", getExtension("/usr/au304582/a.mat"));
	EXPECT_EQ("mat", getExtension("/usr.cheese/au304582-Brie/piece.mat"));
	EXPECT_EQ("json", getExtension("/usr/au304582/a.json"));
	EXPECT_EQ("json", getExtension("/usr.cheese/au304582-Brie/piece.json"));
	EXPECT_EQ("", getExtension("/usr.cheese/au304582-Brie/piece"));

	EXPECT_EQ("mat", getExtension("C:\\Users\\au304582\\Documents\\a.mat"));
	EXPECT_EQ("mat", getExtension("C:\\Users\\au304582\\Documents.cheese\\Cheese-Brie\\piece.mat"));
	EXPECT_EQ("json", getExtension("C:\\Users\\au304582\\Documents\\a.json"));
	EXPECT_EQ("json", getExtension("C:\\Users\\au304582\\Documents.cheese\\Cheese-Brie\\piece.json"));
	EXPECT_EQ("", getExtension("C:\\Users\\au304582\\Documents.cheese\\Cheese-Brie\\piece"));
}

class DataContainer_SerializedObject_Fixture: public DataContainer_Fixture{};

TEST_F(DataContainer_SerializedObject_Fixture, setGetBasicTypes)
{
	// real

	static_assert(!internal::isSerializable_v<real>, "");
	auto r_in = 0.1;
	SerializedObject obj;
	obj = r_in;

	auto r_out = real(obj);
	auto r_out2 = obj.get<real>();

	EXPECT_EQ(r_in, r_out);
	EXPECT_EQ(r_in, r_out2);
	EXPECT_EQ(obj._type(), SerializedObject::REAL);
	EXPECT_EQ(obj._format(), SerializedObject::SCALAR);
	EXPECT_FALSE(mpark::holds_alternative<std::string>(obj._variant()));
	EXPECT_TRUE(mpark::holds_alternative<RMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<CMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<DataStruct>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<std::vector<SerializedObject>>(obj._variant()));


	//complex
	static_assert(!internal::isSerializable_v<complex>, "");
	using namespace std::complex_literals;
	auto c_in = 0.1i;
	obj = c_in;

	auto c_out = obj.get<complex>();

	EXPECT_EQ(c_in, c_out);
	EXPECT_EQ(obj._type(), SerializedObject::COMPLEX);
	EXPECT_EQ(obj._format(), SerializedObject::SCALAR);
	EXPECT_FALSE(mpark::holds_alternative<std::string>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<RMat>(obj._variant()));
	EXPECT_TRUE(mpark::holds_alternative<CMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<DataStruct>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<std::vector<SerializedObject>>(obj._variant()));

	// int (for fun)
	static_assert(!internal::isSerializable_v<int>, "");
	auto i_in = 1;
	obj = i_in;

	auto i_out = int(obj);
	auto i_out2 = obj.get<int>();

	EXPECT_EQ(i_in, i_out);
	EXPECT_EQ(i_in, i_out2);
	EXPECT_EQ(obj._type(), SerializedObject::REAL);
	EXPECT_EQ(obj._format(), SerializedObject::SCALAR);
	EXPECT_FALSE(mpark::holds_alternative<std::string>(obj._variant()));
	EXPECT_TRUE(mpark::holds_alternative<RMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<CMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<DataStruct>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<std::vector<SerializedObject>>(obj._variant()));

	// empty
	obj._clear();

	EXPECT_EQ(i_in, i_out);
	EXPECT_EQ(obj._type(), SerializedObject::EMPTY);
	EXPECT_EQ(obj._format(), SerializedObject::NA);
	EXPECT_FALSE(mpark::holds_alternative<std::string>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<RMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<CMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<DataStruct>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<std::vector<SerializedObject>>(obj._variant()));

	// rvec
	static_assert(internal::isSerializable_v<RVec>, "");
	auto rvec_in = RVec{ 0.1, 2.3 };
	obj = rvec_in;

	//auto rvec_out = RVec(obj); // does not work for some reason
	auto rvec_out2 = obj.get<RVec>();
	RVec rvec_out = obj;

	EXPECT_EQ(rvec_in, rvec_out);
	EXPECT_EQ(obj._type(), SerializedObject::REAL);
	EXPECT_EQ(obj._format(), SerializedObject::VECTOR);
	EXPECT_FALSE(mpark::holds_alternative<std::string>(obj._variant()));
	EXPECT_TRUE(mpark::holds_alternative<RMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<CMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<DataStruct>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<std::vector<SerializedObject>>(obj._variant()));

	// cvec
	static_assert(internal::isSerializable_v<CVec>, "");
	auto cvec_in = CVec{ 0.1, 2.3i };
	obj = cvec_in;

	CVec cvec_out = obj;
	auto cvec_out2 = obj.get<CVec>();

	EXPECT_EQ(cvec_in, cvec_out);
	EXPECT_EQ(obj._type(), SerializedObject::COMPLEX);
	EXPECT_EQ(obj._format(), SerializedObject::VECTOR);
	EXPECT_FALSE(mpark::holds_alternative<std::string>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<RMat>(obj._variant()));
	EXPECT_TRUE(mpark::holds_alternative<CMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<DataStruct>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<std::vector<SerializedObject>>(obj._variant()));

	// rmat
	static_assert(internal::isSerializable_v<RMat>, "");
	auto rmat_in = RMat{ {0.1, 2.3} };
	obj = rmat_in;

	RMat rmat_out = obj;
	auto rmat_out2 = obj.get<RMat>();

	EXPECT_EQ(rvec_in, rvec_out);
	EXPECT_EQ(obj._type(), SerializedObject::REAL);
	EXPECT_EQ(obj._format(), SerializedObject::MATRIX);
	EXPECT_FALSE(mpark::holds_alternative<std::string>(obj._variant()));
	EXPECT_TRUE(mpark::holds_alternative<RMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<CMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<DataStruct>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<std::vector<SerializedObject>>(obj._variant()));

	// cmat
	static_assert(internal::isSerializable_v<CMat>, "");
	auto cmat_in = CMat{ {0.1, 2.3i} };
	obj = cmat_in;

	CMat cmat_out = obj;
	auto cmat_out2 = obj.get<CMat>();

	EXPECT_EQ(cvec_in, cvec_out);
	EXPECT_EQ(obj._type(), SerializedObject::COMPLEX);
	EXPECT_EQ(obj._format(), SerializedObject::MATRIX);
	EXPECT_FALSE(mpark::holds_alternative<std::string>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<RMat>(obj._variant()));
	EXPECT_TRUE(mpark::holds_alternative<CMat>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<DataStruct>(obj._variant()));
	EXPECT_FALSE(mpark::holds_alternative<std::vector<SerializedObject>>(obj._variant()));
}


TEST_F(DataContainer_SerializedObject_Fixture, appendRealScalar)
{
	SerializedObject obj;

	auto ref = RVec{ 0.1,0.1 };
	obj = ref.at(0);
	obj.append(ref.at(1));

	RVec v = obj;
	EXPECT_EQ(v, ref);
}

﻿/* COPYRIGHT
 *
 * file="T_GPE_StaticHarmonic_Diag.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "F_GPE_StaticHarmonic.h"


using namespace qengine;


class T_GPE_StaticHarmonic_Diag_beta0 : public F_GPE_StaticHarmonic
{
public:
	const double tolerance = 1e-5;
	const real beta = 0.0;
	AUTO_MEMBER(H, T + V + makeGpeTerm(beta));
};

TEST_F(T_GPE_StaticHarmonic_Diag_beta0, OptimalDamping)
{
	auto spectrum = H.makeSpectrum(2, GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING);

	ASSERT_EQ(2u, spectrum.largestEigenstate());
	ASSERT_EQ(hilbertSpace.dim(), spectrum.eigenvector(0).size());

	for (auto i = 0u; i <= spectrum.largestEigenstate(); ++i)
	{
		// Analytuc
		ASSERT_NEAR((2 * i + 1)*omega / 2, spectrum.eigenvalue(i), tolerance) << "eigenvalue: " << i;

		// Numeric tests
		ASSERT_NEAR(norm(spectrum.eigenFunction(i)), 1, tolerance) << "eigenstate: " << i;
		ASSERT_NEAR(expectationValue(H, spectrum.eigenFunction(i)), spectrum.eigenvalue(i), tolerance) << "eigenvalue: " << i;
		ASSERT_NEAR(variance(H, spectrum.eigenFunction(i)), 0.0, tolerance);
	}
}


TEST_F(T_GPE_StaticHarmonic_Diag_beta0, Mixing)
{
	auto spectrum = H.makeSpectrum(2, GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING);

	ASSERT_EQ(2u, spectrum.largestEigenstate());
	ASSERT_EQ(hilbertSpace.dim(), spectrum.eigenvector(0).size());

	for (auto i = 0u; i <= spectrum.largestEigenstate(); ++i)
	{
		// Analytuc
		ASSERT_NEAR((2 * i + 1)*omega / 2, spectrum.eigenvalue(i), tolerance) << "eigenvalue: " << i;

		// Numeric tests
		ASSERT_NEAR(norm(spectrum.eigenFunction(i)), 1, tolerance) << "eigenstate: " << i;
		ASSERT_NEAR(expectationValue(H, spectrum.eigenFunction(i)), spectrum.eigenvalue(i), tolerance) << "eigenvalue: " << i;
		ASSERT_NEAR(variance(H, spectrum.eigenFunction(i)), 0.0, tolerance);
	}
}

class T_GPE_StaticHarmonic_Diag_beta10 : public F_GPE_StaticHarmonic
{
public:
	const double tolerance = 1e-5;
	const real beta = 10.0;
	AUTO_MEMBER(H, T + V + makeGpeTerm(beta));
};

TEST_F(T_GPE_StaticHarmonic_Diag_beta10, OptimalDamping_gs)
{
	auto spectrum = H.makeSpectrum(2, GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING);
	ASSERT_NEAR(variance(H, spectrum.eigenFunction(0)), 0, tolerance);
}

TEST_F(T_GPE_StaticHarmonic_Diag_beta10, Mixing_gs)
{
	auto spectrum = H.makeSpectrum(0, GROUNDSTATE_ALGORITHM::MIXING, 1e-8, 30000, { 0.001 });

	ASSERT_NEAR(variance(H, spectrum.eigenFunction(0)), 0, tolerance);
}


TEST_F(T_GPE_StaticHarmonic_Diag_beta10, OptimalDamping_1stExcited)
{
	auto spectrum = H.makeSpectrum(1, GROUNDSTATE_ALGORITHM::OPTIMAL_DAMPING, 1e-12, 40000, { 1, 3 * 1e-4 });
	auto psi = spectrum.eigenFunction(1);

	ASSERT_NEAR(expectationValue(H, psi), spectrum.eigenvalue(1), tolerance);
	ASSERT_NEAR(variance(H, psi), 0, tolerance);
}

TEST_F(T_GPE_StaticHarmonic_Diag_beta10, Mixing_1stExcited)
{
	auto spectrum = H.makeSpectrum(1, GROUNDSTATE_ALGORITHM::MIXING, 1e-12, 40000, { 1e-2, 3 * 1e-4 });
	auto psi = spectrum.eigenFunction(1);

	ASSERT_NEAR(expectationValue(H, psi), spectrum.eigenvalue(1), tolerance);
	ASSERT_NEAR(variance(H, psi), 0, tolerance);
}

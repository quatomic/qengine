﻿/* COPYRIGHT
 *
 * file="F_GPE_StaticHarmonic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <gtest/gtest.h>
#include "qengine/GPE.h"

#include "src/API/UserUtilities/AutoMember.h"

using namespace qengine;


class F_GPE_StaticHarmonic: public testing::Test
{
public:
	real x0 = 0.0;
	real omega = 20.0;
	count_t dim = 512u;
	real kinematicFactor = 0.5;

	AUTO_MEMBER(hilbertSpace, gpe::makeHilbertSpace(-2, 2, dim, kinematicFactor));
	AUTO_MEMBER(x, hilbertSpace.spatialDimension());
	AUTO_MEMBER(V, 0.5*pow(omega, 2)*pow((x - x0), 2));
	AUTO_MEMBER(T, hilbertSpace.T());

};


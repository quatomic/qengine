﻿/* COPYRIGHT
 *
 * file="T_Spatial_SpatialHilbertSpace.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "src/API/Common/Spatial/1D/ApiHilbertSpace.h"


using namespace qengine;


class T_Spatial_SpatialHilbertSpace : public testing::Test{};


TEST_F(T_Spatial_SpatialHilbertSpace, dimension)
{
    ASSERT_EQ(one_particle::makeHilbertSpace(-3, 3, 200).dim(), 200u);
}

TEST_F(T_Spatial_SpatialHilbertSpace, kinematicFactor)
{
    ASSERT_EQ(one_particle::makeHilbertSpace(-3, 3, 200).kinematicFactor(), 0.5);
    ASSERT_EQ(one_particle::makeHilbertSpace(-3, 3, 200, 0.7).kinematicFactor(), 0.7);

}


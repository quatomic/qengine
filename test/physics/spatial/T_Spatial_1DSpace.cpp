﻿/* COPYRIGHT
 *
 * file="T_Spatial_MultiDimensionalSpace.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "src/Abstract/Physics/Spatial/SerializedHilbertSpace1D.h"
#include "src/Abstract/Physics/General/isSerializedHilbertSpace.h"
#include "src/Abstract/Physics/Spatial/isSerializedSpatialHilbertSpace.h"

using namespace qengine;

class T_Spatial_1DSpace : public ::testing::Test {};

TEST_F(T_Spatial_1DSpace, traits)
{
	using namespace internal;

	using T = spatial::SerializedHilbertSpace1D<void>;

	//static_assert(internal::isSerializedSpatialHilbertSpace_v<T>, "");


	static_assert(internal::isEqualityComparable_v<T>, "");
	static_assert(internal::isInequalityComparable_v<T>, "");
	static_assert(internal::hasVar_dim_v<T, const count_t>, "");
	static_assert(internal::isSerializedHilbertSpace_v<T>, "");

	static_assert(internal::hasMethod_integrationConstant_v<const T, real()>, "");
	static_assert(internal::hasMethod_normalization_v<const T, real()>, "");
	static_assert(internal::hasMethod_dimensions_v<const T, std::vector<count_t>()>, "");
	static_assert(internal::isSerializedSpatialHilbertSpace_v<T>, "");
}

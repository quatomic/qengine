﻿/* COPYRIGHT
 *
 * file="F_1P_DrivenHarmonic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <qengine/OneParticle.h>
#include <functional>

#include "src/API/UserUtilities/AutoMember.h"

using namespace qengine;
using namespace std::complex_literals;


class F_1P_DrivenHarmonic
{
public:

    F_1P_DrivenHarmonic(const count_t dim = 1024u, const real  kinematicFactor = 0.5, const real  omega = 15.0):
		omega(omega),
		dim(dim),
		kinematicFactor(kinematicFactor)
    {}

    double omega;
    double a = 0.0;
	count_t dim;
	real kinematicFactor;

	AUTO_MEMBER(hilbertSpace, one_particle::makeHilbertSpace(-3.5, 3.5, dim, kinematicFactor));
	AUTO_MEMBER(x, hilbertSpace.spatialDimension());
	AUTO_MEMBER(T, hilbertSpace.T());
	AUTO_MEMBER(H_static, T + 0.5*pow(omega, 2)*pow(x, 2));

    void setMotion_InitialOffset_StaticPotential(const real offset) {
        a = offset;
        drivingForce = [=](double /*t*/){return a;};
        xc = [=](double t) {return  a*(1-cos(omega*t)); };
        xcDiff = [=](double t) {return a*omega*sin(omega*t); };
        Intx0xc = [=](double t) {return a*a*(t-sin(omega*t)/omega); };

    }

    void setMotion_NoOffset_OscillateWithAmplitude(const real  amplitude) {
        a = amplitude;
        drivingForce = [=](double t){return t*a;};
        xc = [=](double t) {return a*(t - sin(omega*t) / omega); };
        xcDiff = [=](double t) {return a*(1.0 - cos(omega*t)); };
        Intx0xc = [=](double t) {return a*a*(t*t*t / 3 - sin(omega*t) / (omega*omega*omega) + t*cos(omega*t) / (omega*omega)); };

    }

    void setMotion_Shakeup(const real drivingConstant) {
        a = drivingConstant;
        drivingForce = [=](double t){return a*sin(omega*t);};
        xc = [=](double t) {return a*0.5*(sin(omega*t) - omega*t*cos(omega*t)); };
        xcDiff = [=](double t) {return a*0.5*(omega*omega*t*std::sin(omega*t)); };
        Intx0xc = [=](double t) {return a*a*0.25/omega*(omega*t*(1 + 0.5*cos(2 * omega*t)) /*- 0.25 * 3 * std::sin(2 * omega*t)*/); };
    }

    auto analyticStaticEigenFunction(const count_t eigenIndex, const real  t){
        switch (eigenIndex) {
        case 0:
            return pow(omega / PI, 1 / 4.0)  *  exp(-omega / 2 * pow(x-xc(t), 2));
            break;
        case 1:
            return pow(omega / PI, 1 / 4.0)  *  sqrt(2*omega) * (x - xc(t)) * exp(-omega / 2 * pow(x-xc(t), 2));
            break;
        case 2:
            return pow(omega / PI, 1 / 4.0)*1/sqrt(2.0)  *  ( 2 * omega*pow(x - xc(t), 2)-1.0) * exp(-omega / 2 * pow(x-xc(t), 2));
            break;
        default:
            return 0.0*x;
            break;
        }
    }
    auto getDrivenHarmonicEigenstate(const count_t n)
    {
        return [this, n](double t)
        {
            auto a1 = -(n + 0.5)*omega*t   +   0*x;
            auto a2 = xcDiff(t)*(x - 0.5*xc(t));
            auto a3 = 0.5*omega*omega * Intx0xc(t)   +   0*x;

            return analyticStaticEigenFunction(n, t) * exp(1i*(a1+a2+a3));
        };
    }


    std::function<double (double)> drivingForce = [=](double t){return 0*t;};
    std::function<double (double)> xc = [=](double t){return 0*t;};
    std::function<double (double)> xcDiff = [=](double t){return 0*t;};
    std::function<double (double)> Intx0xc = [=](double t){return 0*t;};

};



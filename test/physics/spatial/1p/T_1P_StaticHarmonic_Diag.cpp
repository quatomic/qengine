﻿/* COPYRIGHT
 *
 * file="T_1P_StaticHarmonic_Diag.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "F_1P_StaticHarmonic.h"


using namespace qengine;
using namespace std::complex_literals;


class T_1P_StaticHarmonic_Diag : public testing::Test {

public:

    const double tolerance = 1e-5;
    F_1P_StaticHarmonic sh = F_1P_StaticHarmonic();

};

TEST_F(T_1P_StaticHarmonic_Diag, spectrum)
{

    ASSERT_EQ(2u, sh.spectrum.largestEigenstate());
    ASSERT_EQ(sh.hilbertSpace.dim(), sh.spectrum.eigenvector(0).size());

    for (auto i = 0u; i <= sh.spectrum.largestEigenstate(); ++i)
    {
        // Validate analytic eigenspectrum
        ASSERT_NEAR( variance(sh.H, sh.spectrum.eigenFunction(i)), 0, tolerance);
        ASSERT_NEAR( variance(sh.H, sh.analyticEigenFunction(i)), 0, tolerance);

        // Analytic tests
        ASSERT_NEAR(sh.spectrum.eigenvalue(i), sh.analyticEigenValue(i), tolerance) << "eigenvalue: " << i;
        ASSERT_NEAR(fidelity(sh.spectrum.eigenFunction(i), sh.analyticEigenFunction(i)), 1, tolerance);

        // Numeric tests
        ASSERT_NEAR(norm(sh.spectrum.eigenFunction(i)), 1, tolerance) << "eigenstate: " << i;
        ASSERT_NEAR(expectationValue(sh.H, sh.spectrum.eigenFunction(i)), sh.spectrum.eigenvalue(i), tolerance) << "eigenvalue: " << i;
        ASSERT_NEAR(variance(sh.H, sh.spectrum.eigenFunction(i)), 0.0, tolerance);

    }

}

TEST_F(T_1P_StaticHarmonic_Diag, nice_syntax)
{
	auto psi = makeWavefunction(sh.H[0]+2.0*1i*sh.H[1], NORMALIZE);

	auto spectrum = sh.H.makeSpectrum(1);
	auto psi2 = normalize(spectrum.eigenFunction(0) + 2.0*1i*spectrum.eigenFunction(1));


	EXPECT_NEAR(1.0, overlap(psi, psi2).real(), 1e-14);
	EXPECT_NEAR(0.0, overlap(psi, psi2).imag(), 1e-14);
}


TEST_F(T_1P_StaticHarmonic_Diag, kineticOperator)
{
    ASSERT_NEAR(sh.omega / 4, expectationValue(sh.T, sh.harm0), tolerance);
}

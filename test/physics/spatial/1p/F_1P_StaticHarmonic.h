﻿/* COPYRIGHT
 *
 * file="F_1P_StaticHarmonic.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <gtest/gtest.h>
#include <qengine/OneParticle.h>

#include "src/API/UserUtilities/AutoMember.h"

using namespace qengine;


class F_1P_StaticHarmonic
{
public:

    F_1P_StaticHarmonic(const count_t dim = 512u, const real kinematicFactor = 0.5, const real  x0 = 0.0, const real  omega = 10.0):
        x0(x0),
        omega(omega),
		dim(dim),
		kinematicFactor(kinematicFactor)
    {}

    real x0;
    real omega;
	count_t dim;
	real kinematicFactor;

	AUTO_MEMBER(hilbertSpace, one_particle::makeHilbertSpace(-3, 3, dim, kinematicFactor));
	AUTO_MEMBER(x, hilbertSpace.spatialDimension());
	AUTO_MEMBER(V, 0.5*pow(omega, 2)*pow((x - x0), 2));
	AUTO_MEMBER(T, hilbertSpace.T());
	AUTO_MEMBER(H, T + V);
	AUTO_MEMBER(spectrum, H.makeSpectrum(2));

	AUTO_MEMBER(harm0, spectrum.eigenFunction(0));
	AUTO_MEMBER(harm1, spectrum.eigenFunction(1));
	AUTO_MEMBER(harm2, spectrum.eigenFunction(2));

    auto analyticEigenFunction(const int eigenIndex){
        switch (eigenIndex) {
        case 0:
            return pow(omega / PI, 1 / 4.0)  *  exp(-omega / 2 * pow(x-x0, 2));
            break;
        case 1:
            return pow(omega / PI, 1 / 4.0)  *  sqrt(2*omega) * (x - x0) * exp(-omega / 2 * pow(x-x0, 2));
            break;
        case 2:
            return pow(omega / PI, 1 / 4.0)*1/sqrt(2.0)  *  ( 2 * omega*pow(x - x0, 2)-1.0) * exp(-omega / 2 * pow(x-x0, 2));
            break;
        default:
            return 0.0*x;
            break;
        }
    }


    double analyticEigenValue(const int eigenIndex) const
	{
        return (2 * eigenIndex + 1)*omega / 2;
    }

};


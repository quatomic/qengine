﻿/* COPYRIGHT
 *
 * file="T_1P_StaticHarmonic_TE.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/API/UserUtilities/AutoMember.h"

#include "F_1P_StaticHarmonic.h"

using namespace qengine;
using namespace std::complex_literals;



class T_1P_StaticHarmonic_TE : public testing::Test {

public:
	const double tolerance = 1e-5;
	const double defaultDt = 0.001;
	F_1P_StaticHarmonic sh = F_1P_StaticHarmonic();

};

TEST_F(T_1P_StaticHarmonic_TE, fixedTimeStepper_step)
{
	auto stepper = makeFixedTimeStepper(sh.H, sh.harm0, defaultDt);


	for (auto i = 1; i < 10; i++)
	{
		stepper.step();
		ASSERT_NEAR(0.0, variance(sh.H, stepper.state()), tolerance);
		ASSERT_NEAR(fidelity(stepper.state(), sh.harm0), 1, tolerance);
	}
}
//
//TEST_F(T_1P_StaticHarmonic_TE, fixedTimeStepper_step_krylov)
//{
//	auto stepper = makeFixedTimeStepper(krylov(sh.H, 5, dt), sh.harm0);
//
//	std::cout << 1 - stepper.state().norm() << std::endl;
//
//	for (auto i = 1; i < 10; i++)
//	{
//		stepper.step();
//		ASSERT_NEAR(0.0, variance(sh.H, stepper.state()), tolerance);
//		ASSERT_NEAR(fidelity(stepper.state(), sh.harm0), 1, tolerance);
//	}
//}

TEST_F(T_1P_StaticHarmonic_TE, fixedTimeStepper_cstep)
{
	auto stepper = makeFixedTimeStepper(sh.H, sh.harm0, defaultDt);

	for (auto i = 1; i < 10; i++)
	{
		stepper.cstep();
		ASSERT_NEAR(sh.analyticEigenValue(0), expectationValue(sh.H, stepper.state()), tolerance);
		ASSERT_NEAR(0.0, variance(sh.H, stepper.state()), tolerance);
		ASSERT_NEAR(fidelity(stepper.state(), sh.harm0), 1, tolerance);
	}
}



TEST_F(T_1P_StaticHarmonic_TE, timeStepper_step)
{
	auto stepper = makeTimeStepper(sh.H, sh.harm0);

	for (auto i = 1; i < 10; i++)
	{
		stepper.step(defaultDt);
		ASSERT_NEAR(sh.analyticEigenValue(0), expectationValue(sh.H, stepper.state()), tolerance);
		ASSERT_NEAR(0.0, variance(sh.H, stepper.state()), tolerance);
		ASSERT_NEAR(fidelity(stepper.state(), sh.harm0), 1, tolerance);
	}
}

TEST_F(T_1P_StaticHarmonic_TE, eigenstateRotation)
{
	ASSERT_EQ(2u, sh.spectrum.largestEigenstate());
	ASSERT_EQ(sh.hilbertSpace.dim(), sh.spectrum.eigenvector(0).size());

	for (auto j = 0u; j <= sh.spectrum.largestEigenstate(); j++)
	{

		auto psi = sh.spectrum.eigenFunction(j);
		auto E0 = sh.spectrum.eigenvalue(j);

		auto T = 2.0*PI / E0; // Phase period: exp(-i E0 T) = exp(-i 2*PI)
		auto dt = T * 0.001;

		auto propagator = makeFixedTimeStepper(sh.H, psi, dt);

		for (auto i = 1u; i <= 1000; i++) 
		{
			propagator.step();
		}

		ASSERT_NEAR(1.0, fidelity(psi, propagator.state()), tolerance) << "Eigenstate:" << j;
		EXPECT_NEAR(0.0, std::abs(exp(-1i * E0*T) - overlap(psi, propagator.state())), 4.2e-5) << "Eigenstate:" << j;
	}
}



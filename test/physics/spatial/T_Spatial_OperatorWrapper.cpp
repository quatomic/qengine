﻿/* COPYRIGHT
 *
 * file="T_Spatial_OperatorWrapper.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "qengine/OneParticle.h"
#include "src/Abstract/Physics/Spatial/OperatorWrapper.h"
#include "additionalAsserts.h"


using namespace qengine;


class T_Spatial_OperatorWrapper : public testing::Test{};



TEST_F(T_Spatial_OperatorWrapper, operatorFunctions)
{
	const auto s = one_particle::makeHilbertSpace(-1, 1, 128);
	const auto x = s.x();

	const auto omega = 5.0;

	const auto V = 0.5*omega*omega*x*x;
	const auto T = s.T();
	const auto H = T + V;
	const auto psi0 = makeWavefunction(H[0]);

	const auto xOp = spatial::makeOperatorWrapper(x);
	const auto VOp = spatial::makeOperatorWrapper(V);
	const auto TOp = spatial::makeOperatorWrapper(s, T._hamiltonianMatrix());
	const auto HOp = spatial::makeOperatorWrapper(s, H._hamiltonianMatrix());
	
	using Psi = std17::remove_cvref_t<decltype(psi0)>;
	using O = std17::remove_cvref_t<decltype(xOp)>;

	static_assert(internal::isCompatibleOperator_v<O, Psi>, "");

	EXPECT_EQ(expectationValue(x, psi0), expectationValue(xOp, psi0));
	EXPECT_EQ(expectationValue(V, psi0), expectationValue(VOp, psi0));
	EXPECT_EQ(expectationValue(T, psi0), expectationValue(TOp, psi0));
	EXPECT_EQ(expectationValue(H, psi0), expectationValue(HOp, psi0));

	EXPECT_EQ(variance(x, psi0), variance(xOp, psi0));
	EXPECT_EQ(variance(V, psi0), variance(VOp, psi0));
	EXPECT_EQ(variance(T, psi0), variance(TOp, psi0));
	EXPECT_EQ(variance(H, psi0), variance(HOp, psi0));

	EXPECT_EQ(standardDeviation(x, psi0), standardDeviation(xOp, psi0));
	EXPECT_EQ(standardDeviation(V, psi0), standardDeviation(VOp, psi0));
	EXPECT_EQ(standardDeviation(T, psi0), standardDeviation(TOp, psi0));
	EXPECT_EQ(standardDeviation(H, psi0), standardDeviation(HOp, psi0));


	using BaseType = spatial::IOperatorWrapper<decltype(s)::SerializedHilbertSpace>;
	const BaseType& xOpBase = xOp;
	const BaseType& VOpBase = VOp;
	const BaseType& TOpBase = TOp;
	const BaseType& HOpBase = HOp;

	using OBase = std17::remove_cvref_t<decltype(xOpBase)>;

	static_assert(internal::isCompatibleOperator_v<OBase, Psi>, "");

	EXPECT_EQ(expectationValue(x, psi0), expectationValue(xOpBase, psi0));
	EXPECT_EQ(expectationValue(V, psi0), expectationValue(VOpBase, psi0));
	EXPECT_EQ(expectationValue(T, psi0), expectationValue(TOpBase, psi0));
	EXPECT_EQ(expectationValue(H, psi0), expectationValue(HOpBase, psi0));

	EXPECT_EQ(variance(x, psi0), variance(xOpBase, psi0));
	EXPECT_EQ(variance(V, psi0), variance(VOpBase, psi0));
	EXPECT_EQ(variance(T, psi0), variance(TOpBase, psi0));
	EXPECT_EQ(variance(H, psi0), variance(HOpBase, psi0));

	EXPECT_EQ(standardDeviation(x, psi0), standardDeviation(xOpBase, psi0));
	EXPECT_EQ(standardDeviation(V, psi0), standardDeviation(VOpBase, psi0));
	EXPECT_EQ(standardDeviation(T, psi0), standardDeviation(TOpBase, psi0));
	EXPECT_EQ(standardDeviation(H, psi0), standardDeviation(HOpBase, psi0));
}


TEST_F(T_Spatial_OperatorWrapper, complex)
{
	using namespace std::complex_literals;

	const auto s = one_particle::makeHilbertSpace(-1, 1, 128);
	const auto x = s.x();

	const auto omega = 5.0;

	const auto V = 0.5*omega*omega*x*x;
	const auto T = s.T();
	const auto H = T + V;
	const auto psi0 = makeWavefunction(H[0]);

	const auto xOp = spatial::makeOperatorWrapper(1i*x);

	using Psi = std17::remove_cvref_t<decltype(psi0)>;
	using O = std17::remove_cvref_t<decltype(xOp)>;

	static_assert(internal::isCompatibleOperator_v<O, Psi>, "");

	EXPECT_EQ(1i*expectationValue(x, psi0), cexpectationValue(xOp, psi0));


	using BaseType = spatial::IOperatorWrapper<decltype(s)::SerializedHilbertSpace>;
	const BaseType& xOpBase = xOp;

	using OBase = std17::remove_cvref_t<decltype(xOpBase)>;

	static_assert(internal::isCompatibleOperator_v<OBase, Psi>, "");

	EXPECT_EQ(1i*expectationValue(x, psi0), cexpectationValue(xOpBase, psi0));
}

TEST_F(T_Spatial_OperatorWrapper, spectrum)
{
	const auto s = one_particle::makeHilbertSpace(-1, 1, 128);
	const auto x = s.x();

	const auto omega = 5.0;

	const auto V = 0.5*omega*omega*x*x;
	const auto T = s.T();
	const auto H = T + V;
	const auto psi0 = makeWavefunction(H[0]);

	const auto xOp = spatial::makeOperatorWrapper(x);
	const auto VOp = spatial::makeOperatorWrapper(V);
	const auto TOp = spatial::makeOperatorWrapper(s, T._hamiltonianMatrix());
	const auto HOp = spatial::makeOperatorWrapper(s, H._hamiltonianMatrix());
	
	const auto nStates = 15u;
	{
		const auto spec = HOp.makeSpectrum(nStates - 1);
		const auto refSpec = H.makeSpectrum(nStates - 1);

		for (auto i = 0u; i < nStates; ++i)
		{
			ASSERT_EQ(spec.eigenvalue(i), refSpec.eigenvalue(i));
			ASSERT_NEAR_V(spec.eigenFunction(i).vec(), refSpec.eigenFunction(i).vec(), 1e-16);
		}
	}

	using BaseType = spatial::IOperatorWrapper<decltype(s)::SerializedHilbertSpace>;
	const BaseType& HOpBase = HOp;

	{
		const auto spec = dynamic_cast<const spatial::OperatorWrapper<decltype(s)::SerializedHilbertSpace, internal::SqHermitianBandMat, real>&>(HOpBase).makeSpectrum(nStates - 1);
		const auto refSpec = H.makeSpectrum(nStates - 1);

		for (auto i = 0u; i < nStates; ++i)
		{
			ASSERT_EQ(spec.eigenvalue(i), refSpec.eigenvalue(i));
			ASSERT_NEAR_V(spec.eigenFunction(i).vec(), refSpec.eigenFunction(i).vec(), 1e-16);
		}
	}
}


﻿/* COPYRIGHT
 *
 * file="T_Spatial_SpatialBasics.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "1p/F_1P_StaticHarmonic.h"
#include "src/Abstract/Physics/Spatial/Basics.h"

using namespace qengine;


class T_Spatial_SpatialBasics : public testing::Test 
{

public:

    const double tolerance = 1e-7;
    F_1P_StaticHarmonic sh = F_1P_StaticHarmonic();
};

TEST_F(T_Spatial_SpatialBasics, fidelity)
{

    ASSERT_NEAR(fidelity(sh.harm0, sh.harm0), 1, tolerance);
    ASSERT_NEAR(fidelity(sh.harm0, sh.harm1), 0, tolerance);
    ASSERT_NEAR(fidelity(sh.harm0, sh.harm2), 0, tolerance);

    //test fidelity with real imaginary number
    auto coefficent = complex(1, 1);
    auto wave = (coefficent * sh.harm0 - 2 * sh.harm1) / sqrt(6);

    ASSERT_NEAR(fidelity(sh.harm0, wave), 1.0 / 3.0, tolerance);

}

TEST_F(T_Spatial_SpatialBasics, infidelity)
{
    ASSERT_NEAR(infidelity(sh.harm0, sh.harm0), 0, tolerance);
    ASSERT_NEAR(infidelity(sh.harm0, sh.harm1), 1, tolerance);
    ASSERT_NEAR(infidelity(sh.harm0, sh.harm2), 1, tolerance);
}


TEST_F(T_Spatial_SpatialBasics, overlap)
{
    ASSERT_NEAR(overlap(sh.harm0, sh.harm0).real(), 1, tolerance);
    ASSERT_NEAR(overlap(sh.harm0, sh.harm0).imag(), 0, tolerance);

    ASSERT_NEAR(overlap(sh.harm0, sh.harm1).real(), 0, tolerance);
    ASSERT_NEAR(overlap(sh.harm0, sh.harm1).imag(), 0, tolerance);

    ASSERT_NEAR(overlap(sh.harm0, sh.harm2).real(), 0, tolerance);
    ASSERT_NEAR(overlap(sh.harm0, sh.harm2).imag(), 0, tolerance);

    //test overlap with real imaginary number
    std::complex<double> coefficent(1, 1);
    auto wave = (coefficent * sh.harm0 - 2 * sh.harm1) / sqrt(6);

    ASSERT_NEAR(overlap(sh.harm0, wave).real(), 1.0 / sqrt(6.0), tolerance);
    ASSERT_NEAR(overlap(sh.harm0, wave).imag(), 1.0 / sqrt(6.0), tolerance);

    ////test overlap with itself
    ASSERT_NEAR(overlap(wave, wave).real(), 1.0, tolerance);
    ASSERT_NEAR(overlap(wave, wave).imag(), 0.0, tolerance);

}

TEST_F(T_Spatial_SpatialBasics, variance)
{
    for (auto i = 0u; i <= sh.spectrum.largestEigenstate(); ++i)
    {
        ASSERT_NEAR( variance(sh.H, sh.spectrum.eigenFunction(i)), 0, tolerance);
        ASSERT_NEAR( variance(sh.H, sh.analyticEigenFunction(i)), 0, tolerance);
    }
}

TEST_F(T_Spatial_SpatialBasics, standardDeviation)
{
    for (auto i = 0u; i <= sh.spectrum.largestEigenstate(); ++i)
    {
        ASSERT_NEAR( standardDeviation(sh.H, sh.spectrum.eigenFunction(i)), 0, sqrt(tolerance));
        ASSERT_NEAR( standardDeviation(sh.H, sh.analyticEigenFunction(i)), 0, sqrt(tolerance));
    }
}








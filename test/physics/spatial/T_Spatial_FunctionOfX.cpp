﻿/* COPYRIGHT
 *
 * file="T_Spatial_FunctionOfX.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "qengine/OneParticle.h"
#include "qengine/DataContainer.h"

using namespace qengine;


class T_Spatial_FunctionOfX : public ::testing::Test{};

// Trigonometry

TEST_F(T_Spatial_FunctionOfX, makeWavefunctionFromVectors)
{
	auto hilbertSpace = one_particle::makeHilbertSpace(-1, 1, 256);

	const auto rvec = hilbertSpace.normalization()*RVec(hilbertSpace.dim(), 1) / RVec(hilbertSpace.dim(), 1).norm();

	const auto cvec = CVec(rvec);

	const auto wfRef = FunctionOfX<complex, std::decay_t<decltype(hilbertSpace._serializedHilbertSpace())>>(cvec, hilbertSpace._serializedHilbertSpace());

	EXPECT_NEAR(1, fidelity(wfRef, makeComplexFunctionOfX(hilbertSpace, rvec)), 1e-14) << "copy real";
	EXPECT_NEAR(1, fidelity(wfRef, makeComplexFunctionOfX(hilbertSpace, cvec)), 1e-14) << " copy complex";

	EXPECT_NEAR(1, fidelity(wfRef, makeComplexFunctionOfX(hilbertSpace, RVec(rvec))), 1e-14) << "move real";
	EXPECT_NEAR(1, fidelity(wfRef, makeComplexFunctionOfX(hilbertSpace, CVec(cvec))), 1e-14) << "move complex";


	const auto rvec_wrongSize = hilbertSpace.normalization()*RVec(hilbertSpace.dim()/2, 1) / RVec(hilbertSpace.dim(), 1).norm();
	const auto cvec_wrongSize = CVec(rvec_wrongSize);

#if !QENGINE_DISABLE_DEBUG_CHECKS
	EXPECT_ANY_THROW(makeComplexFunctionOfX(hilbertSpace, rvec_wrongSize));
	EXPECT_ANY_THROW(makeComplexFunctionOfX(hilbertSpace, cvec_wrongSize));
#endif
}

TEST_F(T_Spatial_FunctionOfX, makeWavefunctionFromSerialized)
{
	auto hilbertSpace = one_particle::makeHilbertSpace(-1, 1, 256);

	const auto rvec = hilbertSpace.normalization()*RVec(hilbertSpace.dim(), 1) / RVec(hilbertSpace.dim(), 1).norm();

	const auto cvec = CVec(rvec);

	const auto wfRef = FunctionOfX<complex, std::decay_t<decltype(hilbertSpace._serializedHilbertSpace())>>(cvec, hilbertSpace._serializedHilbertSpace());

	DataContainer dc;
	dc["psiReal"] = rvec;
	dc["psiComplex"] = cvec;

	EXPECT_EQ(dc["psiReal"]._type(), SerializedObject::REAL);
	EXPECT_EQ(dc["psiReal"]._format(), SerializedObject::VECTOR);

	EXPECT_EQ(dc["psiComplex"]._type(), SerializedObject::COMPLEX);
	EXPECT_EQ(dc["psiComplex"]._format(), SerializedObject::VECTOR);

	EXPECT_NEAR(1, fidelity(wfRef, makeComplexFunctionOfX(hilbertSpace, dc["psiReal"])), 1e-14) << "serialized real";
	EXPECT_NEAR(1, fidelity(wfRef, makeComplexFunctionOfX(hilbertSpace, dc["psiComplex"])), 1e-14) << "serialized complex";

}

TEST_F(T_Spatial_FunctionOfX, makePotential)
{
	auto hilbertSpace = one_particle::makeHilbertSpace(-1, 1, 256);

	const auto rvec = hilbertSpace.normalization()*RVec(hilbertSpace.dim(), 1) / RVec(hilbertSpace.dim(), 1).norm();

	const auto VRef = FunctionOfX<real, std::decay_t<decltype(hilbertSpace._serializedHilbertSpace())>>(rvec, hilbertSpace._serializedHilbertSpace());

	DataContainer dc;
	dc["V"] = rvec;

	EXPECT_NEAR(1, fidelity(VRef, makeRealFunctionOfX(hilbertSpace, rvec)), 1e-14) << "copy";
	EXPECT_NEAR(1, fidelity(VRef, makeRealFunctionOfX(hilbertSpace, RVec(rvec))), 1e-14) << "move";

	const auto rvec_wrongSize = hilbertSpace.normalization()*RVec(hilbertSpace.dim() / 2, 1) / RVec(hilbertSpace.dim(), 1).norm();

#if !QENGINE_DISABLE_DEBUG_CHECKS
	EXPECT_ANY_THROW(makeRealFunctionOfX(hilbertSpace, rvec_wrongSize));
#endif

	EXPECT_EQ(dc["V"]._type(), SerializedObject::REAL);
	EXPECT_EQ(dc["V"]._format(), SerializedObject::VECTOR);

	EXPECT_NEAR(1, fidelity(VRef, makeRealFunctionOfX(hilbertSpace, dc["V"])), 1e-14) << "serialized";

}

TEST_F(T_Spatial_FunctionOfX, cos)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(cos(x)), 0.9397, 1e-3);
}

TEST_F(T_Spatial_FunctionOfX, sin)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(sin(x)), -0.1905, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, tan)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(tan(x)), -0.2224, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, acos)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(acos(x)), 1.7824, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, asin)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(asin(x)), -0.2116, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, atan)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(atan(x)), -0.1838, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, cosh)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(cosh(x)), 1.0631, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, sinh)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(sinh(x)), -0.2098, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, tanh)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(tanh(x)), -0.1829, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, acosh)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(1.1, 2.6, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(acosh(x)), 1.7549, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, asinh)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(asinh(x)), -0.1915, 0.1);
}

TEST_F(T_Spatial_FunctionOfX, atanh)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(atanh(x)), -0.2247, 0.1);
}



TEST_F(T_Spatial_FunctionOfX, sqrt)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(0.2, 10, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(sqrt(x)), 20.9723, 0.1);
}


// Arithmetic

TEST_F(T_Spatial_FunctionOfX, plusAndMuliply)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-3, 3, 100);
    auto x = hilbertSpace.spatialDimension();
    auto gauss = exp(-pow(x,2));

    ASSERT_NEAR( integrate(gauss+gauss), integrate(2*gauss), 1e-6);
}

TEST_F(T_Spatial_FunctionOfX, minusAndDivide)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-3, 3, 100);
    auto x = hilbertSpace.spatialDimension();
    auto gauss = exp(-pow(x,2));

    ASSERT_NEAR( integrate(3*gauss-gauss), integrate((8*gauss)/4), 1e-6);
}

// Misc

TEST_F(T_Spatial_FunctionOfX, normalize)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-3, 3, 100);
    auto x = hilbertSpace.spatialDimension();
    auto gauss = exp(-pow(x,2));
    gauss.normalize();
    ASSERT_NEAR( integrate(gauss.absSquare()), 1, 1e-6);
}

TEST_F(T_Spatial_FunctionOfX, integrate)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-0.7, 0.3, 1000);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_NEAR( integrate(cos(x)), 0.9397, 1e-3);
}

TEST_F(T_Spatial_FunctionOfX, dim)
{
    auto hilbertSpace = one_particle::makeHilbertSpace(-3, 3, 200);
    auto x = hilbertSpace.spatialDimension();
    ASSERT_EQ( x.vec().size(), 200u);
}


TEST_F(T_Spatial_FunctionOfX, dx)
{
	auto hilbertSpace = one_particle::makeHilbertSpace(-3, 3, 100);
	auto x = hilbertSpace.spatialDimension();
	auto xVec = x.vec();
	ASSERT_NEAR(xVec.at(1) - xVec.at(0), x._serializedHilbertSpace().dx(), 1e-4);
}

TEST_F(T_Spatial_FunctionOfX, serialization)
{
	auto hilbertSpace = one_particle::makeHilbertSpace(-3, 3, 128);
	auto x = hilbertSpace.spatialDimension();
	auto xVec = x.vec();

	DataContainer dc;

	dc["x"] = x.vec();
	//x = dc["x"];
	//auto x2 = makeFunctionOfX(hilbertSpace, dc["x"]);



}







﻿/* COPYRIGHT
 *
 * file="T_Spatial_PotentialSum.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include <qengine/TwoParticle.h>

#include "src/Utility/std17/as_const.h"

#include "additionalAsserts.h"

using namespace qengine;

class PotentialSum_Fixture : public ::testing::Test
{
public:
};

TEST_F(PotentialSum_Fixture, accessors)
{
	auto s = two_particle::makeHilbertSpace(-1, 1, 16);

	const auto omega = 40.0;
	const auto V = 0.5*omega*omega*pow(s.xPotential(), 2);

	const auto VInt = two_particle::makePointInteraction(0.0*V);


	auto potentialSum = makePotentialSum(V, VInt);

	// access by index
	EXPECT_NEAR_V(V.vec(), get<0>(potentialSum).vec(), 1e-16);
	EXPECT_NEAR_V(VInt.vec(), get<1>(potentialSum).vec(), 1e-16);

	//access const by index
	EXPECT_NEAR_V(V.vec(), get<0>(std17::as_const(potentialSum)).vec(), 1e-16);
	EXPECT_NEAR_V(VInt.vec(), get<1>(std17::as_const(potentialSum)).vec(), 1e-16);
}


TEST_F(PotentialSum_Fixture, addMore)
{
	auto s = two_particle::makeHilbertSpace(-1, 1, 16);

	const auto omega = 40.0;
	const auto V = 0.5*omega*omega*pow(s.xPotential(), 2);

	const auto VInt = two_particle::makePointInteraction(0.0*V);

	const auto potentialSum = makePotentialSum(V, VInt);

	auto potentialSum2 = potentialSum + V;

	EXPECT_NEAR_V(2*V.vec(), get<0>(potentialSum2).vec(), 1e-16);
	EXPECT_NEAR_V(VInt.vec(), get<1>(potentialSum2).vec(), 1e-16);

	auto potentialSum3 = potentialSum + VInt;

	EXPECT_NEAR_V(V.vec(), get<0>(potentialSum3).vec(), 1e-16);
	EXPECT_NEAR_V(2*VInt.vec(), get<1>(potentialSum3).vec(), 1e-16);
}

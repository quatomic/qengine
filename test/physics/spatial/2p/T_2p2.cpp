﻿/* COPYRIGHT
 *
 * file="T_2p2.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "qengine/TwoParticle.h"

#include "additionalAsserts.h"

using namespace qengine;

class T_2P_HamiltonianArithmetic : public testing::Test {};

TEST_F(T_2P_HamiltonianArithmetic, PotentialWithInteraction)
{
	auto s = two_particle::makeHilbertSpace(-1, 1, 16); //tiny hilbertSpace gives speed and we do not actually check any physics

	const auto omega = 40.0;
	const auto V = 0.5*omega*omega*pow(s.xPotential(), 2);

	const auto VInt = two_particle::makePointInteraction(V);

	const auto VwithInteraction = V + VInt;

	EXPECT_NEAR_V(V.vec(), get<0>(VwithInteraction).vec(), 1e-16);
	EXPECT_NEAR_V(VInt.vec(), get<1>(VwithInteraction).vec(), 1e-16);

	const auto VwithInteraction2 = VInt + V;

	EXPECT_NEAR_V(V.vec(), get<0>(VwithInteraction2).vec(), 1e-16);
	EXPECT_NEAR_V(VInt.vec(), get<1>(VwithInteraction2).vec(), 1e-16);

	const auto H = s.T() + VwithInteraction;
	const auto Href = s.T() + V + VInt;

	EXPECT_NEAR_SPM(H._hamiltonianMatrix().mat(), Href._hamiltonianMatrix().mat(), 1e-16);
	EXPECT_NEAR_V(Href._interactionPotential().vec(), H._interactionPotential().vec(), 1e-16);
}

TEST_F(T_2P_HamiltonianArithmetic, DynamicPotentialWithInteraction)
{
	using two_particle::Potential;
	using SerializedHilbertSpace = internal::extract_serialized_hilbertspace_t<Potential<real>>;
	static_assert(std::is_same<SerializedHilbertSpace, spatial::SerializedHilbertSpaceND<2, 1, 2>>::value, "");

	using Psi = FunctionOfX<real, SerializedHilbertSpace>;
	using PsiC = FunctionOfX<complex, SerializedHilbertSpace>;

	static_assert(spatial::isPotential_v<Potential<real>>, "");
	static_assert(spatial::isLabelledAs_Potential_v<Potential<real>>, "");
	static_assert(internal::isCompatibleOperator_v<Potential<real>, Psi>, "");
	static_assert(internal::isLabelledAs_Operator_v<Potential<real>>, "");
	static_assert(internal::isState_v<Psi, SerializedHilbertSpace>, "");
	static_assert(std::is_same<Psi, decltype(std::declval<Potential<real>>() * std::declval<Psi>())>::value, "");
	static_assert(internal::isState_v<decltype(std::declval<Potential<real>>() * std::declval<Psi>()), internal::extract_serialized_hilbertspace_t<Psi>>, "");

	static_assert(internal::isCompatibleOperator_v<Potential<real>, PsiC>, "");
	static_assert(std::is_same<PsiC, decltype(std::declval<Potential<real>>() * std::declval<PsiC>())>::value, "");

	using two_particle::PointInteraction;
	static_assert(spatial::isPotential_v<PointInteraction<real>>, "");
	static_assert(spatial::isLabelledAs_Potential_v<PointInteraction<real>>, "");
	static_assert(internal::isCompatibleOperator_v<PointInteraction<real>, Psi>, "");
	static_assert(internal::isLabelledAs_Operator_v<PointInteraction<real>>, "");
	static_assert(internal::isState_v<Psi, SerializedHilbertSpace>, "");
	static_assert(std::is_same<Psi, decltype(std::declval<PointInteraction<real>>() * std::declval<Psi>())>::value, "");
	static_assert(internal::isState_v<decltype(std::declval<PointInteraction<real>>() * std::declval<Psi>()), internal::extract_serialized_hilbertspace_t<Psi>>, "");

	//static_assert(internal::isCompatibleOperator_v<Potential<real>, PsiC>, "");

	static_assert(repl17::check_all_true(spatial::isPotential_v<Potential<real>>, spatial::isPotential_v<PointInteraction<real>>), "");
	static_assert(std::is_same<void, std::enable_if_t<repl17::check_all_true(spatial::isPotential_v<Potential<real>>, spatial::isPotential_v<PointInteraction<real>>)>>::value, "");

	using V_T = PotentialSum<Potential<real>, PointInteraction<real>>;
	static_assert(spatial::isLabelledAs_Potential_v<V_T>, "");
	static_assert(internal::isLabelledAs_Operator_v<V_T>, "");
	static_assert(internal::isCompatibleOperator_v<V_T, Psi>, "");
	static_assert(internal::isCompatibleOperator_v<V_T, PsiC>, "");
	static_assert(spatial::isPotential_v<V_T>, "");

	auto s = two_particle::makeHilbertSpace(-1, 1, 16); //tiny hilbertSpace gives speed and we do not actually check any physics

	const auto omega = 40.0;
	const auto V = 0.5*omega*omega*pow(s.xPotential(), 2);

	const auto Vdyn = makePotentialFunction([V](const auto t)
	{
		return V + two_particle::makePointInteraction(t*V);
	}, 0.0);

	EXPECT_NEAR_V(V.vec(), get<0>(Vdyn(0.0)).vec(), 1e-16);
	EXPECT_NEAR_V(0.0*V.vec(), get<1>(Vdyn(0.0)).vec(), 1e-16);


	EXPECT_NEAR_V(V.vec(), get<0>(Vdyn(1.0)).vec(), 1e-16);
	EXPECT_NEAR_V(V.vec()/s.dx(), get<1>(Vdyn(1.0)).vec(), 1e-16);


	const auto H = s.T() + Vdyn;
	const auto Href = s.T() + V;

	EXPECT_NEAR_SPM(Href._hamiltonianMatrix().mat(), H(0)._hamiltonianMatrix().mat(), 1e-16);
}

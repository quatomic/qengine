﻿/* COPYRIGHT
 *
 * file="T_2p.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "qengine/TwoParticle.h"
#include "qengine/OneParticle.h"
#include "additionalAsserts.h"
#include "src/Abstract/Physics/SecondQuantization/KrylovStepping.impl.h"

using namespace qengine;
using namespace std::complex_literals;


class T_2P_HilbertSpace : public testing::Test
{
};

TEST_F(T_2P_HilbertSpace, create)
{
	auto s = two_particle::makeHilbertSpace(-1, 1, 128);

	ASSERT_EQ(-1.0, s.xLower());
	ASSERT_EQ(1.0, s.xUpper());
	ASSERT_EQ(128u, s.dim());
	ASSERT_EQ(0.5, s.kinematicFactor());
	ASSERT_EQ(2.0 / (128 - 1), s.dx());
}


TEST_F(T_2P_HilbertSpace, compare)
{
	auto s1 = two_particle::makeHilbertSpace(-1, 1, 128);
	auto s2 = two_particle::makeHilbertSpace(-1, 1, 128);
	auto s3 = two_particle::makeHilbertSpace(-2, 2, 256);

	ASSERT_EQ(s1, s2);
	ASSERT_NE(s1, s3);
	ASSERT_NE(s2, s3);
}

class T_2P : public testing::Test
{
public:
	const real tol = 1e-10;
};

TEST_F(T_2P, interaction)
{
	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);

	const auto omega = 40.0;

	const auto V = 0.5*omega*omega*pow(s.xPotential(), 2);

	const auto VInt = two_particle::makePointInteraction(0.0*V);

	EXPECT_NEAR_V(RVec(s.dim(), 0.0), VInt.vec(), 1e-16);

	EXPECT_NEAR_V(CVec(s.dim(), 0.0), (-1i*0.5*0.001*VInt).vec(), 1e-16);

	EXPECT_NEAR_V(CVec(s.dim(), 1.0), exp(-1i*0.5*0.001*VInt).vec(), 1e-16);
}

TEST_F(T_2P, HamiltonianExpectationValueGroundState)
{
	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);

	const auto omega = 20.0;
	const auto H = s.T() + 0.5*omega*omega*pow(s.xPotential(), 2);

	const auto spectrum = H.makeSpectrum(2);

	EXPECT_NEAR(omega, spectrum.eigenvalue(0), 2e-5);
	EXPECT_NEAR(omega, expectationValue(H, spectrum.eigenFunction(0)), 2e-5);
}


TEST_F(T_2P, TE_static_fixed)
{
	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);

	const auto omega = 40.0;

	const auto V = [omega](auto x) {return 0.5*omega*omega*pow(x, 2); };


	const auto T1 = s.T1();
	const auto V1 = V(s.x1());
	const auto H1 = T1 + V1;
	const auto psi0_1 = makeWavefunction(H1[0], NORMALIZE);

	const auto T2 = s.T2();
	const auto V2 = V(s.x2());
	const auto H2 = T2 + V2;
	const auto psi0_2 = makeWavefunction(H2[0], NORMALIZE);

	const auto H_2P = s.T() + V(s.xPotential());
	const auto psi0_2P = makeWavefunction(H_2P[0], NORMALIZE, 2);

	const auto dt = 0.001;

	auto stepper = makeFixedTimeStepper(H_2P, psi0_2P, dt);
	auto stepper1 = makeFixedTimeStepper(H1, psi0_1, dt);
	auto stepper2 = makeFixedTimeStepper(H2, psi0_2, dt);

	ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << 0;
	ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 1e-13) << 0;
	ASSERT_NEAR(0, 1 - fidelity(stepper.state(), psi0_2P), 5e-8) << 0;

	for (auto i = 1u; i < 100; ++i)
	{
		stepper1.cstep();
		stepper2.cstep();

		stepper.cstep();

		ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << i;
		ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 2e-13) << i;
		ASSERT_NEAR(0, 1 - fidelity(stepper.state(), psi0_2P), 5e-8) << i;
	}
}
//
//TEST_F(T_2P, TE_static_fixed_krylov)
//{
//	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);
//
//	const auto omega = 40.0;
//
//	const auto V = [omega](auto x) {return 0.5*omega*omega*pow(x, 2); };
//
//
//	const auto T1 = s.T1();
//	const auto V1 = V(s.x1());
//	const auto H1 = T1 + V1;
//	const auto psi0_1 = makeWavefunction(H1[0], NORMALIZE);
//
//	const auto T2 = s.T2();
//	const auto V2 = V(s.x2());
//	const auto H2 = T2 + V2;
//	const auto psi0_2 = makeWavefunction(H2[0], NORMALIZE);
//
//	const auto H_2P = s.T() + V(s.xPotential());
//	const auto psi0_2P = makeWavefunction(H_2P[0], NORMALIZE, 2);
//
//	const auto dt = 0.001;
//
//	auto stepper = makeFixedTimeStepper(krylov(H_2P, 5, dt), psi0_2P);
//	auto stepper1 = makeFixedTimeStepper(H1, psi0_1, dt);
//	auto stepper2 = makeFixedTimeStepper(H2, psi0_2, dt);
//
//	ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << 0;
//	ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 1e-13) << 0;
//	ASSERT_NEAR(0, 1 - fidelity(stepper.state(), psi0_2P), 5e-8) << 0;
//
//	for (auto i = 1u; i < 100; ++i)
//	{
//		stepper1.cstep();
//		stepper2.cstep();
//
//		stepper.cstep();
//
//		ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << i;
//		ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 2e-13) << i;
//		ASSERT_NEAR(0, 1 - fidelity(stepper.state(), psi0_2P), 5e-8) << i;
//	}
//}

TEST_F(T_2P, TE_static_notFixed)
{
	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);

	const auto omega = 40.0;

	const auto V = [omega](auto x) {return 0.5*omega*omega*pow(x, 2); };


	const auto T1 = s.T1();
	const auto V1 = V(s.x1());
	const auto H1 = T1 + V1;
	const auto psi0_1 = makeWavefunction(H1[0], NORMALIZE);

	const auto T2 = s.T2();
	const auto V2 = V(s.x2());
	const auto H2 = T2 + V2;
	const auto psi0_2 = makeWavefunction(H2[0], NORMALIZE);

	const auto H_2P = s.T() + V(s.xPotential());
	const auto psi0_2P = makeWavefunction(H_2P[0], NORMALIZE, 2);

	const auto dt = 0.001;

	auto stepper = makeTimeStepper(H_2P, psi0_2P);
	auto stepper1 = makeTimeStepper(H1, psi0_1);
	auto stepper2 = makeTimeStepper(H2, psi0_2);

	ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << 0;
	ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 1e-13) << 0;
	ASSERT_NEAR(0, 1 - fidelity(stepper.state(), psi0_2P), 5e-8) << 0;

	for (auto i = 1u; i < 100; ++i)
	{
		stepper1.cstep(dt);
		stepper2.cstep(dt);

		stepper.cstep(dt);

		ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << i;
		ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 2e-13) << i;
		ASSERT_NEAR(0, 1 - fidelity(stepper.state(), psi0_2P), 5e-8) << i;
	}
}

TEST_F(T_2P, TE_dynamic_fixed)
{
	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);

	const auto omega = 40.0;

	const auto V = [omega](auto x) {return 0.5*omega*omega*pow(x, 2); };

	const auto t0 = 0.0;
	const auto makeDynV = [V, t0](auto x)
	{
		return makePotentialFunction([x, V, t0](real t) {return V(x - 0.1*sin(t)); }, t0);
	};

	const auto T1 = s.T1();
	const auto V1 = makeDynV(s.x1());
	const auto H1 = T1 + V1;
	const auto psi0_1 = makeWavefunction(H1(t0)[0], NORMALIZE);

	const auto T2 = s.T2();
	const auto V2 = makeDynV(s.x2());
	const auto H2 = T2 + V2;
	const auto psi0_2 = makeWavefunction(H2(t0)[0], NORMALIZE);

	const auto V_2P = makeDynV(s.xPotential());
	const auto H_2P = s.T() + V_2P;
	const auto psi0_2P = makeWavefunction(H_2P(t0)[0], NORMALIZE, 2);

	const auto dt = 0.001;

	auto stepper = makeFixedTimeStepper(H_2P, psi0_2P, dt);
	auto stepper1 = makeFixedTimeStepper(H1, psi0_1, dt);
	auto stepper2 = makeFixedTimeStepper(H2, psi0_2, dt);
	
	ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << 0;
	ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 1e-13) << 0;

	for (auto i = 1; i < 100; ++i)
	{
		stepper1.step(i*dt);
		stepper2.step( i*dt );

		stepper.step(i*dt);

		ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << i;
		ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 2e-13) << i;
	}
}

TEST_F(T_2P, TE_dynamic_notFixed)
{
	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);

	const auto omega = 40.0;

	const auto V = [omega](auto x) {return 0.5*omega*omega*pow(x, 2); };

	const auto t0 = 0.0;
	const auto makeDynV = [V, t0](auto x)
	{
		return makePotentialFunction([x, V, t0](real t) {return V(x - 0.1*sin(t)); }, t0);
	};

	const auto T1 = s.T1();
	const auto V1 = makeDynV(s.x1());
	const auto H1 = T1 + V1;
	const auto psi0_1 = makeWavefunction(H1(t0)[0], NORMALIZE);

	const auto T2 = s.T2();
	const auto V2 = makeDynV(s.x2());
	const auto H2 = T2 + V2;
	const auto psi0_2 = makeWavefunction(H2(t0)[0], NORMALIZE);

	const auto V_2P = makeDynV(s.xPotential());
	const auto H_2P = s.T() + V_2P;
	const auto psi0_2P = makeWavefunction(H_2P(t0)[0], NORMALIZE, 2);

	const auto dt = 0.001;

	auto stepper = makeTimeStepper(H_2P, psi0_2P);
	auto stepper1 = makeTimeStepper(H1, psi0_1);
	auto stepper2 = makeTimeStepper(H2, psi0_2);

	ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << 0;
	ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 1e-13) << 0;

	for (auto i = 1; i < 100; ++i)
	{
		stepper1.step(dt, i*dt);
		stepper2.step(dt, i*dt);

		stepper.step(dt, i*dt);

		ASSERT_NEAR(1, stepper.state().norm(), 1e-13) << i;
		ASSERT_NEAR(0, 1 - fidelity(stepper.state(), stepper1.state()*stepper2.state()), 2e-13) << i;
	}
}


class T_2P_Hamiltonian : public testing::Test
{
public:
	const real tol = 1e-10;
};

TEST_F(T_2P_Hamiltonian, plusPotential)
{
	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);

	const auto omega = 40.0;
	const auto V = [omega](auto x) {return 0.5*omega*omega*pow(x, 2); };

	auto VC = V(s.xPotential());

	auto H = s.T() + VC;

	// prepare vectors for comparison
	auto VCvec = VC.vec();
	auto ones = 0.0*VCvec + 1.0;

	auto H_ref = s.T()._hamiltonianMatrix() + internal::SqDiagMat<real>(RVec(kron(VCvec, ones)+kron(ones, VCvec)));
	EXPECT_NEAR_M(H_ref.mat(), H._hamiltonianMatrix().mat(), tol);


}

TEST_F(T_2P_Hamiltonian, plusPotentialAndInteraction)
{
	const auto s = two_particle::makeHilbertSpace(-1, 1, 128, 0.5);

	const auto omega = 40.0;
	const auto V = [omega](auto x) -> decltype(x) {return 0.5*omega*omega*pow(x, 2); };

	auto VC = V(s.xPotential());
	auto VInt = two_particle::makePointInteraction(VC);

	auto H = s.T() + VC + VInt;

	// prepare vectors for comparison
	auto VCvec = VC.vec();
	auto ones = 0.0*VCvec + 1.0;

	const auto size1D = VC._serializedHilbertSpace().dim;
	auto interactionVector = RVec(size1D*size1D, 0.0);
	for (auto i = 0u; i< size1D; ++i)
	{
		interactionVector.at(i*(size1D + 1)) = VInt.vec().at(i);
	}

	auto H_ref = s.T()._hamiltonianMatrix() + internal::SqDiagMat<real>(RVec(kron(VCvec, ones)+kron(ones, VCvec))) + internal::SqDiagMat<real>(interactionVector);
	EXPECT_NEAR_M(H_ref.mat(), H._hamiltonianMatrix().mat(), tol);

	auto H2 = s.T() + (VC + VInt);
	EXPECT_NEAR_M(H_ref.mat(), H2._hamiltonianMatrix().mat(), tol);
}

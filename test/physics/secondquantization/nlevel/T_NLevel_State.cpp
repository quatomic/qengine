﻿/* COPYRIGHT
 *
 * file="T_NLevel_State.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "qengine/NLevel.h"

using namespace qengine;
using namespace second_quantized;
using namespace fock;
using namespace std::complex_literals;

class T_NLevel_State : public ::testing::Test{
public:
	n_level::ApiHilbertSpace hilbertSpace = n_level::makeHilbertSpace(2);
	State<internal::SerializedNLevelHilbertSpace> gs = makeState(hilbertSpace, fock::state{ 0 });
};

TEST_F(T_NLevel_State, arithmetic)
{
    auto multiplied = gs * 4;
    auto divided = multiplied / 2;
    auto added = gs + gs + gs;
    auto substracted = added - gs;

    ASSERT_NEAR( fidelity(divided, substracted), 16, 1e-6);
}

TEST_F(T_NLevel_State, normalize)
{
    auto multiplied = gs * 4;
    multiplied.normalize();
    ASSERT_NEAR( fidelity(gs, multiplied), 1, 1e-6);
}

TEST_F(T_NLevel_State, createFromAbstractState)
{
	const auto abstractState = std::sqrt(2)*fock::state{ 0 }+1i*fock::state{ 1 };

	const auto state = makeState(hilbertSpace, abstractState);
	const auto stateDirect = makeState(hilbertSpace, { std::sqrt(2), 1i });

	ASSERT_EQ(state, stateDirect);
}

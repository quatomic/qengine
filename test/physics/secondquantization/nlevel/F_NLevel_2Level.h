﻿/* COPYRIGHT
 *
 * file="F_NLevel_2Level.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "qengine/NLevel.h"

using namespace qengine;
using namespace second_quantized;

class F_NLevel_2Level
{
public:

    F_NLevel_2Level(const real  E1, const real  E2, const real  omega, const real  gamma):
        omega(omega),
        gamma(gamma),
        E1(E1),
        E2(E2),

        hilbertSpace(n_level::makeHilbertSpace(2u)),
		H0(makeOperator(hilbertSpace, { {E1, 0},
										{0, E2} })),

        // Analytic probability
        g2hbar2(pow(gamma,2)/pow(hbar,2)),
        omega21((E2-E1)/hbar),
        omegaDiff(pow(omega-omega21,2) / 4)
    {}

    double omega;
    double gamma;
    double E1;
    double E2;
    n_level::ApiHilbertSpace hilbertSpace;
    Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace> H0;

    double g2hbar2;
    double omega21;
    double omegaDiff;

    static constexpr double hbar = 1; // J*s

    double analyticProbabilityOfBeingInSecondLevel(const real  t) const {
        return (g2hbar2/(g2hbar2+omegaDiff)) * pow(sin(sqrt(g2hbar2+omegaDiff) * t),2);
    }

};



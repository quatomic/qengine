﻿/* COPYRIGHT
 *
 * file="T_NLevel_2Level_Diag.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "F_NLevel_2Level.h"

using namespace qengine;
using namespace second_quantized;
using namespace fock;

class T_NLevel_2Level_Diag : public testing::Test {

public:
    const double tolerance = 1e-7;
    RVec E = RVec{1, 5};
    F_NLevel_2Level twolevel = F_NLevel_2Level(E.at(0), E.at(1), 4/F_NLevel_2Level::hbar, F_NLevel_2Level::hbar);
};


TEST_F(T_NLevel_2Level_Diag, spectrum)
{
    ASSERT_EQ(twolevel.hilbertSpace.nLevels(), 2u);
    ASSERT_EQ(twolevel.H0._serializedHilbertSpace().nLevels, 2u);

    for (auto i = 0u; i < twolevel.hilbertSpace.nLevels(); ++i)
    {
		auto state = makeState(twolevel.hilbertSpace, fock::state{ i });

        // Analytical tests
        ASSERT_NEAR(expectationValue(twolevel.H0, state), E.at(i), tolerance);

        // Numeric tests
        ASSERT_NEAR(norm(state), 1, tolerance) << "eigenstate: " << i;
        ASSERT_NEAR(variance(twolevel.H0, state), 0.0, tolerance);
    }
}






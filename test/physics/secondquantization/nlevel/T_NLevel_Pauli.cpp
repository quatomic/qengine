﻿/* COPYRIGHT
 *
 * file="T_NLevel_Pauli.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <armadillo>

#include "qengine/NLevel.h"

#include "src/API/UserUtilities/AutoMember.h"


#include "additionalAsserts.h"
using namespace qengine;
using namespace second_quantized;
using namespace internal;
using namespace std::complex_literals;

class T_SQ_Pauli_Fixture : public ::testing::Test
{
public:
	AUTO_MEMBER(I, n_level::makePauliI());
	AUTO_MEMBER(X, n_level::makePauliX());
	AUTO_MEMBER(Y, n_level::makePauliY());
	AUTO_MEMBER(Z, n_level::makePauliZ());

};

TEST_F(T_SQ_Pauli_Fixture, PauliMatrices_X2_I)
{

	// X^2 = I 
	ASSERT_EQ(I.at(0, 0), (X*X).at(0, 0)); ASSERT_EQ(I.at(0, 1), (X*X).at(0, 1));
	ASSERT_EQ(I.at(1, 0), (X*X).at(1, 0)); ASSERT_EQ(I.at(1, 1), (X*X).at(1, 1));
}

TEST_F(T_SQ_Pauli_Fixture, PauliMatrices_Y2_I)
{
	// Y^2 = I
	ASSERT_EQ(I.at(0, 0), (Y*Y).at(0, 0)); ASSERT_EQ(I.at(0, 1), (Y*Y).at(0, 1));
	ASSERT_EQ(I.at(1, 0), (Y*Y).at(1, 0)); ASSERT_EQ(I.at(1, 1), (Y*Y).at(1, 1));
}

TEST_F(T_SQ_Pauli_Fixture, PauliMatrices_Z2_I)
{
	// Z^2 = I
	ASSERT_EQ(I.at(0, 0), (Z*Z).at(0, 0)); ASSERT_EQ(I.at(0, 1), (Z*Z).at(0, 1));
	ASSERT_EQ(I.at(1, 0), (Z*Z).at(1, 0)); ASSERT_EQ(I.at(1, 1), (Z*Z).at(1, 1));
}

TEST_F(T_SQ_Pauli_Fixture, PauliMatrices_iXYZ_I)
{
	ASSERT_EQ(I, -1i * X*Y*Z);
}

TEST_F(T_SQ_Pauli_Fixture, RealExpectationValues)
{
	const auto s = n_level::makeHilbertSpace(2);

	for (auto i = 0u; i < 100; ++i)
	{
		auto x = makeState(s, CVec{ arma::cx_vec(arma::randu(2, 1), arma::randu(2, 1)) });
		x.normalize();

		ASSERT_NEAR(0.0, cexpectationValue(I, x).imag(), 1e-10);
		ASSERT_NEAR(0.0, cexpectationValue(X, x).imag(), 1e-10);
		ASSERT_NEAR(0.0, cexpectationValue(Y, x).imag(), 1e-10);
		ASSERT_NEAR(0.0, cexpectationValue(Z, x).imag(), 1e-10);
	}
}


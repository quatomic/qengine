﻿/* COPYRIGHT
 *
 * file="T_NLevel_Basics.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "qengine/NLevel.h"

using namespace qengine;
using namespace second_quantized;
using namespace fock;

class T_NLevel_Basics : public ::testing::Test
{
public:
	n_level::ApiHilbertSpace hilbertSpace = n_level::makeHilbertSpace(3);
	State<internal::SerializedNLevelHilbertSpace> state0 = makeState(hilbertSpace, fock::state{ 0 });
	State<internal::SerializedNLevelHilbertSpace> state1 = makeState(hilbertSpace, fock::state{ 1 });
	State<internal::SerializedNLevelHilbertSpace> state2 = makeState(hilbertSpace, fock::state{ 2 });
	double tolerance = 1e-5;

	RVec E = RVec{ 1.1, 2.2, 3.3 };
	Operator<internal::SqDenseMat, complex, internal::SerializedNLevelHilbertSpace> H0 =
		makeOperator(hilbertSpace, { {E.at(0), 0,       0},
			{0,       E.at(1), 0},
			{0,       0,       E.at(2) } });
};


TEST_F(T_NLevel_Basics, fidelity)
{
	ASSERT_NEAR(fidelity(state0, state0), 1, tolerance);
	ASSERT_NEAR(fidelity(state0, state1), 0, tolerance);
	ASSERT_NEAR(fidelity(state0, state2), 0, tolerance);

	//test fidelity with real imaginary number
	auto coefficent = complex(1, 1);
	auto wave = (coefficent * state0 - 2 * state1) / sqrt(6);

	ASSERT_NEAR(fidelity(state0, wave), 1.0 / 3.0, tolerance);

}

TEST_F(T_NLevel_Basics, infidelity)
{
	ASSERT_NEAR(infidelity(state0, state0), 0, tolerance);
	ASSERT_NEAR(infidelity(state0, state1), 1, tolerance);
	ASSERT_NEAR(infidelity(state0, state2), 1, tolerance);
}


TEST_F(T_NLevel_Basics, overlap)
{
	ASSERT_NEAR(overlap(state0, state0).real(), 1, tolerance);
	ASSERT_NEAR(overlap(state0, state0).imag(), 0, tolerance);

	ASSERT_NEAR(overlap(state0, state1).real(), 0, tolerance);
	ASSERT_NEAR(overlap(state0, state1).imag(), 0, tolerance);

	ASSERT_NEAR(overlap(state0, state2).real(), 0, tolerance);
	ASSERT_NEAR(overlap(state0, state2).imag(), 0, tolerance);


}


TEST_F(T_NLevel_Basics, variance)
{
	for (auto i = 0u; i < hilbertSpace.nLevels(); i++)
	{
		ASSERT_NEAR(variance(H0, makeState(hilbertSpace, fock::state{ i })), 0, tolerance);
	}
}

TEST_F(T_NLevel_Basics, standardDeviation)
{
	for (auto i = 0u; i < hilbertSpace.nLevels(); i++)
	{
		ASSERT_NEAR(standardDeviation(H0, makeState(hilbertSpace, fock::state{ i })), 0, tolerance);
	}
}


TEST_F(T_NLevel_Basics, expectationValue)
{
	for (auto i = 0u; i < hilbertSpace.nLevels(); i++)
	{
		ASSERT_NEAR(expectationValue(H0, makeState(hilbertSpace, fock::state{ i })), E.at(i), tolerance);
	}
}

TEST_F(T_NLevel_Basics, operatorConstruction)
{
	auto op = makeOperator(hilbertSpace, { {1, 2}, {3,4} });
	auto m = op._mat().mat();

	ASSERT_EQ(m.at(0, 0), 1); 	ASSERT_EQ(m.at(0, 1), 2);
	ASSERT_EQ(m.at(1, 0), 3); 	ASSERT_EQ(m.at(1, 1), 4);


}

TEST_F(T_NLevel_Basics, fockStateConstruction)
{
	auto psi0 = makeState(hilbertSpace,fock::state{ 0 });
	EXPECT_NEAR(1.0, fidelity(psi0, makeState(hilbertSpace, { 1,0,0 })), 1e-16);

	auto psi1 = makeState(hilbertSpace,fock::state{ 1 });
	EXPECT_NEAR(1.0, fidelity(psi1, makeState(hilbertSpace, { 0,1,0 })), 1e-16);

	auto psi2 = makeState(hilbertSpace,fock::state{ 2 });
	EXPECT_NEAR(1.0, fidelity(psi2, makeState(hilbertSpace, { 0,0,1 })), 1e-16);

	EXPECT_ANY_THROW(makeState(hilbertSpace, fock::state{ 3 }));
	EXPECT_ANY_THROW(makeState(hilbertSpace, fock::state{ 0,1 }));
	EXPECT_ANY_THROW(makeState(hilbertSpace, fock::state{ 0,1,0,0,0,0,0,0 }));



}

TEST_F(T_NLevel_Basics, fockOperatorConstruction)
{
	using namespace fock;
	auto op = makeOperator(hilbertSpace, c(0));
	auto opDirect = makeOperator(hilbertSpace, { { 0, 0},
		{1, 0} });
	ASSERT_EQ(opDirect, op);

	op = makeOperator(hilbertSpace, a(0));
	opDirect = makeOperator(hilbertSpace, { {0, 1},
		{	0, 0} });
	ASSERT_EQ(opDirect, op);

	op = makeOperator(hilbertSpace, c(0) * a(0));
	opDirect = makeOperator(hilbertSpace, { {0, 0},
		{0, 1 } });
	ASSERT_EQ(opDirect, op);

	op = makeOperator(hilbertSpace, c(0) + a(0));
	opDirect = makeOperator(hilbertSpace, { {0, 1},
		{1, 0} });
	ASSERT_EQ(opDirect, op);
}

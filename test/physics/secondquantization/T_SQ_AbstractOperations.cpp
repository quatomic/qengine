﻿/* COPYRIGHT
 *
 * file="T_SQ_AbstractOperations.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "src/Abstract/Physics/SecondQuantization/FockState.h"
#include "src/Abstract/Physics/SecondQuantization/FockOperators.h"
#include "src/Utility/Constants.h"


using namespace qengine;
using namespace std::complex_literals;

class T_SQ_AbstractOperations: public ::testing::Test
{
	
};

TEST_F(T_SQ_AbstractOperations, FockState_creation)
{
	using namespace fock;

	auto f1 = internal::FockState{0,1,2};

	ASSERT_EQ(3u, f1._quanta().size());
	ASSERT_EQ(0u, f1._quanta().at(0));
	ASSERT_EQ(1u, f1._quanta().at(1));
	ASSERT_EQ(2u, f1._quanta().at(2));
}

TEST_F(T_SQ_AbstractOperations, FockState_comparison)
{
	using namespace fock;

	auto f1 = internal::FockState{ 0,1,2 };
	auto f1_2 = internal::FockState{ 0,1,2 };
	auto f2 = internal::FockState{ 1,1,1 };
	auto f3 = internal::FockState{ 0,1,3 };

	ASSERT_TRUE(f1 == f1);
	ASSERT_TRUE(f1 == f1_2);
	ASSERT_TRUE(f1_2 == f1);

	ASSERT_FALSE(f1 == f2);
	ASSERT_FALSE(f1 == f3);

	ASSERT_TRUE(f1 != f2);
	ASSERT_TRUE(f1 != f3);

	ASSERT_FALSE(f1 != f1);
	ASSERT_FALSE(f1 != f1_2);

	ASSERT_FALSE(f1 < f1);
	ASSERT_FALSE(f1 < f1_2);

	ASSERT_TRUE(f1 < f2);
	ASSERT_TRUE(f1 < f3);

	ASSERT_FALSE(f1 > f1);
	ASSERT_FALSE(f1 > f1_2);

	ASSERT_FALSE(f1 > f2);
	ASSERT_FALSE(f1 > f3);
	ASSERT_TRUE(f2 > f1);
	ASSERT_TRUE(f2 > f1_2);

	ASSERT_TRUE(f1 >= f1);
	ASSERT_TRUE(f1 >= f1_2);

	ASSERT_FALSE(f1 >= f2);
	ASSERT_FALSE(f1 >= f3);

	ASSERT_TRUE(f1 <= f1);
	ASSERT_TRUE(f1 <= f1_2);

	ASSERT_TRUE(f1 <= f2);
	ASSERT_TRUE(f1 <= f3);
}

TEST_F(T_SQ_AbstractOperations, creationOperator)
{
	using namespace fock; 

	auto f = fock::state{ 0,1,0 };

	auto op = c(0);

	auto cf = op*f;
	auto cf_true = fock::state{1,1,0};
	EXPECT_EQ(cf, cf_true);

	f = fock::state{ 0,1,0 }+fock::state{ 2,1,1 };

	cf = op*f;
	cf_true = fock::state{ 1,1,0 }+std::sqrt(3)*state{ 3, 1, 1 };
	EXPECT_EQ(cf, cf_true);

	f = fock::state{ std::numeric_limits<count_t>::max(), 0,0 };
	cf = op*f;
	EXPECT_TRUE(cf.empty());
}

TEST_F(T_SQ_AbstractOperations, annihilationOperator)
{
	using namespace fock; 

	auto f = state{ 1,1,0 };

	auto op = a(0);

	auto af = op*f;
	auto af_true = state{0,1,0};
	EXPECT_EQ(af_true, af);

	f = state{ 1,1,0 }+state{ 2,1,1 };

	af = op*f;
	af_true = state{ 0,1,0 }+std::sqrt(2)*state{ 1, 1, 1 };
	EXPECT_EQ(af_true, af);

	f = state{0, 0,0 };
	af = op*f;
	EXPECT_TRUE(af.empty());
}

TEST_F(T_SQ_AbstractOperations, numberOperator)
{
	using namespace fock;

	auto f = state{ 1,1,0 };

	auto op = n(0);

	auto af = op*f;
	auto af_true = state{ 1,1,0 };
	EXPECT_EQ(af_true, af);

	f = state{ 2,1,0 }+state{ 5,1,1 };

	af = op*f;
	af_true = 2*state{ 2, 1, 0 }+5*state{ 5, 1, 1 };
	EXPECT_EQ(af_true, af);

	f = state{ 0, 0, 0 };
	af = op*f;
	af_true = 0 * state{ 0,0,0 };
	EXPECT_EQ(af_true, af);
}

TEST_F(T_SQ_AbstractOperations, productOperator)
{
	using namespace fock; 

	auto f = state{ 1,1,0 };

	auto op = c(0)*a(0);

	auto af = op*f;
	auto af_true = state{1,1,0};
	EXPECT_EQ(af_true, af);

	f = state{ 2,1,0 }+state{ 5,1,1 };

	af = op*f;
	af_true = sqrt(2)*sqrt(2)*state{ 2,1,0 }+sqrt(5)*sqrt(5)*state{ 5, 1, 1 };
	EXPECT_EQ(af_true, af);

	f = state{0, 0, 0 };
	af = op*f;
	EXPECT_TRUE(af.empty());
}

TEST_F(T_SQ_AbstractOperations, sumOperator)
{
	using namespace fock; 

	auto f = state{ 1,1,0 };

	auto op = c(0)+a(0);

	auto af = op*f;
	auto af_true = state{0,1,0}+std::sqrt(2)*state{2,1,0};
	EXPECT_EQ(af_true, af);

	f = state{ 2,1,0 }+state{ 4,1,0 };

	af = op*f;
	af_true = std::sqrt(2)*state{ 1, 1, 0 } + (2 + std::sqrt(3))*state{3,1,0}+ std::sqrt(5)*state{5,1,0};
	EXPECT_EQ(af_true, af);

	f = state{0, 0, 0 };
	af = op*f;
	af_true = state{ 1,0,0 };
	EXPECT_EQ(af_true, af);
}

TEST_F(T_SQ_AbstractOperations, scalarMulOperator)
{
	using namespace fock; 

	auto f = state{ 1,1,0 };

	const auto op1 = 2*a(0);
	const auto op2 = 1i * c(0);

	auto af = op1*f;
	auto af_true = 2*state{0,1,0};
	EXPECT_EQ(af_true, af);

	af = op2 * f;
	af_true = 1i * std::sqrt(2)*state{ 2, 1, 0 };
	EXPECT_EQ(af_true, af);

	f = state{ 1,1,0 }+state{ 2,1,1 };

	af = op1*f;
	af_true = 2*state{ 0,1,0 }+2*std::sqrt(2)*state{ 1, 1, 1 };
	EXPECT_EQ(af_true, af);

	af = op2*f;
	af_true = 1i * std::sqrt(2)*state{ 2, 1, 0 } +1i * std::sqrt(3)*state{ 3, 1, 1 };
	EXPECT_EQ(af_true, af);

	f = state{0, 0, 0 };

	af = op1 * f;
	EXPECT_TRUE(af.empty());

	af = op2*f;
	af_true = 1i*state{ 1,0,0 };
	EXPECT_EQ(af_true, af);
}


TEST_F(T_SQ_AbstractOperations, operatorPow)
{
	using namespace fock; 

	auto f = state{ 0,0,0 };

	auto op = pow(c(0) + c(1) + c(2), 2); 

	auto of = op * f;
	auto of_ref = (c(0) + c(1) + c(2))*(c(0) + c(1) + c(2))*f;

	ASSERT_EQ(of_ref, of);
}

﻿/* COPYRIGHT
 *
 * file="T_SQ_OpaqueOperator.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <gtest/gtest.h>

#include "qengine/NLevel.h"
#include "src/Abstract/Physics/SecondQuantization/OpaqueOperator.h"

using namespace qengine;

class T_SQ_OpaqueOperator : public ::testing::Test
{

};

TEST_F(T_SQ_OpaqueOperator, copyIn)
{
	const auto s = n_level::makeHilbertSpace(2);

	const auto A = makeOperator(s, { {0,1},
									{1,0 } });

	const auto opaqueA = makeOpaqueOperator(A);

	const auto state = makeState(s, CVec{ 1.0,0 });

	auto sTest = opaqueA * state;
	auto sRef = makeState(s, { 0,1 });

	EXPECT_EQ(sTest.vec(), sRef.vec());

}
TEST_F(T_SQ_OpaqueOperator, moveIn)
{
	const auto s = n_level::makeHilbertSpace(2);

	const auto A = makeOperator(s, { {0,1},
		{1,0} });

	const auto opaqueA = makeOpaqueOperator(std::move(A));

	const auto state = makeState(s, CVec{ 1.0,0 });

	auto sTest = opaqueA * state;
	auto sRef = makeState(s, { 0,1 });

	EXPECT_EQ(sTest.vec(), sRef.vec());

}

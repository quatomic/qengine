﻿/* COPYRIGHT
 *
 * file="T_BH_Basics.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include <qengine/BoseHubbard.h>

#include "additionalAsserts.h"


using std::cout;
using std::endl;

using namespace qengine;
using namespace second_quantized;


class T_BH_Basics_Fixture : public ::testing::Test
{
public:

};

TEST_F(T_BH_Basics_Fixture, isInBasis)
{
	const auto s = bosehubbard::makeHilbertSpace(3, 3);

	auto ff = internal::FockState{ 3,0,0 };
	
	EXPECT_TRUE(s._isInBasis(internal::FockState{3,0,0}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{0,3,0}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{0,0,3}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{2,1,0}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{2,0,1}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{1,2,0}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{0,2,1}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{1,0,2}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{0,1,2}));
	EXPECT_TRUE(s._isInBasis(internal::FockState{1,1,1}));



	EXPECT_FALSE(s._isInBasis(internal::FockState{0,3})); // not enough modes
	EXPECT_FALSE(s._isInBasis(internal::FockState{0,1,0,2})); // too many modes

	EXPECT_FALSE(s._isInBasis(internal::FockState{ 0,0,1 })); // not enough particles
	EXPECT_FALSE(s._isInBasis(internal::FockState{ 0,0,4 })); // too many particles

}

TEST_F(T_BH_Basics_Fixture, stateFromFock)
{
	const auto s = bosehubbard::makeHilbertSpace(2, 2);

	auto state = makeState(s, fock::state{ 1,1 });
	auto stateExpect = makeState(s, {0.0,1.0,0.0 });
	EXPECT_EQ(state, stateExpect);

	state = makeState(s, fock::state{ 1,1 }+fock::state{ 2,0 });
	stateExpect = makeState(s, {1.0,1.0,0.0});
	EXPECT_EQ(state, stateExpect);

	EXPECT_ANY_THROW(makeState(s, fock::state{ 0 }));
	EXPECT_ANY_THROW(makeState(s, fock::state{ 0,0 }));
}

TEST_F(T_BH_Basics_Fixture, denseOperatorFromFock)
{
	using namespace fock;
	const auto s = bosehubbard::makeHilbertSpace(2, 2);

	auto op = makeDenseOperator(s, n(0));

	auto opDirect = makeDenseOperator(s, {{2, 0, 0}, {0,1,0}, {0,0,0}});

	EXPECT_EQ(opDirect, op);

	op = makeDenseOperator(s, c(0)*a(1)+c(1)*a(0)); //hopping operator

	opDirect = makeDenseOperator(s, { { 0, std::sqrt(2), 0 },{ std::sqrt(2),0,std::sqrt(2) },{ 0,std::sqrt(2),0 } });

	EXPECT_EQ(opDirect, op);
}

TEST_F(T_BH_Basics_Fixture, sparseOperatorFromFock)
{
	using namespace fock;
	const auto s = bosehubbard::makeHilbertSpace(2, 2);

	auto op = makeOperator(s, n(0));

	//auto opDirect = makeDenseOperator(s,);
	auto opDirect = Operator<internal::SqSparseMat, real, internal::SerializedBHHilbertSpace>(internal::SqSparseMat<real>(internal::Sp_RMat(RMat({ { 2, 0, 0 },{ 0,1,0 },{ 0,0,0 } }))), s._serializedHilbertSpace());
	EXPECT_EQ(opDirect, op);

	op = makeOperator(s, c(0)*a(1)+c(1)*a(0)); //hopping operator

	opDirect = Operator<internal::SqSparseMat, real, internal::SerializedBHHilbertSpace>(internal::SqSparseMat<real>(internal::Sp_RMat(RMat({ { 0, std::sqrt(2), 0 },{ std::sqrt(2),0,std::sqrt(2) },{ 0,std::sqrt(2),0 } }))), s._serializedHilbertSpace());

	EXPECT_EQ(opDirect, op);
}


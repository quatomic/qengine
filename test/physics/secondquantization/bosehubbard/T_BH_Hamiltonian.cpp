﻿/* COPYRIGHT
 *
 * file="T_BH_Hamiltonian.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "qengine/BoseHubbard.h"
#include "qengine/NLevel.h"

#include "additionalAsserts.h"

#include <armadillo>

using namespace qengine;
using namespace second_quantized;


class T_BH_Hamiltonian_Fixture : public ::testing::Test
{
public:

	T_BH_Hamiltonian_Fixture() :
		bs2(2, 2),
		bs3(3, 3)
	{}

	bosehubbard::ApiHilbertSpace bs2;
	bosehubbard::ApiHilbertSpace bs3;

	auto makeHamiltonian(const bosehubbard::ApiHilbertSpace hs, const real j, const real u)
	{
		return -j * hs.makeHoppingOperator() + u * hs.makeOnSiteOperator();
	}
	auto makeDenseHamiltonian(const bosehubbard::ApiHilbertSpace hs, const real j, const real u)
	{
		return -j * hs.makeDenseHoppingOperator() + u * hs.makeOnSiteOperator();
	}
};

TEST_F(T_BH_Hamiltonian_Fixture, basis)
{
	using namespace bosehubbard;
	ASSERT_EQ(2u, bs2._basis().at(0)._quanta().size()); ASSERT_EQ(3u, bs2._basis().size());
	ASSERT_EQ(3u, bs3._basis().at(0)._quanta().size()); ASSERT_EQ(10u, bs3._basis().size());
	//    ASSERT_EQ(9,H9._basis().n_rows()); ASSERT_EQ(24310,H9._basis().n_cols());

	auto basisMat = bosehubbard::detail::rMatFromBasis(bs3._basis());

	arma::mat expectedBasisMat;
	expectedBasisMat << 3 << 2 << 1 << 0 << 2 << 1 << 0 << 1 << 0 << 0 << arma::endr
		<< 0 << 1 << 2 << 3 << 0 << 1 << 2 << 0 << 1 << 0 << arma::endr
		<< 0 << 0 << 0 << 0 << 1 << 1 << 1 << 2 << 2 << 3 << arma::endr;

	EXPECT_EQ(RMat(expectedBasisMat), basisMat);
}

//TEST_F(T_BH_Hamiltonian_Fixture, cutoff)
//{
//	using namespace bosehubbard;
//
//	// check dimension size of basis
//	auto space33 = bosehubbard::ApiHilbertSpace(3, 3, 3); auto H3_cut3 = makeHamiltonian(space33, 1.0, 1.0);
//	auto space32 = bosehubbard::ApiHilbertSpace(3, 3, 2); auto H3_cut2 = makeHamiltonian(space32, 1.0, 1.0);
//	auto space31 = bosehubbard::ApiHilbertSpace(3, 3, 1); auto H3_cut1 = makeHamiltonian(space31, 1.0, 1.0);
//
//	ASSERT_EQ(10u, space33._basis().size());
//	ASSERT_EQ(7u, space32._basis().size());
//	ASSERT_EQ(1u, space31._basis().size());
//
//	auto space44 = bosehubbard::ApiHilbertSpace(4, 4, 4); auto H4_cut4 = makeHamiltonian(space44, 1.0, 1.0);
//	auto space43 = bosehubbard::ApiHilbertSpace(4, 4, 3); auto H4_cut3 = makeHamiltonian(space43, 1.0, 1.0);
//	auto space42 = bosehubbard::ApiHilbertSpace(4, 4, 2); auto H4_cut2 = makeHamiltonian(space42, 1.0, 1.0);
//	auto space41 = bosehubbard::ApiHilbertSpace(4, 4, 1); auto H4_cut1 = makeHamiltonian(space41, 1.0, 1.0);
//
//	ASSERT_EQ(35u, space44._basis().size());
//	ASSERT_EQ(31u, space43._basis().size());
//	ASSERT_EQ(19u, space42._basis().size());
//	ASSERT_EQ(1u, space41._basis().size());
//
//	// check stateMap size
//	ASSERT_EQ(space44._basis().size(), space44._stateMap().size());
//	ASSERT_EQ(space43._basis().size(), space43._stateMap().size());
//	ASSERT_EQ(space42._basis().size(), space42._stateMap().size());
//	ASSERT_EQ(space41._basis().size(), space41._stateMap().size());
//
//	// check Hamiltonian size
//	ASSERT_EQ(10u, H3_cut3._mat().dim());
//	ASSERT_EQ(7u, H3_cut2._mat().dim());
//	ASSERT_EQ(1u, H3_cut1._mat().dim());
//
//	ASSERT_EQ(35u, H4_cut4._mat().dim());
//	ASSERT_EQ(31u, H4_cut3._mat().dim());
//	ASSERT_EQ(19u, H4_cut2._mat().dim());
//	ASSERT_EQ(1u, H4_cut1._mat().dim());
//
//	// check Hamiltonian with cutoff 2 corresponds to full Hamiltonian with deleted basis states i=0,3,9
//	// (i,j) belongs to non-truncated Hamiltonian
//	// (ii,jj) belongs to truncated
//
//	auto H3 = makeHamiltonian(bs3, 1.0, 1.0);
//
//	auto ii = 0u;
//	for (auto i = 0u; i < H3._mat().dim(); ++i) {
//		auto jj = 0u;
//		if (i == 0 || i == 3 || i == 9) continue;
//
//		for (auto j = 0u; j < H3._mat().dim(); ++j) {
//			if (j == 0 || j == 3 || j == 9) continue;
//			ASSERT_EQ(H3._mat().mat().at(i, j), H3_cut2._mat().mat().at(ii, jj));
//			++jj;
//		}
//		++ii;
//	}
//
//
//}

TEST_F(T_BH_Hamiltonian_Fixture, hoppingOperator)
{
	auto hop2 = bs2.makeHoppingOperator();
	ASSERT_EQ(bs2.dim(), hop2._mat().dim());

	ASSERT_NEAR(0, hop2._mat().mat().at(0, 0), 1e-10); ASSERT_NEAR(sqrt(2.0), hop2._mat().mat().at(0, 1), 1e-10); ASSERT_NEAR(0, hop2._mat().mat().at(0, 2), 1e-10);
	ASSERT_NEAR(sqrt(2.0), hop2._mat().mat().at(1, 0), 1e-10); ASSERT_NEAR(0, hop2._mat().mat().at(1, 1), 1e-10); ASSERT_NEAR(sqrt(2.0), hop2._mat().mat().at(1, 2), 1e-10);
	ASSERT_NEAR(0, hop2._mat().mat().at(2, 0), 1e-10); ASSERT_NEAR(sqrt(2.0), hop2._mat().mat().at(2, 1), 1e-10); ASSERT_NEAR(0, hop2._mat().mat().at(2, 2), 1e-10);


	auto hop3 = bs3.makeHoppingOperator();
	ASSERT_EQ(bs3.dim(), hop3._mat().dim());
	auto matRef = hop3._mat().mat();


	ASSERT_NEAR(0, matRef.at(0, 0), 1e-4);               // <3 0 0|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(3)*sqrt(1), matRef.at(1, 0), 1e-4); // <2 1 0|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(3)*sqrt(1), matRef.at(4, 0), 1e-4); // <2 0 1|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(2)*sqrt(2), matRef.at(2, 1), 1e-4); // <2 1 0|âˆ‘m_{i,j}|1 2 0>
	ASSERT_NEAR(sqrt(1)*sqrt(1), matRef.at(6, 2), 1e-4); // <1 2 0|âˆ‘m_{i,j}|0 2 1>
	ASSERT_NEAR(sqrt(2)*sqrt(2), matRef.at(4, 7), 1e-4); // <2 0 1|âˆ‘m_{i,j}|1 0 2>
	ASSERT_NEAR(sqrt(3)*sqrt(1), matRef.at(9, 7), 1e-4); // <1 0 2|âˆ‘m_{i,j}|0 0 3>
	ASSERT_NEAR(sqrt(2)*sqrt(1), matRef.at(1, 5), 1e-4); // <2 1 0|âˆ‘m_{i,j}|1 1 1>
	ASSERT_NEAR(sqrt(2)*sqrt(1), matRef.at(5, 6), 1e-4); // <1 1 1|âˆ‘m_{i,j}|0 2 1>

	auto hop3_hard = bs3.makeHoppingOperator(false);
	ASSERT_EQ(bs3.dim(), hop3_hard._mat().dim());
	matRef = hop3_hard._mat().mat();
	ASSERT_NEAR(0, matRef.at(0, 0), 1e-4);               // <3 0 0|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(3)*sqrt(1), matRef.at(1, 0), 1e-4); // <2 1 0|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(0, matRef.at(4, 0), 1e-4);               // <2 0 1|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(2)*sqrt(2), matRef.at(2, 1), 1e-4); // <2 1 0|âˆ‘m_{i,j}|1 2 0>
	ASSERT_NEAR(0, matRef.at(6, 2), 1e-4);               // <1 2 0|âˆ‘m_{i,j}|0 2 1>
	ASSERT_NEAR(0, matRef.at(4, 7), 1e-4);               // <2 0 1|âˆ‘m_{i,j}|1 0 2>
	ASSERT_NEAR(0, matRef.at(9, 7), 1e-4);               // <1 0 2|âˆ‘m_{i,j}|0 0 3>
	ASSERT_NEAR(0, matRef.at(1, 5), 1e-4);               // <2 1 0|âˆ‘m_{i,j}|1 1 1>
	ASSERT_NEAR(sqrt(2)*sqrt(1), matRef.at(5, 6), 1e-4); // <1 1 1|âˆ‘m_{i,j}|0 2 1>
}
TEST_F(T_BH_Hamiltonian_Fixture, denseHoppingOperator)
{
	auto hop2 = bs2.makeDenseHoppingOperator();
	ASSERT_EQ(bs2.dim(), hop2._mat().dim());

	ASSERT_NEAR(0, hop2._mat().mat().at(0, 0), 1e-10); ASSERT_NEAR(sqrt(2.0), hop2._mat().mat().at(0, 1), 1e-10); ASSERT_NEAR(0, hop2._mat().mat().at(0, 2), 1e-10);
	ASSERT_NEAR(sqrt(2.0), hop2._mat().mat().at(1, 0), 1e-10); ASSERT_NEAR(0, hop2._mat().mat().at(1, 1), 1e-10); ASSERT_NEAR(sqrt(2.0), hop2._mat().mat().at(1, 2), 1e-10);
	ASSERT_NEAR(0, hop2._mat().mat().at(2, 0), 1e-10); ASSERT_NEAR(sqrt(2.0), hop2._mat().mat().at(2, 1), 1e-10); ASSERT_NEAR(0, hop2._mat().mat().at(2, 2), 1e-10);


	auto hop3 = bs3.makeDenseHoppingOperator();
	ASSERT_EQ(bs3.dim(), hop3._mat().dim());
	auto matRef = hop3._mat().mat();


	ASSERT_NEAR(0, matRef.at(0, 0), 1e-4);               // <3 0 0|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(3)*sqrt(1), matRef.at(1, 0), 1e-4); // <2 1 0|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(3)*sqrt(1), matRef.at(4, 0), 1e-4); // <2 0 1|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(2)*sqrt(2), matRef.at(2, 1), 1e-4); // <2 1 0|âˆ‘m_{i,j}|1 2 0>
	ASSERT_NEAR(sqrt(1)*sqrt(1), matRef.at(6, 2), 1e-4); // <1 2 0|âˆ‘m_{i,j}|0 2 1>
	ASSERT_NEAR(sqrt(2)*sqrt(2), matRef.at(4, 7), 1e-4); // <2 0 1|âˆ‘m_{i,j}|1 0 2>
	ASSERT_NEAR(sqrt(3)*sqrt(1), matRef.at(9, 7), 1e-4); // <1 0 2|âˆ‘m_{i,j}|0 0 3>
	ASSERT_NEAR(sqrt(2)*sqrt(1), matRef.at(1, 5), 1e-4); // <2 1 0|âˆ‘m_{i,j}|1 1 1>
	ASSERT_NEAR(sqrt(2)*sqrt(1), matRef.at(5, 6), 1e-4); // <1 1 1|âˆ‘m_{i,j}|0 2 1>

	auto hop3_hard = bs3.makeDenseHoppingOperator(false);
	ASSERT_EQ(bs3.dim(), hop3_hard._mat().dim());
	matRef = hop3_hard._mat().mat();
	ASSERT_NEAR(0, matRef.at(0, 0), 1e-4);               // <3 0 0|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(3)*sqrt(1), matRef.at(1, 0), 1e-4); // <2 1 0|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(0, matRef.at(4, 0), 1e-4);               // <2 0 1|âˆ‘m_{i,j}|3 0 0>
	ASSERT_NEAR(sqrt(2)*sqrt(2), matRef.at(2, 1), 1e-4); // <2 1 0|âˆ‘m_{i,j}|1 2 0>
	ASSERT_NEAR(0, matRef.at(6, 2), 1e-4);               // <1 2 0|âˆ‘m_{i,j}|0 2 1>
	ASSERT_NEAR(0, matRef.at(4, 7), 1e-4);               // <2 0 1|âˆ‘m_{i,j}|1 0 2>
	ASSERT_NEAR(0, matRef.at(9, 7), 1e-4);               // <1 0 2|âˆ‘m_{i,j}|0 0 3>
	ASSERT_NEAR(0, matRef.at(1, 5), 1e-4);               // <2 1 0|âˆ‘m_{i,j}|1 1 1>
	ASSERT_NEAR(sqrt(2)*sqrt(1), matRef.at(5, 6), 1e-4); // <1 1 1|âˆ‘m_{i,j}|0 2 1>
}

TEST_F(T_BH_Hamiltonian_Fixture, onSiteOperator)
{
	auto onSite3 = bs3.makeOnSiteOperator();

	EXPECT_DOUBLE_EQ(3, onSite3._mat().diag().at(0));
	EXPECT_DOUBLE_EQ(1, onSite3._mat().diag().at(1));
	EXPECT_DOUBLE_EQ(1, onSite3._mat().diag().at(2));
	EXPECT_DOUBLE_EQ(3, onSite3._mat().diag().at(3));
	EXPECT_DOUBLE_EQ(1, onSite3._mat().diag().at(4));
	EXPECT_DOUBLE_EQ(0, onSite3._mat().diag().at(5));
	EXPECT_DOUBLE_EQ(1, onSite3._mat().diag().at(6));
	EXPECT_DOUBLE_EQ(1, onSite3._mat().diag().at(7));
	EXPECT_DOUBLE_EQ(1, onSite3._mat().diag().at(8));
	EXPECT_DOUBLE_EQ(3, onSite3._mat().diag().at(9));
}


TEST_F(T_BH_Hamiltonian_Fixture, potentialOperator)
{
	const auto sitePositions = linspace(-1, 1, bs3.nSites());
	const auto potential = sitePositions;

	auto V_trans = bs3.transformPotential(potential);

	ASSERT_EQ(-3, V_trans._mat().diag().at(0));
	ASSERT_EQ(-2, V_trans._mat().diag().at(1));
	ASSERT_EQ(-1, V_trans._mat().diag().at(2));
	ASSERT_EQ(0, V_trans._mat().diag().at(3));
	ASSERT_EQ(-1, V_trans._mat().diag().at(4));
	ASSERT_EQ(0, V_trans._mat().diag().at(5));
	ASSERT_EQ(1, V_trans._mat().diag().at(6));
	ASSERT_EQ(1, V_trans._mat().diag().at(7));
	ASSERT_EQ(2, V_trans._mat().diag().at(8));
	ASSERT_EQ(3, V_trans._mat().diag().at(9));

}

TEST_F(T_BH_Hamiltonian_Fixture, hamiltonianOperator)
{
	const RVec sitePositions = linspace(-1, 1, bs3.nSites());
	const auto staticPotential = sitePositions;
	const auto H = (makeHamiltonian(bs3, 1.0, 1.0) + bs3.transformPotential(staticPotential))._mat();

	auto matRef = H.mat();
	EXPECT_DOUBLE_EQ(0, matRef.at(0, 0));
	EXPECT_DOUBLE_EQ(-sqrt(3), matRef.at(0, 1));
	EXPECT_DOUBLE_EQ(0, matRef.at(0, 2));
	EXPECT_DOUBLE_EQ(-sqrt(2)*sqrt(2), matRef.at(1, 2));
	EXPECT_DOUBLE_EQ(0, matRef.at(5, 5));
	EXPECT_DOUBLE_EQ(-sqrt(2), matRef.at(7, 5));
	EXPECT_DOUBLE_EQ(3, matRef.at(8, 8));
	EXPECT_DOUBLE_EQ(6, matRef.at(9, 9));
}

TEST_F(T_BH_Hamiltonian_Fixture, applyTo)
{
	CVec vec(3, complex(1.0, 0));
	auto H2 = makeHamiltonian(bs2, 1.0, 1.0);

	auto state = makeState(bs2, vec);
	H2.applyInPlace(state);
	ASSERT_EQ(1 - sqrt(2.0), state.at(0));
	ASSERT_EQ(-2 * sqrt(2.0), state.at(1));
	ASSERT_EQ(1 - sqrt(2.0), state.at(2));
}

TEST_F(T_BH_Hamiltonian_Fixture, denseSpectrum)
{
	const auto u = 1.0;
	const auto j = 1.0;

	const auto H = makeDenseHamiltonian(bs2, u, j);
	const auto spectrum = H.makeSpectrum(2);

	const auto m = internal::SqDenseMat<real>(H._mat());
	//std::cout << m << std::endl;

	const auto denseSpectrum = internal::getSpectrum(m, 2, 1.0);
	//std::cout << denseSpectrum.largestEigenstate() << std::endl;
/*
	std::cout << "sparse: " << std::endl << "E0: " << makeSpectrum.eigenvalue(0) << std::endl;
	std::cout << "vec0: " << makeSpectrum.eigenState(0) << std::endl;

	std::cout << "dense: " << std::endl;*/

	//for(auto i = 0u;i<3; ++i)
	//{
	//	std::cout << "E" << i << ": " << denseSpectrum.eigenvalue(i) << std::endl;
	//	std::cout << "vec" << i << ": " << denseSpectrum.eigenvector(i) << std::endl;
	//}

	const auto E1_analytical = (u - std::sqrt(pow(u, 2) + 16 * std::pow(j, 2))) / 2.0;
	const auto E2_analytical = u;
	const auto E3_analytical = (u + std::sqrt(pow(u, 2) + 16 * std::pow(j, 2))) / 2.0;

	// todo: check eigenvectors

	//arma::vec v1_analytical(3),v2_analytical(3),v3_analytical(3);
	//v1_analytical << 1/sqrt
	//v2_analytical << -1/sqrt(2) << 0 << +1/sqrt(2);
	//EXPECT_APPROX_EQUAL(v1_analytical,arma::conv_to<arma::vec>::from(bh2.eigVecs.col(0)),tolerance);
	//EXPECT_APPROX_EQUAL(v2_analytical,arma::conv_to<arma::vec>::from(bh2.eigVecs.col(1)),tolerance);


	ASSERT_NEAR(E1_analytical, spectrum.eigenvalue(0).real(), 1e-10);
	ASSERT_NEAR(E2_analytical, spectrum.eigenvalue(1).real(), 1e-10);
	ASSERT_NEAR(E3_analytical, spectrum.eigenvalue(2).real(), 1e-10);
}

TEST_F(T_BH_Hamiltonian_Fixture, sparseEigvalsGrowContinuously)
{
	auto H = makeHamiltonian(bs3, 1.0, 1.0);
	auto spectrum = H.makeSpectrum(7);

	auto eigvals = std::vector<real>();
	for (auto i = 0u; i <= spectrum.largestEigenstate(); ++i)
	{
		eigvals.push_back(spectrum.eigenvalue(i).real());
	}
	auto sorted = eigvals;
	std::sort(eigvals.begin(), eigvals.end());
	EXPECT_EQ(eigvals, sorted);

	for (auto i = 0u; i <= spectrum.largestEigenstate(); ++i)
	{
		ASSERT_NEAR(expectationValue(H, spectrum.eigenState(i)), spectrum.eigenvalue(i).real(), 1e-14);
		ASSERT_NEAR(0.0, variance(H, spectrum.eigenState(i)), 1e-14);
	}
}

TEST_F(T_BH_Hamiltonian_Fixture, singleParticleDensityMatrixNoCutoff)
{
	auto s = bosehubbard::makeHilbertSpace(3, 3);

	auto refFunction = [&space = s](const auto& state)->CMat
	{
		CMat rho1(space.nSites(), space.nSites(), 0.0);
		for (auto i = 0u; i < space.nSites(); ++i)
		{
			for (auto j = 0u; j < space.nSites(); ++j)
			{
				using namespace fock;
				auto op = c(i)*a(j);
				rho1.at(i, j) = expectationValue(makeOperator(space, op), state);
			}
		}
		return rho1;
	};

	auto mott = s.makeMott();
	auto mottmat = s.singleParticleDensityMatrix(mott);
	auto refMat = refFunction(mott);

	EXPECT_NEAR_M(refMat, mottmat, 3e-16);


	auto sf = s.makeSuperFluid();
	auto sfMat = s.singleParticleDensityMatrix(sf);
	refMat = refFunction(sf);

	EXPECT_NEAR_M(refMat, sfMat, 3e-16);
}

class T_BH_TE_Fixture: public T_BH_Hamiltonian_Fixture{};

TEST_F(T_BH_TE_Fixture, stepStationaryU)
{
	// stationary Mott, J=0, U=1
	auto H = makeHamiltonian(bs3, 0.0, 1.0);
	auto state = makeState(H[0]);
	real dt = 0.001;

	auto krylovOrder = 5;
	auto stepper = makeFixedTimeStepper(H, state, krylovOrder, dt);

	for (double t = 0; t < 1; t = t + dt)
	{
		ASSERT_NEAR(1.0, stepper.state().norm(), 1e-10);
		ASSERT_NEAR(1.0, fidelity(stepper.state(), state), 1e-10);
		stepper.cstep();
	}
}

TEST_F(T_BH_TE_Fixture, stepStationaryJ)
{
	// stationary Kinetic, J=1, U=0
	auto H = makeHamiltonian(bs3, 1.0, 0.0);
	auto state = makeState(H[0]);
	real dt = 0.001;

	auto krylovOrder = 5;
	auto stepper = makeFixedTimeStepper(H, state, krylovOrder, dt);

	for (double t = 0; t < 1; t = t + dt)
	{
		ASSERT_NEAR(1.0, stepper.state().norm(), 1e-10);
		ASSERT_NEAR(1.0, fidelity(stepper.state(), state), 1e-10);
		stepper.cstep();
	}
}

TEST_F(T_BH_TE_Fixture, stepStationaryUandJ)
{
	// stationary, J=1, U=1
	auto H = makeHamiltonian(bs3, 1.0, 1.0);
	auto state = makeState(H[0]);
	real dt = 0.001;

	auto krylovOrder = 5;
	auto stepper = makeFixedTimeStepper(H, state, krylovOrder, dt);

	for (double t = 0; t < 1; t = t + dt)
	{
		ASSERT_NEAR(1.0, stepper.state().norm(), 1e-10);
		ASSERT_NEAR(1.0, fidelity(stepper.state(), state), 1e-10);
		stepper.cstep();
	}
}


TEST_F(T_BH_TE_Fixture, stepAndCheckRevivalTime)
{
	auto H3 = makeDenseHamiltonian(bs3, 1.0, 1.0);
	auto spectrum = H3.makeSpectrum(1);

	auto state = spectrum.makeLinearCombination({ 1.0, 1.0 }, NORMALIZE);
	auto E0 = spectrum.eigenvalue(0).real();
	auto E1 = spectrum.eigenvalue(1).real();

	double revivalAnalytic = 2 * PI / (E1 - E0);
	real revivalNumeric = 0;

	real dt = revivalAnalytic / 1000;
	auto krylovOrder = 5;
	auto stepper = makeFixedTimeStepper(H3, state, krylovOrder, dt);

	for (double t = 0; t < 2 * revivalAnalytic; t = t + dt) {
		if (fidelity(state, stepper.state()) >= 1 - 1e-10 && t > 100 * dt) {
			revivalNumeric = t;
			break;
		}
		stepper.cstep();
	}

	ASSERT_NEAR(revivalAnalytic, revivalNumeric, 1e-10);
}

TEST_F(T_BH_TE_Fixture, stepCompare) {

	real dt = 0.001;

	auto H3 = makeHamiltonian(bs3, 1.0, 1.0);
	auto state = makeState(H3[1]);

	// krylov
	auto krylovOrder = 5;
	auto krylovStepper = makeFixedTimeStepper(H3, state, krylovOrder, dt);

	// direct
	auto s = n_level::makeHilbertSpace(10);
	auto H3Direct =internal::makeOperator(H3._mat(), s._serializedHilbertSpace());
	auto directState = makeState(s, state.vec());
	auto directStepper = makeFixedTimeStepper(H3Direct, directState, dt);


	// compare
	for (real t = 0; t <= 100 * dt; t += dt)
	{
		ASSERT_NEAR(1.0, abs(dot(krylovStepper.state().vec(), directStepper.state().vec())), 1e-10) << "t: " << t;


		ASSERT_NEAR_V(directStepper.state().vec(), krylovStepper.state().vec(),1e-10);

		krylovStepper.cstep();
		directStepper.cstep();
	}
}

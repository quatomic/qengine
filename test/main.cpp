﻿/* COPYRIGHT
 *
 * file="main.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <gtest/gtest.h>
#include <chrono>

#include "WriteTime.h"

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);

	auto start = std::chrono::steady_clock::now();
	int result = RUN_ALL_TESTS();
	auto end = std::chrono::steady_clock::now(); 
	
	unsigned int elapsed_seconds = static_cast<unsigned int>((end - start).count()/ std::chrono::steady_clock::period::den);

	std::cout << "Tests took: ";
	writeTime(elapsed_seconds);

	return result;
}

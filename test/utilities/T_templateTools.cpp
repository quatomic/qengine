﻿/* COPYRIGHT
 *
 * file="T_templateTools.cpp"
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#include "src/API/UserUtilities/AutoMember.h"
#include "src/Utility/has_function.h"
#include "src/NumberTypes.h"
#include "src/Utility/TypeList.h"
#include "src/Utility/cpp17Replacements/Tuple.h"
#include <src/Utility/isEqualityComparable.h>


using namespace qengine;
using namespace internal;

class TemplateTools_Fixture : public ::testing::Test
{
public:
};

TEST_F(TemplateTools_Fixture, and_all)
{
	static_assert(repl17::check_all_true(), "");
	static_assert(repl17::check_all_true(true), "");
	static_assert(repl17::check_all_true(true, true, true, true, true, true, true, true), "");


	static_assert(!repl17::check_all_true(false), "");
	static_assert(!repl17::check_all_true(true, true, true, false, true, true, true, true), "");
	static_assert(!repl17::check_all_true(false, true, true, false), "");
}


TEST_F(TemplateTools_Fixture, equalityComparable)
{
	struct NotEqualityComparable {};

	static_assert(isEqualityComparable_v<int>, "");
	static_assert(isInequalityComparable_v<int>, "");

	static_assert(!isEqualityComparable_v<NotEqualityComparable>, "");
	static_assert(!isInequalityComparable_v<NotEqualityComparable>, "");
}

class TypeListFixture : public ::testing::Test
{
public:
};


TEST_F(TypeListFixture, findFirstIndex)
{
	using Uniques = TypeList<int, bool, real, complex>;
	static_assert(findFirstIndex<int>(Uniques{}) == 0, "");
	static_assert(findFirstIndex<bool>(Uniques{}) == 1, "");
	static_assert(findFirstIndex<real>(Uniques{}) == 2, "");
	static_assert(findFirstIndex<complex>(Uniques{}) == 3, "");

	// If it is not in there, it returns past-the-end index
	static_assert(findFirstIndex<unsigned>(Uniques{}) == 4, "");

	using NotUnique = TypeList<int, bool, int, real, complex>;
	static_assert(findFirstIndex<int>(NotUnique{}) == 0, "");
	static_assert(findFirstIndex<bool>(NotUnique{}) == 1, "");
	static_assert(findFirstIndex<real>(NotUnique{}) == 3, "");
	static_assert(findFirstIndex<complex>(NotUnique{}) == 4, "");
	static_assert(findFirstIndex<unsigned>(NotUnique{}) == 5, "");
}

TEST_F(TypeListFixture, isInList)
{
	using Uniques = TypeList<int, bool, real, complex>;

	static_assert(isInList<int>(Uniques{}), "");
	static_assert(isInList<bool>(Uniques{}), "");
	static_assert(isInList<real>(Uniques{}), "");
	static_assert(isInList<complex>(Uniques{}), "");

	static_assert(!isInList<unsigned>(Uniques{}), "");

	using NotUnique = TypeList<int, bool, int, real, complex>;
	static_assert(isInList<int>(NotUnique{}), "");
	static_assert(isInList<bool>(NotUnique{}), "");
	static_assert(isInList<real>(NotUnique{}), "");
	static_assert(isInList<complex>(NotUnique{}), "");

	static_assert(!isInList<unsigned>(NotUnique{}), "");
}

TEST_F(TypeListFixture, containsNoDuplicates)
{
	using Uniques = TypeList<int, bool, real, complex>;
	static_assert(containsNoDuplicates(Uniques{}), "");

	using NotUnique = TypeList<int, bool, int, real, complex>;
	static_assert(!containsNoDuplicates(NotUnique{}), "");
}

TEST_F(TypeListFixture, size)
{
	using List0 = TypeList<>;
	using List1 = TypeList<int>;
	using List5 = TypeList<int, double, bool, complex, std::string>;

	static_assert(size(List0{}) == 0, "");
	static_assert(size(List1{}) == 1, "");
	static_assert(size(List5{}) == 5, "");
}

TEST_F(TypeListFixture, isEmpty)
{
	using List0 = TypeList<>;
	using List1 = TypeList<int>;
	using List5 = TypeList<int, double, bool, complex, std::string>;

	static_assert(isEmpty(List0{}) == true, "");
	static_assert(isEmpty(List1{}) == false, "");
	static_assert(isEmpty(List5{}) == false, "");
}

class Tuple_Fixture : public ::testing::Test
{
public:
};

TEST_F(Tuple_Fixture, applyToEach)
{
	auto f = [](const auto left, const auto right) {return left + right; };

	const auto left = makeTuple(1, 2);
	const auto right = makeTuple(3, 4);

	constexpr size_t size = internal::getFirstElementSize<decltype(left), decltype(right)>();
	static_assert(size == 2, "");


	auto result = internal::applyToEach(f, left, right);

	ASSERT_EQ(std::get<0>(result.data), 4);
	ASSERT_EQ(std::get<1>(result.data), 6);
}

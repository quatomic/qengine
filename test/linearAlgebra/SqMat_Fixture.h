﻿/* COPYRIGHT
 *
 * file="SqMat_Fixture.h" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include "src/Utility/Constants.h"

#include "src/Utility/LinearAlgebra/Vector.h"

#include "HarmonicAnalyticResults.h"

using namespace qengine::internal;

class SqMat_Fixture: public ::testing::Test
{
public:
	const double tolerance = 1e-13;

	const unsigned int defaultDim = 256;

	double x0 = 1.0;
	double omega = 5.0;
	RVec xArray = linspace(x0 - 2.5, x0 + 2.5, defaultDim);

	double dx = xArray.at(1) - xArray.at(0);

	CVec harm0{ (harmonicGroundstate(xArray, x0, omega)) };
	CVec harm1{ (harmonicFirstExcitedstate(xArray, x0, omega)) };
	CVec harm2{ (harmonicSecondExcitedstate(xArray, x0, omega)) };
};

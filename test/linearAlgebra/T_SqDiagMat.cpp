﻿/* COPYRIGHT
 *
 * file="T_SqDiagMat.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/Constants.h"

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/SqDiagMat.h"

#include "additionalAsserts.h"
#include "HarmonicAnalyticResults.h"

#include "SqMat_Fixture.h"

using namespace qengine;
using namespace internal;
using namespace std::complex_literals;

class SqDiagMat_Fixture : public SqMat_Fixture
{
public:
	SqDiagMat<real> x{ xArray };
	SqDiagMat<complex> ix{ 1i*xArray };
	SqDiagMat<complex> cx{ xArray };

	SqDiagMat<real> mr22{ RVec(2, 1.0) };
	SqDiagMat<real> mr22original = mr22;

	SqDiagMat<complex> mc22{ CVec(2, {1.0,1.0}) };
	SqDiagMat<complex> mc22original = mc22;

	complex one{ 1, 1 };
	complex zero{ 0, 0 };
};

TEST_F(SqDiagMat_Fixture, traits)
{
	static_assert(is_SqMat_v<SqDiagMat>, "SqDiagMat does not fulfill is_SqMat!");
}

TEST_F(SqDiagMat_Fixture, dimensions)
{
	EXPECT_EQ(defaultDim, x.dim());
	EXPECT_EQ(defaultDim, ix.dim());
	EXPECT_EQ(defaultDim, cx.dim());
}

TEST_F(SqDiagMat_Fixture, isHermitian)
{
	EXPECT_TRUE(x.isHermitian());
	EXPECT_FALSE(ix.isHermitian());

	EXPECT_TRUE(cx.isHermitian());
}

TEST_F(SqDiagMat_Fixture, multiplyWithState)
{
	EXPECT_NEAR(x0, std::real(dot(harm0, x*harm0))*dx, tolerance);
	EXPECT_NEAR(0, std::imag(dot(harm0, x*harm0))*dx, tolerance);

	EXPECT_NEAR(0, std::real(dot(harm0, ix*harm0))*dx, tolerance);
	EXPECT_NEAR(x0, std::imag(dot(harm0, ix*harm0))*dx, tolerance);
}

TEST_F(SqDiagMat_Fixture, eigHerm)
{
	count_t largestEigenstate = 3;
	{
		auto spectrum = getSpectrum_herm(x, largestEigenstate);

		auto vals = spectrum.eigenvalues();
		auto vecs = spectrum.eigenvectors();

		EXPECT_EQ(largestEigenstate + 1, vals.size());
		EXPECT_EQ(largestEigenstate + 1, vecs.n_cols());

		for (auto i = 0u; i <= largestEigenstate; ++i)
		{
			EXPECT_NEAR(xArray.at(i), vals.at(i), tolerance) << "x val" << i;

			auto state = CVec(vecs.col(i));
			EXPECT_NEAR(vals.at(i), std::real(dot(state, x*state)), tolerance) << "x vec" << i;
		}
	}


	{
		auto spectrum = getSpectrum_herm(cx, largestEigenstate);

		auto vals = spectrum.eigenvalues();
		auto vecs = spectrum.eigenvectors();

		EXPECT_EQ(largestEigenstate + 1, vals.size());
		EXPECT_EQ(largestEigenstate + 1, vecs.n_cols());

		for (auto i = 0u; i <= largestEigenstate; ++i)
		{
			EXPECT_NEAR(xArray.at(i), vals.at(i), tolerance) << "cx val" << i;

			auto state = CVec(vecs.col(i));
			EXPECT_NEAR(vals.at(i), std::real(dot(state, cx*state)), tolerance) << "cx vec" << i;
		}}
}

TEST_F(SqDiagMat_Fixture, exponentiate)
{
	EXPECT_NEAR_V(exp(x)*harm0, CVec(exp(xArray) * harm0), tolerance);
	EXPECT_NEAR_V(exp(ix)*harm0, CVec(exp(1i*xArray) * harm0), tolerance);
}


TEST_F(SqDiagMat_Fixture, inplaceSum)
{

	mr22 += mr22;
	EXPECT_EQ(2, mr22.diag().at(0));
	EXPECT_EQ(2, mr22.diag().at(1));
	mr22 = mr22original;

	mc22 += mc22;
	EXPECT_EQ(2.0*one, mc22.diag().at(0));
	EXPECT_EQ(2.0*one, mc22.diag().at(1));

	mc22 = mc22original;
}

TEST_F(SqDiagMat_Fixture, inplaceSubtract) {

	// subtraction
	mr22 -= mr22;
	EXPECT_EQ(0, mr22.diag().at(0));
	EXPECT_EQ(0, mr22.diag().at(1));

	mr22 = mr22original;

	mc22 -= mc22;
	EXPECT_EQ(zero, mc22.diag().at(0));
	EXPECT_EQ(zero, mc22.diag().at(1));

	mc22 = mc22original;
}

TEST_F(SqDiagMat_Fixture, inplaceScalarMultiply) {

	// scalar multiplication
	mr22 *= 2.0;
	EXPECT_EQ(2, mr22.diag().at(0));
	EXPECT_EQ(2, mr22.diag().at(1));
	mr22 = mr22original;

	mc22 *= 2.0;
	EXPECT_EQ(2.0*one, mc22.diag().at(0));
	EXPECT_EQ(2.0*one, mc22.diag().at(1));
	mc22 = mc22original;

	mc22 *= one;
	EXPECT_EQ(complex(0, 2), mc22.diag().at(0));
	EXPECT_EQ(complex(0, 2), mc22.diag().at(1));
	mc22 = mc22original;
}

TEST_F(SqDiagMat_Fixture, inplaceScalarDivision) {

	// scalar division
	mr22 /= 2.0;
	EXPECT_EQ(0.5, mr22.diag().at(0));
	EXPECT_EQ(0.5, mr22.diag().at(1));
	mr22 = mr22original;

	mc22 /= 2.0;
	EXPECT_EQ(0.5*one, mc22.diag().at(0));
	EXPECT_EQ(0.5*one, mc22.diag().at(1));
	mc22 = mc22original;

	mc22 /= one;
	EXPECT_EQ(complex(1, 0), mc22.diag().at(0));
	EXPECT_EQ(complex(1, 0), mc22.diag().at(1));
	mc22 = mc22original;
}

TEST_F(SqDiagMat_Fixture, negate) {

	// negation
	EXPECT_EQ(-1, -mr22.diag().at(0));
	EXPECT_EQ(-1, -mr22.diag().at(1));

	EXPECT_EQ(-1.0*one, -mc22.diag().at(0));
	EXPECT_EQ(-1.0*one, -mc22.diag().at(1));
}

TEST_F(SqDiagMat_Fixture, matrixAddition) {

	////// addition
	// real+real
	EXPECT_EQ(2, (mr22 + mr22).diag().at(0)); EXPECT_EQ(2, (mr22 + mr22).diag().at(1));

	// complex+real
	EXPECT_EQ(complex(2, 1), (mc22 + mr22).diag().at(0)); EXPECT_EQ(complex(2, 1), (mc22 + mr22).diag().at(1));

	// real+complex
	EXPECT_EQ(complex(2, 1), (mr22 + mc22).diag().at(0)); EXPECT_EQ(complex(2, 1), (mr22 + mc22).diag().at(1));

	// comple+complex	
	EXPECT_EQ(complex(2, 2), (mc22 + mc22).diag().at(0)); EXPECT_EQ(complex(2, 2), (mc22 + mc22).diag().at(1));

}

TEST_F(SqDiagMat_Fixture, matrixSubtraction) {

	////// subtraction
	// real-real
	EXPECT_EQ(0, (mr22 - mr22).diag().at(0)); EXPECT_EQ(0, (mr22 - mr22).diag().at(1));

	// complex-real
	EXPECT_EQ(complex(0, 1), (mc22 - mr22).diag().at(0)); EXPECT_EQ(complex(0, 1), (mc22 - mr22).diag().at(1));

	// real-complex
	EXPECT_EQ(complex(0, -1), (mr22 - mc22).diag().at(0)); EXPECT_EQ(complex(0, -1), (mr22 - mc22).diag().at(1));

	// complex+complex	
	EXPECT_EQ(zero, (mc22 - mc22).diag().at(0)); EXPECT_EQ(zero, (mc22 - mc22).diag().at(1));

}

TEST_F(SqDiagMat_Fixture, matrixMultiplication) {

	///// multiplication
	// real*real
	EXPECT_EQ(1.0, (mr22*mr22).diag().at(0)); EXPECT_EQ(1.0, (mr22*mr22).diag().at(1));

	// real*complex
	EXPECT_EQ(complex(1, 1), (mr22*mc22).diag().at(0)); EXPECT_EQ(complex(1, 1), (mr22*mc22).diag().at(1));

	// complex*real
	EXPECT_EQ(complex(1, 1), (mc22*mr22).diag().at(0)); EXPECT_EQ(complex(1, 1), (mc22*mr22).diag().at(1));

	// complex*complex
	EXPECT_EQ(complex(0, 2), (mc22*mc22).diag().at(0)); EXPECT_EQ(complex(0, 2), (mc22*mc22).diag().at(1));
}

TEST_F(SqDiagMat_Fixture, scalarAddition)
{

	// real matrix + real scalar
	EXPECT_EQ(2, (mr22 + 1.0).diag().at(0)); EXPECT_EQ(2, (mr22 + 1.0).diag().at(1));

	// real scalar + real matrix
	EXPECT_EQ(2, (1.0 + mr22).diag().at(0)); EXPECT_EQ(2, (1.0 + mr22).diag().at(1));

	// real matrix + complex scalar
	EXPECT_EQ(complex(2, 1), (mr22 + one).diag().at(0)); EXPECT_EQ(complex(2, 1), (mr22 + one).diag().at(1));

	// complex scalar + real matrix
	EXPECT_EQ(complex(2, 1), (one + mr22).diag().at(0)); EXPECT_EQ(complex(2, 1), (one + mr22).diag().at(1));

	// complex matrix + real scalar
	EXPECT_EQ(complex(2, 1), (mc22 + 1.0).diag().at(0)); EXPECT_EQ(complex(2, 1), (mc22 + 1.0).diag().at(1));

	// real scalar + complex matrix
	EXPECT_EQ(complex(2, 1), (1.0 + mc22).diag().at(0)); EXPECT_EQ(complex(2, 1), (1.0 + mc22).diag().at(1));

	// complex matrix + complex scalar
	EXPECT_EQ(complex(2, 2), (mc22 + one).diag().at(0)); EXPECT_EQ(complex(2, 2), (mc22 + one).diag().at(1));

	// complex scalar + complex matrix
	EXPECT_EQ(complex(2, 2), (one + mc22).diag().at(0)); EXPECT_EQ(complex(2, 2), (one + mc22).diag().at(1));
}

TEST_F(SqDiagMat_Fixture, scalarSubtraction) {

	/// SUBTRACTION
	// real matrix - real scalar
	EXPECT_EQ(0, (mr22 - 1.0).diag().at(0)); EXPECT_EQ(0, (mr22 - 1.0).diag().at(1));

	// real matrix - complex scalar
	EXPECT_EQ(complex(0, -1), (mr22 - one).diag().at(0)); EXPECT_EQ(complex(0, -1), (mr22 - one).diag().at(1));

	// complex matrix - real scalar
	EXPECT_EQ(complex(0, 1), (mc22 - 1.0).diag().at(0)); EXPECT_EQ(complex(0, 1), (mc22 - 1.0).diag().at(1));

	// complex matrix - complex scalar
	EXPECT_EQ(complex(0, 0), (mc22 - one).diag().at(0)); EXPECT_EQ(complex(0, 0), (mc22 - one).diag().at(1));
}

TEST_F(SqDiagMat_Fixture, scalarMultiplication) {

	/// SCALAR MULTIPLICATION
	// real scalar * real mat
	EXPECT_EQ(2, (2.0*mr22).diag().at(0)); EXPECT_EQ(2, (2.0*mr22).diag().at(1));

	// real scalar * complex mat
	EXPECT_EQ(complex(2, 2), (2.0*mc22).diag().at(0)); EXPECT_EQ(complex(2, 2), (2.0*mc22).diag().at(1));

	// complex scalar * real mat
	EXPECT_EQ(complex(2, 2), (complex(2, 2)*mr22).diag().at(0)); EXPECT_EQ(complex(2, 2), (complex(2, 2)*mr22).diag().at(1));

	// complex scalar * complex mat
	EXPECT_EQ(complex(0, 4), (complex(2, 2)*mc22).diag().at(0)); EXPECT_EQ(complex(0, 4), (complex(2, 2)*mc22).diag().at(1));
}

TEST_F(SqDiagMat_Fixture, scalarDivision) {

	/// SCALAR DIVISION
	// real mat / real scalar
	EXPECT_EQ(0.5, (mr22 / 2.0).diag().at(0)); EXPECT_EQ(0.5, (mr22 / 2.0).diag().at(1));

	// real mat / complex scalar
	EXPECT_EQ(complex(0.25, -0.25), (mr22 / complex(2, 2)).diag().at(0)); EXPECT_EQ(complex(0.25, -0.25), (mr22 / complex(2, 2)).diag().at(1));

	// complex mat / real scalar
	EXPECT_EQ(complex(0.5, 0.5), (mc22 / 2.0).diag().at(0)); EXPECT_EQ(complex(0.5, 0.5), (mc22 / 2.0).diag().at(1));

	// complex mat/complex scalar
	EXPECT_EQ(complex(0.5, 0), (mc22 / complex(2, 2)).diag().at(0)); EXPECT_EQ(complex(0.5, 0), (mc22 / complex(2, 2)).diag().at(1));
}

/*
TEST_F(SqDiagMat_Fixture, sum)
{
	EXPECT_NEAR_V(x*harm0 + x*harm0, (x + x)*harm0, tolerance);
	EXPECT_NEAR_V(x*harm0 + ix*harm0, (x + ix)*harm0, tolerance);
	EXPECT_NEAR_V(ix*harm0 + x*harm0, (ix + x)*harm0, tolerance);
	EXPECT_NEAR_V(ix*harm0 + ix*harm0, (ix + ix)*harm0, tolerance);
}

TEST_F(SqDiagMat_Fixture, difference)
{
	EXPECT_NEAR_V(x*harm0 - x*harm0, (x - x)*harm0, tolerance);
	EXPECT_NEAR_V(x*harm0 - ix*harm0, (x - ix)*harm0, tolerance);
	EXPECT_NEAR_V(ix*harm0 - x*harm0, (ix - x)*harm0, tolerance);
	EXPECT_NEAR_V(ix*harm0 - ix*harm0, (ix - ix)*harm0, tolerance);
}

TEST_F(SqDiagMat_Fixture, negate)
{
	EXPECT_NEAR_V(-x*harm0, -(x*harm0), tolerance);
	EXPECT_NEAR_V(-ix*harm0, -(ix*harm0), tolerance);
}

TEST_F(SqDiagMat_Fixture, multiply_with_constant)
{
	EXPECT_NEAR_V(2.0*(x*harm0), (2.0 * x)*harm0, tolerance);
	EXPECT_NEAR_V(2.0*(ix*harm0), (2.0 * ix)*harm0, tolerance);
	EXPECT_NEAR_V(2.0*1i*(x*harm0), (2.0*1i * x)*harm0, tolerance);
	EXPECT_NEAR_V(2.0*1i*(ix*harm0), (2.0*1i * ix)*harm0, tolerance);
}
*/

TEST_F(SqDiagMat_Fixture, kron)
{
	const auto dim = 3;

	const auto Ar = SqDiagMat<real>(Vector<real>(arma::randn(dim)));
	const auto Br = SqDiagMat<real>(Vector<real>(arma::randn(dim)));

	const auto Ac = SqDiagMat<complex>(Vector<complex>(arma::cx_vec(arma::randn(dim), arma::randn(dim))));
	const auto Bc = SqDiagMat<complex>(Vector<complex>(arma::cx_vec(arma::randn(dim), arma::randn(dim))));

	const auto Crr = SqDiagMat<real>(Vector<real>(arma::vec(arma::kron(arma::vec(Ar.diag()._vec()), arma::vec(Br.diag()._vec())))));
	EXPECT_EQ(Crr, kron(Ar, Br));

	const auto Crc = SqDiagMat<complex>(Vector<complex>(arma::cx_vec(arma::kron(arma::vec(Ar.diag()._vec()), arma::cx_vec(Bc.diag()._vec())))));
	EXPECT_EQ(Crc, kron(Ar, Bc));

	const auto Ccr = SqDiagMat<complex>(Vector<complex>(arma::cx_vec(arma::kron(arma::cx_vec(Ac.diag()._vec()), arma::vec(Br.diag()._vec())))));
	EXPECT_EQ(Ccr, kron(Ac, Br));

	const auto Ccc = SqDiagMat<complex>(Vector<complex>(arma::cx_vec(arma::kron(arma::cx_vec(Ac.diag()._vec()), arma::cx_vec(Bc.diag()._vec())))));
	EXPECT_EQ(Ccc, kron(Ac, Bc));
}


﻿/* COPYRIGHT
 *
 * file="T_SqDenseMat.cpp" 
 * Copyright (c) 2017 - 2018 Aarhus University, Department of Physics and Astronomy, All rights reserved
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a 
 * copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "src/Utility/Constants.h"

#include "src/Utility/LinearAlgebra/GenericSquareMatrixArithmetic.h"
#include "src/Utility/LinearAlgebra/SqHermitianBandMat.h"
#include "src/Utility/LinearAlgebra/SqDiagMat.h"
#include "src/Utility/LinearAlgebra/SqDenseMat.h"

#include "additionalAsserts.h"
#include "HarmonicAnalyticResults.h"

#include "SqMat_Fixture.h"

using namespace qengine;
using namespace internal;
using namespace std::complex_literals;

class SqDenseMat_Fixture : public SqMat_Fixture
{
	arma::mat makeHmat() const
	{
		auto a = 30.0 / 12.0;
		auto b = -16.0 / 12.0;
		auto c = 1.0 / 12.0;

		auto scale = 0.5 / (dx*dx);

		auto kinetic = arma::mat(defaultDim, defaultDim, arma::fill::zeros);
		kinetic.diag(0) += a*scale;
		kinetic.diag(1) += b*scale;
		kinetic.diag(-1) += b*scale;
		kinetic.diag(2) += c*scale;
		kinetic.diag(-2) += c*scale;

		auto potential = 0.5*omega*omega*(xArray - x0) * (xArray - x0);
		auto Hmat = kinetic;
		Hmat.diag() += potential._vec();
		return Hmat;
	}
public:
	SqDenseMat<real> H{ RMat(makeHmat()) };
	SqDenseMat<complex> iH{ CMat(1i*makeHmat()) };
	SqDenseMat<complex> cH{ CMat(arma::conv_to<arma::cx_mat>::from(makeHmat())) };

	SqDenseMat<real> mr22{ RMat(2, 2, 1) };
	SqDenseMat<real> mr22original= mr22;

	SqDenseMat<complex> mc22{ CMat(2, 2, {1,1}) };
	SqDenseMat<complex> mc22original = mc22;

	complex one{ 1, 1 };
	complex zero{ 0, 0 };
};

TEST_F(SqDenseMat_Fixture, traits)
{
	static_assert(is_SqMat_v<SqDenseMat>, "SqDenseMat does not fulfill is_SqMat!");
}

TEST_F(SqDenseMat_Fixture, dimensions)
{
	EXPECT_EQ(defaultDim, H.dim());
	EXPECT_EQ(defaultDim, iH.dim());
	EXPECT_EQ(defaultDim, cH.dim());
}

TEST_F(SqDenseMat_Fixture, isHermitian)
{
	EXPECT_TRUE(H.isHermitian());
	EXPECT_FALSE(iH.isHermitian());

	EXPECT_TRUE(cH.isHermitian());
}

TEST_F(SqDenseMat_Fixture, multiplyWithState)
{
	EXPECT_NEAR(omega*0.5, std::real(cdot(harm0, H*harm0))*dx, 2e-7);
	EXPECT_NEAR(0, std::imag(cdot(harm0, H*harm0))*dx, 1e-7);

	EXPECT_NEAR(0, std::real(cdot(harm0, iH*harm0))*dx, 1e-7);
	EXPECT_NEAR(omega*0.5, std::imag(cdot(harm0, iH*harm0))*dx, 2e-7);

	auto psi = (harm0+ 1i*harm1) / std::sqrt(2);
	EXPECT_NEAR(omega, std::real(cdot(psi, H*psi))*dx, 8e-7) << "psi = harm0+i*harm1";
	EXPECT_NEAR(0, std::imag(cdot(psi, H*psi))*dx, 1e-7) << "psi = harm0+i*harm1";

	EXPECT_NEAR(0, std::real(cdot(psi, iH*psi))*dx, 1e-7) << "psi = harm0+i*harm1";
	EXPECT_NEAR(omega, std::imag(cdot(psi, iH*psi))*dx, 8e-7) << "psi = harm0+i*harm1";
}

TEST_F(SqDenseMat_Fixture, eigHerm)
{
	count_t largestEigenstate = 2;
		arma::vec expectedvals = omega * (0.5 + arma::linspace(0, static_cast<double>(largestEigenstate), largestEigenstate + 1));

	{
		auto spectrum = getSpectrum_herm(H, largestEigenstate);

		auto vals = spectrum.eigenvalues();
		auto vecs = spectrum.eigenvectors();

		EXPECT_EQ(largestEigenstate + 1, vals.size());
		EXPECT_EQ(largestEigenstate + 1, vecs.n_cols());

		for (auto i = 0u; i <= largestEigenstate; ++i)
		{
			EXPECT_NEAR(expectedvals.at(i), vals.at(i), 5e-6) << "x val" << i;

			auto state = CVec(vecs.col(i));
			EXPECT_NEAR(vals.at(i), std::real(dot(state, H*state)), 1e-7) << "x vec" << i;
		}
	}
	{
		auto spectrum = getSpectrum_herm(cH, largestEigenstate);

		auto vals = spectrum.eigenvalues();
		auto vecs = spectrum.eigenvectors();

		EXPECT_EQ(largestEigenstate + 1, vals.size());
		EXPECT_EQ(largestEigenstate + 1, vecs.n_cols());


		for (auto i = 0u; i <= largestEigenstate; ++i)
		{
			EXPECT_NEAR(expectedvals.at(i), vals.at(i), 5e-6) << "x val" << i;

			auto state = CVec(vecs.col(i));
			EXPECT_NEAR(vals.at(i), std::real(dot(state, cH*state)), 1e-7) << "x vec" << i;
		}
	}
}

TEST_F(SqDenseMat_Fixture, exponentiate)
{
	EXPECT_NEAR_V(exp(H)*harm0, CVec(arma::expmat(H.mat()._mat()) * harm0._vec()), tolerance);
	EXPECT_NEAR_V(exp(iH)*harm0, CVec(arma::expmat(1i*H.mat()._mat())* harm0._vec()), tolerance);
}

TEST_F(SqDenseMat_Fixture,pow){
    auto pow1 = pow(mr22,1);
    auto pow2 = pow(mr22,2);
    auto pow3 = pow(mr22,3);

    EXPECT_EQ(1,pow1.mat().at(0,0)); EXPECT_EQ(1,pow1.mat().at(0,1));
    EXPECT_EQ(1,pow1.mat().at(1,0)); EXPECT_EQ(1,pow1.mat().at(1,1));
    EXPECT_EQ(2,pow2.mat().at(0,0)); EXPECT_EQ(2,pow2.mat().at(0,1));
    EXPECT_EQ(2,pow2.mat().at(1,0)); EXPECT_EQ(2,pow2.mat().at(1,1));
    EXPECT_EQ(4,pow3.mat().at(0,0)); EXPECT_EQ(4,pow3.mat().at(0,1));
    EXPECT_EQ(4,pow3.mat().at(1,0)); EXPECT_EQ(4,pow3.mat().at(1,1));

    auto cpow1 = pow(mc22,1);
    auto cpow2 = pow(mc22,2);
    auto cpow3 = pow(mc22,3);

    EXPECT_EQ(one,cpow1.mat().at(0,0)); EXPECT_EQ(one,cpow1.mat().at(0,1));
    EXPECT_EQ(one,cpow1.mat().at(1,0)); EXPECT_EQ(one,cpow1.mat().at(1,1));
    EXPECT_EQ(complex(0,4),cpow2.mat().at(0,0)); EXPECT_EQ(complex(0,4),cpow2.mat().at(0,1));
    EXPECT_EQ(complex(0,4),cpow2.mat().at(1,0)); EXPECT_EQ(complex(0,4),cpow2.mat().at(1,1));
    EXPECT_EQ(complex(-8,8),cpow3.mat().at(0,0)); EXPECT_EQ(complex(-8,8),cpow3.mat().at(0,1));
    EXPECT_EQ(complex(-8,8),cpow3.mat().at(1,0)); EXPECT_EQ(complex(-8,8),cpow3.mat().at(1,1));
}

TEST_F(SqDenseMat_Fixture, inplaceSum) 
{

	mr22 += mr22;
	EXPECT_EQ(2, mr22.mat().at(0, 0));
	EXPECT_EQ(2, mr22.mat().at(0, 1));
	EXPECT_EQ(2, mr22.mat().at(1, 0));
	EXPECT_EQ(2, mr22.mat().at(1, 1));
	mr22 = mr22original;

    mc22 += mr22;
    EXPECT_EQ(complex(2,1), mc22.mat().at(0, 0));
    EXPECT_EQ(complex(2,1), mc22.mat().at(1, 0));
    EXPECT_EQ(complex(2,1), mc22.mat().at(0, 1));
    EXPECT_EQ(complex(2,1), mc22.mat().at(1, 1));
    mc22 = mc22original;

	mc22 += mc22;
	EXPECT_EQ(2.0*one, mc22.mat().at(0, 0));
	EXPECT_EQ(2.0*one, mc22.mat().at(1, 0));
	EXPECT_EQ(2.0*one, mc22.mat().at(0, 1));
	EXPECT_EQ(2.0*one, mc22.mat().at(1, 1));

	mc22 = mc22original;
}

TEST_F(SqDenseMat_Fixture, inplaceSubtract) {

	// subtraction
	mr22 -= mr22;
	EXPECT_EQ(0, mr22.mat().at(0, 0));
	EXPECT_EQ(0, mr22.mat().at(0, 1));
	EXPECT_EQ(0, mr22.mat().at(1, 0));
	EXPECT_EQ(0, mr22.mat().at(1, 1));
	mr22 = mr22original;

    mc22 -= mr22;
    EXPECT_EQ(complex(0,1), mc22.mat().at(0, 0));
    EXPECT_EQ(complex(0,1), mc22.mat().at(1, 0));
    EXPECT_EQ(complex(0,1), mc22.mat().at(0, 1));
    EXPECT_EQ(complex(0,1), mc22.mat().at(1, 1));
    mc22 = mc22original;

	mc22 -= mc22;
	EXPECT_EQ(zero, mc22.mat().at(0, 0));
	EXPECT_EQ(zero, mc22.mat().at(1, 0));
	EXPECT_EQ(zero, mc22.mat().at(0, 1));
	EXPECT_EQ(zero, mc22.mat().at(1, 1));
	mc22 = mc22original;
}

TEST_F(SqDenseMat_Fixture, inplaceScalarMultiply) {

	// scalar multiplication
	mr22 *= 2.0;
	EXPECT_EQ(2, mr22.mat().at(0, 0));
	EXPECT_EQ(2, mr22.mat().at(0, 1));
	EXPECT_EQ(2, mr22.mat().at(1, 0));
	EXPECT_EQ(2, mr22.mat().at(1, 1));
	mr22 = mr22original;

	mc22 *= 2.0;
	EXPECT_EQ(2.0*one, mc22.mat().at(0, 0));
	EXPECT_EQ(2.0*one, mc22.mat().at(1, 0));
	EXPECT_EQ(2.0*one, mc22.mat().at(0, 1));
	EXPECT_EQ(2.0*one, mc22.mat().at(1, 1));
	mc22 = mc22original;

	mc22 *= one;
	EXPECT_EQ(complex(0, 2), mc22.mat().at(0, 0));
	EXPECT_EQ(complex(0, 2), mc22.mat().at(1, 0));
	EXPECT_EQ(complex(0, 2), mc22.mat().at(0, 1));
	EXPECT_EQ(complex(0, 2), mc22.mat().at(1, 1));
	mc22 = mc22original;
}

TEST_F(SqDenseMat_Fixture,inplaceScalarDivision){

	// scalar division
	mr22 /=2.0; 
	EXPECT_EQ(0.5,mr22.mat().at(0,0));
	EXPECT_EQ(0.5,mr22.mat().at(0,1));
	EXPECT_EQ(0.5,mr22.mat().at(1,0));
	EXPECT_EQ(0.5,mr22.mat().at(1,1));
	mr22 = mr22original;

	mc22 /=2.0;
	EXPECT_EQ(0.5*one,mc22.mat().at(0,0));
	EXPECT_EQ(0.5*one,mc22.mat().at(1,0));
	EXPECT_EQ(0.5*one,mc22.mat().at(0,1));
	EXPECT_EQ(0.5*one,mc22.mat().at(1,1));
	mc22 = mc22original;

	mc22 /=one;
	EXPECT_EQ(complex(1,0),mc22.mat().at(0,0));
	EXPECT_EQ(complex(1,0),mc22.mat().at(1,0));
	EXPECT_EQ(complex(1,0),mc22.mat().at(0,1));
	EXPECT_EQ(complex(1,0),mc22.mat().at(1,1));
	mc22 = mc22original;
}

TEST_F(SqDenseMat_Fixture, negate) {

	// negation
	EXPECT_EQ(-1, -mr22.mat().at(0, 0));
	EXPECT_EQ(-1, -mr22.mat().at(0, 1));
	EXPECT_EQ(-1, -mr22.mat().at(1, 0));
	EXPECT_EQ(-1, -mr22.mat().at(1, 1));

	EXPECT_EQ(-1.0*one, -mc22.mat().at(0, 0));
	EXPECT_EQ(-1.0*one, -mc22.mat().at(0, 1));
	EXPECT_EQ(-1.0*one, -mc22.mat().at(1, 0));
	EXPECT_EQ(-1.0*one, -mc22.mat().at(1, 1));
}

TEST_F(SqDenseMat_Fixture, matrixAddition){

	////// addition

	// real+real
	EXPECT_EQ(2,(mr22+mr22).mat().at(0,0));EXPECT_EQ(2,(mr22+mr22).mat().at(0,1));
	EXPECT_EQ(2,(mr22+mr22).mat().at(1,0));EXPECT_EQ(2,(mr22+mr22).mat().at(1,1));

	// complex+real
	EXPECT_EQ(complex(2,1),(mc22+mr22).mat().at(0,0)); EXPECT_EQ(complex(2,1),(mc22+mr22).mat().at(0,1));
	EXPECT_EQ(complex(2,1),(mc22+mr22).mat().at(1,0)); EXPECT_EQ(complex(2,1),(mc22+mr22).mat().at(1,1));

	// real+complex
	EXPECT_EQ(complex(2,1),(mr22+mc22).mat().at(0,0)); EXPECT_EQ(complex(2,1),(mr22+mc22).mat().at(0,1));
	EXPECT_EQ(complex(2,1),(mr22+mc22).mat().at(1,0)); EXPECT_EQ(complex(2,1),(mr22+mc22).mat().at(1,1));

	// comple+complex	
	EXPECT_EQ(complex(2,2),(mc22+mc22).mat().at(0,0)); EXPECT_EQ(complex(2,2),(mc22+mc22).mat().at(0,1));
	EXPECT_EQ(complex(2,2),(mc22+mc22).mat().at(1,0)); EXPECT_EQ(complex(2,2),(mc22+mc22).mat().at(1,1));

}

TEST_F(SqDenseMat_Fixture, matrixSubtraction) {

	////// subtraction
	// real-real
	EXPECT_EQ(0, (mr22 - mr22).mat().at(0, 0)); EXPECT_EQ(0, (mr22 - mr22).mat().at(0, 1));
	EXPECT_EQ(0, (mr22 - mr22).mat().at(1, 0)); EXPECT_EQ(0, (mr22 - mr22).mat().at(1, 1));

	// complex-real
	EXPECT_EQ(complex(0, 1), (mc22 - mr22).mat().at(0, 0)); EXPECT_EQ(complex(0, 1), (mc22 - mr22).mat().at(0, 1));
	EXPECT_EQ(complex(0, 1), (mc22 - mr22).mat().at(1, 0)); EXPECT_EQ(complex(0, 1), (mc22 - mr22).mat().at(1, 1));

	// real-complex
	EXPECT_EQ(complex(0, -1), (mr22 - mc22).mat().at(0, 0)); EXPECT_EQ(complex(0, -1), (mr22 - mc22).mat().at(0, 1));
	EXPECT_EQ(complex(0, -1), (mr22 - mc22).mat().at(1, 0)); EXPECT_EQ(complex(0, -1), (mr22 - mc22).mat().at(1, 1));

	// complex+complex	
	EXPECT_EQ(zero, (mc22 - mc22).mat().at(0, 0)); EXPECT_EQ(zero, (mc22 - mc22).mat().at(0, 1));
	EXPECT_EQ(zero, (mc22 - mc22).mat().at(1, 0)); EXPECT_EQ(zero, (mc22 - mc22).mat().at(1, 1));

}

TEST_F(SqDenseMat_Fixture, matrixMultiplication) {

	///// multiplication
	// real*real
	EXPECT_EQ(2.0, (mr22*mr22).mat().at(0, 0)); EXPECT_EQ(2.0, (mr22*mr22).mat().at(0, 1));
	EXPECT_EQ(2.0, (mr22*mr22).mat().at(1, 0)); EXPECT_EQ(2.0, (mr22*mr22).mat().at(1, 1));

	// real*complex
	EXPECT_EQ(complex(2, 2), (mr22*mc22).mat().at(0, 0)); EXPECT_EQ(complex(2, 2), (mr22*mc22).mat().at(0, 1));
	EXPECT_EQ(complex(2, 2), (mr22*mc22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (mr22*mc22).mat().at(1, 1));

	// complex*real
	EXPECT_EQ(complex(2, 2), (mc22*mr22).mat().at(0, 0)); EXPECT_EQ(complex(2, 2), (mc22*mr22).mat().at(0, 1));
	EXPECT_EQ(complex(2, 2), (mc22*mr22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (mc22*mr22).mat().at(1, 1));

	// complex*complex
	EXPECT_EQ(complex(0, 4), (mc22*mc22).mat().at(0, 0)); EXPECT_EQ(complex(0, 4), (mc22*mc22).mat().at(0, 1));
	EXPECT_EQ(complex(0, 4), (mc22*mc22).mat().at(1, 0)); EXPECT_EQ(complex(0, 4), (mc22*mc22).mat().at(1, 1));
}

TEST_F(SqDenseMat_Fixture, scalarAddition) {

	// ADDITION
	// real matrix + real scalar
	EXPECT_EQ(2, (mr22 + 1.0).mat().at(0, 0)); EXPECT_EQ(1, (mr22 + 1.0).mat().at(0, 1));
	EXPECT_EQ(1, (mr22 + 1.0).mat().at(1, 0)); EXPECT_EQ(2, (mr22 + 1.0).mat().at(1, 1));
	// real scalar + real matrix
	EXPECT_EQ(2, (1.0 + mr22).mat().at(0, 0)); EXPECT_EQ(1, (1.0 + mr22).mat().at(0, 1));
	EXPECT_EQ(1, (1.0 + mr22).mat().at(1, 0)); EXPECT_EQ(2, (1.0 + mr22).mat().at(1, 1));

	// real matrix + complex scalar
	EXPECT_EQ(complex(2, 1), (mr22 + one).mat().at(0, 0)); EXPECT_EQ(complex(1, 0), (mr22 + one).mat().at(0, 1));
	EXPECT_EQ(complex(1, 0), (mr22 + one).mat().at(1, 0)); EXPECT_EQ(complex(2, 1), (mr22 + one).mat().at(1, 1));

	// complex scalar + real matrix
	EXPECT_EQ(complex(2, 1), (one + mr22).mat().at(0, 0)); EXPECT_EQ(complex(1, 0), (one + mr22).mat().at(0, 1));
	EXPECT_EQ(complex(1, 0), (one + mr22).mat().at(1, 0)); EXPECT_EQ(complex(2, 1), (one + mr22).mat().at(1, 1));

	// complex matrix + real scalar
	EXPECT_EQ(complex(2, 1), (mc22 + 1.0).mat().at(0, 0)); EXPECT_EQ(complex(1, 1), (mc22 + 1.0).mat().at(0, 1));
	EXPECT_EQ(complex(1, 1), (mc22 + 1.0).mat().at(1, 0)); EXPECT_EQ(complex(2, 1), (mc22 + 1.0).mat().at(1, 1));

	// real scalar + complex matrix
	EXPECT_EQ(complex(2, 1), (1.0 + mc22).mat().at(0, 0)); EXPECT_EQ(complex(1, 1), (1.0 + mc22).mat().at(0, 1));
	EXPECT_EQ(complex(1, 1), (1.0 + mc22).mat().at(1, 0)); EXPECT_EQ(complex(2, 1), (1.0 + mc22).mat().at(1, 1));

	// complex matrix + complex scalar
	EXPECT_EQ(complex(2, 2), (mc22 + one).mat().at(0, 0)); EXPECT_EQ(complex(1, 1), (mc22 + one).mat().at(0, 1));
	EXPECT_EQ(complex(1, 1), (mc22 + one).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (mc22 + one).mat().at(1, 1));

	// complex scalar + complex matrix
	EXPECT_EQ(complex(2, 2), (one + mc22).mat().at(0, 0)); EXPECT_EQ(complex(1, 1), (one + mc22).mat().at(0, 1));
	EXPECT_EQ(complex(1, 1), (one + mc22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (one + mc22).mat().at(1, 1));
}

TEST_F(SqDenseMat_Fixture, scalarSubtraction) {

	/// SUBTRACTION
	// real matrix - real scalar
	EXPECT_EQ(0, (mr22 - 1.0).mat().at(0, 0)); EXPECT_EQ(1, (mr22 - 1.0).mat().at(0, 1));
	EXPECT_EQ(1, (mr22 - 1.0).mat().at(1, 0)); EXPECT_EQ(0, (mr22 - 1.0).mat().at(1, 1));

	// real matrix - complex scalar
	EXPECT_EQ(complex(0, -1), (mr22 - one).mat().at(0, 0)); EXPECT_EQ(complex(1, 0), (mr22 - one).mat().at(0, 1));
	EXPECT_EQ(complex(1, 0), (mr22 - one).mat().at(1, 0)); EXPECT_EQ(complex(0, -1), (mr22 - one).mat().at(1, 1));

	// complex matrix - real scalar
	EXPECT_EQ(complex(0, 1), (mc22 - 1.0).mat().at(0, 0)); EXPECT_EQ(complex(1, 1), (mc22 - 1.0).mat().at(0, 1));
	EXPECT_EQ(complex(1, 1), (mc22 - 1.0).mat().at(1, 0)); EXPECT_EQ(complex(0, 1), (mc22 - 1.0).mat().at(1, 1));

	// complex matrix - complex scalar
	EXPECT_EQ(complex(0, 0), (mc22 - one).mat().at(0, 0)); EXPECT_EQ(complex(1, 1), (mc22 - one).mat().at(0, 1));
	EXPECT_EQ(complex(1, 1), (mc22 - one).mat().at(1, 0)); EXPECT_EQ(complex(0, 0), (mc22 - one).mat().at(1, 1));
}

TEST_F(SqDenseMat_Fixture, scalarMultiplication) {

	/// SCALAR MULTIPLICATION
	mr22 = SqDenseMat<real>(RMat(2, 2, 1));
	mc22 = SqDenseMat<complex>(CMat(2, 2, complex(1.0, 1.0)));

	// real scalar * real mat
	EXPECT_EQ(2, (2.0*mr22).mat().at(0, 0)); EXPECT_EQ(2, (2.0*mr22).mat().at(0, 1));
	EXPECT_EQ(2, (2.0*mr22).mat().at(1, 0)); EXPECT_EQ(2, (2.0*mr22).mat().at(1, 1));

	// real scalar * complex mat
	EXPECT_EQ(complex(2, 2), (2.0*mc22).mat().at(0, 0)); EXPECT_EQ(complex(2, 2), (2.0*mc22).mat().at(0, 1));
	EXPECT_EQ(complex(2, 2), (2.0*mc22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (2.0*mc22).mat().at(1, 1));

	// complex scalar * real mat
	EXPECT_EQ(complex(2, 2), (complex(2, 2)*mr22).mat().at(0, 0)); EXPECT_EQ(complex(2, 2), (complex(2, 2)*mr22).mat().at(0, 1));
	EXPECT_EQ(complex(2, 2), (complex(2, 2)*mr22).mat().at(1, 0)); EXPECT_EQ(complex(2, 2), (complex(2, 2)*mr22).mat().at(1, 1));

	// complex scalar * complex mat
	EXPECT_EQ(complex(0, 4), (complex(2, 2)*mc22).mat().at(0, 0)); EXPECT_EQ(complex(0, 4), (complex(2, 2)*mc22).mat().at(0, 1));
	EXPECT_EQ(complex(0, 4), (complex(2, 2)*mc22).mat().at(1, 0)); EXPECT_EQ(complex(0, 4), (complex(2, 2)*mc22).mat().at(1, 1));

	mr22 = SqDenseMat<real>(RMat(2, 2, 1));
	mc22 = SqDenseMat<complex>(CMat(2, 2, complex(1.0, 1.0)));
}

TEST_F(SqDenseMat_Fixture, scalarDivision) {

	/// SCALAR DIVISION
	// real mat / real scalar
	EXPECT_EQ(0.5, (mr22 / 2.0).mat().at(0, 0)); EXPECT_EQ(0.5, (mr22 / 2.0).mat().at(0, 1));
	EXPECT_EQ(0.5, (mr22 / 2.0).mat().at(1, 0)); EXPECT_EQ(0.5, (mr22 / 2.0).mat().at(1, 1));

	// real mat / complex scalar
	EXPECT_EQ(complex(0.25, -0.25), (mr22 / complex(2, 2)).mat().at(0, 0)); EXPECT_EQ(complex(0.25, -0.25), (mr22 / complex(2, 2)).mat().at(0, 1));
	EXPECT_EQ(complex(0.25, -0.25), (mr22 / complex(2, 2)).mat().at(1, 0)); EXPECT_EQ(complex(0.25, -0.25), (mr22 / complex(2, 2)).mat().at(1, 1));

	// complex mat / real scalar
	EXPECT_EQ(complex(0.5, 0.5), (mc22 / 2.0).mat().at(0, 0)); EXPECT_EQ(complex(0.5, 0.5), (mc22 / 2.0).mat().at(0, 1));
	EXPECT_EQ(complex(0.5, 0.5), (mc22 / 2.0).mat().at(1, 0)); EXPECT_EQ(complex(0.5, 0.5), (mc22 / 2.0).mat().at(1, 1));

	// complex mat/complex scalar
	EXPECT_EQ(complex(0.5, 0), (mc22 / complex(2, 2)).mat().at(0, 0)); EXPECT_EQ(complex(0.5, 0), (mc22 / complex(2, 2)).mat().at(0, 1));
	EXPECT_EQ(complex(0.5, 0), (mc22 / complex(2, 2)).mat().at(1, 0)); EXPECT_EQ(complex(0.5, 0), (mc22 / complex(2, 2)).mat().at(1, 1));
}

TEST_F(SqDenseMat_Fixture, kron)
{
	const auto dim = 3;

	const auto Ar = SqDenseMat<real>(Matrix<real>(arma::randn(dim, dim)));
	const auto Br = SqDenseMat<real>(Matrix<real>(arma::randn(dim, dim)));

	const auto Ac = SqDenseMat<complex>(Matrix<complex>(arma::cx_mat(arma::randn(dim, dim), arma::randn(dim, dim))));
	const auto Bc = SqDenseMat<complex>(Matrix<complex>(arma::cx_mat(arma::randn(dim, dim), arma::randn(dim, dim))));

	const auto Crr = SqDenseMat<real>(Matrix<real>(arma::mat(arma::kron(arma::mat(Ar.mat()._mat()), arma::mat(Br.mat()._mat())))));
	EXPECT_EQ(Crr, kron(Ar, Br));

	const auto Crc = SqDenseMat<complex>(Matrix<complex>(arma::cx_mat(arma::kron(arma::mat(Ar.mat()._mat()), arma::cx_mat(Bc.mat()._mat())))));
	EXPECT_EQ(Crc, kron(Ar, Bc));

	const auto Ccr = SqDenseMat<complex>(Matrix<complex>(arma::cx_mat(arma::kron(arma::cx_mat(Ac.mat()._mat()), arma::mat(Br.mat()._mat())))));
	EXPECT_EQ(Ccr, kron(Ac, Br));

	const auto Ccc = SqDenseMat<complex>(Matrix<complex>(arma::cx_mat(arma::kron(arma::cx_mat(Ac.mat()._mat()), arma::cx_mat(Bc.mat()._mat())))));
	EXPECT_EQ(Ccc, kron(Ac, Bc));
}

#pragma once
#pragma warning(push, 0)

// standard library
#include <iostream>
#include <functional>
#include <numeric>


// dependencies
#include <armadillo>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

#pragma warning(pop)